<%-- 
    Document   : side-menu
    Created on : Jan 2, 2017, 9:39:32 PM
    Author     : Hoang Tuyet
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="row-fluid text-center">
    <div class="col-sm-3 col-md-3" style="height: 300px">
        <div class="panel-group" id="accordion">
            <!--menu for admin-->

            <c:if test="${sessionScope.empInfo.position eq 'adminv2'}">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                </span>Service Management</a> <!--Change here for the outside menu content-->
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <table class="table">
                                <tr>
                                    <td>
                                        <a href="TuyetTestController?action=group">Service Group Management</a> <!--Change here for the sub menu content-->
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <a href="TuyetTestController?action=price">Service Price Management</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <a href="TuyetTestController?action=service">Service Management</a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </c:if>
            <!--Menu for doctor & doctorv2-->
            <c:if test="${sessionScope.empInfo.position eq 'doctorv2' or sessionScope.empInfo.position eq 'doctor' or sessionScope.empInfo.position eq 'testdoctorv2'}">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Test Management</a>
                        </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse">
                        <div class="panel-body">
                            <table class="table">
                                <c:if test="${sessionScope.empInfo.position eq 'doctor' || sessionScope.empInfo.position eq 'doctorv2'}">
                                    <tr>
                                        <td>
                                            <a href="TuyetTestController?action=mainreq">Test Request Management</a>
                                        </td>
                                    </tr>
                                </c:if>
                                <!--menu for testdoctorv2-->
                                <c:if test="${sessionScope.empInfo.position eq 'testdoctorv2'}">
                                    <tr>
                                        <td>
                                            <a href="TuyetTestController?action=result">Test Result Management</a>
                                        </td>
                                    </tr>
                                </c:if>
                            </table>
                        </div>
                    </div>
                </div>
                <!--menu for doctor-->
                <c:if test="${sessionScope.empInfo.position eq 'doctor'}">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a href="TuyetTestController?action=treament">Daily Treament Management</a>
                            </h4>
                        </div>
                    </div>
                </c:if>
            </c:if>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Account</a>
                    </h4>
                </div>
                <div id="collapseThree" class="panel-collapse collapse">
                    <div class="panel-body">
                        <table class="table">
                            <tr>
                                <td>
                                    <a href="TuyetTestController?action=logout">Logout</a>
                                </td>
                            </tr>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- css for side menu -->
    <style>
        .glyphicon { margin-right:10px; }
        .panel-body { padding:0px; }
        .panel-body table tr td { padding-left: 15px }
        .panel-body .table {margin-bottom: 0px; }
    </style>
