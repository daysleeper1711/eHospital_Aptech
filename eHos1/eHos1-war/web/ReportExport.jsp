<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="loc" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Report Import</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!-- jQuery -->
        <script src="js/jquery.js"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>
        <style>
            html {
                position: relative;
                min-height: 100%;
            }
            body {
                /* Margin bottom by footer height */
                margin-bottom: 10px;
            }
            footer {
                position: absolute;
                bottom: 0;
                width: 100%;
                /* Set the fixed height of the footer here */
                height: 10px;
                background-color: #f5f5f5;
            }
            header {
                background: url(images/banner.jpg) no-repeat;
                background-size: cover;
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;	
                height:130px;
            }
            /*Borderless*/
            .showInfo td {
                padding-top: 10px;
                padding-left: 20px;
                padding-right: 30px;
                padding-bottom: 10px;
            }

            .loccolum{
                display: table-cell;
                padding: 0px 8px 0px 8px;

            }
            .loccenter{
                display: table;
                padding: 0px 0px 0px 20px;
                text-align: center;
                width: 100%;
            }
        </style>
    </head>
    <body>
        <!--Header logo and user-->
        <div class="row-fluid text-center">
            <div class="col-sm-3 col-md-3"></div>
            <div class="col-sm-8 col-md-8">
                <div class="col-sm-3 col-md-3">
                    <img src="images/logo.jpg" class="img-responsive" style="opacity:0.6;width:100px"/>
                </div>
                <div class="col-sm-5 col-md-5">
                    <h1 class="page-header">
                        <p>
                            REPORT EXPORT
                        </p>
                    </h1>

                </div>
            </div>
        </div>
        <div class="row-fluid text-left">
            <div class="col-sm-2 col-md-2 col-xs-2"></div>
            
            <!--Display list patient's waiting-->
            <div class="col-sm-8 col-md-8 col-xs-8">
                <form style="padding:20px">


                    <!--copy form input here...-->
                    <!--input 1-->
                    <div class="loccenter">
                        <!--Left-->

                        <div class="loccolum">
                            <div class="form-group">
                                <label>Docentry:</label> ${requestScope.header.docentry}

                            </div>
                            <div class="form-group">
                                <label>Type:</label> ${requestScope.header_type}

                            </div>
                        </div>
                        <div class="loccolum">                          
                            <div class="form-group">
                                <label>Create Date:</label> ${requestScope.header_sfromdate}
                            </div>
                            <div class="form-group">
                                <label>Update Date:</label> ${requestScope.header_stodate}
                            </div>

                        </div>
                        <div class="loccolum">
                            <div class="form-group">
                                <label>Status:</label> <samp id="Status">${requestScope.header_status}</samp> 
                            </div>
                        </div>
                    </div><!--End div Test-->

                    <div class="form-group">
                        <label>Note</label>
                        <textarea class="form-control" rows="3" id="disabledInput" type="text"  disabled >${requestScope.header.note}</textarea>
                    </div>

                </form>


                <h2>Export Drugs Details</h2>
                <div class="table-responsive" >
                    <table class="table table-bordered table-hover table-striped" >
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>ItemCode</th>
                                <th>ItemName</th>
                                <th>Package</th>
                                <th>War_Date</th>
                                <th>Quantity</th>
                                <th>X_Price</th>
                            </tr>
                        </thead>
                        <tbody>
                            <loc:forEach items="${requestScope.listdetails}"  var="dto" varStatus="stt">
                                <tr>
                                    <td>${stt.count}</td>
                                    <td>${dto.itemCode}</td>
                                    <td>
                                        <loc:forEach items="${requestScope.listITEMCODE}"  var="dto1">
                                            <loc:if test="${dto1.itemCode eq dto.itemCode}">${dto1.itemName}</loc:if>
                                        </loc:forEach>
                                    </td>
                                    <td>${dto.package1}</td>
                                    <td>${dto.exDetails}</td>
                                    <td>${dto.quantity}</td>
                                    <td>${dto.XPrice}</td>
                                </tr>
                            </loc:forEach>
                        </tbody>
                    </table>

                    <!-- end table here-->
                    <!--IMPORTANT!!! EDIT ZONE END FROM HERE - Do not change anything else-->

                </div>
            </div>
    </body>
</html>