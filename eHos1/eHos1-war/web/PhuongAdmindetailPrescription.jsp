<%-- 
    Document   : AddPrescription
    Created on : Jan 3, 2017, 4:19:57 PM
    Author     : Nguyet Phuong, Phuong
--%>
<%@include file="header.jsp" %>
<div class="col-sm-9 col-md-9">
    <h1>
        Detail Prescription<!--Change here for title of the page-->
    </h1>
    <h4><i>(${requestScope.DATE})</i></h4>
</div>
</div>
<div class="row-fluid text-left">
    <div class="col-md-1 col-sm-1"></div>
    <div class="col-sm-7 col-md-7">
        <div class="row">
            <div class="col-md-8 col-sm-8 col-xs-8"><strong>Medical record ID :</strong> ${sessionScope.MEDID.medRecordID}</div>
            <div class="col-md-4 col-sm-4 col-xs-4"><strong>Patient name:</strong> ${sessionScope.MEDID.patInfo.patName}</div>
        </div>
        <div class="row" style="padding-top: 20px;">
            <div class="col-md-4 col-sm-4 col-xs-4"><strong>Sex:</strong> ${sessionScope.MEDID.patInfo.gender}</div>
            <div class="col-md-3 col-sm-3 col-xs-3"><strong>Birthday:</strong> ${sessionScope.MEDID.patInfo.dob}</div>
            <div class="col-md-4 col-sm-4 col-xs-4"><strong>Doctor: </strong> ${requestScope.EDITPRES.pres.employeeID.fullname}</div>
        </div>
        <div class="row" style="padding-top: 20px;">
            <div class="col-md-4 col-sm-4 col-xs-4"><strong>Faculty:</strong> ${sessionScope.MEDID.currentFaculty}</div>
            <div class="col-md-3 col-sm-3 col-xs-3"><strong>Date in:</strong> ${sessionScope.MEDID.dateInHospital}</div>
            <div class="col-sm-2 col-md-2 col-xs-2"><strong>Bed:</strong> ${sessionScope.MEDID.currentBed}</div>
        </div>
        <div class="row" style="padding-top: 20px;">
            <div class="col-md-4 col-sm-4 col-xs-4"><strong>Date create: </strong> ${requestScope.DATECREATE}</div>
            <div class="col-md-3 col-sm-3 col-xs-3"><strong>Status: </strong> ${requestScope.EDITPRES.pres.statusPres}</div>
        </div>
        <div class="row" style="padding-top: 20px;">
            <div class="col-md-12 col-sm-12 col-xs-12"><strong>Delitescence:</strong> ${sessionScope.MEDID.symptoms}</div>
        </div>
        <div class="row" style="padding-top: 20px; padding-bottom: 10px;">
            <div class="col-md-12 col-sm-12 col-xs-12"><strong>Dignose:</strong> <span style="overflow: auto; max-width: 500px;">${requestScope.EDITPRES.pres.dignose}</span></div>
        </div>
        <!--IMPORTANT!!! EDIT ZONE START FROM HERE - Do not change anything else-->
        <!--I am not responsible any wrong if you change anything without permission-->
        <!--FOR THE ENGLISH TRANSLATE - GOOGLE PLZ ^_^-->
        <!--table here-->
        <c:forEach items="${requestScope.EDITPRES.lstDetail}" var="dto" varStatus="stt">
            <div class="row" style="padding-top: 20px;">
                <div class="col-md-12 col-sm-12 col-xs-12"><strong><i>${stt.count} &nbsp;&nbsp; ${dto.medicineID.itemName}, ${dto.medicineID.content}, ${dto.medicineID.nation}</i></strong></div>
            </div>
            <div class="row" style="padding-top: 10px; margin-left: 10px;">
                <div class="col-md-3 col-sm-3 col-xs-3">Morning: ${dto.morning}</div>
                <div class="col-md-3 col-sm-3 col-xs-3">Afternoon: ${dto.afternoon}</div>
                <div class="col-md-3 col-sm-3 col-xs-3">Night: ${dto.night}</div>
                <div class="col-md-3 col-sm-3 col-xs-3">Quantity: ${dto.quantity}</div>
            </div>
        </c:forEach>
        <div class="row" style="padding-top: 20px; padding-bottom: 10px;">
            <hr/>
            <form action="AdminListPrescriptionController" method="post">
                <input type="hidden" name="txtPresID" value="${requestScope.EDITPRES.pres.prescriptionID}"/>
                <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                    <button name="action" value="EditPres" class="btn btn-info" style="width: 100px;"><i class="fa fa-pencil"></i> Edit</button>
                    <c:if test="${requestScope.EDITPRES.pres.statusPres ne 'Gived medicine'}">
                        <input type="hidden" name="txtMedRCID" value="${sessionScope.MEDID.medRecordID}"/>
                        <button name="action" value="DeletePres" class="btn btn-danger" style="width: 100px;"><i class="fa fa-times"></i> Delete</button>
                    </c:if>
                </div>
            </form>
        </div>
    </div>
    <!-- end table here-->
    <!--IMPORTANT!!! EDIT ZONE END FROM HERE - Do not change anything else-->
</div>
<div class="col-md-1 col-sm-1"></div>
</div>	

<%@include file="footer.jsp" %>