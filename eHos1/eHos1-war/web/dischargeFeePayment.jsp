<%@include file="header.jsp" %>
<div class="col-sm-9 col-md-9 text-left">
    <h1 class="page-header">
        Bed Assignment <small>${sessionScope.empInfo.faculty} - ${sessionScope.empInfo.position}</small>
    </h1>
</div>
</div>
<div class="row-fluid text-left">
    <div class="col-sm-8 col-md-8">
        <!--Load button-->
        <form style="padding:20px" id="loadForm" 
              action="DischargeFeeMainProcess" method="post">
            <button class="btn btn-default" type="submit" form="loadForm"
                    name="btnSubmit" value="Load discharge"/>
            Load<i class="fa fa-download" aria-hidden="true"></i>
            </button>			
        </form>
        <c:if test="${requestScope.listDischargePatient eq null or empty requestScope.listDischargePatient}">
            <div class="alert alert-warning">
                <strong>No patient is waiting for discharge</strong> 
            </div>
        </c:if>
        <!--Display list patient's waiting-->
        <c:if test="${requestScope.listDischargePatient ne null}">
            <c:if test="${not empty requestScope.listDischargePatient}">
                <div class="table-responsive">
                    <h3>List patient is waiting for discharge</h3>
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                                <th>Ord</th>
                                <th>Medical Record ID</th>
                                <th>Name</th>
                                <th>Date in</th>
                                <th>Bed</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${requestScope.listDischargePatient}"
                                       var="mr" varStatus="i">
                                <tr>
                                    <td>${i.count}</td>
                                    <td>${mr.medRecordID}</td>
                                    <td>${mr.patInfo.patName}</td>
                                    <td>${mr.dateInHospital}</td>
                                    <td>${mr.bedInfo.bedName}</td>
                                    <td>
                                        <form action="DischargeFeeMainProcess" method="post">
                                            <button type="submit" class="btn btn-default"
                                                    name="btnSubmit" value="view fee">
                                                View Fee <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                                            </button>
                                            <input type="hidden" name="passID" value="${mr.medRecordID}" />
                                        </form>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </c:if>
        </c:if>
        <!-- end table here-->
        <!--IMPORTANT!!! EDIT ZONE END FROM HERE - Do not change anything else-->
    </div>
</div>	
<%@include file="footer.jsp" %>