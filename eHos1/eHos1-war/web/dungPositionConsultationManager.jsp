<%-- 
    Document   : dungPositionConsultationManager
    Created on : Jan 12, 2017, 6:27:29 PM
    Author     : Sony
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Surgery Admin Page</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap Core CSS -->
        <link rel="stylesheet" href="css/w3.css">
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- jQuery -->
        <script src="js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>
        <!--pagination-->
        <script type="text/javascript" src="js/jPager/js.js"></script>
        <script type="text/javascript" src="js/jPager/jPages.min.js"></script>
        <link rel="stylesheet" href="css/jPager/jPages.css" type="text/css"/>

        <script>
//            run when document is ready
            $(function() {
//                initiate plugin
                $("div.holder").jPages({
                    containerID: "itemContainer",
                    perPage: 5
                });
            });
        </script>
        
        <style>
            /* Set black background color, white text and some padding */
            footer {
                background-color: #555;
                color: white;
                padding: 8px;
            }
            html {
                position: relative;
                min-height: 100%;
            }
            body {
                /* Margin bottom by footer height */
                margin-bottom: 20px;
            }
            footer {
                position: absolute;
                text-align: center;
                bottom: 0;
                width: 100%;
                /* Set the fixed height of the footer here */
                height: 20px;
                background-color: #f5f5f5;
            }
            .glyphicon { margin-right:10px; }
            .panel-body { padding:0px; }
            .panel-body table tr td { padding-left: 15px }
            .panel-body .table {margin-bottom: 0px; }

            header {
                background: url(images/banner.jpg) no-repeat;
                background-size: cover;
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;	
                height:170px;
            }
            .mr10 {
                margin-right: 10px;
            }
            .mr20 {
                margin-right: 20px;
            }
            .pd20 {
                padding-top: 20px;
            }
        </style>
    </head>

    <body>

        
        <%@include file="dungHeaderAdmin.jsp" %>

        <%@include file="dungAsideRightAdmin.jsp" %>

        <div class="row-fluid text-left">
            <div class="col-md-1 col-sm-1"></div>
            <div class="col-sm-6 col-md-6">
                <c:if test="${requestScope.msgError ne null}">
                    <label style="margin-left: 10px" class="w3-text-red">${requestScope.msgError}</label><br/>
                </c:if>
                <form style="padding:10px" action="dungSearchPositionConsultationServlet" method="post">
                    <!--IMPORTANT!!! EDIT ZONE START FROM HERE - Do not change anything else-->
                    <!--I am not responsible any wrong if you change anything without permission-->
                    <!--FOR THE ENGLISH TRANSLATE - GOOGLE PLZ ^_^-->
                    <!--copy form input here...-->
                    <!--input 1-->
                    <!--end form input...-->
                    <div class="form-group input-group">
                        <input type="text" placeholder="Enter Position Consultation" class="form-control" name="txtSearch">
                        <span class="input-group-btn"><button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button></span>
                    </div>
                </form>
                <a style="margin-left: 10px; margin-bottom: 15px;" href="dungAddPositionConsultationServlet">Add Position Consultation</a>
                <!--IMPORTANT!!! EDIT ZONE START FROM HERE - Do not change anything else-->
                <!--I am not responsible any wrong if you change anything without permission-->
                <!--FOR THE ENGLISH TRANSLATE - GOOGLE PLZ ^_^-->
                <!--table here-->
                <c:if test="${not empty requestScope.listPostionConsultation}">
                <div style="margin-top: 20px;margin-left: 10px" class="table-responsive">
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Position Consultation Name</th>
                                <th>Status Position</th>
                            </tr>
                        </thead>
                        <tbody id="itemContainer">
                        <c:forEach items="${requestScope.listPostionConsultation}" var="dto" varStatus="stt">
                            <tr>
                                <td>${stt.count}</td>
                                <td>
                                    <a href="dungEditPositionConsultationServlet?txtPositionID=${dto.positionID}">${dto.positionName}</a>
                                </td>
                                <td>
                                    <c:if test="${dto.statusPosition eq 'true'}">
                                        Enable
                                    </c:if>
                                    <c:if test="${dto.statusPosition eq 'false'}">
                                        Disable
                                    </c:if>
                                </td>                                
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
                <!--pagination-->
                    <div class="holder" align="right"></div>
                <!-- end table here-->
                </c:if>
                <!--IMPORTANT!!! EDIT ZONE END FROM HERE - Do not change anything else-->
            </div>
            <div class="col-md-1 col-sm-1"></div>
        </div>	

        <%@include file="dungFooter.jsp" %>
        <!--css for footer-->

    </body>
</html>
<!-- css for side menu -->
