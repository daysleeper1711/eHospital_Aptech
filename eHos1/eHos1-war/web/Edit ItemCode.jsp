<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="loc" %>

<!DOCTYPE html>

<html lang="en">

    <head>
        <title>Edit ItemCode</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- jQuery -->
        <script language="javascript" src="js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>
        <style>    
            /* Set black background color, white text and some padding */
            footer {
                background-color: #555;
                color: white;
                padding: 8px;
            }

            .loccolum{
                display: table-cell;
                padding: 20px;
                width: 40%;
            }
            .loccenter{
                display: table;
                margin: auto;
            }

        </style>
    </head>
    <body>
        <%@include file="locGUI/locheader.jsp" %>

        <div class="row-fluid text-center">
            <%@include file="locGUI/locmenu.jsp" %>
            <div class="col-sm-9 col-md-9">
                <h1 class="page-header">
                    <small>Edit ItemCode</small> <!--Change here for title of the page-->
                </h1>
            </div>

        </div>
        <div class="row-fluid text-left">
            <div class="col-md-1 col-sm-1"></div>
            <div class="col-sm-6 col-md-6">
                <form style="padding:20px" action= "EditItemCodeServlet" method="post">
                    <input type="hidden" name="txtUserID" value="${sessionScope.empInfo.employeeID}"/>
                    <input type="hidden" name="txtUserName" value="${sessionScope.empInfo.empName}"/>
                    <!--IMPORTANT!!! EDIT ZONE START FROM HERE - Do not change anything else-->
                    <!--I am not responsible any wrong if you change anything without permission-->
                    <!--FOR THE ENGLISH TRANSLATE - GOOGLE PLZ ^_^-->

                    <!--copy form input here...-->
                    <!--input 1-->
                    <div class="loccenter" >
                        <!--Left-->
                        <div class="loccolum">
                            <div class="form-group">
                                <label>ItemCode</label>
                                <input class="form-control" name="txtItemCode" value="${requestScope.ItemCode}" readonly="true"/>

                            </div>
                            <div class="form-group">
                                <label>ItemName</label>
                                <input class="form-control" name="txtItemName" id="txtItemName" value="${requestScope.ItemName}">
                                <p style="color: red" id="errortxtItemName"></p>
                            </div>

                            <div class="form-group input-group">
                                <label>Content</label>
                                <input class="form-control" name="txtContent" id="txtContent" value="${requestScope.Content}">
                                <p style="color: red" id="errortxtContent"></p>
                            </div>

                            <div class="form-group">
                                <label>Type</label>
                                <select class="form-control" name="selectType">
                                    <option <loc:if test="${requestScope.TypeITcode eq 'D'}">selected="true"</loc:if> >Drugs</option>
                                    <option <loc:if test="${requestScope.TypeITcode eq 'S'}">selected="true"</loc:if> >Supplies</option>
                                    </select>
                                </div>



                                <div class="form-group">
                                    <label>How To Use</label>
                                    <select class="form-control" name="selectHowToUse">
                                        <option <loc:if test="${requestScope.HowToUse eq 'Tablet'}">selected="true"</loc:if> >Tablet</option> <!--Uong-->
                                    <option <loc:if test="${requestScope.HowToUse eq 'Injectable'}">selected="true"</loc:if> >Injectable</option><!--Tiem-->
                                    <option <loc:if test="${requestScope.HowToUse eq 'Suppository'}">selected="true"</loc:if> >Suppository</option><!--Dat-->
                                    <option <loc:if test="${requestScope.HowToUse eq 'Powder Injectable'}">selected="true"</loc:if> >Powder Injectable</option> <!--bot tiem-->
                                    <option <loc:if test="${requestScope.HowToUse eq 'Topical Cream'}">selected="true"</loc:if> >Topical Cream</option><!--thoa ngoai-->
                                    <option <loc:if test="${requestScope.HowToUse eq 'Perfusion'}">selected="true"</loc:if> >Perfusion</option><!--Truyen-->
                                    <option <loc:if test="${requestScope.HowToUse eq 'Sublingual'}">selected="true"</loc:if> >Sublingual</option><!--Ngam-->
                                    <option <loc:if test="${requestScope.HowToUse eq 'Modified Release Tablet'}">selected="true"</loc:if> >Modified Release Tablet</option><!--Vien tac dung cham-->
                                    </select>

                                </div>

                            </div>

                            <!--Right-->
                            <div class="loccolum">

                                <div class="form-group">
                                    <label>GenericDrug</label>
                                    <input class="form-control" name="txtGenericDrug" id="txtGenericDrug" value="${requestScope.GenericDrug}">
                                <p style="color: red" id="errortxtGenericDrug"></p>
                            </div>

                            <div class="form-group">
                                <label>Unit</label>
                                <select class="form-control" name="txtUnit" id="txtUnit">
                                    <option <loc:if test="${requestScope.Unit eq 'Phial'}">selected="true"</loc:if> >Phial</option><!--lo-ong-->
                                    <option <loc:if test="${requestScope.Unit eq 'Medicine bag'}">selected="true"</loc:if> >Medicine bag</option><!--Tui-->
                                    <option <loc:if test="${requestScope.Unit eq 'Pill'}">selected="true"</loc:if> >Pill</option><!--Vien-->
                                    <option <loc:if test="${requestScope.Unit eq 'Bottle'}">selected="true"</loc:if> >Bottle</option><!--Chai-->
                                    </select>

                                </div>

                                <div class="form-group">
                                    <label>Price</label>
                                    <input class="form-control" type="number" name="txtPrice" id="txtPrice" value="${requestScope.Price}">
                                <p style="color: red" id="errortxtPrice"></p>   
                            </div>



                            <div class="form-group">
                                <label>Nation</label>
                                <select name="country" class="form-control"> 
                                    <optgroup label="Asia" value="Asia">
                                        <option <loc:if test="${requestScope.Nation eq 'VietNam'}">selected="true"</loc:if> >VietNam</option>
                                        <option <loc:if test="${requestScope.Nation eq 'ThaiLan'}">selected="true"</loc:if> >ThaiLan</option>
                                        <option <loc:if test="${requestScope.Nation eq 'Singapore'}">selected="true"</loc:if> >Singapore</option>
                                        </optgroup>
                                        <optgroup  label="America" value="America">
                                            <option <loc:if test="${requestScope.Nation eq 'USA'}">selected="true"</loc:if> >USA</option> 
                                        <option <loc:if test="${requestScope.Nation eq 'Canada'}">selected="true"</loc:if> >Canada</option>
                                        <option <loc:if test="${requestScope.Nation eq 'Mexico'}">selected="true"</loc:if> >Mexico</option>
                                        </optgroup>
                                    </select>
                                </div>

                                <div class="form-group" >
                                    <label>Status</label>
                                    <select class="form-control" name="selectStatus">
                                        <option <loc:if test="${requestScope.UStatus eq 'A'}">selected="true"</loc:if> >Action</option>
                                    <option <loc:if test="${requestScope.UStatus eq 'I'}">selected="true"</loc:if> >Inaction</option>                                   
                                    </select>
                                </div>


                            </div>

                        </div>

                        <div style="text-align: center; padding:0px 0px 100px 0px">
                            <button type="submit" class="btn btn-default" name="action" id="btnUpdate" value="Update">Update</button> 
                        </div>

                </div>

            </form>

            <script language="javascript">
                $(document).ready(function()
                {
                    $('#btnUpdate').click(function()
                    {
                        $('#errortxtItemName').text("");
                        $('#errortxtGenericDrug').text("");
                        $('#errortxtContent').text("");
                        $('#errortxtPrice').text("");

                        if ($('#txtItemName').val() === "")
                        {
                            $('#errortxtItemName').text("ItemName not blank");
                            return false;
                        } else if ($('#txtGenericDrug').val() === "")
                        {
                            $('#errortxtGenericDrug').text("GenericDrug not blank");
                            return false;
                        }
                        else if ($('#txtContent').val() === "")
                        {
                            $('#errortxtContent').text("Content not blank");
                            return false;
                        }
                        else if ($('#txtPrice').val() === "")
                        {
                            $('#errortxtPrice').text("Price not blank");
                            return false;
                        }

                        var price = parseInt($('#txtPrice').val());
                        if (price < 0)
                        {
                            $('#errortxtPrice').text("Price is number bigger than 0");
                            return false;
                        }
                    });
                });
            </script>

        </div>
        <div class="col-md-1 col-sm-1"></div>
    </div>	

<%@include file="locGUI/locfooter.jsp" %>
</body>
</html>
