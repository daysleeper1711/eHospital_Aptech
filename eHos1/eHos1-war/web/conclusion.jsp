<%@include file="header.jsp" %>
<div class="col-sm-9 col-md-9 text-left">
    <h1 class="page-header">
        Treatment Conclusion <small>${sessionScope.empInfo.faculty} - ${sessionScope.empInfo.position}</small>
    </h1>
</div>
</div>
<div class="row-fluid text-left">
    <div class="col-sm-8 col-md-8">
        <!--Load button-->
        <form style="padding:20px" action="ConclusionMainProcess" method="post">
            <button class="btn btn-default" type="submit" name="btnSubmit" value="search"/>
            Load<i class="fa fa-download" aria-hidden="true"></i>
            </button>			
        </form>
        <!--DISPLAY RESULT-->
        <c:if test="${requestScope.listPatientInTreatment ne null}">
            <h3>List patient in faculty</h3>
            <c:if test="${empty requestScope.listPatientInTreatment}">
                <div class="alert alert-warning">
                    <strong>Attention!</strong> List is empty...
                </div>
            </c:if>
            <c:if test="${not empty requestScope.listPatientInTreatment}">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                                <th>Ord</th>
                                <th>Medical Record ID</th>
                                <th>Name</th>
                                <th>Date in</th>
                                <th>Bed</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody id="itemContainer">
                            <c:forEach items="${requestScope.listPatientInTreatment}"
                                       var="mr" varStatus="i">
                                <tr>
                                    <td>${i.count}</td>
                                    <td>${mr.medRecordID}</td>
                                    <td>${mr.patInfo.patName}</td>
                                    <td>${mr.dateInHospital}</td>
                                    <td>${mr.bedInfo.bedName}</td>
                                    <td>
                                        <form action="ConclusionMainProcess" method="post">
                                            <button type="submit" class="btn btn-default"
                                                    name="btnSubmit" value="conclusion input">
                                                Conclusion <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                                            </button>
                                            <input type="hidden" name="passID" value="${mr.medRecordID}" />
                                        </form>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
                <!--PAGINATION-->
                <div class="holder"></div>
            </c:if>
        </c:if>
        <!-- end table here-->
        <!--IMPORTANT!!! EDIT ZONE END FROM HERE - Do not change anything else-->
    </div>
</div>	
<%@include file="footer.jsp" %>
