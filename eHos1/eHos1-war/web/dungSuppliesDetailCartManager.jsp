<%-- 
    Document   : dungSuppliesDetailCartManager
    Created on : Feb 4, 2017, 2:09:55 AM
    Author     : Sony
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Supplies Detail Page</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/select2.min.css" rel="stylesheet" type="text/css">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- jQuery -->
        <script src="js/jquery.js"></script>
        <script src="js/select2.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <link rel="stylesheet" href="css/w3.css">
        <script src="js/bootstrap.min.js"></script>
        <!--pagination-->
        <script type="text/javascript" src="js/jPager/js.js"></script>
        <script type="text/javascript" src="js/jPager/jPages.min.js"></script>
        <link rel="stylesheet" href="css/jPager/jPages.css" type="text/css"/>

        <script>
//            run when document is ready
            $(function() {
//                initiate plugin
                $("div.holder").jPages({
                    containerID: "itemContainer",
                    perPage: 5
                });
            });
        </script>
        <script type="text/javascript">
            $(document).ready(function() {
                $(".js-example-basic-single").select2();
            });
        </script>
        <style>
            /* Set black background color, white text and some padding */
            footer {
                background-color: #555;
                color: white;
                padding: 8px;
            }
            html {
                position: relative;
                min-height: 100%;
            }
            body {
                /* Margin bottom by footer height */
                margin-bottom: 20px;
            }
            footer {
                position: absolute;
                text-align: center;
                bottom: 0;
                width: 100%;
                /* Set the fixed height of the footer here */
                height: 20px;
                background-color: #f5f5f5;
            }
            .glyphicon { margin-right:10px; }
            .panel-body { padding:0px; }
            .panel-body table tr td { padding-left: 15px }
            .panel-body .table {margin-bottom: 0px; }

            header {
                background: url(images/banner.jpg) no-repeat;
                background-size: cover;
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;	
                height:170px;
            }
            .mr10 {
                margin-right: 10px;
            }
            .mr20 {
                margin-right: 20px;
            }
            .pd20 {
                padding-top: 20px;
            }
        </style>
    </head>

    <body>

        <%@include file="dungHeaderAdmin.jsp" %>

        <%@include file="dungAsideRightDoctor.jsp" %>

        <div class="row-fluid text-left">
            <div class="col-md-1 col-sm-1"></div>
            <div class="col-sm-9 col-md-9">
                <c:if test="${requestScope.msgError ne null}">
                    <label style="margin-left: 10px" class="w3-text-red">${requestScope.msgError}</label><br/>
                </c:if>
                <label style="margin-left: 10px;">Surgery Service Name : ${requestScope.dtoSuggest.surgeryServiceID.surgeryServiceName}</label><br/>
                <label style="margin-left: 10px;">Medical Record : ${sessionScope.sInfo.medRecordID}</label><br/>
                <label style="margin-left: 10px;">Patient Name : ${sessionScope.sInfo.patInfo.patName}</label><br/>
                <label style="margin-left: 10px;">Sex : ${sessionScope.sInfo.patInfo.gender}</label><br/>
                <label style="margin-left: 10px;">Faculty Name : ${sessionScope.sInfo.currentFaculty}</label><br/>
                <label style="margin-left: 10px;">Bed Name : ${sessionScope.sInfo.bedInfo.bedName}</label><br/>

                <form style="padding:10px" action="InsertSuppliesDetailCartServlet" method="post">
                    <div class="form-group">
                        <input class="form-control" name="txtSuggestID" type="hidden" value="${requestScope.dtoSuggest.suggestSurgeryID}">
                    </div>
                    <div class="form-group">
                        <label>Supplies Name</label>
                        <select class="js-example-basic-single form-control" name="cbSuppliesName">
                            <c:forEach items="${requestScope.listSuppliesName}" var = "role">
                                <option value="${role.itemCode}">${role.itemName} (${role.nation} - ${role.unit} - ${role.content})</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Quantity</label>
                        <input class="form-control" name="txtQuantity">
                    </div>
                    <div style="text-align: center">
                        <button style="margin: 10px" type="submit" class="btn btn-primary" name="action" value="Insert">Insert</button>
                        <button style="margin: 10px" type="submit" class="btn btn-primary" name="action" value="Cancel">Back</button>
                    </div>
                </form>
                <!--IMPORTANT!!! EDIT ZONE START FROM HERE - Do not change anything else-->
                <!--I am not responsible any wrong if you change anything without permission-->
                <!--FOR THE ENGLISH TRANSLATE - GOOGLE PLZ ^_^-->
                <!--table here-->
                <c:if test="${not empty requestScope.listSuppliesDetail}">
                    <div style="margin-top: 20px;margin-left: 10px" class="table-responsive">
                        <table class="table table-bordered table-hover table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Supplies Name</th>
                                    <th>Unit</th>
                                    <th>Quantity</th>
                                    <th>Update</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <tbody id="itemContainer">
                                <c:forEach items="${requestScope.listSuppliesDetail}" var="dto" varStatus="stt">
                                    <tr>
                                <form style="padding:10px" action="dungChangeSuppliesDetailCartServlet" method="post">
                                    <div class="form-group">
                                        <input class="form-control" name="txtSuppliesDetailCartId" type="hidden" value="${dto.suppliesDetailID}">
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" name="txtSuggestID" type="hidden" value="${requestScope.dtoSuggest.suggestSurgeryID}">
                                    </div>
                                    <td>
                                        <div class="form-group">
                                            ${stt.count}
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            ${dto.suppliesID.itemName}
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            ${dto.unit}
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <input class="form-control" name="txtQuan" value="${dto.quantity}">
                                        </div>
                                    </td>
                                    <td>
                                        <div style="text-align: center" class="form-group">
                                            <Button type="submit" name="action" class="btn btn-primary" value="Edit">Edit Quantity</Button>
                                        </div>
                                    </td>
                                    <td>
                                        <div style="text-align: center" class="form-group">
                                            <Button type="submit" name="action" class="btn btn-primary" value="Delete">Delete</Button>
                                        </div>
                                    </td>
                                </form>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                    <!--pagination-->
                    <div class="holder" align="right"></div>
                </c:if>

                <!-- end table here-->
                <!--IMPORTANT!!! EDIT ZONE END FROM HERE - Do not change anything else-->
            </div>
            <div class="col-md-1 col-sm-1"></div>
        </div>
        <%@include file="dungFooter.jsp" %>
        <!--css for footer-->

    </body>
</html>
<!-- css for side menu -->
