<%-- 
    Document   : dungReportSuggestSurgery
    Created on : Feb 4, 2017, 7:17:41 PM
    Author     : Sony
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Suggest Surgery Report</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!-- jQuery -->
        <script src="js/jquery.js"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>
        <style>    
            /* Set black background color, white text and some padding */
            footer {
                background-color: #555;
                color: white;
                padding: 8px;
            }
            .div-table {
                display:table
            }
            .div-colunm {
                display: table-cell;
                width: 20%;
                padding: 0px 8px 0px 8px;
            }
        </style>
    </head>
    <body>
        <header>
            <div class="container-fluid" style="z-index:4;float: left">
                <div class="col-sm-12 col-md-12">
                    <img src="images/logo.jpg" class="img-responsive" style="opacity:0.6;width:120px; height:120px"/>
                </div>
            </div>
            <div class="col-sm-8 col-md-8" style="margin-right: 15px">
                <h3>eHospital</h3>
                <h4>Faculty. ${sessionScope.sInfo.currentFaculty}</h4>
                <h4>Doctor. ${sessionScope.empInfo.empName}</h4>
            </div>
        </header>
        <div class="row-fluid text-center">
            <div class="col-sm-12 col-md-12">
                <h2 class="page-header">
                    Suggest Surgery <!--Change here for title of the page-->
                </h2>
            </div>
        </div>
        <div class="row-fluid text-left">
            <div class="col-sm-2 col-md-2"></div>
            <div class="col-sm-8 col-md-8" style="">
                <!--information of patient-->
                <div class="col-sm-12 col-md-12" >
                    <div class = "div-table form-group">
                        <div class= "div-colunm div-col1">
                            <div class="form-group">			
                                <label>Patient Name. ${sessionScope.sInfo.patInfo.patName}</label>
                            </div>
                            <div class="form-group">			
                                <label>Gender. ${sessionScope.sInfo.patInfo.gender}</label>
                            </div>
                            <div class="form-group">			
                                <label>Address. ${sessionScope.sInfo.patInfo.address}</label>
                            </div>
                            <div class="form-group">			
                                <label>Date In. ${sessionScope.sInfo.dateInHospital}</label>
                            </div>
                        </div>
                        <div class= "div-colunm div-col2">
                            <div class="form-group">			
                                <label>Suggest No. ${requestScope.txtSuggestID}</label>
                            </div>
                            <div class="form-group">			
                                <label>Medical Record No. ${sessionScope.sInfo.medRecordID}</label>
                            </div>
                            <div class="form-group">			
                                <label>Faculty Name. ${sessionScope.sInfo.currentFaculty}</label>
                            </div>
                            <div class="form-group">			
                                <label>Bed Name. ${sessionScope.sInfo.bedInfo.bedName}</label>
                            </div>
                        </div>    
                    </div>
                </div>
                <div class="col-sm-12 col-md-12" >
                    <div class = "div-table form-group">
                        <div class= "div-colunm div-col1">
                            
                            <div class="form-group">			
                                <label>Surgery Service Name. ${requestScope.rSS.surgeryServiceID.surgeryServiceName}</label>
                            </div>
                            <div class="form-group">			
                                <label>Suggest Description. ${requestScope.rSS.suggestDescription}</label>
                            </div>
                            <div class="form-group">			
                                <label>Doctor Name. ${requestScope.rSS.doctorID.fullname}</label>
                            </div>
                        </div>
                    </div>
                </div>
                <!--table show result of test-->
                <div class = "div-table form-group" style="margin-left: 70%">
                    <div class="form-group"  style="margin-bottom: 100px; ">
                        <label><fmt:formatDate value="${requestScope.rSS.createDate}" type="date" dateStyle="long" timeStyle="long"/></label><br/>
                        <label style="margin-left: 25px">Doctor</label>
                    </div>
                    <div class="form-group">
                         <label>${requestScope.rSS.doctorID.fullname}</label>
                    </div>
                </div>
            </div>	
            <!--css for footer-->
            <style>
                html {
                    position: relative;
                    min-height: 100%;
                }
                body {
                    /* Margin bottom by footer height */
                    margin-bottom: 20px;
                }
                footer {
                    position: absolute;
                    bottom: 0;
                    width: 100%;
                    /* Set the fixed height of the footer here */
                    height: 20px;
                    background-color: #f5f5f5;
                }
            </style>
            <!-- css header
            ================================================== -->
            <style>
                header {
                    background: url(images/banner.jpg) no-repeat;
                    background-size: cover;
                    -webkit-background-size: cover;
                    -moz-background-size: cover;
                    -o-background-size: cover;	
                    height:130px;
                }
                .mr10 {
                    margin-right: 10px;
                }
                .mr20 {
                    margin-right: 20px;
                }
                .pd20 {
                    padding-top: 20px;
                }
            </style>
            <!-- css header
            ================================================== -->
    </body>
</html> 
