<%-- 
    Document   : test
    Created on : Jan 1, 2017, 6:22:36 PM
    Author     : DaySLeePer
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <c:if test="${sessionScope.empInfo ne null}">
            <c:out value="${sessionScope.empInfo.faculty}"></c:out>
        </c:if>
        <c:if test="${requestScope.empInfo eq null}">
            <c:out value="${requestScope.empInfo}"></c:out>
        </c:if>
        <a href="TestGeneratePatID" >Generate patient ID</a>
        <c:if test="${requestScope.patID ne null}">
            <p>Patient ID: <c:out value="${requestScope.patID}"></c:out></p>
        </c:if>
        <c:if test="${requestScope.medID ne null}">
            <p>Medical Record ID: <c:out value="${requestScope.medID}"></c:out></p>
        </c:if>
        <c:if test="${requestScope.patInfo ne null}">
            <c:out value="${requestScope.patInfo.patID}"></c:out>
            <c:out value="${requestScope.patInfo.dob}"></c:out>
            <c:out value="${requestScope.patInfo.patName}"></c:out>
            <c:out value="${requestScope.patInfo.address}"></c:out>
            <c:out value="${requestScope.patInfo.phone}"></c:out>
            <c:out value="${requestScope.patInfo.iDCard}"></c:out>
            <c:out value="${requestScope.patInfo.regDate}"></c:out>
            <c:out value="${requestScope.patInfo.regByWhom}"></c:out>
        </c:if>
        <c:forEach items="${requestScope.resultList1}" var="mrInfo">
            <ul>
                <li>${mrInfo.medRecordID}</li>
            </ul>
        </c:forEach>
        <p>Faculty to go: <c:out value="${requestScope.facultyToGo}"></c:out></p>
        <p>Current faculty: <c:out value="${requestScope.currentFaculty}"></c:out></p>
        <!--Test max date-->
        <c:forEach items="${requestScope.listMaxDate}" var="maxDate">
            <ul>
                
            </ul>
        </c:forEach>
    </body>
</html>
