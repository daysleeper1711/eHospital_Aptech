<%-- 
    Document   : ChooseGivedReport
    Created on : Jan 12, 2017, 8:44:59 PM
    Author     : Nguyet Phuong
--%>
<%@include file="header.jsp" %>
<div class="col-sm-9 col-md-9">
    <h1>
        Choose Gived Medicine <!--Change here for title of the page-->
    </h1>
    <h4><i>(${requestScope.DATE})</i></h4>
    <hr/>
</div>
</div>
<div class="row-fluid text-left">
    <div class="col-md-1 col-sm-1"></div>
    <div class="col-sm-6 col-md-6">
        <div class="row">
            <div class="col-md-7"><strong>Medical record ID :</strong> ${sessionScope.MEDID.medRecordID}</div>
            <div class="col-md-5"><strong>Patient name:</strong> ${sessionScope.MEDID.patInfo.patName}</div>
        </div>
        <div class="row" style="padding-top: 20px;">
            <div class="col-md-4"><strong>Birthday:</strong> ${sessionScope.MEDID.patInfo.dob}</div>
            <div class="col-md-4"><strong>Sex:</strong> ${sessionScope.MEDID.patInfo.gender}</div>
            <div class="col-md-4 text-center"><strong>Doctor :</strong> ${sessionScope.empInfo.empName}</div>
        </div>
        <div class="row" style="padding-top: 20px;">
            <div class="col-md-3"><strong>Date in:</strong> ${sessionScope.MEDID.dateInHospital}</div>
            <div class="col-md-4"><strong>Faculty:</strong> ${sessionScope.MEDID.currentFaculty}</div>
            <div class="col-md-2"><strong>Bed:</strong> ${sessionScope.MEDID.currentBed}</div>
        </div>
        <div class="row" style="padding-top: 20px;">
            <div class="col-md-12"><strong>Delitescence:</strong> ${sessionScope.MEDID.symptoms}</div>
        </div>
        <form method="post" action="ReportGivedMedicineController">
            <div class="row" style="padding-top: 20px;">
                <div class="col-md-4">
                    <strong>Choose Gived medicine</strong>
                </div>
                <div class="col-md-8">
                    <select name="lstGivedMed">
                        <c:forEach items="${requestScope.LISTCH}" var="dto">
                            <option value="${dto.statusGRP}/${dto.presID}/${dto.suppID}">${dto.statusGRP}</option>
                        </c:forEach>
                    </select>

                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    <div style="padding: 20px 0px;"><input type="submit" value="Ok" class="btn btn-primary"/></div>
                </div>
            </div>
        </form>
        <!-- end table here-->
        <!--IMPORTANT!!! EDIT ZONE END FROM HERE - Do not change anything else-->
    </div>
    <div class="col-md-1 col-sm-1"></div>
</div>	
<%@include file="footer.jsp" %>