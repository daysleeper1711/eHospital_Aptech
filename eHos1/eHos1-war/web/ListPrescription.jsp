<%-- 
    Document   : ListPrescription
    Created on : Jan 7, 2017, 3:54:37 AM
    Author     : Nguyet Phuong
--%>
<%@include file="header.jsp" %>
<div class="col-sm-9 col-md-9">
    <h1 class="page-header">
        List prescription <!--Change here for title of the page-->
    </h1>
</div>
</div>
<div class="row-fluid text-left">
    <div class="col-md-1 col-sm-1"></div>
    <div class="col-sm-6 col-md-6">
        <div class="row">
            <div class="col-md-5"><strong>Medical record :</strong> ${sessionScope.MEDID.medRecordID}</div>
            <div class="col-md-7 text-right">
                <a href="PhuongControllerAdd?action=AddNewPrescription" class="btn btn-default"><i class="fa fa-plus" style="color: green;"></i> Add new prescription</a>
            </div>
        </div>
        <div class="row" style="padding-top: 20px;">
            <div class="col-md-6"><strong>Patient name:</strong> ${sessionScope.MEDID.patInfo.patName}</div>
            <div class="col-md-4"><strong>Birthday:</strong> ${sessionScope.MEDID.patInfo.dob}</div>
            <div class="col-md-2"><strong>Sex:</strong> ${sessionScope.MEDID.patInfo.gender}</div>
        </div>
        <div class="row" style="padding-top: 20px;">
            <div class="col-md-3"><strong>Date in:</strong> ${sessionScope.MEDID.dateInHospital}</div>
            <div class="col-md-4"><strong>Faculty:</strong> ${sessionScope.MEDID.currentFaculty}</div>
            <div class="col-md-2"><strong>Bed:</strong> ${sessionScope.MEDID.currentBed}</div>
        </div>
        <div class="row" style="padding-top: 20px;">
            <div class="col-md-5"><strong>Create for date:</strong> ${requestScope.DATE}</div>
            <div class="col-md-7"><strong>Delitescence:</strong> ${sessionScope.MEDID.symptoms}</div>
        </div>
        <!--IMPORTANT!!! EDIT ZONE START FROM HERE - Do not change anything else-->
        <!--I am not responsible any wrong if you change anything without permission-->
        <!--FOR THE ENGLISH TRANSLATE - GOOGLE PLZ ^_^-->
        <!--table here-->
        <div class="table-responsive" style="padding-top: 20px;">
            <table class="table table-bordered table-hover table-striped">
                <thead>
                    <tr>
                        <th>Date create</th>
                        <th>Employee Name</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody id="itemContainer">
                    <c:forEach items="${requestScope.LISTPRESCRIPTION}" var="lst">
                        <tr>
                            <td>${lst.date}</td>
                            <td>${lst.employName}</td>
                            <td>
                                <form action="ListPrescriptionController" method="post">
                                    <input type="hidden" name="txtPresID" value="${lst.presID}"/>
                                    <input type="hidden" name="txtCreateFDate" value="${requestScope.DATE}"/>
                                    <c:if test="${lst.status ne 'Gived medicine'}">
                                        <button name="action" value="EditPres"><i class="fa fa-info-circle" style="color: green;"></i> Edit</button>
                                        <button name="action" value="DeletePres"><i class="fa fa-times" style="color: red;"></i> Delete</button>
                                    </c:if>
                                    <button name="action" value="ReportPres"><i class="fa fa-print" style="color: blue;"></i> Print</button>
                                </form>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
        <div class="holder"></div>
        <!-- end table here-->
        <!--IMPORTANT!!! EDIT ZONE END FROM HERE - Do not change anything else-->
    </div>
    <div class="col-md-1 col-sm-1"></div>
</div>
<%@include file="footer.jsp" %>