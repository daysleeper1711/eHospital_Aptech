<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>add test result</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- jQuery -->
        <script src="js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>
        <style>
            .div-table {
                display:table
            }
            .div-colunm {
                display: table-cell;
                width: 25%;
                padding: 0px 8px 0px 8px;
            }
        </style>
    </head>
    <body>
        <%@include file="../tuyetTemplate/header.jsp" %>
        <%@include file="../tuyetTemplate/side-menu.jsp" %>
        <div class="col-sm-9 col-md-9">
            <h1 class="page-header">
                Add Test Result <!--Change here for title of the page-->
            </h1>
        </div>

    </div>
    <div class="row-fluid text-left">
        <div class="col-sm-9 col-md-9">
            <form action ="ResultController" method="post" style="padding:20px" class = "col-sm-11 col-md-11">
                <input name="requestID" type="hidden" class="form-control" readonly 
                       value = "${requestScope.testID}">
                <button name="action" value="btnCancel" type="submit" class="btn btn-primary">Cancel</button>												
            </form>
            <!--form insert-->
            <form action ="ResultController" method="post" style="padding:20px" class = "col-sm-11 col-md-11">
                <!--copy form input here...-->
                <div class = "div-table form-group">
                    <div class= "div-colunm div-col1">
                        <div class="form-group">			
                            <label>Test No</label>
                            <input name="requestID" type="text" class="form-control" readonly 
                                   value = "${requestScope.testID}">											
                        </div>
                        <div class="form-group">			
                            <label>Patient name</label>
                            <input name="patName" type="text" class="form-control" readonly 
                                   value = "${requestScope.pat.patName}">											
                        </div>
                        <div class="form-group">
                            <label>Patient Address</label>
                            <textarea name="patAddress" class="form-control" rows="2" 
                                      readonly>${requestScope.pat.address}</textarea>																		
                        </div>
                    </div>
                    <div class= "div-colunm div-col2">
                        <div class="form-group">	
                            <label>Admission ID</label>
                            <input name="adID" type="text" class="form-control" readonly 
                                   value = "${requestScope.adID}">													
                        </div>
                        <div class="form-group" style="width:45%; float:left">			
                            <label>Patient age</label>
                            <input name="patAge" type="text" class="form-control" readonly 
                                   value = "${requestScope.age}">												
                        </div>
                        <div class="form-group" style="float:right; width:45%">			
                            <label>Gender</label>
                            <input name="patGender" type="text" class="form-control" readonly 
                                   <c:if test="${requestScope.pat.gender eq true}"> value = "Female"</c:if>
                                   <c:if test="${requestScope.pat.gender eq false}"> value = "Male"</c:if>>												
                            </div>
                            <div class="form-group">			
                                <label>Appointed Date</label>
                                <input name="appDate" type="text" class="form-control" readonly 
                                       value = "<fmt:formatDate value="${requestScope.test.request.createDate}" pattern="dd/MM/yyyy HH:mm:ss"/>">												
                        </div>
                        <div class="form-group">			
                            <label>Completed Date</label>
                            <input name="compDate" type="text" class="form-control" readonly 
                                   value = "<fmt:formatDate value="${sessionScope.compDate}" pattern="dd/MM/yyyy HH:mm:ss"/>">												
                        </div>

                    </div>
                    <div class = "div-colunm div-col3">
                        <div class="form-group">			
                            <label>From faculty</label>
                            <input name="fFac" type="text" class="form-control" readonly 
                                   value = "${requestScope.test.request.fromFac.facultyName}">												
                        </div>

                        <div class="form-group">			
                            <label>Appointed doctor</label>
                            <input name="appDoc" type="text" class="form-control" readonly 
                                   value = "${requestScope.test.request.appointedDoc.fullname}">												
                        </div>
                        <div class="form-group">			
                            <label>To faculty</label>
                            <input name="concDoc" type="text" class="form-control" readonly 
                                   value = "${requestScope.test.request.toFac.facultyName}">												
                        </div>
                        <div class="form-group">			
                            <label>Tested doctor</label>
                            <input name="testDoc" type="text" class="form-control" readonly 
                                   value = "${sessionScope.empInfo.empName}">												
                        </div>

                    </div>	
                </div>
                <div class="form-group col-sm-8 col-md-8" style = "float:left">			
                    <label>Diagnosis</label>
                    <textarea class="form-control" rows="2" name="txtDiagn"
                              readonly>${requestScope.test.request.diagnosis}</textarea>											
                </div>
                <div class="form-group col-sm-4 col-md-4" align = "right" style = "margin-top:5%">
                    <button name="action" value="btnInsert" type="submit" class="btn btn-primary">Add Result</button>					
                </div>

                <div class="form-group col-sm-12 col-md-12">
                    <label>Test requirement:</label>		
                    <div class="table-responsive" style = "height:500px; overflow: auto">

                        <table class="table table-bordered table-hover table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Content</th>
                                    <th>Result</th>
                                    <th>Standard</th>
                                    <th>Unit</th>
                                    <th>Group</th>

                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${requestScope.showDataList}" var = "sv" varStatus="stt">
                                    <tr>
                                        <td>${stt.count}</td>
                                        <td>${sv.serviceName}</td>
                                        <td>
                                            <input name="${sv.reqDetailID}" class="form-control txtRs" type="text" 
                                                   <c:if test="${sv.svfMale ne null}"> pattern="^(-)?(\d+)(\.\d+)?$"</c:if>
                                                       required>
                                            </td>
                                        <td style="text-align: center">
                                            <c:if test="${requestScope.pat.gender eq true}">${sv.svfMale}</c:if>
                                            <c:if test="${requestScope.pat.gender eq false}">${sv.svMale}</c:if>
                                            </td>
                                        <td>${sv.unitOfMea}</td>
                                        <td>${sv.groupName}</td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </form>



            <!--
            <ul class="pagination">
                    <li><a href="#">First</a></li>
                    <li><a href="#">Previous</a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">Next</a></li>
                    <li><a href="#">Last</a></li>
      </ul>
            -->

            <!-- end table here-->
            <!--IMPORTANT!!! EDIT ZONE END FROM HERE - Do not change anything else-->

        </div>
    </div>
</body>
<%@include file="../tuyetTemplate/footer.jsp" %>

</html>
