<%-- 
    Document   : Homev2
    Created on : Jan 3, 2017, 11:01:20 AM
    Author     : Hoang Tuyet
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Home-v2</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap Core CSS -->
        <link href="../eHos1-war/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="../eHos1-war/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- jQuery -->
        <script src="../eHos1-war/js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="../eHos1-war/js/bootstrap.min.js"></script>
    </head>
    <body>
        <%@include file="../tuyetTemplate/header.jsp" %>
        <%@include file="../tuyetTemplate/side-menu.jsp" %>
    </div>
    <div class="row-fluid text-center" >
        <div class="col-md-9 col-sm-9" style="margin-bottom: 50px">
            <img src="./tuyetTemplate/bg_home.jpg" height="500px"/>
        </div>

        
        <!-- end table here-->
        <!--IMPORTANT!!! EDIT ZONE END FROM HERE - Do not change anything else-->

    </div>  
    <%@include file="../tuyetTemplate/footer.jsp" %>
</body>

</html>
