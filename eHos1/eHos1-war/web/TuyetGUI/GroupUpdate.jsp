<%-- 
    Document   : ServiceUpdate
    Created on : Jan 23, 2017, 2:48:59 PM
    Author     : Hoang Tuyet
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Update service Group</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- jQuery -->
        <script src="js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>
    </head>
    <body>
        <%@include file="../tuyetTemplate/header.jsp" %>
        <%@include file="../tuyetTemplate/side-menu.jsp" %>
        <div class="col-sm-9 col-md-9">
            <h1 class="page-header">
                Update Service Group <!--Change here for title of the page-->
            </h1>
        </div>

    </div>
    <div class="row-fluid text-left">
        <div class="col-sm-8 col-md-8" style="margin-bottom:50px">
            <!--Messages-->
            <div id="divMsg" hidden class="form-group alert alert-danger" align="left" >
                <p id="msgValidate"></p>
            </div>
            <c:if test="${requestScope.msg ne null}">
                <div id="svMsg" class="form-group alert <c:if test="${requestScope.t eq null}">alert-danger</c:if>
                     <c:if test="${requestScope.t eq '1'}">alert-success</c:if>" align="left" >
                    <p id="msg">${requestScope.msg}</p>
                </div>
            </c:if>

            <!--form update-->
            <form action="GroupController" method="post" style="padding:20px">
                <input type="hidden" name="groupID" value="${requestScope.editG.groupID}"/>
                <div class="form-group" >
                    <label>Group name </label>
                    <textarea name="txtGroupName" id="txtGroupName" class="form-control" rows="3">${requestScope.editG.groupName}</textarea>
                    <label>Description </label>
                    <textarea name="txtDesc" class="form-control" rows="3">${requestScope.editG.description}</textarea>
                    <label>Group type </label>
                    <select name="cbType" class="form-control" >
                        <option value="Exam" <c:if test="${requestScope.editG.groupType eq 'Exam'}">selected</c:if>>Exam</option>
                        <option value="Other" <c:if test="${requestScope.editG.groupType eq 'Other'}">selected</c:if>>Other</option>
                        </select>
                        <label>Status </label>
                        <select name="cbStatus" class="form-control" >
                            <option value="1" <c:if test="${requestScope.editG.status eq true}">selected</c:if>>Enable</option>
                        <option value="0" <c:if test="${requestScope.editG.status eq false}">selected</c:if>>Disable</option>
                        </select>
                        <label>Create date </label>
                        <input type="text" class="form-control" readonly 
                               value="<fmt:formatDate value="${requestScope.editG.createDate}" pattern="dd/MM/yyyy HH:mm:ss"/>"/>
                    <label>Creator </label>
                    <input type="text" class="form-control"readonly value="${requestScope.editG.creator.fullname}"/>
                    <label>Update date </label>
                    <input type="text" class="form-control" readonly 
                           value="<fmt:formatDate value="${requestScope.editG.updateDate}" pattern="dd/MM/yyyy HH:mm:ss"/>"/>
                    <label>Update by </label>
                    <input type="text" class="form-control" readonly value="${requestScope.editG.updateEmp.fullname}"/>									
                </div>


                <div class="form-group" align="right" >
                    <button name="action" value="btnUpdate" onclick="return checkValidate();"
                            type="submit" class="btn btn-primary" style="margin-top:10px;">Update</button>
                    <button name="action" value="btnCancel" type="submit" class="btn btn-primary" style="margin-top:10px;">Cancel</button>

                </div>
                <!--end form input...-->
            </form>

        </div>
    </div>	

</body>
<%@include file="../tuyetTemplate/footer.jsp" %>

<script type="text/javascript">
    function checkValidate() {
        var name = document.getElementById("txtGroupName").value;
        if (name === "") {
            //document.getElementById("svMsg").setAttribute("hidden", "hidden");
            document.getElementById("divMsg").removeAttribute("hidden");
            document.getElementById("msgValidate").innerHTML = "Group name is required";
            document.getElementById("txtGroupName").focus();

            return false;
        }
        return true;
    }
</script>
</html>

