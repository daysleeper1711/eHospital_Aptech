<%-- 
    Document   : AddTestRequest
    Created on : Jan 5, 2017, 11:54:31 AM
    Author     : Hoang Tuyet
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Edit Test Request</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- jQuery -->
        <script src="js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>
        <style>    
            /* Set black background color, white text and some padding */

            .div-table {
                display:table
            }
            .div-colunm {
                display: table-cell;
                width: 25%;
                padding: 0px 8px 0px 8px;
            }

        </style>
    </head>
    <body>
        <%@include file="../tuyetTemplate/header.jsp" %>
        <%@include file="../tuyetTemplate/side-menu.jsp" %>
        <div class="row-fluid text-center">
            <div class="col-sm-9 col-md-9">
                <h1 class="page-header">
                    Edit Test Request <!--Change here for title of the page-->
                </h1>
            </div>

        </div>
        <div class="row-fluid text-left">
            <div class="col-sm-9 col-md-9" style="margin-bottom:50px">
                <!--********* MESSAGES *************-->   
                <!--msg if list service empty-->
                <div class="form-group col-sm-11 col-md-11">
                    <c:if test="${requestScope.msgCheckCart ne null}">
                        <div class="alert alert-danger col-sm-8 col-md-8">
                            <c:out value = "${requestScope.msgCheckCart}"></c:out>
                            </div>
                    </c:if>
                    <!--msg if totalPayment > deposit-->
                    <c:if test="${requestScope.msgCheckDeposit ne null}">
                        <div class="alert alert-danger col-sm-8 col-md-8">
                            <p> ${requestScope.msgCheckDeposit} </p>
                        </div>
                    </c:if>
                    <!--msg if update Emp is not creator-->
                    <c:if test="${requestScope.msgUser ne null}">
                    <div class="alert alert-danger col-sm-8 col-md-8" id="msg">
                        <p id="msgCheckUser">${requestScope.msgUser}</p>
                    </div>
                    </c:if>
                    <!--msg if totalPayment > deposit-->
                    <c:if test="${requestScope.msgSuccess ne null}">
                        <div class="alert alert-success col-sm-8 col-md-8">
                            <p> ${requestScope.msgSuccess} </p>
                        </div>
                    </c:if>

                </div>
                <!--form update-->
                <form action="TuyetMainController" method="post" style="padding:20px" class = "col-sm-11 col-md-11">
                    <!--IMPORTANT!!! EDIT ZONE START FROM HERE - Do not change anything else-->
                    <!--I am not responsible any wrong if you change anything without permission-->
                    <!--FOR THE ENGLISH TRANSLATE - GOOGLE PLZ ^_^-->

                    <!--copy form input here...-->
                    <div class="form-group" align = "right">
                        <button name="action" value="btnSaveReq" type="submit" 
                                class="btn btn-primary">Save Changes</button>					

                    </div>
                    <div class = "div-table form-group">
                        <div class= "div-colunm div-col1">

                            <div class="form-group" >
                                <label>Test ID</label>
                                <input name="txtTestID" type="text" class="form-control" readonly 
                                       value = "${sessionScope.infoTest.testID}">
                            </div>	

                            <div class="form-group">			
                                <label>Patient name</label>
                                <input name="txtPatName" type="text" class="form-control" readonly 
                                       value = "${sessionScope.infoTest.patName}">											
                            </div>
                            <div class="form-group">
                                <label>Patient Address</label>
                                <textarea name="txtAddr" class="form-control" rows="2" 
                                          readonly>${sessionScope.infoTest.patAddr}</textarea>																		
                            </div>
                        </div>
                        <div class= "div-colunm div-col2">
                            <div class="form-group">	
                                <label>Admission ID</label>
                                <input name="txtAdID" type="text" class="form-control" readonly 
                                       value = "${sessionScope.infoTest.admissionID}">													
                            </div>
                            <div class="form-group">			
                                <label>Patient age</label>
                                <input name ="txtName" type="text" class="form-control" readonly 
                                       value = "${sessionScope.infoTest.patAge}">												
                            </div>
                            <div class="form-group">			
                                <label>Gender</label>
                                <input name="txtGender" type="text" class="form-control" readonly 
                                       value = "${sessionScope.infoTest.patGender}">												
                            </div>

                        </div>
                        <div class = "div-colunm div-col3">
                            <div class="form-group">			
                                <label>From faculty</label>
                                <input name="txtFrFac" type="text" class="form-control" readonly 
                                       value = "${sessionScope.infoTest.frFac}">												
                            </div>
                            <div class="form-group">			
                                <label>To faculty</label>
                                <input name="txtToFac" type="text" class="form-control" readonly 
                                       value = "${sessionScope.infoTest.toFac}">												
                            </div>
                            <div class="form-group">			
                                <label>Appointed doctor</label>
                                <input name="txtAppDoc" type="text" class="form-control" readonly id="txtAppDoc"
                                       value = "${sessionScope.empInfo.empName}">												
                            </div>
                        </div>	
                    </div>
                    <div name="txtDiagnosis"  class="form-group col-sm-12 col-md-12">			
                        <label>Diagnosis</label>
                        <textarea name="txtDiagnosis" id="txtDiagnosis" class="form-control" rows="2" 
                                  required onblur="getcontain();">${sessionScope.infoTest.diagnosis}</textarea>											
                    </div>
                </form>
                <form action="TuyetMainController" method="post" class = "col-sm-10 col-md-10">
                    <!--diagnosis-->
                    <textarea hidden id ="saveDiagnosis" name="saveDiagnosis"></textarea>
                    <!--test ID-->
                    <input name="txtTestID" type="hidden" value="${sessionScope.infoTest.testID}"/>
                    <!--page signal-->
                    <input name="pSignal" type="hidden" value="1"/>
                    <div class="form-group col-sm-8 col-md-8">
                        <button name="action" value="btnChooseService" type="submit" 
                                class="btn btn-primary" align="right">
                            Choose services
                        </button>
                        <button name="action" value="btnCancelEditReq" type="submit" 
                                class="btn btn-primary">Cancel</button>
                        <button name="action" value="btnDeleteReq" type="submit" 
                                class="btn btn-primary">Delete</button>	
                    </div>
                </form>
                

                <c:if test="${not empty sessionScope.cart.serviceCart}">
                    <div class="form-group col-sm-11 col-md-11">	
                        <div class="table-responsive">
                            <label>List of requested service</label>
                            <label style="margin-left: 70%">Total payment: <b style="color:red">$${sessionScope.cart.totalPayment()}</b></label>
                            <table class="table table-bordered table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Service name</th>
                                        <th>Price </th>
                                        <th>Group</th>		
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach items="${sessionScope.cart.serviceCart}" var="dto" varStatus="stt">
                                        <tr>
                                            <td>${stt.count}</td>
                                            <td>${dto.serviceName} </td>
                                            <td>$${dto.unitPrice}</td>
                                            <td>${dto.serviceGroup}</td>
                                            <td>
                                                <form action = "TuyetMainController" method="post">
                                                    <!--diagnosis-->
                                                    <textarea hidden id ="saveDiagnosis1" name="saveDiagnosis1"></textarea>
                                                    <!--page signal-->
                                                    <input name="pSignal" type="hidden" value="1"/>
                                                    <input name="txtID" type="hidden" value ="${dto.serviceID}"/>
                                                    <button name="action" value="btnRemoveFromCart" type="submit" 
                                                            class="btn btn-default"> Remove </button>							
                                                </form>
                                            </td>
                                        </tr>
                                    </c:forEach> 
                                </tbody>
                            </table>
                        </div>
                        <!--
                        <ul class="pagination">
                                <li><a href="#">First</a></li>
                                <li><a href="#">Next</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">Previous</a></li>
                                <li><a href="#">Last</a></li>
                  </ul> -->


                        <!-- end table here-->
                        <!--IMPORTANT!!! EDIT ZONE END FROM HERE - Do not change anything else-->
                    </div>
                </c:if>
            </div>
        </div>
    </body>
    <script>
        function getcontain() {
            var contains = document.getElementById("txtDiagnosis").value;

            document.getElementById("saveDiagnosis").value = contains;
            document.getElementById("saveDiagnosis1").value = contains;
        }

    </script>

    <%@include file="../tuyetTemplate/footer.jsp" %>
</html>
