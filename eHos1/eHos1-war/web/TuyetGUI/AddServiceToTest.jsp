<%-- 
    Document   : AddTestRequest
    Created on : Jan 5, 2017, 11:54:31 AM
    Author     : Hoang Tuyet
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Add Test Request</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- jQuery -->
        <script src="js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>

    </head>
    <body>
        <%@include file="../tuyetTemplate/header.jsp" %>
        <%@include file="../tuyetTemplate/side-menu.jsp" %>
        <div class="row-fluid text-center">
            <div class="col-sm-9 col-md-9">
                <h1 class="page-header">
                    Add Test Service(s) <!--Change here for title of the page-->
                </h1>
            </div>

        </div>
        <div class="row-fluid text-left">
            <div class="col-sm-9 col-md-9"style="margin-bottom:50px">
                <form action="TuyetMainController" method="post" name="frSearch" style="padding:20px">
                    <div class="form-group col-sm-4 col-md-4" style = "float:left; margin-right:5%">
                        <input name="txtSearchName" id="txtSearchName"
                               <c:if test="${requestScope.name ne null}">value="${requestScope.name}"</c:if>
                                   class="form-control" placeholder = "Enter service name">
                        </div>
                        <div class="form-group col-sm-4 col-md-4" style = "float:left; margin-right:5%">
                            <select name="selectGroup" id="selectGroup" class="form-control">
                                <option value = "" <c:if test="${group == null}">selected</c:if>>All Group</option>
                            <c:forEach items="${requestScope.listGroup}" var="gr" varStatus="stt">
                                <option value = "${gr.groupName}" <c:if test="${requestScope.group eq gr.groupName}">selected</c:if>>${gr.groupName}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group col-sm-6 col-md-6" >
                        <button name= "action" value="btnShowAllService" type="submit" class="btn btn-primary">
                            Show all services</button>
                        <button name= "action" value="btnFindService" type="submit" 
                                class="btn btn-primary">Search</button>
                        <button name="action" value="btnBack" type="submit" 
                                class="btn btn-primary"> Back </button>
                        <!--page signal-->
                        <input name="pSignal" type="hidden" value="${requestScope.ps}"/>

                    </div>

                </form>
                <!--Messages-->
                <c:if test="${requestScope.msg != null}">
                    <div class="alert alert-danger col-sm-8 col-md-8">
                        <p>${requestScope.msg}</p>
                    </div>
                </c:if>
                <c:if test="${requestScope.msgClickSearch ne null}">
                    <div class="alert alert-danger col-sm-8 col-md-8">
                        <p> ${requestScope.msgClickSearch} </p>
                    </div>
                </c:if>

                <c:if test = "${not empty requestScope.listService}">
                    <div class="col-sm-9 col-md-9">	
                        <div id="listService" class = "panel panel-default">
                            <div class = "panel-body">
                                <form action ="TuyetMainController" method="post" style="padding:20px">
                                    <input name="pSignal" type="hidden" value="${requestScope.ps}"/>
                                    <div class="form-group" align = "right">
                                        <label style = "float:left">List of services</label>							
                                        <button name="action" value="btnAddSV2Test" type="submit" class="btn btn-primary">
                                            Add service</button>
                                    </div>

                                    <!--copy form input here...-->
                                    <div class="form-group" style = "height: 300px; overflow:auto">
                                        <c:forEach items="${requestScope.listService}" var = "dto" varStatus = "stt">
                                            <div class="checkbox">
                                                <label>
                                                    <input name="selectedService" type="checkbox" value="${dto.service.serviceID}" >${dto.service.serviceName}
                                                </label>
                                            </div>
                                        </c:forEach>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </c:if>     
            </div>
        </div>
    </body>
    <%@include file="../tuyetTemplate/footer.jsp" %>
</html>
