<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Detail of test result</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- jQuery -->
        <script src="js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>
        <style>    
            .div-table {
                display:table
            }
            .div-colunm {
                display: table-cell;
                width: 25%;
                padding: 0px 8px 0px 8px;
            }

        </style>
    </head>
    <body>
        <%@include file="../tuyetTemplate/header.jsp" %>
        <%@include file="../tuyetTemplate/side-menu.jsp" %>
        <div class="col-sm-9 col-md-9">
            <h1 class="page-header">
                Detail of Test Result <!--Change here for title of the page-->
            </h1>
        </div>

    </div>
    <div class="row-fluid text-left">
        <div class="col-sm-9 col-md-9" style="margin-bottom: 50px">
            <!--Messages-->
<!--            <div id="divMsg" hidden class="form-group alert alert-danger" align="left" >
                <p id="msgValidate"></p>
            </div>-->
            <c:if test="${requestScope.msg ne null}">
                <div id="serverMsg" class="form-group alert <c:if test="${requestScope.t eq '1'}">alert-success</c:if>
                     <c:if test="${requestScope.t eq null}">alert-danger</c:if> " align="left" >
                    <p id="msg">${requestScope.msg}</p>
            </div>
            </c:if>
            <!--form update-->
            <form action="ResultController" method="post" style="padding:20px" class = "col-sm-11 col-md-11">

                <div class = "div-table form-group">
                    <div class= "div-colunm div-col1">
                        <div class="form-group">			
                            <label>Test No</label>
                            <input name="requestID" type="text" class="form-control" readonly 
                                   value = "${requestScope.testID}">											
                        </div>
                        <div class="form-group">			
                            <label>Patient name</label>
                            <input name="patName" type="text" class="form-control" readonly 
                                   value = "${requestScope.pat.patName}">											
                        </div>
                        <div class="form-group">
                            <label>Patient Address</label>
                            <textarea name="padAddress" class="form-control" rows="2" 
                                      readonly>${requestScope.pat.address}</textarea>																		
                        </div>
                    </div>
                    <div class= "div-colunm div-col2">
                        <div class="form-group">	
                            <label>Admission ID</label>
                            <input name="adID" type="text" class="form-control" readonly 
                                   value = "${requestScope.adID}">													
                        </div>
                        <div class="form-group" style="width:45%; float:left">			
                            <label>Patient age</label>
                            <input name="patAge" type="text" class="form-control" readonly 
                                   value = "${requestScope.age}">												
                        </div>
                        <div class="form-group" style="float:right; width:45%">			
                            <label>Gender</label>
                            <input type="text" class="form-control" readonly 
                                   <c:if test="${requestScope.pat.gender eq true}">value = "Female"</c:if>
                                   <c:if test="${requestScope.pat.gender eq false}">value = "Male"</c:if>>												
                            </div>
                            <div class="form-group">			
                                <label>Appointed Date</label>
                                <input name="appDate" type="text" class="form-control" readonly 
                                       value = "<fmt:formatDate value="${requestScope.test.request.createDate}" pattern="dd/MM/yyyy HH:mm:ss"/>">												
                        </div>
                        <div class="form-group">			
                            <label>Completed Date</label>
                            <input name="compDate" type="text" class="form-control" readonly 
                                   value = "<fmt:formatDate value="${requestScope.rs.createDate}" pattern="dd/MM/yyyy HH:mm:ss"/>">												
                        </div>

                    </div>
                    <div class = "div-colunm div-col3">
                        <div class="form-group">			
                            <label>From faculty</label>
                            <input name="fFac" type="text" class="form-control" readonly 
                                   value = "${requestScope.test.request.fromFac.facultyName}">												
                        </div>

                        <div class="form-group">			
                            <label>Appointed doctor</label>
                            <input name="appDoc" type="text" class="form-control" readonly 
                                   value = "${requestScope.test.request.appointedDoc.fullname}">												
                        </div>
                        <div class="form-group">			
                            <label>Tested doctor</label>
                            <input name="testDoc" type="text" class="form-control" readonly 
                                   value = "${requestScope.rs.testDoc.fullname}">												
                        </div>
                        <div class="form-group">			
                            <label>Concluded doctor</label>
                            <input name="concludeDoc" type="text" class="form-control" readonly 
                                   <c:if test="${requestScope.rs.concludeDoc.fullname ne null}">value = "${requestScope.rs.concludeDoc.fullname}"</c:if>
                                   <c:if test="${requestScope.rs.concludeDoc.fullname eq null and sessionScope.empInfo.position ne 'testdoctorv2'}"> value = "${sessionScope.empInfo.empName}"</c:if> />												
                        </div>
                    </div>	
                </div>
                <div class="form-group" style = "width:30%; float:left; margin-right:3%">			
                    <label>Diagnosis</label>
                    <textarea name="txtDiagn" class="form-control" rows="3" 
                              readonly>${requestScope.test.request.diagnosis}</textarea>											
                </div>
                <div class="form-group" style = "width:30%; float:left; margin-right:3%">			
                    <label>Conclusion</label>
                    <textarea id="txtConclusion" name="txtConclusion" class="form-control" 
                              rows="3">${requestScope.rs.conclusion}</textarea>											
                </div>
                <div class="form-group" style = "width:30%; float:left;">			
                    <label>Note</label>
                    <textarea name="txtNote" class="form-control" 
                              rows="3">${requestScope.rs.note}</textarea>											
                </div>
                <div class="form-group" align = "right">
                    <button name="action" value="btnSave" type="submit" onclick="return checkNull();" class="btn btn-primary">Save</button>					
                    <button name="action" value="btnCancel" type="submit" class="btn btn-primary">Cancel</button>	
                    <button name="action" type="submit" value="btnPrint" class="btn btn-primary"
                            <c:if test="${requestScope.rs.conclusion eq null}">hidden</c:if>>Print</button>

                    <input name="txtSearchAddID" type="hidden" value="${requestScope.adID}"/>
                    <input name="optionsRadiosInline" type="hidden" value="${requestScope.patType}"/>
                    <input name="psignal" value="${requestScope.ps}" type="hidden"/>
                </div>		
            </form>

            <div class="form-group col-sm-11 col-md-11">	
                <div class="table-responsive">
                    <label>Result(s) of Test</label>
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Service name</th>
                                <th>Result </th>
                                <th>Standard</th>
                                <th>Unit</th>
                                <th>Group</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${requestScope.showResultList}" var="rs" varStatus="stt">
                            <tr>
                                <td>${stt.count}</td>
                                <td>${rs.serviceName}</td>
                                <td <c:if test="${rs.cpStandard eq 'lt'}">style = "text-align:left; font-weight:bold; text-decoration: underline"</c:if> 
                                    <c:if test="${rs.cpStandard eq 'gt'}">style = "text-align:right; font-weight:bold; text-decoration: underline"</c:if> 
                                    <c:if test="${rs.cpStandard eq 'eq'}">style = "text-align:center;"</c:if> >
                                    ${rs.result}
                                </td>
                                <td style="text-align: center">
                                    <c:if test="${requestScope.pat.gender  eq true}">${rs.svfMale}</c:if>
                                    <c:if test="${requestScope.pat.gender  eq false}">${rs.svMale}</c:if>
                                </td>
                                <td>${rs.unitOfMea}</td>
                                <td>${rs.groupName}</td>
                            </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
                <!--
                <ul class="pagination">
                        <li><a href="#">First</a></li>
                        <li><a href="#">Next</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">Previous</a></li>
                        <li><a href="#">Last</a></li>
          </ul> -->


                <!-- end table here-->
                <!--IMPORTANT!!! EDIT ZONE END FROM HERE - Do not change anything else-->
            </div>
        </div>	
</body>
<%@include file="../tuyetTemplate/footer.jsp" %>
<!--<script>
    function checkNull() {
        var conclude = document.getElementById("txtConclusion").value;
        if(conclude === "") {
            document.getElementById("divMsg").removeAttribute("hidden");
            //document.getElementById("serverMsg").setAttribute("hidden","hidden");
            document.getElementById("msgValidate").innerHTML = "Conclusion is required";
            document.getElementById("txtConclusion").focus();
            return false;
        }
        return true;
    }
</script>-->
</html>
