<%-- 
    Document   : ServiceUpdate
    Created on : Jan 23, 2017, 2:48:59 PM
    Author     : Hoang Tuyet
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Update Standard Value</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- jQuery -->
        <script src="js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>
    </head>
    <body>
        <%@include file="../tuyetTemplate/header.jsp" %>
        <%@include file="../tuyetTemplate/side-menu.jsp" %>
        <div class="col-sm-9 col-md-9">
            <h1 class="page-header">
                Update Standard Value of Service  <!--Change here for title of the page-->
            </h1>
        </div>

    </div>
    <div class="row-fluid text-left">
        <div class="col-sm-8 col-md-8" style="margin-bottom:50px">
            <!--Messages-->
            <div id="divMsg" hidden class="form-group alert alert-danger" align="left" >
                <p id="msgValidate"></p>
            </div>
            <c:if test="${requestScope.msg ne null}">
                <div class="form-group alert alert-success" id="successMsg" align="left" >
                    <p id="msg">${requestScope.msg}</p>
                </div>
            </c:if>
            <!--form insert-->
            <form action="SVController" method="post" style="padding:20px">
                <input name="serviceID" type="hidden" value="${requestScope.standard.serviceID.serviceID}"/>
                <input name="svID" type="hidden" value="${requestScope.standard.svId}"/>
                <div class="form-group" >
                    <label>Service name </label>
                    <textarea name="serviceName" class="form-control" rows="2" 
                              readonly="true">${requestScope.serviceName}</textarea>															
                </div>
                <div class="form-group" style="width:45%; float:right; margin-left:10%" >
                    <fieldset>
                        <legend>Standard value for female</legend>
                        <label>Standard value </label>
                        <input name="txtSVfmale" id="txtSVfmale" onblur="styleSV();" 
                               <c:if test = "${not empty requestScope.fmin}">readonly </c:if>
                                   type="text" class="form-control"
                                   value="<c:if test="${empty requestScope.fmin}">${requestScope.fmale}</c:if>"/>
                               <label>From </label>
                               <input name="txtfmaleMin" id="txtfmaleMin" onblur="styleSV();"
                               <c:if test = "${empty requestScope.fmin}">readonly</c:if>
                               type="text" class="form-control" placeholder="Enter min value" value="${requestScope.fmin}"/>
                        <label>To </label>
                        <input name="txtfmaleMax" id="txtfmaleMax" onblur="styleSV();"
                               <c:if test = "${empty requestScope.fmax}">readonly</c:if>
                               type="text" class="form-control" placeholder="Enter max value" value="${requestScope.fmax}"/>
                    </fieldset>                
                </div>
                <div class="form-group" style="width:45%;" >

                    <fieldset>
                        <legend>Standard value for male</legend>
                        <label>Standard value </label>
                        <input name="txtSVMale" id="txtSVMale" onblur="styleSV();"
                               <c:if test = "${not empty requestScope.mmin}">readonly</c:if>
                                   type="text" class="form-control"
                                   value="<c:if test="${empty requestScope.mmin}">${requestScope.male}</c:if>"/>
                               <label>From </label>
                               <input name="txtMaleMin" id="txtMaleMin" onblur="styleSV();"
                               <c:if test = "${empty requestScope.mmin}">readonly</c:if>
                               type="text" class="form-control" placeholder="Enter min value" value="${requestScope.mmin}"/>
                        <label>To </label>
                        <input name="txtMaleMax" id="txtMaleMax" onblur="styleSV();"
                               <c:if test = "${empty requestScope.mmax}">readonly</c:if>
                               type="text" class="form-control" placeholder="Enter max value" value="${requestScope.mmax}"/>
                    </fieldset>                
                </div>

                <div class="form-group" >

                    <label>Create date: </label>
                    <input type="text" class="form-control" readonly 
                           value="<fmt:formatDate value="${requestScope.standard.createDate}" pattern="dd/MM/yyyy HH:mm:ss"/>"/>
                    <label>Creator: </label>
                    <input type="text" class="form-control"readonly value="${requestScope.standard.creator.fullname}"/>
                    <label>Update date: </label>
                    <input type="text" class="form-control" readonly 
                           value="<fmt:formatDate value="${requestScope.standard.updateDate}" pattern="dd/MM/yyyy HH:mm:ss"/>"/>
                    <label>Update by: </label>
                    <input type="text" class="form-control" readonly value="${requestScope.standard.updateEmp.fullname}"/>

                </div>

                <div class="form-group" align="right" >
                    <button name="action" id="btnAdd" type="submit" value="updateSV" onclick="return validate();"
                            class="btn btn-primary" style="margin-top:10px;">Update</button>
                    <button name="action" id="btnCancel" type="submit" value="btnCancel"
                            class="btn btn-primary" style="margin-top:10px;">Cancel</button>

                </div>
                <!--end form input...-->
            </form>
            <p style="color: red; font-style: italic">*Enter 'Standard Value' if it is fixed value</p>
            <p style="color: red; font-style: italic">*Enter 'From' - 'To' if it is dynamic value</p>

        </div>
    </div>	

</body>
<%@include file="../tuyetTemplate/footer.jsp" %>
<script>
    function validate() {

        var male = document.getElementById("txtSVMale").value;
        var mmin = document.getElementById("txtMaleMin").value;
        var mmax = document.getElementById("txtMaleMax").value;

        var fmale = document.getElementById("txtSVfmale").value;
        var fmin = document.getElementById("txtfmaleMin").value;
        var fmax = document.getElementById("txtfmaleMax").value;

        if (male === "" && mmin === "" && mmax === "") {
            document.getElementById("divMsg").removeAttribute("hidden");
            document.getElementById("msgValidate").innerHTML = "Please, enter standard value for male!";
            document.getElementById("successMsg").setAttribute("hidden", "hidden");
            document.getElementById("txtSVMale").focus();
            return false;
        }
        if (fmale === "" && fmin === "" && fmax === "") {
            document.getElementById("divMsg").removeAttribute("hidden");
            document.getElementById("msgValidate").innerHTML = "Please, enter standard value for female!";
            document.getElementById("successMsg").setAttribute("hidden", "hidden");
            document.getElementById("txtSVfmale").focus();
            return false;
        }

        if (mmin !== "" && mmax === "") {
            document.getElementById("divMsg").removeAttribute("hidden");
            document.getElementById("msgValidate").innerHTML = "Please, enter max standard value for male!";
            document.getElementById("successMsg").setAttribute("hidden", "hidden");
            document.getElementById("txtMaleMax").focus();
            return false;
        }

        if (mmin === "" && mmax !== "") {
            document.getElementById("divMsg").removeAttribute("hidden");
            document.getElementById("msgValidate").innerHTML = "Please, enter min standard value for male!";
            document.getElementById("successMsg").setAttribute("hidden", "hidden");
            document.getElementById("txtMaleMin").focus();
            return false;
        }

        if (fmin !== "" && fmax === "") {
            document.getElementById("divMsg").removeAttribute("hidden");
            document.getElementById("msgValidate").innerHTML = "Please, enter max standard value for female!";
            document.getElementById("successMsg").setAttribute("hidden", "hidden");
            document.getElementById("txtfmaleMax").focus();
            return false;
        }

        if (fmin === "" && fmax !== "") {
            document.getElementById("divMsg").removeAttribute("hidden");
            document.getElementById("msgValidate").innerHTML = "Please, enter min standard value for female!";
            document.getElementById("successMsg").setAttribute("hidden", "hidden");
            document.getElementById("txtfmaleMin").focus();
            return false;
        }
       //error pattern
        var ptt = /^(-)?(\d+)(\.\d+)?$/;

        if (male !== "") {
            if (!ptt.test(male) || !ptt.test(fmale)) {
                document.getElementById("divMsg").removeAttribute("hidden");
                document.getElementById("successMsg").setAttribute("hidden", "hidden");
                document.getElementById("msgValidate").innerHTML = "Value of Standard didn't match the requested format! (Format Ex: 10, 10.5)";
                return false;
            }
        } else {
            if (!ptt.test(mmin) || !ptt.test(mmax) || !ptt.test(fmin) || !ptt.test(fmax)) {
                document.getElementById("divMsg").removeAttribute("hidden");
                document.getElementById("successMsg").setAttribute("hidden", "hidden");
                document.getElementById("msgValidate").innerHTML = "Value of From/To field didn't match the requested format! (Format Ex: 10, 10.5)";
                return false;
            }
        }
        return true;
    }

    function styleSV() {
        var male = document.getElementById("txtSVMale").value;
        var mmin = document.getElementById("txtMaleMin").value;
        var mmax = document.getElementById("txtMaleMax").value;

        var fmale = document.getElementById("txtSVfmale").value;
        var fmin = document.getElementById("txtfmaleMin").value;
        var fmax = document.getElementById("txtfmaleMax").value;

        if (male !== "" || fmale !== "") {
            document.getElementById("txtMaleMin").setAttribute("readonly", "readonly")
            document.getElementById("txtMaleMax").setAttribute("readonly", "readonly");
            document.getElementById("txtfmaleMin").setAttribute("readonly", "readonly");
            document.getElementById("txtfmaleMax").setAttribute("readonly", "readonly");
        }

        if (male === "" && fmale === "") {
            document.getElementById("txtMaleMin").removeAttribute("readonly")
            document.getElementById("txtMaleMax").removeAttribute("readonly");
            document.getElementById("txtfmaleMin").removeAttribute("readonly");
            document.getElementById("txtfmaleMax").removeAttribute("readonly");
        }
        if (mmin !== "" || mmax !== "" || fmin !== "" || fmax !== "") {
            document.getElementById("txtSVMale").setAttribute("readonly", "readonly");
            document.getElementById("txtSVfmale").setAttribute("readonly", "readonly");
        }
        if (mmin === "" && mmax === "" && fmin === "" && fmax === "") {
            document.getElementById("txtSVMale").removeAttribute("readonly");
            document.getElementById("txtSVfmale").removeAttribute("readonly");
        }
    }
</script>
</html>

