<%@include file="header.jsp" %>
<div class="col-sm-9 col-md-9 text-left">
    <h1 class="page-header">
        Diagnosis <small>${sessionScope.empInfo.faculty} - ${sessionScope.empInfo.position}</small>
    </h1>
</div>
</div>
<div class="row-fluid text-left">
    <div class="col-sm-8 col-md-8">
        <!--Load button-->
        <form style="padding:20px" id="loadForm" action="DiagnosisMainProcess" method="post">
            <button class="btn btn-default" type="submit" form="loadForm" name="btnSubmit" 
                    value="Load Diagnosis Medical Record"/>
            Load <i class="fa fa-download" aria-hidden="true"></i>
            </button>			
        </form>
        <!-- AFTER INPUT-->
        <c:if test="${requestScope.success ne null}">
            <div class="alert alert-success">
                <strong>Success putting diagnosis</strong> 
            </div>
        </c:if>
        <c:if test="${requestScope.success eq null and requestScope.resultList eq null}">
            <div class="alert alert-warning">
                <strong>Fails putting diagnosis</strong> 
            </div>
        </c:if>
        <!-- DISPLAY RESULT -->
        <c:if test="${requestScope.resultList ne null}">
            <!--Empty result display here...-->
            <c:if test="${empty requestScope.resultList}">
                <div class="alert alert-warning">
                    <strong>Attention!</strong> List is empty...
                </div>
            </c:if>
            <!--Result display here...-->
            <c:if test="${not empty requestScope.resultList}">
                <div class="table-responsive">
                    <h3>List patient waiting for diagnosis</h3>
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                                <th>Ord</th>
                                <th>Medical Record ID</th>
                                <th>Name</th>
                                <th>Registry date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody id="itemContainer">
                            <c:forEach items="${requestScope.resultList}" var="mrInfo" varStatus="i">
                                <tr>
                                    <td>${i.count}</td>
                                    <td>${mrInfo.medRecordID}</td>
                                    <td>${mrInfo.patInfo.patName}</td>
                                    <td>${mrInfo.createDate}</td>
                                    <td>
                                        <form action="DiagnosisMainProcess" method="post">
                                            <button type="submit" class="btn btn-default" 
                                                    name="btnSubmit" value="Diagnosis Input">
                                                Diagnosis <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                                            </button>
                                            <!--TEST pass object-->
                                            <input type="hidden" name="passObject" value="${mrInfo.medRecordID}" />
                                        </form>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
                <!--PAGINATION-->
                <div class="holder"></div>
            </c:if>
        </c:if>
        <!-- end table here-->
        <!--IMPORTANT!!! EDIT ZONE END FROM HERE - Do not change anything else-->
    </div>
</div>	
<%@include file="footer.jsp" %>