<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="loc" %>
<!DOCTYPE html>

<html >
    <head>
        <title>Export Drugs Management</title>
        <meta charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/datepicker.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- jQuery -->
        <script language="javascript" src="js/jquery.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>
        <style>    
            /* Set black background color, white text and some padding */
            footer {
                background-color: #555;
                color: white;
                padding: 8px;
            }

            .loccolum{
                display: table-cell;
                padding: 0px 8px 0px 0px;

            }
            .loccenter{
                display: table;
                padding: 0px 0px 0px 20px;
            }


        </style>

        <!--pagination-->
        <script type="text/javascript" src="js/jPager/js.js"></script>
        <script type="text/javascript" src="js/jPager/jPages.min.js"></script>
        <link rel="stylesheet" href="css/jPager/jPages.css" type="text/css"/>

        <script>
//            run when document is ready
            $(function() {
//                initiate plugin
                $("div.holder").jPages({
                    containerID: "itemContainer",
                    perPage: 5
                });
            });
        </script>

    </head>
    <body>
        <%@include file="locGUI/locheader.jsp" %>

        <div class="row-fluid text-center">
            <%@include file="locGUI/locmenu.jsp" %>
            <div class="col-sm-9 col-md-9">
                <h1 class="page-header">
                    <small>Export Drugs Management</small> <!--Change here for title of the page-->
                </h1>
            </div>

        </div>

        <div class="row-fluid text-left">
            <div class="col-md-1 col-sm-1"></div>
            <div class="col-sm-6 col-md-6">
                <form style="padding:20px" action="SearchExportDrugsServlet" method="post">
                    <!--IMPORTANT!!! EDIT ZONE START FROM HERE - Do not change anything else-->
                    <!--I am not responsible any wrong if you change anything without permission-->
                    <!--FOR THE ENGLISH TRANSLATE - GOOGLE PLZ ^_^-->

                    <!--copy form input here...-->
                    <!--input 1-->

                    <div class="loccenter" >
                        <!--Left-->
                        <div class="loccolum">

                            <div class="form-group">
                                <label>Docentry</label>
                                <input class="form-control" id="txtdocentry" name="txtdocentry">
                            </div>
                        </div>

                        <!--Right-->
                        <div class="loccolum">

                            <div class="form-group">
                                <label>Type</label>
                                <select class="form-control" id="selectType" name="selectType">
                                    <option value="S">Surgery</option>
                                    <option value="P">Prescription</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>From Date</label>
                                <div class='input-group date'>
                                    <input type="text" id="startDate" class="form-control" name="txtFromDate" readonly="true"/>
                                    <span class="input-group-addon">
                                        <a href="#"  id="dp4" data-date-format="yyyy-mm-dd" data-date="2017-01-08">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </a>
                                    </span>
                                </div>
                            </div> 

                        </div>

                        <div class="loccolum">

                            <div class="form-group">
                                <label>Status</label>
                                <select class="form-control" id="selectStatus" name="selectStatus">
                                    <option value="O">Open</option>
                                    <option value="F">Finish</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>To Date</label>
                                <div class='input-group date'>
                                    <input type="text" id="endDate" class="form-control" name="txtToDate" readonly="true"/>
                                    <span class="input-group-addon">
                                        <a href="#" id="dp5"  data-date-format="yyyy-mm-dd" data-date="2017-01-08">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </a>
                                    </span>
                                </div>
                            </div>

                        </div>

                    </div><!--End div Test-->

                    <div class="form-group" id="changeradio" >
                        <label>Search ... </label>
                        <label class="radio-inline">
                            <input type="radio" name="optionsRadiosInline" id="radio" value="Docentry" checked>Docentry</input>
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="optionsRadiosInline" id="radio" value="Status">Status</input>
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="optionsRadiosInline" id="radio" value="Date">Date</input> 
                        </label>

                    </div>

                    <div class="alert alert-error" id="alert">
                        <strong style="color: red;"></strong>
                    </div><b/>

                    <div style="text-align: center">
                        <button type="submit" class="btn btn-default" name="action" id="btnSearch" value="btnSearch">Search</button> 
                    </div>

                    <!--JQuery-->

                    <script language="javascript">
                        $(document).ready(function()
                        {
                            //Begin Ham xu ly datepicker
                            var startDate = new Date();
                            var endDate = new Date();
                            startDate.setDate(startDate.getDate() - 1);

                            $('#startDate').val(startDate.getFullYear() + "-" + (startDate.getMonth() + 1) + "-" + startDate.getDate());
                            $('#endDate').val(endDate.getFullYear() + "-" + (endDate.getMonth() + 1) + "-" + endDate.getDate());
                            $('#dp4').attr("data-date", startDate.getFullYear() + "-" + (startDate.getMonth() + 1) + "-" + startDate.getDate());
                            $('#dp5').attr("data-date", startDate.getFullYear() + "-" + (startDate.getMonth() + 1) + "-" + startDate.getDate());
                            $('#dp4').datepicker()
                                    .on('changeDate', function(ev) {
                                        if (ev.date.valueOf() > endDate.valueOf()) {
                                            $('#alert').show().find('strong').text('Error: The From date can not be greater then the To date');
                                        } else {
                                            $('#alert').hide();
                                            startDate = new Date(ev.date);
                                            $('#startDate').val($('#dp4').data('date'));
                                        }

                                    });
                            $('#dp5').datepicker()
                                    .on('changeDate', function(ev) {
                                        if (ev.date.valueOf() < startDate.valueOf()) {
                                            $('#alert').show().find('strong').text('Error: The To date can not be less then the From date');
                                        } else {
                                            $('#alert').hide();
                                            endDate = new Date(ev.date);
                                            $('#endDate').val($('#dp5').data('date'));
                                        }
                                        $('#dp5').datepicker('hide');
                                    });
                            //Begin Ham xu ly datepicker




                            //Chan disabled luc onLoad
                            var a = $('input:checked').val();
                            if (a === "Docentry") {
                                $('#txtdocentry').prop("disabled", false);
                                $('#selectType').prop("disabled", false);
                                $('#selectStatus').prop("disabled", true);
                                
                                $('#dp4').prop("hidden", true);
                                $('#dp5').prop("hidden", true);
                            } else if (a === "Status")
                            {
                                $('#txtdocentry').prop("disabled", true);
                                $('#selectType').prop("disabled", true);
                                $('#selectStatus').prop("disabled", false);
                                
                                $('#dp4').prop("hidden", true);
                                $('#dp5').prop("hidden", true);
                            } else if (a === "Date")
                            {
                                $('#txtdocentry').prop("disabled", true);
                                $('#selectType').prop("disabled", true);
                                $('#selectStatus').prop("disabled", true);
                                
                                $('#dp4').removeProp("hidden");
                                $('#dp5').removeProp("hidden");
                            }



                            //Chan disabled luc check chon radio
                            $('#changeradio').change(function() {
                                var a = $('input:checked').val();
                                if (a === "Docentry") {
                                    $('#txtdocentry').prop("disabled", false);
                                    $('#selectType').prop("disabled", false);
                                    $('#selectStatus').prop("disabled", true);
                                    
                                    $('#dp4').prop("hidden", true);
                                    $('#dp5').prop("hidden", true);
                                } else if (a === "Status")
                                {
                                    $('#txtdocentry').prop("disabled", true);
                                    $('#selectType').prop("disabled", true);
                                    $('#selectStatus').prop("disabled", false);
                                    
                                    $('#dp4').prop("hidden", true);
                                    $('#dp5').prop("hidden", true);
                                } else if (a === "Date")
                                {
                                    $('#txtdocentry').prop("disabled", true);
                                    $('#selectType').prop("disabled", true);
                                    $('#selectStatus').prop("disabled", true);
                                    
                                    $('#dp4').removeProp("hidden");
                                    $('#dp5').removeProp("hidden");
                                }
                            });

                            //Chan Blank
                            $('#btnSearch').click(function() {

                                var a = $('input:checked').val();

                                if (a === "Docentry") {
                                    if ($('#txtdocentry').val() === "")
                                    {
                                        $('#alert').show().find('strong').text('Docentry not blank');
                                        return false;
                                    }
                                }

                            });

                        });
                    </script>
                </form>
                
                <!-- Test Book Book   
                <br/><br/><br/><br/><br/>
                <form action="TestExportServlet" method="post">
                    <div class="form-group">
                        <label>docentry</label>
                        <input type="text" name="tempdocentry" value="040220172"/>
                    </div>
                    <div class="form-group">
                        <label>type</label>
                        <input type="text" name="temptype" value="S"/>
                    </div>
                    <div class="form-group">
                        <label>ItemCode</label>
                        <input type="text" name="tempItemCode" value="TICO1"/>
                    </div>
                    <div class="form-group">
                        <label>quantity</label>
                        <input type="text" name="tempquantity" value="4"/>
                    </div>
                    <div class="form-group">
                        <label>note</label>
                        <input type="text" name="tempnote" value="no"/>
                    </div>
                    <div class="form-group">
                        <label>user</label>
                        <input type="text" name="tempuser" value="locth"/>
                    </div>
                    <div class="form-group">
                        <input type="submit" name="action" value="TestExport"/>
                    </div>
                </form>
                End test book kho-->
                
                
                <loc:if test="${not empty requestScope.list}">
                    <h2>List Result Export</h2>
                    <div class="table-responsive" >
                        <table class="table table-bordered table-hover table-striped" >
                            <thead>
                                <tr>
                                    <th>Export ID</th>
                                    <th>Docentry</th>
                                    <th>Type</th>
                                    <th>Status</th>
                                    <th>Create Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody id="itemContainer">
                                <loc:forEach items="${requestScope.list}"  var="dto" varStatus="stt">
                                    <tr>
                                        <td>${dto.exID}</td>
                                        <td>${dto.docentry}</td>
                                        <td>${dto.updateby}</td>
                                        <td>${dto.note}</td>
                                        <td>${dto.createby}</td>
                                        <td>
                                            <form action="DetailsExportDrugsServlet" method="post">
                                                <input type="submit" value="Details" name="action"/>
                                                <input type="submit" value="Report" name="action"/>
                                                <input type="hidden" value="${dto.exID}" name="txtID"/>
                                            </form>
                                        </td>
                                    </tr>
                                </loc:forEach>
                            </tbody>
                        </table>
                    </div>
                    <!--pagination-->
                    <div class="holder" align="right"></div>
                </loc:if>

            </div>

        </div>	

        <%@include file="locGUI/locfooter.jsp" %>
    </body>
</html>
