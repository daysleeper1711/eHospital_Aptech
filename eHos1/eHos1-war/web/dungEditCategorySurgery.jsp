<%-- 
    Document   : dungEditCategorySurgery
    Created on : Jan 10, 2017, 8:19:06 PM
    Author     : Sony
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Update Category Surgery Page</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap Core CSS -->
        <link rel="stylesheet" href="css/w3.css">
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- jQuery -->
        <script src="js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>
        <style>
            /* Set black background color, white text and some padding */
            footer {
                background-color: #555;
                color: white;
                padding: 8px;
            }
            html {
                position: relative;
                min-height: 100%;
            }
            body {
                /* Margin bottom by footer height */
                margin-bottom: 20px;
            }
            footer {
                position: absolute;
                text-align: center;
                bottom: 0;
                width: 100%;
                /* Set the fixed height of the footer here */
                height: 20px;
                background-color: #f5f5f5;
            }
            .glyphicon { margin-right:10px; }
            .panel-body { padding:0px; }
            .panel-body table tr td { padding-left: 15px }
            .panel-body .table {margin-bottom: 0px; }

            header {
                background: url(images/banner.jpg) no-repeat;
                background-size: cover;
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;	
                height:170px;
            }
            .mr10 {
                margin-right: 10px;
            }
            .mr20 {
                margin-right: 20px;
            }
            .pd20 {
                padding-top: 20px;
            }
        </style>
    </head>

    <body>
    
        <%@include file="dungHeaderAdmin.jsp" %>

        <%@include file="dungAsideRightAdmin.jsp" %>
        
        <div class="row-fluid text-left">
            <div class="col-md-1 col-sm-1"></div>
            <div class="col-sm-6 col-md-6">
                <c:if test="${requestScope.msgError ne null}">
                    <label style="margin-left: 10px" class="w3-text-red">Note ! ${requestScope.msgError}</label><br/>
                </c:if>
                <form style="padding:10px" action="dungUpdateCateSurgeryServlet" method="post">
                    <div class="form-group">
                        <input class="form-control" name="txtCategoryID" type="hidden" value="${editCate.categoryID}">
                    </div>
                    <div class="form-group">
                        <label>Category Name</label>
                        <input class="form-control" name="txtCateName" value="${editCate.categoryName}">
                    </div>

                    <div class="form-group">
                        <label>Category Description</label>
                        <textarea class="form-control" rows="3" name="txtDescriptionName" >${editCate.categoryDescription}</textarea>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="cbStatus"  value="${editCate.statusCategory}" <c:if test="${editCate.statusCategory eq 'true'}">checked</c:if>>Accept
                        </label>
                    </div>
                    <div style="text-align: center">
                        <button style="margin: 10px" type="submit" class="btn btn-primary" name="action" value="Update">Update</button>
                        <button style="margin: 10px" type="submit" class="btn btn-primary" name="action" value="Cancel">Cancel</button>
                    </div>           
                </form>

                <!--IMPORTANT!!! EDIT ZONE START FROM HERE - Do not change anything else-->
                <!--I am not responsible any wrong if you change anything without permission-->
                <!--FOR THE ENGLISH TRANSLATE - GOOGLE PLZ ^_^-->
                <!--table here-->                
                <!-- end table here-->
                <!--IMPORTANT!!! EDIT ZONE END FROM HERE - Do not change anything else-->
            </div>
            <div class="col-md-1 col-sm-1"></div>
        </div>

        <footer>
            <div class="container">
                <p style="vertical-align: central" class="text-muted">&copy; Design by F21504T1 FPT Aptech<p>
            </div>
        </footer>

        <!--css for footer-->

    </body>

</html>
<!-- css for side menu -->
