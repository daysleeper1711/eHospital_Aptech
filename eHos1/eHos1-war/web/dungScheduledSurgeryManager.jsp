<%-- 
    Document   : dungScheduledSurgeryManager
    Created on : Jan 30, 2017, 2:26:21 PM
    Author     : Sony
--%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Scheduled Surgery Manager Page</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- jQuery -->
        <script src="js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>
        <style>
            /* Set black background color, white text and some padding */
            footer {
                background-color: #555;
                color: white;
                padding: 8px;
            }
            html {
                position: relative;
                min-height: 100%;
            }
            body {
                /* Margin bottom by footer height */
                margin-bottom: 20px;
            }
            footer {
                position: absolute;
                text-align: center;
                bottom: 0;
                width: 100%;
                /* Set the fixed height of the footer here */
                height: 20px;
                background-color: #f5f5f5;
            }
            .glyphicon { margin-right:10px; }
            .panel-body { padding:0px; }
            .panel-body table tr td { padding-left: 15px }
            .panel-body .table {margin-bottom: 0px; }

            header {
                background: url(images/banner.jpg) no-repeat;
                background-size: cover;
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;	
                height:170px;
            }
            .mr10 {
                margin-right: 10px;
            }
            .mr20 {
                margin-right: 20px;
            }
            .pd20 {
                padding-top: 20px;
            }
        </style>
    </head>

    <body>

        <%@include file="dungHeaderAdmin.jsp" %>

        <%@include file="dungAsideRightDoctor.jsp" %>

        <div class="row-fluid text-left">
            <div class="col-md-1 col-sm-1"></div>
            <div class="col-sm-9 col-md-9">
                <label style="margin-left: 10px;">Suggest No : ${requestScope.dtoSuggest.suggestSurgeryID}</label><br/>
                <label style="margin-left: 10px;">Surgery Service Name : ${requestScope.dtoSuggest.surgeryServiceID.surgeryServiceName}</label><br/>
                <label style="margin-left: 10px;">Medical Record : ${sessionScope.sInfo.medRecordID}</label><br/>
                <label style="margin-left: 10px;">Patient Name : ${sessionScope.sInfo.patInfo.patName}</label><br/>
                <label style="margin-left: 10px;">Sex : ${sessionScope.sInfo.patInfo.gender}</label><br/>
                <label style="margin-left: 10px;">Faculty Name : ${sessionScope.sInfo.currentFaculty}</label><br/>
                <label style="margin-left: 10px;">Bed Name : ${sessionScope.sInfo.bedInfo.bedName}</label><br/>
                <c:if test="${empty requestScope.listScheduledSurgery}">
                    <a style="margin-left: 10px; margin-bottom: 15px;" href="dungAddScheduledSurgeryServlet?txtSuggestID=${requestScope.dtoSuggest.suggestSurgeryID}">Add Scheduled Surgery</a>
                </c:if>
                <!--IMPORTANT!!! EDIT ZONE START FROM HERE - Do not change anything else-->
                <!--I am not responsible any wrong if you change anything without permission-->
                <!--FOR THE ENGLISH TRANSLATE - GOOGLE PLZ ^_^-->
                <!--table here-->
                <c:if test="${not empty requestScope.listScheduledSurgery}">
                    <div style="margin-top: 20px;margin-left: 10px" class="table-responsive">
                        <table class="table table-bordered table-hover table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Scheduled No</th>
                                    <th>Time Of Start</th>
                                    <th>Time Of End</th>
                                    <th>Status Scheduled</th>
                                    <th>Print</th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${requestScope.listScheduledSurgery}" var="dto" varStatus="stt">
                                    <tr>
                                        <td>${stt.count}</td>
                                        <td>
                                            <a href="dungEditScheduledSurgeryServlet?txtScheduledID=${dto.scheduledID}&txtSuggestID=${requestScope.dtoSuggest.suggestSurgeryID}">${dto.scheduledID}</a>
                                        </td>
                                        <td>
                                            <fmt:formatDate value="${dto.timeOfStart}" pattern="MM/dd/yyyy HH:mm"/>
                                        </td>
                                        <td>
                                            <fmt:formatDate value="${dto.timeOfEnd}" pattern="MM/dd/yyyy HH:mm"/>
                                        </td>
                                        <td>
                                            <c:if test="${dto.statusScheduled eq 'true'}">
                                                Enable
                                            </c:if>
                                            <c:if test="${dto.statusScheduled eq 'false'}">
                                                Disable
                                            </c:if>
                                        </td>
                                        <td>
                                            <a href="dungReportScheduledSurgeryServlet?txtScheduledID=${dto.scheduledID}&txtSuggestID=${requestScope.dtoSuggest.suggestSurgeryID}">Print Scheduled</a>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                    <!-- end table here-->
                </c:if>
                <!--IMPORTANT!!! EDIT ZONE END FROM HERE - Do not change anything else-->
            </div>
            <div class="col-md-1 col-sm-1"></div>
        </div>	

        <%@include file="dungFooter.jsp" %>
        <!--css for footer-->

    </body>
</html>
<!-- css for side menu -->
