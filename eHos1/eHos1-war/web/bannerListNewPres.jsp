<%-- 
    Document   : bannerListPressBookMed
    Created on : Jan 12, 2017, 6:56:04 PM
    Author     : Nguyet Phuong
--%>

<%@include file="header.jsp" %>
<div class="col-sm-9 col-md-9">
    <h1 class="page-header">
        List New Prescription<!--Change here for title of the page-->
    </h1>
</div>

</div>
<div class="row-fluid text-left">
    <div class="col-md-1 col-sm-1"></div>
    <div class="col-sm-9 col-md-9">
        <div class="row col-md-12" style="padding-top: 20px;">
            <c:if test="${requestScope.ERROR ne null}"><span style="color: red; font-size: 20px;"> ${requestScope.ERROR}</span></c:if>
            </div>
            <div class="row col-md-12">
            <c:if test="${requestScope.ERROR eq null}">
                <div class="table-responsive" style="padding-top: 20px;">
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                                <th>Patient name</th>
                                <th>Date create</th>
                                <th>Create for date</th>
                                <th>Employee Name</th>
                            </tr>
                        </thead>
                        <tbody id="itemContainer">
                            <c:forEach items="${requestScope.LISTPRESBOOK}" var="lst">
                                <tr>
                                    <td>${lst.patientName}</td>
                                    <td>${lst.date}</td>
                                    <td>${lst.cfdate}</td>
                                    <td>${lst.employName}</td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
                <div class="holder"></div>
            </c:if>
        </div>
        <!-- end table here-->
        <!--IMPORTANT!!! EDIT ZONE END FROM HERE - Do not change anything else-->
    </div>
    <div class="col-md-1 col-sm-1"></div>
</div>
<%@include file="footer.jsp" %>

