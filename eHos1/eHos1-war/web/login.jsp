<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="css/w3.css">
	<link rel="stylesheet" href="css/default.css">
	<title>Login</title>
</head>
<body>
<div class="bgimg w3-display-container w3-image">
  <div class="w3-display-middle">
      <form class="w3-container w3-card-4" style="width:350px; margin-bottom:10px" id="formLogin"
            action="LoginProcess" method="post">
		<div class="w3-container w3-left w3-text-white">
			<h1>Login</h1>
                        <c:if test="${requestScope.loginFail ne null}">
                            <p class="w3-text-red">
                                <c:out value="${requestScope.loginFail}"></c:out>
                            </p>
                        </c:if>
		</div>
		<p>
                    <input class="w3-input" type="text" style="width:90%" name="txtEmployeeID">
			<label class="w3-label w3-text-white"><strong>Name</strong></label>
		</p>
		<p>
                    <input class="w3-input" type="password" style="width:90%" name="txtPassword">
			<label class="w3-label w3-text-white"><strong>Password</strong></label>
		</p>
		<p>
                    <button class="w3-btn w3-theme-dark w3-ripple" form="formLogin" type="submit"> Submit </button>
		</p>
	</form>
  </div>
</div>


</body></html>
