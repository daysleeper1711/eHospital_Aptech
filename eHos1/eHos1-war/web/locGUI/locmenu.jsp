<!-- css for side menu -->
<style>
    .glyphicon { margin-right:10px; }
    .panel-body { padding:0px; }
    .panel-body table tr td { padding-left: 15px }
    .panel-body .table {margin-bottom: 0px; }
</style>


<div class="col-sm-3 col-md-3">
    <div class="panel-group" id="accordion">
        <div class="panel panel-default">
                           <div class="panel-heading">
                               <h4 class="panel-title">
                                   <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                       </span>Drugs and Supplies Management</a> <!--Change here for the outside menu content-->
                               </h4>
                           </div>
                           <div id="collapseOne" class="panel-collapse collapse in">
                               <div class="panel-body">
                                   <table class="table">
                                       <tr>
                                           <td>
                                               <a href="HyperlinkServlet?action=ItemCode Management">ItemCode Management</a> <!--Change here for the sub menu content-->
                                           </td>
                                       </tr>
                                       <tr>
                                           <td>
                                               <a href="HyperlinkServlet?action=Export Drugs Management">Export Drugs Management</a>
                                           </td>
                                       </tr>
                                       <tr>
                                           <td>
                                               <a href="HyperlinkServlet?action=Import Drugs Management">Import Drugs Management</a>
                                           </td>
                                       </tr>
                                       <tr>
                                           <td>
                                               <a href="HyperlinkServlet?action=Find Warehouse">Find Warehouse</a>
                                           </td>
                                       </tr>
                                       

                                   </table>
                               </div>
                           </div>
                       </div>
                       <div class="panel panel-default">
                           <div class="panel-heading">
                               <h4 class="panel-title">
                                   <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Supplier</a>
                               </h4>
                           </div>
                           <div id="collapseTwo" class="panel-collapse collapse">
                               <div class="panel-body">
                                   <table class="table">
                                       <tr>
                                           <td>
                                               <a href="HyperlinkServlet?action=Supplier Management">Supplier Management</a>
                                           </td>
                                       </tr>                                  
                                   </table>
                               </div>
                           </div>
                       </div>
                       <div class="panel panel-default">
                           <div class="panel-heading">
                               <h4 class="panel-title" >
                                   <a href="LogoutProcess" style="color: red">Logout</a>
                               </h4>
                           </div>
                       </div>
        </div>
    </div>