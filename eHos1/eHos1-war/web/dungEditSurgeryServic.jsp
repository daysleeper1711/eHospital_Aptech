<%-- 
    Document   : dungEditSurgeryServic
    Created on : Jan 11, 2017, 10:58:50 PM
    Author     : Sony
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Update Surgery Service Page</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap Core CSS -->
        <link rel="stylesheet" href="css/w3.css">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/select2.min.css" rel="stylesheet" type="text/css">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- jQuery -->
        <script src="js/jquery.js"></script>
        <script src="js/select2.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $(".js-example-basic-single").select2();
            });
        </script>
        <style>
            /* Set black background color, white text and some padding */
            footer {
                background-color: #555;
                color: white;
                padding: 8px;
            }
            html {
                position: relative;
                min-height: 100%;
            }
            body {
                /* Margin bottom by footer height */
                margin-bottom: 20px;
            }
            footer {
                position: absolute;
                text-align: center;
                bottom: 0;
                width: 100%;
                /* Set the fixed height of the footer here */
                height: 20px;
                background-color: #f5f5f5;
            }
            .glyphicon { margin-right:10px; }
            .panel-body { padding:0px; }
            .panel-body table tr td { padding-left: 15px }
            .panel-body .table {margin-bottom: 0px; }

            header {
                background: url(images/banner.jpg) no-repeat;
                background-size: cover;
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;	
                height:170px;
            }
            .mr10 {
                margin-right: 10px;
            }
            .mr20 {
                margin-right: 20px;
            }
            .pd20 {
                padding-top: 20px;
            }
        </style>
    </head>

    <body>
    
        <%@include file="dungHeaderAdmin.jsp" %>

        <%@include file="dungAsideRightAdmin.jsp" %>
        
        <div class="row-fluid text-left">
            <div class="col-md-1 col-sm-1"></div>
            <div class="col-sm-6 col-md-6">
                <c:if test="${requestScope.msgError ne null}">
                    <label style="margin-left: 10px" class="w3-text-red">${requestScope.msgError}</label><br/>
                </c:if>
                <form style="padding:10px" action="dungUpdateSurgeryServiceServlet" method="post">
                    <div class="form-group">
                        <input class="form-control" name="txtServiceID" type="hidden" value="${editService.surgeryServiceID}">
                    </div>
                    <div class="form-group">
                        <label>Surgery Service Name</label>
                        <input class="form-control" name="txtServiceName" value="${editService.surgeryServiceName}">
                    </div>
                    <div class="form-group">
                        <label>Service Description</label>
                        <textarea class="form-control" rows="2" name="txtServiceDescription">${editService.serviceDescription}</textarea>
                    </div>
                    <div class="form-group">
                        <label>Initial Cost</label>
                        <input class="form-control" name="txtInitialCost" value="${editService.initialCost}">
                    </div>
                    <div class="form-group">
                        <label>Faculty Name</label>
                        <select class="js-example-basic-single form-control" name="cbFaculty">
                            <c:forEach items="${requestScope.listtbFac}" var = "role">
                                <option value="${role.faID}" <c:if test="${role.faID eq editService.faultyID.faID}">selected</c:if>>${role.facultyName}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Category Name</label>
                        <select class="js-example-basic-single form-control" name="cbCate">
                            <c:forEach items="${requestScope.listtbCate}" var = "role">
                                <option value="${role.categoryID}" <c:if test="${role.categoryID eq editService.categoryID.categoryID}">selected</c:if>>${role.categoryName}</option>
                            </c:forEach>                                                             
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Level Name</label>
                        <select class="js-example-basic-single form-control" name="cbLevel">
                            <c:forEach items="${requestScope.listtbLevel}" var = "role">
                                <option value="${role.levelID}" <c:if test="${role.levelID eq editService.levelSurgery.levelID}">selected</c:if> >${role.levelName}</option>
                            </c:forEach>                                                                
                        </select>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="cbStatus"  value="${editService.statusService}" <c:if test="${editService.statusService eq 'true'}">checked</c:if>>Accept
                        </label>
                    </div>
                    <div style="text-align: center">
                        <button style="margin: 10px" type="submit" class="btn btn-primary" name="action" value="Update">Update</button>
                        <button style="margin: 10px" type="submit" class="btn btn-primary" name="action" value="Cancel">Cancel</button>
                    </div>      
                </form>
                     
                <!--IMPORTANT!!! EDIT ZONE START FROM HERE - Do not change anything else-->
                <!--I am not responsible any wrong if you change anything without permission-->
                <!--FOR THE ENGLISH TRANSLATE - GOOGLE PLZ ^_^-->
                <!--table here-->                
                <!-- end table here-->
                <!--IMPORTANT!!! EDIT ZONE END FROM HERE - Do not change anything else-->
            </div>
            <div class="col-md-1 col-sm-1"></div>
        </div>

        <footer>
            <div class="container">
                <p style="vertical-align: central" class="text-muted">&copy; Design by F21504T1 FPT Aptech<p>
            </div>
        </footer>

        <!--css for footer-->

    </body>

</html>
<!-- css for side menu -->
