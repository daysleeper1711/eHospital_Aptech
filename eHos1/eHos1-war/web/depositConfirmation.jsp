<%@include file="header.jsp" %>
<div class="col-sm-9 col-md-9 text-left">
    <h1 class="page-header">
        Deposit payment <small>${sessionScope.empInfo.faculty} - ${sessionScope.empInfo.position}</small>
    </h1>
</div>
</div>
<div class="row-fluid text-left">
    <div class="col-sm-8 col-md-8">
        <form style="margin: 20px" id="searchForm"
              action="DepositMainProcess" method="post">
            <!--SEARCH FORM-->
            <div class="table-responsive">
                <table>
                    <tr>
                        <td colspan="3"><strong>Search type</strong></td>
                    </tr>
                    <tr>
                        <td style="padding-right: 5px">
                            <input type="text" class="form-control" placeholder="Medical Record ID..."
                                   name="txtSearchMR">
                        </td>
                        <td>
                            <button class="btn btn-default" type="submit" form="searchForm"
                                    name="btnSubmit" value="search">
                                Search <i class="fa fa-search"></i>
                            </button>
                        </td>
                    </tr>
                </table>
            </div>
            <!--end form search...-->
        </form>
        <!--Success confirm-->
        <c:if test="${requestScope.success ne null}">
            <div class="alert alert-success">
                <strong>
                    <p>Received <i class="fa fa-usd" aria-hidden="true"></i> ${requestScope.amount}</p> 
                    <p>From ${patientInfo.patName} - patient ID: ${patientInfo.patID}</p>
                </strong> 
            </div>
        </c:if>
        <!--Fail search-->
        <c:if test="${requestScope.result eq null && requestScope.fails ne null}">
            <div class="alert alert-warning">
                <strong>Medical Record ID is incorrect !!!</strong> 
            </div>
        </c:if>
        <!--Display result-->
        <c:if test="${requestScope.result ne null}">
            <h3>Medical Record info</h3>
            <div class="table-responsive">
                <table class="showInfo">
                    <tr>
                        <th>Medical Record ID:</th>
                        <td>${requestScope.result.medRecordID}</td>
                        <th>Registry date:</th>
                        <td>${requestScope.result.createDate}</td>
                    </tr>
                </table>
            </div>
            <h3>Info details</h3>
            <div class="table-responsive">
                <table class="showInfo">
                    <tr>
                        <th>Patient ID:</th>
                        <td colspan="5">${requestScope.result.patInfo.patID}</td>
                    </tr>
                    <tr>
                        <th>Name:</th>
                        <td>${requestScope.result.patInfo.patName}</td>
                        <th>Date of birth:</th>
                        <td>${requestScope.result.patInfo.dob}</td>
                        <th>Gender:</th>
                        <td>${requestScope.result.patInfo.gender}</td>
                    </tr>
                    <c:if test="${requestScope.totalDeposit ne 0}">
                        <tr>
                            <th>Total deposit: </th>
                            <td colspan="5">${requestScope.totalDeposit}</td>
                        </tr>
                    </c:if>
                </table>
            </div>
            <!--end show...-->
            <form id="inputForm" style="width: 70%;margin-bottom: 20px;"
                  action="DepositMainProcess" method="post">
                <h3>Declaration</h3>
                <table class="showInfo">
                    <tr>
                        <th>Amount:</th>
                        <td style="padding-right: 0px">
                            <i class="fa fa-usd" aria-hidden="true"></i>
                        </td>
                        <td>
                            <input class="form-control" style="width: 100px" name="txtAmount"/>
                        </td>
                    </tr>
                </table>
                <button type="submit" form="inputForm" class="btn btn-default"
                        name="btnSubmit" value="Deposit confirm">
                    Confirm <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
                </button>
                <input type="hidden" name="passID" value="${requestScope.result.medRecordID}" />
            </form>
        </c:if>
        <!-- end table here-->
        <!--IMPORTANT!!! EDIT ZONE END FROM HERE - Do not change anything else-->
    </div>
</div>	
<%@include file="footer.jsp" %>