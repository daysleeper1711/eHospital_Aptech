<%-- 
    Document   : dungReportSurgeryInfo
    Created on : Feb 6, 2017, 6:14:05 AM
    Author     : Sony
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Consultation Surgery Report</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!-- jQuery -->
        <script src="js/jquery.js"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>
        <style>    
            /* Set black background color, white text and some padding */
            footer {
                background-color: #555;
                color: white;
                padding: 8px;
            }
            .div-table {
                display:table
            }
            .div-colunm {
                display: table-cell;
                width: 20%;
                padding: 0px 8px 0px 8px;
            }
        </style>
    </head>
    <body>
        <header>
            <div class="container-fluid" style="z-index:4;float: left">
                <div class="col-sm-12 col-md-12">
                    <img src="images/logo.jpg" class="img-responsive" style="opacity:0.6;width:120px; height:120px"/>
                </div>
            </div>
            <div class="col-sm-8 col-md-8" style="margin-right: 15px">
                <h3>eHospital</h3>
                <h4>Faculty. ${sessionScope.sInfo.currentFaculty}</h4>
                <h4>Doctor. ${sessionScope.empInfo.empName}</h4>
            </div>
        </header>
        <div class="row-fluid text-center">
            <div class="col-sm-12 col-md-12">
                <h2 class="page-header">
                    Surgery Report <!--Change here for title of the page-->
                </h2>
            </div>
        </div>
        <div class="row-fluid text-left">
            <div class="col-sm-2 col-md-2"></div>
            <div class="col-sm-8 col-md-8" style="">
                <!--information of patient-->
                <div class="col-sm-12 col-md-12" >
                    <div class = "div-table form-group">
                        <div class= "div-colunm div-col1">
                            <div class="form-group">			
                                <label>Patient Name. ${sessionScope.sInfo.patInfo.patName}</label>
                            </div>
                            <div class="form-group">			
                                <label>Gender. ${sessionScope.sInfo.patInfo.gender}</label>
                            </div>
                            <div class="form-group">			
                                <label>Address. ${sessionScope.sInfo.patInfo.address}</label>
                            </div>
                            <div class="form-group">			
                                <label>Date In. ${sessionScope.sInfo.dateInHospital}</label>
                            </div>
                        </div>
                        <div class= "div-colunm div-col2">

                            <div class="form-group">			
                                <label>Suggest No. ${requestScope.rSS.suggestSurgeryID}</label>
                            </div>
                            <div class="form-group">			
                                <label>Medical Record No. ${sessionScope.sInfo.medRecordID}</label>
                            </div>
                            <div class="form-group">			
                                <label>Faculty Name. ${sessionScope.sInfo.currentFaculty}</label>
                            </div>
                            <div class="form-group">			
                                <label>Bed Name. ${sessionScope.sInfo.bedInfo.bedName}</label>
                            </div>
                        </div>    
                    </div>
                </div>

                <div class="col-sm-12 col-md-12" >
                    <div class = "div-table form-group">
                        <div class= "div-colunm div-col1">
                            <div class="form-group">			
                                <label>Surgery Record No. ${requestScope.tbSR.surgeryRecordID}</label>
                            </div>
                            <div class="form-group">			
                                <label>Surgery Service Name. ${requestScope.rSS.surgeryServiceID.surgeryServiceName}</label>
                            </div>
                            <div class="form-group">	
                                <label>Process Of Surgery.  ${requestScope.tbSR.processOfSurgery}</label>
                            </div>
                            <div class="form-group">			
                                <label>Result of Surgery. ${requestScope.tbSR.surgeryResult}</label>
                            </div>
                        </div>
                        <div class= "div-colunm div-col2">
                            <div class="form-group">			
                                <label>Time Of Start. <fmt:formatDate value="${requestScope.tbSR.timeOfStart}" pattern="MM/dd/yyyy HH:mm"/></label>
                            </div>
                            <div class="form-group">			
                                <label>Time Of End. <fmt:formatDate value="${requestScope.tbSR.timeOfEnd}" pattern="MM/dd/yyyy HH:mm"/></label>
                            </div>
                            <div class="form-group">			
                                <label>Result of Surgery. ${requestScope.tbSR.surgeryResult}</label>
                            </div>
                            <div class="form-group">			
                                <label>Pay Cost. ${requestScope.tbSR.payCost} ($)</label>
                            </div>
                        </div>
                    </div>
                </div>
                <!--table show result of test-->
                <div class="form-group col-sm-12 col-md-12">	
                    <div class="table-responsive">
                        <c:if test="${not empty requestScope.lstDrugsDetail}">
                            <label style = "text-decoration:underline; font-weight:bold">List of Drugs Surgery Detail</label>
                            <table class="table table-bordered table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Drugs Name</th>
                                        <th>Unit</th>
                                        <th>Quantity</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <c:forEach items="${requestScope.lstDrugsDetail}" var="rs" varStatus="stt">
                                    <tr>
                                        <td>${stt.count}</td>
                                        <td>${rs.drugID.itemName}</td>
                                        <td>${rs.unit}</td>
                                        <td>${rs.quantity}</td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </c:if>
                    </div>
                    <div class="table-responsive">
                        <c:if test="${not empty requestScope.lstSuppliesDetail}">
                            <label style = "text-decoration:underline; font-weight:bold">List of Supplies Surgery Detail</label>
                            <table class="table table-bordered table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Supplies Name</th>
                                        <th>Unit</th>
                                        <th>Quantity</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <c:forEach items="${requestScope.lstSuppliesDetail}" var="rs" varStatus="stt">
                                    <tr>
                                        <td>${stt.count}</td>
                                        <td>${rs.suppliesID.itemName}</td>
                                        <td>${rs.unit}</td>
                                        <td>${rs.quantity}</td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </c:if>
                    </div>
                    <div class="table-responsive">
                        <c:if test="${not empty requestScope.lstOperatorDoctor}">
                            <label style = "text-decoration:underline; font-weight:bold">List of Operator Doctor Detail</label>
                            <table class="table table-bordered table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Doctor Name</th>
                                        <th>Position Name</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <c:forEach items="${requestScope.lstOperatorDoctor}" var="rs" varStatus="stt">
                                    <tr>
                                        <td>${stt.count}</td>
                                        <td>${rs.doctorID.fullname}</td>
                                        <td>${rs.positionID.positionName}</td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </c:if>
                    </div>
                    <div class = "div-table form-group" style="margin-left: 70%">
                        <div class="form-group"  style="margin-bottom: 100px; ">
                            <Label><fmt:formatDate value="${requestScope.tbSR.createDate}" type="date" dateStyle="long" timeStyle="long"/></label><br/>
                            <label style="margin-left: 25px">Doctor</label>
                        </div>
                        <div class="form-group">
                            <label>${sessionScope.empInfo.empName}</label>
                        </div>
                    </div>

                </div>
            </div>	
            <!--css for footer-->
            <style>
                html {
                    position: relative;
                    min-height: 100%;
                }
                body {
                    /* Margin bottom by footer height */
                    margin-bottom: 20px;
                }
                footer {
                    position: absolute;
                    bottom: 0;
                    width: 100%;
                    /* Set the fixed height of the footer here */
                    height: 20px;
                    background-color: #f5f5f5;
                }
            </style>
            <!-- css header
            ================================================== -->
            <style>
                header {
                    background: url(images/banner.jpg) no-repeat;
                    background-size: cover;
                    -webkit-background-size: cover;
                    -moz-background-size: cover;
                    -o-background-size: cover;	
                    height:130px;
                }
                .mr10 {
                    margin-right: 10px;
                }
                .mr20 {
                    margin-right: 20px;
                }
                .pd20 {
                    padding-top: 20px;
                }
            </style>
            <!-- css header
            ================================================== -->
    </body>
</html> 
