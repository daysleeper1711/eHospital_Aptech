<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="loc" %>
<!DOCTYPE html>

<html >
    <head>
        <title>Find Warehouse</title>
        <meta charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- jQuery -->
        <script language="javascript" src="js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>
        <style>    
            /* Set black background color, white text and some padding */
            footer {
                background-color: #555;
                color: white;
                padding: 8px;
            }

            .loccolum{
                display: table-cell;
                padding: 0px 8px 0px 0px;

            }
            .loccenter{
                display: table;
                padding: 0px 0px 0px 20px;
            }


        </style>

        <!--pagination-->
        <script type="text/javascript" src="js/jPager/js.js"></script>
        <script type="text/javascript" src="js/jPager/jPages.min.js"></script>
        <link rel="stylesheet" href="css/jPager/jPages.css" type="text/css"/>

        <script>
//            run when document is ready
            $(function() {
//                initiate plugin
                $("div.holder").jPages({
                    containerID: "itemContainer",
                    perPage: 5
                });
            });
        </script>

    </head>
    <body>
        <%@include file="locGUI/locheader.jsp" %>

        <div class="row-fluid text-center">
            <%@include file="locGUI/locmenu.jsp" %>
            <div class="col-sm-9 col-md-9">
                <h1 class="page-header">
                    <small>Find Warehouse</small> <!--Change here for title of the page-->
                </h1>
            </div>

        </div>
        <div class="row-fluid text-left">
            <div class="col-md-1 col-sm-1"></div>
            <div class="col-sm-6 col-md-6">
                <form style="padding:20px" action="FindWarehouseServlet" method="post">
                    <!--IMPORTANT!!! EDIT ZONE START FROM HERE - Do not change anything else-->
                    <!--I am not responsible any wrong if you change anything without permission-->
                    <!--FOR THE ENGLISH TRANSLATE - GOOGLE PLZ ^_^-->

                    <!--copy form input here...-->
                    <!--input 1-->

                    <div class="form-group input-group">
                        <input type="text" class="form-control" id="txtsearch" name="txtsearch"/>
                        <span class="input-group-btn"><button class="btn btn-default" type="submit" id="btnSearch" name="action" value="btnSearch"><i class="fa fa-search"></i></button></span>
                    </div>
                    <p style="color: red" id="error"></p>
                    <div class="form-group">
                        <label>Type</label>
                        <select class="form-control" id="select" name="select">
                            <option>All</option>
                            <option>Drugs</option>
                            <option>Supplies</option>
                        </select>

                    </div>

                    <div>
                        <div class="form-group" id="changeradio">
                            <label>Search ... </label>
                            <label class="radio-inline">
                                <input type="radio" name="optionsRadiosInline" id="radio" value="ItemCode" checked> ItemCode
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="optionsRadiosInline" id="radio" value="ItemName"> ItemName
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="optionsRadiosInline" id="radio" value="Package"> Package
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="optionsRadiosInline" id="radio" value="GenericDrug"> GenericDrug
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="optionsRadiosInline" id="radio" value="Type"> Type
                            </label>
                        </div>
                    </div>

                    <script language="javascript">
                        $(document).ready(function()
                        {
                            //Begin Check chan disabled luc dau onLoad 
                            var a = $('input:checked').val();
                            if (a === "Type") {
                                $('#select').prop("disabled", false);
                                $('#txtsearch').prop("disabled", true);
                            }
                            else {
                                $('#select').prop("disabled", true);
                                $('#txtsearch').prop("disabled", false);
                            }
                            //Chan disabled click chon radio
                            $('#changeradio').change(function() {
                                var a = $('input:checked').val();
                                if (a === "Type") {
                                    $('#select').prop("disabled", false);
                                    $('#txtsearch').prop("disabled", true);
                                }
                                else {
                                    $('#select').prop("disabled", true);
                                    $('#txtsearch').prop("disabled", false);
                                }
                            });
                            //Chan disabled click chon radio

                            //Begin Check chan nut Search Not Blank
                            $('#btnSearch').click(function()
                            {

                                var checkblank = $('#txtsearch').val().trim();
                                var a = $('input:checked').val();
                                if (a !== "Type") {
                                    if (checkblank === "") {
                                        $('#error').text(a + " not blank");
                                        return false;
                                    }
                                }
                            });
                            //End Check chan nut Search Not Blank
                        });
                    </script>
                </form>
                <loc:if test="${not empty requestScope.list}">
                    <form>
                        <h2>List Result </h2>
                        <div class="table-responsive" >
                            <table class="table table-bordered table-hover table-striped" >
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Itemcode</th>
                                        <th>Item Name</th>
                                        <th>Package</th>
                                        <th>OnHand</th>
                                        <th>Quantity Available</th>                                   
                                        <th>Quantity Order</th>
                                        <th>War_Date</th>
                                    </tr>
                                </thead>
                                <tbody id="itemContainer">
                                    <loc:forEach items="${requestScope.list}"  var="dto" varStatus="stt">
                                        <tr>
                                            <td>${stt.count}</td>
                                            <td>${dto.itemcode}</td>
                                            <td>${dto.itemname}</td>
                                            <td>${dto.package1}</td>
                                            <td>${dto.onHand}</td>
                                            <td>${dto.qtyAvailable}</td>
                                            <td>${dto.qtyOrder}</td>
                                            <td>${dto.warDate}</td>
                                        </tr>
                                    </loc:forEach>
                                </tbody>
                            </table>
                        </div>
                        <!--pagination-->
                        <div class="holder" align="right"></div>
                    </form>
                </loc:if>
            </div>
            <div class="col-md-1 col-sm-1"></div>
        </div>	

        <%@include file="locGUI/locfooter.jsp" %>
    </body>
</html>
