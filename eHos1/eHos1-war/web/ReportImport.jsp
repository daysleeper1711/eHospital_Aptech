<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="loc" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Report Import</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!-- jQuery -->
        <script src="js/jquery.js"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>
        <style>
            html {
                position: relative;
                min-height: 100%;
            }
            body {
                /* Margin bottom by footer height */
                margin-bottom: 10px;
            }
            footer {
                position: absolute;
                bottom: 0;
                width: 100%;
                /* Set the fixed height of the footer here */
                height: 10px;
                background-color: #f5f5f5;
            }
            header {
                background: url(images/banner.jpg) no-repeat;
                background-size: cover;
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;	
                height:130px;
            }
            /*Borderless*/
            .showInfo td {
                padding-top: 10px;
                padding-left: 20px;
                padding-right: 30px;
                padding-bottom: 10px;
            }
        </style>
    </head>
    <body>
        <!--Header logo and user-->
        <div class="row-fluid text-center">
            <div class="col-sm-3 col-md-3"></div>
            <div class="col-sm-8 col-md-8">
                <div class="col-sm-3 col-md-3">
                    <img src="images/logo.jpg" class="img-responsive" style="opacity:0.6;width:100px"/>
                </div>
                <div class="col-sm-5 col-md-5">
                    <h1 class="page-header">
                        <p>
                            REPORT IMPORT
                        </p>
                    </h1>

                </div>
            </div>
            <div class="row-fluid text-left">
                <div class="col-sm-2 col-md-2 col-xs-2"></div>
                <div class="col-sm-8 col-md-8 col-xs-8">
                    <form style="padding:20px">
                        <!--IMPORTANT!!! EDIT ZONE START FROM HERE - Do not change anything else-->
                        <!--I am not responsible any wrong if you change anything without permission-->
                        <!--FOR THE ENGLISH TRANSLATE - GOOGLE PLZ ^_^-->

                        <!--copy form input here...-->
                        <!--input 1-->

                        <table style="width: 100%;" border="true">
                            <tr>
                                <td colspan="2" style="padding-left: 5px">
                                    <b>Supplier Name:</b> ${requestScope.SupName}

                                </td>

                                <td style="padding-left: 5px">

                                    <b>Status:</b> ${requestScope.header_details.header.UStatus}
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-left: 5px">
                                    <b>SymBill:</b> ${requestScope.header_details.header.symBill}
                                </td>
                                <td style="padding-left: 5px">
                                    <b>NumBill:</b> ${requestScope.header_details.header.numBill}
                                </td>
                                <td style="padding-left: 5px">
                                    <b>Create Date:</b> ${requestScope.sfromdate}

                                </td>
                            </tr>
                            <tr>
                                <td style="padding-left: 5px">
                                    <b>Package:</b> ${requestScope.header_details.header.package1}
                                </td>
                                <td style="padding-left: 5px">
                                    <b>Update by:</b> ${requestScope.header_details.header.updateby}

                                </td>
                                <td style="padding-left: 5px">
                                    <b>Update Date:</b> ${requestScope.stodate}
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" style="padding: 0 5px 5px 5px">
                                    <b>Note:</b><textarea style="padding-left: 10px" class="form-control" rows="3" id="disabledInput"  disabled="true">${requestScope.header_details.header.note}</textarea>
                                </td>
                            </tr>
                        </table>


                        <h2>Import Drugs Details</h2>
                        <div class="table-responsive" >
                            <table class="table table-bordered table-hover table-striped" >
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>ItemCode</th>
                                        <th>ItemName</th>
                                        <th>War_Date</th>
                                        <th>Quantity</th>
                                        <th>N_Price</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <loc:forEach items="${requestScope.listdetails}"  var="dto" varStatus="stt">

                                        <tr>
                                            <td>${stt.count}</td>
                                            <td>${dto.itemCode}</td>
                                            <td>
                                                <loc:forEach items="${requestScope.listITEMCODE}"  var="dto1">
                                                    <loc:if test="${dto1.itemCode eq dto.itemCode}">${dto1.itemName}</loc:if>
                                                </loc:forEach>
                                            </td>
                                            <td>${dto.imDetails}</td>
                                            <td>${dto.quantity}</td>
                                            <td>${dto.NPrice}</td>
                                        </tr>

                                    </loc:forEach>
                                </tbody>
                            </table>
                        </div>

                    </form>
                </div>
                <!-- end table here-->
                <!--IMPORTANT!!! EDIT ZONE END FROM HERE - Do not change anything else-->
            </div>

    </body>
</html>