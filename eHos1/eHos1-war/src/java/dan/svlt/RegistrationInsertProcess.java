/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dan.svlt;

import dan.dto.EmpInfo;
import dan.ssb.DanSessionBean;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import project.ett.TbPatient;

/**
 *
 * @author DaySLeePer
 */
public class RegistrationInsertProcess extends HttpServlet {
    @EJB
    private DanSessionBean danSSB;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String destination = "DanTest/fail.jsp"; //this is for developing
        try {
            /*To do code here - remember to set the destination!!!*/
            //---get Session empInfo
            HttpSession session = request.getSession();
            EmpInfo employeeInfo = (EmpInfo) session.getAttribute("empInfo");
            //--- get employee ID
            String employeeID = employeeInfo.getEmployeeID();
            //---get patient ID
            String patID = request.getParameter("passID");
            //---get input symptoms
            String symptoms = request.getParameter("txtSymptoms");
            //---get faculty to go
            String facultyToGo = request.getParameter("cbbFacultyToGo");
            //--- generate medical record ID
            String medRecordID = danSSB.generateMedRecordID();
            //--- get value of submit button
            String btnSubmit = request.getParameter("btnSubmit");
            //--- insert patient when button submit get value "create"
            if (btnSubmit.equals("create")) {
                //--- generate pat ID
                String newPatID = danSSB.generatePatID();
                String patName = request.getParameter("txtPatientName");
                String strDOB = request.getParameter("txtDob");
                //--- convert strDOB to Date dob
                Date dob = new SimpleDateFormat("dd/MM/yyyy").parse(strDOB);
                //--- convert gender
                String strGender = request.getParameter("radGender");
                boolean gender = false;
                if (strGender.equals("Female")) {
                    gender = true;
                }
                String iDCard = request.getParameter("txtIDCard");
                String address = request.getParameter("txtAddress");
                String phone = request.getParameter("txtPhone");
                //--- Set value to entity patient
                TbPatient insPat = new TbPatient(newPatID);
                insPat.setPatName(patName);
                insPat.setDob(dob);
                insPat.setGender(gender);
                insPat.setIDCard(iDCard);
                insPat.setAddress(address);
                insPat.setPhone(phone);
                //--- insert patient
                if (danSSB.insertPatient(insPat, employeeID)) {
                    request.setAttribute("insPatID", newPatID);
                }
                patID = newPatID;
            }
            if (danSSB.insertMR(patID, medRecordID, symptoms, employeeID)) {
                if (danSSB.inHouse(facultyToGo, medRecordID)) {
                    request.setAttribute("insMedRecordID", medRecordID);
                    destination = "inpatientRegistration.jsp";
                }
            }
            //--- get facultyID from facultyName
            String facultyID = danSSB.searchFacultyIDByName(facultyToGo);
            //--- check the faculty is full or not
            if (!danSSB.checkFreeBedInFaculty(facultyID)) {
                request.setAttribute("facultyToGo", facultyToGo);
                request.setAttribute("fullFaculty", true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            request.getRequestDispatcher(destination).forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
