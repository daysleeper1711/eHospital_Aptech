/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dan.svlt;

import dan.dto.PatientInfo;
import dan.ssb.DanSessionBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author DaySLeePer
 */
public class RegistrationSearchProcess extends HttpServlet {
    @EJB
    private DanSessionBean danSSB;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String destination = "DanTest/fail.jsp"; //this is for developing
        try {
            /*To do code here - remember to set the destination!!!*/
            String searchType = request.getParameter("cbbSearchType");
            String searchData = request.getParameter("txtSearchText");
            //--- create List of result
            List<PatientInfo> resultList = new ArrayList<>();
            if (searchType.equals("sID_Card")) {
                //--- search by ID card
                PatientInfo info = danSSB.searchByIDCard(searchData);
                //--- if find the info, add to resultList
                if (info != null) {
                    resultList.add(info);
                }
            } else if (searchType.equals("sPatientName")) {
                //--- search by patient name
                resultList = danSSB.searchByName(searchData);
            } else if (searchType.equals("sPatientID")) {
                //--- search by patient ID
                PatientInfo info = danSSB.searchByPatID(searchData);
                //--- if find the result, add to resultList
                if (info != null) {
                    resultList.add(info);
                }
            } else {
                //--- other redirect to wrong page
                request.getRequestDispatcher(destination).forward(request, response);
            }
            //--- set list patient is already registered
            List<PatientInfo> registeredList = new ArrayList<>();
            //--- out the patient has already registered
            Iterator<PatientInfo> iter = resultList.iterator();
            while (iter.hasNext()) {
                PatientInfo patient = iter.next();
                if (danSSB.checkPatientIsRegistered(patient.getPatID())) {
                    registeredList.add(patient);
                    iter.remove();
                }
            }//end iterator
            request.setAttribute("registeredList", registeredList);
            request.setAttribute("resultList", resultList);
            destination = "inpatientRegistration.jsp";
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            request.getRequestDispatcher(destination).forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
