/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dan.svlt;

import dan.dto.MedicalRecordInfo;
import dan.dto.TreatmentFee;
import dan.ssb.DanSessionBean;
import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author DaySLeePer
 */
public class DischargeFeeDetaisProcess extends HttpServlet {
    @EJB
    private DanSessionBean danSSB;
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String destination = "test/fail.jsp"; //this is for developing
        try {
            /*To do code here - remember to set the destination!!!*/
            //--- get medical record ID
            String medRecordID = request.getParameter("passID");
            //--- get medical record info
            MedicalRecordInfo mrInfo = danSSB.transToMRView(medRecordID);
            request.setAttribute("medInfo", mrInfo);
            //--- get the TreatmentFee
            TreatmentFee tFee = danSSB.feeDetails(medRecordID);
            request.setAttribute("tFee", tFee);
            //--- catch there is no test request
            if (tFee.getTestFee().getLstTestService().isEmpty()) {
                request.setAttribute("noSubclinicalTest", true);
            }
            //--- catch there is no daily drugs
            if (tFee.getDailyDrugFee().getLstDrugService().isEmpty()) {
                request.setAttribute("noDailyDrugs", true);
            }
            //--- catch there is no daily drugs supplies
            if (tFee.getDailyDrugFee().getLstDrugSuppliesService().isEmpty()) {
                request.setAttribute("noDailySuppliesDrugs", true);
            }
            //--- catcg there is no surgery
            if (tFee.getSurgeryFee().getLstSurgeryService().isEmpty()) {
                request.setAttribute("noSurgery", true);
            }
            destination = "dischargeFeePaymentDetail.jsp";
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            request.getRequestDispatcher(destination).forward(request, response);
        }
     }
     

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
