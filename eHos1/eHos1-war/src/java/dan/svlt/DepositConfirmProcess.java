/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dan.svlt;

import dan.dto.EmpInfo;
import dan.ssb.DanSessionBean;
import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author DaySLeePer
 */
public class DepositConfirmProcess extends HttpServlet {
    @EJB
    private DanSessionBean danSSB;
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String destination = "DanTest/fail.jsp"; //this is for developing
        try {
            /*To do code here - remember to set the destination!!!*/
            //--- get amount
            double amount = Double.parseDouble(request.getParameter("txtAmount"));
            //--- get medical record ID
            String medRecordID = request.getParameter("passID");
            //--- get employeeID
            HttpSession session = request.getSession();
            EmpInfo emp = (EmpInfo) session.getAttribute("empInfo");
            String employeeID = emp.getEmployeeID();
            //--- confirm payment
            if (danSSB.depositConfirm(medRecordID, amount, employeeID)) {
                request.setAttribute("success", true);
                request.setAttribute("amount", amount);
                request.setAttribute("patientInfo", danSSB.patient_MR(medRecordID));
            }
            destination = "depositConfirmation.jsp";
            
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            request.getRequestDispatcher(destination).forward(request, response);
        }
     }
     

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
