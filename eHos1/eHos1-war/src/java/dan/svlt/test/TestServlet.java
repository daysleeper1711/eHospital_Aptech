/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dan.svlt.test;

import dan.dto.MedicalRecordInfo;
import dan.dto.PatientInfo;
import dan.dto.TreatmentFee;
import dan.service.BedService;
import dan.service.TestDTO;
import dan.service.TestService;
import dan.ssb.DanSessionBean;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author DaySLeePer
 */
public class TestServlet extends HttpServlet {

    @EJB
    private DanSessionBean danSSB;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String destination = "DanTest/fail.jsp"; //this is for developing
        try {
            /*To do code here - remember to set the destination!!!*/
//            String id = danSSB.generatePatID();
//            String medID = danSSB.generateMedRecordID();
//                    
//            request.setAttribute("patID", id);
//            request.setAttribute("medID", medID);
//            PatientInfo patInfo = danSSB.patient_MR("MR-03012017-1");
//            request.setAttribute("patInfo", patInfo);
//            
//            List<MedicalRecordInfo> resultList1 = danSSB.diagnosisMR();
//            request.setAttribute("resultList1", resultList1);
//            request.setAttribute("facultyToGo",danSSB.facultyToGo("MR-05012017-6"));
//            request.setAttribute("currentFaculty",danSSB.currentFaculty("MR-05012017-6"));
//            boolean registered = danSSB.checkPatientIsRegistered("PAT-01012017-1"); //-- false
//            System.out.println("PAT-01012017-1 is registered - " + registered);
//            registered = danSSB.checkPatientIsRegistered("PAT-02012017-4"); //-- true
//            System.out.println("PAT-02012017-4 is registered - " + registered);

            //--- test query max
//            List<Date> listMaxDate = danSSB.maxDateInFCH("MR-04012017-3");
//            for (Date date : listMaxDate) {
//                System.out.println("Date: " + new SimpleDateFormat("dd/MM/yyyy").format(date));
//            }
//            request.setAttribute("lstMaxDate", listMaxDate);
//            boolean checkChangeFaculty = danSSB.checkChangeFacultyEvent("MR-04012017-3");
//            System.out.println("Change faculty: " + checkChangeFaculty);
//            String currentFaculty = danSSB.currentFaculty("MR-09012017-Test");
//            System.out.println("Current Faculty: " + currentFaculty);
//            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
//            Calendar cal1 = Calendar.getInstance();
//            cal1.setTime(sdf.parse("17/10/2017"));
//            Calendar cal2 = Calendar.getInstance();
//            cal2.setTime(sdf.parse("17/12/2017"));
//            int days = (int)((cal2.getTimeInMillis() - cal1.getTimeInMillis())/(1000*60*60*24));
//            System.out.println("Days: " + days);
//            boolean checkFreeBedInNEURO = danSSB.checkFreeBedInFaculty("CARDIO");
//            if (checkFreeBedInNEURO) {
//                System.out.println("CARDIO is not full");
//            } else {
//                System.out.println("CARDIO is full");
//            }
            //--- test bed fee
//            BedService bedFee = danSSB.bedFeeDetail("MR-09012017-12");
//            System.out.println("Price per night: " + bedFee.getPricePerDay());
//            System.out.println("Total days: " + bedFee.getTotalDays());
//            System.out.println("Total: " + bedFee.getTotal());
//            List<TestService> lstTestService = danSSB.lstTestFeeDetail("MR-10012017-13");
//            for (TestService ts : lstTestService) {
//                System.out.println("---------------------------");
//                System.out.println(ts.toString());
//                System.out.println("---------------------------");
//            }
            
//            TreatmentFee tf = danSSB.feeDetails("MR-10012017-15");
//            System.out.println("----Treatment fee detail----");
//            //--- get bed fee
//            BedService bedFee = tf.getBedFee();
//            System.out.println("***BED FEE***");
//            System.out.println(bedFee.toString());
//            System.out.println("-----------------------------");
//            //--- get test fee
//            TestDTO testFee = tf.getTestFee();
//            System.out.println("***TEST FEE***");
//            System.out.println(testFee.toString());
//            System.out.println("--Details--");
//            List<TestService> testFeeDetail = testFee.getLstTestService();
//            for (TestService ts : testFeeDetail) {
//                System.out.println(ts.toString());
//                System.out.println("------------------------");
//            }
//            System.out.println("***CONCLUSION***");
//            System.out.println(tf.toString());
            destination = "DanTest/test.jsp";
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            request.getRequestDispatcher(destination).forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
