/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phuong.editMedSupp;

import dan.dto.EmpInfo;
import dan.ssb.DanSessionBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import phuong.dto.EditSuppDTO;
import phuong.dto.lstMedicineDTO;
import phuong.ssb.PhuongSessionBean;
import project.ett.TbItemCodeManagement;
import project.ett.TbMedicineSuppliesDetail;
import project.locssbean.LocSessionBean;

/**
 *
 * @author Nguyet Phuong
 */
public class UpdateSuppController extends HttpServlet {

    @EJB
    private DanSessionBean danSSB;
    @EJB
    private LocSessionBean locSessionBean;

    @EJB
    private PhuongSessionBean phuongSessionBean;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            String url = "phuongWeb/Fail.jsp";
            HttpSession session = request.getSession();
            EmpInfo emp = (EmpInfo) session.getAttribute("empInfo");
            String empID = emp.getEmployeeID();

            int quantity = 0;
            String suppID = request.getParameter("txtSuppID");
            String suppDetaID = request.getParameter("txtSuppDetaID");
            String txtQ = request.getParameter("txtQ");
            String txtMedID = request.getParameter("txtMedID");
            boolean fla = false;
            if (txtQ.equals("")) {
                fla = true;
                String er = "Sorry, quantity of medicine equal 0, plz input quanity!!!";
                request.setAttribute("ERROR", er);
                url = "EditMedSupplies.jsp";
            } else {
                if (txtQ.length() < 9) {
                    quantity = Integer.parseInt(txtQ);
                } else {
                    quantity = 0;
                    fla = true;
                    String er = "Sorry, quantity must be less than 10!!";
                    request.setAttribute("ERROR", er);
                    url = "EditMedSupplies.jsp";
                }
            }
            if (quantity <= 0) {
                fla = true;
                String er = "Sorry, quantity of medicine equal 0, plz input quanity!!!";
                request.setAttribute("ERROR", er);
                url = "EditMedSupplies.jsp";
            }
            if (quantity > 10) {
                fla = true;
                String er = "Sorry, quantity of must be least than 10, plz re-input quanity!!!";
                request.setAttribute("ERROR", er);
                url = "EditMedSupplies.jsp";
            }
            if (!fla) {
                TbMedicineSuppliesDetail suppcheck = phuongSessionBean.suppDetailBySuppDetaID(suppDetaID);
                int q = quantity - suppcheck.getQuantity();
                if (locSessionBean.xuLyExportWarehouse(suppID, "P", txtMedID, quantity, "", empID)) {
                    boolean ch = true;
                    if (q > 0) {
                        TbItemCodeManagement thuoc = locSessionBean.findbyItemCode(txtMedID);
                        if (!danSSB.checkDepositPayment(q * thuoc.getXPrice(), suppcheck.getMedicineSuppliesID().getMedicalRecordID().getMedRecordID())) {
                            ch = false;
                            String er = "Deposit not enough, plz add deposit!!";
                            request.setAttribute("ERROR", er);
                            locSessionBean.xuLyExportWarehouse(suppID, "P", txtMedID, suppcheck.getQuantity(), "", empID);
                        }
                    } else {
                        danSSB.checkDepositPayment(q * suppcheck.getPrice(), suppcheck.getMedicineSuppliesID().getMedicalRecordID().getMedRecordID());
                    }
                    if (ch) {
                        phuongSessionBean.updateMedSuppDetaQuantity(suppID, suppDetaID, quantity, empID);
                    }
                } else {
                    String er = "Sorry, but this medicine's quantity don't enough!!!";
                    request.setAttribute("ERROR", er);
                }
            }
            SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");
            EditSuppDTO edSuppDto = phuongSessionBean.editSupp(suppID);
            if (edSuppDto != null) {
                String createfordate = formatDate.format(edSuppDto.getSupp().getCreateForDate());
                List<lstMedicineDTO> lstMedicine = phuongSessionBean.showAllMedicineSupp();
                request.setAttribute("LISTMED", lstMedicine);
                request.setAttribute("DATE", createfordate);
                request.setAttribute("EDITSUPP", edSuppDto);
                url = "EditMedSupplies.jsp";
            }
            request.getRequestDispatcher(url).forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
