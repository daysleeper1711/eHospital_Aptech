/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phuong.editMedSupp;

import dan.dto.EmpInfo;
import dan.ssb.DanSessionBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import phuong.dto.EditSuppDTO;
import phuong.dto.lstMedicineDTO;
import phuong.ssb.PhuongSessionBean;
import project.ett.TbMedicineSuppliesDetail;
import project.ett.TbPrescriptionDetail;
import project.locssbean.LocSessionBean;

/**
 *
 * @author Nguyet Phuong
 */
public class DeleteMedOfSuppController extends HttpServlet {
    @EJB
    private DanSessionBean danSSB;
    @EJB
    private LocSessionBean locSessionBean;

    @EJB
    private PhuongSessionBean phuongSessionBean;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            String url = "phuongWeb/Fail.jsp";
            HttpSession session = request.getSession();
            EmpInfo emp = (EmpInfo) session.getAttribute("empInfo");
            String empID = emp.getEmployeeID();
            String suppID = request.getParameter("txtSuppID");
            String suppDetaID = request.getParameter("txtSuppDetaID");
            String txtMedID = request.getParameter("txtMedID");
            if (locSessionBean.xuLyExportWarehouse(suppID, "P", txtMedID, 0, "", empID)) {
                TbMedicineSuppliesDetail suppDeta = phuongSessionBean.suppDetailBySuppDetaID(suppDetaID);
                System.out.println("Refung "+suppDeta.getPrice()*suppDeta.getQuantity());
                danSSB.refundDeposit(suppDeta.getPrice()*suppDeta.getQuantity(), suppDeta.getMedicineSuppliesID().getMedicalRecordID().getMedRecordID());
                phuongSessionBean.deleteMedSuppDeta(suppDetaID, suppID);
                phuongSessionBean.updateMedSupplies(suppID, empID);
            } else {
                String er = "Sorry, but this medicine's quantity don't enough!!!";
                request.setAttribute("ERROR", er);
            }
            SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");
            EditSuppDTO edSuppDto = phuongSessionBean.editSupp(suppID);
            if (edSuppDto != null) {
                String createfordate = formatDate.format(edSuppDto.getSupp().getCreateForDate());
                List<lstMedicineDTO> lstMedicine = phuongSessionBean.showAllMedicineSupp();
                request.setAttribute("LISTMED", lstMedicine);
                request.setAttribute("DATE", createfordate);
                request.setAttribute("EDITSUPP", edSuppDto);
                url = "EditMedSupplies.jsp";
            }
            request.getRequestDispatcher(url).forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
