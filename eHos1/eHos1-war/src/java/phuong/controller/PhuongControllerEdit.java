/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phuong.controller;

import dan.dto.EmpInfo;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import phuong.dto.lstMedicineDTO;
import phuong.ssb.PhuongSessionBean;

/**
 *
 * @author Nguyet Phuong
 */
public class PhuongControllerEdit extends HttpServlet {

    @EJB
    private PhuongSessionBean phuongSessionBean;
    private static final String fail = "phuongWeb/Fail.jsp";
    private static final String phuongUpdatePreController = "PhuongUpdatePrescription";
    private static final String insertPresController = "InsertPresController";
    private static final String deletePresController = "DeleteMedOfPresController";
    private static final String updatePresController = "UpdatePresController";
    private static final String insertSuppController = "InsertSuppController";
    private static final String deleteSuppController = "DeleteMedOfSuppController";
    private static final String updateSuppController = "UpdateSuppController";
    private static final String listAllPrescriptionMedSupp = "listAllPrescriptionMedSupp";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            String action = request.getParameter("action");
            String url = fail;
            if (action.equals("Update Prescription")) {
                url = phuongUpdatePreController;
            } else if (action.equals("InsertPres")) {
                List<lstMedicineDTO> lstMedicine = phuongSessionBean.showAllMedicinePres();
                request.setAttribute("LISTMED", lstMedicine);
                url = insertPresController;
            } else if (action.equals("UpdatePres")) {
                List<lstMedicineDTO> lstMedicine = phuongSessionBean.showAllMedicinePres();
                request.setAttribute("LISTMED", lstMedicine);
                url = updatePresController;
            } else if (action.equals("DeletePres")) {
                List<lstMedicineDTO> lstMedicine = phuongSessionBean.showAllMedicinePres();
                request.setAttribute("LISTMED", lstMedicine);
                url = deletePresController;
            } else if (action.equals("InsertSupp")) {
                List<lstMedicineDTO> lstMedicine = phuongSessionBean.showAllMedicineSupp();
                request.setAttribute("LISTMED", lstMedicine);
                url = insertSuppController;
            } else if (action.equals("UpdateSupp")) {
                List<lstMedicineDTO> lstMedicine = phuongSessionBean.showAllMedicineSupp();
                request.setAttribute("LISTMED", lstMedicine);
                url = updateSuppController;
            } else if (action.equals("DeleteSupp")) {
                List<lstMedicineDTO> lstMedicine = phuongSessionBean.showAllMedicineSupp();
                request.setAttribute("LISTMED", lstMedicine);
                url = deleteSuppController;
            } else if (action.equals("UpdateMedicineSupplies")) {
                HttpSession session = request.getSession();
                EmpInfo emp = (EmpInfo) session.getAttribute("empInfo");
                if (emp.getPosition().equals("admin")) {
                    url = "MedSuppliesManagement";
                } else {
                    url = listAllPrescriptionMedSupp;
                }
                
            } else if (action.equals("Ok")) {
                HttpSession session = request.getSession();
                EmpInfo emp = (EmpInfo) session.getAttribute("empInfo");
                String empID = emp.getEmployeeID();
                String presID = request.getParameter("txtPresID");
                String diagnose = request.getParameter("txtDiagnose");
                phuongSessionBean.updatePresDignose(presID, empID, diagnose);
                phuongSessionBean.updatePrescription(presID, empID);
                if (emp.getPosition().equals("admin")) {
                    url = "PrescriptionManagement";
                } else {
                    url = listAllPrescriptionMedSupp;
                }
            }
            request.getRequestDispatcher(url).forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
