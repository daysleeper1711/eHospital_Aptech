/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phuong.controller;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import phuong.dto.lstMedicineDTO;
import phuong.ssb.PhuongSessionBean;
/**
 *
 * @author Nguyet Phuong
 */
public class PhuongControllerAdd extends HttpServlet {
    @EJB
    private PhuongSessionBean phuongSessionBean;
    private static final String addPrePage = "AddPrescription.jsp";
    private static final String addMedSuppPage = "AddMedicineSupplies.jsp";    
    private static final String fail = "phuongWeb/Fail.jsp";
    private static final String addPreController = "PhuongAddPrescription";
    private static final String insertMedPresController = "InsertMedPresController";
    private static final String deleteMedPresController = "DeleteMedPresController";
    private static final String updateMedPresController = "UpdateMedPresController";
    private static final String addSuppController = "PhuongAddMedSuppController";
    private static final String insertMedSuppController = "InsertMedSuppController";
    private static final String deleteMedSuppController = "DeleteMedSuppController";
    private static final String updateMedSuppController = "UpdateMedSuppController";
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            String action = request.getParameter("action");
            String url = fail;
            if (action.equals("AddNewPrescription")) {
                List<lstMedicineDTO> lstMedicine = phuongSessionBean.showAllMedicinePres();
                request.setAttribute("LISTMED", lstMedicine);
                url = addPrePage;
            } else if (action.equals("Insert Prescription")) {
                url = addPreController;
            }else if (action.equals("InsertPres")) {
                System.out.println("AAAAAAAAAA");
                List<lstMedicineDTO> lstMedicine = phuongSessionBean.showAllMedicinePres();
                request.setAttribute("LISTMED", lstMedicine);
                url = insertMedPresController;
            }else if (action.equals("UpdatePres")) {
                List<lstMedicineDTO> lstMedicine = phuongSessionBean.showAllMedicinePres();
                request.setAttribute("LISTMED", lstMedicine);
                url = updateMedPresController;
            }else if (action.equals("DeletePres")) {
                List<lstMedicineDTO> lstMedicine = phuongSessionBean.showAllMedicinePres();
                request.setAttribute("LISTMED", lstMedicine);
                url = deleteMedPresController;
            }else if (action.equals("AddNewMedSupp")) {
                List<lstMedicineDTO> lstMedicine = phuongSessionBean.showAllMedicineSupp();
                request.setAttribute("LISTMED", lstMedicine);
                url = addMedSuppPage;
            }else if (action.equals("InsertSupp")) {
                System.out.println("PPPPPPPPPPPPP");
                List<lstMedicineDTO> lstMedicine = phuongSessionBean.showAllMedicineSupp();
                request.setAttribute("LISTMED", lstMedicine);
                url = insertMedSuppController;
            }else if (action.equals("UpdateSupp")) {
                List<lstMedicineDTO> lstMedicine = phuongSessionBean.showAllMedicineSupp();
                request.setAttribute("LISTMED", lstMedicine);
                url = updateMedSuppController;
            }else if (action.equals("DeleteSupp")) {
                List<lstMedicineDTO> lstMedicine = phuongSessionBean.showAllMedicineSupp();
                request.setAttribute("LISTMED", lstMedicine);
                url = deleteMedSuppController;
            }else if (action.equals("Insert Medicine Supplies")) {
                List<lstMedicineDTO> lstMedicine = phuongSessionBean.showAllMedicineSupp();
                request.setAttribute("LISTMED", lstMedicine);
                url = addSuppController;
            }
            
            request.getRequestDispatcher(url).forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}