/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phuong.controller;
import dan.dto.MedicalRecordInfo;
import dan.ssb.DanSessionBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import phuong.ssb.PhuongSessionBean;
/**
 *
 * @author Nguyet Phuong
 */
public class listAllPrescriptionMedSupp extends HttpServlet {
    @EJB
    private DanSessionBean danSSB;
    @EJB
    private PhuongSessionBean phuongSessionBean;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String destination = "phuongWeb/Fail.jsp";
        try {
            String medID = request.getParameter("MedRCID");
            System.out.println("AAAAA"+medID);
            HttpSession session = request.getSession();
            MedicalRecordInfo medRC = danSSB.transToMRView(medID);
            session.setAttribute("MEDID", medRC);
            List<String> lstDate = phuongSessionBean.searchCFDateByMedID(medID);
            List<String> lstPres = phuongSessionBean.searchPresByMedID(medID);
            List<String> lstSupp = phuongSessionBean.searchMedSuppByMedID(medID);
            request.setAttribute("LSTCREFORDATE", lstDate);
            request.setAttribute("LSTPRES", lstPres);
            request.setAttribute("LSTSUPP", lstSupp);
            destination = "ViewAllPresAndMedSupp.jsp";
            request.getRequestDispatcher(destination).forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}