/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phuong.PrescriptionController;

import dan.dto.EmpInfo;
import dan.dto.MedicalRecordInfo;
import dan.ssb.DanSessionBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import phuong.dto.ListMedicinePresDTO;
import phuong.dto.MedicinePresDTO;
import phuong.dto.lstMedicineDTO;
import phuong.ssb.PhuongSessionBean;
import project.ett.TbPrescription;
import project.ett.TbPrescriptionDetail;
import project.locssbean.LocSessionBean;

/**
 *
 * @author Nguyet Phuong
 */
public class PhuongAddPrescription extends HttpServlet {

    @EJB
    private DanSessionBean danSSB;

    @EJB
    private LocSessionBean locSessionBean;

    @EJB
    private PhuongSessionBean phuongSessionBean;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     *
     * danSSB.checkDepositPayment(totalServiceCheck, medID); // đủ true, k đủ
     * false danSSB.refundDeposit(oldTotal, medID); //update
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            String url = "phuongWeb/Fail.jsp";
            // Prescription
            HttpSession session = request.getSession();
            MedicalRecordInfo medRC = (MedicalRecordInfo) session.getAttribute("MEDID");
            EmpInfo emp = (EmpInfo) session.getAttribute("empInfo");
            String empID = emp.getEmployeeID();
            ListMedicinePresDTO listMedDTO = (ListMedicinePresDTO) session.getAttribute("PHUONGMEDICINEPRES");
            if (listMedDTO == null) {
                url = "AddPrescription.jsp";
                String er = "Plz add at least ont medicine!";
                List<lstMedicineDTO> lstMedicine = phuongSessionBean.showAllMedicinePres();
                request.setAttribute("LISTMED", lstMedicine);
                request.setAttribute("ERROR", er);
            } else {
                List<MedicinePresDTO> listMed = listMedDTO.getLtMedicinePrescription();
                if (listMed.isEmpty()) {
                    url = "AddPrescription.jsp";
                    String er = "Plz add at least ont medicine!";
                    List<lstMedicineDTO> lstMedicine = phuongSessionBean.showAllMedicinePres();
                    request.setAttribute("LISTMED", lstMedicine);
                    request.setAttribute("ERROR", er);
                } else {
                    double totalPrice = 0;
                    for (MedicinePresDTO medicinePresDTO : listMed) {
                        totalPrice += medicinePresDTO.getPrice() * medicinePresDTO.getQuantity();
                    }
                    // doi DTO Anh AN
                    String fafrom = phuongSessionBean.findFacIDByFacName(medRC.getCurrentFaculty());
                    //end doi DTO anh AN
                    String dignose = request.getParameter("txtDignose");
                    String createfordate = request.getParameter("txtCreateForDate");
                    if (dignose.equals("")) {
                        url = "AddPrescription.jsp";
                        String er = "Plz insert Dignose!";
                        List<lstMedicineDTO> lstMedicine = phuongSessionBean.showAllMedicinePres();
                        request.setAttribute("LISTMED", lstMedicine);
                        request.setAttribute("ERROR", er);
                    } else if (createfordate.equals("")) {
                        url = "AddPrescription.jsp";
                        String er = "Plz insert create for date!";
                        List<lstMedicineDTO> lstMedicine = phuongSessionBean.showAllMedicinePres();
                        request.setAttribute("LISTMED", lstMedicine);
                        request.setAttribute("ERROR", er);
                    } else {
                        boolean checkTien = true;
                        ListMedicinePresDTO lstMedCancel = new ListMedicinePresDTO();
                        if (danSSB.checkDepositPayment(totalPrice, medRC.getMedRecordID())) {
                            String presID = phuongSessionBean.findMaxRCPres();
                            String[] ngay = createfordate.split("/");
                            Timestamp cfordate = new Timestamp(Integer.parseInt(ngay[2]), Integer.parseInt(ngay[1]), Integer.parseInt(ngay[0]), 0, 0, 0, 0);
                            TbPrescription tbpres = new TbPrescription();
                            tbpres.setPrescriptionID(presID);
                            tbpres.setDignose(dignose);
                            tbpres.setCreateForDate(cfordate);
                            tbpres.setDateCreate(Calendar.getInstance().getTime());
                            tbpres.setStatusPres("New prescription");
                            tbpres.setTotalPrice(totalPrice);
                            phuongSessionBean.insertPres(tbpres, medRC.getMedRecordID(), empID, fafrom);
                        //begin xu ly trong prescription detail
                            //lstMedCancel dùng để tạo 1 dto bao gồm các ListMedicinePresDTO, 
                            //co 1 list chua nhung thuoc k add vao dc vi thieu so luong trong kho
                            for (MedicinePresDTO medicinePresDTO : listMed) {
                                String presDetaID = phuongSessionBean.findMaxRCPresDeta();
                                String medicineID = medicinePresDTO.getId();
                                if (locSessionBean.xuLyExportWarehouse(presID, "P", medicineID, medicinePresDTO.getQuantity(), "", empID)) {
                                    TbPrescriptionDetail presDeta = new TbPrescriptionDetail();
                                    presDeta.setPrescriptionDetailID(presDetaID);
                                    presDeta.setMorning(medicinePresDTO.getMorning());
                                    presDeta.setAfternoon(medicinePresDTO.getAfternoon());
                                    presDeta.setNight(medicinePresDTO.getNight());
                                    presDeta.setQuantity(medicinePresDTO.getQuantity());
                                    presDeta.setDateCreate(Calendar.getInstance().getTime());
                                    presDeta.setPrice(medicinePresDTO.getPrice());
                                    phuongSessionBean.insertDetaPres(presDeta, presID, medicineID);
                                } else {
                                    lstMedCancel.getLtMedicinePrescription().add(medicinePresDTO);
                                }
                            }
                        } else {
                            checkTien = false;
                            String er = "Deposit not enough, plz add deposit!!";
                            List<lstMedicineDTO> lstMedicine = phuongSessionBean.showAllMedicinePres();
                            request.setAttribute("LISTMED", lstMedicine);
                            request.setAttribute("ERROR", er);
                        }
                        if (!checkTien) {
                            url = "AddPrescription.jsp";
                        } else if (!lstMedCancel.getLtMedicinePrescription().isEmpty()) {
                            url = "AddPrescription.jsp";
                            String er = "Quantity don't enough. Plz choose another medicine!!!";
                            request.setAttribute("ERROR", er);
                            session.setAttribute("PHUONGMEDICINEPRES", lstMedCancel);
                        } else {
                            session.removeAttribute("PHUONGMEDICINEPRES");
                            if (emp.getPosition().equals("admin")) {
                                url = "PrescriptionManagement";
                            } else {
                                url = "listAllPrescriptionMedSupp";
                            }
                        }
                    }
                }
            }
            request.getRequestDispatcher(url).forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
