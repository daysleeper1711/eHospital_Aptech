/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phuong.PrescriptionController;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import phuong.dto.ListMedicinePresDTO;
import phuong.dto.MedicinePresDTO;
import phuong.ssb.PhuongSessionBean;

/**
 *
 * @author Nguyet Phuong
 */
public class InsertMedPresController extends HttpServlet {

    @EJB
    private PhuongSessionBean phuongSessionBean;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            int morning, afternoon, night;
            HttpSession session = request.getSession();
            Object obj = session.getAttribute("PHUONGMEDICINEPRES");
            String url = "phuongWeb/Fail.jsp";
            String medicineID = request.getParameter("lstMedicineID");
            String medicineName = phuongSessionBean.searchMedicineNameByID(medicineID);
            double price = phuongSessionBean.findPriceByMedicineID(medicineID);
            String txtMorning = request.getParameter("txtMorning");
            boolean fla = false;
            if (txtMorning.equals("")) {
                morning = 0;
            } else {
                if (txtMorning.length() < 9) {
                    morning = Integer.parseInt(txtMorning);
                } else {
                    morning = 0;
                    fla = true;
                    String er = "Sorry, quantity of moring must be less than 10!!";
                    request.setAttribute("ERROR", er);
                    url = "AddPrescription.jsp";
                }
            }
            String txtafternoon = request.getParameter("txtAfternoon");
            if (txtafternoon.equals("")) {
                afternoon = 0;
            } else {
                if (txtafternoon.length() < 9) {
                    afternoon = Integer.parseInt(txtafternoon);
                } else {
                    afternoon = 0;
                    fla = true;
                    String er = "Sorry, quantity of afternoon must be less than 10!!";
                    request.setAttribute("ERROR", er);
                    url = "AddPrescription.jsp";
                }
            }
            String txtnight = request.getParameter("txtNight");
            if (txtnight.equals("")) {
                night = 0;
            } else {
                if (txtnight.length() < 9) {
                    night = Integer.parseInt(txtnight);
                } else {
                    night = 0;
                    fla = true;
                    String er = "Sorry, quantity of night must be less than 10!!";
                    request.setAttribute("ERROR", er);
                    url = "AddPrescription.jsp";
                }
            }
            int quantity = morning + afternoon + night;
            ListMedicinePresDTO lstDTO = null;
            if (obj == null) {
                lstDTO = new ListMedicinePresDTO();
            } else {
                lstDTO = (ListMedicinePresDTO) obj;
            }

            for (MedicinePresDTO medicinePresDTO : lstDTO.getLtMedicinePrescription()) {
                if (medicinePresDTO.getId().equals(medicineID)) {
                    fla = true;
                    String er = "Sorry, but this medicine already exists";
                    request.setAttribute("ERROR", er);
                    url = "AddPrescription.jsp";
                    break;
                }
            }
            if (quantity <= 0) {
                fla = true;
                String er = "Sorry, quantity of medicine equal 0, plz input quanity of Morning, Afternoon or Night!!";
                request.setAttribute("ERROR", er);
                url = "AddPrescription.jsp";
            }
            if (quantity > 10) {
                fla = true;
                String er = "Sorry, quantity of must least than 10, plz re-input quanity of Morning, Afternoon or Night!!";
                request.setAttribute("ERROR", er);
                url = "AddPrescription.jsp";
            }
            if (fla == false) {
                MedicinePresDTO dto = new MedicinePresDTO(medicineID, medicineName, morning, afternoon, night, quantity, price);
                boolean check = lstDTO.addMedicinePres(dto);
                if (check) {
                    url = "AddPrescription.jsp";
                }
            }
            session.setAttribute("PHUONGMEDICINEPRES", lstDTO);
            request.getRequestDispatcher(url).forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
