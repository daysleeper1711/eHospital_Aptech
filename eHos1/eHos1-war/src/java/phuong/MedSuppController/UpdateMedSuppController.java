/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phuong.MedSuppController;

import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import phuong.dto.ListMedSuppDTO;
import phuong.ssb.PhuongSessionBean;

/**
 *
 * @author Nguyet Phuong
 */
public class UpdateMedSuppController extends HttpServlet {

    @EJB
    private PhuongSessionBean phuongSessionBean;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            int quantity;
            String url = "phuongWeb/Fail.jsp";
            HttpSession session = request.getSession();
            //BookBean bb = (BookBean) session.getAttribute("cart");
            ListMedSuppDTO lstMedSuppDTO = (ListMedSuppDTO) session.getAttribute("PHUONGMEDICINESUPP");
            String medicineID = request.getParameter("txtIDMED");
            String txtQuantity = request.getParameter("txtQ");
            boolean fla = false;
            if (txtQuantity.equals("")) {
                quantity = 0;
            } else {
                if (txtQuantity.length() < 9) {
                    quantity = Integer.parseInt(txtQuantity);
                } else {
                    quantity = 0;
                    fla = true;
                    String er = "Sorry, quantity must be less than 10!!";
                    request.setAttribute("ERROR", er);
                    url = "AddMedicineSupplies.jsp";
                }
            }

            if (quantity <= 0) {
                fla = true;
                String er = "Sorry, quantity of medicine equal 0, plz input quanity!!!";
                request.setAttribute("ERROR", er);
                url = "AddMedicineSupplies.jsp";
            }
            if (quantity > 10) {
                fla = true;
                String er = "Sorry, quantity of must be least than 10, plz re-input quanity!!!";
                request.setAttribute("ERROR", er);
                url = "AddMedicineSupplies.jsp";
            }
            if (fla == false) {
                if (lstMedSuppDTO.updateMedicineSupp(medicineID, quantity)) {
                    session.setAttribute("PHUONGMEDICINESUPP", lstMedSuppDTO);
                    url = "AddMedicineSupplies.jsp";
                }
            }
            request.getRequestDispatcher(url).forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
