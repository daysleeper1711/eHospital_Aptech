/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phuong.MedSuppController;

import dan.dto.EmpInfo;
import dan.dto.MedicalRecordInfo;
import dan.ssb.DanSessionBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import phuong.dto.ListMedSuppDTO;
import phuong.dto.MedicineSuppDTO;
import phuong.dto.lstMedicineDTO;
import phuong.ssb.PhuongSessionBean;
import project.ett.TbMedicineSupplies;
import project.ett.TbMedicineSuppliesDetail;
import project.locssbean.LocSessionBean;
import tuyet.dto.TuyetMedicalRecordDTO;

/**
 *
 * @author Nguyet Phuong
 */
public class PhuongAddMedSuppController extends HttpServlet {

    @EJB
    private DanSessionBean danSSB;

    @EJB
    private LocSessionBean locSessionBean;

    @EJB
    private PhuongSessionBean phuongSessionBean;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            String url = "phuongWeb/Fail.jsp";
            // Prescription
            HttpSession session = request.getSession();
            MedicalRecordInfo medRC = (MedicalRecordInfo) session.getAttribute("MEDID");
            EmpInfo emp = (EmpInfo) session.getAttribute("empInfo");
            String empID = emp.getEmployeeID();
            ListMedSuppDTO listMedDTO = (ListMedSuppDTO) session.getAttribute("PHUONGMEDICINESUPP");
            if (listMedDTO == null) {
                url = "AddMedicineSupplies.jsp";
                String er = "Plz add at least ont medicine!";
                List<lstMedicineDTO> lstMedicine = phuongSessionBean.showAllMedicineSupp();
                request.setAttribute("LISTMED", lstMedicine);
                request.setAttribute("ERROR", er);
            } else {
                List<MedicineSuppDTO> listMed = listMedDTO.getLstMedSuppDTO();
                if (listMed.isEmpty()) {
                    url = "AddMedicineSupplies.jsp";
                    String er = "Plz add at least ont medicine!";
                    List<lstMedicineDTO> lstMedicine = phuongSessionBean.showAllMedicineSupp();
                    request.setAttribute("LISTMED", lstMedicine);
                    request.setAttribute("ERROR", er);
                } else {
                    boolean checkTien = true;
                    double totalPrice = 0;
                    for (MedicineSuppDTO medicineSuppDTO : listMed) {
                        totalPrice += medicineSuppDTO.getPrice() * medicineSuppDTO.getQuantity();
                    }
                    // doi DTO Anh AN
                    String facID = phuongSessionBean.findFacIDByFacName(medRC.getCurrentFaculty());
                    //end doi DTO anh AN
                    String createfordate = request.getParameter("txtCreateForDate");
                    if (createfordate.equals("")) {
                        url = "AddMedicineSupplies.jsp";
                        String er = "Plz insert create for date!";
                        List<lstMedicineDTO> lstMedicine = phuongSessionBean.showAllMedicineSupp();
                        request.setAttribute("LISTMED", lstMedicine);
                        request.setAttribute("ERROR", er);
                    } else {
                        String suppID = phuongSessionBean.findMaxRCSupp();
                        String[] ngay = createfordate.split("/");
                        Timestamp cfordate = new Timestamp(Integer.parseInt(ngay[2]), Integer.parseInt(ngay[1]), Integer.parseInt(ngay[0]), 0, 0, 0, 0);
                        ListMedSuppDTO lstMedCancel = new ListMedSuppDTO();
                        if (danSSB.checkDepositPayment(totalPrice, medRC.getMedRecordID())) {
                            TbMedicineSupplies tbSupp = new TbMedicineSupplies();
                            tbSupp.setMedicineSuppliesID(suppID);
                            tbSupp.setCreateForDate(cfordate);
                            tbSupp.setDateCreate(Calendar.getInstance().getTime());
                            tbSupp.setStatusMedSupp("New medicine supplies");
                            tbSupp.setTotalPrice(totalPrice);
                            phuongSessionBean.insertSupp(tbSupp, medRC.getMedRecordID(), empID, facID);
                            //begin xu ly trong prescription detail
                            //lstMedCancel dùng để tạo 1 dto bao gồm các ListMedicinePresDTO, 
                            //co 1 list chua nhung thuoc k add vao dc vi thieu so luong trong kho
                            System.out.println("Tooo " + totalPrice);
                            for (MedicineSuppDTO medicineSuppDTO : listMed) {
                                String suppDetaID = phuongSessionBean.findMaxRCSuppDeta();
                                String medicineID = medicineSuppDTO.getId();
                                if (locSessionBean.xuLyExportWarehouse(suppID, "P", medicineID, medicineSuppDTO.getQuantity(), "", empID)) {
                                    TbMedicineSuppliesDetail suppDeta = new TbMedicineSuppliesDetail();
                                    suppDeta.setMedicineSuppliesDetailID(suppDetaID);
                                    suppDeta.setQuantity(medicineSuppDTO.getQuantity());
                                    suppDeta.setDateCreate(Calendar.getInstance().getTime());
                                    suppDeta.setPrice(medicineSuppDTO.getPrice());
                                    phuongSessionBean.insertDetaSupp(suppDeta, suppID, medicineID);
                                } else {
                                    lstMedCancel.getLstMedSuppDTO().add(medicineSuppDTO);
                                }
                            }
                        } else {
                            checkTien = false;
                            String er = "Deposit not enough, plz add deposit!!";
                            List<lstMedicineDTO> lstMedicine = phuongSessionBean.showAllMedicineSupp();
                            request.setAttribute("LISTMED", lstMedicine);
                            request.setAttribute("ERROR", er);
                        }
                        if (!checkTien) {
                            url = "AddMedicineSupplies.jsp";
                        } else if (!lstMedCancel.getLstMedSuppDTO().isEmpty()) {
                            url = "AddMedicineSupplies.jsp";
                            session.setAttribute("PHUONGMEDICINESUPP", lstMedCancel);
                        } else {
                            session.removeAttribute("PHUONGMEDICINESUPP");
                            if (emp.getPosition().equals("admin")) {
                                url = "PrescriptionManagement";
                            } else {
                                url = "listAllPrescriptionMedSupp";
                            }
                        }
                    }
                }
            }
            request.getRequestDispatcher(url).forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
