/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phuong.listDetailController;
import dan.dto.MedicalRecordInfo;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import phuong.dto.ListMedSuppDTO;
import phuong.dto.ListSuppDTO;
import phuong.ssb.PhuongSessionBean;
/**
 *
 * @author Nguyet Phuong
 */
public class ListMedSuppController extends HttpServlet {
    @EJB
    private PhuongSessionBean phuongSessionBean;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            String url = "ListSupp.jsp";
            HttpSession session = request.getSession();
            MedicalRecordInfo medRC = (MedicalRecordInfo) session.getAttribute("MEDID");
            String createfordate = request.getParameter("txtCreateFDate");
//            SimpleDateFormat parseFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
//            Date date = parseFormat.parse(createfordate);
            SimpleDateFormat parseFormat = new SimpleDateFormat("dd/MM/yyyy");
            Date date = parseFormat.parse(createfordate);
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            String result = format.format(date);
            String[] ngay = result.split("-");
            Timestamp cfordate = new Timestamp(Integer.parseInt(ngay[0]) - 1900, Integer.parseInt(ngay[1]) - 1, Integer.parseInt(ngay[2]), 0, 0, 0, 0);;
            List<ListSuppDTO> lstSupp = phuongSessionBean.findSuppByCreFDate(cfordate, medRC.getMedRecordID());
            request.setAttribute("DATE", createfordate);
            request.setAttribute("LISTSUPP", lstSupp);
            request.getRequestDispatcher(url).forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}