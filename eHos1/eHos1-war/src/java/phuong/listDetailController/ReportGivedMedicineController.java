/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phuong.listDetailController;

import dan.dto.MedicalRecordInfo;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import phuong.dto.EditPresDTO;
import phuong.dto.EditSuppDTO;
import phuong.ssb.PhuongSessionBean;

/**
 *
 * @author Nguyet Phuong
 */
public class ReportGivedMedicineController extends HttpServlet {

    @EJB
    private PhuongSessionBean phuongSessionBean;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            String url = "phuongWeb/Fail.jsp";
            String selectbox = request.getParameter("lstGivedMed");
            String[] arr = selectbox.split("/");
            
            EditPresDTO pres = null;
            EditSuppDTO supp = null;
            
            SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");
            SimpleDateFormat parseFormat = new SimpleDateFormat("EEEE, MMMM dd yyyy");
            
            String createfordate = "";
            String createDate = "";

            if (arr.length == 3) {
                supp = phuongSessionBean.editSupp(arr[2]);
                if (!arr[1].isEmpty()) {
                    pres = phuongSessionBean.editPres(arr[1]);
                }
            } else {
                pres = phuongSessionBean.editPres(arr[1]);
            }
            if (pres == null) {
                createfordate = formatDate.format(supp.getSupp().getCreateForDate());
                createDate = parseFormat.format(supp.getSupp().getDateCreate());
                request.setAttribute("CHECK", "1");
                request.setAttribute("DATE", createfordate);
                request.setAttribute("DATECREATE", createDate);
                request.setAttribute("SUPP", supp);
            } else if (supp == null) {
                createfordate = formatDate.format(pres.getPres().getCreateForDate());
                createDate = parseFormat.format(pres.getPres().getDateCreate());
                request.setAttribute("CHECK", "2");
                request.setAttribute("DATE", createfordate);
                request.setAttribute("DATECREATE", createDate);
                request.setAttribute("RP", pres);
            } else if (supp != null && pres != null) {
                createfordate = formatDate.format(pres.getPres().getCreateForDate());
                createDate = parseFormat.format(pres.getPres().getDateCreate());
                request.setAttribute("CHECK", "3");
                request.setAttribute("DATE", createfordate);
                request.setAttribute("DATECREATE", createDate);
                request.setAttribute("RP", pres);
                request.setAttribute("SUPP", supp);
            } 
            url = "ReportGivedMedicine.jsp";
            request.getRequestDispatcher(url).forward(request, response);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
