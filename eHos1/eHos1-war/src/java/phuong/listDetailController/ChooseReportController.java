/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phuong.listDetailController;

import dan.dto.MedicalRecordInfo;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import phuong.dto.ChooseGivedMedReportDTO;
import phuong.dto.ListPresDTO;
import phuong.dto.ListSuppDTO;
import phuong.ssb.PhuongSessionBean;

/**
 *
 * @author Nguyet Phuong
 */
public class ChooseReportController extends HttpServlet {

    @EJB
    private PhuongSessionBean phuongSessionBean;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            String url = "phuongWeb/Fail.jsp";
            String createfordate = request.getParameter("txtCreateFDate");
            HttpSession session = request.getSession();
            MedicalRecordInfo medRC = (MedicalRecordInfo) session.getAttribute("MEDID");

            SimpleDateFormat parseFormat = new SimpleDateFormat("dd/MM/yyyy");
            Date date = parseFormat.parse(createfordate);
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            String result = format.format(date);
            String[] ngay = result.split("-");
            Timestamp cfordate = new Timestamp(Integer.parseInt(ngay[0]) - 1900, Integer.parseInt(ngay[1]) - 1, Integer.parseInt(ngay[2]), 0, 0, 0, 0);

            List<ChooseGivedMedReportDTO> lstCh = phuongSessionBean.searchLstGRePortByCFDMEDRC(cfordate, medRC.getMedRecordID());
            request.setAttribute("DATE", createfordate);
            request.setAttribute("LISTCH", lstCh);
            url = "ChooseGivedReport.jsp";
            request.getRequestDispatcher(url).forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
