/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phuong.listPresController;

import dan.dto.EmpInfo;
import dan.ssb.DanSessionBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import phuong.dto.EditPresDTO;
import phuong.dto.lstMedicineDTO;
import phuong.ssb.PhuongSessionBean;
import project.ett.TbPrescription;
import project.ett.TbPrescriptionDetail;
import project.locssbean.LocSessionBean;

/**
 *
 * @author Nguyet Phuong
 */
public class DeletePresController extends HttpServlet {

    @EJB
    private DanSessionBean danSSB;
    @EJB
    private LocSessionBean locSessionBean;
    @EJB
    private PhuongSessionBean phuongSessionBean;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            String url = "phuongWeb/Fail.jsp";
            String cfD = request.getParameter("txtCreateFDate");
            String presID = request.getParameter("txtPresID");
            HttpSession session = request.getSession();
            EmpInfo emp = (EmpInfo) session.getAttribute("empInfo");
            EditPresDTO ed = phuongSessionBean.editPres(presID);
            for (TbPrescriptionDetail object : ed.getLstDetail()) {
                locSessionBean.xuLyExportWarehouse(presID, "P", object.getMedicineID().getItemCode(), 0, "", emp.getEmployeeID());
                danSSB.refundDeposit(object.getPrice() * object.getQuantity(), object.getPrescriptionID().getMedicalRecordID().getMedRecordID());
            }
            phuongSessionBean.deletePres(presID);
            url = "ListPresController?txtCreateFDate=" + cfD;
            request.getRequestDispatcher(url).forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
