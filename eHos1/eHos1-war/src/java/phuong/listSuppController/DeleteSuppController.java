/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package phuong.listSuppController;

import dan.dto.EmpInfo;
import dan.ssb.DanSessionBean;
import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import phuong.dto.EditSuppDTO;
import phuong.ssb.PhuongSessionBean;
import project.ett.TbMedicineSuppliesDetail;
import project.locssbean.LocSessionBean;

/**
 *
 * @author Nguyet Phuong
 */
public class DeleteSuppController extends HttpServlet {
    @EJB
    private DanSessionBean danSSB;
    @EJB
    private LocSessionBean locSessionBean;
    @EJB
    private PhuongSessionBean phuongSessionBean;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            String url = "phuongWeb/Fail.jsp";
            HttpSession session = request.getSession();
            EmpInfo emp = (EmpInfo) session.getAttribute("empInfo");
            String cfD = request.getParameter("txtCreateFDate");
            String suppID = request.getParameter("txtSuppID");
            EditSuppDTO ed = phuongSessionBean.editSupp(suppID);
            for (TbMedicineSuppliesDetail object : ed.getLstSuppDeta()) {
                locSessionBean.xuLyExportWarehouse(suppID, "P", object.getMedicineID().getItemCode(), 0, "", emp.getEmployeeID());
                danSSB.refundDeposit(object.getPrice() * object.getQuantity(), object.getMedicineSuppliesID().getMedicalRecordID().getMedRecordID());
            }
            phuongSessionBean.deleteMedSupp(suppID);
            url = "ListMedSuppController?txtCreateFDate="+cfD;
            request.getRequestDispatcher(url).forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
