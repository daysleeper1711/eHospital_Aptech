/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phuong.editPrescription;

import dan.dto.EmpInfo;
import dan.dto.MedicalRecordInfo;
import dan.ssb.DanSessionBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import phuong.dto.EditPresDTO;
import phuong.dto.lstMedicineDTO;
import phuong.ssb.PhuongSessionBean;
import project.ett.TbPrescriptionDetail;
import project.locssbean.LocSessionBean;

/**
 *
 * @author Nguyet Phuong
 */
public class InsertPresController extends HttpServlet {

    @EJB
    private DanSessionBean danSSB;

    @EJB
    private LocSessionBean locSessionBean;

    @EJB
    private PhuongSessionBean phuongSessionBean;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            String url = "phuongWeb/Fail.jsp";
            // Prescription
            String presID = request.getParameter("txtPresID");
            String medicineID = request.getParameter("lstMediID");
            boolean fla = phuongSessionBean.checkMedIdInPrescription(presID, medicineID);
            if (fla) {
                String er = "Sorry, but this medicine already exists";
                request.setAttribute("ERROR", er);
            } else {
                HttpSession session = request.getSession();
                EmpInfo emp = (EmpInfo) session.getAttribute("empInfo");
                String empID = emp.getEmployeeID();
                double price = phuongSessionBean.findPriceByMedicineID(medicineID);
                int morning, afternoon, night;
                String txtMorning = request.getParameter("txtMorning");
                if (txtMorning.equals("")) {
                    morning = 0;
                } else {
                    if (txtMorning.length() < 9) {
                        morning = Integer.parseInt(txtMorning);
                    } else {
                        morning = 0;
                        fla = true;
                        String er = "Sorry, quantity of moring must be less than 10!!";
                        request.setAttribute("ERROR", er);
                        url = "EditPrescription.jsp";
                    }
                }
                String txtAfternoon = request.getParameter("txtAfternoon");
                if (txtAfternoon.equals("")) {
                    afternoon = 0;
                } else {
                    if (txtAfternoon.length() < 9) {
                        afternoon = Integer.parseInt(txtAfternoon);
                    } else {
                        afternoon = 0;
                        fla = true;
                        String er = "Sorry, quantity of afternoon must be less than 10!!";
                        request.setAttribute("ERROR", er);
                        url = "EditPrescription.jsp";
                    }
                }
                String txtNight = request.getParameter("txtNight");
                if (txtNight.equals("")) {
                    night = 0;
                } else {
                    if (txtNight.length() < 9) {
                        night = Integer.parseInt(txtNight);
                    } else {
                        night = 0;
                        fla = true;
                        String er = "Sorry, quantity of night must be less than 10!!";
                        request.setAttribute("ERROR", er);
                        url = "EditPrescription.jsp";
                    }
                }
                int quantity = morning + night + afternoon;
                if (quantity <= 0) {
                    fla = true;
                    String er = "Sorry, quantity of medicine equal 0, plz input quanity of Morning, Afternoon or Night!!";
                    request.setAttribute("ERROR", er);
                    url = "EditPrescription.jsp";
                }
                if (quantity > 10) {
                    fla = true;
                    String er = "Sorry, quantity of must least than 10, plz re-input quanity of Morning, Afternoon or Night!!";
                    request.setAttribute("ERROR", er);
                    url = "EditPrescription.jsp";
                }
                if (fla == false) {
                    String presDetaID = phuongSessionBean.findMaxRCPresDeta();
                    if (!locSessionBean.xuLyExportWarehouse(presID, "P", medicineID, quantity, "", empID)) {
                        String er = "Sorry, but this medicine's quantity don't enough!!!";
                        request.setAttribute("ERROR", er);
                    } else {
                        String medRCiD = request.getParameter("medRC");
                        if (danSSB.checkDepositPayment(quantity * price, medRCiD)) {
                            TbPrescriptionDetail presDeta = new TbPrescriptionDetail();
                            presDeta.setPrescriptionDetailID(presDetaID);
                            presDeta.setDateCreate(Calendar.getInstance().getTime());
                            presDeta.setMorning(morning);
                            presDeta.setAfternoon(afternoon);
                            presDeta.setNight(night);
                            presDeta.setQuantity(quantity);
                            presDeta.setPrice(price);
                            phuongSessionBean.insertDetaPres(presDeta, presID, medicineID);
                            phuongSessionBean.updatePrescription(presID, empID);
                        } else {
                            locSessionBean.xuLyExportWarehouse(presID, "P", medicineID, 0, "", empID);
                            String er = "Deposit not enough, plz add deposit!!";
                            request.setAttribute("ERROR", er);
                        }
                    }
                }
            }
            SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");
            EditPresDTO edPreDto = phuongSessionBean.editPres(presID);
            if (edPreDto != null) {
                String createfordate = formatDate.format(edPreDto.getPres().getCreateForDate());
                List<lstMedicineDTO> lstMedicine = phuongSessionBean.showAllMedicinePres();
                request.setAttribute("LISTMED", lstMedicine);
                request.setAttribute("DATE", createfordate);
                request.setAttribute("EDITPRES", edPreDto);
                url = "EditPrescription.jsp";
            }
            request.getRequestDispatcher(url).forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
