/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phuong.editPrescription;
import dan.dto.EmpInfo;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import phuong.dto.EditPresDTO;
import phuong.dto.lstMedicineDTO;
import phuong.ssb.PhuongSessionBean;
/**
 *
 * @author Nguyet Phuong
 */
public class PhuongUpdatePrescription extends HttpServlet {
    @EJB
    private PhuongSessionBean phuongSessionBean;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            String url = "phuongWeb/Fail.jsp";
            HttpSession session = request.getSession();
            EmpInfo emp = (EmpInfo) session.getAttribute("empInfo");
            String empID = emp.getEmployeeID();
            String presID = request.getParameter("txtPresID");
            String diagnose = request.getParameter("txtDiagnose");
            phuongSessionBean.updatePresDignose(presID, empID, diagnose);
            phuongSessionBean.updatePrescription(presID, empID);
            SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");
            EditPresDTO edPreDto = phuongSessionBean.editPres(presID);
            if (edPreDto != null) {
                String createfordate = formatDate.format(edPreDto.getPres().getCreateForDate());
                List<lstMedicineDTO> lstMedicine = phuongSessionBean.showAllMedicinePres();
                request.setAttribute("LISTMED", lstMedicine);
                request.setAttribute("DATE", createfordate);
                request.setAttribute("EDITPRES", edPreDto);
                url = "EditPrescription.jsp";
            }
            request.getRequestDispatcher(url).forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}