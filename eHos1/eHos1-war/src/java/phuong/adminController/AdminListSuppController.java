/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phuong.adminController;

import static com.sun.tools.ws.wsdl.parser.Util.fail;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import phuong.dto.EditSuppDTO;
import phuong.dto.lstMedicineDTO;
import phuong.ssb.PhuongSessionBean;

/**
 *
 * @author Nguyet Phuong
 */
public class AdminListSuppController extends HttpServlet {
    @EJB
    private PhuongSessionBean phuongSessionBean;

    private static final String fail = "phuongWeb/Fail.jsp";
    private static final String detailPres = "PhuongAdminDetailSupp.jsp";
    private static final String editPres = "EditMedSupplies.jsp";
    private static final String deletePres = "MedSuppliesManagement";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            String action = request.getParameter("action");
            String url = fail;
            String txtSupp = request.getParameter("txtSuppID");
            SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");
            System.out.println("Action "+action);
            if (action.equals("DetailSupp")) {
                EditSuppDTO edPreDto = phuongSessionBean.editSupp(txtSupp);
                if (edPreDto != null) {
                    String createfordate = formatDate.format(edPreDto.getSupp().getCreateForDate());
                    String createDate = formatDate.format(edPreDto.getSupp().getDateCreate());
                    request.setAttribute("DATE", createfordate);
                    request.setAttribute("DATECREATE", createDate);
                    request.setAttribute("EDITPRES", edPreDto);
                    url = detailPres;
                }
            } else if (action.equals("EditSupp")) {
                EditSuppDTO edPreDto = phuongSessionBean.editSupp(txtSupp);
                if (edPreDto != null) {
                    String createfordate = formatDate.format(edPreDto.getSupp().getCreateForDate());
                    List<lstMedicineDTO> lstMedicine = phuongSessionBean.showAllMedicineSupp();
                    request.setAttribute("LISTMED", lstMedicine);
                    request.setAttribute("DATE", createfordate);
                    request.setAttribute("EDITSUPP", edPreDto);
                    url = editPres;
                }
            } else if (action.equals("DeleteSupp")) {
                String suppID = request.getParameter("txtSuppID");
                phuongSessionBean.deleteMedSupp(suppID);
                url = deletePres;
            }
            request.getRequestDispatcher(url).forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
