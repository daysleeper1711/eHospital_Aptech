/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phuong.adminController;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import phuong.dto.EditPresDTO;
import phuong.dto.lstMedicineDTO;
import phuong.ssb.PhuongSessionBean;

/**
 *
 * @author Nguyet Phuong
 */
public class AdminListPrescriptionController extends HttpServlet {

    @EJB
    private PhuongSessionBean phuongSessionBean;

    private static final String fail = "phuongWeb/Fail.jsp";
    private static final String detailPres = "PhuongAdmindetailPrescription.jsp";
    private static final String editPres = "EditPrescription.jsp";
    private static final String deletePres = "PrescriptionManagement";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            String action = request.getParameter("action");
            String url = fail;
            String txtPres = request.getParameter("txtPresID");
            SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");
            if (action.equals("DetailPres")) {
                EditPresDTO edPreDto = phuongSessionBean.editPres(txtPres);
                if (edPreDto != null) {
                    String createfordate = formatDate.format(edPreDto.getPres().getCreateForDate());
                    String createDate = formatDate.format(edPreDto.getPres().getDateCreate());
                    request.setAttribute("DATE", createfordate);
                    request.setAttribute("DATECREATE", createDate);
                    request.setAttribute("EDITPRES", edPreDto);
                    url = detailPres;
                }
            } else if (action.equals("EditPres")) {
                EditPresDTO edPreDto = phuongSessionBean.editPres(txtPres);
                if (edPreDto != null) {
                    String createfordate = formatDate.format(edPreDto.getPres().getCreateForDate());
                    List<lstMedicineDTO> lstMedicine = phuongSessionBean.showAllMedicinePres();
                    request.setAttribute("LISTMED", lstMedicine);
                    request.setAttribute("DATE", createfordate);
                    request.setAttribute("EDITPRES", edPreDto);
                    url = editPres;
                }
            } else if (action.equals("DeletePres")) {
                String presID = request.getParameter("txtPresID");
                phuongSessionBean.deletePres(presID);
                url = deletePres;
            }
            request.getRequestDispatcher(url).forward(request, response);
        } catch (Exception e) {
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
