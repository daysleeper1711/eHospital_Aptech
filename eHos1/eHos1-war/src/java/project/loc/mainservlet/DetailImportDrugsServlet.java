/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project.loc.mainservlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import project.ett.TbImDurgsSupplies;
import project.ett.TbImDurgsSuppliesDetails;
import project.ett.TbItemCodeManagement;
import project.ett.TbSupplier;
import project.locdto.ImDurgsSuppliesHeader_Details_DTO;
import project.locssbean.LocSessionBean;

/**
 *
 * @author locth-laptop
 */
public class DetailImportDrugsServlet extends HttpServlet {

    @EJB
    private LocSessionBean locSessionBean;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            String action = request.getParameter("action");

            ImDurgsSuppliesHeader_Details_DTO all = new ImDurgsSuppliesHeader_Details_DTO();
            String supname = "";
            String sfromdate = "";
            String stodate = "";
            List<TbImDurgsSuppliesDetails> details = null;

            String pak = request.getParameter("txtID");
            all = locSessionBean.findbyPackage_ImportHeader_Details(pak);
            //Update lai trang thai
            TbImDurgsSupplies dto_header = all.getHeader();
            if (dto_header.getUStatus().equals("F")) {
                dto_header.setUStatus("Finish");
            } else {
                dto_header.setUStatus("Open");
            }

            //Update ngay gio           
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            sfromdate = sdf.format(dto_header.getCrDate());
            stodate = sdf.format(dto_header.getUpDate());

            all.setHeader(dto_header);

            //Lay Name NCC
            TbSupplier dto_sup = locSessionBean.findbySupplier_ImportDrugsHeader(pak);
            supname = dto_sup.getSupplierName();

            //Lay import details
            details = all.getDetails();

            for (TbImDurgsSuppliesDetails item : details) {

                //Update ngay gio           
                Date tempdate = item.getWarDate();
                item.setImDetails(sdf.format(tempdate));
            }

            if (all != null) {
                request.setAttribute("header_details", all);
                request.setAttribute("listdetails", details);
                request.setAttribute("SupName", supname);
                request.setAttribute("sfromdate", sfromdate);
                request.setAttribute("stodate", stodate);

                //Lay Name Itemcode
                List<TbItemCodeManagement> listitemcode = locSessionBean.findbyAllItemCode();
                request.setAttribute("listITEMCODE", listitemcode);

            }
            String web = "";
            if (action.equals("Details")) {
                web = "Import Drugs Details.jsp";
            }else{
                web = "ReportImport.jsp";
            }
            request.getRequestDispatcher(web).forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
