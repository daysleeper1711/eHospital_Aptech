/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project.loc.mainservlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import project.ett.TbSupplier;
import project.locssbean.LocSessionBean;

/**
 *
 * @author locth-laptop
 */
public class SearchSupplierServlet extends HttpServlet {

    @EJB
    private LocSessionBean locSessionBean;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            String type = request.getParameter("optionsRadiosInline");

            List<TbSupplier> list = new ArrayList<>();
            TbSupplier dto = new TbSupplier();

            if (type.equals("SupplierID")) {
                String search = request.getParameter("txtSearch");
                dto = locSessionBean.findbySupplierID(search);
                if (dto != null) {
                    list.add(dto);
                }               
            } else if (type.equals("SupplierName")) {
                String search = request.getParameter("txtSearch");
                list = locSessionBean.findbyLikeSupplierName(search);
            } else {
                String status = request.getParameter("select");
                if (status.equals("All")) {
                    list = locSessionBean.findbyAllSupplier();
                } else if (status.equals("Action")) {
                    list = locSessionBean.findbySupplierStatus("A");

                } else if (status.equals("Inactive")) {
                    list = locSessionBean.findbySupplierStatus("I");

                }
            }
            
            

            if (list != null) {
                for (int i = 0; i < list.size(); i++) {
                    TbSupplier item = list.get(i);
                    if (item.getUStatus().equals("A")) {
                        item.setUStatus("Action");
                        list.set(i, item);
                    } else {
                        item.setUStatus("Inactive");
                        list.set(i, item);
                    }
                }
                request.setAttribute("list", list);
            }

            request.getRequestDispatcher("Supplier Management.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
