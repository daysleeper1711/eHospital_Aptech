/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project.loc.mainservlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import project.ett.TbItemCodeManagement;
import project.locssbean.LocSessionBean;

/**
 *
 * @author locth-laptop
 */
public class SearchItemCodeServlet extends HttpServlet {
    @EJB
    private LocSessionBean locSessionBean;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String type = request.getParameter("optionsRadiosInline");
            TbItemCodeManagement dto = new TbItemCodeManagement();
            List<TbItemCodeManagement> list = new ArrayList<TbItemCodeManagement>();
            
            if (type.equals("ItemCode")) {
                String search = request.getParameter("txtItemCode");
                System.out.println(search);
                dto = locSessionBean.findbyItemCode(search);
                if (dto != null) {
                    list.add(dto);
                }               
            } else if (type.equals("ItemName")) {
                String search = request.getParameter("txtItemName");
                list = locSessionBean.findbyLikeItemName(search);
            } else if (type.equals("GenericDrug")) {
                String search = request.getParameter("txtGenericDrug");
                list = locSessionBean.findbyLikeGenericDrug(search);
            }else{
                String status = request.getParameter("select");
                if (status.equals("Drugs")) {
                    list = locSessionBean.findbyType("D");
                } else if (status.equals("Supplies")) {
                    list = locSessionBean.findbyType("S");
                }else{
                    list = locSessionBean.findbyAllItemCode();
                }
            }
            
            if (list != null) {
                for (int i = 0; i < list.size(); i++) {
                    TbItemCodeManagement item = list.get(i);
                    if (item.getTypeITcode().equals("D")) {
                        item.setTypeITcode("Drugs");
                        list.set(i, item);
                    } else {
                        item.setTypeITcode("Supplies");
                        list.set(i, item);
                    }
                    
                }
                request.setAttribute("list", list);
            }

            request.getRequestDispatcher("ItemCode Management.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
