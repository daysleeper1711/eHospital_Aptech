/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project.loc.mainservlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import project.ett.TbItemCodeManagement;
import project.ett.TbWhouse;
import project.locdto.WhouseFullDTO;
import project.locssbean.LocSessionBean;

/**
 *
 * @author locth-laptop
 */
public class FindWarehouseServlet extends HttpServlet {

    @EJB
    private LocSessionBean locSessionBean;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            String action = request.getParameter("action");

            if (action.equals("btnSearch")) {
                String search = request.getParameter("txtsearch");

                String type = request.getParameter("optionsRadiosInline");
                List<TbWhouse> list = new ArrayList<TbWhouse>();
                if (type.equals("ItemCode")) {
                    List<TbWhouse> listdto = null;
                    listdto = locSessionBean.findbyWarehouseItemCode(search);
                    if (listdto != null) {
                        list = listdto;
                    }
                } else if (type.equals("ItemName")) {
                    List<TbItemCodeManagement> listitemcode = new ArrayList<TbItemCodeManagement>();
                    listitemcode = locSessionBean.findbyLikeItemName(search);
                    for (TbItemCodeManagement item : listitemcode) {
                        List<TbWhouse> listdto = null;
                        listdto = locSessionBean.findbyWarehouseItemCode(item.getItemCode());
                        if (listdto != null) {
                            for (TbWhouse item2 : listdto) {
                                list.add(item2);
                            }
                        }
                    }
                } else if (type.equals("Package")) {
                    list = locSessionBean.findbyWarehousePackage(search);
                } else if (type.equals("GenericDrug")) {
                    List<TbItemCodeManagement> listitemcode = new ArrayList<TbItemCodeManagement>();
                    listitemcode = locSessionBean.findbyLikeGenericDrug(search);
                    for (TbItemCodeManagement item : listitemcode) {
                        List<TbWhouse> listdto = null;
                        listdto = locSessionBean.findbyWarehouseItemCode(item.getItemCode());
                        if (listdto != null) {
                            for (TbWhouse item2 : listdto) {
                                list.add(item2);
                            }
                        }
                    }
                } else {
                    String nametype = request.getParameter("select");
                    if (nametype.equals("All")) {
                        List<TbItemCodeManagement> listitemcode = new ArrayList<TbItemCodeManagement>();
                        listitemcode = locSessionBean.findbyAllItemCode();
                        for (TbItemCodeManagement item : listitemcode) {
                            List<TbWhouse> listdto = null;
                            listdto = locSessionBean.findbyWarehouseItemCode(item.getItemCode());
                            if (listdto != null) {
                                for (TbWhouse item2 : listdto) {
                                    list.add(item2);
                                }
                            }
                        }
                    } else if (nametype.equals("Drugs")) {
                        List<TbItemCodeManagement> listitemcode = new ArrayList<TbItemCodeManagement>();
                        listitemcode = locSessionBean.findbyType("D");
                        for (TbItemCodeManagement item : listitemcode) {
                            List<TbWhouse> listdto = null;
                            listdto = locSessionBean.findbyWarehouseItemCode(item.getItemCode());
                            if (listdto != null) {
                                for (TbWhouse item2 : listdto) {
                                    list.add(item2);
                                }
                            }
                        }
                    } else {
                        List<TbItemCodeManagement> listitemcode = new ArrayList<TbItemCodeManagement>();
                        listitemcode = locSessionBean.findbyType("S");
                        for (TbItemCodeManagement item : listitemcode) {
                            List<TbWhouse> listdto = null;
                            listdto = locSessionBean.findbyWarehouseItemCode(item.getItemCode());
                            if (listdto != null) {
                                for (TbWhouse item2 : listdto) {
                                    list.add(item2);
                                }
                            }
                        }
                    }
                }

                if (list != null) {
                    List<WhouseFullDTO> listAll = new ArrayList<WhouseFullDTO>();

                    for (TbWhouse item : list) {
                        TbItemCodeManagement idto = new TbItemCodeManagement();
                        WhouseFullDTO wdto = new WhouseFullDTO();

                        idto = locSessionBean.findbyItemCode(item.getItemCode());
                        String name = idto.getItemName();

                        wdto.setItemcode(item.getItemCode());
                        wdto.setItemname(name);
                        wdto.setOnHand(item.getOnHand());
                        wdto.setQtyAvailable(item.getQtyAvailable());
                        wdto.setQtyOrder(item.getQtyOrder());
                        wdto.setPackage1(item.getPackage1());
                        //Xu ly ngay thang
                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                        String d = sdf.format(item.getWarDate());
                        wdto.setWarDate(d);
                        listAll.add(wdto);
                    }

                    request.setAttribute("list", listAll);
                }

                request.getRequestDispatcher("Find Warehouse.jsp").forward(request, response);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
