/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project.loc.mainservlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import project.ett.TbItemCodeManagement;
import project.locssbean.LocSessionBean;

/**
 *
 * @author locth-laptop
 */
public class AddItemCodeServlet extends HttpServlet {
    @EJB
    private LocSessionBean locSessionBean;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            String action = request.getParameter("action");
            String web = "Add ItemCode.jsp";
            
            if (action.equals("Add New ItemCode")) {
                
                String itemname = request.getParameter("txtItemName");
                String content = request.getParameter("txtContent");
                String type = request.getParameter("selectType");
                String howToUse = request.getParameter("selectHowToUse");
                String genericDrug = request.getParameter("txtGenericDrug");
                String unit = request.getParameter("txtUnit");
                double price = Double.parseDouble(request.getParameter("txtPrice"));
                String nation = request.getParameter("country");
                
                TbItemCodeManagement dto = new TbItemCodeManagement();
                dto.setItemName(itemname);
                dto.setContent(content);
                dto.setHowToUse(howToUse);
                dto.setGenericDrug(genericDrug);
                dto.setUnit(unit);
                dto.setXPrice(price);
                dto.setNation(nation);
                
                if (type.equals("Drugs")) {
                    type = "D";
                }else{
                    type = "S";
                }
                dto.setTypeITcode(type);
                //User
                String user = request.getParameter("txtUserID") + " - " + request.getParameter("txtUserName");
                
                locSessionBean.insertItemCode(dto, user);
                               
                web = "ItemCode Management.jsp";
            }
            
            request.getRequestDispatcher(web).forward(request, response);
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        processRequest(request, response);
        
       
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
