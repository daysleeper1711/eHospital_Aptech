/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project.loc.mainservlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import project.ett.TbImDurgsSupplies;
import project.ett.TbImDurgsSuppliesDetails;
import project.ett.TbWhouse;
import project.locssbean.LocSessionBean;

/**
 *
 * @author locth-laptop
 */
public class ImportDrugsServlet extends HttpServlet {
    @EJB
    private LocSessionBean locSessionBean;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String sybill = request.getParameter("txtSymBill");
            String numbill = request.getParameter("txtNumBill");
            String supplierid = request.getParameter("select");
            String note = request.getParameter("txtnote");
            
          
            
            //Details
            String a = request.getParameter("txtDataImport");
            
            String test = a.substring(0, a.length() - 1);
            
            String test1 [] = test.split(";");
            double totat = 0;
            List<TbImDurgsSuppliesDetails> list_details = new ArrayList<TbImDurgsSuppliesDetails>();
            for (int i = 0; i < test1.length; i++) {
                String data = test1[i];
                String data_details []= data.split(",");
                String itemcode = data_details[0];
                String quantity = data_details[1];
                String price = data_details[2];
                String wardate = data_details[3];
                
                double tempprice = Integer.parseInt(price);
                
                totat += Integer.parseInt(price);
                TbImDurgsSuppliesDetails dto_details = new TbImDurgsSuppliesDetails();
                dto_details.setItemCode(itemcode);
                dto_details.setNPrice(tempprice);
                dto_details.setQuantity(Integer.parseInt(quantity));
                //Xu ly ngay bao hanh
                String wardatedetails []= wardate.split("-");
                Date d = new Date(Integer.parseInt(wardatedetails[0])-1900, Integer.parseInt(wardatedetails[1])-1, Integer.parseInt(wardatedetails[2]));
                dto_details.setWarDate(d);
                
                
                list_details.add(dto_details);
            }
            
            //Insert Header
            TbImDurgsSupplies dto_header = new TbImDurgsSupplies();
            dto_header.setNote(note);
            dto_header.setNumBill(numbill);
            dto_header.setSymBill(sybill);
            dto_header.setTotatPrice(totat);
            dto_header.setUStatus("F");
            //user thao tac
            String user = "locth";
            dto_header.setCreateby(user);
            dto_header.setUpdateby(user);
            
            Date d = new Date();
            dto_header.setCrDate(d);
            dto_header.setUpDate(d);

            //insert header
            String pk_header = locSessionBean.insertImportDrugsHeader(dto_header, supplierid);
            
            //insert details
            for (TbImDurgsSuppliesDetails item : list_details) {
                locSessionBean.insertImportDrugsDetails(item, pk_header);
                
                //insert tang kho
                TbWhouse dto_whouse = new TbWhouse();
                dto_whouse.setItemCode(item.getItemCode());
                dto_whouse.setPackage1(pk_header);
                dto_whouse.setWarDate(item.getWarDate());
                dto_whouse.setOnHand(item.getQuantity());
                dto_whouse.setQtyAvailable(item.getQuantity());
                dto_whouse.setQtyOrder(0);
                locSessionBean.insertWarehouse(dto_whouse);
                
            }
            
            request.getRequestDispatcher("Import Drugs Management.jsp").forward(request, response);
            

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
