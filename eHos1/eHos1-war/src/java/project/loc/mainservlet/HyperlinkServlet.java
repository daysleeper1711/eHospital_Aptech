/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project.loc.mainservlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import project.ett.TbItemCodeManagement;
import project.ett.TbSupplier;
import project.locssbean.LocSessionBean;

/**
 *
 * @author locth-laptop
 */
public class HyperlinkServlet extends HttpServlet {
    @EJB
    private LocSessionBean locSessionBean;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String action = request.getParameter("action");
            String web = "";
            
            if (action.equals("Import Drugs")) {
                web = "Import Drugs.jsp";
                
                List<TbSupplier> list = new ArrayList<TbSupplier>();
                list = locSessionBean.findbyAllSupplier();
                request.setAttribute("list", list);
                
                //Lay Name Itemcode
                List<TbItemCodeManagement> listitemcode = locSessionBean.findbyAllItemCode();
                request.setAttribute("listITEMCODE", listitemcode);
            }
            else if (action.equals("Add New ItemCode")) {
                web = "Add ItemCode.jsp";
            }
            else if (action.equals("ItemCode Management")) {
                web = "ItemCode Management.jsp";
            }
            else if (action.equals("Export Drugs Management")) {
                web = "Export Drugs Management.jsp";
            }
            else if (action.equals("Import Drugs Management")) {
                web = "Import Drugs Management.jsp";
            }
            else if (action.equals("Find Warehouse")) {
                web = "Find Warehouse.jsp";
            }
            else if (action.equals("Supplier Management")) {
                web = "Supplier Management.jsp";
            }
            else if (action.equals("ReportImport")) {
                web = "ReportImportServlet";
            }
            else if (action.equals("ReportExport")) {
                web = "ReportExportServlet";
            }
            request.getRequestDispatcher(web).forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
