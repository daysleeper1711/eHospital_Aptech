/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project.loc.mainservlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import project.ett.TbImDurgsSupplies;
import project.ett.TbSupplier;
import project.locdto.Supplier_ImDrugsHeader_DTO;
import project.locssbean.LocSessionBean;

/**
 *
 * @author locth-laptop
 */
public class SearchImportDrugsServlet extends HttpServlet {

    @EJB
    private LocSessionBean locSessionBean;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String action = request.getParameter("optionsRadiosInline");

            List<Supplier_ImDrugsHeader_DTO> list = new ArrayList<Supplier_ImDrugsHeader_DTO>();
            
            if (action.equals("Package")) {
                String pkpackage = request.getParameter("txtPackage");

                TbImDurgsSupplies dto_im = locSessionBean.findbyPackage_ImportDrugsHeader(pkpackage);
                TbSupplier dto_Sup = dto_im.getSupplierID();
                if (dto_im.getUStatus().equals("F")) {
                    dto_im.setUStatus("Finish");
                } else {
                    dto_im.setUStatus("Open");
                }
                Supplier_ImDrugsHeader_DTO dto_all = new Supplier_ImDrugsHeader_DTO();
                dto_all.setS(dto_Sup);
                dto_all.setI(dto_im);
                list.add(dto_all);
            } else if (action.equals("NumBill")) {
                String numbill = request.getParameter("txtNumBill");
                List<TbImDurgsSupplies> list_im = locSessionBean.findbyNumBill_ImportDrugsHeader(numbill);
                for (TbImDurgsSupplies i : list_im) {
                    TbSupplier dto_Sup = i.getSupplierID();
                    if (i.getUStatus().equals("F")) {
                        i.setUStatus("Finish");
                    } else {
                        i.setUStatus("Open");
                    }
                    Supplier_ImDrugsHeader_DTO dto_all = new Supplier_ImDrugsHeader_DTO();
                    dto_all.setS(dto_Sup);
                    dto_all.setI(i);
                    list.add(dto_all);
                }
            } else if (action.equals("Date")) {
                String fromdate = request.getParameter("txtFromDate");
                //Xu ly tu ngay
                String fromdatedetails[] = fromdate.split("-");
                Date start = new Date(Integer.parseInt(fromdatedetails[0])-1900, Integer.parseInt(fromdatedetails[1])-1, Integer.parseInt(fromdatedetails[2]),0,0,0);
                String todate = request.getParameter("txtToDate");
                //Xu ly den ngay
                String todatedetails[] = todate.split("-");
                Date end = new Date(Integer.parseInt(todatedetails[0])-1900, Integer.parseInt(todatedetails[1])-1, Integer.parseInt(todatedetails[2]),23,59,59);
                List<TbImDurgsSupplies> list_im = locSessionBean.findbyTime_ImportDrugsHeader(start, end);

                for (TbImDurgsSupplies i : list_im) {
                    TbSupplier dto_Sup = i.getSupplierID();
                    if (i.getUStatus().equals("F")) {
                        i.setUStatus("Finish");
                    } else {
                        i.setUStatus("Open");
                    }
                    Supplier_ImDrugsHeader_DTO dto_all = new Supplier_ImDrugsHeader_DTO();
                    dto_all.setS(dto_Sup);
                    dto_all.setI(i);
                    list.add(dto_all);
                }
            }

            if (list != null) {
                request.setAttribute("list", list);
            }
            request.getRequestDispatcher("Import Drugs Management.jsp").forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
