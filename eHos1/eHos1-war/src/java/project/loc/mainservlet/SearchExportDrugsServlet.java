/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project.loc.mainservlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import project.ett.TbExDurgsSupplies;
import project.locssbean.LocSessionBean;

/**
 *
 * @author locth-laptop
 */
public class SearchExportDrugsServlet extends HttpServlet {
    @EJB
    private LocSessionBean locSessionBean;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String action = request.getParameter("optionsRadiosInline");
            
            List<TbExDurgsSupplies> list = new ArrayList<TbExDurgsSupplies>();
            if (action.equals("Docentry")) {
                String docentry = request.getParameter("txtdocentry");
                String type = request.getParameter("selectType");
                list = locSessionBean.findbyDocentry_Type_ExportDrugsHeader(docentry, type);
            }else if (action.equals("Status")) {
                String status = request.getParameter("selectStatus");
                
                list = locSessionBean.findbyStatus_ExportDrugsHeader(status);
            }else{
                String fromdate = request.getParameter("txtFromDate");
                //Xu ly tu ngay (-1 do thoi gian dung ko)
                String fromdatedetails[] = fromdate.split("-");
                Date start = new Date(Integer.parseInt(fromdatedetails[0])-1900, Integer.parseInt(fromdatedetails[1])-1, Integer.parseInt(fromdatedetails[2]),0,0,0);
                String todate = request.getParameter("txtToDate");
                //Xu ly den ngay (+1 do thoi gian dung ko)
                String todatedetails[] = todate.split("-");
                Date end = new Date(Integer.parseInt(todatedetails[0])-1900, Integer.parseInt(todatedetails[1])-1, Integer.parseInt(todatedetails[2]),23,59,59);
                
                list = locSessionBean.findbyTime_ExportDrugsHeader(start,end);
            }
            
            if (list != null) {
                for (TbExDurgsSupplies item : list) {
                    //Cap nhat type - updateby
                    if (item.getTypeEx().equals("S")) {
                        item.setUpdateby("Surgery");
                    }else{
                        item.setUpdateby("Prescription");
                    }
                    //Cap nhap status - note
                    if (item.getUStatus().equals("O")) {
                        item.setNote("Open");
                    }else{
                        item.setNote("Finish");
                    }
                    //Cap nhat ngay - createby
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    item.setCreateby(sdf.format(item.getCrDate())); 
                }
                request.setAttribute("list", list);
            }
            
            request.getRequestDispatcher("Export Drugs Management.jsp").forward(request, response);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
