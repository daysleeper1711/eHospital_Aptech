/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project.loc.mainservlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import project.ett.TbSupplier;
import project.locssbean.LocSessionBean;

/**
 *
 * @author locth-laptop
 */
public class UpdateSupplierServlet extends HttpServlet {
    @EJB
    private LocSessionBean locSessionBean;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String action = request.getParameter("action");
            String web = "";

            if (action.equals("Edit")) {
                String id = request.getParameter("txtID");
                TbSupplier dto = new TbSupplier();
                dto = locSessionBean.findbySupplierID(id);
                
                web = "Edit Supplier.jsp";
                request.setAttribute("SupplierID", dto.getSupplierID());
                request.setAttribute("SupplierName", dto.getSupplierName());
                request.setAttribute("UStatus", dto.getUStatus());
            }else if (action.equals("Update")) {
                TbSupplier dto = new TbSupplier();
                dto.setSupplierID(request.getParameter("txtSupplierID"));
                System.out.println(request.getParameter("txtSupplierID"));
                dto.setSupplierName(request.getParameter("txtSupplierName"));
                String status = request.getParameter("select");
                if (status.equals("Action")) {
                    status = "A";
                }else{
                    status = "I";
                }
                dto.setUStatus(status);
                
                locSessionBean.updateSupplier(dto);
                web = "Supplier Management.jsp";
            }
            
            request.getRequestDispatcher(web).forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
