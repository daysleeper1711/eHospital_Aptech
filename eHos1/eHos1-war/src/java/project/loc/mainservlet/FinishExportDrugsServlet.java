/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project.loc.mainservlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import phuong.ssb.PhuongSessionBean;
import project.ett.TbExDurgsSupplies;
import project.ett.TbExDurgsSuppliesDetails;
import project.ett.TbItemCodeManagement;
import project.locdto.ExDurgsSuppliesHeader_Details_DTO;
import project.locssbean.LocSessionBean;

/**
 *
 * @author locth-laptop
 */
public class FinishExportDrugsServlet extends HttpServlet {

    @EJB
    private PhuongSessionBean phuongSessionBean;

    @EJB
    private LocSessionBean locSessionBean;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String pkExHeader = request.getParameter("txtEXPO");
            String userID = request.getParameter("txtUserID");
            String userName = request.getParameter("txtUserName");

            //Cap nhat trang thai va kho
            TbExDurgsSupplies dto_ExHeader = locSessionBean.finishExportWhouse(pkExHeader);

            //Lay chung tu cua phieu va cap trang thai cua toa thuoc
            String toathuocid = dto_ExHeader.getDocentry();
            if (dto_ExHeader.getTypeEx().equals("P")) {
                //Cat String
                String id[] = toathuocid.split("_");
                if (id[0].equals("Pres")) {
                    phuongSessionBean.updatePresStatusBookMed(toathuocid);
                } else {
                    phuongSessionBean.updateSuppStatusBookMed(toathuocid);
                }
            }

            //Quay lai trang Export Details
            String web = "";

            TbExDurgsSupplies header = new TbExDurgsSupplies();
            List<TbExDurgsSuppliesDetails> details = new ArrayList<TbExDurgsSuppliesDetails>();

            
            
            ExDurgsSuppliesHeader_Details_DTO dto = locSessionBean.findbyPK_ExportDrugsHeader(dto_ExHeader.getExID());
            header = dto.getHeader();
            
            //Set Status
            String status = "",sfromdate = "",stodate = "",type = "";
            if (header.getUStatus().equals("F")) {
                status = "Finish";
            } else {
                status = "Open";
            }
            //Update ngay gio           
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            sfromdate = sdf.format(header.getCrDate());
            stodate = sdf.format(header.getUpDate());
            //Cap nhat type - updateby
            if (header.getTypeEx().equals("S")) {
                type = "Surgery";
            } else {
                type = "Prescription";
            }

            details = dto.getDetails();

            for (TbExDurgsSuppliesDetails item : details) {

                //Update ngay gio           
                Date tempdate = item.getWarDate();
                item.setExDetails(sdf.format(tempdate));
            }
            web = "Export Drugs Details.jsp";

            if (header != null && details != null) {

                request.setAttribute("header", header);
                request.setAttribute("header_status", status);
                request.setAttribute("header_sfromdate", sfromdate);
                request.setAttribute("header_stodate", stodate);
                request.setAttribute("header_type", type);

                request.setAttribute("listdetails", details);
                //Lay Name Itemcode
                List<TbItemCodeManagement> listitemcode = locSessionBean.findbyAllItemCode();
                request.setAttribute("listITEMCODE", listitemcode);
            }
            
            request.getRequestDispatcher(web).forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
