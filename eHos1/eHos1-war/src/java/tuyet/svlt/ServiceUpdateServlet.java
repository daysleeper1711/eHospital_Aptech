/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tuyet.svlt;

import dan.dto.EmpInfo;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import project.ett.TbService;
import project.ett.TbServiceGroup;
import tuyet.dto.TuyetServiceDTO;
import tuyet.ssb.TuyetSessionBean;

/**
 *
 * @author Hoang Tuyet
 */
public class ServiceUpdateServlet extends HttpServlet {

    @EJB
    private TuyetSessionBean tuyetSSB;
    private static final String fail = "/TuyetGUI/Fail.jsp";
    private static final String updateServiceJSP = "/TuyetGUI/ServiceUpdate.jsp";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            HttpSession session = request.getSession();
            String action = request.getParameter("action");
            int serviceID = Integer.parseInt(request.getParameter("id"));
            String url = fail;
            //set list group
            List<TbServiceGroup> listGroup = tuyetSSB.getAllGroup();
            request.setAttribute("listGroup", listGroup);

            if (action.equals("serviceDetail")) {
                TuyetServiceDTO dto = tuyetSSB.findServiceByID(serviceID);
                request.setAttribute("serviceDTO", dto);
                url = updateServiceJSP;
            } else if (action.equals("btnUpdateService")) {
                //current values of updated service
                TuyetServiceDTO curServiceDTO = tuyetSSB.findServiceByID(serviceID);
                //create new value for updated service
                TbService service = new TbService();

                String name = request.getParameter("txtName");
                String content = request.getParameter("txtContent");
                double price = Double.parseDouble(request.getParameter("txtPrice"));
                String measure = request.getParameter("txtMeasure");
                int groupID = Integer.parseInt(request.getParameter("cbGroup"));
                String status = request.getParameter("cbStatus");
                boolean stt = false;
                if (status.equals("1")) {
                    stt = true;
                }
                EmpInfo emp = (EmpInfo) session.getAttribute("empInfo");
                String empID = emp.getEmployeeID();
                //check name is unique
                String dupErr = "";
                List<TbService> listservice = tuyetSSB.getAll();
                if (listservice != null) {
                    for (TbService sv : listservice) {
                        if (sv.getServiceName().toLowerCase().equals(name.toLowerCase()) && !sv.getServiceID().equals(serviceID)) {
                            request.setAttribute("msg", "Service name '" + name + "' is exist !");
                            request.setAttribute("serviceDTO", curServiceDTO);
                            dupErr = "duplicate name";
                            url = updateServiceJSP;
                            request.getRequestDispatcher(url).forward(request, response);
                        }
                    }
                }
                if (dupErr.equals("")) {
                    //set values
                    service.setServiceID(serviceID);
                    service.setServiceName(name);
                    service.setServiceContent(content);
                    service.setUnitOfMea(measure);
                    service.setUpdateDate(new Date());

                    if (tuyetSSB.updateService(groupID, empID, service)) {
                        //insert new price
                        if (curServiceDTO.getCurPrice() != price) {
                            tuyetSSB.insertPrice(serviceID, empID, price);
                        }
                        //update status
                        tuyetSSB.updateServiceStatus(serviceID, empID, stt);
                        url = updateServiceJSP;
                        //set new value for view update
                        TuyetServiceDTO newDTO = tuyetSSB.findServiceByID(serviceID);
                        request.setAttribute("serviceDTO", newDTO);
                        request.setAttribute("msg", "Updated successfully !");
                        request.setAttribute("t", '1');
                    }
                }
            }

            request.getRequestDispatcher(url).forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
