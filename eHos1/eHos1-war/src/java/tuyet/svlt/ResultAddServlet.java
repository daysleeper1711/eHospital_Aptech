/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tuyet.svlt;

import dan.dto.EmpInfo;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import project.ett.TbMedicalRecord;
import project.ett.TbOutPatient;
import project.ett.TbPatient;
import project.ett.TbStandardValue;
import project.ett.TbTestReqDetail;
import project.ett.TbTestResult;
import project.ett.TbTestResultDetail;
import tuyet.dto.ResultDTO;
import tuyet.dto.TuyetRequestDTO;
import tuyet.ssb.TuyetSessionBean;

/**
 *
 * @author Hoang Tuyet
 */
public class ResultAddServlet extends HttpServlet {

    @EJB
    private TuyetSessionBean tuyetSSB;

    private static final String manage = "ResultManagementServlet";
    private static final String add = "ResultAddServlet";
    private static final String fail = "/TuyetGUI/Fail.jsp";
    private static final String addJSP = "/TuyetGUI/ResultCreate.jsp";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            String url = fail;

            HttpSession session = request.getSession();
            EmpInfo emp = (EmpInfo) session.getAttribute("empInfo");

            String action = request.getParameter("action");
            String testID = request.getParameter("requestID");
            TuyetRequestDTO test = tuyetSSB.findByTestID(testID);

            if (action.equals("btnAddResult")) {
                String adID = "";
                TbPatient pat = new TbPatient();
                TbMedicalRecord mr = test.getRequest().getMrId();
                TbOutPatient opt = test.getRequest().getOutPatID();
                if (mr != null) {
                    adID = mr.getMedRecordID();
                    pat = mr.getPatID();
                } else {
                    adID = opt.getOutPatID();
                    pat = mr.getPatID();
                }
                //cal age
                int yearb = pat.getDob().getYear();
                Date now = new Date();
                int curYear = now.getYear();
                int age = curYear - yearb;

                //date complete test
                session.setAttribute("compDate", new Date());

                //list resultDTO to show interface
                List<ResultDTO> showDataList = new ArrayList<ResultDTO>();
                //--set value for showDataList
                List<TbTestReqDetail> listReqDetail = test.getReqDetails();
                for (TbTestReqDetail item : listReqDetail) {
                    ResultDTO data = new ResultDTO();
                    data.setServiceName(item.getServiceID().getServiceName());
                    data.setGroupName(item.getServiceID().getGroupID().getGroupName());
                    data.setUnitOfMea(item.getServiceID().getUnitOfMea());
                    data.setReqDetailID(item.getTestReqDetailID());
                    List<TbStandardValue> listSV = (List<TbStandardValue>) item.getServiceID().getTbStandardValueCollection();
                    if (listSV.size() > 0) {
                        TbStandardValue stand = listSV.get(listSV.size() - 1);
                        data.setSvMale(stand.getSVmale());
                        data.setSvfMale(stand.getSVfemale());
                    } else {
                        data.setSvMale(null);
                        data.setSvfMale(null);
                    }

                    showDataList.add(data);
                }

                request.setAttribute("age", age);
                request.setAttribute("testID", testID);
                request.setAttribute("adID", adID);
                request.setAttribute("pat", pat);
                request.setAttribute("showDataList", showDataList);
                request.setAttribute("test", test);
                url = addJSP;

            } else if (action.equals("btnInsert")) {
                Date compdate = (Date) session.getAttribute("compDate");
                //insert result
                TbTestResult rs = new TbTestResult();
                rs.setCreateDate(compdate);
                rs.setStatus(Boolean.TRUE);//status 'New'
                if (tuyetSSB.insertResult(testID, emp.getEmployeeID(), rs)) {
                    //update status of request
                    tuyetSSB.updateTestStatus(testID, "Wait-Conclude");
                    //insert detail
                    TuyetRequestDTO dto = tuyetSSB.findByTestID(testID);
                    List<TbTestResult> listRS = dto.getResult();
                    int resultID = listRS.get(listRS.size() - 1).getTestResultID();

                    List<TbTestReqDetail> listSV = test.getReqDetails();
                    for (TbTestReqDetail item : listSV) {
                        TbTestResultDetail detail = new TbTestResultDetail();
                        String kq = request.getParameter(item.getTestReqDetailID().toString());
                        detail.setResults(kq);
                        detail.setCreateDate(compdate);
                        tuyetSSB.insertResultDetail(resultID, emp.getEmployeeID(), item.getTestReqDetailID(), detail);
                    }

                    session.removeAttribute("compDate");
                    request.setAttribute("msg", "Inserted result successfully");
                    request.setAttribute("t", "1");
                    url = manage;
                }
            }

            request.getRequestDispatcher(url).forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
