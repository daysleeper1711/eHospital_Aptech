/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tuyet.svlt;

import dan.dto.EmpInfo;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.jasper.tagplugins.jstl.ForEach;
import project.ett.TbMedicalRecord;
import project.ett.TbOutPatient;
import project.ett.TbPatient;
import project.ett.TbTestResult;
import project.ett.TbTestResultDetail;
import tuyet.dto.ResultDTO;
import tuyet.dto.TuyetRequestDTO;
import tuyet.dto.TuyetServiceDTO;
import tuyet.ssb.TuyetSessionBean;

/**
 *
 * @author Hoang Tuyet
 */
public class ResultUpdateServlet extends HttpServlet {

    @EJB
    private TuyetSessionBean tuyetSSB;

    private static final String fail = "/TuyetGUI/Fail.jsp";
    private static final String updateJSP = "/TuyetGUI/ResultUpdate.jsp";
    private static final String reportJSP = "/TuyetGUI/ResultReport.jsp";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            String url = fail;

            HttpSession session = request.getSession();
            EmpInfo emp = (EmpInfo) session.getAttribute("empInfo");

            String ps = request.getParameter("pSign");
            String action = request.getParameter("action");
            String testID = request.getParameter("requestID");
            TuyetRequestDTO test = tuyetSSB.findByTestID(testID);
            if (action.equals("btnViewResult")) {
                
                String patType = "inpatient";
                String adID = "";
                TbPatient pat = new TbPatient();
                TbMedicalRecord mr = test.getRequest().getMrId();
                TbOutPatient opt = test.getRequest().getOutPatID();
                if (mr != null) {
                    adID = mr.getMedRecordID();
                    pat = mr.getPatID();
                } else {
                    adID = opt.getOutPatID();
                    pat = mr.getPatID();
                    patType = "outpatient";
                }
                //cal age
                int yearb = pat.getDob().getYear();
                Date now = new Date();
                int curYear = now.getYear();
                int age = curYear - yearb;

                //create list resultDTO to save results
                List<ResultDTO> showResultList = new ArrayList<ResultDTO>();
                //--take list result of request (1 request - 1 result)
                List<TbTestResult> listRS = test.getResult();
                //--take newest result
                TbTestResult rs = listRS.get(listRS.size() - 1);
                List<TbTestResultDetail> listDetail = (List<TbTestResultDetail>) rs.getTbTestResultDetailCollection();
                //--compare standard
                for (TbTestResultDetail item : listDetail) {
                    ResultDTO rsDTO = new ResultDTO();
                    rsDTO.setServiceName(item.getReqDetailID().getServiceID().getServiceName());
                    rsDTO.setGroupName(item.getReqDetailID().getServiceID().getGroupID().getGroupName());
                    rsDTO.setResult(item.getResults());
                    rsDTO.setUnitOfMea(item.getReqDetailID().getServiceID().getUnitOfMea());

                    int idService = item.getReqDetailID().getServiceID().getServiceID();
                    TuyetServiceDTO dto = tuyetSSB.findServiceByID(idService);
                    if (!pat.getGender()) {
                        String svMale = dto.getSvMale();
                        if (svMale != null) {
                            rsDTO.setSvMale(svMale);
                            if (svMale.contains(":")) {
                                String[] val = svMale.split(":");
                                float min = Float.parseFloat(val[0]);
                                float max = Float.parseFloat(val[1]);
                                float result = Float.parseFloat(item.getResults());
                                if (result < min) {
                                    rsDTO.setCpStandard("lt");
                                } else if (result > max) {
                                    rsDTO.setCpStandard("gt");
                                } else {
                                    rsDTO.setCpStandard("eq");
                                }
                            } else {
                                float sd = Float.parseFloat(svMale);
                                float result = Float.parseFloat(item.getResults());
                                if (result < sd) {
                                    rsDTO.setCpStandard("lt");
                                } else if (result > sd) {
                                    rsDTO.setCpStandard("gt");
                                } else {
                                    rsDTO.setCpStandard("eq");
                                }
                            }
                        } else {
                            //if standard == null
                            rsDTO.setCpStandard("eq");
                        }

                    } else {
                        //gender is female
                        String svfMale = dto.getSvfMale();
                        if (svfMale != null) {
                            rsDTO.setSvfMale(svfMale);
                            if (svfMale.contains(":")) {
                                String[] val = svfMale.split(":");
                                float min = Float.parseFloat(val[0]);
                                float max = Float.parseFloat(val[1]);
                                float result = Float.parseFloat(item.getResults());
                                if (result < min) {
                                    rsDTO.setCpStandard("lt");
                                } else if (result > max) {
                                    rsDTO.setCpStandard("gt");
                                } else {
                                    rsDTO.setCpStandard("eq");
                                }
                            } else {
                                float sd = Float.parseFloat(svfMale);
                                float result = Float.parseFloat(item.getResults());
                                if (result < sd) {
                                    rsDTO.setCpStandard("lt");
                                } else if (result > sd) {
                                    rsDTO.setCpStandard("gt");
                                } else {
                                    rsDTO.setCpStandard("eq");
                                }
                            }
                        } else {
                            //if standard == null
                            rsDTO.setCpStandard("eq");
                        }
                    }
                    showResultList.add(rsDTO);
                }//end for

                request.setAttribute("showResultList", showResultList);
                request.setAttribute("patType", patType);
                request.setAttribute("age", age);
                request.setAttribute("testID", testID);
                request.setAttribute("adID", adID);
                request.setAttribute("pat", pat);
                request.setAttribute("test", test);
                request.setAttribute("rs", rs);
                request.setAttribute("ps", ps);

                url = updateJSP;

            } else if (action.equals("btnSave")) {
                if (!emp.getPosition().equals("doctor") && !emp.getPosition().equals("doctorv2")) {
                    //check user
                    request.setAttribute("msg", "Sorry! You isn't allowed to update this test result.");
                } else {
                    String conc = request.getParameter("txtConclusion");
                    String note = request.getParameter("txtNote");
                    if (conc.trim().equals("")) {
                        request.setAttribute("msg", "Conclusion is required !");
                    } else {
                        //--take list result of request (1 request - 1 result)
                        List<TbTestResult> listRS = test.getResult();
                        //--take newest result
                        TbTestResult result = listRS.get(listRS.size() - 1);

                        if (tuyetSSB.updateResult(result.getTestResultID(), emp.getEmployeeID(), conc, note)) {
                            tuyetSSB.updateTestStatus(testID, "Done");
                            request.setAttribute("msg", "Updated test result successfully !");
                            request.setAttribute("t", "1");
                        }
                    }
                }
                url = "ResultUpdateServlet?action=btnViewResult";

            } else if (action.equals("btnPrint")) {
                String patType = "inpatient";
                String adID = "";
                TbPatient pat = new TbPatient();
                TbMedicalRecord mr = test.getRequest().getMrId();
                TbOutPatient opt = test.getRequest().getOutPatID();
                if (mr != null) {
                    adID = mr.getMedRecordID();
                    pat = mr.getPatID();
                } else {
                    adID = opt.getOutPatID();
                    pat = mr.getPatID();
                    patType = "outpatient";
                }
                //cal age
                int yearb = pat.getDob().getYear();
                Date now = new Date();
                int curYear = now.getYear();
                int age = curYear - yearb;

                //create list resultDTO to save results
                List<ResultDTO> showResultList = new ArrayList<ResultDTO>();
                //--take list result of request (1 request - 1 result)
                List<TbTestResult> listRS = test.getResult();
                //--take newest result
                TbTestResult rs = listRS.get(listRS.size() - 1);
                List<TbTestResultDetail> listDetail = (List<TbTestResultDetail>) rs.getTbTestResultDetailCollection();
                //--compare standard
                for (TbTestResultDetail item : listDetail) {
                    ResultDTO rsDTO = new ResultDTO();
                    rsDTO.setServiceName(item.getReqDetailID().getServiceID().getServiceName());
                    rsDTO.setGroupName(item.getReqDetailID().getServiceID().getGroupID().getGroupName());
                    rsDTO.setResult(item.getResults());
                    rsDTO.setUnitOfMea(item.getReqDetailID().getServiceID().getUnitOfMea());

                    int idService = item.getReqDetailID().getServiceID().getServiceID();
                    TuyetServiceDTO dto = tuyetSSB.findServiceByID(idService);
                    if (!pat.getGender()) {
                        String svMale = dto.getSvMale();
                        if (svMale != null) {
                            rsDTO.setSvMale(svMale);
                            if (svMale.contains(":")) {
                                String[] val = svMale.split(":");
                                float min = Float.parseFloat(val[0]);
                                float max = Float.parseFloat(val[1]);
                                float result = Float.parseFloat(item.getResults());
                                if (result < min) {
                                    rsDTO.setCpStandard("lt");
                                } else if (result > max) {
                                    rsDTO.setCpStandard("gt");
                                } else {
                                    rsDTO.setCpStandard("eq");
                                }
                            } else {
                                float sd = Float.parseFloat(svMale);
                                float result = Float.parseFloat(item.getResults());
                                if (result < sd) {
                                    rsDTO.setCpStandard("lt");
                                } else if (result > sd) {
                                    rsDTO.setCpStandard("gt");
                                } else {
                                    rsDTO.setCpStandard("eq");
                                }
                            }
                        } else {
                            //if standard == null
                            rsDTO.setCpStandard("eq");
                        }

                    } else {
                        //gender is female
                        String svfMale = dto.getSvfMale();
                        if (svfMale != null) {
                            rsDTO.setSvfMale(svfMale);
                            if (svfMale.contains(":")) {
                                String[] val = svfMale.split(":");
                                float min = Float.parseFloat(val[0]);
                                float max = Float.parseFloat(val[1]);
                                float result = Float.parseFloat(item.getResults());
                                if (result < min) {
                                    rsDTO.setCpStandard("lt");
                                } else if (result > max) {
                                    rsDTO.setCpStandard("gt");
                                } else {
                                    rsDTO.setCpStandard("eq");
                                }
                            } else {
                                float sd = Float.parseFloat(svfMale);
                                float result = Float.parseFloat(item.getResults());
                                if (result < sd) {
                                    rsDTO.setCpStandard("lt");
                                } else if (result > sd) {
                                    rsDTO.setCpStandard("gt");
                                } else {
                                    rsDTO.setCpStandard("eq");
                                }
                            }
                        } else {
                            //if standard == null
                            rsDTO.setCpStandard("eq");
                        }
                    }
                    showResultList.add(rsDTO);

                }//end for

                request.setAttribute("showResultList", showResultList);
                request.setAttribute("patType", patType);
                request.setAttribute("age", age);
                request.setAttribute("testID", testID);
                request.setAttribute("adID", adID);
                request.setAttribute("pat", pat);
                request.setAttribute("test", test);
                request.setAttribute("rs", rs);
                request.setAttribute("ps", ps);

                url = reportJSP;
            }
            request.getRequestDispatcher(url).forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
