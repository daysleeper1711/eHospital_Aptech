/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tuyet.svlt;

import dan.dto.EmpInfo;
import dan.ssb.DanSessionBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import project.ett.TbEmployee;
import project.ett.TbTestReqDetail;
import project.ett.TbTestRequest;
import tuyet.dto.TuyetCartBean;
import tuyet.dto.TuyetCartServiceDTO;
import tuyet.dto.TuyetInfoTestDTO;
import tuyet.ssb.TuyetSessionBean;

/**
 *
 * @author Hoang Tuyet
 */
public class TuyetCreateReqServlet extends HttpServlet {
    @EJB
    private DanSessionBean danSSB;

    @EJB
    private TuyetSessionBean tuyetSSB;

    private static final String create = "/TuyetGUI/CreateTestRequest.jsp";
    private static final String reqManagement = "TuyetSearchReqServlet?txtSearchAddID=";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            String action = request.getParameter("action");
            String url = "/TuyetGUI/Fail.jsp";
            HttpSession session = request.getSession();
            if (action.equals("btnCreateRequest")) {
                String ad_ID = request.getParameter("txtAdmission");
                String patType = request.getParameter("patType");
                String patName = request.getParameter("txtPatName");
                String patAddr = request.getParameter("txtAddr");
                String patAge = request.getParameter("txtAge");
                String patGender = request.getParameter("txtGender");
                String frFac = request.getParameter("txtFrFac");
                String toFac = "Examination";
                String diagnosis = "";
                //set session for admissionID, detroy it when click send request or cancel

                //set value for session var infoTest
                TuyetInfoTestDTO infoTest = new TuyetInfoTestDTO();
                infoTest.setAdmissionID(ad_ID);
                infoTest.setPatName(patName);
                infoTest.setPatType(patType);
                infoTest.setPatAge(patAge);
                infoTest.setPatAddr(patAddr);
                infoTest.setPatGender(patGender);
                infoTest.setFrFac(frFac);
                infoTest.setToFac(toFac);
                infoTest.setDiagnosis(diagnosis);
                session.setAttribute("infoTest", infoTest);

                url = create;
            } else if (action.equals("btnCancel")) {
                TuyetInfoTestDTO infoTest = (TuyetInfoTestDTO) session.getAttribute("infoTest");
                url = reqManagement + infoTest.getAdmissionID() + "&optionsRadiosInline=" + infoTest.getPatType();
                session.removeAttribute("infoTest");
                if (session.getAttribute("cart") != null) {
                    session.removeAttribute("cart");
                }
            } else if (action.equals("btnSendReq")) {
                String diagnosis = request.getParameter("txtDiagnosis");
                //add request to database
                TuyetCartBean cbean = (TuyetCartBean) session.getAttribute("cart");
                TuyetInfoTestDTO infoTest = (TuyetInfoTestDTO) session.getAttribute("infoTest");
                EmpInfo empInfo = (EmpInfo) session.getAttribute("empInfo");
                //--set new value of diagnosis for infoTest
                infoTest.setDiagnosis(diagnosis);
                //--check empty cart service
                if (cbean == null ||cbean.getServiceCart().isEmpty()) {
                    request.setAttribute("msgCheckCart", "Please, choose services for test!");
                    url = "/TuyetGUI/CreateTestRequest.jsp";

                } else {
                    //--check deposit
                    double deposit = tuyetSSB.getDeposit(infoTest.getAdmissionID());
                    if(!danSSB.checkDepositPayment(cbean.totalPayment(), infoTest.getAdmissionID())) {
                        request.setAttribute("msgCheckDeposit", "*Deposit is <b> $" + deposit + "</b>. It's not enough for payment.");
                        url = "/TuyetGUI/CreateTestRequest.jsp";
                    } else {

                        //--take value medical record id
                        String mrID = "";
                        if (infoTest.getPatType().equals("inpatient")) {
                            mrID = infoTest.getAdmissionID();
                        }
                        //--take value outpatient id
                        String opID = "";
                        if (infoTest.getPatType().equals("outpatient")) {
                            opID = infoTest.getAdmissionID();
                        }
                        //--value of current Treatment Faculty
                        String frfac = tuyetSSB.findbyFacName(infoTest.getFrFac());
                        //--Examination faculty ID
                        String toFac = tuyetSSB.findbyFacName(infoTest.getToFac());
                        //--take value for doctor appoint
                        String empID = empInfo.getEmployeeID();

                        //--set other value for test request
                        String req_ID = tuyetSSB.identityTestID();
                        TbTestRequest testReq = new TbTestRequest();
                        testReq.setTestReqID(req_ID);
                        testReq.setDiagnosis(infoTest.getDiagnosis());
                        testReq.setTotalPayment(cbean.totalPayment());
                        Date createDate = new Date();
                        testReq.setCreateDate(createDate);
                        testReq.setStatus("New");
                        //--insert test request
                        if (tuyetSSB.insertTestRequest(testReq, mrID, opID, frfac, toFac, empID)) {

                            //insert test detail
                            for (TuyetCartServiceDTO dto : cbean.getServiceCart()) {
                                int serviceID = dto.getServiceID();
                                double price = dto.getUnitPrice();
                                tuyetSSB.insertReqDetail(req_ID, serviceID, price, empID);
                            }

                            url = reqManagement + infoTest.getAdmissionID() + "&optionsRadiosInline=" + infoTest.getPatType();
                            session.removeAttribute("infoTest");
                            session.removeAttribute("cart");
                        }//end if
                    }//end check deposit
                }//end check cart
            }
            request.getRequestDispatcher(url).forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
