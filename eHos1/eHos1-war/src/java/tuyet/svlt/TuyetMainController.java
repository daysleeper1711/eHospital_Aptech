/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tuyet.svlt;

import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import tuyet.dto.TuyetInfoTestDTO;
import tuyet.ssb.TuyetSessionBean;

/**
 *
 * @author Hoang Tuyet
 */
public class TuyetMainController extends HttpServlet {

    private static final String fail = "/TuyetGUI/Fail.jsp";
    private static final String createReqJSP = "/TuyetGUI/CreateTestRequest.jsp";
    private static final String addService2Test = "/TuyetGUI/AddServiceToTest.jsp";
    private static final String updateJSP = "/TuyetGUI/TestRequestUpdate.jsp";

    private static final String searchReq = "TuyetSearchReqServlet";
    private static final String createReq = "TuyetCreateReqServlet";
    private static final String searchService = "TuyetSearchServiceServlet";
    private static final String addToCart = "TuyetAddService2TestServlet";
    private static final String removeFromCart = "TuyetRemoveServiceFromTestServlet";
    private static final String updateReq = "TuyetUpdateReqServlet";
    private static final String deleteReq = "TuyetDeleteReqServlet";
    private static final String detailResult = "ResultUpdateServlet";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            String action = request.getParameter("action");
            String destination = fail;
            if (action.equals("btnSearchRequest")) {
                //btn search at test request management page
                destination = searchReq;
            } else if (action.equals("btnCancelCreateReq")) {
                //btn cancel at createTestRequest page or TestRequestUpdate page
                destination = createReq + "?action=btnCancel";
            } else if (action.equals("btnCreateRequest")) {
                //btn Create new at Test request management page
                destination = createReq;
            } else if (action.equals("btnChooseService")) {
                //btn choose service at CreateTestRequest.jsp
                String diagn = request.getParameter("saveDiagnosis");
                if (!diagn.equals("")) {
                    HttpSession session = request.getSession();
                    TuyetInfoTestDTO infoTest = (TuyetInfoTestDTO) session.getAttribute("infoTest");
                    infoTest.setDiagnosis(diagn);
                }
                destination = searchService;
            } else if (action.equals("btnFindService") || action.equals("btnShowAllService")) {
                //btn search/show-all at AddServiceToTest.jsp
                destination = searchService;
            } else if (action.equals("btnAddSV2Test")) {
                //btn Add service at AddServiceToTest.jsp
                destination = addToCart;
            } else if (action.equals("btnBack")) {
                //btn back at AddServiceToTest.jsp
                String ps = request.getParameter("pSignal");
                if (ps.equals("0")) {
                    destination = createReqJSP;
                } else {
                    destination = updateJSP;
                }

            } else if (action.equals("btnRemoveFromCart")) {
                //btn Remove at CreateTestRequest.jsp
                destination = removeFromCart;
            } else if (action.equals("btnSendReq")) {
                //btn Send request at CreateTestRequest.jsp
                destination = createReq;
            } else if (action.equals("btnEditRequest") || action.equals("btnSaveReq")) {
                //btn Edit at TestRequestManagement.jsp or btn Save Changes at TestRequestUpdate page
                destination = updateReq;
            } else if (action.equals("btnCancelEditReq")) {
                //btn cancel at createTestRequest page or TestRequestUpdate page
                destination = updateReq;
            } else if (action.equals("btnDeleteReq")) {
                //btn Delete at TestRequestUpdate.jsp
                destination = deleteReq;
            } else if (action.equals("btnViewResult")) {
                //btn View detail at TestRequestManagement.jsp
                destination = detailResult;
            }
            //Navigate to page
            request.getRequestDispatcher(destination).forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
