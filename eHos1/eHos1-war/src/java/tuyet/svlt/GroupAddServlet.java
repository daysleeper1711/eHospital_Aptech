/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tuyet.svlt;

import dan.dto.EmpInfo;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import project.ett.TbServiceGroup;
import tuyet.ssb.TuyetSessionBean;

/**
 *
 * @author Hoang Tuyet
 */
public class GroupAddServlet extends HttpServlet {

    @EJB
    private TuyetSessionBean tuyetSSB;

    private static final String fail = "/TuyetGUI/Fail.jsp";
    private static final String insertJSP = "/TuyetGUI/GroupCreate.jsp";
    private static final String search = "GroupSearchServlet";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            String url = fail;
            HttpSession session = request.getSession();
            EmpInfo emp = (EmpInfo) session.getAttribute("empInfo");

            String gname = request.getParameter("gName");
            String desc = request.getParameter("description");
            String type = request.getParameter("cbType");

            //check unique group name
            String checkName = "";
            List<TbServiceGroup> list = tuyetSSB.getAllGroup();
            if (list != null) {
                for (TbServiceGroup item : list) {
                    if (item.getGroupName().toLowerCase().equals(gname.toLowerCase())) {
                        url = insertJSP;
                        request.setAttribute("msg", "Group name '" + gname + "' is exist");
                        checkName = "error";
                        request.setAttribute("gname", gname);
                        request.setAttribute("desc", desc);
                        request.setAttribute("type", type);
                        request.getRequestDispatcher(url).forward(request, response);
                    }
                }
            }
            if (checkName.equals("")) {
                TbServiceGroup group = new TbServiceGroup();
                group.setGroupName(gname);
                group.setDescription(desc);
                group.setGroupType(type);
                group.setCreateDate(new Date());
                group.setStatus(Boolean.TRUE);

                if (tuyetSSB.insertGroup(emp.getEmployeeID(), group)) {
                    url = search;
                }
            }
            request.getRequestDispatcher(url).forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
