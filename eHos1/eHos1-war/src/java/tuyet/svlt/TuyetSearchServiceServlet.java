/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tuyet.svlt;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import project.ett.TbServiceGroup;
import project.ett.TbServicePrice;
import tuyet.dto.TuyetServiceDTO;
import tuyet.ssb.TuyetSessionBean;

/**
 *
 * @author Hoang Tuyet
 */
public class TuyetSearchServiceServlet extends HttpServlet {

    @EJB
    private TuyetSessionBean tuyetSSB;
    private static final String fail = "/TuyetGUI/Fail.jsp";
    private static final String addService2Test = "/TuyetGUI/AddServiceToTest.jsp";
    private static final String serviceManagement = "/TuyetGUI/ServiceManagement.jsp";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            String action = request.getParameter("action");
            String signal = request.getParameter("pSignal");
            String url = fail;

            String svName = "";
            String groupName = "";
            String status = "";

            //set list group type Exam
            String gname = "";
            String s = "1";
            String type = "Exam";
            List<TbServiceGroup> listGroup = tuyetSSB.searchGroup(gname, status, type);
            request.setAttribute("listGroup", listGroup);

            if (action.equals("btnShowAllService") || action.equals("btnChooseService")) {
                //request search method
                List<TuyetServiceDTO> listService = tuyetSSB.searchService(svName, groupName, status);
                if (listService == null) {
                    request.setAttribute("msg", "*Not found any service!");

                } else {
                    //take all service in group type: Exam
                    List<TuyetServiceDTO> list = new ArrayList<TuyetServiceDTO>();
                    for (TuyetServiceDTO dto : listService) {
                        if (dto.getService().getGroupID().getGroupType().equals("Exam") && dto.getService().getStatus()) {
                            list.add(dto);
                        }
                    }
                    if (list.isEmpty()) {
                        request.setAttribute("msg", "*Not found any service!");
                    } else {
                        //sort list
                        Collections.sort(list, new Comparator<TuyetServiceDTO>() {
                            @Override
                            public int compare(TuyetServiceDTO o1, TuyetServiceDTO o2) {
                                return o1.getServiceName().compareTo(o2.getServiceName());
                            }
                        });
                        request.setAttribute("listService", list);

                    }
                }
                request.setAttribute("group", groupName);
                request.setAttribute("name", svName);
                request.setAttribute("ps", signal);
                url = addService2Test;
            } else if (action.equals("btnFindService")) {
                svName = request.getParameter("txtSearchName");
                groupName = request.getParameter("selectGroup");
                //--Error: No information of search
                if (svName.equals("") && groupName.equals("")) {
                    request.setAttribute("msgClickSearch", "Please, enter search infomation!");
                    request.getRequestDispatcher(addService2Test).forward(request, response);
                } else {
                    //request search method
                    List<TuyetServiceDTO> listService = tuyetSSB.searchService(svName, groupName, status);
                    if (listService == null) {
                        request.setAttribute("msg", "*Not found any service!");
                    } else {
                        //take all service in group type: Exam
                        List<TuyetServiceDTO> list = new ArrayList<TuyetServiceDTO>();
                        for (TuyetServiceDTO dto : listService) {
                            if (dto.getService().getGroupID().getGroupType().equals("Exam") && dto.getService().getStatus()) {
                                list.add(dto);
                            }
                        }
                        if (list.isEmpty()) {
                            request.setAttribute("msg", "*Not found any service!");
                        } else {
                            //sort list
                            Collections.sort(listService, new Comparator<TuyetServiceDTO>() {
                                @Override
                                public int compare(TuyetServiceDTO o1, TuyetServiceDTO o2) {
                                    return o1.getService().getServiceName().compareTo(o2.getService().getServiceName());
                                }
                            });
                            request.setAttribute("listService", list);

                        }
                    }
                }
                request.setAttribute("group", groupName);
                request.setAttribute("name", svName);
                request.setAttribute("ps", signal);
                url = addService2Test;
            }

            //Move to page
            request.getRequestDispatcher(url).forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
