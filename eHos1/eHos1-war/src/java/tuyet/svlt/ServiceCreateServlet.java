/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tuyet.svlt;

import dan.dto.EmpInfo;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import project.ett.TbService;
import project.ett.TbServiceGroup;
import tuyet.ssb.TuyetSessionBean;

/**
 *
 * @author Hoang Tuyet
 */
public class ServiceCreateServlet extends HttpServlet {

    @EJB
    private TuyetSessionBean tuyetSSB;
    private static final String fail = "/TuyetGUI/Fail.jsp";
    private static final String addServiceJSP = "/TuyetGUI/ServiceCreate.jsp";
    private static final String searchService = "ServiceSearchServlet";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            HttpSession session = request.getSession();
            String action = request.getParameter("action");
            String url = fail;

            //set list group
            List<TbServiceGroup> listGroup = tuyetSSB.getAllGroup();
            request.setAttribute("listGroup", listGroup);
            if (action.equals("btnAddNew")) {
                url = addServiceJSP;
            } else if (action.equals("btnCreateService")) {
                String name = request.getParameter("txtName");
                String content = request.getParameter("txtContent");
                double price = Double.parseDouble(request.getParameter("txtPrice"));
                String measure = request.getParameter("txtMeasure");
                int groupID = Integer.parseInt(request.getParameter("cbGroup"));
                EmpInfo emp = (EmpInfo) session.getAttribute("empInfo");
                String empID = emp.getEmployeeID();

                //set value for new service
                TbService service = new TbService();
                service.setServiceName(name);
                service.setServiceContent(content);
                service.setUnitOfMea(measure);
                service.setStatus(Boolean.TRUE);
                service.setCreateDate(new Date());
                
                //check name is unique
                String dupErr = "";
                List<TbService> listservice = tuyetSSB.getAll();
                if (listservice != null) {
                    for (TbService sv : listservice) {
                        if (sv.getServiceName().toLowerCase().equals(name.toLowerCase())) {
                            request.setAttribute("msg", "Service name '" + name + "' is exist");
                            dupErr = "OK";
                            request.setAttribute("insertService", service);
                            request.setAttribute("groupid", groupID);
                            request.setAttribute("insPrice", price);
                            url = addServiceJSP;
                            request.getRequestDispatcher(url).forward(request, response);
                        }
                    }
                }
                if (dupErr.equals("")) {

                    //insert new service
                    if (tuyetSSB.insertService(groupID, empID, service)) {
                        //insert price
                        List<TbService> listSV = tuyetSSB.getAll();
                        TbService sv = listSV.get(listSV.size() - 1);
                        if (tuyetSSB.insertPrice(sv.getServiceID(), empID, price)) {
                            url = searchService + "?action=btnShow";
                        }
                    }

                }
            }
            //move page
            request.getRequestDispatcher(url).forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
