/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tuyet.svlt;

import dan.dto.EmpInfo;
import dan.ssb.DanSessionBean;
import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import tuyet.dto.TuyetInfoTestDTO;
import tuyet.ssb.TuyetSessionBean;

/**
 *
 * @author Hoang Tuyet
 */
public class TuyetDeleteReqServlet extends HttpServlet {

    @EJB
    private DanSessionBean danSSB;

    @EJB
    private TuyetSessionBean tuyetSSB;
    private static final String reqManagement = "TuyetSearchReqServlet?txtSearchAddID=";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            String id = request.getParameter("txtTestID");
            HttpSession session = request.getSession();
            TuyetInfoTestDTO infoTest = (TuyetInfoTestDTO) session.getAttribute("infoTest");
            EmpInfo empInfo = (EmpInfo) session.getAttribute("empInfo");
            String url = "/TuyetGUI/Fail.jsp";
            //check if updateEmp isn't creator
            if (!infoTest.getAppDoc().equals(empInfo.getEmpName())) {
                request.setAttribute("msgUser", "Sorry! You aren't allowed to delete this test!");
                url = "/TuyetGUI/TestRequestUpdate.jsp";
            } else {
                if (tuyetSSB.deleteTestRequest(id)) {
                    danSSB.refundDeposit(infoTest.getOldPayment(), infoTest.getAdmissionID());
                    url = reqManagement + infoTest.getAdmissionID() + "&optionsRadiosInline=" + infoTest.getPatType();
                    session.removeAttribute("infoTest");
                    if (session.getAttribute("cart") != null) {
                        session.removeAttribute("cart");
                    }
                }
            }
            request.getRequestDispatcher(url).forward(request, response);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
