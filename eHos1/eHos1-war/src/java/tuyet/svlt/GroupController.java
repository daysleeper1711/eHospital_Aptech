/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tuyet.svlt;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Hoang Tuyet
 */
public class GroupController extends HttpServlet {

    private static final String fail = "/TuyetGUI/Fail.jsp";
    private static final String search = "GroupSearchServlet";
    private static final String insert = "GroupAddServlet";
    private static final String update = "GroupUpdateServlet";
    private static final String insertJSP = "/TuyetGUI/GroupCreate.jsp";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            String action = request.getParameter("action");
            String url = fail;
            if (action.equals("btnSearch") || action.equals("btnShow")) {
                //btn search and show at GroupManagement page
                System.out.println("----------------");
                url = search;
            } else if (action.equals("btnAddNew")) {
                //btn add new at GroupManagement page
                url = insertJSP;
            } else if (action.equals("btnEdit")) {
                //btn Edit at GroupManagement page
                url = update;
            } else if (action.equals("Disable") || action.equals("Enable") || action.equals("btnUpdate")) {
                //btn Disable at GroupManagement page
                url = update;
            } else if (action.equals("btnInsert")) {
                //btn Disable at GroupManagement page
                url = insert;
            } else if (action.equals("btnCancel")) {
                //btn Disable at GroupManagement page
                url = search;
            }
            
            
            request.getRequestDispatcher(url).forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
