/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tuyet.svlt;

import dan.dto.EmpInfo;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import project.ett.TbServiceGroup;
import tuyet.ssb.TuyetSessionBean;

/**
 *
 * @author Hoang Tuyet
 */
public class GroupUpdateServlet extends HttpServlet {

    @EJB
    private TuyetSessionBean tuyetSSB;

    private static final String fail = "/TuyetGUI/Fail.jsp";
    private static final String search = "GroupSearchServlet";
    private static final String updateJSP = "/TuyetGUI/GroupUpdate.jsp";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            String url = fail;

            HttpSession session = request.getSession();
            EmpInfo emp = (EmpInfo) session.getAttribute("empInfo");

            String action = request.getParameter("action");
            int groupID = Integer.parseInt(request.getParameter("groupID"));
            TbServiceGroup group = tuyetSSB.findGroupbyID(groupID);

            if (action.equals("Enable") || action.equals("Disable")) {
                if (tuyetSSB.updateGroupStatus(groupID, action, emp.getEmployeeID())) {
                    url = search;
                }
            } else if (action.equals("btnEdit")) {
                request.setAttribute("editG", group);
                url = updateJSP;
            } else if (action.equals("btnUpdate")) {
                String gname = request.getParameter("txtGroupName");
                String desc = request.getParameter("txtDesc");
                String type = request.getParameter("cbType");
                String stt = request.getParameter("cbStatus");
                //check name is unique
                String checkName = "";
                List<TbServiceGroup> list = tuyetSSB.getAllGroup();
                if (list != null) {
                    for (TbServiceGroup item : list) {
                        if (item.getGroupName().toLowerCase().equals(gname.toLowerCase()) && item.getGroupID() != groupID) {
                            url = updateJSP;
                            request.setAttribute("msg", "Group name '" + gname + "' is exist");
                            checkName = "error";
                            
                            //set new value to show to interface (except name)
                            group.setDescription(desc);
                            group.setGroupType(type);
                            if (stt.equals("1")) {
                                group.setStatus(true);
                            } else {
                                group.setStatus(false);
                            }
                            request.setAttribute("editG", group);
                            request.getRequestDispatcher(url).forward(request, response);
                        }
                    }
                }

                if (checkName.equals("")) {
                    TbServiceGroup svg = new TbServiceGroup();
                    //set updated values
                    svg.setGroupID(groupID);
                    svg.setGroupName(gname);
                    svg.setDescription(desc);
                    svg.setGroupType(type);
                    svg.setUpdateDate(new Date());
                    if (tuyetSSB.updateGroup(emp.getEmployeeID(), svg)) {
                        //update status
                        String s = "0";
                        if (group.getStatus()) {
                            s = "1";
                        }
                        if (!s.equals(stt)) {
                            tuyetSSB.updateGroupStatus(groupID, stt, emp.getEmployeeID());
                            
                        }
                        request.setAttribute("msg", "Updated group successfully");
                        request.setAttribute("t", "1");
                        request.setAttribute("editG", tuyetSSB.findGroupbyID(groupID));
                        url = updateJSP;
                    }
                }
            }

            request.getRequestDispatcher(url).forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
