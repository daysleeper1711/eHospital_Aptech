/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tuyet.svlt;

import dan.dto.EmpInfo;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import project.ett.TbStandardValue;
import tuyet.dto.TuyetServiceDTO;
import tuyet.ssb.TuyetSessionBean;

/**
 *
 * @author Hoang Tuyet
 */
public class SVUpdateServlet extends HttpServlet {

    @EJB
    private TuyetSessionBean tuyetSSB;

    private static final String fail = "/TuyetGUI/Fail.jsp";
    private static final String updateSVJSP = "/TuyetGUI/StandardValueUpdate.jsp";
    private static final String updateSV = "SVUpdateServlet";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            String action = request.getParameter("action");
            String url = fail;
            if (action.equals("insertSV") || action.equals("detailSV")) {
                String msg = (String) request.getAttribute("msg");
                TbStandardValue sv = (TbStandardValue) request.getAttribute("editSV");
                String serviceName = sv.getServiceID().getServiceName();
                String male = sv.getSVmale();
                if (male.contains(":")) {
                    String[] svmale = male.split(":");
                    String mmin = svmale[0];
                    String mmax = svmale[1];
                    request.setAttribute("mmin", mmin);
                    request.setAttribute("mmax", mmax);
                }
                String fmale = sv.getSVfemale();
                if (fmale.contains(":")) {
                    String[] svf = fmale.split(":");
                    String fmin = svf[0];
                    String fmax = svf[1];
                    request.setAttribute("fmin", fmin);
                    request.setAttribute("fmax", fmax);
                }

                request.setAttribute("serviceName", serviceName);
                request.setAttribute("male", male);
                request.setAttribute("fmale", fmale);
                request.setAttribute("standard", sv);
                request.setAttribute("msg", msg);
                url = updateSVJSP;
            } else if (action.equals("updateSV")) {
                int serviceID = Integer.parseInt(request.getParameter("serviceID"));
                int svID = Integer.parseInt(request.getParameter("svID"));

                HttpSession session = request.getSession();
                EmpInfo emp = (EmpInfo) session.getAttribute("empInfo");

                String male = request.getParameter("txtSVMale");
                String mmin = request.getParameter("txtMaleMin");
                String mmax = request.getParameter("txtMaleMax");

                String fmale = request.getParameter("txtSVfmale");
                String fmin = request.getParameter("txtfmaleMin");
                String fmax = request.getParameter("txtfmaleMax");

                TbStandardValue sv = new TbStandardValue();
                if (!male.equals("")) {
                    sv.setSVmale(male);
                    sv.setSVfemale(fmale);
                } else {
                    String svm = mmin + ":" + mmax;
                    String svf = fmin + ":" + fmax;
                    sv.setSVmale(svm);
                    sv.setSVfemale(svf);
                }
                sv.setUpdateDate(new Date());
                sv.setSvId(svID);

                //update standard value
                if (tuyetSSB.updateStandardValue(emp.getEmployeeID(), sv)) {
                    TuyetServiceDTO dto = (TuyetServiceDTO) tuyetSSB.findServiceByID(serviceID);
                    List<TbStandardValue> listSV = dto.getListStandarValue();
                    //Take updated standard value
                    TbStandardValue tbSV = listSV.get(listSV.size() - 1);
                    request.setAttribute("editSV", tbSV);
                    request.setAttribute("msg", "Updated standard value successfully!");
                    url = updateSV + "?action=detailSV";
                }
            } else if (action.equals("viewSV")) {
                int serviceID = Integer.parseInt(request.getParameter("serviceID"));
                TuyetServiceDTO dto = (TuyetServiceDTO) tuyetSSB.findServiceByID(serviceID);
                List<TbStandardValue> listSV = dto.getListStandarValue();
                //Take standard value to show details
                TbStandardValue tbSV = listSV.get(listSV.size() - 1);
                request.setAttribute("editSV", tbSV);
                url = updateSV + "?action=detailSV";
            }
            request.getRequestDispatcher(url).forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
