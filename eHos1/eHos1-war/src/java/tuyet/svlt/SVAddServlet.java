/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tuyet.svlt;

import dan.dto.EmpInfo;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import project.ett.TbStandardValue;
import tuyet.dto.TuyetServiceDTO;
import tuyet.ssb.TuyetSessionBean;

/**
 *
 * @author Hoang Tuyet
 */
public class SVAddServlet extends HttpServlet {

    @EJB
    private TuyetSessionBean tuyetSSB;
    private static final String fail = "/TuyetGUI/Fail.jsp";
    private static final String addJSP = "/TuyetGUI/StandardValueCreate.jsp";
    private static final String updateSV = "SVUpdateServlet";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            HttpSession session = request.getSession();
            EmpInfo emp = (EmpInfo) session.getAttribute("empInfo");
            String action = request.getParameter("action");
            int serviceID = Integer.parseInt(request.getParameter("serviceID"));
            String serviceName = request.getParameter("serviceName");
            String url = fail;

            if (action.equals("addSV")) {
                request.setAttribute("serviceID", serviceID);
                request.setAttribute("serviceName", serviceName);
                url = addJSP;
            } else if (action.equals("insertSV")) {
                String male = request.getParameter("txtSVMale");
                String mmin = request.getParameter("txtMaleMin");
                String mmax = request.getParameter("txtMaleMax");

                String fmale = request.getParameter("txtSVfmale");
                String fmin = request.getParameter("txtfmaleMin");
                String fmax = request.getParameter("txtfmaleMax");

                TbStandardValue sv = new TbStandardValue();
                if (!male.equals("")) {
                    sv.setSVmale(male);
                    sv.setSVfemale(fmale);
                } else {
                    String svm = mmin + ":" + mmax;
                    String svf = fmin + ":" + fmax;
                    sv.setSVmale(svm);
                    sv.setSVfemale(svf);
                }
                sv.setCreateDate(new Date());

                //insert standard value
                if (tuyetSSB.insertSV(serviceID, emp.getEmployeeID(), sv)) {

                    TuyetServiceDTO dto = (TuyetServiceDTO) tuyetSSB.findServiceByID(serviceID);
                    List<TbStandardValue> listSV = dto.getListStandarValue();
                    //Take new standard value which inserted
                    TbStandardValue tbSV = listSV.get(listSV.size() - 1);
                    request.setAttribute("editSV", tbSV);
                    request.setAttribute("msg", "Added standard value successfully!");

                    url = updateSV;
                }

            }//end 

            request.getRequestDispatcher(url).forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
