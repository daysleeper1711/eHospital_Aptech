/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tuyet.svlt;

import dan.dto.EmpInfo;
import dan.ssb.DanSessionBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import project.ett.TbTestReqDetail;
import tuyet.dto.TuyetCartBean;
import tuyet.dto.TuyetCartServiceDTO;
import tuyet.dto.TuyetInfoTestDTO;
import tuyet.dto.TuyetRequestDTO;
import tuyet.ssb.TuyetSessionBean;

/**
 *
 * @author Hoang Tuyet
 */
public class TuyetUpdateReqServlet extends HttpServlet {
    @EJB
    private DanSessionBean danSSB;

    @EJB
    private TuyetSessionBean tuyetSSB;
    private static final String updateJSP = "/TuyetGUI/TestRequestUpdate.jsp";
    private static final String reqManagement = "TuyetSearchReqServlet?txtSearchAddID=";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            HttpSession session = request.getSession();
            String url = "/TuyetGUI/Fail.jsp";
            String action = request.getParameter("action");

            if (action.equals("btnEditRequest")) {
                String testID = request.getParameter("txtReqID");
                TuyetRequestDTO reqDTO = tuyetSSB.findByTestID(testID);
                //create var of session infoTest to store infomation of test request
                TuyetInfoTestDTO infoTest = new TuyetInfoTestDTO();
                //--set value for infoTest
                if (reqDTO.getRequest().getMrId().getMedRecordID() != null) {
                    infoTest.setAdmissionID(reqDTO.getRequest().getMrId().getMedRecordID());
                    infoTest.setPatType("inpatient");

                    //--set other values for infoTest
                    infoTest.setTestID(testID);
                    infoTest.setPatName(reqDTO.getRequest().getMrId().getPatID().getPatName());
                    infoTest.setPatAddr(reqDTO.getRequest().getMrId().getPatID().getAddress());
                    //--set age of patien
                    Date dob = reqDTO.getRequest().getMrId().getPatID().getDob();
                    int year1 = dob.getYear();
                    Date today = new Date();
                    int year2 = today.getYear();
                    int patAge = year2 - year1;
                    infoTest.setPatAge(patAge + "");
                    //--set gender
                    boolean gender = reqDTO.getRequest().getMrId().getPatID().getGender();
                    if (gender) {
                        infoTest.setPatGender("Female");
                    } else {
                        infoTest.setPatGender("Male");
                    }
                    //--set other values
                    infoTest.setFrFac(reqDTO.getRequest().getFromFac().getFacultyName());
                    infoTest.setToFac(reqDTO.getRequest().getToFac().getFacultyName());
                    infoTest.setDiagnosis(reqDTO.getRequest().getDiagnosis());
                    infoTest.setAppDoc(reqDTO.getRequest().getAppointedDoc().getFullname());
                    infoTest.setOldPayment(reqDTO.getRequest().getTotalPayment());//payment of old cart
                    //--create sesision var
                    session.setAttribute("infoTest", infoTest);

                    //set value to create session var: cart
                    TuyetCartBean cbean = new TuyetCartBean();
                    for (TbTestReqDetail detail : reqDTO.getReqDetails()) {
                        TuyetCartServiceDTO dto = new TuyetCartServiceDTO();
                        dto.setAdmissionID(detail.getTestReqID().getMrId().getMedRecordID());
                        dto.setServiceGroup(detail.getServiceID().getGroupID().getGroupName());
                        dto.setServiceID(detail.getServiceID().getServiceID());
                        dto.setServiceName(detail.getServiceID().getServiceName());
                        dto.setTestRequestID(detail.getTestReqID().getTestReqID());
                        dto.setTestReqDetailID(detail.getTestReqDetailID());
                        dto.setUnitPrice(detail.getRealPrice());
                        cbean.getServiceCart().add(dto);

                    }
                    session.setAttribute("cart", cbean);
                } else {
                    //for outpatient
                    infoTest.setAdmissionID(reqDTO.getRequest().getOutPatID().getOutPatID());
                }//end if
                url = updateJSP;
                //update status 'Updating' for test request
                tuyetSSB.updateTestStatus(testID, "Updating");
            } else if (action.equals("btnCancelEditReq")) {
                TuyetInfoTestDTO infoTest = (TuyetInfoTestDTO) session.getAttribute("infoTest");
                //update status 'Updating' for test request
                tuyetSSB.updateTestStatus(infoTest.getTestID(), "New");
                url = reqManagement + infoTest.getAdmissionID() + "&optionsRadiosInline=" + infoTest.getPatType();
                //remove session variables
                session.removeAttribute("infoTest");
                if (session.getAttribute("cart") != null) {
                    session.removeAttribute("cart");
                }

            } else if (action.equals("btnSaveReq")) {
                //Update test request
                TuyetCartBean cbean = (TuyetCartBean) session.getAttribute("cart");
                TuyetInfoTestDTO infoTest = (TuyetInfoTestDTO) session.getAttribute("infoTest");
                EmpInfo empInfo = (EmpInfo) session.getAttribute("empInfo");
                
                //reset diagnosis
                infoTest.setDiagnosis(request.getParameter("txtDiagnosis"));

                //check if updateEmp isn't creator
                if (!infoTest.getAppDoc().equals(empInfo.getEmpName())) {
                    request.setAttribute("msgUser", "Sorry! You aren't allowed to update this test!");
                    url = "/TuyetGUI/TestRequestUpdate.jsp";
                } else {

                    //--check empty cart service
                    if (cbean.getServiceCart().isEmpty()) {
                        request.setAttribute("msgCheckCart", "Please, choose services for test!");
                        url = "/TuyetGUI/TestRequestUpdate.jsp";
                    } else {
                        //--check deposit
                        //----take value of deposit
                        double deposit = tuyetSSB.getDeposit(infoTest.getAdmissionID());
                        //----check payment ability
                        if ((deposit + infoTest.getOldPayment()) < cbean.totalPayment()) {
                            request.setAttribute("msgCheckDeposit", "*Deposit is <b> $" + (deposit + infoTest.getOldPayment()) + "</b>. It's not enough for payment.");
                            url = "/TuyetGUI/TestRequestUpdate.jsp";
                        } else {
                            danSSB.refundDeposit(infoTest.getOldPayment(), infoTest.getAdmissionID());
                            danSSB.checkDepositPayment(cbean.totalPayment(), infoTest.getAdmissionID());
                            //reset value oldPayment of infoTest
                            infoTest.setOldPayment(cbean.totalPayment());
                            
                            String testID = infoTest.getTestID();
                            //update diagnosis
                            String diagnosis = infoTest.getDiagnosis();
                            if (tuyetSSB.updateTestRequest(testID, diagnosis, empInfo.getEmployeeID())) {
                                TuyetRequestDTO reqDTO = tuyetSSB.findByTestID(testID);
                                System.out.println("ReqDTO : " + reqDTO.getRequest().getTestReqID());
                                List<TuyetCartServiceDTO> newlist = cbean.getServiceCart();
                                List<TbTestReqDetail> oldlist = reqDTO.getReqDetails();

                                //find and remove service in old cart
                                for (TbTestReqDetail kq : oldlist) {
                                    int id = kq.getServiceID().getServiceID();
                                    if (!tuyetSSB.compareOldToNew(id, newlist)) {
                                        tuyetSSB.deleteTestDetail(kq.getTestReqDetailID());

                                    }
                                }
                                //find and insert new service
                                for (TuyetCartServiceDTO sv : newlist) {
                                    int id = sv.getServiceID();
                                    if (!tuyetSSB.compareNewtoOld(id, oldlist)) {
                                        String reqID = testID;
                                        System.out.println("reqID: " + reqID);
                                        int serviceID = sv.getServiceID();
                                        System.out.println("Service ID: " + serviceID);
                                        double price = sv.getUnitPrice();
                                        String empCreate = empInfo.getEmployeeID();
                                        tuyetSSB.insertReqDetail(reqID, serviceID, price, empCreate);
                                    }
                                }

                                url = updateJSP;
                                request.setAttribute("msgSuccess", "Changes saved!");
                            }//end if
                        }
                    }//end check deposit
                }//end check user
            }//end
            request.getRequestDispatcher(url).forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
