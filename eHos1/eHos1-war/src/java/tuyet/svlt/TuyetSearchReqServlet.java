/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tuyet.svlt;

import dan.ssb.DanSessionBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import tuyet.dto.TuyetMedicalRecordDTO;
import tuyet.dto.TuyetRequestDTO;
import tuyet.ssb.TuyetSessionBean;

/**
 *
 * @author Hoang Tuyet
 */
public class TuyetSearchReqServlet extends HttpServlet {

    @EJB
    private DanSessionBean danSSB;

    @EJB
    private TuyetSessionBean tuyetSSB;

    private static final String searchRequest = "/TuyetGUI/TestRequestManagement.jsp";
    private static final String fail = "/TuyetGUI/Fail.jsp";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            String txtSearch = request.getParameter("txtSearchAddID");
            String url = searchRequest;
            //get values
            String checkPat = request.getParameter("optionsRadiosInline");
            String admissionID = "";
            String patName = "";
            String patAddr = "";
            int patAge = 0;
            String patGender = "";
            String treatmentFac = "";
            List<TuyetRequestDTO> listReqDTO = new ArrayList<TuyetRequestDTO>();
            if (checkPat.equals("inpatient")) {
                TuyetMedicalRecordDTO mr_DTO = tuyetSSB.findbyMRID(txtSearch);
                if (mr_DTO == null) {
                    request.setAttribute("msg", "Don't found any result !");
                } else {
                    //Medical record done
                    if (!mr_DTO.getTbMR().getStatus().equals("in treatment")) {
                        request.setAttribute("msg", "Medical record is unavailable.");
                    }

                    request.setAttribute("mrStatus", mr_DTO.getTbMR().getStatus());
                    System.out.println("mrStatus: " + mr_DTO.getTbMR().getStatus());

                    //set value for admissionID
                    admissionID = mr_DTO.getTbMR().getMedRecordID();
                    //set value for patName
                    patName = mr_DTO.getTbPat().getPatName();
                    //set value for patAddr
                    patAddr = mr_DTO.getTbPat().getAddress();
                    //set value for patAge
                    Date dob = mr_DTO.getTbPat().getDob();
                    int year1 = dob.getYear();
                    Date today = new Date();
                    int year2 = today.getYear();
                    patAge = year2 - year1;
                    //set value to show gender
                    boolean gender = mr_DTO.getTbPat().getGender();
                    if (gender) {
                        patGender = "Female";
                    } else {
                        patGender = "Male";
                    }

                    //this is method of An
                    treatmentFac = danSSB.currentFaculty(txtSearch);

                    //set value of list request
                    listReqDTO = mr_DTO.getListRequestDTO();
                    //sort listReqDTO
                    Collections.sort(listReqDTO, new Comparator<TuyetRequestDTO>() {
                        @Override
                        public int compare(TuyetRequestDTO o1, TuyetRequestDTO o2) {
                            return o2.getCreateDate().compareTo(o1.getCreateDate());
                        }
                    });

                }

            } else if (checkPat.equals("outpatient")) {
                //System.out.println("wait...");
                request.setAttribute("msg", "Don't found any result !");
            }
            //set Attributes

            request.setAttribute("checkPat", checkPat);
            request.setAttribute("admissionID", admissionID);
            request.setAttribute("patType", checkPat);
            request.setAttribute("patName", patName);
            request.setAttribute("patAddr", patAddr);
            request.setAttribute("patAge", patAge);
            request.setAttribute("sex", patGender);
            request.setAttribute("treatmentFac", treatmentFac);
            request.setAttribute("listRequestDTO", listReqDTO);

            request.getRequestDispatcher(url).forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
