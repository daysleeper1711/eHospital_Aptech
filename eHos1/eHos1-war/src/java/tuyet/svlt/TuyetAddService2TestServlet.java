/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tuyet.svlt;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import project.ett.TbServicePrice;
import tuyet.dto.TuyetCartBean;
import tuyet.dto.TuyetCartServiceDTO;
import tuyet.dto.TuyetServiceDTO;
import tuyet.ssb.TuyetSessionBean;

/**
 *
 * @author Hoang Tuyet
 */
public class TuyetAddService2TestServlet extends HttpServlet {

    @EJB
    private TuyetSessionBean tuyetSSB;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            String pSignal = request.getParameter("pSignal");

            String[] listServiceID = request.getParameterValues("selectedService");
            if (listServiceID == null) {
                request.setAttribute("msg", "Please, select service(s) to add!");
                request.getRequestDispatcher("TuyetSearchServiceServlet?action=btnShowAllService").forward(request, response);
            } else {
                TuyetCartBean cBean = null;
                HttpSession session = request.getSession();
                Object obj = session.getAttribute("cart");
                if (obj == null) {
                    cBean = new TuyetCartBean();
                } else {
                    cBean = (TuyetCartBean) obj;
                }
                for (String serviceID : listServiceID) {

                    int id = Integer.parseInt(serviceID);
                    TuyetServiceDTO dto = tuyetSSB.findServiceByID(id);
                    TuyetCartServiceDTO cartDTO = new TuyetCartServiceDTO();
                    String ad_ID = (String) session.getAttribute("admissionID");
                    cartDTO.setServiceID(id);
                    cartDTO.setAdmissionID(ad_ID);
                    cartDTO.setServiceName(dto.getService().getServiceName());
                    cartDTO.setServiceGroup(dto.getService().getGroupID().getGroupName());
                    List<TbServicePrice> listPrice = dto.getListPrice();
                    for (TbServicePrice price : listPrice) {
                        if (price.getStatus()) {
                            cartDTO.setUnitPrice(price.getPrice());
                        }
                    }
                    cBean.addToCart(cartDTO);
                }
                session.setAttribute("cart", cBean);

                if (pSignal.equals("0")) {
                    request.getRequestDispatcher("/TuyetGUI/CreateTestRequest.jsp").forward(request, response);
                } else if (pSignal.equals("1")) {
                    request.getRequestDispatcher("/TuyetGUI/TestRequestUpdate.jsp").forward(request, response);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
