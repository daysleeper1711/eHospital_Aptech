/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tuyet.svlt;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import project.ett.TbServiceGroup;
import project.ett.TbStandardValue;
import tuyet.dto.TuyetServiceDTO;
import tuyet.ssb.TuyetSessionBean;

/**
 *
 * @author Hoang Tuyet
 */
public class ServiceSearchServlet extends HttpServlet {

    @EJB
    private TuyetSessionBean tuyetSSB;
    private static final String fail = "/TuyetGUI/Fail.jsp";
    private static final String serviceManage = "/TuyetGUI/ServiceManagement.jsp";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            String action = request.getParameter("action");
            String searchName = request.getParameter("txtName");
            String searchGroup = request.getParameter("selGroup");
            String searchStatus = request.getParameter("selStatus");

            String url = "";
            //set list group
            List<TbServiceGroup> listGroup = tuyetSSB.getAllGroup();
            request.setAttribute("listGroup", listGroup);
            //--Error: no search information
            if (action.equals("btnSearch") && searchName.equals("") && searchGroup.equals("") && searchStatus.equals("")) {
                request.setAttribute("msgClickSearch", "Please, enter seach infomation!");
                request.getRequestDispatcher(serviceManage).forward(request, response);
            }

            if (action.equals("btnShow")) {
                searchName = "";
                searchGroup = "";
                searchStatus = "";
            }

            List<TuyetServiceDTO> listService = tuyetSSB.searchService(searchName, searchGroup, searchStatus);
            if (listService != null) {
                //sort listService
                Collections.sort(listService, new Comparator<TuyetServiceDTO>() {
                    @Override
                    public int compare(TuyetServiceDTO o1, TuyetServiceDTO o2) {
                        return o2.getCreateDate().compareTo(o1.getCreateDate());
                    }
                });
                //set var
                request.setAttribute("listService", listService);
                request.setAttribute("name", searchName);
                request.setAttribute("group", searchGroup);
                request.setAttribute("status", searchStatus);
            } else {
                request.setAttribute("msg", "Don't found any service !");
            }

            url = serviceManage;
            request.getRequestDispatcher(url).forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
