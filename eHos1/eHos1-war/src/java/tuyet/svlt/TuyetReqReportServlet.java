/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tuyet.svlt;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import project.ett.TbMedicalRecord;
import project.ett.TbOutPatient;
import project.ett.TbPatient;
import project.ett.TbTestReqDetail;
import tuyet.dto.TuyetRequestDTO;
import tuyet.ssb.TuyetSessionBean;

/**
 *
 * @author Hoang Tuyet
 */
public class TuyetReqReportServlet extends HttpServlet {
    @EJB
    private TuyetSessionBean tuyetSSB;
    
    private static final String fail = "/TuyetGUI/Fail.jsp";
    //private static final String updateJSP = "/TuyetGUI/ResultUpdate.jsp";
    private static final String reportJSP = "/TuyetGUI/TuyetRequestReport.jsp";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            String url = fail;
            String action = request.getParameter("action");
            String reqID = request.getParameter("reqID");
            TuyetRequestDTO  test = tuyetSSB.findByTestID(reqID);
            
            if(action.equals("reportReq")) {
               String patType = "inpatient";
                String adID = "";
                TbPatient pat = new TbPatient();
                TbMedicalRecord mr = test.getRequest().getMrId();
                TbOutPatient opt = test.getRequest().getOutPatID();
                if (mr != null) {
                    adID = mr.getMedRecordID();
                    pat = mr.getPatID();
                } else {
                    adID = opt.getOutPatID();
                    pat = mr.getPatID();
                    patType = "outpatient";
                }
                //cal age
                int yearb = pat.getDob().getYear();
                Date now = new Date();
                int curYear = now.getYear();
                int age = curYear - yearb; 
                
                List<TbTestReqDetail> listReqDetail = test.getReqDetails();
                
                request.setAttribute("listReqDetail", listReqDetail);
                request.setAttribute("patType", patType);
                request.setAttribute("age", age);
                request.setAttribute("testID", reqID);
                request.setAttribute("adID", adID);
                request.setAttribute("pat", pat);
                request.setAttribute("test", test);
                
                url = reportJSP;
            } else if (action.equals("")) {
                
            }
            
            request.getRequestDispatcher(url).forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
