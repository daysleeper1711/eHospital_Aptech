/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dung.vns.servlets;

import dung.vns.ssbeans.dungSuggestSurgerySessionBean;
import dung.vns.ssbeans.dungSurgeryInfoSessionBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import project.ett.TbSuggestSurgery;
import project.ett.TbSurgeryRecord;

/**
 *
 * @author Sony
 */
public class dungEditSurgeryRecordServlet extends HttpServlet {
    @EJB
    private dungSurgeryInfoSessionBean dungSurgeryInfoSessionBean;
    @EJB
    private dungSuggestSurgerySessionBean dungSuggestSurgerySessionBean;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            String destinationUrl = "login.jsp";
            HttpSession session = request.getSession();
            Object obj = session.getAttribute("empInfo");
            if (obj != null) {
                String txtSuggestID = request.getParameter("txtSuggestID");
                TbSuggestSurgery dtoSuggest = dungSuggestSurgerySessionBean.findByPK(txtSuggestID);
                request.setAttribute("dtoSuggest", dtoSuggest);

                String txtSurgeryRecordID = request.getParameter("txtSurgeryRecordID");
                TbSurgeryRecord editSR = dungSurgeryInfoSessionBean.findByPK(txtSurgeryRecordID);
                request.setAttribute("editSR", editSR);

                String editTOS = new SimpleDateFormat("MM/dd/yyyy HH:mm").format(editSR.getTimeOfStart());
                request.setAttribute("editTOS", editTOS);
                
                String editTOE = new SimpleDateFormat("MM/dd/yyyy HH:mm").format(editSR.getTimeOfEnd());
                request.setAttribute("editTOE", editTOE);

                request.setAttribute("titlePage", "Update Surgery Record");
                destinationUrl = "dungEditSurgeryRecord.jsp";
            }

            request.getRequestDispatcher(destinationUrl).forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
