/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dung.vns.servlets;

import dan.dto.EmpInfo;
import dan.dto.MedicalRecordInfo;
import dung.vns.ssbeans.dungSuggestSurgerySessionBean;
import dung.vns.ssbeans.dungSurgeryServiceSessionBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import project.ett.TbSuggestSurgery;
import project.ett.TbSurgeryService;

/**
 *
 * @author Sony
 */
public class InsertSuggestSurgeryServlet extends HttpServlet {
    @EJB
    private dungSurgeryServiceSessionBean dungSurgeryServiceSessionBean;

    @EJB
    private dungSuggestSurgerySessionBean dungSuggestSurgerySessionBean;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            String destinationUrl = "dungFail.jsp";
            HttpSession session = request.getSession();
            Object obj = session.getAttribute("empInfo");
            MedicalRecordInfo mrInfo = (MedicalRecordInfo) session.getAttribute("sInfo");
            if (obj != null) {
                EmpInfo emI = (EmpInfo) obj;
                String action = request.getParameter("action");
                if (action.equals("Insert")) {

                    // ID generate auto
                    Date currentD = new Date();
                    Timestamp tsDay = new Timestamp(currentD.getTime());
                    String txtSuggestID = new SimpleDateFormat("yyyyMMddHHmmss").format(currentD);
                    String txtMedicalID = mrInfo.getMedRecordID();
                    String cbServiceID = request.getParameter("cbServiceName");
                    String txtSuggestDescription = request.getParameter("txtSuggestDescription");
                    String cbDoctorID = request.getParameter("cbDoctor");

                    TbSuggestSurgery tbSS = new TbSuggestSurgery();
                    tbSS.setSuggestSurgeryID(txtSuggestID);
                    tbSS.setSuggestDescription(txtSuggestDescription);
                    tbSS.setStatusSuggest(Boolean.TRUE);
                    tbSS.setCreateDate(tsDay);
                    
                    TbSurgeryService tbSService = dungSurgeryServiceSessionBean.findByPK(cbServiceID);

                    if (!dungSuggestSurgerySessionBean.checkExistWhenInsert(txtMedicalID, cbServiceID)) {
                        if (dungSuggestSurgerySessionBean.insertSuggestSurgery(tbSS, txtMedicalID, cbServiceID, cbDoctorID, emI.getEmployeeID())) {
                            destinationUrl = "dungSuggestSurgeryManagerServlet";
                        }
                    } else {
                        request.setAttribute("msgError", tbSService.getSurgeryServiceName() + " has exist.");
                        destinationUrl = "dungAddSuggestSurgeryServlet";
                    }
                }
                if (action.equals("Cancel")) {
                    destinationUrl = "dungSuggestSurgeryManagerServlet";
                }
            } else {
                destinationUrl = "login.jsp";
            }

            request.getRequestDispatcher(destinationUrl).forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
