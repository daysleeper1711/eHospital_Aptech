/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dung.vns.servlets;

import dan.dto.EmpInfo;
import dung.vns.ssbeans.dungSurgerySessionBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.Date;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import project.ett.TbLevelSurgery;

/**
 *
 * @author Sony
 */
public class dungUpdateLevelSurgeryServlet extends HttpServlet {

    @EJB
    private dungSurgerySessionBean dungSurgerySessionBean;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            String destinationUrl = "dungFail.jsp";
            HttpSession session = request.getSession();
            Object obj = session.getAttribute("empInfo");
            if (obj != null) {
                EmpInfo emI = (EmpInfo) obj;
                String action = request.getParameter("action");
                if (action.equals("Update")) {
                    String lvID = request.getParameter("txtLevelID");
                    String lvName = request.getParameter("txtLevelName");
                    if (lvName != null && !lvName.trim().isEmpty()) {
                        String lvDescription = request.getParameter("txtDescriptionName");
                        Date currentD = new Date();
                        Timestamp tsDay = new Timestamp(currentD.getTime());
                        TbLevelSurgery tbLevel = new TbLevelSurgery();
                        tbLevel.setLevelID(Integer.parseInt(lvID));
                        tbLevel.setLevelName(lvName.trim());
                        tbLevel.setLevelDecription(lvDescription.trim());
                        boolean fsta = (request.getParameter("cbStatus") != null);
                        tbLevel.setStatusLevel(fsta);
                        tbLevel.setCreateDate(tsDay);
                        if (!dungSurgerySessionBean.checkExistWhenUpdate(Integer.parseInt(lvID), lvName.trim())) {
                            if (dungSurgerySessionBean.updateLevelSurgery(tbLevel, emI.getEmployeeID())) {
                                destinationUrl = "dungLevelSurgeryManagerServlet";
                            }
                        } else {
                            TbLevelSurgery editLV = dungSurgerySessionBean.findByPK(Integer.parseInt(lvID));
                            request.setAttribute("editLV", editLV);
                            request.setAttribute("msgError", lvName + " has exist");
                            destinationUrl = "dungEditLevelSurgery.jsp";
                        }
                    } else {
                        TbLevelSurgery editLV = dungSurgerySessionBean.findByPK(Integer.parseInt(lvID));
                        request.setAttribute("editLV", editLV);
                        request.setAttribute("msgError", lvName + " is required");
                        destinationUrl = "dungEditLevelSurgery.jsp";
                    }

                }
                if (action.equals("Cancel")) {
                    destinationUrl = "dungLevelSurgeryManagerServlet";
                }
            } else {
                destinationUrl = "login.jsp";
            }
            request.getRequestDispatcher(destinationUrl).forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
