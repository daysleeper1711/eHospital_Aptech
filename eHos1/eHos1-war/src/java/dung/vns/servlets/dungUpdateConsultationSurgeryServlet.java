/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dung.vns.servlets;

import dan.dto.EmpInfo;
import dung.vns.ssbeans.dungConsultationSurgerySessionBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.Date;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import project.ett.TbConsulationSurgery;

/**
 *
 * @author Sony
 */
public class dungUpdateConsultationSurgeryServlet extends HttpServlet {

    @EJB
    private dungConsultationSurgerySessionBean dungConsultationSurgerySessionBean;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            String destinationUrl = "dungFail.jsp";
            HttpSession session = request.getSession();
            Object obj = session.getAttribute("empInfo");
            if (obj != null) {
                EmpInfo emI = (EmpInfo) obj;
                String action = request.getParameter("action");
                if (action.equals("Update")) {
                    String txtSuggestID = request.getParameter("txtSuggestID");
                    String txtConsultationID = request.getParameter("txtConsultationID");
                    String vLocalDate = request.getParameter("txtConsultationDate");
                    if (vLocalDate != null && !vLocalDate.isEmpty()) {
                        String[] vDateTime = vLocalDate.trim().split(" ");
                        String[] vDate = vDateTime[0].trim().split("/");
                        String[] vTime = vDateTime[1].trim().split(":");
                        Timestamp vConsultationDate = new Timestamp(Integer.parseInt(vDate[2]) - 1900, Integer.parseInt(vDate[0]) - 1, Integer.parseInt(vDate[1]), Integer.parseInt(vTime[0]), Integer.parseInt(vTime[1]), 0, 0);
                        String txtConsultation = request.getParameter("txtConsultation");
                        if (txtConsultation != null && !txtConsultation.isEmpty()) {
                            boolean fsta = (request.getParameter("cbStatus") != null);
                            Date currentD = new Date();
                            Timestamp tsDay = new Timestamp(currentD.getTime());

                            TbConsulationSurgery tbCS = new TbConsulationSurgery();
                            tbCS.setConsulationID(txtConsultationID);
                            tbCS.setConsulationDate(vConsultationDate);
                            tbCS.setConsulation(txtConsultation);
                            tbCS.setStatusConsulation(fsta);
                            tbCS.setCreateDate(tsDay);

                            if (dungConsultationSurgerySessionBean.updateConsultationSurgery(tbCS, txtSuggestID, emI.getEmployeeID())) {
                                destinationUrl = "dungConsultationSurgeryManagerServlet";
                            }
                        } else {
                            request.setAttribute("msgError", "Consultation is required");
                            destinationUrl = "dungEditConsultationSurgeryServlet";
                        }
                    } else {
                        request.setAttribute("msgError", "Consultation Date is required");
                        destinationUrl = "dungEditConsultationSurgeryServlet";
                    }
                }
                if (action.equals("Cancel")) {
                    destinationUrl = "dungConsultationSurgeryManagerServlet";
                }
            } else {
                destinationUrl = "login.jsp";
            }
            request.getRequestDispatcher(destinationUrl).forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
