/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dung.vns.servlets;

import dan.dto.EmpInfo;
import dung.vns.ssbeans.dungPositionDoctorSessionBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.Date;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import project.ett.TbPositionDoctor;

/**
 *
 * @author Sony
 */
public class dungUpdatePositionDoctorServlet extends HttpServlet {

    @EJB
    private dungPositionDoctorSessionBean dungPositionDoctorSessionBean;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            String destinationUrl = "dungFail.jsp";
            HttpSession session = request.getSession();
            Object obj = session.getAttribute("empInfo");
            if (obj != null) {
                EmpInfo emI = (EmpInfo) obj;
                String action = request.getParameter("action");
                if (action.equals("Update")) {
                    String posID = request.getParameter("txtPositionID");
                    String posName = request.getParameter("txtPositionName");
                    if (posName != null && !posName.trim().isEmpty()) {
                        Date currentD = new Date();
                        Timestamp tsDay = new Timestamp(currentD.getTime());

                        TbPositionDoctor ePos = new TbPositionDoctor();

                        ePos.setPositionID(Integer.parseInt(posID));
                        ePos.setPositionName(posName.trim());
                        boolean fsta = (request.getParameter("cbStatus") != null);
                        ePos.setStatusPosition(fsta);
                        ePos.setCreateDate(tsDay);

                        if (!dungPositionDoctorSessionBean.checkExistWhenUpdate(Integer.parseInt(posID), posName.trim())) {
                            if (dungPositionDoctorSessionBean.updatePositionDoctor(ePos, emI.getEmployeeID())) {
                                destinationUrl = "dungPositionDoctorManagerServlet";
                            }
                        } else {
                            request.setAttribute("msgError", posName + "is exist");
                            destinationUrl = "dungEditPositionDoctorServlet";
                        }
                    } else {
                        request.setAttribute("msgError", "Position Name is required");
                        destinationUrl = "dungEditPositionDoctorServlet";
                    }
                }
                if (action.equals("Cancel")) {
                    destinationUrl = "dungPositionDoctorManagerServlet";
                }
            } else {
                destinationUrl = "login.jsp";
            }
            request.getRequestDispatcher(destinationUrl).forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
