/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dung.vns.servlets;

import dan.dto.EmpInfo;
import dung.dto.dungSurgeryRecordBean;
import dung.vns.ssbeans.dungDrugsDetailSessionBean;
import dung.vns.ssbeans.dungOperatorDoctorDetailSessionBean;
import dung.vns.ssbeans.dungSuggestSurgerySessionBean;
import dung.vns.ssbeans.dungSuppliesDetailSessionBean;
import dung.vns.ssbeans.dungSurgeryInfoSessionBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import project.ett.TbDrugsDetail;
import project.ett.TbOperatorDoctorDetail;
import project.ett.TbSuggestSurgery;
import project.ett.TbSuppliesDetail;
import project.ett.TbSurgeryRecord;
import project.ett.TbSurgeryService;
import project.locssbean.LocSessionBean;

/**
 *
 * @author Sony
 */
public class InsertSurgeryRecordCartServlet extends HttpServlet {

    @EJB
    private LocSessionBean locSessionBean;

    @EJB
    private dungOperatorDoctorDetailSessionBean dungOperatorDoctorDetailSessionBean;
    @EJB
    private dungSuppliesDetailSessionBean dungSuppliesDetailSessionBean;
    @EJB
    private dungDrugsDetailSessionBean dungDrugsDetailSessionBean;

    @EJB
    private dungSuggestSurgerySessionBean dungSuggestSurgerySessionBean;
    @EJB
    private dungSurgeryInfoSessionBean dungSurgeryInfoSessionBean;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            /* Get Session */
            HttpSession session = request.getSession();
            Object obj = session.getAttribute("cartSurgery");

            dungSurgeryRecordBean dSR = null;
            if (obj == null) {
                dSR = new dungSurgeryRecordBean();
            } else {
                dSR = (dungSurgeryRecordBean) obj;
            }
            /* Get DTO Drugs Detail */
            String destinationUrl = "dungFail.jsp";
            Object objOB = session.getAttribute("empInfo");
            if (objOB != null) {
                EmpInfo emI = (EmpInfo) objOB;
                String action = request.getParameter("action");
                if (action.equals("Insert")) {
                    // ID generate auto
                    String txtSuggestID = request.getParameter("txtSuggestID");
                    TbSuggestSurgery tbSS = dungSuggestSurgerySessionBean.findByPK(txtSuggestID);

                    TbSurgeryService tbSService = tbSS.getSurgeryServiceID();

                    Date currentD = new Date();
                    Timestamp tsDay = new Timestamp(currentD.getTime());
                    String txtSurgeryRecordID = new SimpleDateFormat("yyyyMMddHHmmss").format(currentD);
                    String txtProcessOfSurgery = request.getParameter("txtProcessOfSurgery");
                    if (txtProcessOfSurgery != null && !txtProcessOfSurgery.trim().isEmpty()) {
                        /* Get Time Of Start */
                        String vLocalDateStart = request.getParameter("txtTimeOfStart");
                        if (vLocalDateStart != null && !vLocalDateStart.isEmpty()) {
                            String[] vDateTimeStart = vLocalDateStart.trim().split(" ");
                            String[] vDateStart = vDateTimeStart[0].trim().split("/");
                            String[] vTimeStart = vDateTimeStart[1].trim().split(":");
                            Timestamp vTimeOfStart = new Timestamp(Integer.parseInt(vDateStart[2]) - 1900, Integer.parseInt(vDateStart[0]) - 1, Integer.parseInt(vDateStart[1]), Integer.parseInt(vTimeStart[0]), Integer.parseInt(vTimeStart[1]), 0, 0);

                            /* Get Time Of End */
                            String vLocalDateEnd = request.getParameter("txtTimeOfEnd");
                            if (vLocalDateEnd != null && !vLocalDateEnd.isEmpty()) {
                                String[] vDateTimeEnd = vLocalDateEnd.trim().split(" ");
                                String[] vDateEnd = vDateTimeEnd[0].trim().split("/");
                                String[] vTimeEnd = vDateTimeEnd[1].trim().split(":");
                                Timestamp vTimeOfEnd = new Timestamp(Integer.parseInt(vDateEnd[2]) - 1900, Integer.parseInt(vDateEnd[0]) - 1, Integer.parseInt(vDateEnd[1]), Integer.parseInt(vTimeEnd[0]), Integer.parseInt(vTimeEnd[1]), 0, 0);

                                /* validate datetime */
                                if (vTimeOfEnd.after(vTimeOfStart)) {
                                    if (dSR.getListtbDrugs().size() > 0) {
                                        if (dSR.getListtbSupplies().size() > 0) {
                                            if (dSR.getListtbOperator().size() > 0) {
                                                String txtResultSurgery = request.getParameter("txtResultSurgery");
                                                if (txtResultSurgery != null && !txtResultSurgery.trim().isEmpty()) {
                                                    boolean fsta = (request.getParameter("cbStatus") != null);

                                                    TbSurgeryRecord addSRecord = new TbSurgeryRecord();
                                                    addSRecord.setSurgeryRecordID(txtSurgeryRecordID);
                                                    addSRecord.setProcessOfSurgery(txtProcessOfSurgery.trim());
                                                    addSRecord.setTimeOfStart(vTimeOfStart);
                                                    addSRecord.setTimeOfEnd(vTimeOfEnd);
                                                    addSRecord.setSurgeryResult(txtResultSurgery.trim());
                                                    addSRecord.setBeginCost(tbSService.getInitialCost());
                                                    addSRecord.setPayCost(tbSService.getInitialCost());
                                                    addSRecord.setStatusSurgery(fsta);
                                                    addSRecord.setCreateDate(tsDay);

                                                    if (dungSurgeryInfoSessionBean.insertSurgeryRecord(addSRecord, txtSuggestID, emI.getEmployeeID())) {
                                                        /* Add Drugs */
                                                        for (TbDrugsDetail tbDD : dSR.getListtbDrugs()) {
                                                            if (locSessionBean.xuLyExportWarehouse(txtSurgeryRecordID, "S", tbDD.getDrugID().getItemCode(), tbDD.getQuantity(), "S", emI.getEmployeeID())) {
                                                                if (!dungDrugsDetailSessionBean.insertDrugsDetail(tbDD, txtSurgeryRecordID, tbDD.getDrugID().getItemCode(), emI.getEmployeeID())) {
                                                                    System.out.println("ID Drugs Dung : " + tbDD.getDrugsDetailID());
                                                                }
                                                            } else {
                                                                System.out.println("ID Drugs Loc : " + tbDD.getDrugsDetailID());
                                                            }
                                                        }

                                                        /* Add Supplies */
                                                        for (TbSuppliesDetail tbSD : dSR.getListtbSupplies()) {
                                                            if (locSessionBean.xuLyExportWarehouse(txtSurgeryRecordID, "S", tbSD.getSuppliesID().getItemCode(), tbSD.getQuantity(), "S", emI.getEmployeeID())) {
                                                                if (!dungSuppliesDetailSessionBean.insertSuppliesDetail(tbSD, txtSurgeryRecordID, tbSD.getSuppliesID().getItemCode(), emI.getEmployeeID())) {
                                                                    System.out.println("ID Supplies Dung : " + tbSD.getSuppliesDetailID());
                                                                }
                                                            } else {
                                                                System.out.println("ID Supplies Loc : " + tbSD.getSuppliesDetailID());
                                                            }
                                                        }

                                                        /* Add Operator Doctor */
                                                        for (TbOperatorDoctorDetail tbODD : dSR.getListtbOperator()) {
                                                            if (!dungOperatorDoctorDetailSessionBean.insertOperatorDoctorDetail(tbODD, txtSurgeryRecordID, tbODD.getDoctorID().getEmployeeID(), tbODD.getPositionID().getPositionID(), emI.getEmployeeID())) {
                                                                System.out.println("ID Operator : " + tbODD.getOperatorDoctorDetailID());
                                                            }
                                                        }
                                                        dSR = null;
                                                        session.setAttribute("cartSurgery", dSR);
                                                        destinationUrl = "dungSurgeryInfoManagerServlet";
                                                    }
                                                } else {
                                                    request.setAttribute("msgError", "Result Surgery is not blank.");
                                                    destinationUrl = "dungSurgeryInfoManagerServlet";
                                                }

                                            } else {
                                                request.setAttribute("msgError", "Operator Doctor Detail has not chosen.");
                                                destinationUrl = "dungSurgeryInfoManagerServlet";
                                            }
                                        } else {
                                            request.setAttribute("msgError", "Supplies Detail has not chosen.");
                                            destinationUrl = "dungSurgeryInfoManagerServlet";
                                        }
                                    } else {
                                        request.setAttribute("msgError", "Drugs Detail has not chosen.");
                                        destinationUrl = "dungSurgeryInfoManagerServlet";
                                    }

                                } else {
                                    request.setAttribute("msgError", "Time of End has after Time of Start.");
                                    destinationUrl = "dungSurgeryInfoManagerServlet";
                                }
                            } else {
                                request.setAttribute("msgError", "Time of End is not blank.");
                                destinationUrl = "dungSurgeryInfoManagerServlet";
                            }

                        } else {
                            request.setAttribute("msgError", "Time of Start is not blank.");
                            destinationUrl = "dungSurgeryInfoManagerServlet";
                        }
                    } else {
                        request.setAttribute("msgError", "Process of Surgery is not blank.");
                        destinationUrl = "dungSurgeryInfoManagerServlet";
                    }

                } else if (action.equals("Cancel")) {
                    destinationUrl = "dungSurgeryInfoManagerServlet";
                }

            } else {
                destinationUrl = "login.jsp";
            }
            /* Set Session */

            request.getRequestDispatcher(destinationUrl).forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
