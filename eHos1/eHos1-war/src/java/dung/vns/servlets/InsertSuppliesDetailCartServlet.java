/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dung.vns.servlets;

import dan.dto.EmpInfo;
import dung.dto.dungSurgeryRecordBean;
import dung.vns.ssbeans.dungSuppliesDetailSessionBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import project.ett.TbEmployee;
import project.ett.TbItemCodeManagement;
import project.ett.TbSuppliesDetail;

/**
 *
 * @author Sony
 */
public class InsertSuppliesDetailCartServlet extends HttpServlet {

    @EJB
    private dungSuppliesDetailSessionBean dungSuppliesDetailSessionBean;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            /* Get Session */
            HttpSession session = request.getSession();
            Object obj = session.getAttribute("cartSurgery");

            dungSurgeryRecordBean dSR = null;
            if (obj == null) {
                dSR = new dungSurgeryRecordBean();
            } else {
                dSR = (dungSurgeryRecordBean) obj;
            }

            /* Get DTO Drugs Detail */
            String destinationUrl = "dungFail.jsp";
            Object objOB = session.getAttribute("empInfo");
            if (objOB != null) {
                EmpInfo emI = (EmpInfo) objOB;
                String action = request.getParameter("action");
                if (action.equals("Insert")) {

                    // ID generate auto
                    Date currentD = new Date();
                    Timestamp tsDay = new Timestamp(currentD.getTime());
                    String txtSuppliesDetailID = new SimpleDateFormat("yyyyMMddHHmmss").format(currentD);

                    String txtSuppliesNameID = request.getParameter("cbSuppliesName");
                    boolean vflat = true;
                    for (TbSuppliesDetail objectDD : dSR.getListtbSupplies()) {
                        if (objectDD.getSuppliesID().getItemCode().equals(txtSuppliesNameID)) {
                            vflat = false;
                        }
                    }
                    if (vflat) {
                        TbItemCodeManagement tbICM = dungSuppliesDetailSessionBean.findICM(txtSuppliesNameID);

                        String txtQuantity = request.getParameter("txtQuantity");
                        try {
                            int txtQuan = Integer.parseInt(txtQuantity);
                        } catch (Exception e) {
                            request.setAttribute("msgError", "Quanity is Number");
                            request.getRequestDispatcher("dungSuppliesDetailCartServlet").forward(request, response);
                        }

                        TbEmployee tbEpm = dungSuppliesDetailSessionBean.findEmp(emI.getEmployeeID());

                        TbSuppliesDetail addSD = new TbSuppliesDetail();
                        addSD.setSuppliesDetailID(txtSuppliesDetailID);
                        addSD.setSuppliesID(tbICM);
                        addSD.setUnit(tbICM.getUnit());
                        addSD.setQuantity(Integer.parseInt(txtQuantity));
                        addSD.setStatusSupplies(Boolean.TRUE);
                        addSD.setUserID(tbEpm);
                        addSD.setCreateDate(tsDay);

                        dSR.getListtbSupplies().add(addSD);
                        /* Set Session */
                        session.setAttribute("cartSurgery", dSR);
                        destinationUrl = "dungSuppliesDetailCartServlet";
                    } else {
                        request.setAttribute("msgError", "Supplies Name has Exist.");
                        destinationUrl = "dungSuppliesDetailCartServlet";
                    }

                } else if (action.equals("Cancel")) {
                    destinationUrl = "dungSurgeryInfoManagerServlet";
                }
            } else {
                destinationUrl = "login.jsp";
            }
            request.getRequestDispatcher(destinationUrl).forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
