/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dung.vns.servlets;

import dung.vns.ssbeans.dungCategorySessionBean;
import dung.vns.ssbeans.dungSurgeryServiceSessionBean;
import dung.vns.ssbeans.dungSurgerySessionBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import project.ett.TbCategory;
import project.ett.TbFaculty;
import project.ett.TbLevelSurgery;

/**
 *
 * @author Sony
 */
public class dungAddSurgeryServiceServlet extends HttpServlet {
    @EJB
    private dungSurgeryServiceSessionBean dungSurgeryServiceSessionBean;
    @EJB
    private dungCategorySessionBean dungCategorySessionBean;
    @EJB
    private dungSurgerySessionBean dungSurgerySessionBean;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            List<TbFaculty> tbFac = dungSurgeryServiceSessionBean.getAllFaculty();
            List<TbLevelSurgery> tbLV = dungSurgerySessionBean.getAllLevelSurgery();
            List<TbCategory> tbCate = dungCategorySessionBean.getAllCategorySurgery();           
            
            
            request.setAttribute("listtbFac", tbFac);
            request.setAttribute("listtbLevel", tbLV);
            request.setAttribute("listtbCate", tbCate);
            
            request.setAttribute("titlePage", "Insert Surgery Service");
            request.getRequestDispatcher("dungInsertSurgeryService.jsp").forward(request, response);
            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
