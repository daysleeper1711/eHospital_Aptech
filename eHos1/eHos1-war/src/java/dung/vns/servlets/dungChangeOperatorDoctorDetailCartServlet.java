/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dung.vns.servlets;

import dan.dto.EmpInfo;
import dung.dto.dungSurgeryRecordBean;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Sony
 */
public class dungChangeOperatorDoctorDetailCartServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            HttpSession session = request.getSession();
            Object obj = session.getAttribute("cartSurgery");

            dungSurgeryRecordBean dSR = null;
            if (obj == null) {
                dSR = new dungSurgeryRecordBean();
            } else {
                dSR = (dungSurgeryRecordBean) obj;
            }

            /* Get DTO Drugs Detail */
            String destinationUrl = "dungFail.jsp";
            Object objOB = session.getAttribute("empInfo");
            if (objOB != null) {
                EmpInfo emI = (EmpInfo) objOB;
                String txtOperatorDoctorDetailCartId = request.getParameter("txtOperatorDoctorDetailCartId");
                String action = request.getParameter("action");
                if (action.equals("Delete")) {
                    if (dSR.removeOperatorDoctoDetail(txtOperatorDoctorDetailCartId)) {
                        session.setAttribute("cartSurgery", dSR);
                        destinationUrl = "dungOperatorDoctorCartServlet";
                    } else {
                        destinationUrl = "dungFail.jsp";
                    }
                }
            } else {
                destinationUrl = "login.jsp";
            }
            /* Set Session */

            request.getRequestDispatcher(destinationUrl).forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
