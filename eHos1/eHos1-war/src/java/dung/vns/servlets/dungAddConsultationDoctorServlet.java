/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dung.vns.servlets;

import dung.vns.ssbeans.dungConsultationSurgerySessionBean;
import dung.vns.ssbeans.dungPositionConsultationSessionBean;
import dung.vns.ssbeans.dungSuggestSurgerySessionBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import project.ett.TbConsulationSurgery;
import project.ett.TbEmployee;
import project.ett.TbPositionConsulation;
import project.ett.TbSuggestSurgery;

/**
 *
 * @author Sony
 */
public class dungAddConsultationDoctorServlet extends HttpServlet {
    @EJB
    private dungPositionConsultationSessionBean dungPositionConsultationSessionBean;
    
    @EJB
    private dungConsultationSurgerySessionBean dungConsultationSurgerySessionBean;
    @EJB
    private dungSuggestSurgerySessionBean dungSuggestSurgerySessionBean;

    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            String txtConsultationID = request.getParameter("txtConsultationID");
            TbConsulationSurgery tbCS = dungConsultationSurgerySessionBean.findByPK(txtConsultationID);
            
            TbSuggestSurgery tbSS = dungSuggestSurgerySessionBean.findByPK(tbCS.getSuggestSurgeryID().getSuggestSurgeryID());
            
            List<TbEmployee> tbEmp = dungSuggestSurgerySessionBean.getAllDoctor();
            
            List<TbPositionConsulation> tbPC = dungPositionConsultationSessionBean.getAllPositionConsultation();
            
            request.setAttribute("attConsultationID", txtConsultationID);
            request.setAttribute("dtoSuggest", tbSS);
            
            request.setAttribute("listtbDoctor", tbEmp);
            request.setAttribute("listtbPosition", tbPC);
            
            request.setAttribute("titlePage", "Insert Consultation Doctor Detail");
            
            request.getRequestDispatcher("dungInsertConsultationDoctor.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
