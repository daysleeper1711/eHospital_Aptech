/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dung.vns.servlets;

import dung.vns.ssbeans.dungSuggestSurgerySessionBean;
import dung.vns.ssbeans.dungSurgeryInfoSessionBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import project.ett.TbDrugsDetail;
import project.ett.TbOperatorDoctorDetail;
import project.ett.TbSuggestSurgery;
import project.ett.TbSuppliesDetail;
import project.ett.TbSurgeryRecord;

/**
 *
 * @author Sony
 */
public class dungReportSurgeryInfoServlet extends HttpServlet {
    @EJB
    private dungSuggestSurgerySessionBean dungSuggestSurgerySessionBean;
    @EJB
    private dungSurgeryInfoSessionBean dungSurgeryInfoSessionBean;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            String txtSurgeryRecordID = request.getParameter("txtSurgeryRecordID");
            TbSurgeryRecord tbSR = dungSurgeryInfoSessionBean.findByPK(txtSurgeryRecordID);
            request.setAttribute("tbSR", tbSR);
            
            String txtSuggestID = request.getParameter("txtSuggestID");
            TbSuggestSurgery tbSS = dungSuggestSurgerySessionBean.findByPK(txtSuggestID);
            request.setAttribute("rSS", tbSS);
            
            
            List<TbDrugsDetail> lstDrugsDetail = (List<TbDrugsDetail>) tbSR.getTbDrugsDetailCollection();
            request.setAttribute("lstDrugsDetail", lstDrugsDetail);
            
            List<TbSuppliesDetail> lstSuppliesDetail = (List<TbSuppliesDetail>) tbSR.getTbSuppliesDetailCollection();
            request.setAttribute("lstSuppliesDetail", lstSuppliesDetail);
            
            List<TbOperatorDoctorDetail> lstOperatorDoctor = (List<TbOperatorDoctorDetail>) tbSR.getTbOperatorDoctorDetailCollection();
            request.setAttribute("lstOperatorDoctor", lstOperatorDoctor);
            
            request.getRequestDispatcher("dungReportSurgeryInfo.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
