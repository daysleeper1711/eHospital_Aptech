/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dung.vns.servlets;

import dan.dto.EmpInfo;
import dung.vns.ssbeans.dungSuggestSurgerySessionBean;
import dung.vns.ssbeans.dungSurgeryInfoSessionBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.Date;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import project.ett.TbSurgeryRecord;

/**
 *
 * @author Sony
 */
public class dungUpdateSurgeryRecordServlet extends HttpServlet {

    @EJB
    private dungSurgeryInfoSessionBean dungSurgeryInfoSessionBean;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            HttpSession session = request.getSession();
            /* Get DTO Drugs Detail */
            String destinationUrl = "dungFail.jsp";
            Object objOB = session.getAttribute("empInfo");
            if (objOB != null) {
                EmpInfo emI = (EmpInfo) objOB;
                String action = request.getParameter("action");
                if (action.equals("Update")) {
                    // ID generate auto

                    String txtSuggestID = request.getParameter("txtSuggestID");
                    String txtSurgeryRecordID = request.getParameter("txtSurgeryRecordID");

                    Date currentD = new Date();
                    Timestamp tsDay = new Timestamp(currentD.getTime());
                    String txtProcessOfSurgery = request.getParameter("txtProcessOfSurgery");
                    if (txtProcessOfSurgery != null && !txtProcessOfSurgery.trim().isEmpty()) {
                        /* Get Time Of Start */
                        String vLocalDateStart = request.getParameter("txtTimeOfStart");
                        if (vLocalDateStart != null && !vLocalDateStart.isEmpty()) {
                            String[] vDateTimeStart = vLocalDateStart.trim().split(" ");
                            String[] vDateStart = vDateTimeStart[0].trim().split("/");
                            String[] vTimeStart = vDateTimeStart[1].trim().split(":");
                            Timestamp vTimeOfStart = new Timestamp(Integer.parseInt(vDateStart[2]) - 1900, Integer.parseInt(vDateStart[0]) - 1, Integer.parseInt(vDateStart[1]), Integer.parseInt(vTimeStart[0]), Integer.parseInt(vTimeStart[1]), 0, 0);

                            /* Get Time Of End */
                            String vLocalDateEnd = request.getParameter("txtTimeOfEnd");
                            if (vLocalDateEnd != null && !vLocalDateEnd.isEmpty()) {
                                String[] vDateTimeEnd = vLocalDateEnd.trim().split(" ");
                                String[] vDateEnd = vDateTimeEnd[0].trim().split("/");
                                String[] vTimeEnd = vDateTimeEnd[1].trim().split(":");
                                Timestamp vTimeOfEnd = new Timestamp(Integer.parseInt(vDateEnd[2]) - 1900, Integer.parseInt(vDateEnd[0]) - 1, Integer.parseInt(vDateEnd[1]), Integer.parseInt(vTimeEnd[0]), Integer.parseInt(vTimeEnd[1]), 0, 0);

                                /* validate datetime */
                                if (vTimeOfEnd.after(vTimeOfStart)) {

                                    String txtResultSurgery = request.getParameter("txtResultSurgery");
                                    if (txtResultSurgery != null && !txtResultSurgery.trim().isEmpty()) {
                                        String txtPayCost = request.getParameter("txtPayCost");
                                        boolean fsta = (request.getParameter("cbStatus") != null);

                                        TbSurgeryRecord addSRecord = new TbSurgeryRecord();

                                        addSRecord.setSurgeryRecordID(txtSurgeryRecordID);
                                        addSRecord.setProcessOfSurgery(txtProcessOfSurgery.trim());
                                        addSRecord.setTimeOfStart(vTimeOfStart);
                                        addSRecord.setTimeOfEnd(vTimeOfEnd);
                                        addSRecord.setSurgeryResult(txtResultSurgery.trim());
                                        addSRecord.setBeginCost(Double.parseDouble(txtPayCost));
                                        addSRecord.setPayCost(Double.parseDouble(txtPayCost));
                                        addSRecord.setStatusSurgery(fsta);
                                        addSRecord.setCreateDate(tsDay);

                                        if (dungSurgeryInfoSessionBean.updateSurgeryRecord(addSRecord, txtSuggestID, emI.getEmployeeID())) {

                                            destinationUrl = "dungSurgeryInfoManagerServlet";
                                        }
                                    } else {
                                        request.setAttribute("msgError", "Result Surgery is not blank.");
                                        destinationUrl = "dungEditSurgeryRecordServlet";
                                    }

                                } else {
                                    request.setAttribute("msgError", "Time of End has after Time of Start.");
                                    destinationUrl = "dungEditSurgeryRecordServlet";
                                }
                            } else {
                                request.setAttribute("msgError", "Time of End is not blank.");
                                destinationUrl = "dungEditSurgeryRecordServlet";
                            }

                        } else {
                            request.setAttribute("msgError", "Time of Start is not blank.");
                            destinationUrl = "dungEditSurgeryRecordServlet";
                        }
                    } else {
                        request.setAttribute("msgError", "Process of Surgery is not blank.");
                        destinationUrl = "dungEditSurgeryRecordServlet";
                    }

                } else if (action.equals("Cancel")) {
                    destinationUrl = "dungSurgeryInfoManagerServlet";
                }

            } else {
                destinationUrl = "login.jsp";
            }
            /* Set Session */

            request.getRequestDispatcher(destinationUrl).forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
