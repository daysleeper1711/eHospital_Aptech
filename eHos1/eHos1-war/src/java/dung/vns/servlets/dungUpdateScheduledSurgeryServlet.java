/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dung.vns.servlets;

import dan.dto.EmpInfo;
import dung.vns.ssbeans.dungScheduledSurgerySessionBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.Date;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import project.ett.TbScheduledSurgery;

/**
 *
 * @author Sony
 */
public class dungUpdateScheduledSurgeryServlet extends HttpServlet {

    @EJB
    private dungScheduledSurgerySessionBean dungScheduledSurgerySessionBean;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            String destinationUrl = "dungFail.jsp";
            HttpSession session = request.getSession();
            Object obj = session.getAttribute("empInfo");
            if (obj != null) {
                EmpInfo emI = (EmpInfo) obj;
                String action = request.getParameter("action");
                if (action.equals("Update")) {

                    String txtSuggestID = request.getParameter("txtSuggestID");

                    String txtScheduledID = request.getParameter("txtScheduledID");

                    String vLocalDateStart = request.getParameter("txtTimeOfStart");
                    if (vLocalDateStart != null && !vLocalDateStart.isEmpty()) {
                        String[] vDateTimeStart = vLocalDateStart.trim().split(" ");
                        String[] vDateStart = vDateTimeStart[0].trim().split("/");
                        String[] vTimeStart = vDateTimeStart[1].trim().split(":");
                        Timestamp vTimeOfStart = new Timestamp(Integer.parseInt(vDateStart[2]) - 1900, Integer.parseInt(vDateStart[0]) - 1, Integer.parseInt(vDateStart[1]), Integer.parseInt(vTimeStart[0]), Integer.parseInt(vTimeStart[1]), 0, 0);

                        /* Get Time Of End */
                        String vLocalDateEnd = request.getParameter("txtTimeOfEnd");
                        if (vLocalDateEnd != null && !vLocalDateEnd.isEmpty()) {
                            String[] vDateTimeEnd = vLocalDateEnd.trim().split(" ");
                            String[] vDateEnd = vDateTimeEnd[0].trim().split("/");
                            String[] vTimeEnd = vDateTimeEnd[1].trim().split(":");
                            Timestamp vTimeOfEnd = new Timestamp(Integer.parseInt(vDateEnd[2]) - 1900, Integer.parseInt(vDateEnd[0]) - 1, Integer.parseInt(vDateEnd[1]), Integer.parseInt(vTimeEnd[0]), Integer.parseInt(vTimeEnd[1]), 0, 0);

                            if (vTimeOfEnd.after(vTimeOfStart)) {
                                boolean fsta = (request.getParameter("cbStatus") != null);
                                Date currentD = new Date();
                                Timestamp tsDay = new Timestamp(currentD.getTime());

                                TbScheduledSurgery tbSScheduled = new TbScheduledSurgery();
                                tbSScheduled.setScheduledID(txtScheduledID);
                                tbSScheduled.setTimeOfStart(vTimeOfStart);
                                tbSScheduled.setTimeOfEnd(vTimeOfEnd);
                                tbSScheduled.setStatusScheduled(fsta);
                                tbSScheduled.setCreateDate(tsDay);

                                if (dungScheduledSurgerySessionBean.updateScheduledSurgery(tbSScheduled, txtSuggestID, emI.getEmployeeID())) {
                                    destinationUrl = "dungScheduledSurgeryManagerServlet";
                                }
                            } else {
                                request.setAttribute("msgError", "Time of End has after Time of Start.");
                                destinationUrl = "dungEditScheduledSurgeryServlet";
                            }
                        } else {
                            request.setAttribute("msgError", "Time of End is not blank.");
                            destinationUrl = "dungEditScheduledSurgeryServlet";
                        }
                    } else {
                        request.setAttribute("msgError", "Time of Start is not blank.");
                        destinationUrl = "dungEditScheduledSurgeryServlet";
                    }

                }
                if (action.equals("Cancel")) {
                    destinationUrl = "dungScheduledSurgeryManagerServlet";
                }
            } else {
                destinationUrl = "login.jsp";
            }
            request.getRequestDispatcher(destinationUrl).forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
