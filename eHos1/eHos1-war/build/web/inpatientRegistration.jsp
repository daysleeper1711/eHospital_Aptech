<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@include file="header.jsp" %>
<div class="col-sm-9 col-md-9 text-left">
    <h1 class="page-header">
        Inpatient Registration <small>${sessionScope.empInfo.faculty} - ${sessionScope.empInfo.position}</small>
    </h1>
</div>
</div>
<div class="row-fluid text-left">
    <div class="col-sm-9 col-md-9">
        <form style="margin: 20px" id="searchForm" 
              class="form-inline"
              action="RegistrationMainProcess" method="post"
              novalidate="novalidate">
            <p><strong>Search type</strong></p>
            <p>
                <a href="RegistrationMainProcess?createPatient=yes">Create new profile</a>
            </p>
            <div class="form-group">
                <select class="form-control" name="cbbSearchType">
                    <option selected value="sID_Card">ID card number</option>
                    <option value="sPatientName">Name</option>
                    <option value="sPatientID">Patient ID</option>
                </select> 
            </div>
            <div class="form-group">
                <input type="text" class="form-control"
                       placeholder="Your text here" name="txtSearchText"
                       data-parsley-notblank="true" data-parsley-required="true">
            </div>
            <div style="margin-bottom: 20px"></div>
            <button class="btn btn-default" type="submit" name="btnSubmit" value="search">
                Search <i class="fa fa-search"></i>
            </button>
            <!--end form search...-->
        </form>
        <!--DISPLAY NOTIFICATION-->
        <c:if test="${requestScope.insPatID ne null and requestScope.insMedRecordID ne null}">
            <div class="alert alert-success">
                <strong>Insert successfully!</strong> 
                <p>New Patient ID is ${requestScope.insPatID}</p> 
                <p>New Medical Record ID is ${requestScope.insMedRecordID}</p>
            </div>
        </c:if>
        <c:if test="${requestScope.insPatID eq null and requestScope.insMedRecordID ne null}">
            <div class="alert alert-success">
                <strong>Insert successfully!</strong> 
                <p>New Medical Record ID is ${requestScope.insMedRecordID}</p>
            </div>
        </c:if>
        <c:if test="${requestScope.fullFaculty ne null}">
            <div class="alert alert-warning">
                <strong>${requestScope.facultyToGo} is full!</strong>
                <p>Please confirm the administration...!!!</p>
            </div>
        </c:if>
        <!--DISPLAY RESULT ...-->
        <!--resultList is not null...-->
        <c:if test="${requestScope.resultList ne null}">
            <!--Empty result display here...-->
            <c:if test="${empty requestScope.resultList and empty requestScope.registeredList}">
                <div class="alert alert-warning">
                    <strong>Warning!</strong> <c:out value="Can not find data...!!!"></c:out>
                    </div>
            </c:if>
            <c:if test="${not empty requestScope.registeredList}">
                <h3>Already registered patient</h3>
                <ul id="itemContainer">
                    <c:forEach items="${requestScope.registeredList}" var="patient">
                        <li>
                            <p>Patient ID: ${patient.patID}</p>
                            <p>Patient Name: ${patient.patName}</p>
                            <p>Date of birth: ${patient.dob}</p>
                            <p>ID card: ${patient.iDCard}</p>
                        </li>
                    </c:forEach>  
                </ul>
                <!--PAGINATION-->
                <div class="holder"></div>
            </c:if>
            <!--Result display here...-->
            <c:if test="${not empty requestScope.resultList}">
                <div class="table-responsive">
                    <h3>List registered patient</h3>
                    <a href="RegistrationMainProcess?createPatient=yes">Create new profile</a>
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                                <th>Ord</th>
                                <th>Patient ID</th>
                                <th>Name</th>
                                <th>Date of birth</th>
                                <th>ID card</th>
                                <th>Created date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody id="itemContainer">
                            <c:forEach items="${requestScope.resultList}" var="patientInfo" varStatus="i">
                                <tr>
                                    <td>${i.count}</td>
                                    <td>${patientInfo.patID}</td>
                                    <td>${patientInfo.patName}</td>
                                    <td>
                                        ${patientInfo.dob}
                                    </td>
                                    <td>
                                        ${patientInfo.iDCard}
                                    </td>
                                    <td>
                                        ${patientInfo.regDate}
                                    </td>
                                    <td>
                                        <form  action="RegistrationMainProcess" method="post">
                                            <button type="submit" class="btn btn-default"
                                                    name="btnSubmit" value="view">
                                                View <i class="fa fa-angle-double-right"></i>
                                            </button>
                                            <input type="hidden" value="${patientInfo.patID}" name="passID" />
                                        </form>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
                <!--PAGINATION-->
                <div class="holder"></div>
            </c:if>
        </c:if>
        <!-- end table here-->
        <!--IMPORTANT!!! EDIT ZONE END FROM HERE - Do not change anything else-->
    </div>
</div>
<script type="text/javascript">
    var parsleyOptions = {
        successClass: "has-success",
        errorClass: "has-error",
        classHandler: function(e) {
            return e.$element.closest(".form-group");
        },
        errorsWrapper: '<span class="pull-right text-danger" style="padding-left: 10px; padding-top: 5px"></span>',
        errorTemplate: '<p></p>'
    };
    $("#searchForm").parsley(parsleyOptions);
</script>
<%@include file="footer.jsp" %>