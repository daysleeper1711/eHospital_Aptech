<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="header.jsp" %>
<div class="col-sm-9 col-md-9 text-left">
    <h1 class="page-header">
        Bed assignment <small>${sessionScope.empInfo.faculty} - ${sessionScope.empInfo.position}</small>
    </h1>
</div>
</div>
<div class="row-fluid text-left" >
    <!--DISPLAY Medical Record Info-->
    <div class="col-sm-8 col-md-8">
        <h3>Medical Record info</h3>
        <div class="table-responsive">
            <table class="showInfo">
                <tr>
                    <th>Medical Record ID:</th>
                    <td>${requestScope.medInfo.medRecordID}</td>
                    <th>Registry date:</th>
                    <td>${requestScope.medInfo.createDate}</td>
                </tr>
            </table>
        </div>
        <!--Show form-->
        <h3>Info details</h3>
        <div class="table-responsive">
            <table class="showInfo">
                <tr>
                    <th>Patient ID:</th>
                    <td colspan="5">${requestScope.medInfo.patInfo.patID}</td>
                </tr>
                <tr>
                    <th>Name:</th>
                    <td>${requestScope.medInfo.patInfo.patName}</td>
                    <th>Date of birth:</th>
                    <td>${requestScope.medInfo.patInfo.dob}</td>
                    <th>Gender:</th>
                    <td>${requestScope.medInfo.patInfo.gender}</td>
                </tr>
            </table>
        </div>
        <!--end show...-->
        <c:if test="${requestScope.listFreeBed ne null}">
            <c:if test="${not empty requestScope.listFreeBed}">
                <div class="table-responsive">
                    <h3>List available bed in faculty</h3>
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                                <th>Ord</th>
                                <th>Bed</th>
                                <th>Type</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${requestScope.listFreeBed}"
                                       var="bed" varStatus="i">
                            <tr>
                                <td>${i.count}</td>
                                <td>${bed.bedName}</td>
                                <td>${bed.bedType}</td>
                                <td>
                                    <form action="BedAssignmentMainProcess" method="post">
                                        <button type="submit" class="btn btn-default"
                                                name="btnSubmit" value="Give bed">
                                            Give <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                                        </button>
                                        <input type="hidden" name="bedID" value="${bed.bedID}" />
                                        <input type="hidden" name="medRecordID" value="${requestScope.medInfo.medRecordID}" />
                                    </form>
                                </td>
                            </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </c:if>
        </c:if>
    </div>
</div>	
<%@include file="footer.jsp" %>
