<%@include file="header.jsp" %>
<div class="col-sm-9 col-md-9 text-left">
    <h1 class="page-header">
        Medical Record Detail
    </h1>
</div>
</div>
<div class="row-fluid text-left" >
    <div class="col-sm-8 col-md-8">
        <h2>Patient Info details</h2>
        <div class="table-responsive">
            <table class="showInfo">
                <tr>
                    <th>Patient ID:</th>
                    <td colspan="5">${requestScope.medInfo.patInfo.patID}</td>
                </tr>
                <tr>
                    <th>Name:</th>
                    <td>${requestScope.medInfo.patInfo.patName}</td>
                    <th>Date of birth:</th>
                    <td>${requestScope.medInfo.patInfo.dob}</td>
                    <th>Gender:</th>
                    <td>${requestScope.medInfo.patInfo.gender}</td>
                </tr>
            </table>
        </div>
        <h2>Medical Record info</h2>
        <div class="table-responsive">
            <table class="showInfo">
                <tr>
                    <th>Medical Record ID:</th>
                    <td colspan="2">${requestScope.medInfo.medRecordID}</td>
                </tr>
                <tr>
                    <th>Registry date:</th>
                    <td>${requestScope.medInfo.createDate}</td>
                    <th>Registry by:</th>
                    <td>${requestScope.medInfo.createByWhom}</td>
                </tr>
                <tr>
                    <th>Symptoms:</th>
                    <td colspan="3">${requestScope.medInfo.symptoms}</td>
                </tr>
            </table>
        </div>
        <!--Show form-->
        <h2>Diagnosis Declaration</h2>
        <div class="table-responsive">
            <table class="showInfo">
                <tr>
                    <th>Diagnosis:</th>
                    <td colspan="5">${requestScope.medInfo.diagnosis}</td>
                </tr>
                <tr>
                    <th>Suggest treatment:</th>
                    <td colspan="5">${requestScope.medInfo.suggestTreatment}</td>
                </tr>
                <tr>
                    <th>Diagnosis date:</th>
                    <td>${requestScope.medInfo.diagnosisDate}</td>
                    <th>Diagnosis by:</th>
                    <td>${requestScope.medInfo.diagnosisDoc}</td>
                </tr>
            </table>
        </div>
        <h3>Faculty changement history </h3>
        <div class="table-responsive">
            <table class="showInfo">
                <tr>
                    <th>Date in hospital: </th>
                    <td colspan="5">${requestScope.medInfo.dateInHospital}</td>

                </tr>
                <tr>
                    <th>Faculty in: </th>
                    <td>${requestScope.medInfo.fch_start.toFaculty}</td>
                    <th>Accept in by: </th>
                    <td>${requestScope.medInfo.fch_start.acceptIn}</td>
                    <th>Bed: </th>
                    <td>${requestScope.medInfo.fch_start.bed.bedName}</td>
                </tr>
                <tr>
                    <th colspan="6">
                <h3>Faculty change information</h3>
                </th>
                </tr>
                <c:if test="${not empty requestScope.medInfo.listFCH_change}">
                    <c:forEach items="${requestScope.medInfo.listFCH_change}"
                               var="fch" varStatus="i">
                        <tr>
                            <th>
                        <h4>No: ${i.count}</h4>
                        </th>
                        </tr>
                        <tr>

                            <th>From faculty:</th>
                            <td colspan="2">${fch.fromFaculty}</td>
                            <th>To Faculty:</th>
                            <td colspan="2">${fch.toFaculty}</td>
                        </tr>
                        <tr>
                            <th>Reason to discharge</th>
                            <td colspan="5">${fch.reasonToChange}</td>
                        </tr>
                        <tr>
                            <th>Bed: </th>
                            <td>${fch.bed.bedName}</td>
                            <th>Date in:</th>
                            <td>${fch.dateIn}</td>
                            <th>Accept in by:</th>
                            <td>${fch.acceptIn}</td>
                        </tr>
                        <tr>
                            <th>Date out: </th>
                            <td colspan="2">${fch.dateOut}</td>
                            <th>Accept out by:</th>
                            <td colspan="2">${fch.acceptOut}</td>
                        </tr>
                    </c:forEach>
                </c:if>
            </table>
            <c:if test="${empty requestScope.medInfo.listFCH_change}">
                <div class="alert alert-info">
                    No change faculty had been made.
                </div>
            </c:if>
        </div>
        <h2>Conclusion</h2>
        <div class="table-responsive">
            <table class="showInfo">
                <tr>
                    <th>Main disease:</th>
                    <td colspan="2">${requestScope.medInfo.mainDisease}</td>
                    <th>Side disease:</th>
                    <td colspan="2">${requestScope.medInfo.sideDisease}</td>
                </tr>
                <tr>
                    <th>Treatment result</th>
                    <td coslpan="5">${requestScope.medInfo.treatmentResult}</td>
                </tr>
                <tr>
                    <th>Health conclusion:</th>
                    <td colspan="5">${requestScope.medInfo.healthCondition}</td>
                </tr>
                <tr>
                    <th>Date out hospital: </th>
                    <td>${requestScope.medInfo.conclusionDate}</td>
                    <th>Total treatment days: </th>
                    <td>${requestScope.medInfo.totalTreatmentDays}</td>
                    <th>Permission to discharge:</th>
                    <td>${requestScope.medInfo.conclusionByWhom}</td>
                </tr>
            </table>
        </div>
        <form id="actionForm" style="margin-bottom: 20px">
            <button type="submit" form="actionForm" class="btn btn-default">
                Back <i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
            </button>
        </form>
    </div>
</div>	
<%@include file="footer.jsp" %>