<%-- 
    Document   : dungInsertScheduledSurgery
    Created on : Jan 30, 2017, 4:53:30 PM
    Author     : Sony
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Insert Scheduled Surgery Page</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap Core CSS -->
        <link rel="stylesheet" href="css/w3.css">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/prettify-1.0.css" rel="stylesheet">
        <link href="css/base.css" rel="stylesheet">
        <link href="css/bootstrap-datetimepicker.css" rel="stylesheet">
        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">


        <!-- jQuery -->
        <script src="js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/analytics.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/moment-with-locales.js"></script>
        <script src="js/bootstrap-datetimepicker.js"></script>
        <style>
            /* Set black background color, white text and some padding */
            footer {
                background-color: #555;
                color: white;
                padding: 8px;
            }
            html {
                position: relative;
                min-height: 100%;
            }
            body {
                /* Margin bottom by footer height */
                margin-bottom: 20px;
            }
            footer {
                position: absolute;
                text-align: center;
                bottom: 0;
                width: 100%;
                /* Set the fixed height of the footer here */
                height: 20px;
                background-color: #f5f5f5;
            }
            .glyphicon { margin-right:10px; }
            .panel-body { padding:0px; }
            .panel-body table tr td { padding-left: 15px }
            .panel-body .table {margin-bottom: 0px; }

            header {
                background: url(images/banner.jpg) no-repeat;
                background-size: cover;
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;	
                height:170px;
            }
            .mr10 {
                margin-right: 10px;
            }
            .mr20 {
                margin-right: 20px;
            }
            .pd20 {
                padding-top: 20px;
            }
        </style>
    </head>

    <body>

        <%@include file="dungHeaderAdmin.jsp" %>

        <%@include file="dungAsideRightDoctor.jsp" %>

        <div class="row-fluid text-left">
            <div class="col-md-1 col-sm-1"></div>
            <div class="col-sm-6 col-md-6">
                <c:if test="${requestScope.msgError ne null}">
                    <label style="margin-left: 10px" class="w3-text-red">${requestScope.msgError}</label><br/>
                </c:if>
                <form style="padding:10px" action="InsertScheduledSurgeryServlet" method="post">
                    <label>Suggest No : ${requestScope.dtoSuggest.suggestSurgeryID}</label><br/>
                    <label>Surgery Service Name : ${requestScope.dtoSuggest.surgeryServiceID.surgeryServiceName}</label><br/>
                    <label>Medical Record : ${sessionScope.sInfo.medRecordID}</label><br/>
                    <label>Patient Name : ${sessionScope.sInfo.patInfo.patName}</label><br/>
                    <label>Sex : ${sessionScope.sInfo.patInfo.gender}</label><br/>
                    <label>Faculty Name : ${sessionScope.sInfo.currentFaculty}</label><br/>
                    <label>Bed Name : ${sessionScope.sInfo.bedInfo.bedName}</label><br/>
                    <div class="form-group">
                        <input class="form-control" name="txtSuggestID" type="hidden" value="${requestScope.dtoSuggest.suggestSurgeryID}">
                    </div>
                    <div class="form-group">
                        <label>Time Of Start</label>
                        <div class="input-group date" id="datetimepicker1">
                            <input type="text" class="form-control" name="txtTimeOfStart" />
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                    <script type="text/javascript">
                        $(function() {
                            $('#datetimepicker1').datetimepicker({
                                sideBySide: true,
                                format: 'MM/DD/YYYY HH:ss'
                            });
                        });
                    </script>
                    <div class="form-group">
                        <label>Time Of End</label>
                        <div class="input-group date" id="datetimepicker2">
                            <input type="text" class="form-control" name="txtTimeOfEnd" />
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                    <script type="text/javascript">
                        $(function() {
                            $('#datetimepicker2').datetimepicker({
                                sideBySide: true,
                                format: 'MM/DD/YYYY HH:ss'
                            });
                        });
                    </script>
                    <div style="text-align: center">
                        <button style="margin: 10px" type="submit" class="btn btn-primary" name="action" value="Insert">Insert</button>
                        <button style="margin: 10px" type="submit" class="btn btn-primary" name="action" value="Cancel">Cancel</button>
                    </div>
                </form>

                <!--IMPORTANT!!! EDIT ZONE START FROM HERE - Do not change anything else-->
                <!--I am not responsible any wrong if you change anything without permission-->
                <!--FOR THE ENGLISH TRANSLATE - GOOGLE PLZ ^_^-->
                <!--table here-->                
                <!-- end table here-->
                <!--IMPORTANT!!! EDIT ZONE END FROM HERE - Do not change anything else-->
            </div>
            <div class="col-md-1 col-sm-1"></div>
        </div>

        <%@include file="dungFooter.jsp" %>

        <!--css for footer-->

    </body>
</html>
<!-- css for side menu -->

