<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="loc" %>

<!DOCTYPE html>

<html >
    <head>
        <title>ItemCode Management</title>
        <meta charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- jQuery -->
        <script language="javascript" src="js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>
        <style>    
            /* Set black background color, white text and some padding */
            footer {
                background-color: #555;
                color: white;
                padding: 8px;
            }

            .loccolum{
                display: table-cell;
                padding: 0px 8px 0px 0px;
                width: 40%;
            }
            .loccenter{
                display: table;
                padding: 0px 0px 0px 20px;
            }


        </style>
        <!--pagination-->
        <script type="text/javascript" src="js/jPager/js.js"></script>
        <script type="text/javascript" src="js/jPager/jPages.min.js"></script>
        <link rel="stylesheet" href="css/jPager/jPages.css" type="text/css"/>

        <script>
//            run when document is ready
            $(function() {
//                initiate plugin
                $("div.holder").jPages({
                    containerID: "itemContainer",
                    perPage: 5
                });
            });
        </script>

    </head>
    <body>
        <%@include file="locGUI/locheader.jsp" %>

        <div class="row-fluid text-center">
            <%@include file="locGUI/locmenu.jsp" %>
            <div class="col-sm-9 col-md-9">
                <h1 class="page-header">
                    <small>ItemCode Management</small> <!--Change here for title of the page-->
                </h1>
            </div>

        </div>
        <div class="row-fluid text-left">
            <div class="col-md-1 col-sm-1"></div>
            <div class="col-sm-6 col-md-6">
                <form style="padding:20px" action="SearchItemCodeServlet" method="post">
                    <!--IMPORTANT!!! EDIT ZONE START FROM HERE - Do not change anything else-->
                    <!--I am not responsible any wrong if you change anything without permission-->
                    <!--FOR THE ENGLISH TRANSLATE - GOOGLE PLZ ^_^-->

                    <!--copy form input here...-->
                    <!--input 1-->

                    <div class="loccenter" >
                        <!--Left-->
                        <div class="loccolum">

                            <div class="form-group">
                                <label>ItemCode</label>
                                <input class="form-control" id="txtItemCode" name="txtItemCode">
                                <p style="color: red" id="errortxtItemCode"></p>
                            </div>



                            <div class="form-group">
                                <label>GenericDrug</label>
                                <input class="form-control" id="txtGenericDrug" name="txtGenericDrug">
                                <p style="color: red" id="errortxtGenericDrug"></p>
                            </div>
                        </div>

                        <!--Right-->
                        <div class="loccolum">
                            <div class="form-group">
                                <label>ItemName</label>
                                <input class="form-control" id="txtItemName" name="txtItemName">
                                <p style="color: red" id="errortxtItemName"></p>
                            </div>

                            <div class="form-group">
                                <label>Type</label>
                                <select class="form-control" id="select" name="select">
                                    <option>All</option>
                                    <option>Drugs</option>
                                    <option>Supplies</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group" id="changeradio" >
                        <label>Search ... </label>
                        <label class="radio-inline">
                            <input type="radio" name="optionsRadiosInline" id="radio" value="ItemCode" checked> ItemCode
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="optionsRadiosInline" id="radio" value="ItemName"> ItemName
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="optionsRadiosInline" id="radio" value="GenericDrug"> GenericDrug   
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="optionsRadiosInline" id="radio" value="Type"> Type
                        </label>
                    </div>

                    <div style="text-align: center">
                        <button type="submit" class="btn btn-default" name="action" id="btnSearch">Search</button> 
                    </div>

                    <script language="javascript">
                        $(document).ready(function()
                        {
                            //Chan disabled luc onLoad
                            var a = $('input:checked').val();
                            if (a === "ItemCode") {
                                $('#txtItemCode').prop("disabled", false);
                                $('#txtGenericDrug').prop("disabled", true);
                                $('#txtItemName').prop("disabled", true);
                                $('#select').prop("disabled", true);
                            } else if (a === "ItemName")
                            {
                                $('#txtItemCode').prop("disabled", true);
                                $('#txtGenericDrug').prop("disabled", true);
                                $('#txtItemName').prop("disabled", false);
                                $('#select').prop("disabled", true);
                            } else if (a === "GenericDrug")
                            {
                                $('#txtItemCode').prop("disabled", true);
                                $('#txtGenericDrug').prop("disabled", false);
                                $('#txtItemName').prop("disabled", true);
                                $('#select').prop("disabled", true);
                            } else
                            {
                                $('#txtItemCode').prop("disabled", true);
                                $('#txtGenericDrug').prop("disabled", true);
                                $('#txtItemName').prop("disabled", true);
                                $('#select').prop("disabled", false);
                            }

                            //Chan disabled luc check chon radio
                            $('#changeradio').change(function() {
                                var a = $('input:checked').val();
                                if (a === "ItemCode") {
                                    $('#txtItemCode').prop("disabled", false);
                                    $('#txtGenericDrug').prop("disabled", true);
                                    $('#txtItemName').prop("disabled", true);
                                    $('#select').prop("disabled", true);
                                } else if (a === "ItemName")
                                {
                                    $('#txtItemCode').prop("disabled", true);
                                    $('#txtGenericDrug').prop("disabled", true);
                                    $('#txtItemName').prop("disabled", false);
                                    $('#select').prop("disabled", true);
                                } else if (a === "GenericDrug")
                                {
                                    $('#txtItemCode').prop("disabled", true);
                                    $('#txtGenericDrug').prop("disabled", false);
                                    $('#txtItemName').prop("disabled", true);
                                    $('#select').prop("disabled", true);
                                } else
                                {
                                    $('#txtItemCode').prop("disabled", true);
                                    $('#txtGenericDrug').prop("disabled", true);
                                    $('#txtItemName').prop("disabled", true);
                                    $('#select').prop("disabled", false);
                                }
                            });

                            //Chan Blank
                            $('#btnSearch').click(function() {
                                $('#errortxtItemName').text("");
                                $('#errortxtItemCode').text("");
                                $('#errortxtGenericDrug').text("");
                                var a = $('input:checked').val();

                                if (a === "ItemCode") {
                                    if ($('#txtItemCode').val() === "")
                                    {
                                        $('#errortxtItemCode').text("ItemCode not blank");
                                        return false;
                                    }
                                } else if (a === "ItemName")
                                {
                                    if ($('#txtItemName').val() === "")
                                    {
                                        $('#errortxtItemName').text("ItemName not blank");
                                        return false;
                                    }
                                } else if (a === "GenericDrug")
                                {
                                    if ($('#txtGenericDrug').val() === "")
                                    {
                                        $('#errortxtGenericDrug').text("GenericDrug not blank");
                                        return false;
                                    }
                                }

                            });
                        });

                    </script>

                </form><br/><br/>

                <a href="HyperlinkServlet?action=Add New ItemCode">Add New ItemCode</a>

                <loc:if test="${not empty requestScope.list}">

                    <h2>List ItemCode</h2>
                    <div class="table-responsive" >
                        <table class="table table-bordered table-hover table-striped" >
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>ItemCode</th>
                                    <th>ItemName</th>                                      
                                    <th>GenericDrug</th>
                                    <th>Type</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody id="itemContainer">
                                <loc:forEach items="${requestScope.list}"  var="dto" varStatus="stt">
                                    <tr>
                                        <td>${stt.count}</td>
                                        <td>${dto.itemCode}</td>
                                        <td>${dto.itemName}</td>
                                        <td>${dto.genericDrug}</td>
                                        <td>${dto.typeITcode}</td>

                                        <td>
                                            <form action="EditItemCodeServlet" method="post">
                                                <input type="submit" value="Edit" name="action"/>

                                                <input type="hidden" value="${dto.itemCode}" name="txtID"/>
                                            </form>
                                        </td>
                                    </tr>

                                </loc:forEach>
                            </tbody>
                        </table>
                    </div>
                    <!--pagination-->
                    <div class="holder" align="right"></div>
                </loc:if>
            </div>
            <div class="col-md-1 col-sm-1"></div>
        </div>	

        <%@include file="locGUI/locfooter.jsp" %>
    </body>
</html>
