<%@include file="header.jsp" %>
<div class="col-sm-9 col-md-9 text-left">
    <h1 class="page-header">
        Request to change faculty <small>${sessionScope.empInfo.faculty} - ${sessionScope.empInfo.position}</small>
    </h1>
</div>
</div>
<div class="row-fluid text-left" >
    <div class="col-sm-8 col-md-8">
        <h3>Medical Record info</h3>
        <div class="table-responsive">
            <table class="showInfo">
                <tr>
                    <th>Medical Record ID:</th>
                    <td>${requestScope.medicalRecordInfo.medRecordID}</td>
                    <th>Date in:</th>
                    <td>${requestScope.medicalRecordInfo.dateInHospital}</td>
                    <th>Bed:</th>
                    <td>${requestScope.medicalRecordInfo.bedInfo.bedName}</td>
                    <th>Type:</th>
                    <td>${requestScope.medicalRecordInfo.bedInfo.bedType}</td>
                </tr>
            </table>
        </div>
        <!--Show form-->
        <h3>Info details</h3>
        <div class="table-responsive">
            <table class="showInfo">
                <tr>
                    <th>Patient ID:</th>
                    <td colspan="5">${requestScope.medicalRecordInfo.patInfo.patID}</td>
                </tr>
                <tr>
                    <th>Name:</th>
                    <td>${requestScope.medicalRecordInfo.patInfo.patName}</td>
                    <th>Date of birth:</th>
                    <td>${requestScope.medicalRecordInfo.patInfo.dob}</td>
                    <th>Gender:</th>
                    <td>${requestScope.medicalRecordInfo.patInfo.gender}</td>
                </tr>
            </table>
        </div>
        <!--end show...-->
        <form style="width: 70%;margin-bottom: 20px;" 
              action="DailyTreatmentMainProcess"
              method="post">
            <h3>Declaration</h3>
            <div class="form-group">
                <label>Change to faculty</label>
                <select class="form-control" name="cbbfacultyChange"> 
                    <c:forEach items="${requestScope.lstFacultyChange}"
                               var="facultyName">
                        <option value="${facultyName}">${facultyName}</option>
                    </c:forEach>
                </select>
            </div>
            <div class="form-group">
                <label>Reason to change</label>
                <textarea class="form-control" rows="3" placeholder="Reason to change faculty..."
                          name="txtReasonToChange"></textarea>
            </div>
            <button type="submit" class="btn btn-default" 
                    name="btnSubmit" value="change faculty confirm">
                Submit <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
            </button>
            <input type="hidden" value="${requestScope.medicalRecordInfo.medRecordID}"
                   name="passID" />
        </form>
    </div>
</div>	
<%@include file="footer.jsp" %>
