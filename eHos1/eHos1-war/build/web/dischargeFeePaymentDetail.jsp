<%@include file="header.jsp" %>
<div class="col-sm-9 col-md-9 text-left">
    <h1 class="page-header">
        Discharge fee details <small>${sessionScope.empInfo.faculty} - ${sessionScope.empInfo.position}</small>
    </h1>
</div>
</div>
<div class="row-fluid text-left">
    <div class="col-sm-8 col-md-8">
        <h2>Patient Info details</h2>
        <div class="table-responsive">
            <table class="showInfo">
                <tr>
                    <th>Patient ID:</th>
                    <td colspan="5">${requestScope.medInfo.patInfo.patID}</td>
                </tr>
                <tr>
                    <th>Name:</th>
                    <td>${requestScope.medInfo.patInfo.patName}</td>
                    <th>Date of birth:</th>
                    <td>${requestScope.medInfo.patInfo.dob}</td>
                    <th>Gender:</th>
                    <td>${requestScope.medInfo.patInfo.gender}</td>
                </tr>
            </table>
        </div>
        <h2>Medical Record info</h2>
        <div class="table-responsive">
            <table class="showInfo">
                <tr>
                    <th>Medical Record ID:</th>
                    <td colspan="2">${requestScope.medInfo.medRecordID}</td>
                </tr>
                <tr>
                    <th>Date in hospital:</th>
                    <td>${requestScope.medInfo.dateInHospital}</td>
                    <th>Bed: </th>
                    <td>${requestScope.medInfo.bedInfo.bedName}</td>
                </tr>
            </table>
        </div>
        <!--Display list patient's waiting-->
        <div class="table-responsive">
            <h2>List Fee details</h2>
            <table class="table table-bordered table-striped">
                <!--BED FEE DISPLAY-->
                <tr>
                    <th colspan="5">
                <h3>Bed's fee</h3>
                </th>
                </tr>
                <tr>
                    <th>No</th>
                    <th>Name</th>
                    <th>Quantity</th>
                    <th>Price</th>
                    <th>Total</th>
                </tr>
                <c:forEach items="${requestScope.tFee.bedFee.lstBedService}"
                           var="bs" varStatus="i">
                    <tr>
                        <td>${i.count}</td>
                        <td>${bs.name}</td>
                        <td>${bs.quantity}</td>
                        <td>${bs.price}</td>
                        <td>${bs.total}</td>
                    </tr>
                </c:forEach>
                <tr>
                    <td colspan="3"></td>
                    <th>Total Bed fee: </th>
                    <td>${requestScope.tFee.bedFee.totalPayment}</td>
                </tr>
                <!--ALL TEST FEE DISPLAY-->
                <c:if test="${requestScope.noSubclinicalTest ne null}">
                    <div class="alert alert-info">
                        <strong>No test request</strong>
                    </div>
                </c:if>
                <c:if test="${requestScope.noSubclinicalTest eq null}">
                    <tr>
                        <th colspan="5">
                    <h3>Subclinical test service fee</h3>
                    </th>
                    </tr>
                    <!--Get details of test fee-->
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Quantity</th>
                        <th>Price</th>
                        <th>Total</th>
                    </tr>
                    <c:forEach items="${requestScope.tFee.testFee.lstTestService}"
                               var="ts" varStatus="i">
                        <tr>
                            <td>${i.count}</td>
                            <td>${ts.name}</td>
                            <td>${ts.quantity}</td>
                            <td>${ts.price}</td>
                            <td>${ts.total}</td>
                        </tr>
                    </c:forEach>
                    <tr>
                        <td colspan="3"></td>
                        <th>Total test fee: </th>
                        <td>${requestScope.tFee.testFee.totalPayment}</td>
                    </tr>
                </c:if>
                <!--ALL DAILY DRUGS FEE-->
                <!--if daily treatment has no drugs and no supplies use-->
                <c:if test="${requestScope.noDailyDrugs eq null}">
                    <tr>
                        <th colspan="5">
                    <h3>Daily Drugs and Drugs' supplies using</h3>
                    </th>
                    </tr>
                </c:if>
                <!--if daily treatment has no drugs use-->
                <c:if test="${requestScope.noDailyDrugs ne null and requestScope.noDailySuppliesDrugs ne null}">
                    <div class="alert alert-info">
                        <strong>No daily drugs using</strong>
                    </div>
                </c:if>
                <!--display daily drugs use and fee-->
                <c:if test="${requestScope.noDailyDrugs eq null}">
                    <!--Get details of daily drugs use fee-->
                    <tr>
                        <th colspan="5">
                    <h4>Daily Drugs' supplies use</h4>
                    </th>
                    </tr>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Quantity</th>
                        <th>Price</th>
                        <th>Total</th>
                    </tr>
                    <c:forEach items="${requestScope.tFee.dailyDrugFee.lstDrugService}"
                               var="ts" varStatus="i">
                        <tr>
                            <td>${i.count}</td>
                            <td>${ts.name}</td>
                            <td>${ts.quantity}</td>
                            <td>${ts.price}</td>
                            <td>${ts.total}</td>
                        </tr>
                    </c:forEach>
                    <tr>
                        <td colspan="3"></td>
                        <th>Total daily drugs fee: </th>
                        <td>${requestScope.tFee.dailyDrugFee.totalPaymentForDrugs}</td>
                    </tr>
                </c:if>
                <!--if daily treatment has no drugs supplies use-->
                <c:if test="${requestScope.noDailySuppliesDrugs ne null}">
                    <div class="alert alert-info">
                        <strong>No daily drugs supplies using</strong>
                    </div>
                </c:if>
                <!--display daily drugs supplies use and fee-->
                <c:if test="${requestScope.noDailySuppliesDrugs eq null}">
                    <!--Get details of daily drugs supplies fee-->
                    <tr>
                        <th colspan="5">
                    <h4>Daily Drugs use</h4>
                    </th>
                    </tr>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Quantity</th>
                        <th>Price</th>
                        <th>Total</th>
                    </tr>
                    <c:forEach items="${requestScope.tFee.dailyDrugFee.lstDrugSuppliesService}"
                               var="ts" varStatus="i">
                        <tr>
                            <td>${i.count}</td>
                            <td>${ts.name}</td>
                            <td>${ts.quantity}</td>
                            <td>${ts.price}</td>
                            <td>${ts.total}</td>
                        </tr>
                    </c:forEach>
                    <tr>
                        <td colspan="3"></td>
                        <th>Total daily drugs' supplies fee: </th>
                        <td>${requestScope.tFee.dailyDrugFee.totalPaymentForDrugsSupplies}</td>
                    </tr>
                </c:if>
                <c:if test="${requestScope.noDailyDrugs eq null or requestScope.noDailySuppliesDrugs eq null}">
                    <tr>
                        <th colspan="5">
                    <h4>Summary</h4>
                    </th>
                    </tr>
                    <tr>
                        <td colspan="3"></td>
                        <th>Total daily drugs and drugs' supplies fee: </th>
                        <td>${requestScope.tFee.dailyDrugFee.totalPayment}</td>
                    </tr>
                </c:if>
                <!--SURGERY FEE DISPLAY-->
                <c:if test="${requestScope.noSurgery ne null}">
                    <div class="alert alert-info">
                        <strong>No surgery request</strong>
                    </div>
                </c:if>
                <c:if test="${requestScope.noSurgery eq null}">
                    <tr>
                        <th colspan="5">
                    <h3>Surgery fee</h3>
                    </th>
                    </tr>
                    <!--Get details of test fee-->
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Quantity</th>
                        <th>Price</th>
                        <th>Total</th>
                    </tr>
                    <c:forEach items="${requestScope.tFee.surgeryFee.lstSurgeryService}"
                               var="ts" varStatus="i">
                        <tr>
                            <td>${i.count}</td>
                            <td>${ts.name}</td>
                            <td>${ts.quantity}</td>
                            <td>${ts.price}</td>
                            <td>${ts.total}</td>
                        </tr>
                    </c:forEach>
                    <tr>
                        <td colspan="3"></td>
                        <th>Total surgery fee: </th>
                        <td>${requestScope.tFee.surgeryFee.totalPayment}</td>
                    </tr>
                </c:if>
                <!--SUMMARY-->
                <tr>
                    <th colspan="5">
                <h3>Summary treatment's fee</h3>
                </th>
                <tr>
                    <td colspan="3"></td>
                    <th>Total fee: </th>
                    <td>${requestScope.tFee.totalFee}</td>
                </tr>
                <tr>
                    <td colspan="3"></td>
                    <th>Total deposit: </th>
                    <td>${requestScope.tFee.totalDeposit}</td>
                </tr>
                <tr>
                    <td colspan="3"></td>
                    <th>Total return: </th>
                    <td>${requestScope.tFee.totalReturn}</td>
                </tr>
                <tr>
                    <td colspan="3"></td>
                    <th>Total pay more: </th>
                    <td>${requestScope.tFee.totalPaymore}</td>
                </tr>
                <tr>
                    <td colspan="5">
                        <form action="DischargeFeeMainProcess"
                              method="post">
                            <button type="submit" class="btn btn-default"
                                    value="report preview" name="btnSubmit">
                                        Preview <i class="fa fa-search" aria-hidden="true"></i>
                            </button>
                            <input type="hidden" value="${requestScope.medInfo.medRecordID}"
                                   name="passID" />
                        </form>
                    </td>
                </tr>
            </table>
        </div>
        <!-- end table here-->
        <!--IMPORTANT!!! EDIT ZONE END FROM HERE - Do not change anything else-->
    </div>
</div>	
<%@include file="footer.jsp" %>
