<%-- 
    Document   : AddMedicineSupplies
    Created on : Jan 8, 2017, 5:46:39 PM
    Author     : Nguyet Phuong
--%>
<%@include file="header.jsp" %>
<div class="col-sm-9 col-md-9">
    <h1 class="page-header">
        Add new medicine supplies <!--Change here for title of the page-->
    </h1>
</div>
</div>
<div class="row-fluid text-left">
    <div class="col-md-1 col-sm-1"></div>
    <div class="col-sm-6 col-md-6">

        <div class="row">
            <div class="col-md-7"><strong>Medical record ID :</strong> ${sessionScope.MEDID.medRecordID}</div>
            <div class="col-md-5"><strong>Patient name:</strong> ${sessionScope.MEDID.patInfo.patName}</div>
        </div>
        <div class="row" style="padding-top: 20px;">
            <div class="col-md-4"><strong>Birthday:</strong> ${sessionScope.MEDID.patInfo.dob}</div>
            <div class="col-md-4"><strong>Sex:</strong> ${sessionScope.MEDID.patInfo.gender}</div>
            <div class="col-md-4 text-center"><strong>Doctor :</strong> ${sessionScope.empInfo.empName}</div>
        </div>
        <div class="row" style="padding-top: 20px;">
            <div class="col-md-3"><strong>Date in:</strong> ${sessionScope.MEDID.dateInHospital}</div>
            <div class="col-md-4"><strong>Faculty:</strong> ${sessionScope.MEDID.currentFaculty}</div>
            <div class="col-md-2"><strong>Bed:</strong> ${sessionScope.MEDID.currentBed}</div>
        </div>
        <div class="row" style="padding-top: 20px;">
            <div class="col-md-12"><strong>Delitescence:</strong><span style="max-width: 400px;"> ${sessionScope.MEDID.symptoms}</span></div>
        </div>
        <form action="PhuongControllerAdd" method="post" id="inputForm">
            <div class="row" style="padding-top: 20px;">
                <div class="col-md-3">
                    <strong>Medicine Name: </strong>
                </div>
                <div class="col-md-9">
                    <select name="lstMedicineID" class="js-example-basic-single">
                        <c:forEach items="${requestScope.LISTMED}" var="dto">
                            <option value="${dto.id}">${dto.name}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class="row" style="padding-top: 20px;">
                <div class="col-md-6">
                    <strong>Create for date:</strong>
                    <input class="txtCreFoDate" name="txtCreateForDate" id="datetimepicker"/>
                    <script type="text/javascript">
                        $(function() {      
                            var currentDate1 = new Date();
                            var trung = new Date();
                            var trung1 = new Date();
                            trung.setDate(trung1.getDate()+4);
                            var maxD = trung;
                            $('#datetimepicker').datetimepicker({
                                defaultDate: currentDate1,
                                format: 'DD/MM/YYYY',
                                minDate: currentDate1,
                                maxDate: maxD
                             });

                        });
                    </script>
                </div>
                <div class="col-md-3 form-group">
                    <strong>Quantity:</strong> <input type="number" name="txtQuantity" style="width: 50px;" min="0" max="10" /> 
                </div>
            </div>
            <div class="row" style="padding-top: 20px;">
                <div class="col-md-4"></div>
                <div class="col-md-4"></div>
                <div class="col-md-4 text-right"> 
                   <button type="submit" name="action" value="InsertSupp" class="btn btn-info"><i class="fa fa-download"></i> Insert</button>
                </div>
            </div>
        </form>
        <c:if test="${requestScope.ERROR ne null}"><span style="color: red;"> ${requestScope.ERROR}</span></c:if>
            <div class="row" style="padding-top: 20px;">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                                <th>Medicine supplies Name</th>
                                <th>Unit</th>
                                <th>Quantity</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${sessionScope.PHUONGMEDICINESUPP.lstMedSuppDTO}" var="medDTO">
                        <form action="PhuongControllerAdd" method="post" id="inputForm">
                            <tr>
                                <td>${medDTO.name}</td>
                                <td>${medDTO.unit}</td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" value="${medDTO.quantity}" min="0" max="10"  name="txtQ" style="width: 50px;"/>
                                    </div>
                                </td>
                                <td>
                                    <input type="hidden" name="txtIDMED" value="${medDTO.id}"/>
                                    <button type="submit" value="UpdateSupp" name="action" class="btn btn-success"><i class="fa fa-refresh"></i> Update</button>
                                    <button type="submit" value="DeleteSupp" name="action" class="btn btn-danger"><i class="fa fa-times"></i> Remove</button>
                                </td>
                            </tr>
                        </form>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row" style="padding-bottom: 30px;">
            <div class="col-md-4"></div>
            <div class="col-md-4 text-center">
                <form action="PhuongControllerAdd" method="post" id="inputForm">
                    <input type="hidden"  name="txtCreateForDate" id="txtCreateForDate"/>
                    <input type="hidden" name="MedRCID" value="${sessionScope.MEDID.medRecordID}"/>
                    <input type="hidden" name="txtMedRCID" value="${sessionScope.MEDID.medRecordID}"/>
                    <input type="submit" value="Insert Medicine Supplies" name="action" class="btn btn-primary phuongadd"/>
                </form>
            </div>
            <div class="col-md-4"></div>
        </div>
        </form>
        <!-- end table here-->
        <!--IMPORTANT!!! EDIT ZONE END FROM HERE - Do not change anything else-->
    </div>
    <div class="col-md-1 col-sm-1"></div>
</div>	
<script type="text/javascript">
    var parsleyOptions = {
        successClass: "has-success",
        errorClass: "has-error",
        classHandler: function(e) {
            return e.$element.closest(".form-group");
        },
        errorsWrapper: '<span class="center-block text-danger" style="padding-left: 10px; padding-top: 5px"></span>',
        errorTemplate: '<p></p>'
    };
    $("#inputForm").parsley(parsleyOptions);

    $(".phuongadd").click(function() {
        var dig = $(".txtCreFoDate").val();
        $("#txtCreateForDate").val(dig);
        if (!$("#txtCreateForDate").val()) {
            alert("Please insert create for date before Insert Prescription!!!");
            return false;
        }
    });
</script>                    
<%@include file="footer.jsp" %>