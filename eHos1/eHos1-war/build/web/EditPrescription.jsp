<%-- 
    Document   : EditPrescription
    Created on : Jan 8, 2017, 1:51:33 PM
    Author     : Nguyet Phuong
--%>
<%@include file="header.jsp" %>
<div class="col-sm-9 col-md-9">
    <h1 class="page-header">
        Update prescription <!--Change here for title of the page-->
    </h1>
</div>
</div>
<div class="row-fluid text-left">
    <div class="col-md-1 col-sm-1"></div>
    <div class="col-sm-6 col-md-6">
        <div class="row">
            <div class="col-md-7"><strong>Medical record ID :</strong> ${sessionScope.MEDID.medRecordID}</div>
            <div class="col-md-5"><strong>Patient name:</strong> ${sessionScope.MEDID.patInfo.patName}</div>
        </div>
        <div class="row" style="padding-top: 20px;">
            <div class="col-md-4"><strong>Birthday:</strong> ${sessionScope.MEDID.patInfo.dob}</div>
            <div class="col-md-4"><strong>Sex:</strong> ${sessionScope.MEDID.patInfo.gender}</div>
            <div class="col-md-4 text-center"><strong>Doctor :</strong> ${requestScope.EDITPRES.pres.employeeID.fullname}</div>
        </div>
        <div class="row" style="padding-top: 20px;">
            <div class="col-md-3"><strong>Date in:</strong> ${sessionScope.MEDID.dateInHospital}</div>
            <div class="col-md-4"><strong>Faculty:</strong> ${sessionScope.MEDID.currentFaculty}</div>
            <div class="col-md-2"><strong>Bed:</strong> ${sessionScope.MEDID.currentBed}</div>
        </div>
        <div class="row" style="padding-top: 20px;">
            <div class="col-md-12"><strong>Delitescence:</strong> <span style="max-width: 500px;">${sessionScope.MEDID.symptoms}</span></div>
        </div>
        <div class="row" style="padding-top: 20px;">
            <div class="col-md-2">
                <strong>Dignose:</strong> 
            </div>
            <form action="PhuongControllerEdit" method="post" id="inputForm">
                <div class="col-md-9">
                    <input type="hidden" name="txtPresID" value="${requestScope.EDITPRES.pres.prescriptionID}"/>
                    <div class="form-group">
                        <textarea style="width: 500px; height: 100px;" name="txtDiagnose"  class="txtDiag" data-parsley-required="true" data-parsley-minlength="5" data-parsley-maxlength="255">${requestScope.EDITPRES.pres.dignose}</textarea>
                    </div>
                </div>
                <div class="col-md-1"><button style="float: right; background-color: #ffffff; border: none;" type="submit" name="action" value="Update Prescription"><i class="fa fa-refresh" style="color: green;"></i></button></div>
            </form>
        </div>
        <hr/>
        <form action="PhuongControllerEdit" method="post" id="inputForm">
            <input type="hidden" name="txtPresID" value="${requestScope.EDITPRES.pres.prescriptionID}"/>
            <div class="row" style="padding-top: 20px;">
                <div class="col-md-3">
                    <strong>Medicine Name: </strong>
                </div>
                <div class="col-md-9">
                    <select name="lstMediID" class="js-example-basic-single">
                        <c:forEach items="${requestScope.LISTMED}" var="dto">
                            <option value="${dto.id}">${dto.name}</option>
                        </c:forEach>                          
                    </select>
                </div>
            </div>
            <div class="row" style="padding-top: 20px;">
                <div class="col-md-4"><strong>Morning:</strong> <input type="number" name="txtMorning" style="width: 50px;" min="0" max="10" /> </div>
                <div class="col-md-4"><strong>Afternoon:</strong> <input type="number" name="txtAfternoon" style="width: 50px;" min="0" max="10" /> </div>
                <div class="col-md-4"><strong>Night:</strong> <input type="number" name="txtNight" style="width: 50px;" min="0" max="10" /> </div>
            </div>
            <div class="row" style="padding-top: 20px;">
                <div class="col-md-6">
                    <strong>Create for date:</strong> ${requestScope.DATE}
                </div>
                <!--<div class="col-md-3"><strong>Quantity:</strong> <input type="number" readonly value="5" style="width: 50px;"/> </div>-->
            </div>
            <div class="row" style="padding-top: 20px;">
                <div class="col-md-5">
                </div>
                <div class="col-md-3"></div>
                <div class="col-md-4 text-right">
                    <input type="hidden" name="medRC" value="${sessionScope.MEDID.medRecordID}"/>
                    <button type="submit" name="action"  value="InsertPres" class="btn btn-info"><i class="fa fa-download"></i> Insert</button>
                </div>
            </div>
        </form>
        <c:if test="${requestScope.ERROR ne null}"><span style="color: red;"> ${requestScope.ERROR}</span></c:if>
            <div class="row" style="padding-top: 20px;">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                                <th>Medicine Name</th>
                                <th>Morning</th>
                                <th>Afternoon</th>
                                <th>Night</th>
                                <th>Quantity</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${requestScope.EDITPRES.lstDetail}" var="dto">
                        <form action="PhuongControllerEdit" method="post" id="inputForm">
                            <tr>
                                <td>${dto.medicineID.itemName}</td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" value="${dto.morning}" name="txtM" style="width: 50px;" min="0" max="10" />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group"><input type="number" value="${dto.afternoon}" name="txtA" style="width: 50px;"  min="0" max="10" /></div>
                                </td>
                                <td>
                                    <div class="form-group"><input type="number" value="${dto.night}" name="txtN" style="width: 50px;" min="0" max="10" /></div>
                                </td>
                                <td><input type="number" name="txtQ" value="${dto.quantity}" style="width: 50px;" readonly/></td>
                                <td>
                                    <input type="hidden" name="txtPresDetaID" value="${dto.prescriptionDetailID}"/>
                                    <input type="hidden" name="txtMedID" value="${dto.medicineID.itemCode}"/>
                                    <button type="submit" name="action" value="UpdatePres" class="btn btn-success"><i class="fa fa-refresh"></i> Update</button>
                                    <input type="hidden" name="txtPresID" value="${requestScope.EDITPRES.pres.prescriptionID}"/>
                                    <button type="submit" name="action" value="DeletePres" class="btn btn-danger"><i class="fa fa-times"></i> Delete</button>
                                </td>
                            </tr>
                        </form>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row" style="padding-bottom: 30px;">
            <div class="col-md-4"></div>
            <div class="col-md-4 text-center">
                <form action="PhuongControllerEdit" method="post">
                    <input type="hidden" name="txtDignose" id="txtDignose"/>
                    <input type="hidden" name="MedRCID" value="${sessionScope.MEDID.medRecordID}"/>
                    <input type="hidden" name="txtMedRCID" value="${sessionScope.MEDID.medRecordID}"/>
                    <input type="hidden" name="txtPresID" value="${requestScope.EDITPRES.pres.prescriptionID}"/>
                    <button type="submit" value="Ok" class="btn btn-primary phuongadd" name="action"><i class="fa fa-check"></i> Ok</button>
                </form>
            </div>
            <div class="col-md-4"></div>
        </div>
        <!-- end table here-->
        <!--IMPORTANT!!! EDIT ZONE END FROM HERE - Do not change anything else-->
    </div>
    <div class="col-md-1 col-sm-1"></div>
</div>	
<script type="text/javascript">

    var parsleyOptions = {
    successClass: "has-success",
            errorClass: "has-error",
            classHandler: function(e) {
            return e.$element.closest(".form-group");
            },
            errorsWrapper: '<span class="center-block text-danger" style="padding-left: 10px; padding-top: 5px"></span>',
            errorTemplate: '<p></p>'
    };
    $("#inputForm").parsley(parsleyOptions);
    $(".phuongadd").click(function() {
        var dig = $(".txtDiag").val();
        $("#txtDignose").val(dig);
        if (!$("#txtDignose").val()) {
            alert("Please insert dignose before Insert Prescription!!!");
            return false;
        }
    });


</script>                     
<%@include file="footer.jsp" %>