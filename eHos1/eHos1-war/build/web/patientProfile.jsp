<%@include file="header.jsp" %>
<div class="col-sm-9 col-md-9 text-left">
    <h1 class="page-header">
        Inpatient Registration <small>${sessionScope.empInfo.faculty} - ${sessionScope.empInfo.position}</small>
    </h1>
</div>
</div>
<div class="row-fluid text-left" >
    <div class="col-sm-8 col-md-8">
        <!--Show form-->
        <h3>Info details</h3>
        <div class="table-responsive">
            <table class="showInfo">
                <tr>
                    <th>Patient ID:</th>
                    <td colspan="5">${requestScope.patientInfo.patID}</td>
                </tr>
                <tr>
                    <th>Name:</th>
                    <td>${requestScope.patientInfo.patName}</td>
                    <th>Date of birth:</th>
                    <td>${requestScope.patientInfo.dob}</td>
                    <th>Gender:</th>
                    <td>${requestScope.patientInfo.gender}</td>
                </tr>
                <tr>
                    <th>ID card:</th>
                    <td colspan="5">${requestScope.patientInfo.iDCard}</td>
                </tr>
                <tr>
                    <th>Address:</th>
                    <td colspan="2">${requestScope.patientInfo.address}</td>
                    <th>Phone:</th>
                    <td colspan="2">${requestScope.patientInfo.phone}</td>
                </tr>
                <tr>
                    <th>Created date:</th>
                    <td colspan="2">${requestScope.patientInfo.regDate}</td>
                    <th>Last registry:</th>
                    <td colspan="2">29/12/2014 10:23</td>
                </tr>
            </table>
        </div>
        <!--end show...-->
        <form id="inputForm" style="width: 70%;margin-bottom: 20px;"
              action="RegistrationMainProcess" method="post">
            <h3>Declaration</h3>
            <div class="form-group">
                <label>Symptoms</label>
                <textarea class="form-control" 
                          data-parsley-notblank="true" data-parsley-required="true"
                          data-parsley-maxlength="150"
                          rows="3" name="txtSymptoms"></textarea>
            </div>
            <div class="form-group">
                <label>Faculty to go</label>
                <select class="form-control" name="cbbFacultyToGo">
                    <c:forEach items="${requestScope.listFacultyName}" var="i">
                        <option value="${i}">${i}</option>
                    </c:forEach>
                </select>
            </div>
            <button type="submit" form="inputForm" class="btn btn-default" name="btnSubmit" value="insert">
                Submit <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
            </button>
            <!--Pass patient ID to servlet-->
            <input type="hidden" value="${requestScope.patientInfo.patID}" name="passID" />
        </form>
    </div>
</div>	
<script type="text/javascript">
    var parsleyOptions = {
        successClass: "has-success",
        errorClass: "has-error",
        classHandler: function(e) {
            return e.$element.closest(".form-group");
        },
        errorsWrapper: '<span class="center-block text-danger" style="padding-left: 10px; padding-top: 5px"></span>',
        errorTemplate: '<p></p>'
    };
    $("#inputForm").parsley(parsleyOptions);
</script>
<%@include file="footer.jsp" %>