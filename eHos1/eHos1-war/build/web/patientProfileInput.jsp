<%@include file="header.jsp" %>
<div class="col-sm-9 col-md-9 text-left">
    <h1 class="page-header">
        Inpatient Registration <small>Receptionist</small>
    </h1>
</div>
</div>
<div class="row-fluid text-left" >
    <div class="col-sm-8 col-md-8">
        <form id="inputForm" style="width: 70%;margin-bottom: 20px;" 
              action="RegistrationMainProcess" method="post">
            <h3>Info detail</h3>
            <div class="form-group">
                <label>Name</label>
                <input class="form-control" placeholder="Patient's full name..." 
                       data-parsley-notblank="true" data-parsley-required="true"
                       data-parsley-maxlength="50"
                       name="txtPatientName">
            </div>
            <div class="form-group">
                <label>Date of birth</label>
                <input class="form-control" placeholder="dd/MM/yyyy..."
                       data-parsley-notblank="true" data-parsley-required="true"
                       id="datetimepicker" name="txtDob">
                <script type="text/javascript">
                    $(function() {
                        $('#datetimepicker').datetimepicker({
                            format: 'DD/MM/YYYY',
                            maxDate: moment()
                        });
                    });
                </script>
            </div>
            <div class="form-group">
                <label style="padding-right: 20px">Gender </label>
                <label class="radio-inline">
                    <input type="radio" name="radGender" id="optionsRadiosInline1" value="Male" checked> Male
                </label>
                <label class="radio-inline">
                    <input type="radio" name="radGender" id="optionsRadiosInline2" value="Female"> Female
                </label>
            </div>
            <div class="form-group">
                <label>ID card number</label>
                <input class="form-control" placeholder="Patient ID card number..." 
                       data-parsley-notblank="true" data-parsley-required="true"
                       data-parsley-type="digits"
                       name="txtIDCard">
            </div>
            <div class="form-group">
                <label>Address</label>
                <textarea class="form-control" rows="3" placeholder="Patient current address..."
                          data-parsley-notblank="true" data-parsley-required="true"
                          data-parsley-maxlength="150"
                          name="txtAddress"></textarea>
            </div>
            <div class="form-group">
                <label>Phone</label>
                <input class="form-control" placeholder="Patient phone number..."
                       data-parsley-notblank="true" data-parsley-required="true"
                       data-parsley-type="digits"
                       data-parsley-maxlength="20"
                       name="txtPhone"/>
            </div>
            <h3>Declaration</h3>
            <div class="form-group">
                <label>Symptoms</label>
                <textarea class="form-control" 
                          data-parsley-notblank="true" data-parsley-required="true"
                          data-parsley-maxlength="150"
                          rows="3" name="txtSymptoms"></textarea>
            </div>
            <div class="form-group">
                <label>Faculty to go</label>
                <select class="form-control" name="cbbFacultyToGo">
                    <c:forEach items="${requestScope.listFacultyName}" var="i">
                        <option value="${i}">${i}</option>
                    </c:forEach>
                </select>
            </div>
            <button type="submit" form="inputForm" class="btn btn-default" name="btnSubmit" value="create">
                Submit <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
            </button>
        </form>
    </div>
</div>	
<script type="text/javascript">
    var parsleyOptions = {
        successClass: "has-success",
        errorClass: "has-error",
        classHandler: function(e) {
            return e.$element.closest(".form-group");
        },
        errorsWrapper: '<span class="center-block text-danger" style="padding-left: 10px; padding-top: 5px"></span>',
        errorTemplate: '<p></p>'
    };
    $("#inputForm").parsley(parsleyOptions);
</script>
<%@include file="footer.jsp" %>