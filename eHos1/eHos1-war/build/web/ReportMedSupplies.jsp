<%-- 
    Document   : ReportPrescription
    Created on : Jan 8, 2017, 3:38:24 PM
    Author     : Nguyet Phuong
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Print prescription</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!-- jQuery -->
        <script src="js/jquery.js"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>
        <style>    
            /* Set black background color, white text and some padding */
            footer {
                background-color: #555;
                color: white;
                padding: 8px;
            }
        </style>
    </head>
    <body>
        <header>
            <div class="container-fluid" style="z-index:4; float:left;">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <img src="./images/logo.jpg" class="img-responsive" style="opacity:0.6;width:120px; height:120px"/>
                </div>
            </div>
            <div class="col-sm-8 col-md-8 col-xs-8">
                <h3>eHopital</h3>
                <h4>A01 - Cardiology</h4>
                <h4>Admission ID: MR12192016-1</h4>
                <h4>Test No: TR12192016-10</h4>
            </div>
        </header>
        <div class="row-fluid text-center">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <h1>
                    REPORT MEDICINE SUPPLIES<!--Change here for title of the page-->
                </h1>
                <h4><i>(${requestScope.DATE})</i></h4>
            </div>
        </div>
        <div class="row-fluid text-left">
            <div class="col-sm-2 col-md-2 col-xs-2"></div>
            <div class="col-sm-8 col-md-8 col-xs-8">
                <!--information of patient-->
                <div class="row">
                    <div class="col-md-8 col-sm-8 col-xs-8"><strong>Medical record ID :</strong> ${sessionScope.MEDID.medRecordID}</div>
                    <div class="col-md-4 col-sm-4 col-xs-4"><strong>Patient name:</strong> ${sessionScope.MEDID.patInfo.patName}</div>
                </div>
                <div class="row" style="padding-top: 20px;">
                    <div class="col-md-4 col-sm-4 col-xs-4"><strong>Sex:</strong> ${sessionScope.MEDID.patInfo.gender}</div>
                    <div class="col-md-3 col-sm-3 col-xs-3"><strong>Birthday:</strong> ${sessionScope.MEDID.patInfo.dob}</div>
                </div>
                <div class="row" style="padding-top: 20px;">
                    <div class="col-md-4 col-sm-4 col-xs-4"><strong>Faculty:</strong> ${sessionScope.MEDID.currentFaculty}</div>
                    <div class="col-md-3 col-sm-3 col-xs-3"><strong>Date in:</strong> ${sessionScope.MEDID.dateInHospital}</div>
                    <div class="col-sm-2 col-md-2 col-xs-2"><strong>Bed:</strong> ${sessionScope.MEDID.currentBed}</div>
                </div>
                <div class="row" style="padding-top: 20px; padding-bottom: 10px;">
                    <div class="col-md-12 col-sm-12 col-xs-12"><strong>Delitescence:</strong> ${sessionScope.MEDID.symptoms}</div>
                </div>
                <!--IMPORTANT!!! EDIT ZONE START FROM HERE - Do not change anything else-->
                <!--I am not responsible any wrong if you change anything without permission-->
                <!--FOR THE ENGLISH TRANSLATE - GOOGLE PLZ ^_^-->
                <!--table here-->
                <c:forEach items="${requestScope.EDITSUPP.lstSuppDeta}" var="dto" varStatus="stt">
                    <div class="row" style="padding-top: 20px;">
                    <div class="col-md-9 col-sm-9 col-xs-9"><strong><i>${stt.count} &nbsp;&nbsp; ${dto.medicineID.itemName}, ${dto.medicineID.nation}</i></strong></div>
                    <div class="col-md-3 col-sm-3 col-xs-3">Quantity: ${dto.quantity}</div>
                </div>
                </c:forEach>
                <hr/>
                <div class="row" style="padding-top: 20px;">
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-4"></div>
                        <div class="col-md-4 col-sm-4 col-xs-4"></div>
                        <div class="col-md-4 col-sm-4 col-xs-4 text-center">${requestScope.DATECREATE}</div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-4"></div>
                        <div class="col-md-4 col-sm-4 col-xs-4"></div>
                        <div class="col-md-4 col-sm-4 col-xs-4 text-center"><strong>Doctor</strong></div>
                    </div>
                    <br/><br/><br/><br/>
                    <div class="row" style="padding-bottom: 20px;">
                        <div class="col-md-4 col-sm-4 col-xs-4"></div>
                        <div class="col-md-4 col-sm-4 col-xs-4"></div>
                        <div class="col-md-4 col-sm-4 col-xs-4 text-center"><strong>${requestScope.EDITSUPP.supp.employeeID.fullname}</strong></div>
                    </div>
                </div>
            </div>	
            <footer>
                <div class="container">
                    <p class="text-muted">eHopital@2016</p>
                </div>
            </footer>
            <!--css for footer-->
            <style>
                html {
                    position: relative;
                    min-height: 100%;
                }
                body {
                    /* Margin bottom by footer height */
                    margin-bottom: 20px;
                }
                footer {
                    position: absolute;
                    bottom: 0;
                    width: 100%;
                    /* Set the fixed height of the footer here */
                    height: 20px;
                    background-color: #f5f5f5;
                }
            </style>
            <!-- css for side menu -->
            <style>
                .glyphicon { margin-right:10px; }
                .panel-body { padding:0px; }
                .panel-body table tr td { padding-left: 15px }
                .panel-body .table {margin-bottom: 0px; }
            </style>
            <!-- css header
            ================================================== -->
            <style>
                header {
                    background: url(./images/banner.jpg) no-repeat;
                    background-size: cover;
                    -webkit-background-size: cover;
                    -moz-background-size: cover;
                    -o-background-size: cover;	
                    height:130px;
                }
                .mr10 {
                    margin-right: 10px;
                }
                .mr20 {
                    margin-right: 20px;
                }
                .pd20 {
                    padding-top: 20px;
                }
            </style>
            <!-- css header
            ================================================== -->
    </body>
</html>