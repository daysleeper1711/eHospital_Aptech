<%@include file="header.jsp" %>
<div class="col-sm-9 col-md-9 text-left">
    <h1 class="page-header">
        Treatment Conclusion <small>${sessionScope.empInfo.faculty} - ${sessionScope.empInfo.position}</small>
    </h1>
</div>
</div>
<div class="row-fluid text-left" >
    <div class="col-sm-8 col-md-8">
        <h3>Medical Record info</h3>
        <div class="table-responsive">
            <table class="showInfo">
                <tr>
                    <th>Medical Record ID:</th>
                    <td>${requestScope.medicalRecordInfo.medRecordID}</td>
                    <th>Date in:</th>
                    <td>${requestScope.medicalRecordInfo.dateInHospital}</td>
                    <th>Bed:</th>
                    <td>${requestScope.medicalRecordInfo.bedInfo.bedName}</td>
                    <th>Type: </th>
                    <td>${requestScope.medicalRecordInfo.bedInfo.bedType}</td>
                </tr>
            </table>
        </div>
        <!--Show form-->
        <h3>Info details</h3>
        <div class="table-responsive">
            <table class="showInfo">
                <tr>
                    <th>Patient ID:</th>
                    <td colspan="5">${requestScope.medicalRecordInfo.patInfo.patID}</td>
                </tr>
                <tr>
                    <th>Name:</th>
                    <td>${requestScope.medicalRecordInfo.patInfo.patName}</td>
                    <th>Date of birth:</th>
                    <td>${requestScope.medicalRecordInfo.patInfo.dob}</td>
                    <th>Gender:</th>
                    <td>${requestScope.medicalRecordInfo.patInfo.gender}</td>
                </tr>
            </table>
        </div>
        <!--end show...-->
        <form style="width: 70%;margin-bottom: 20px;" 
              action="ConclusionMainProcess" method="post">
            <h3>Declaration</h3>
            <div class="form-group">
                <label>Main disease (*)</label>
                <input class="form-control" placeholder="Patient's main disease..." 
                       name="txtMainDisease"/>
            </div>
            <div class="form-group">
                <label>Side disease</label>
                <input class="form-control" placeholder="Patient's side disease..."
                       name="txtSideDisease"/>
            </div>
            <div class="form-group">
                <label style="padding-right: 20px">Treatment result (*)</label>
                <label class="radio-inline">
                    <input type="radio" name="radTreatmentResult" id="optionsRadiosInline1" value="Effective" checked> Effective
                </label>
                <label class="radio-inline">
                    <input type="radio" name="radTreatmentResult" id="optionsRadiosInline2" value="Ineffective"> Ineffective
                </label>
            </div>
            <div class="form-group">
                <label style="padding-right: 20px">Health condition (*)</label>
                <label class="radio-inline">
                    <input type="radio" name="radHealthCondition" id="optionsRadiosInline1" value="Better" checked> Better
                </label>
                <label class="radio-inline">
                    <input type="radio" name="radHealthCondition" id="optionsRadiosInline2" value="Unchange"> Unchange
                </label>
                <label class="radio-inline">
                    <input type="radio" name="radHealthCondition" id="optionsRadiosInline2" value="Unchange"> Worst
                </label>
            </div>
            <button type="submit" class="btn btn-default"
                    name="btnSubmit" value="conclusion confirm">
                Submit <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
            </button>
            <input type="hidden" name="passID" value="${requestScope.medicalRecordInfo.medRecordID}" />
        </form>
        
    </div>
</div>	
<%@include file="footer.jsp" %>
