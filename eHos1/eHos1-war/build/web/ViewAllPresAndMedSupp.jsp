<%-- 
    Document   : ViewAllPresAndMedSupp
    Created on : Jan 3, 2017, 2:40:01 PM
    Author     : Nguyet Phuong
--%>
<%@include file="header.jsp" %>
<div class="col-sm-9 col-md-9">
    <h1 class="page-header">
        View all prescription & Medicine Supplies <!--Change here for title of the page-->
    </h1>
</div>
</div>
<div class="row-fluid text-left">
    <div class="col-md-1 col-sm-1"></div>
    <div class="col-sm-6 col-md-6">
        <div class="row">
            <div class="col-md-5"><strong>Medical record :</strong> ${sessionScope.MEDID.medRecordID}</div>
            <div class="col-md-7 text-right">
                <a href="PhuongControllerAdd?action=AddNewPrescription" class="btn btn-default"> <i class="fa fa-plus" style="color: green;"></i> Add prescription</a>
                <a href="PhuongControllerAdd?action=AddNewMedSupp" class="btn btn-default"><i class="fa fa-plus" style="color: green;"></i> Add medicine supplies</a>
            </div>
        </div>
        <div class="row" style="padding-top: 20px;">
            <div class="col-md-6"><strong>Patient name:</strong> ${sessionScope.MEDID.patInfo.patName}</div>
            <div class="col-md-4"><strong>Birthday:</strong> ${sessionScope.MEDID.patInfo.dob}</div>
            <div class="col-md-2"><strong>Sex:</strong> ${sessionScope.MEDID.patInfo.gender}</div>
        </div>
        <div class="row" style="padding-top: 20px;">
            <div class="col-md-3"><strong>Date in:</strong> ${sessionScope.MEDID.dateInHospital}</div>
            <div class="col-md-4"><strong>Faculty:</strong> ${sessionScope.MEDID.currentFaculty}</div>
            <div class="col-md-2"><strong>Bed:</strong> ${sessionScope.MEDID.currentBed}</div>
        </div>
        <div class="row" style="padding-top: 20px;">
            <div class="col-md-12"><strong>Delitescence:</strong> ${sessionScope.MEDID.symptoms}</div>
        </div>
        <c:if test="${requestScope.LSTCREFORDATE ne null}">
            <div class="table-responsive" style="padding-top: 20px;">
                <table class="table table-bordered table-hover table-striped">
                    <thead>
                        <tr>
                            <th>Create for date</th>
                            <th>Prescription</th>
                            <th>Medicine supplies</th>
                            <th>Report Gived Medicine</th>
                        </tr>
                    </thead>
                    <tbody id="itemContainer">
                        <c:forEach items="${requestScope.LSTCREFORDATE}" var="createfordate" varStatus="stt">
                            <form action="ViewListController" method="post">
                                <tr>
                                <input type="hidden" name="txtCreateFDate" value="${createfordate}"/>
                                <td>${createfordate}</td>
                                <td>
                                    <c:forEach items="${requestScope.LSTPRES}" var="pres">
                                        <c:if test="${pres eq createfordate}">
                                            <c:set var="check" value="1"/>
                                        </c:if>
                                    </c:forEach>
                                    <c:if test="${check eq 1}">
                                        <button name="action" value="Detaillistprescription"><i class="fa fa-info-circle" style="color: green;"></i> Detail</button>
                                    </c:if>
                                </td>
                                <td>
                                    <c:forEach items="${requestScope.LSTSUPP}" var="supp">
                                        <c:if test="${supp eq createfordate}">
                                            <c:set var="check" value="2"/>
                                        </c:if>
                                    </c:forEach>
                                    <c:if test="${check eq 2}">
                                        <button name="action" value="Detail lstMedSupp"><i class="fa fa-info-circle" style="color: green;"></i> Detail</button>
                                    </c:if>
                                </td>
                                <td><button name="action" value="ChooseReport"><i class="fa fa-print" style="color: blue;"></i> Print</button></td>
                                </tr>
                            </form>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
            <div class="holder"></div>
        </c:if>
        <c:if test="${requestScope.LSTCREFORDATE eq null}">
            <h3 style="color: red;"><i>Don't have any prescription or medicine supplies</i></h3>
        </c:if>
        <!-- end table here-->
        <!--IMPORTANT!!! EDIT ZONE END FROM HERE - Do not change anything else-->
    </div>
    <div class="col-md-1 col-sm-1"></div>
</div>	
<%@include file="footer.jsp" %>