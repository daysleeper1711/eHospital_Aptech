<%-- 
    Document   : locheader
    Created on : Jan 20, 2017, 4:19:59 PM
    Author     : locth-laptop
--%>

<!-- css header
        ================================================== -->
<style>
    header {
        background: url(./locGUI/banner.jpg) no-repeat;
        background-size: cover;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;	
        height:130px;
    }
    .mr10 {
        margin-right: 10px;
    }
    .mr20 {
        margin-right: 20px;
    }
    .pd20 {
        padding-top: 20px;
    }
</style>
<!-- css header
================================================== -->

<header>
    <div class="container-fluid" style="z-index:4">
        <div class="col-sm-3 col-md-3">
            <img src="./locGUI/logo.jpg" class="img-responsive" style="opacity:0.6;width:100px"/>
        </div>
        <div class="col-sm-9 col-md-9 text-right lead">
            <i class="fa fa-user-circle" aria-hidden="true"></i>
            Welcome, <strong>${sessionScope.empInfo.empName}</strong>
            
        </div>
    </div>
</header>