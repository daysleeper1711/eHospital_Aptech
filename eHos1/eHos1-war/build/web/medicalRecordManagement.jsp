<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@include file="header.jsp" %>
<div class="col-sm-9 col-md-9 text-left">
    <h1 class="page-header">
        Medical Record Management <small>search page</small>
    </h1>
</div>
</div>
<div class="row-fluid text-left">
    <div class="col-sm-9 col-md-9">
        <form style="margin: 20px" class="form-inline" 
              id="searchForm" action="MR_MainProcess" method="post"
              novalidate="novalidate">
            <!--SEARCH FORM-->
            <p><strong>Search type</strong></p>
            <div class="form-group">
                <select class="form-control" name="cbbSearchType">
                    <option selected value="sMedRecordID">Medical Record ID</option>
                </select> 
            </div>
            <div class="form-group">
                <input type="text" class="form-control"
                       placeholder="Your text here" name="txtSearch"
                       data-parsley-notblank="true" data-parsley-required="true">
            </div>
            <div style="margin-bottom: 20px"></div>
            <button class="btn btn-default" type="submit" name="btnSubmit" value="search">
                Search <i class="fa fa-search"></i>
            </button>
            <!--end form search...-->
        </form>
        <!--DISPLAY NOTIFICATION-->
        <c:if test="${requestScope.searchFails ne null}">
            <div class="alert alert-warning">
                <strong>Can not found data!!!</strong>
            </div>
        </c:if>
    </div>
</div>
<script type="text/javascript">
    var parsleyOptions = {
        successClass: "has-success",
        errorClass: "has-error",
        classHandler: function(e) {
            return e.$element.closest(".form-group");
        },
        errorsWrapper: '<span class="pull-right text-danger" style="padding-left: 10px; padding-top: 5px"></span>',
        errorTemplate: '<p></p>'
    };
    $("#searchForm").parsley(parsleyOptions);
</script>
<%@include file="footer.jsp" %>