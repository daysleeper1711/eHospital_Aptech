<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="loc" %>
<!DOCTYPE html>

<html >
    <head>
        <title>Supplier Management</title>
        <meta charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- jQuery -->
        <script language="javascript" src="js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>
        <style>    
            /* Set black background color, white text and some padding */
            footer {
                background-color: #555;
                color: white;
                padding: 8px;
            }




        </style>

        <!--pagination-->
        <script type="text/javascript" src="js/jPager/js.js"></script>
        <script type="text/javascript" src="js/jPager/jPages.min.js"></script>
        <link rel="stylesheet" href="css/jPager/jPages.css" type="text/css"/>

        <script>
//            run when document is ready
            $(function() {
//                initiate plugin
                $("div.holder").jPages({
                    containerID: "itemContainer",
                    perPage: 5
                });
            });
        </script>

    </head>
    <body>
        <%@include file="locGUI/locheader.jsp" %>

        <div class="row-fluid text-center">
            <%@include file="locGUI/locmenu.jsp" %>
            <div class="col-sm-9 col-md-9">
                <h1 class="page-header">
                    <small>Supplier Management</small> <!--Change here for title of the page-->
                </h1>
            </div>

        </div>
        <div class="row-fluid text-left">
            <div class="col-md-1 col-sm-1"></div>
            <div class="col-sm-6 col-md-6">
                <form style="padding:20px" action="SearchSupplierServlet" method="post">
                    <!--IMPORTANT!!! EDIT ZONE START FROM HERE - Do not change anything else-->
                    <!--I am not responsible any wrong if you change anything without permission-->
                    <!--FOR THE ENGLISH TRANSLATE - GOOGLE PLZ ^_^-->

                    <!--copy form input here...-->
                    <!--input 1-->

                    <div class="form-group input-group">
                        <input type="text" class="form-control" name="txtSearch" id="txtSearch"/>
                        <span class="input-group-btn"><button class="btn btn-default" name="action" id="btnSearch" type="submit"><i class="fa fa-search"></i></button></span>
                    </div>
                    <p style="color: red" id="error"></p>
                    <div class="form-group">
                        <label>Status</label>
                        <select class="form-control" id="select" name="select">
                            <option>All</option>
                            <option>Action</option>
                            <option>Inactive</option>
                        </select>

                    </div>


                    <div class="form-group" id="changeradio">
                        <label>Search ... </label>
                        <label class="radio-inline">
                            <input type="radio" name="optionsRadiosInline" id="radio" value="SupplierID" checked> Supplier ID
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="optionsRadiosInline" id="radio" value="SupplierName"> Supplier Name
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="optionsRadiosInline" id="radio" value="Status"> Status
                        </label>
                    </div>

                </form>

                <script language="javascript">
                    $(document).ready(function() {
                        //Begin Check chan disabled luc dau onLoad 
                        var a = $('input:checked').val();
                        if (a === "SupplierID" || a === "SupplierName") {
                            $('#select').prop("disabled", true);
                            $('#txtSearch').prop("disabled", false);

                        }
                        else {
                            $('#select').prop("disabled", false);
                            $('#txtSearch').prop("disabled", true);
                        }
                        //Chan disabled click chon radio
                        $('#changeradio').change(function() {
                            var a = $('input:checked').val();


                            if (a === "SupplierID" || a === "SupplierName") {
                                $('#select').prop("disabled", true);
                                $('#txtSearch').prop("disabled", false);
                            }
                            else {
                                $('#select').prop("disabled", false);
                                $('#txtSearch').prop("disabled", true);
                            }
                        });
                        //Chan disabled click chon radio

                        //Begin Check chan nut Search Not Blank
                        $('#btnSearch').click(function()
                        {
                            var checkblank = $('#txtSearch').val().trim();
                            var a = $('input:checked').val();
                            if (a === "SupplierID" || a === "SupplierName") {
                                if (checkblank === "") {
                                    $('#error').text(a + " not blank");
                                    return false;
                                }
                            }
                        });
                        //End Check chan nut Search Not Blank


                    });
                </script>
                <a href="Add Supplier.jsp" >Add New Supplier</a>
                <loc:if test="${not empty requestScope.list}">
                    <form action="UpdateSupplierServlet" method="post">
                        <h2>List Result </h2>
                        <div class="table-responsive" >
                            <table class="table table-bordered table-hover table-striped" >
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>SupplierID</th>
                                        <th>SupplierName</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody id="itemContainer">
                                    <loc:forEach items="${requestScope.list}"  var="dto" varStatus="stt">
                                        <tr>
                                            <td>${stt.count}</td>
                                            <td>${dto.supplierID}</td>
                                            <td>${dto.supplierName}</td>
                                            <td>${dto.UStatus}</td>
                                            <td>
                                                <form action="UpdateSupplierServlet" method="post">
                                                    <input type="submit" value="Edit" name="action"/>
                                                    <input type="hidden" name="txtID" value="${dto.supplierID}"/>
                                                </form>
                                            </td>
                                        </tr>

                                    </loc:forEach>
                                </tbody>
                            </table>
                        </div>
                        <!--pagination-->
                        <div class="holder" align="right"></div>
                    </form>

                </loc:if>
            </div>
            <div class="col-md-1 col-sm-1"></div>
        </div>	

        <%@include file="locGUI/locfooter.jsp" %>
    </body>
</html>
