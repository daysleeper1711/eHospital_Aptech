<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>F2-1504-Template</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <!--Datetime picker css-->
        <link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet">
        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!--JPager-->
        <link href="css/jPages/jPages.css" rel="stylesheet" type="text/css">
        <!-- select 2-->
        <link href="css/select2.min.css" rel="stylesheet" type="text/css">
        <!-- jQuery -->
        <script src="js/jquery.js"></script>
        <!--parsley jquery validation-->
        <script src="js/parsley.min.js"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>
        <!--Bootstrap datetime picker-->
        <script src="js/moment.js"></script>
        <script src="js/bootstrap-datetimepicker.min.js"></script>
        <!--JPager-->
        <script src="js/jPages/js.js"></script>
        <script src="js/jPages/jPages.min.js"></script>
        <!-- select 2-->
        <script src="js/select2.min.js"></script>
        <script>
            /* when document is ready */
            $(function() {
                /* initiate plugin */
                $("div.holder").jPages({
                    containerID: "itemContainer",
                    perPage: 3
                });
            });
        </script>
        <script type="text/javascript">
            $(document).ready(function() {
                $(".js-example-basic-single").select2();
            });
        </script>
        <style>
            html {
                position: relative;
                min-height: 100%;
            }
            body {
                /* Margin bottom by footer height */
                margin-bottom: 10px;
            }
            footer {
                position: absolute;
                bottom: 0;
                width: 100%;
                /* Set the fixed height of the footer here */
                height: 10px;
                background-color: #f5f5f5;
            }
            /*Side menu css*/
            .glyphicon { margin-right:10px; }
            .panel-body { padding:0px; }
            .panel-body table tr td { padding-left: 15px }
            .panel-body .table {margin-bottom: 0px; }
            /*Header css*/
            header {
                background: url(images/banner.jpg) no-repeat;
                background-size: cover;
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;	
                height:130px;
            }
            /*Borderless*/
            .showInfo td {
                padding-top: 10px;
                padding-left: 20px;
                padding-right: 30px;
                padding-bottom: 10px;
            }
        </style>
    </head>
    <body>
        <!--Header logo and user-->
        <header>
            <div class="container-fluid" style="z-index:4">
                <div class="col-sm-3 col-md-3">
                    <img src="images/logo.jpg" class="img-responsive" style="opacity:0.6;width:100px"/>
                </div>
                <div class="col-sm-9 col-md-9 text-right lead">
                    <i class="fa fa-user-circle" aria-hidden="true"></i>
                    Welcome, <strong>${sessionScope.empInfo.empName}</strong> <a href="LogoutProcess">logout</a>
                </div>
            </div>
        </header>
        <div class="row-fluid text-center">
            <div class="col-sm-3 col-md-3" style="margin-top: 40px">
                <div class="panel-group" id="accordion">
                    <!--Bed management-->
                    <c:if test="${sessionScope.empInfo.position eq 'admin'}">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse " data-parent="#accordion" href="#collapseSeven">Administration</a>
                                </h4>
                            </div>
                            <div id="collapseSeven" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <table class="table">
                                        <tr>
                                            <td>
                                                <a href="#">Update patient profile</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="#">Bed Management</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="#">Update Medical Record</a>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </c:if>
                    <!--Patient Registration-->
                    <c:if test="${sessionScope.empInfo.position eq 'receptionist'}">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                        Patient Registration <!--Change here for the outside menu content-->
                                    </a> 
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <table class="table">
                                        <tr>
                                            <td>
                                                <a href="inpatientRegistration.jsp">Inpatient</a> <!--Change here for the sub menu content-->
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="">Health-care package registry</a>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </c:if>
                    <c:if test="${sessionScope.empInfo.position eq 'doctor' or sessionScope.empInfo.position eq 'nurse'}">
                        <!--Inpatient management-->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">Inpatient Management</a>
                                </h4>
                            </div>
                            <div id="collapseFive" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <table class="table">
                                        <c:if test="${sessionScope.empInfo.position eq 'doctor'}">
                                        <tr>
                                            <td>
                                                <a href="DiagnosisMainProcess?open=yes">Diagnosis</a>
                                            </td>
                                        </tr>
                                        </c:if>
                                        <c:if test="${sessionScope.empInfo.position eq 'nurse'}">
                                            <tr>
                                                <td>
                                                    <a href="BedAssignmentMainProcess?open=yes">Bed Assignment</a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <a href="DischargeFeeMainProcess?open=yes">Inpatient total fee</a>
                                                </td>
                                            </tr>
                                        </c:if>
                                        <c:if test="${sessionScope.empInfo.position eq 'doctor'}">
                                        <tr>
                                            <td>
                                                <a href="DailyTreatmentMainProcess?open=yes">Daily Treatment Management</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="ConclusionMainProcess?open=yes">Request to Discharge</a>
                                            </td>
                                        </tr>
                                        </c:if>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </c:if>
                    <c:if test="${sessionScope.empInfo.position eq 'cashier'}">
                        <!--Inpatient fee management-->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix">Inpatient Fee Management</a>
                                </h4>
                            </div>
                            <div id="collapseSix" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <table class="table">
                                        <tr>
                                            <td>
                                                <a href="depositConfirmation.jsp">Deposit payment</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="#">Discharge fee payment</a>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </c:if>
                    <!--Medical record management-->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">Medical Record Management</a>
                            </h4>
                        </div>
                        <div id="collapseFour" class="panel-collapse collapse">
                            <div class="panel-body">
                                <table class="table">
                                    <tr>
                                        <td>
                                            <a href="MR_MainProcess?open=yes">Search Medical Record</a>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <c:if test="${sessionScope.empInfo.position eq 'doctor' or sessionScope.empInfo.position eq 'admin'}">
                        <!--Prescription management -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapsePresDoc">Prescription Management</a>
                                </h4>
                            </div>
                            <div id="collapsePresDoc" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <table class="table">
                                        <c:if test="${sessionScope.empInfo.position eq 'doctor'}">
                                            <tr>
                                                <td>
                                                    <a href="BannerController?action=PresStatusBookMed">Prescription Status Booked Medicine</a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <a href="BannerController?action=ListNewPres">List New Prescription</a>
                                                </td>
                                            </tr>
                                        </c:if>
                                        <c:if test="${sessionScope.empInfo.position eq 'admin'}">
                                            <tr>
                                                <td>
                                                    <a href="PhuongAdminBanner?action=ManagementPres">Management Prescription</a>
                                                </td>
                                            </tr>
                                        </c:if>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!--Medicine supplies management -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseMedSupDoc">Medicine Supplies Management</a>
                                </h4>
                            </div>
                            <div id="collapseMedSupDoc" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <table class="table">
                                        <c:if test="${sessionScope.empInfo.position eq 'doctor'}">
                                            <tr>
                                                <td>
                                                    <a href="BannerController?action=SuppStatusBookMed">Medicine Supplies is Booked Medicine</a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <a href="BannerController?action=ListNewSupp">List New Medicine Supplies</a>
                                                </td>
                                            </tr>
                                        </c:if>
                                        <c:if test="${sessionScope.empInfo.position eq 'admin'}">
                                            <tr>
                                                <td>
                                                    <a href="PhuongAdminBanner?action=MangementSupp">Management Medicine Supplies</a>
                                                </td>
                                            </tr>
                                        </c:if>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </c:if>
                </div>
            </div>