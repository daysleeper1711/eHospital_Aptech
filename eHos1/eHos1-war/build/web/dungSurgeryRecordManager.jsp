<%-- 
    Document   : dungSurgeryRecordManager
    Created on : Feb 2, 2017, 8:12:42 PM
    Author     : Sony
--%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Surgery Record Page</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap Core CSS -->
        <link rel="stylesheet" href="css/w3.css">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/prettify-1.0.css" rel="stylesheet">
        <link href="css/base.css" rel="stylesheet">
        <link href="css/bootstrap-datetimepicker.css" rel="stylesheet">
        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">


        <!-- jQuery -->
        <script src="js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/analytics.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/moment-with-locales.js"></script>
        <script src="js/bootstrap-datetimepicker.js"></script>
        
        <!--pagination-->
        <script type="text/javascript" src="js/jPager/js.js"></script>
        <script type="text/javascript" src="js/jPager/jPages.min.js"></script>
        <link rel="stylesheet" href="css/jPager/jPages.css" type="text/css"/>

        <script>
//            run when document is ready
            $(function() {
//                initiate plugin
                $("div.holder").jPages({
                    containerID: "itemContainer",
                    perPage: 5
                });
            });
        </script>
        <style>
            /* Set black background color, white text and some padding */
            footer {
                background-color: #555;
                color: white;
                padding: 8px;
            }
            html {
                position: relative;
                min-height: 100%;
            }
            body {
                /* Margin bottom by footer height */
                margin-bottom: 20px;
            }
            footer {
                position: absolute;
                text-align: center;
                bottom: 0;
                width: 100%;
                /* Set the fixed height of the footer here */
                height: 20px;
                background-color: #f5f5f5;
            }
            .glyphicon { margin-right:10px; }
            .panel-body { padding:0px; }
            .panel-body table tr td { padding-left: 15px }
            .panel-body .table {margin-bottom: 0px; }

            header {
                background: url(images/banner.jpg) no-repeat;
                background-size: cover;
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;	
                height:170px;
            }
            .mr10 {
                margin-right: 10px;
            }
            .mr20 {
                margin-right: 20px;
            }
            .pd20 {
                padding-top: 20px;
            }
        </style>
    </head>

    <body>
        <%@include file="dungHeaderAdmin.jsp" %>
        <%@include file="dungAsideRightDoctor.jsp" %>
        <div class="row-fluid text-left">
            <div class="col-md-1 col-sm-1"></div>
            <div class="col-sm-9 col-md-9">
                <c:if test="${requestScope.msgError ne null}">
                    <label style="margin-left: 10px" class="w3-text-red">Note ! ${requestScope.msgError}</label><br/>
                </c:if>
                <label style="margin-left: 10px;">Suggest No : ${requestScope.dtoSuggest.suggestSurgeryID}</label><br/>
                <label style="margin-left: 10px;">Surgery Service Name : ${requestScope.dtoSuggest.surgeryServiceID.surgeryServiceName}</label><br/>
                <label style="margin-left: 10px;">Medical Record : ${sessionScope.sInfo.medRecordID}</label><br/>
                <label style="margin-left: 10px;">Patient Name : ${sessionScope.sInfo.patInfo.patName}</label><br/>
                <label style="margin-left: 10px;">Sex : ${sessionScope.sInfo.patInfo.gender}</label><br/>
                <label style="margin-left: 10px;">Faculty Name : ${sessionScope.sInfo.currentFaculty}</label><br/>
                <label style="margin-left: 10px;">Bed Name : ${sessionScope.sInfo.bedInfo.bedName}</label><br/>

                <c:if test="${empty requestScope.listSurgeryRecord}">
                    <a style="margin-left: 10px; margin-bottom: 15px;" href="dungDrugsDetailCartServlet?txtSuggestID=${requestScope.dtoSuggest.suggestSurgeryID}">Choose Drugs Detail <c:if test="${sessionScope.cartSurgery ne null}">(${sessionScope.cartSurgery.listtbDrugs.size()})</c:if></a><br/>
                    <a style="margin-left: 10px; margin-bottom: 15px;" href="dungSuppliesDetailCartServlet?txtSuggestID=${requestScope.dtoSuggest.suggestSurgeryID}">Choose Supplies Detail <c:if test="${sessionScope.cartSurgery ne null}">(${sessionScope.cartSurgery.listtbSupplies.size()})</c:if></a><br/>
                    <a style="margin-left: 10px; margin-bottom: 15px;" href="dungOperatorDoctorCartServlet?txtSuggestID=${requestScope.dtoSuggest.suggestSurgeryID}">choose Operator Doctor Detail <c:if test="${sessionScope.cartSurgery ne null}">(${sessionScope.cartSurgery.listtbOperator.size()})</c:if></a><br/>
                        <!--IMPORTANT!!! EDIT ZONE START FROM HERE - Do not change anything else-->
                        <!--I am not responsible any wrong if you change anything without permission-->
                        <!--FOR THE ENGLISH TRANSLATE - GOOGLE PLZ ^_^-->
                        <!--table here-->
                        <form style="padding:10px" action="InsertSurgeryRecordCartServlet" method="post">
                            <div class="form-group">
                                <input class="form-control" name="txtSuggestID" type="hidden" value="${requestScope.dtoSuggest.suggestSurgeryID}">
                        </div>
                        <div class="form-group">
                            <label>Process of Surgery</label>
                            <textarea class="form-control" rows="2" Name="txtProcessOfSurgery"></textarea>
                        </div>
                        <div class="form-group">
                            <label>Time Of Start</label>
                            <div class="input-group date" id="datetimepicker1">
                                <input type="text" class="form-control" name="txtTimeOfStart" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                        <script type="text/javascript">
                            $(function() {
                                $('#datetimepicker1').datetimepicker({
                                    sideBySide: true,
                                    format: 'MM/DD/YYYY HH:ss'
                                });
                            });
                        </script>
                        <div class="form-group">
                            <label>Time Of End</label>
                            <div class="input-group date" id="datetimepicker2">
                                <input type="text" class="form-control" name="txtTimeOfEnd" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                        <script type="text/javascript">
                            $(function() {
                                $('#datetimepicker2').datetimepicker({
                                    sideBySide: true,
                                    format: 'MM/DD/YYYY HH:ss'
                                });
                            });
                        </script>
                        <div class="form-group">
                            <label>Result of Surgery</label>
                            <textarea class="form-control" rows="2" Name="txtResultSurgery"></textarea>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="cbStatus">Success
                            </label>
                        </div>
                        <div style="text-align: center">
                            <button style="margin: 10px" type="submit" class="btn btn-primary" name="action" value="Insert">Insert</button>
                            <button style="margin: 10px" type="submit" class="btn btn-primary" name="action" value="Cancel">Cancel</button>
                        </div>
                    </form>
                </c:if>
                <c:if test="${not empty requestScope.listSurgeryRecord}">      
                    <div style="margin-top: 20px;margin-left: 10px" class="table-responsive">
                        <table class="table table-bordered table-hover table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Process Of Surgery</th>
                                    <th>Time Of Start</th>
                                    <th>Time Of End</th>
                                    <th>Pay Cost ($)</th>
                                    <th>Status Surgery</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody id="itemContainer">
                                <c:forEach items="${requestScope.listSurgeryRecord}" var="dto" varStatus="stt">
                                    <tr>
                                        <td>${stt.count}</td>
                                        <td>
                                            <a href="dungEditSurgeryRecordServlet?txtSurgeryRecordID=${dto.surgeryRecordID}&txtSuggestID=${requestScope.dtoSuggest.suggestSurgeryID}">${dto.processOfSurgery}</a>
                                        </td>
                                        <td>
                                            <fmt:formatDate value="${dto.timeOfStart}" pattern="MM/dd/yyyy HH:mm"/>
                                        </td>
                                        <td>
                                            <fmt:formatDate value="${dto.timeOfEnd}" pattern="MM/dd/yyyy HH:mm"/>
                                        </td>
                                        <td>${dto.payCost}</td>
                                        <td>
                                            <c:if test="${dto.statusSurgery eq 'true'}">
                                                Success
                                            </c:if>
                                            <c:if test="${dto.statusSurgery eq 'false'}">
                                                Fail
                                            </c:if>
                                        </td>
                                        <td>
                                            <a href="dungReportSurgeryInfoServlet?txtSurgeryRecordID=${dto.surgeryRecordID}&txtSuggestID=${requestScope.dtoSuggest.suggestSurgeryID}">Print Surgery</a>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                    <!--pagination-->
                    <div class="holder" align="right"></div>
                </c:if>
                <!-- end table here-->
                <!--IMPORTANT!!! EDIT ZONE END FROM HERE - Do not change anything else-->
            </div>
            <div class="col-md-1 col-sm-1"></div>
        </div>	

        <%@include file="dungFooter.jsp" %>
        <!--css for footer-->

    </body>
</html>
<!-- css for side menu -->
