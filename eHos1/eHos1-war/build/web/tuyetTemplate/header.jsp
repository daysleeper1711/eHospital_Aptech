<%-- 
    Document   : header
    Created on : Jan 2, 2017, 9:16:25 PM
    Author     : Hoang Tuyet
--%>
<style>
	header {
		background: url(./tuyetTemplate/banner.jpg) no-repeat;
		background-size: cover;
		-webkit-background-size: cover;
		-moz-background-size: cover;
		-o-background-size: cover;	
		height:180px;
	}
	.mr10 {
		margin-right: 10px;
	}
	.mr20 {
		margin-right: 20px;
	}
	.pd20 {
		padding-top: 20px;
	}
</style>																							
<header>
	<div class="container-fluid" style="z-index:4">
		<div class="col-sm-3 col-md-3">
			<img src="./tuyetTemplate/logo.JPG" class="img-responsive" style="opacity:0.6;width:150px"/>
		</div>
		<div class="col-sm-9 col-md-9 text-right lead">
                    
				<i class="fa fa-user-circle" aria-hidden="true"></i>
                                Welcome, <strong>${sessionScope.empInfo.employeeID}</strong>
		</div>
	</div>
</header>
