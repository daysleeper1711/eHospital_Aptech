<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>F2-1504-Template</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!-- jQuery -->
        <script src="js/jquery.js"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>
        <style>
            html {
                position: relative;
                min-height: 100%;
            }
            body {
                /* Margin bottom by footer height */
                margin-bottom: 10px;
            }
            footer {
                position: absolute;
                bottom: 0;
                width: 100%;
                /* Set the fixed height of the footer here */
                height: 10px;
                background-color: #f5f5f5;
            }
            header {
                background: url(images/banner.jpg) no-repeat;
                background-size: cover;
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;	
                height:130px;
            }
            /*Borderless*/
            .showInfo td {
                padding-top: 10px;
                padding-left: 20px;
                padding-right: 30px;
                padding-bottom: 10px;
            }
        </style>
    </head>
    <body>
        <!--Header logo and user-->
        <div class="row-fluid text-left">
            <div class="col-sm-3 col-md-3"></div>
            <div class="col-sm-8 col-md-8">
                    <div class="col-sm-3 col-md-3">
                        <img src="images/logo.jpg" class="img-responsive" style="opacity:0.6;width:100px"/>
                    </div>
                    <div class="col-sm-5 col-md-5">
                        <h1 class="page-header">
                            <p>
                                Discharge fee details 
                            </p>
                        </h1>
                        <h4>
                            Medical Record ID: ${requestScope.medInfo.medRecordID}
                        </h4>
                    </div>
                <div class="table-responsive col-sm-9 col-md-9">
                    <h2>Patient Info details</h2>
                    <table class="showInfo">
                        <tr>
                            <th>Patient ID:</th>
                            <td colspan="5">${requestScope.medInfo.patInfo.patID}</td>
                        </tr>
                        <tr>
                            <th>Name:</th>
                            <td>${requestScope.medInfo.patInfo.patName}</td>
                            <th>Date of birth:</th>
                            <td>${requestScope.medInfo.patInfo.dob}</td>
                            <th>Gender:</th>
                            <td>${requestScope.medInfo.patInfo.gender}</td>
                        </tr>
                        <tr>
                            <th>Address:</th>
                            <td colspan="5">${requestScope.medInfo.patInfo.address}</td>
                        </tr>
                        <tr>
                            <th>ID card: </th>
                            <td>${requestScope.medInfo.patInfo.iDCard}</td>
                        </tr>
                        <tr>
                            <th>Conclusion: </th>
                            <td colspan="5">${requestScope.medInfo.mainDisease}</td>
                        </tr>
                    </table>
                </div>
                <!--Display list patient's waiting-->
                <div class="table-responsive col-sm-10 col-md-10">
                    <h2>List Fee details</h2>
                    <table class="table table-bordered">
                        <!--BED FEE DISPLAY-->
                        <tr>
                            <th colspan="8">
                        <h3>Bed's fee</h3>
                        </th>
                        </tr>
                        <tr>
                            <th class="text-center">No</th>
                            <th colspan="4">Name</th>
                            <th class="text-center">Quantity</th>
                            <th class="text-right">Price</th>
                            <th class="text-right">Total</th>
                        </tr>
                        <c:forEach items="${requestScope.tFee.bedFee.lstBedService}"
                                   var="bs" varStatus="i">
                            <tr>
                                <td class="text-center">${i.count}</td>
                                <td colspan="4">${bs.name}</td>
                                <td class="text-center">${bs.quantity}</td>
                                <td class="text-right">${bs.price}</td>
                                <td class="text-right">${bs.total}</td>
                            </tr>
                        </c:forEach>
                        <tr>
                            <td colspan="6"></td>
                            <th class="text-right">Total Bed fee: </th>
                            <td class="text-right">${requestScope.tFee.bedFee.totalPayment}</td>
                        </tr>
                        <!--ALL TEST FEE DISPLAY-->
                        <c:if test="${requestScope.noSubclinicalTest ne null}">
                            <div class="alert alert-info">
                                <strong>No test request</strong>
                            </div>
                        </c:if>
                        <c:if test="${requestScope.noSubclinicalTest eq null}">
                            <tr>
                                <th colspan="8">
                            <h3>Subclinical test service fee</h3>
                            </th>
                            </tr>
                            <!--Get details of test fee-->
                            <tr>
                                <th class="text-center">No</th>
                                <th colspan="4">Name</th>
                                <th class="text-center">Quantity</th>
                                <th class="text-right">Price</th>
                                <th class="text-right">Total</th>
                            </tr>
                            <c:forEach items="${requestScope.tFee.testFee.lstTestService}"
                                       var="ts" varStatus="i">
                                <tr>
                                    <td class="text-center">${i.count}</td>
                                    <td colspan="4">${ts.name}</td>
                                    <td class="text-center">${ts.quantity}</td>
                                    <td class="text-right">${ts.price}</td>
                                    <td class="text-right">${ts.total}</td>
                                </tr>
                            </c:forEach>
                            <tr>
                                <td colspan="6"></td>
                                <th class="text-right">Total test fee: </th>
                                <td class="text-right">${requestScope.tFee.testFee.totalPayment}</td>
                            </tr>
                        </c:if>
                        <!--ALL DAILY DRUGS FEE-->
                        <!--if daily treatment has no drugs and no supplies use-->
                        <c:if test="${requestScope.noDailyDrugs eq null}">
                            <tr>
                                <th colspan="8">
                            <h3>Daily Drugs and Drugs' supplies using</h3>
                            </th>
                            </tr>
                        </c:if>
                        <!--if daily treatment has no drugs use-->
                        <c:if test="${requestScope.noDailyDrugs ne null and requestScope.noDailySuppliesDrugs ne null}">
                            <div class="alert alert-info">
                                <strong>No daily drugs using</strong>
                            </div>
                        </c:if>
                        <!--display daily drugs use and fee-->
                        <c:if test="${requestScope.noDailyDrugs eq null}">
                            <!--Get details of daily drugs use fee-->
                            <tr>
                                <th colspan="8">
                            <h4>Daily Drugs' supplies use</h4>
                            </th>
                            </tr>
                            <tr>
                                <th class="text-center">No</th>
                                <th colspan="4">Name</th>
                                <th class="text-center">Quantity</th>
                                <th class="text-right">Price</th>
                                <th class="text-right">Total</th>
                            </tr>
                            <c:forEach items="${requestScope.tFee.dailyDrugFee.lstDrugService}"
                                       var="ts" varStatus="i">
                                <tr>
                                    <td class="text-center">${i.count}</td>
                                    <td colspan="4">${ts.name}</td>
                                    <td class="text-center">${ts.quantity}</td>
                                    <td class="text-right">${ts.price}</td>
                                    <td class="text-right">${ts.total}</td>
                                </tr>
                            </c:forEach>
                            <tr>
                                <td colspan="6"></td>
                                <th class="text-right">Total daily drugs fee: </th>
                                <td class="text-right">${requestScope.tFee.dailyDrugFee.totalPaymentForDrugs}</td>
                            </tr>
                        </c:if>
                        <!--if daily treatment has no drugs supplies use-->
                        <c:if test="${requestScope.noDailySuppliesDrugs ne null}">
                            <div class="alert alert-info">
                                <strong>No daily drugs supplies using</strong>
                            </div>
                        </c:if>
                        <!--display daily drugs supplies use and fee-->
                        <c:if test="${requestScope.noDailySuppliesDrugs eq null}">
                            <!--Get details of daily drugs supplies fee-->
                            <tr>
                                <th colspan="8">
                            <h4>Daily Drugs use</h4>
                            </th>
                            </tr>
                            <tr>
                                <th class="text-center">No</th>
                                <th colspan="4">Name</th>
                                <th class="text-center">Quantity</th>
                                <th class="text-right">Price</th>
                                <th class="text-right">Total</th>
                            </tr>
                            <c:forEach items="${requestScope.tFee.dailyDrugFee.lstDrugSuppliesService}"
                                       var="ts" varStatus="i">
                                <tr>
                                    <td class="text-center">${i.count}</td>
                                    <td colspan="4">${ts.name}</td>
                                    <td class="text-center">${ts.quantity}</td>
                                    <td class="text-right">${ts.price}</td>
                                    <td class="text-right">${ts.total}</td>
                                </tr>
                            </c:forEach>
                            <tr>
                                <td colspan="6"></td>
                                <th class="text-right">Total daily drugs' supplies fee: </th>
                                <td class="text-right">${requestScope.tFee.dailyDrugFee.totalPaymentForDrugsSupplies}</td>
                            </tr>
                        </c:if>
                        <c:if test="${requestScope.noDailyDrugs eq null or requestScope.noDailySuppliesDrugs eq null}">
                            <tr>
                                <th colspan="8">
                            <h4>Summary</h4>
                            </th>
                            </tr>
                            <tr>
                                <td colspan="6"></td>
                                <th class="text-right">Total daily drugs and drugs' supplies fee: </th>
                                <td class="text-right">${requestScope.tFee.dailyDrugFee.totalPayment}</td>
                            </tr>
                        </c:if>
                        <!--SURGERY FEE DISPLAY-->
                        <c:if test="${requestScope.noSurgery ne null}">
                            <div class="alert alert-info">
                                <strong>No surgery request</strong>
                            </div>
                        </c:if>
                        <c:if test="${requestScope.noSurgery eq null}">
                            <tr>
                                <th colspan="8">
                            <h3>Surgery fee</h3>
                            </th>
                            </tr>
                            <!--Get details of test fee-->
                            <tr>
                                <th>No</th>
                                <th colspan="4">Name</th>
                                <th>Quantity</th>
                                <th class="text-right">Price</th>
                                <th class="text-right">Total</th>
                            </tr>
                            <c:forEach items="${requestScope.tFee.surgeryFee.lstSurgeryService}"
                                       var="ts" varStatus="i">
                                <tr>
                                    <td>${i.count}</td>
                                    <td colspan="4">${ts.name}</td>
                                    <td>${ts.quantity}</td>
                                    <td class="text-right">${ts.price}</td>
                                    <td class="text-right">${ts.total}</td>
                                </tr>
                            </c:forEach>
                            <tr>
                                <td colspan="6"></td>
                                <th class="text-right">Total surgery fee: </th>
                                <td class="text-right">${requestScope.tFee.surgeryFee.totalPayment}</td>
                            </tr>
                        </c:if>
                        <!--SUMMARY-->
                        <tr>
                            <th colspan="8">
                        <h3>Summary treatment's fee</h3>
                        </th>
                        <tr>
                            <td colspan="6"></td>
                            <th class="text-right">Total fee: </th>
                            <td class="text-right">${requestScope.tFee.totalFee}</td>
                        </tr>
                        <tr>
                            <td colspan="6"></td>
                            <th class="text-right">Total deposit: </th>
                            <td class="text-right">${requestScope.tFee.totalDeposit}</td>
                        </tr>
                        <tr>
                            <td colspan="6"></td>
                            <th class="text-right">Total return: </th>
                            <td class="text-right">${requestScope.tFee.totalReturn}</td>
                        </tr>
                        <tr>
                            <td colspan="6"></td>
                            <th class="text-right">Total pay more: </th>
                            <td class="text-right">${requestScope.tFee.totalPaymore}</td>
                        </tr>
                    </table>
                </div>
                <!-- end table here-->
                <!--IMPORTANT!!! EDIT ZONE END FROM HERE - Do not change anything else-->
            </div>
    </body>
</html>