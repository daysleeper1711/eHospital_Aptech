<%@include file="header.jsp" %>
<div class="col-sm-9 col-md-9 text-left">
    <h1 class="page-header">
        Diagnosis <small>${sessionScope.empInfo.faculty} - ${sessionScope.empInfo.position}</small>
    </h1>
</div>
</div>
<div class="row-fluid text-left" >
    <div class="col-sm-8 col-md-8">
        <h3>Medical Record info</h3>
        <div class="table-responsive">
            <table class="showInfo">
                <tr>
                    <th>Medical Record ID:</th>
                    <td>${requestScope.passObject.medRecordID}</td>
                    <th>Registry date:</th>
                    <td>${requestScope.passObject.createDate}</td>
                </tr>
                <tr>
                    <th>Symptoms:</th>
                    <td colspan="5">${requestScope.passObject.symptoms}</td>
                </tr>
            </table>
        </div>
        <!--Show form-->
        <h3>Info details</h3>
        <div class="table-responsive">
            <table class="showInfo">
                <tr>
                    <th>Patient ID:</th>
                    <td colspan="5">${requestScope.passObject.patInfo.patID}</td>
                </tr>
                <tr>
                    <th>Name:</th>
                    <td>${requestScope.passObject.patInfo.patName}</td>
                    <th>Date of birth:</th>
                    <td>${requestScope.passObject.patInfo.dob}</td>
                    <th>Gender:</th>
                    <td>${requestScope.passObject.patInfo.gender}</td>
                </tr>
            </table>
        </div>
        <!--end show...-->
        <form id="inputForm" style="width: 70%;margin-bottom: 20px;"
              action="DiagnosisMainProcess" method="post">
            <h3>Declaration</h3>
            <div class="form-group">
                <label>Diagnosis</label>
                <textarea class="form-control" rows="3" placeholder="Doctor's diagnosis input..."
                          data-parsley-notblank="true" data-parsley-required="true"
                          data-parsley-maxlength="100"
                          name="txtDiagnosis"></textarea>
            </div>
            <div class="form-group">
                <label>Suggest treatment</label>
                <input class="form-control" placeholder="Doctor's suggest treatment..."
                       data-parsley-notblank="true" data-parsley-required="true"
                       data-parsley-maxlength="100"
                       name="txtSuggestTreatment"/>
            </div>
            <button type="submit" form="inputForm" class="btn btn-default"
                    name="btnSubmit" value="Input">
                Submit <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
            </button>
            <input type="hidden" name="passID" value="${requestScope.passObject.medRecordID}" />
        </form>
    </div>
</div>
<script type="text/javascript">
    var parsleyOptions = {
        successClass: "has-success",
        errorClass: "has-error",
        classHandler: function(e) {
            return e.$element.closest(".form-group");
        },
        errorsWrapper: '<span class="center-block text-danger" style="padding-left: 10px; padding-top: 5px"></span>',
        errorTemplate: '<p></p>'
    };
    $("#inputForm").parsley(parsleyOptions);
</script>
<%@include file="footer.jsp" %>