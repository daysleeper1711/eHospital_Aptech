<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Test result Management</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- jQuery -->
        <script src="js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>
    </head>
    <body>
        <%@include file="../tuyetTemplate/header.jsp" %>
        <%@include file="../tuyetTemplate/side-menu.jsp" %>
        <div class="col-sm-9 col-md-9">
            <h1 class="page-header">
                Test result Management <!--Change here for title of the page-->
            </h1>
        </div>
    </div>
    <div class="row-fluid text-left">
        <div class="col-sm-8 col-md-8" style="margin-bottom:50px">
            <!--Messages-->
            <c:if test="${requestScope.msg ne null}">
                <div id="serverMsg" class="form-group alert <c:if test="${requestScope.t eq '1'}">alert-info</c:if> 
                     <c:if test="${requestScope.t eq null}">alert-danger</c:if>" align="left" >
                    <p id="msg">${requestScope.msg}</p>
                </div>
            </c:if>
            <!--form search-->
            <form action="ResultController" style="padding:20px">
                <div class="form-group" >
                    <input name="requestID" type="text" class="form-control" 
                           value="<c:if test="${requestScope.testID ne null}">${requestScope.testID}</c:if>"
                           placeholder="Enter Test Request ID" style="width:45%; margin-right: 5%; float:left; margin-left:25%">

                    <button name="action" value="btnSearch" type="submit" class="btn btn-primary">Search</button>
                </div>
                <!--end form input...-->

            </form>

            <form action="ResultController" style="padding:20px" class = "col-sm-12 col-md-12">
                <input name="requestID" type="hidden" value="${requestScope.testID}"/>
                <div class="form-group col-sm-6 col-md-6" style = "float:left">
                    <div class="form-group">
                        <label for="t">Admission ID</label>
                        <input name="adID" class="form-control" id="disabledInput" type="text" readonly 
                               value = "${requestScope.adID}">
                    </div>
                    <div class="form-group">
                        <label for="t">Patient name</label>
                        <input name="patName" class="form-control" id="disabledInput" type="text" readonly 
                               value = "${requestScope.pat.patName}">
                    </div>
                    <div class="form-group">
                        <label for="t">Status</label>
                        <input name="status" class="form-control" id="disabledInput" type="text" readonly 
                               value = "${requestScope.test.request.status}">
                    </div>

                </div>	
                <div class="form-group col-sm-6 col-md-6">
                    <div class="form-group">
                        <label for="t">From faculty</label>
                        <input name="fFac" class="form-control" id="disabledInput" type="text" readonly 
                               value = "${requestScope.test.request.fromFac.facultyName}">
                    </div>
                    <div class="form-group">
                        <label for="t">Appointed doctor</label>
                        <input name="appDoc" class="form-control" id="disabledInput" type="text" readonly 
                               value = "${requestScope.test.request.appointedDoc.fullname}">
                    </div>
                    <div class="form-group" style = "margin-top:11%">
                        <c:if test="${requestScope.test.request.status eq 'New'}" >
                            <button name="action" value="btnAddResult" type="submit" class="btn btn-primary">Add result</button>					
                        </c:if>
                        <c:if test="${requestScope.test.request.status eq 'Updating'}" >
                            <button name="action" value="btnAddResult" type="submit" class="btn btn-primary" disabled>Add result</button>					
                        </c:if>
                        <c:if test="${requestScope.test.request.status ne 'New' and requestScope.test.request.status ne 'Updating' and requestScope.test.request.status ne null}">
                            <button name="action" value="btnViewResult" type="submit" class="btn btn-primary">View result</button>	
                        </c:if>
                    </div>
                </div>
            </form>

        </div>
        <!-- end table here-->
        <!--IMPORTANT!!! EDIT ZONE END FROM HERE - Do not change anything else-->
    </div>

</body>
<%@include file="../tuyetTemplate/footer.jsp" %>
</html>
