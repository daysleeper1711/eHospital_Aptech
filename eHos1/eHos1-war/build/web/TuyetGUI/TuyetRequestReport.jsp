<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Report test result</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- jQuery -->
        <script src="js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>
        <style>    
            /* Set black background color, white text and some padding */
            footer {
                background-color: #555;
                color: white;
                padding: 8px;
            }
            .div-table {
                display:table
            }
            .div-colunm {
                display: table-cell;
                width: 20%;
                padding: 0px 8px 0px 8px;
            }

        </style>
    </head>
    <body>
        <header>
            <div class="container-fluid" style="z-index:4; float:left;">
                <div class="col-sm-12 col-md-12">
                    <img src="./tuyetTemplate/logo.JPG" class="img-responsive" style="opacity:0.6;width:120px; height:120px"/>
                </div>
            </div>
            <div class="col-sm-8 col-md-8">
                <h3>eHopital</h3>
                <h4>${requestScope.test.request.fromFac.faID} - ${requestScope.test.request.fromFac.facultyName}</h4>
                <h4>Admission ID: ${requestScope.adID}</h4>
                <h4>Test No: <span name="requestID">${requestScope.testID}</span></h4>
            </div>
        </header>
        <div class="row-fluid text-center">

            <div class="col-sm-12 col-md-12">
                <h1 class="page-header">
                    Test Request Form<!--Change here for title of the page-->
                </h1>
            </div>

        </div>
        <div class="row-fluid text-left">
            <div class="col-sm-2 col-md-2"></div>
            <div class="col-sm-8 col-md-8" style="">
                <!--information of patient-->
                <div class="col-sm-12 col-md-12" >
                    <div class = "div-table form-group">
                        <div class= "div-colunm div-col1">
                            <div class="form-group">			
                                <label>Patient name: ${requestScope.pat.patName}</label>
                            </div>

                            <div class="form-group">	
                                <label>Address: ${requestScope.pat.address}</label>
                            </div>
                            <div class="form-group">			
                                <label>Diagnosis: ${requestScope.test.request.diagnosis}</label>
                            </div>
                        </div>
                        <div class= "div-colunm div-col2">
                            <div class="form-group" style="display: inline">			
                                <label style="margin-right: 10%">Age: ${requestScope.age}</label>
                                <label>Gender: <c:if test="${requestScope.pat.gender eq true}">Female</c:if><c:if test="${requestScope.pat.gender eq false}">Male</c:if></label>
                                </div>
                                <br/><br/>
                            <div class="form-group">			
                                <label>To faculty: ${requestScope.test.request.toFac.faID} - ${requestScope.test.request.toFac.facultyName}</label>
                            </div>

                        </div>

                    </div>
                </div>

                <!--table show result of test-->
                <div class="form-group col-sm-12 col-md-12">	
                    <div class="table-responsive">
                        <label style = "text-decoration:underline; font-weight:bold">List of requested services</label>
                        <table class="table table-bordered table-hover table-striped">
                            <thead>
                               <tr>
                                    <th>No</th>
                                    <th>Content</th>
                                    <th>Group</th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${requestScope.listReqDetail}" var="rs" varStatus="stt">
                                    <tr>
                                        <td>${stt.count}</td>
                                        <td>${rs.serviceID.serviceName}</td>
                                        <td>${rs.serviceID.groupID.groupName}</td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                   
                    <div class = "div-table form-group" style="margin-left: 70%">
                        <div class="form-group"  style="margin-bottom: 100px; ">
                            <p><fmt:formatDate value="${requestScope.test.request.createDate}" type="date" dateStyle="long" timeStyle="long"/></p>
                        </div>
                        <div class="form-group">
                            <p>${requestScope.test.request.appointedDoc.fullname}</p>
                        </div>
                    </div>

                </div>
            </div>	


            <footer>
                <div class="container">
                    <p class="text-muted">eHopital@2017</p>
                </div>
            </footer>
            <!--css for footer-->
            <style>
                html {
                    position: relative;
                    min-height: 100%;
                }
                body {
                    /* Margin bottom by footer height */
                    margin-bottom: 20px;
                }
                footer {
                    position: absolute;
                    bottom: 0;
                    width: 100%;
                    /* Set the fixed height of the footer here */
                    height: 20px;
                    background-color: #f5f5f5;
                }
            </style>
            <!-- css header
            ================================================== -->
            <style>
                header {
                    background: url(./tuyetTemplate/banner.jpg) no-repeat;
                    background-size: cover;
                    -webkit-background-size: cover;
                    -moz-background-size: cover;
                    -o-background-size: cover;	
                    height:130px;
                }
                .mr10 {
                    margin-right: 10px;
                }
                .mr20 {
                    margin-right: 20px;
                }
                .pd20 {
                    padding-top: 20px;
                }
            </style>
            <!-- css header
            ================================================== -->
    </body>
</html>
