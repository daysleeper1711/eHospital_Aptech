<%-- 
    Document   : ServiceUpdate
    Created on : Jan 23, 2017, 2:48:59 PM
    Author     : Hoang Tuyet
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Update service</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- jQuery -->
        <script src="js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>
    </head>
    <body>
        <%@include file="../tuyetTemplate/header.jsp" %>
        <%@include file="../tuyetTemplate/side-menu.jsp" %>
        <div class="col-sm-9 col-md-9">
            <h1 class="page-header">
                Update Service <!--Change here for title of the page-->
            </h1>
        </div>

    </div>
    <div class="row-fluid text-left">
        <div class="col-sm-8 col-md-8" style="margin-bottom:50px">
            <!--Messages-->
            <div id="divMsg" hidden class="form-group alert alert-danger" align="left" >
                <p id="msgValidate"></p>
            </div>
            <c:if test="${requestScope.msg ne null}">
                <div id="serverMsg" class="form-group alert <c:if test="${requestScope.t eq '1'}">alert-success</c:if> 
                     <c:if test="${requestScope.t eq null}">alert-danger</c:if>" align="left" >
                    <p id="msg">${requestScope.msg}</p>
                </div>
            </c:if>
            <!--form update-->
            <form action="ServiceController" method="post" style="padding:20px">
                <div class="form-group" style="width:45%; float:right; magin-right:5%">

                    <label>Service content </label>
                    <textarea name="txtContent" class="form-control" rows="3">${requestScope.serviceDTO.service.serviceContent}</textarea>
                    <label>Create date </label>
                    <input type="text" class="form-control" readonly value="<fmt:formatDate value="${requestScope.serviceDTO.service.createDate}" pattern="dd/MM/yyyy HH:mm:ss"/>"/>
                    <label>Creator: </label>
                    <input type="text" class="form-control"readonly value= "${requestScope.serviceDTO.service.creator.fullname}"/>
                    <label>Update date </label>
                    <input type="text" class="form-control" readonly value="<fmt:formatDate value="${requestScope.serviceDTO.service.updateDate}" pattern="dd/MM/yyyy HH:mm:ss"/>"/>
                    <label>Update by </label>
                    <input type="text" class="form-control" readonly value="${requestScope.serviceDTO.service.updateEmp.fullname}"/>
                </div>
                <div class="form-group" style="width:50%;">
                    <input type="hidden" name="id" value="${requestScope.serviceDTO.service.serviceID}"/>
                    <label>Service name </label>
                    <textarea name="txtName" id="txtName" class="form-control" rows="3">${requestScope.serviceDTO.service.serviceName}</textarea>

                    <label>Price (USD)</label>
                    <input name="txtPrice" id="txtPrice" type="text" class="form-control" value="${requestScope.serviceDTO.curPrice}">

                    <label>Unit of measure </label>
                    <input name="txtMeasure" type="text" class="form-control" value="${requestScope.serviceDTO.service.unitOfMea}">

                    <label>Service group </label>
                    <select name="cbGroup" id="cbGroup" class="form-control" >
                        <c:forEach items="${requestScope.listGroup}" var="group" varStatus="stt">
                            <option value="${group.groupID}" 
                                    <c:if test="${group.groupID eq requestScope.serviceDTO.service.groupID.groupID}">selected</c:if> >${group.groupName}</option>
                        </c:forEach>
                    </select>

                    <label>Status </label>
                    <select name="cbStatus" class="form-control" >
                        <option value = "1" <c:if test="${requestScope.serviceDTO.service.status eq true}">selected</c:if>>Enable</option>
                        <option value = "0" <c:if test="${requestScope.serviceDTO.service.status eq false}">selected</c:if>>Disable</option>
                        </select>
                    </div>
                    <div class="form-group" style="width:100%; clear:both" align = "right">
                        <button name="action" value="btnUpdateService" type="submit" onclick="return validate();"
                                class="btn btn-primary" style="margin-top:10px;">Update</button>
                        <button name="action" value="btnCancel" type="submit" 
                                class="btn btn-primary" style="margin-top:10px;">Cancel</button>
                    </div>

                </form>

            </div>
        </div>	

    </body>
<%@include file="../tuyetTemplate/footer.jsp" %>
<script>
    function validate() {
        var name = document.getElementById("txtName").value;
        var group = document.getElementById("cbGroup").value;
        var price = document.getElementById("txtPrice").value;

        if (name === "") {
            document.getElementById("divMsg").removeAttribute("hidden");
            document.getElementById("serverMsg").setAttribute("hidden","hidden");
            document.getElementById("msgValidate").innerHTML = "Service name is not null";
            document.getElementById("txtName").focus();
            return false;
        }

        if (group === "") {
            document.getElementById("divMsg").removeAttribute("hidden");
            document.getElementById("serverMsg").setAttribute("hidden","hidden");
            document.getElementById("msgValidate").innerHTML = "Please, select group service!";
            document.getElementById("cbGroup").focus();
            return false;
        }

        if (price === "") {
            document.getElementById("divMsg").removeAttribute("hidden");
            document.getElementById("serverMsg").setAttribute("hidden","hidden");
            document.getElementById("msgValidate").innerHTML = "Price is not null";
            document.getElementById("txtPrice").focus();
            return false;
        }
         var p = parseFloat(price);
        if(p < 0) {
            document.getElementById("divMsg").removeAttribute("hidden");
            document.getElementById("serverMsg").setAttribute("hidden","hidden");
            document.getElementById("msgValidate").innerHTML = "Price is not less than 0";
            document.getElementById("txtPrice").focus();
            return false;
        }
        var ptt = /^\d+(\.\d+)?$/;
        if(!ptt.test(price)) {
            document.getElementById("divMsg").removeAttribute("hidden");
            document.getElementById("serverMsg").setAttribute("hidden","hidden");
            document.getElementById("msgValidate").innerHTML = "Please, match the requested format! (Ex: 10, 10.5)";
            document.getElementById("txtPrice").focus();
            return false;
        }
        return true;
    }
</script>
</html>

