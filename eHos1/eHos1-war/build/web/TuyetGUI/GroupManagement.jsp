<%-- 
    Document   : ServiceUpdate
    Created on : Jan 23, 2017, 2:48:59 PM
    Author     : Hoang Tuyet
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Service Group Management</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- jQuery -->
        <script src="js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>
        
        <!--pagination-->
        <script type="text/javascript" src="js/jPager/js.js"></script>
        <script type="text/javascript" src="js/jPager/jPages.min.js"></script>
        <link rel="stylesheet" href="css/jPager/jPages.css" type="text/css"/>

        <script>
//            run when document is ready
            $(function() {
//                initiate plugin
                $("div.holder").jPages({
                    containerID: "itemContainer",
                    perPage: 5
                });
            });
        </script>
    </head>
    <body>
        <%@include file="../tuyetTemplate/header.jsp" %>
        <%@include file="../tuyetTemplate/side-menu.jsp" %>
        <div class="col-sm-9 col-md-9">
            <h1 class="page-header">
                Service Group Management <!--Change here for title of the page-->
            </h1>
        </div>

    </div>
    <div class="row-fluid text-left">
        <div class="col-sm-8 col-md-8" style="margin-bottom:50px">
            <!--Messages-->
            <div id="divMsg" hidden class="form-group alert alert-danger" align="left" >
                <p id="msgValidate"></p>
            </div>
            <!--form search-->
            <form action="GroupController" method="post" style="padding:20px">
                <div class="form-group">
                    <div style="display:inline">
                        <input name="groupName" id="groupName" type="text"
                               value="<c:if test="${requestScope.groupName ne null}">${requestScope.groupName}</c:if>" 
                                   class="form-control" placeholder="Enter group name" style="width:30%; float:left; margin-right:5%;"/>
                               <select name="cbGroup" id="cbGroup" class="form-control" style  = "width:30%;float:left; margin-right:5%;">
                                   <option value="" <c:if test="${requestScope.stt ne null}">selected</c:if>>All status</option>
                               <option value="1" <c:if test="${requestScope.stt eq '1'}">selected</c:if>>Enable</option>
                               <option value="0" <c:if test="${requestScope.stt eq '0'}">selected</c:if>>Disable</option>
                               </select>

                               <select name="cbType" id="cbType" class="form-control" style  = "width:30%;">
                                   <option value="" <c:if test="${requestScope.type ne null}">selected</c:if>>All group type</option>
                               <option value="Exam" <c:if test="${requestScope.type eq 'Exam'}">selected</c:if>>Exam</option>
                               <option value="Other" <c:if test="${requestScope.type eq 'Other'}">selected</c:if>>Other</option>
                               </select>
                        </div>

                        <button name="action" type="submit" value="btnSearch" onclick="return checkValid();"
                                class="btn btn-primary" style="margin-top:10px;">Search</button>
                        <button name="action" value="btnShow"
                                type="submit" class="btn btn-primary" style="margin-top:10px;">Show all</button>
                        <button name="action" value="btnAddNew" 
                                type="submit" class="btn btn-primary" style="margin-top:10px;">Add New</button>
                    </div>
                    <!--end form input...-->
                </form>

                <!--data table-->
            <c:if test="${empty requestScope.listGroup}">
                <p><i>There is no group</i></p>
            </c:if>
            <c:if test="${not empty requestScope.listGroup}" >
                <div class="table-responsive">
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Group Name</th>
                                <th>Type</th>
                                <th>Create Date</th>
                                <th>Creator</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody id="itemContainer">
                            <c:forEach items="${requestScope.listGroup}" var="group" varStatus="stt"> 
                                <tr>
                                    <td>${stt.count}</td>
                                    <td>${group.groupName}</td>
                                    <td>${group.groupType}</td>
                                    <td><fmt:formatDate value="${group.createDate}" pattern="dd/MM/yyyy HH:mm:ss"/></td>
                                    <td>${group.creator.fullname}</td>
                                    <td>
                                        <c:if test="${group.status eq true}">Enable</c:if>
                                        <c:if test="${group.status eq false}">Disable</c:if>
                                        </td>

                                        <td>
                                            <form action="GroupController" method="post">
                                                <input name="groupID" type="hidden" value="${group.groupID}"/>
                                                <button name="action" value="btnEdit" type="submit" class="btn btn-default">Edit</button>
                                            <c:if test="${group.status eq true}">
                                                <button name="action" value="Disable" type="submit" class="btn btn-default">Disable</button>
                                            </c:if>
                                            <c:if test="${group.status eq false}">
                                                <button name="action" value="Enable" type="submit" class="btn btn-default">Enable</button>
                                            </c:if>
                                        </form>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
                <!--pagination-->
                <div class="holder" align="right"></div>
            </c:if>
            
        </div>
        <!-- end table here-->
        <!--IMPORTANT!!! EDIT ZONE END FROM HERE - Do not change anything else-->
    </div>
</body>
<%@include file="../tuyetTemplate/footer.jsp" %>
<script>
    function checkValid() {
        var gname = document.getElementById("groupName").value;
        var st = document.getElementById("cbGroup").value;
        var t = document.getElementById("cbType").value;

        if (gname === "" && st === "" && t === "") {
            document.getElementById("divMsg").removeAttribute("hidden");
            document.getElementById("msgValidate").innerHTML = "Please, enter search information!";
            document.getElementById("groupName").focus();
            return false;
        }
        return true;
    }
</script>

</html>

