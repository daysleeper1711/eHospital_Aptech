<%-- 
    Document   : ServiceCreate
    Created on : Jan 20, 2017, 10:58:23 AM
    Author     : Hoang Tuyet
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Create service</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- jQuery -->
        <script src="js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>
    </head>
    <body>
        <%@include file="../tuyetTemplate/header.jsp" %>
        <%@include file="../tuyetTemplate/side-menu.jsp" %>
        <div class="col-sm-9 col-md-9">
            <h1 class="page-header">
                Add New Service <!--Change here for title of the page-->
            </h1>
        </div>

    </div>
    <div class="row-fluid text-left">
        <div class="col-sm-8 col-md-8" style="margin-bottom:50px">
            <!--Messages-->
            <div id="divMsg" hidden class="form-group alert alert-danger" align="left" >
                <p id="msgValidate"></p>
            </div>
            <c:if test="${requestScope.msg ne null}">
                <div class="form-group alert alert-danger" id="serverMsg" align="left" >
                <p>${requestScope.msg}</p>
            </div>
            </c:if>
            <!-- Insert Form -->
            <form action="ServiceController" method="post" style="padding:20px">
                <div class="form-group" >
                    <label>Service name: </label>
                    <textarea name="txtName" id="txtName" class="form-control" 
                              rows="3" ><c:if test="${requestScope.insertService ne null}">${requestScope.insertService.serviceName}</c:if></textarea>

                    <label>Service content: </label>
                    <textarea name="txtContent" id="txtContent" class="form-control" 
                              rows="3" ><c:if test="${requestScope.insertService ne null}">${requestScope.insertService.serviceContent}</c:if></textarea>

                    <label>Service group: </label>
                    <select name="cbGroup" id="cbGroup" class="form-control" style  = "width:50%;">
                        <option value="">Choose service group</option>
                        <c:forEach items="${requestScope.listGroup}" var="group" varStatus="stt">
                            <option value="${group.groupID}" <c:if test="${group.groupID eq requestScope.groupid}">selected</c:if> >${group.groupName}</option>
                        </c:forEach>
                    </select>
                    <label>Price (USD): </label>
                    <input name="txtPrice" id="txtPrice" <c:if test="${requestScope.insPrice ne null}">value="${requestScope.insPrice}"</c:if>
                           type="text" class="form-control" >
                    <label>Unit of measure: </label>
                    <input name="txtMeasure" id="txtMeasure" <c:if test="${requestScope.insertService ne null}"> value="${requestScope.insertService.unitOfMea}"</c:if>
                           type="text" class="form-control">

                </div>
                <div class="form-group" align="right" >
                    <button name="action" value="btnCreateService" type="submit" onclick="return validate();"
                            class="btn btn-primary" style="margin-top:10px;">Add</button>
                    <button name="action" value="btnCancel" type="submit" 
                            class="btn btn-primary" style="margin-top:10px;">Cancel</button>
                </div>
                <!--end form input...-->
            </form>

            <!--IMPORTANT!!! EDIT ZONE START FROM HERE - Do not change anything else-->
            <!--I am not responsible any wrong if you change anything without permission-->
            <!--FOR THE ENGLISH TRANSLATE - GOOGLE PLZ ^_^-->
            <!--table here-->

            <!-- end table here-->
            <!--IMPORTANT!!! EDIT ZONE END FROM HERE - Do not change anything else-->
        </div>
    </div>	
</body>
<%@include file="../tuyetTemplate/footer.jsp" %>
<script>
    function validate() {
        var name = document.getElementById("txtName").value;
        var group = document.getElementById("cbGroup").value;
        var price = document.getElementById("txtPrice").value;

        if (name === "") {
            document.getElementById("divMsg").removeAttribute("hidden");
            document.getElementById("serverMsg").setAttribute("hidden","hidden");
            document.getElementById("msgValidate").innerHTML = "Service name is not null";
            document.getElementById("txtName").focus();
            return false;
        }

        if (group === "") {
            document.getElementById("divMsg").removeAttribute("hidden");
            document.getElementById("serverMsg").setAttribute("hidden","hidden");
            document.getElementById("msgValidate").innerHTML = "Please, select group service!";
            document.getElementById("cbGroup").focus();
            return false;
        }

        if (price === "") {
            document.getElementById("divMsg").removeAttribute("hidden");
            document.getElementById("serverMsg").setAttribute("hidden","hidden");
            document.getElementById("msgValidate").innerHTML = "Price is not null";
            document.getElementById("txtPrice").focus();
            return false;
        }
         var p = parseFloat(price);
        if(p < 0) {
            document.getElementById("divMsg").removeAttribute("hidden");
            document.getElementById("serverMsg").setAttribute("hidden","hidden");
            document.getElementById("msgValidate").innerHTML = "Price is not less than 0";
            document.getElementById("txtPrice").focus();
            return false;
        }
        var ptt = /^\d+(\.\d+)?$/;
        if(!ptt.test(price)) {
            document.getElementById("divMsg").removeAttribute("hidden");
            document.getElementById("serverMsg").setAttribute("hidden","hidden");
            document.getElementById("msgValidate").innerHTML = "Please, match the requested format! (Ex: 10, 10.5)";
            document.getElementById("txtPrice").focus();
            return false;
        }
        return true;
    }
</script>
</html>
