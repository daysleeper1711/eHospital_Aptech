<%-- 
    Document   : ServiceUpdate
    Created on : Jan 23, 2017, 2:48:59 PM
    Author     : Hoang Tuyet
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Add service Group</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- jQuery -->
        <script src="js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>
    </head>
    <body>
        <%@include file="../tuyetTemplate/header.jsp" %>
        <%@include file="../tuyetTemplate/side-menu.jsp" %>
        <div class="col-sm-9 col-md-9">
            <h1 class="page-header">
                Add Service Group <!--Change here for title of the page-->
            </h1>
        </div>

    </div>
    <div class="row-fluid text-left">
        <div class="col-sm-8 col-md-8" style="margin-bottom:50px">
            <!--Messages-->
            <div id="divMsg" hidden class="form-group alert alert-danger" align="left" >
                <p id="msgValidate"></p>
            </div>
            <c:if test="${requestScope.msg ne null}">
                <div id="serverMsg" class="form-group alert alert-danger" align="left" >
                    <p id="msg">${requestScope.msg}</p>
            </div>
            </c:if>
            <!--form insert-->
            <form action="GroupController" method="post" style="padding:20px">

                <div class="form-group" >
                    <label>Group name </label>
                    <textarea name="gName" id="txtGName" class="form-control" rows="1"><c:if test="${requestScope.gname ne null}">${requestScope.gname}</c:if></textarea>
                    <label>Description </label>
                    <textarea name="description" id="description" class="form-control" rows="3"><c:if test="${requestScope.desc ne null}">${requestScope.desc}</c:if></textarea>
                    <label>Type</label>
                    <select name="cbType" class="form-control" style="width: 50%">
                        <option value="Exam" <c:if test="${requestScope.type eq 'Exam'}">selected</c:if>>Exam</option>
                        <option value="Other" <c:if test="${requestScope.type eq 'Other'}">selected</c:if>>Other</option>
                    </select>
                </div>

                <div class="form-group" align="right" >
                    <button name="action" value="btnInsert" onclick="return validate();"
                            type="submit" class="btn btn-primary" style="margin-top:10px;">Add</button>
                    <button name="action" value="btnCancel" type="submit" class="btn btn-primary" style="margin-top:10px;">Cancel</button>
                </div>
                <!--end form input...-->
            </form>

        </div>
    </div>	

</body>
<%@include file="../tuyetTemplate/footer.jsp" %>

<script type="text/javascript">
    function validate() {
        var name = document.getElementById("txtGName").value;
        if(name === "") {
            document.getElementById("divMsg").removeAttribute("hidden");
            //document.getElementById("serverMsg").setAttribute("hidden","hidden");
            document.getElementById("msgValidate").innerHTML = "Group name is required";
            document.getElementById("txtGName").focus();
            return false;
        }
        return true;
    }
</script>
</html>

