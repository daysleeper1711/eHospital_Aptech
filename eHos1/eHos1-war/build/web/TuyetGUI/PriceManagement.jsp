<%-- 
    Document   : ServiceUpdate
    Created on : Jan 23, 2017, 2:48:59 PM
    Author     : Hoang Tuyet
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Service Price Management</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- jQuery -->
        <script src="js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>
        <!--pagination-->
        <script type="text/javascript" src="js/jPager/js.js"></script>
        <script type="text/javascript" src="js/jPager/jPages.min.js"></script>
        <link rel="stylesheet" href="css/jPager/jPages.css" type="text/css"/>

        <script>
//            run when document is ready
            $(function() {
//                initiate plugin
                $("div.holder").jPages({
                    containerID: "itemContainer",
                    perPage: 5
                });
            });
        </script>
    </head>
    <body>
        <%@include file="../tuyetTemplate/header.jsp" %>
        <%@include file="../tuyetTemplate/side-menu.jsp" %>
        <div class="col-sm-9 col-md-9">
            <h1 class="page-header">
                Service Price Management <!--Change here for title of the page-->
            </h1>
        </div>

    </div>
    <div class="row-fluid text-left">
        <div class="col-sm-8 col-md-8" style="margin-bottom:50px">
            <!--Messages-->
            <div id="divMsg" hidden class="form-group alert alert-danger" align="left" >
                <p id="msgValidate"></p>
            </div>
            <!--form search-->
            <form action="PriceController"  method="post" style="padding:20px">
                <div class="form-group " >
                    <div style="display:inline">
                        <input name="serviceName" id="serviceName" type="text" class="form-control"
                               value="<c:if test="${requestScope.nameSearch ne null}">${requestScope.nameSearch}</c:if>"
                                   placeholder="Enter service name" style="width:45%; float:left; margin-right: 5%">

                               <select name="searchStt" id="cbStatus" class="form-control" style  = "width:50%;" >
                                   <option value="" <c:if test="${requestScope.stt ne null}">selected</c:if>>All status</option>
                               <option value="1" <c:if test="${requestScope.stt eq '1'}">selected</c:if>>Enable</option>
                               <option value="0" <c:if test="${requestScope.stt eq '0'}">selected</c:if>>Disable</option>
                               </select>

                        </div>

                        <button name="action" type="submit" value="btnSearch" onclick="return validate();"
                                class="btn btn-primary" style="margin-top:10px;">Search</button>
                        <button name="action" value="btnShow" type="submit" class="btn btn-primary" style="margin-top:10px;">Show all</button>

                    </div>
                    <!--end form input...-->
                </form>

                <!--data table-->
            <c:if test="${empty requestScope.listPrice}">
                <p><i>There is no price</i></p>
            </c:if>
            <c:if test="${not empty requestScope.listPrice}" >
                <div class="table-responsive">
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Service Name</th>
                                <th>Price</th>
                                <th>Create Date</th>
                                <th>Creator</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody id="itemContainer">
                            <c:forEach items="${requestScope.listPrice}" var="price" varStatus="stt"> 
                                <tr>
                                    <td>${stt.count}</td>
                                    <td>${price.serviceID.serviceName}</td>
                                    <td>$${price.price}</td>
                                    <td><fmt:formatDate value="${price.createDate}" pattern="dd/MM/yyyy HH:mm:ss"/></td>
                                    <td>${price.creator.fullname}</td>
                                    <td>
                                        <c:if test="${price.status eq true}">Enable</c:if>
                                        <c:if test="${price.status eq false}">Disable</c:if>
                                        </td>
                                    </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
                <!--pagination-->
                <div class="holder" align="right"></div>
            </c:if>
        </div>
        <!-- end table here-->
        <!--IMPORTANT!!! EDIT ZONE END FROM HERE - Do not change anything else-->
    </div>

</body>
<%@include file="../tuyetTemplate/footer.jsp" %>
<script>
    function validate() {
        var name = document.getElementById("serviceName").value;
        var stt = document.getElementById("cbStatus").value;

        if (name === "" && stt === "") {
            document.getElementById("divMsg").removeAttribute("hidden");
            document.getElementById("msgValidate").innerHTML = "Please, enter search information!"
            document.getElementById("serviceName").focus();
            return false;
        }
        return true;
    }
</script>
</html>

