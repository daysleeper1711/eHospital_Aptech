<%-- 
    Document   : TestRequestManagement
    Created on : Jan 3, 2017, 1:22:30 PM
    Author     : Hoang Tuyet
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Test request management</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap Core CSS -->
        <link href="../eHos1-war/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="../eHos1-war/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- jQuery -->
        <script src="../eHos1-war/js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="../eHos1-war/js/bootstrap.min.js"></script>
        
        <!--pagination-->
        <script type="text/javascript" src="js/jPager/js.js"></script>
        <script type="text/javascript" src="js/jPager/jPages.min.js"></script>
        <link rel="stylesheet" href="css/jPager/jPages.css" type="text/css"/>

        <script>
//            run when document is ready
            $(function() {
//                initiate plugin
                $("div.holder").jPages({
                    containerID: "itemContainer",
                    perPage: 5
                });
            });
        </script>
    </head>
    <body>
        <%@include file="../tuyetTemplate/header.jsp" %>
        <%@include file="../tuyetTemplate/side-menu.jsp" %>
            <div class="col-sm-9 col-md-9">
                <h1 class="page-header">
                    Test Request Management <!--Change here for title of the page-->
                </h1>
            </div>

        </div>
        <div class="row-fluid text-left">
            <div class="col-sm-8 col-md-8" style="margin-bottom: 50px">
                <!--Messages-->
                <c:if test="${requestScope.msg ne null}">
                    <div id="serverMsg" class="form-group alert alert-info" >
                        <p>${requestScope.msg}</p>
                    </div>
                </c:if>
                <!--message if click create new but no information of medical record-->
                <div class="form-group">
                    <p id="error" hidden style="color: red">*You have not supply admission information yet. Please, search for that!</p>
                </div>
                <!--form search-->
                <form action="TuyetMainController" method = "post" style="padding:20px">
                    <!--IMPORTANT!!! EDIT ZONE START FROM HERE - Do not change anything else-->
                    <!--I am not responsible any wrong if you change anything without permission-->
                    <!--FOR THE ENGLISH TRANSLATE - GOOGLE PLZ ^_^-->

                    <!--copy form input here...-->
                    <!--input 1-->
                    <div class="form-group" style="float:left; margin-right:5%" >

                        <label class="radio-inline">
                            <input type="radio" name="optionsRadiosInline" id="optionsRadiosInline1" 
                                   value="inpatient" <c:if test="${(empty requestScope.patType) or (requestScope.patType eq 'inpatient')}"> checked </c:if>>Inpatient 
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="optionsRadiosInline" id="optionsRadiosInline2" 
                                       value="outpatient" <c:if test="${requestScope.patType eq 'outpatient'}"> checked </c:if>>OutPatient 
                            </label>																				
                        </div>
                        <div class="form-group">		
                            <input name="txtSearchAddID" type="text" <c:if test="${requestScope.admissionID ne null}"> value="${requestScope.admissionID}"</c:if> 
                                   required class="form-control" placeholder="Enter Admission ID" style="float:left;width:40%; margin-right: 5%">							 
                            <button name="action" value="btnSearchRequest" type="submit" class="btn btn-primary">Search</button>													
                        </div>
                    <c:if test="${requestScope.msgSearch != null}">
                        <p style='color: red'><i>*${requestScope.msgSearch}</i></p>
                    </c:if>
                    <!--end form input...-->
                </form>

                <!--Result of search-->
                <form action="TuyetMainController" method="post" style="padding:20px" onsubmit="return checkNull();" >
                    <div class="form-group" style="width:40%; float:right; margin-left:5%">
                        <input name="patType" type="hidden" value="${requestScope.patType}"/>
                        <input name="txtAdmission" id = "txtAdmission" type="hidden" class="form-control" value="${requestScope.admissionID}">
                        <label>Patient age</label>
                        <input name="txtAge" type="text" class="form-control" readonly 
                               value = "${requestScope.patAge}">
                        <label>Gender</label>
                        <input name="txtGender" type="text" class="form-control" readonly 
                               value="${requestScope.sex}">
                         <!--disable if medical record was close-->
                         
                        <button name="action" value="btnCreateRequest" type="submit" 
                                class="btn btn-primary" style = "margin-top:8%"
                                <c:if test="${requestScope.mrStatus ne 'in treatment'}"> disabled</c:if>>
                            Create New
                        </button>
                    </div>
                    <div class="form-group" style="width:55%;">	
                        <label>Patient name</label>
                        <input name="txtPatName" type="text" class="form-control" readonly 
                               value = "${requestScope.patName}">
                        <label>Patient Address</label>
                        <input name="txtAddr" type="text" class="form-control" readonly 
                               value = "${requestScope.patAddr}">								
                        <label>Treatment Faculty</label>
                        <input name="txtFrFac" type="text" class="form-control" readonly 
                               value = "${requestScope.treatmentFac}">												
                    </div>

                    <!--end form input...-->
                </form>


                <!--table here-->
                <c:if test="${not empty requestScope.listRequestDTO}">
                    <h4>List of Tests:</h4>
                    <div class="table-responsive" style = "margin-bottom: 50px">
                        <table class="table table-bordered table-hover table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Test ID</th>
                                    <th>Doctor appointed</th>
                                    <th>Create Date</th>
                                    <th>Status</th>
                                    <th>From Factory</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody id="itemContainer">
                                <c:forEach items="${requestScope.listRequestDTO}" var="dto" varStatus="stt">
                                    <tr>
                                        <td>${stt.count}</td>
                                        <td><a href="TuyetTestController?action=reportReq&&reqID=${dto.request.testReqID}"> ${dto.request.testReqID} </a></td>
                                        <td>${dto.request.appointedDoc.fullname}</td>
                                        <td><fmt:formatDate value="${dto.request.createDate}" pattern="dd/MM/yyyy HH:mm:ss" /></td>
                                        <td>${dto.request.status}</td>
                                        <td>${dto.request.fromFac.facultyName}</td>
                                        <td>
                                            <form action="TuyetMainController" method="post">
                                                <input name="txtReqID"type="hidden" value="${dto.request.testReqID}"/>
                                                <c:if test="${dto.request.status eq 'New'}">
                                                    <button name="action" value="btnEditRequest" type="submit" class="btn btn-default">Edit</button>
                                                </c:if>
                                                <c:if test="${dto.request.status eq 'Updating'}">
                                                    <button name="action" value="btnEditRequest" type="submit" class="btn btn-default" disabled>Edit</button>
                                                </c:if>
                                                <c:if test="${dto.request.status ne 'New' and dto.request.status ne 'Updating'}">
                                                    <button name="action" value="btnViewResult" type="submit" class="btn btn-default" >View Result</button>
                                                </c:if>
                                                <input name="requestID"type="hidden" value="${dto.request.testReqID}"/>
                                                <input name="patType" type="hidden" value="${requestScope.patType}"/>
                                                <input name="pSign" value="0" type="hidden"/>

                                            </form>
                                        </td>
                                        <!--end 1 test request--> 
                                    </c:forEach>
                            </tbody>
                        </table>
                    </div>
                    <!--pagination-->
                <div class="holder" align="right"></div>
                </c:if>
            </div> 
        </div>
    </body>

    <script>
        function checkNull() {
            var id = document.getElementById("txtAdmission").value;
            if (id === "") {
                //alert("Please, enter admission ID empty !")
                document.getElementById("error").removeAttribute("hidden");
                return false;
            }
            return true;
        }
    </script>
    <%@include file="../tuyetTemplate/footer.jsp" %>
</html>
