<%-- 
    Document   : ServiceManagement
    Created on : Jan 17, 2017, 8:33:18 PM
    Author     : Hoang Tuyet
--%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html lang="en">
    <head>
        <title>Service Management</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- jQuery -->
        <script src="js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>

        <!--pagination-->
        <script type="text/javascript" src="js/jPager/js.js"></script>
        <script type="text/javascript" src="js/jPager/jPages.min.js"></script>
        <link rel="stylesheet" href="css/jPager/jPages.css" type="text/css"/>

        <script>
//            run when document is ready
            $(function() {
//                initiate plugin
                $("div.holder").jPages({
                    containerID: "itemContainer",
                    perPage: 5
                });
            });
        </script>

    </head>
    <body>
        <%@include file="../tuyetTemplate/header.jsp" %>
        <%@include file="../tuyetTemplate/side-menu.jsp" %>
        <div class="col-sm-9 col-md-9">
            <h1 class="page-header">
                Service Management <!--Change here for title of the page-->
            </h1>
        </div>
    </div>
    <div class="row-fluid text-left">	
        <div class="col-sm-8 col-md-8" style="margin-bottom:50px">
            <!--Messages-->
            <c:if test="${requestScope.msgClickSearch ne null}">
                <div class="alert alert-danger col-sm-12 col-md-12">
                    <p> ${requestScope.msgClickSearch} </p>
                </div>
            </c:if>
            <c:if test="${requestScope.msg ne null}">
                <div class="alert alert-info col-sm-12 col-md-12">
                    <p> ${requestScope.msg} </p>
                </div>
            </c:if>
            <!--form search-->
            <form action="ServiceController" method="post" style="padding:20px">

                <div class="form-group" >

                    <input name="txtName" type="text" class="form-control" placeholder="Enter service name" 
                           <c:if test="${requestScope.name ne null}">value="${requestScope.name}"</c:if>
                               id="txtname" style="width:30%; float:left; margin-right: 5%">

                           <select name="selGroup" id="selGroup" class="form-control" style  = "width:30%; float:left; margin-right:5%" >
                               <option value="" <c:if test="${requestScope.group eq null}">selected</c:if>>All groups</option>
                           <c:forEach items="${requestScope.listGroup}" var="gr" varStatus="stt">
                               <option value="${gr.groupName}" <c:if test="${requestScope.group eq gr.groupName}">selected</c:if>>${gr.groupName}</option>
                           </c:forEach>
                    </select>
                    <select name="selStatus" id="selStatus" class="form-control" style  = "width:30%;" >
                        <option value= "" <c:if test="${requestScope.status eq null}">selected</c:if>>All status</option>
                        <option value="1" <c:if test="${requestScope.status eq '1'}">selected</c:if>>Enable</option>
                        <option value="0" <c:if test="${requestScope.status eq '0'}">selected</c:if>>Disable</option>
                        </select>

                        <button name="action" type="submit" value="btnSearch" onclick="return clickSearch();"
                                class="btn btn-primary" style="margin-top:10px;">Search</button>
                        <button name="action" type="submit" value="btnShow" 
                                class="btn btn-primary" style="margin-top:10px;">Show all</button>
                        <button name="action" type="submit" value="btnAddNew"
                                class="btn btn-primary" style="margin-top:10px;">Add new service</button>

                        <!--page signal-->
                        <input name="pSignal" type="hidden" value="3"/>
                    </div>
                    <!--end form input...-->
                </form>

                <!--show data-->
            <c:if test="${not empty requestScope.listService}">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Service Name</th>
                                <th>Price</th>
                                <th>Group</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody id="itemContainer">
                            <c:forEach items="${requestScope.listService}" var="dto" varStatus="stt">
                                <tr>
                                    <td>${stt.count}</td>
                                    <td> <a href = "ServiceController?action=serviceDetail&id=${dto.service.serviceID}">${dto.service.serviceName} </a></td>
                                    <td>${dto.curPrice}</td>
                                    <td>${dto.service.groupID.groupName}</td>
                                    <td>${dto.status}</td>
                                    <td>
                                        <form action="SVController" method="post">
                                            <input name="serviceID" type="hidden" value="${dto.service.serviceID}"/>
                                            <input name="serviceName" type="hidden" value="${dto.service.serviceName}"/>
                                            <c:if test="${empty dto.service.tbStandardValueCollection}">
                                                <button name="action" value="addSV" type="submit" class="btn btn-default">Add Standard</button>
                                            </c:if>
                                            <c:if test="${not empty dto.service.tbStandardValueCollection}">
                                                <button name="action" value="viewSV" type="submit" class="btn btn-default">View Standard</button>
                                            </c:if>
                                        </form>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
                <!--pagination-->
                <div class="holder" align="right"></div>

            </c:if>

            <!-- end table here-->
            <!--IMPORTANT!!! EDIT ZONE END FROM HERE - Do not change anything else-->
        </div>

    </div>
</body>

<%@include file="../tuyetTemplate/footer.jsp" %>
</html>
