<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="loc" %>

<!DOCTYPE html>

<html >
    <head>
        <title>Edit Supplier</title>
        <meta charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- jQuery -->
        <script language="javascript" src="js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>
        <style>    
            /* Set black background color, white text and some padding */
            footer {
                background-color: #555;
                color: white;
                padding: 8px;
            }

            .loccolum{
                display: table-cell;
                padding: 0px 8px 0px 0px;

            }
            .loccenter{
                display: table;
                padding: 0px 0px 0px 20px;
            }


        </style>
    </head>
    <body>
        <%@include file="locGUI/locheader.jsp" %>

        <div class="row-fluid text-center">
            <%@include file="locGUI/locmenu.jsp" %>
            <div class="col-sm-9 col-md-9">
                <h1 class="page-header">
                    Edit Supplier <small></small> <!--Change here for title of the page-->
                </h1>
            </div>

        </div>
        <div class="row-fluid text-left">
            <div class="col-md-1 col-sm-1"></div>
            <div class="col-sm-6 col-md-6">
                <form style="padding:20px" action="UpdateSupplierServlet" method="post">
                    <!--IMPORTANT!!! EDIT ZONE START FROM HERE - Do not change anything else-->
                    <!--I am not responsible any wrong if you change anything without permission-->
                    <!--FOR THE ENGLISH TRANSLATE - GOOGLE PLZ ^_^-->

                    <!--copy form input here...-->
                    <!--input 1-->

                    <div class="form-group">
                        <label>Supplier ID</label>
                        <input class="form-control" value="${requestScope.SupplierID}" name="txtSupplierID" id="txtSupplierID" readonly="true" />

                    </div>
                    <div class="form-group">
                        <label>Supplier Name</label>
                        <input class="form-control" value="${requestScope.SupplierName}" name="txtSupplierName" id="txtSupplierName">
                        <p style="color: red" id="error"></p>
                    </div>

                    <div class="form-group">
                        <label>Status</label>
                        <select class="form-control" name="select">
                            <option  <loc:if test="${requestScope.UStatus eq 'A'}">selected="true"</loc:if> >Action</option>
                            <option  <loc:if test="${requestScope.UStatus eq 'I'}">selected="true"</loc:if>>Inactive</option>
                            </select>

                        </div>

                        <div style="text-align: center; padding:0px 0px 100px 0px">
                            <input type="submit" class="btn btn-default" value="Update" name="action" id="btnUpdate"/> 
                        </div>

                        <script language="javascript">
                            $('#btnUpdate').click(function() {
                                if ($('#txtSupplierName') === "")
                                {
                                    $('#error').text("Supplier not blank");
                                    return false;
                                }
                            });
                        </script>
                    </form>

                </div>
                <div class="col-md-1 col-sm-1"></div>
            </div>	

        <%@include file="locGUI/locfooter.jsp" %>
    </body>
</html>
