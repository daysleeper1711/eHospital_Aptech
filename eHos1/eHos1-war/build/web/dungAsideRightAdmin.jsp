        <!-- aside left -->
        <div class="row-fluid text-center">
            <div class="col-sm-3 col-md-3" style="margin-top: 100px">
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                    <span>Surgery Menu</span></a> <!--Change here for the outside menu content-->
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse">
                            <div class="panel-body">
                                <table class="table">
                                    <tr>
                                        <td>
                                            <a href="dungInsertLevelSurgery.jsp">Insert Level Surgery</a> <!--Change here for the sub menu content-->
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="dungLevelSurgeryManagerServlet">Level Surgery Manager</a>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Category</a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse">
                            <div class="panel-body">
                                <table class="table">
                                    <tr>
                                        <td>
                                            <a href="dungInsertCategorySurgery.jsp">Insert Category</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="dungCategoryManagerServlet">Category Manager</a>
                                        </td>
                                    </tr>                                   
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Surgery Service</a>
                            </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse">
                            <div class="panel-body">
                                <table class="table">
                                    <tr>
                                        <td>
                                            <a href="dungAddSurgeryServiceServlet">Insert Surgery Service</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="dungSurgeryServiceManagerServlet">Surgery Service Manager</a>
                                        </td>
                                    </tr>                                    
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapsefour">Position Consultation</a>
                            </h4>
                        </div>
                        <div id="collapsefour" class="panel-collapse collapse">
                            <div class="panel-body">
                                <table class="table">
                                    <tr>
                                        <td>
                                            <a href="dungAddPositionConsultationServlet">Insert Position Consultation</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="dungPositionConsulationManagerServlet">Position Consultation Manager</a>
                                        </td>
                                    </tr>                                    
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapsefive">Position Doctor</a>
                            </h4>
                        </div>
                        <div id="collapsefive" class="panel-collapse collapse">
                            <div class="panel-body">
                                <table class="table">
                                    <tr>
                                        <td>
                                            <a href="dungAddPositionDoctorServlet">Insert Position Doctor</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="dungPositionDoctorManagerServlet">Position Doctor Manager</a>
                                        </td>
                                    </tr>                                    
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-sm-9 col-md-9">
                <h2 class="page-header">
                    ${titlePage}
                </h2>
            </div>
        </div>
        <!-- end #sidebar -->

