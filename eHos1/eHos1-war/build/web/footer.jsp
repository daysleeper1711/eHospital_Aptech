<!--css for footer-->
<style>
    html {
        position: relative;
        min-height: 100%;
    }
    body {
        /* Margin bottom by footer height */
        margin-bottom: 20px;
    }
    footer {
        position: absolute;
        bottom: 0;
        width: 100%;
        /* Set the fixed height of the footer here */
        height: 20px;
        background-color: #ffffff;
    }
</style>

<footer>
    <hr/>
    <div class="container" style="text-align: center;">
        <marquee direction="right" scrollamount="7" onmouseover="this.stop();" onmouseout="this.start();">
            <p class="text-muted" style="color: black">
                    <img src="./locGUI/logo.jpg" class="img-responsive" style="opacity:0.6;width:100px"/>
                    <b>Copyright � 2016 by Team eHopital</b>
            </p>
        </marquee> 
    </div>
</footer>
<!--<footer class="row-fluid">
    <div class="col-md12 col-sm12" style="padding-left:20px;background-color:#e3e7ed">
        <p class="text-muted">Copyright <i class="fa fa-copyright"></i> <strong>F2-1504-T1</strong></p>
    </div>
</footer>-->
</body>
</html>