<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="loc" %>

<!DOCTYPE html>

<html lang="en">

    <head>
        <title>Add New ItemCode</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- jQuery -->
        <script language="javascript" src="js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>
        <style>    
            /* Set black background color, white text and some padding */
            footer {
                background-color: #555;
                color: white;
                padding: 8px;
            }

            .loccolum{
                display: table-cell;
                padding: 20px;
                width: 40%;
            }
            .loccenter{
                display: table;
                margin: auto;
            }


        </style>
    </head>
    <body>
        <%@include file="locGUI/locheader.jsp" %>

        <div class="row-fluid text-center">
            <%@include file="locGUI/locmenu.jsp" %>
            <div class="col-sm-9 col-md-9">
                <h1 class="page-header">
                    <small>Add New ItemCode</small> <!--Change here for title of the page-->
                </h1>
            </div>

        </div>
        <div class="row-fluid text-left">
            <div class="col-md-1 col-sm-1"></div>
            <div class="col-sm-6 col-md-6">
                <form style="padding:20px" action="AddItemCodeServlet" method="post">
                    <input type="hidden" name="txtUserID" value="${sessionScope.empInfo.employeeID}"/>
                    <input type="hidden" name="txtUserName" value="${sessionScope.empInfo.empName}"/>
                    <!--IMPORTANT!!! EDIT ZONE START FROM HERE - Do not change anything else-->
                    <!--I am not responsible any wrong if you change anything without permission-->
                    <!--FOR THE ENGLISH TRANSLATE - GOOGLE PLZ ^_^-->

                    <!--copy form input here...-->
                    <!--input 1-->
                    <div class="loccenter" >
                        <!--Left-->
                        <div class="loccolum">

                            <div class="form-group">
                                <label>ItemName</label>
                                <input class="form-control" name="txtItemName" id="txtItemName">
                                <p style="color: red" id="errortxtItemName"></p>
                            </div>

                            <div class="form-group input-group">
                                <label>Content</label>
                                <input class="form-control" name="txtContent" id="txtContent">
                                <p style="color: red" id="errortxtContent"></p>
                            </div>

                            <div class="form-group">
                                <label>Type</label>
                                <select class="form-control" name="selectType">
                                    <option>Drugs</option>
                                    <option>Supplies</option>
                                </select>
                            </div>



                            <div class="form-group">
                                <label>How To Use</label>
                                <select class="form-control" name="selectHowToUse">
                                    <option>Tablet</option> <!--Uong-->
                                    <option>Injectable</option><!--Tiem-->
                                    <option>Suppository</option><!--Dat-->
                                    <option>Powder Injectable</option> <!--bot tiem-->
                                    <option>Topical Cream</option><!--thoa ngoai-->
                                    <option>Perfusion</option><!--Truyen-->
                                    <option>Sublingual</option><!--Ngam-->
                                    <option>Modified Release Tablet</option><!--Vien tac dung cham-->
                                </select>

                            </div>

                        </div>

                        <!--Right-->
                        <div class="loccolum">

                            <div class="form-group">
                                <label>GenericDrug</label>
                                <input class="form-control" name="txtGenericDrug" id="txtGenericDrug">
                                <p style="color: red" id="errortxtGenericDrug"></p>
                            </div>

                            <div class="form-group">
                                <label>Unit</label>
                                <select class="form-control" name="txtUnit" id="txtUnit">
                                    <option>Phial</option><!--lo-ong-->
                                    <option>Medicine bag</option><!--Tui-->
                                    <option>Pill</option><!--Vien-->
                                    <option>Bottle</option><!--Chai-->
                                </select>

                            </div>

                            <div class="form-group">
                                <label>Price</label>
                                <input class="form-control" type="number" name="txtPrice" id="txtPrice">
                                <p style="color: red" id="errortxtPrice"></p>   
                            </div>



                            <div class="form-group">
                                <label>Nation</label>
                                <select name="country" class="form-control"> 
                                    <optgroup label="Asia" value="Asia">
                                        <option>VietNam</option>
                                        <option>ThaiLan</option>
                                        <option>Singapore</option>
                                    </optgroup>
                                    <optgroup  label="America" value="America">
                                        <option>USA</option> 
                                        <option>Canada</option>
                                        <option>Mexico</option>
                                    </optgroup>
                                </select>

                            </div>

                        </div>

                    </div>

                    <div style="text-align: center; padding:0px 0px 100px 0px">
                        <button type="submit" class="btn btn-default" name="action" id="btnAddNewItemCode" value="Add New ItemCode">Add New ItemCode</button> 
                    </div>

                </form>

                <script language="javascript">
                    $(document).ready(function()
                    {
                        $('#btnAddNewItemCode').click(function()
                        {
                            $('#errortxtItemName').text("");
                            $('#errortxtGenericDrug').text("");
                            $('#errortxtContent').text("");
                            $('#errortxtPrice').text("");

                            if ($('#txtItemName').val() === "")
                            {
                                $('#errortxtItemName').text("ItemName not blank");
                                return false;
                            } else if ($('#txtGenericDrug').val() === "")
                            {
                                $('#errortxtGenericDrug').text("GenericDrug not blank");
                                return false;
                            }
                            else if ($('#txtContent').val() === "")
                            {
                                $('#errortxtContent').text("Content not blank");
                                return false;
                            }
                            else if ($('#txtPrice').val() === "")
                            {
                                $('#errortxtPrice').text("Price not blank");
                                return false;
                            }

                            var price = parseInt($('#txtPrice').val());
                            if (price < 1)
                            {
                                $('#errortxtPrice').text("Price is number bigger than 0");
                                return false;
                            }
                        });
                    });
                </script>



            </div>
            <div class="col-md-1 col-sm-1"></div>
        </div>	

        <%@include file="locGUI/locfooter.jsp" %>
    </body>
</html>
