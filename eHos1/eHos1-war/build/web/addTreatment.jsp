<%@include file="header.jsp" %>
<div class="col-sm-9 col-md-9 text-left">
    <h1 class="page-header">
        Daily Treatment <small>${sessionScope.empInfo.faculty} - ${sessionScope.empInfo.position}</small>
    </h1>
</div>
</div>
<div class="row-fluid text-left" >
    <div class="col-sm-8 col-md-8">
        <h3>Medical Record info</h3>
        <div class="table-responsive">
            <table class="showInfo">
                <tr>
                    <th>Medical Record ID:</th>
                    <td>${requestScope.info.medRecordID}</td>
                    <th>Date in:</th>
                    <td>${requestScope.info.dateInHospital}</td>
                    <th>Bed:</th>
                    <td>${requestScope.info.bedInfo.bedName}</td>
                </tr>
            </table>
        </div>
        <!--Show form-->
        <h3>Info details</h3>
        <div class="table-responsive">
            <table class="showInfo">
                <tr>
                    <th>Patient ID:</th>
                    <td colspan="5">${requestScope.info.patInfo.patID}</td>
                </tr>
                <tr>
                    <th>Name:</th>
                    <td>${requestScope.info.patInfo.patName}</td>
                    <th>Date of birth:</th>
                    <td>${requestScope.info.patInfo.dob}</td>
                    <th>Gender:</th>
                    <td>${requestScope.info.patInfo.gender}</td>
                </tr>
            </table>
        </div>
        <!--end show...-->
        <h3>Request treatment</h3>

        <form action="listAllPrescriptionMedSupp" method="post">
            <input type="hidden" name="MedRCID" value="${requestScope.info.medRecordID}"/>
            <button type="submit" class="btn btn-default">
                Prescription <i class="fa fa-angle-double-right" aria-hidden="true"></i>
            </button>
        </form>
        <!--form of tuyet-->
        <form action="TuyetSearchReqServlet" method="post">
            <input type="hidden" name="txtSearchAddID" value="${requestScope.info.medRecordID}"/>
            <input type="hidden" name="optionsRadiosInline" value="inpatient"/>
            <button type="submit" name class="btn btn-default">
                Request subclinical test <i class="fa fa-angle-double-right" aria-hidden="true"></i>
            </button>
        </form>
        <form action="transformTuyetPhuongDungServlet" method="post" >
            <div class="form-group">
                <input class="form-control" name="txtMedicalRecordID" type="hidden" value="${requestScope.info.medRecordID}">
            </div>
            <button type="submit" class="btn btn-default" name="action" value="requestsurgery">
                Request surgery <i class="fa fa-angle-double-right" aria-hidden="true"></i>
            </button>
        </form>
    </div>
</div>	
<%@include file="footer.jsp" %>
