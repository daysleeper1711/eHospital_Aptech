<%-- 
    Document   : phuongManagementSupp
    Created on : Jan 19, 2017, 12:11:39 AM
    Author     : Nguyet Phuong
--%>

<%@include file="header.jsp" %>
<div class="col-sm-9 col-md-9">
    <h1 class="page-header">
        Search Medicine Supplies By Medical Record <!--Change here for title of the page-->
    </h1>
</div>
</div>
<div class="row-fluid text-left">
    <div class="col-md-1 col-sm-1"></div>
    <div class="col-sm-6 col-md-6">
        <div class="row">
            <form action="MedSuppliesManagement" method="post" id="inputForm">
                <div class="form-group">
                    <div class="col-md-3 col-sm-3">Medical Record ID : </div>
                    <div class="col-md-4 col-sm-4"><input type="text" name="txtMedRCID" class="form-control" data-parsley-required data-parsley-minlength="13" data-parsley-maxlength="18"/></div> 
                </div>
                <div class="col-md-4 col-sm-4"><button type="submit" name="action" value="Search" class="btn btn-default"><i class="fa fa-search" style="color: blue;"></i> Search</button></div>
            </form>
        </div>
        <c:if test="${requestScope.ERRO ne null}">
            <div class="row col-md-12 col-sm-12 col-xs-12 col-lg-12"><h3 style="color: red;"><i>${requestScope.ERRO}</i></h3></div>
                    </c:if>
                    <c:if test="${requestScope.ERRO eq null}">
            <hr/>
            <div class="row" style="padding-top: 20px;">
                <div class="col-md-5"><strong>Medical record :</strong> ${sessionScope.MEDID.medRecordID}</div>
                <div class="col-md-7 text-right">
                    <a href="PhuongControllerAdd?action=AddNewMedSupp" class="btn btn-default"><i class="fa fa-plus" style="color: green;"></i> Add new medicine supplies</a>
                </div>
            </div>
            <div class="row" style="padding-top: 20px;">
                <div class="col-md-6"><strong>Patient name:</strong> ${sessionScope.MEDID.patInfo.patName}</div>
                <div class="col-md-4"><strong>Birthday:</strong> ${sessionScope.MEDID.patInfo.dob}</div>
                <div class="col-md-2"><strong>Sex:</strong> ${sessionScope.MEDID.patInfo.gender}</div>
            </div>
            <div class="row" style="padding-top: 20px;">
                <div class="col-md-3"><strong>Date in:</strong> ${sessionScope.MEDID.dateInHospital}</div>
                <div class="col-md-3"><strong>Date out:</strong> ${sessionScope.MEDID.dateOutHospital}</div>
                <div class="col-md-4"><strong>Faculty:</strong> ${sessionScope.MEDID.currentFaculty}</div>
                <div class="col-md-2"><strong>Bed:</strong> ${sessionScope.MEDID.currentBed}</div>
            </div>
            <div class="row" style="padding-top: 20px;">
                <div class="col-md-12"><strong>Delitescence:</strong> <span style="max-width: 400px;">${sessionScope.MEDID.symptoms}</span></div>
            </div>
            <!--IMPORTANT!!! EDIT ZONE START FROM HERE - Do not change anything else-->
            <!--I am not responsible any wrong if you change anything without permission-->
            <!--FOR THE ENGLISH TRANSLATE - GOOGLE PLZ ^_^-->
            <!--table here-->
            <div class="table-responsive" style="padding-top: 20px;">
                <table class="table table-bordered table-hover table-striped">
                    <thead>
                        <tr>
                            <th>Date create</th>
                            <th>Create for date</th>
                            <th>Employee Name</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody id="itemContainer">
                        <c:forEach items="${requestScope.LISTPRESS}" var="lst">
                            <tr>
                                <td>${lst.dateCreate}</td>
                                <td>${lst.createFDate}</td>
                                <td>${lst.employeeName}</td>
                                <td>${lst.status}</td>
                                <td>
                                    <form action="AdminListSuppController" method="post">
                                        <input type="hidden" name="txtSuppID" value="${lst.suppID}"/>
                                        <button type="submit" name="action" value="DetailSupp" class="btn btn-default"><i class="fa fa-info-circle" style="color: blue;"></i> Detail</button>
                                        <button type="submit" name="action" value="EditSupp" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</button>
                                        <c:if test="${lst.status ne 'Gived medicine'}">
                                            <input type="hidden" name="txtMedRCID" value="${sessionScope.MEDID.medRecordID}"/>
                                            <button type="submit" name="action" value="DeleteSupp" class="btn btn-danger"><i class="fa fa-times"></i> Delete</button>
                                        </c:if>
                                    </form>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
            <div class="holder"></div>
        </c:if>
    </div>
    <div class="col-md-1 col-sm-1"></div>
</div>	
<script type="text/javascript">
    var parsleyOptions = {
        successClass: "has-success",
        errorClass: "has-error",
        classHandler: function(e) {
            return e.$element.closest(".form-group");
        },
        errorsWrapper: '<span class="center-block text-danger" style="padding-left: 10px; padding-top: 5px"></span>',
        errorTemplate: '<p></p>'
    };
    $("#inputForm").parsley(parsleyOptions);
</script>
<%@include file="footer.jsp" %>