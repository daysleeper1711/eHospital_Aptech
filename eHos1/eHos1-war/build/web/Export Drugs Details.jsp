<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="loc" %>

<!DOCTYPE html>

<html >
    <head>
        <title>View Export Drugs Details</title>
        <meta charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- jQuery -->
        <script language="javascript" src="js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>
        <style>    
            /* Set black background color, white text and some padding */
            footer {
                background-color: #555;
                color: white;
                padding: 8px;
            }

            .loccolum{
                display: table-cell;
                padding: 0px 8px 0px 8px;

            }
            .loccenter{
                display: table;
                padding: 0px 0px 0px 20px;
                text-align: center;
                width: 100%;
            }


        </style>
    </head>
    <body>
        <%@include file="locGUI/locheader.jsp" %>

        <div class="row-fluid text-center">
            <%@include file="locGUI/locmenu.jsp" %>
            <div class="col-sm-9 col-md-9">
                <h1 class="page-header">
                    View Export Drugs Details <small></small> <!--Change here for title of the page-->
                </h1>
            </div>

        </div>
        <div class="row-fluid text-left">
            <div class="col-md-1 col-sm-1"></div>
            <div class="col-sm-6 col-md-6">
                <form style="padding:20px">

                    <!--IMPORTANT!!! EDIT ZONE START FROM HERE - Do not change anything else-->
                    <!--I am not responsible any wrong if you change anything without permission-->
                    <!--FOR THE ENGLISH TRANSLATE - GOOGLE PLZ ^_^-->

                    <!--copy form input here...-->
                    <!--input 1-->

                    

                    <div class="loccenter">
                        <!--Left-->

                        <div class="loccolum">
                            <div class="form-group">
                                <label>Docentry:</label> ${requestScope.header.docentry}

                            </div>
                            <div class="form-group">
                                <label>Type:</label> ${requestScope.header_type}

                            </div>
                        </div>
                        <div class="loccolum">                          
                            <div class="form-group">
                                <label>Create Date:</label> ${requestScope.header_sfromdate}
                            </div>
                            <div class="form-group">
                                <label>Update Date:</label> ${requestScope.header_stodate}
                            </div>

                        </div>
                        <div class="loccolum">
                            <div class="form-group">
                                <label>Status:</label> <samp id="Status">${requestScope.header_status}</samp> 
                            </div>
                        </div>
                    </div><!--End div Test-->

                    <div class="form-group">
                        <label>Note</label>
                        <textarea class="form-control" rows="3" id="disabledInput" type="text"  disabled >${requestScope.header.note}</textarea>
                    </div>

                </form>


                <h2>List Result Export Drugs Details</h2>
                <div class="table-responsive" >
                    <table class="table table-bordered table-hover table-striped" >
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>ItemCode</th>
                                <th>ItemName</th>
                                <th>Package</th>
                                <th>War_Date</th>
                                <th>Quantity</th>
                                <th>X_Price</th>
                            </tr>
                        </thead>
                        <tbody>
                            <loc:forEach items="${requestScope.listdetails}"  var="dto" varStatus="stt">
                                <tr>
                                    <td>${stt.count}</td>
                                    <td>${dto.itemCode}</td>
                                    <td>
                                        <loc:forEach items="${requestScope.listITEMCODE}"  var="dto1">
                                            <loc:if test="${dto1.itemCode eq dto.itemCode}">${dto1.itemName}</loc:if>
                                        </loc:forEach>
                                    </td>
                                    <td>${dto.package1}</td>
                                    <td>${dto.exDetails}</td>
                                    <td>${dto.quantity}</td>
                                    <td>${dto.XPrice}</td>
                                </tr>
                            </loc:forEach>
                        </tbody>
                    </table>
                </div>
                <div style="text-align: center;padding: 20px" >
                    <form action="FinishExportDrugsServlet" method="post">                        
                        <input type="hidden" name="txtUserID" value="${sessionScope.empInfo.employeeID}"/>
                        <input type="hidden" name="txtUserName" value="${sessionScope.empInfo.empName}"/>
                        
                        <button type="submit" id="btnFinish" class="btn btn-default">Finish</button>
                        <input type="hidden" name="txtEXPO" value="${requestScope.header.exID}"/>
                    </form>

                </div>

                <script language="javascript">
                    //Check chung tu da hoan tat de disble nut Finish
                    $(document).ready(function()
                    {
                        if ($('#Status').text() === "Finish")
                        {
                            $('#btnFinish').prop("disabled", true);
                        }
                        else
                        {
                            $('#btnFinish').prop("disabled", false);
                        }
                    });
                </script>
            </div>


        </div>	

        <%@include file="locGUI/locfooter.jsp" %>
    </body>
</html>
