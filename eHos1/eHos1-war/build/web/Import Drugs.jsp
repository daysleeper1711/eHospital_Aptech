
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="loc" %>
<!DOCTYPE html>

<html >
    <head>
        <title>Import Drugs</title>
        <meta charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">


        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- jQuery -->
        <script language="javascript" src="js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>
        <style>    
            /* Set black background color, white text and some padding */
            footer {
                background-color: #555;
                color: white;
                padding: 8px;
            }

            .loccolum{
                display: table-cell;
                padding: 0px 8px 0px 0px;

            }
            .loccenter{
                display: table;
                padding: 0px 0px 0px 20px;
            }


        </style>
    </head>
    <body>
        <%@include file="locGUI/locheader.jsp" %>

        <div class="row-fluid text-center">
            <%@include file="locGUI/locmenu.jsp" %>
            <div class="col-sm-9 col-md-9">
                <h1 class="page-header">
                    Import Drugs Management <small></small> <!--Change here for title of the page-->
                </h1>
            </div>

        </div>
        <div class="row-fluid text-left">
            <div class="col-md-1 col-sm-1"></div>
            <div class="col-sm-6 col-md-6">
                <form action="ImportDrugsServlet" method="post">

                    <input type="hidden" name="txtUserID" value="${sessionScope.empInfo.employeeID}"/>
                    <input type="hidden" name="txtUserName" value="${sessionScope.empInfo.empName}"/>
                    <!--IMPORTANT!!! EDIT ZONE START FROM HERE - Do not change anything else-->
                    <!--I am not responsible any wrong if you change anything without permission-->
                    <!--FOR THE ENGLISH TRANSLATE - GOOGLE PLZ ^_^-->

                    <!--copy form input here...-->
                    <!--input 1-->


                    <table style="width: 100%">
                        <tr>
                            <td style="width: 200px;">
                                <div class="form-group" style="padding: 0 8px 0 0; ">
                                    <label>SymBill</label>
                                    <input class="form-control" name="txtSymBill" id="txtSymBill"/>
                                </div>
                            </td > 
                            <td style="width: 200px;">
                                <div class="form-group" style="padding: 0 8px 0 0;">
                                    <label>NumBill</label>
                                    <input class="form-control" name="txtNumBill" id="txtNumBill"/>
                                </div>
                            </td>

                            <td style="width: 200px;">
                                <div class="form-group" style="padding: 0 8px 0 0;">
                                    <label>SupplierName</label>
                                    <select class="form-control" name="select">
                                        <loc:forEach items="${requestScope.list}" var="dto">
                                            <option value="${dto.supplierID}">${dto.supplierName}</option>
                                        </loc:forEach>
                                    </select>
                                </div>

                            </td>

                        </tr>
                        <tr>
                            <td colspan="3">
                                <div class="form-group">
                                    <label>Note</label>
                                    <textarea class="form-control" rows="3" id="disabledInput" type="text" name="txtnote"></textarea>
                                </div>
                            </td>
                        </tr>
                    </table>

                    <div class="alert alert-error" id="alert">
                        <strong style="color: red;"></strong>
                    </div><b/>

                    <INPUT type="button" value="Add Row" id="btnadd" class="btn btn-default"/>
                    <INPUT type="submit" value="Import Drugs" class="btn btn-default" id="btnImport" disabled/>

                    <h2>Information</h2>
                    <div class="table-responsive" >
                        <table class="table table-bordered table-hover table-striped" id="myTable">
                            <thead>
                            <th>ItemCode</th>
                            <th>Quantity</th>
                            <th>N_Price</th>
                            <th>War_Date</th>
                            <th>Action</th>
                            </thead>
                            <tbody>

                            </tbody>

                        </table>


                    </div>
                    <input type="hidden" id="txtDataImport" name="txtDataImport"/>

                    <script language="javascript">

                        $(document).ready(function() {
                            //Delete dong
                            $("#myTable").on('click', '#btnDelete', function() {
                                $(this).closest('tr').remove();
                            });

                            //Chan de trong


                            //Add them dong
                            $('#btnadd').click(function()
                            {
                                $('#myTable tr:last').after('<tr>\n\
                                <td><select class="form-control" name="select">\n\
                        <loc:forEach items="${requestScope.listITEMCODE}" var="dto">\n\
                                            <option value="${dto.itemCode}">${dto.itemCode} - ${dto.itemName}</option>\n\
                        </loc:forEach>\n\
                                    </select>\n\
                                <td>\n\
                                <input class="form-control" type="number" id="txtquantity"/>\n\
                                </td>\n\
                                <td>\n\
                                <input class="form-control" type="number" id="txtprice"/>\n\
                                </td>\n\
                                <td>\n\
                                    <input type="text" id="wardate" class="form-control" placeholder="yyyy-mm-dd"/>\n\
                                </td>\n\
                                <td><button id="btnDelete" class="btn btn-danger">Delete</button></td>\n\
                                </tr>');

                                $('#btnImport').prop("disabled", false);
                            });

                            $("#btnImport").click(function() {

                                //Chan blank
                                if ($('#txtSymBill').val() === "")
                                {
                                    $('#alert').show().find('strong').text('Error: SymBill not blank');
                                    return false;
                                }

                                if ($('#txtNumBill').val() === "")
                                {
                                    $('#alert').show().find('strong').text('Error: NumBill not blank');
                                    return false;
                                }

                                var n = 0;
                                var data = "";
                                var checkitemcode = [];
                                var checkblank = true;
                                $('#myTable tr').each(function() {
                                    if (n !== 0)
                                    {
                                        var item1 = $(this).find("td:eq(0) select").val();
                                        var item2 = $(this).find("td:eq(1) input[type='number']").val();
                                        var item3 = $(this).find("td:eq(2) input[type='number']").val();
                                        var item4 = $(this).find("td:eq(3) input[type='text']").val();

                                        //Check trung
                                        checkitemcode.forEach(function(e) {
                                            var text = e;
                                            if (text === item1)
                                            {
                                                $('#alert').show().find('strong').text('Error: ItemCode must different');
                                                checkblank = false;
                                            }
                                        });


                                        //Chan Blank
                                        if (item1 === "")
                                        {
                                            $('#alert').show().find('strong').text('Error: ItemCode not blank');
                                            checkblank = false;

                                        }
                                        if (item2 === "")
                                        {
                                            $('#alert').show().find('strong').text('Error: Quantity not blank');
                                            checkblank = false;

                                        }
                                        if (item3 === "")
                                        {
                                            $('#alert').show().find('strong').text('Error: Price not blank');
                                            checkblank = false;

                                        }
                                        if (item4 === "")
                                        {
                                            $('#alert').show().find('strong').text('Error: War_Date not blank');
                                            checkblank = false;

                                        }

                                        //Check > 0
                                        var num1 = parseInt(item2);
                                        var num2 = parseInt(item3);
                                        if (num1 < 1)
                                        {
                                            $('#alert').show().find('strong').text("Quantity is number bigger than 0");
                                            checkblank = false;

                                        }
                                        if (num2 < 1)
                                        {
                                            $('#alert').show().find('strong').text("Price is number bigger than 0");
                                            checkblank = false;

                                        }

                                        if (!checkblank)
                                        {
                                            return false;
                                        }

                                        var abc = item1 + "," + item2 + "," + item3 + "," + item4;
                                        data = data + abc + ";";
                                        checkitemcode.push(item1);

                                    }
                                    else
                                    {
                                        n++;
                                    }
                                });

                                if (!checkblank)
                                {
                                    return false;
                                }

                                //Bo vao control tam
                                $('#txtDataImport').val(data);

                            });
                        });

                    </script>
                </form>

            </div>
            <div class="col-md-1 col-sm-1"></div>
        </div>	

        <%@include file="locGUI/locfooter.jsp" %>
    </body>
</html>
