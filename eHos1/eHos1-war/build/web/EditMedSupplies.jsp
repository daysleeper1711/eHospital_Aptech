<%-- 
    Document   : EditPrescription
    Created on : Jan 8, 2017, 1:51:33 PM
    Author     : Nguyet Phuong
--%>
<%@include file="header.jsp" %>
<div class="col-sm-9 col-md-9">
    <h1 class="page-header">
        Update Medicine Supplies <!--Change here for title of the page-->
    </h1>
</div>
</div>
<div class="row-fluid text-left">
    <div class="col-md-1 col-sm-1"></div>
    <div class="col-sm-6 col-md-6">
        <div class="row">
            <div class="col-md-7"><strong>Medical record ID :</strong> ${sessionScope.MEDID.medRecordID}</div>
            <div class="col-md-5"><strong>Patient name:</strong> ${sessionScope.MEDID.patInfo.patName}</div>
        </div>
        <div class="row" style="padding-top: 20px;">
            <div class="col-md-4"><strong>Birthday:</strong> ${sessionScope.MEDID.patInfo.dob}</div>
            <div class="col-md-4"><strong>Sex:</strong> ${sessionScope.MEDID.patInfo.gender}</div>
            <div class="col-md-4 text-center"><strong>Doctor :</strong> ${requestScope.EDITSUPP.supp.employeeID.fullname}</div>
        </div>
        <div class="row" style="padding-top: 20px;">
            <div class="col-md-3"><strong>Date in:</strong> ${sessionScope.MEDID.dateInHospital}</div>
            <div class="col-md-4"><strong>Faculty:</strong> ${sessionScope.MEDID.currentFaculty}</div>
            <div class="col-md-2"><strong>Bed:</strong> ${sessionScope.MEDID.currentBed}</div>
        </div>
        <div class="row" style="padding-top: 20px;">
            <div class="col-md-12"><strong>Delitescence:</strong> <span style="max-width: 500px;">${sessionScope.MEDID.symptoms}</span></div>
        </div>
        <form action="PhuongControllerEdit" method="post">
            <input type="hidden" name="txtSuppID" value="${requestScope.EDITSUPP.supp.medicineSuppliesID}"/>
            <div class="row" style="padding-top: 20px;">
                <div class="col-md-3">
                    <strong>Medicine Name: </strong>
                </div>
                <div class="col-md-9">
                    <select name="lstMediID" class="js-example-basic-single">
                        <c:forEach items="${requestScope.LISTMED}" var="dto">
                            <option value="${dto.id}">${dto.name}</option>
                        </c:forEach>                          
                    </select>
                </div>
                <div class="col-md-3 form-group"><strong>Quantity:</strong> <input type="number" min="0" max="10" name="txtQuantity" style="width: 50px;"/>  </div>
            </div>
            <div class="row" style="padding-top: 20px;">
                <div class="col-md-6">
                    <strong>Create for date:</strong> ${requestScope.DATE}
                </div>
                <!--<div class="col-md-3"><strong>Quantity:</strong> <input type="number" readonly value="5" style="width: 50px;"/> </div>-->
            </div>
            <div class="row" style="padding-top: 20px;">
                <div class="col-md-5">
                </div>
                <div class="col-md-3"></div>
                <div class="col-md-4 text-right">
                    <input type="hidden" name="medRC" value="${sessionScope.MEDID.medRecordID}"/>
                    <button type="submit" name="action"  value="InsertSupp" class="btn btn-info"><i class="fa fa-download"></i> Insert</button>
                </div>
            </div>
        </form>
        <c:if test="${requestScope.ERROR ne null}"><span style="color: red;"> ${requestScope.ERROR}</span></c:if>
            <div class="row" style="padding-top: 20px;">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                                <th>Medicine Name</th>
                                <th>Quantity</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <form id="inputForm"></form>
                        <c:forEach items="${requestScope.EDITSUPP.lstSuppDeta}" var="dto">
                        <form action="PhuongControllerEdit" method="post" id="inputForm">
                            <tr>
                                <td>${dto.medicineID.itemName}, ${dto.medicineID.nation}</td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" value="${dto.quantity}" min="0" max="10"  name="txtQ" style="width: 50px;"/>
                                    </div>
                                </td>
                                <td>
                                    <input type="hidden" name="txtSuppDetaID" value="${dto.medicineSuppliesDetailID}"/>
                                    <input type="hidden" name="txtMedID" value="${dto.medicineID.itemCode}"/>
                                    <input type="hidden" name="txtSuppID" value="${requestScope.EDITSUPP.supp.medicineSuppliesID}"/>
                                    <button type="submit" name="action" value="UpdateSupp" class="btn btn-success"><i class="fa fa-refresh"></i> Update</button>
                                    <button type="submit" name="action" value="DeleteSupp" class="btn btn-danger"><i class="fa fa-times"></i> Delete</button>
                                </td>
                            </tr>
                        </form>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row" style="padding-bottom: 30px;">
            <div class="col-md-4"></div>
            <div class="col-md-4 text-center">
                <form action="PhuongControllerEdit" method="post">
                    <input type="hidden" name="MedRCID" value="${sessionScope.MEDID.medRecordID}"/>
                    <input type="hidden" name="txtMedRCID" value="${sessionScope.MEDID.medRecordID}"/>
                    <button type="submit" value="UpdateMedicineSupplies" name="action" class="btn btn-primary"><i class="fa fa-check"></i> Ok</button>
                </form>
            </div>
            <div class="col-md-4"></div>
        </div>
        <!-- end table here-->
        <!--IMPORTANT!!! EDIT ZONE END FROM HERE - Do not change anything else-->
    </div>
    <div class="col-md-1 col-sm-1"></div>
</div>
<script type="text/javascript">
    var parsleyOptions = {
        successClass: "has-success",
        errorClass: "has-error",
        classHandler: function(e) {
            return e.$element.closest(".form-group");
        },
        errorsWrapper: '<span class="center-block text-danger" style="padding-left: 10px; padding-top: 5px"></span>',
        errorTemplate: '<p></p>'
    };
    $("#inputForm").parsley(parsleyOptions);

</script>  
<%@include file="footer.jsp" %>