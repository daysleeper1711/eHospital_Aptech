<header>
    <div class="container-fluid" style="z-index:4">
        <div class="col-sm-3 col-md-3">
            <img src="images/logo.jpg" class="img-responsive" style="opacity:0.6;width:100px"/>
        </div>
        <div class="col-sm-9 col-md-9 text-right lead">
            <i class="fa fa-user-circle" aria-hidden="true"></i>
            Welcome, <strong>${sessionScope.empInfo.empName}</strong> <a href="LogoutProcess">logout</a>
        </div>
    </div>
</header>