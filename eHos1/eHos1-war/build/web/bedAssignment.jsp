<%@include file="header.jsp" %>
<div class="col-sm-9 col-md-9 text-left">
    <h1 class="page-header">
        Bed Assignment <small>${sessionScope.empInfo.faculty} - ${sessionScope.empInfo.position}</small>
    </h1>
</div>
</div>
<div class="row-fluid text-left">
    <div class="col-sm-8 col-md-8">
        <!--Load button-->
        <form style="padding:20px" id="loadForm" 
              action="BedAssignmentMainProcess" method="post">
            <button class="btn btn-default" type="submit" form="loadForm"
                    name="btnSubmit" value="Load waiting for bed"/>
            Load<i class="fa fa-download" aria-hidden="true"></i>
            </button>			
        </form>
        <c:if test="${requestScope.success ne null}">
            <div class="alert alert-success">
                <strong>Success assign bed</strong> 
            </div>
        </c:if>
        <c:if test="${requestScope.fail ne null }">
            <div class="alert alert-warning">
                <strong>Fails assign bed</strong> 
            </div>
        </c:if>
        <c:if test="${requestScope.listWaitingPatient eq null or empty requestScope.listWaitingPatient}">
            <div class="alert alert-warning">
                <strong>No patient waiting</strong> 
            </div>
        </c:if>
        <c:if test="${requestScope.listPatientInTreatment eq null or empty requestScope.listPatientInTreatment}">
            <div class="alert alert-warning">
                <strong>No patient in faculty</strong> 
            </div>
        </c:if>
        <!--Display list patient's waiting-->
        <c:if test="${requestScope.listWaitingPatient ne null}">
            <c:if test="${not empty requestScope.listWaitingPatient}">
                <h3>List patient waiting for bed</h3>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                                <th>Ord</th>
                                <th>Medical Record ID</th>
                                <th>Name</th>
                                <th>Registry date</th>
                                <th>Diagnosis date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${requestScope.listWaitingPatient}"
                                       var="mr" varStatus="i">
                                <tr>
                                    <td>${i.count}</td>
                                    <td>${mr.medRecordID}</td>
                                    <td>${mr.patInfo.patName}</td>
                                    <td>${mr.createDate}</td>
                                    <td>${mr.diagnosisDate}</td>
                                    <td>
                                        <form action="BedAssignmentMainProcess" method="post">
                                            <button type="submit" class="btn btn-default"
                                                    name="btnSubmit" value="Bed assignment">
                                                Assign <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                                            </button>
                                            <input type="hidden" name="passID" value="${mr.medRecordID}" />
                                        </form>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </c:if>
        </c:if>
        <c:if test="${requestScope.listPatientInTreatment ne null}">
            <c:if test="${not empty requestScope.listWaitingPatient}">
                <div class="table-responsive">
                    <h3>List patient in faculty</h3>
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                                <th>Ord</th>
                                <th>Medical Record ID</th>
                                <th>Name</th>
                                <th>Date in</th>
                                <th>Bed</th>
                                <th>Type</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${requestScope.listPatientInTreatment}"
                                       var="inTreatmentMR" varStatus="i">
                                <tr>
                                    <td>${i.count}</td>
                                    <td>${inTreatmentMR.medRecordID}</td>
                                    <td>${inTreatmentMR.patInfo.patName}</td>
                                    <td>${inTreatmentMR.dateInHospital}</td>
                                    <td>${inTreatmentMR.bedInfo.bedName}</td>
                                    <td>${inTreatmentMR.bedInfo.bedType}</td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </c:if>
        </c:if>
        <!-- end table here-->
        <!--IMPORTANT!!! EDIT ZONE END FROM HERE - Do not change anything else-->
    </div>
</div>	
<%@include file="footer.jsp" %>