        <!-- aside left -->
        <div class="row-fluid text-center">
            <div class="col-sm-3 col-md-3" style="margin-top: 100px">
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a href="DailyTreatmentMainProcess?open=true"><span>Daily Treatment Main Process</span></a>
                            </h4>
                        </div>
                    </div>
                    
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                    <span>Suggest Surgery</span></a> <!--Change here for the outside menu content-->
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse">
                            <div class="panel-body">
                                <table class="table">
                                    <tr>
                                        <td>
                                            <a href="dungAddSuggestSurgeryServlet">Insert Suggest Surgery</a> <!--Change here for the sub menu content-->
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="dungSuggestSurgeryManagerServlet">Suggest Surgery Manager</a>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-9 col-md-9">
                <h2 class="page-header">
                    ${titlePage}
                </h2>
            </div>
        </div>
        <!-- end #sidebar -->

