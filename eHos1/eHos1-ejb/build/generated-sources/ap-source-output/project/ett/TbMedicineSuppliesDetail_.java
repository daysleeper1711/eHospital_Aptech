package project.ett;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import project.ett.TbEmployee;
import project.ett.TbItemCodeManagement;
import project.ett.TbMedicineSupplies;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2017-02-06T16:29:05")
@StaticMetamodel(TbMedicineSuppliesDetail.class)
public class TbMedicineSuppliesDetail_ { 

    public static volatile SingularAttribute<TbMedicineSuppliesDetail, Double> price;
    public static volatile SingularAttribute<TbMedicineSuppliesDetail, TbItemCodeManagement> medicineID;
    public static volatile SingularAttribute<TbMedicineSuppliesDetail, TbEmployee> upEmployeeID;
    public static volatile SingularAttribute<TbMedicineSuppliesDetail, String> medicineSuppliesDetailID;
    public static volatile SingularAttribute<TbMedicineSuppliesDetail, Integer> quantity;
    public static volatile SingularAttribute<TbMedicineSuppliesDetail, TbMedicineSupplies> medicineSuppliesID;
    public static volatile SingularAttribute<TbMedicineSuppliesDetail, Date> dateCreate;
    public static volatile SingularAttribute<TbMedicineSuppliesDetail, Date> upDateTime;

}