package project.ett;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import project.ett.TbEmployee;
import project.ett.TbService;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2017-02-06T16:29:05")
@StaticMetamodel(TbStandardValue.class)
public class TbStandardValue_ { 

    public static volatile SingularAttribute<TbStandardValue, String> sVmale;
    public static volatile SingularAttribute<TbStandardValue, String> sVfemale;
    public static volatile SingularAttribute<TbStandardValue, Integer> svId;
    public static volatile SingularAttribute<TbStandardValue, TbService> serviceID;
    public static volatile SingularAttribute<TbStandardValue, Date> createDate;
    public static volatile SingularAttribute<TbStandardValue, TbEmployee> updateEmp;
    public static volatile SingularAttribute<TbStandardValue, Date> updateDate;
    public static volatile SingularAttribute<TbStandardValue, TbEmployee> creator;

}