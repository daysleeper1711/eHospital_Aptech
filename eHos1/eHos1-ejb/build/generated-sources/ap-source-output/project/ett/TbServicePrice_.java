package project.ett;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import project.ett.TbEmployee;
import project.ett.TbService;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2017-02-06T16:29:05")
@StaticMetamodel(TbServicePrice.class)
public class TbServicePrice_ { 

    public static volatile SingularAttribute<TbServicePrice, Double> price;
    public static volatile SingularAttribute<TbServicePrice, Boolean> status;
    public static volatile SingularAttribute<TbServicePrice, TbService> serviceID;
    public static volatile SingularAttribute<TbServicePrice, Integer> priceID;
    public static volatile SingularAttribute<TbServicePrice, Date> createDate;
    public static volatile SingularAttribute<TbServicePrice, TbEmployee> updateEmp;
    public static volatile SingularAttribute<TbServicePrice, Date> updateDate;
    public static volatile SingularAttribute<TbServicePrice, TbEmployee> creator;

}