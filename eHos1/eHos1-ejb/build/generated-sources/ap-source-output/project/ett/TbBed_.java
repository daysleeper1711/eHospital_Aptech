package project.ett;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import project.ett.TbEmployee;
import project.ett.TbFaculty;
import project.ett.TbService;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2017-02-06T16:29:05")
@StaticMetamodel(TbBed.class)
public class TbBed_ { 

    public static volatile SingularAttribute<TbBed, Boolean> isEnable;
    public static volatile SingularAttribute<TbBed, TbFaculty> faID;
    public static volatile SingularAttribute<TbBed, String> status;
    public static volatile SingularAttribute<TbBed, TbService> serviceID;
    public static volatile SingularAttribute<TbBed, TbEmployee> createByWhom;
    public static volatile SingularAttribute<TbBed, Integer> bedID;
    public static volatile SingularAttribute<TbBed, TbEmployee> updateByWhom;
    public static volatile SingularAttribute<TbBed, Date> createDate;
    public static volatile SingularAttribute<TbBed, Boolean> isSubbed;
    public static volatile SingularAttribute<TbBed, Date> updateDate;
    public static volatile SingularAttribute<TbBed, String> bedName;

}