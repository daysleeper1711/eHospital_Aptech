package project.ett;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import project.ett.TbGKB;
import project.ett.TbGKBService;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2017-02-06T16:29:05")
@StaticMetamodel(TbGKBDetail.class)
public class TbGKBDetail_ { 

    public static volatile SingularAttribute<TbGKBDetail, Integer> gKBDetailID;
    public static volatile SingularAttribute<TbGKBDetail, String> tenGK;
    public static volatile CollectionAttribute<TbGKBDetail, TbGKBService> tbGKBServiceCollection;
    public static volatile SingularAttribute<TbGKBDetail, TbGKB> gkbid;

}