package project.ett;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import project.ett.TbExDurgsSuppliesDetails;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2017-02-06T16:29:05")
@StaticMetamodel(TbExDurgsSupplies.class)
public class TbExDurgsSupplies_ { 

    public static volatile SingularAttribute<TbExDurgsSupplies, Date> upDate;
    public static volatile SingularAttribute<TbExDurgsSupplies, String> typeEx;
    public static volatile SingularAttribute<TbExDurgsSupplies, String> updateby;
    public static volatile SingularAttribute<TbExDurgsSupplies, String> docentry;
    public static volatile SingularAttribute<TbExDurgsSupplies, String> exID;
    public static volatile SingularAttribute<TbExDurgsSupplies, String> createby;
    public static volatile SingularAttribute<TbExDurgsSupplies, Boolean> retrun;
    public static volatile CollectionAttribute<TbExDurgsSupplies, TbExDurgsSuppliesDetails> tbExDurgsSuppliesDetailsCollection;
    public static volatile SingularAttribute<TbExDurgsSupplies, String> note;
    public static volatile SingularAttribute<TbExDurgsSupplies, Date> crDate;
    public static volatile SingularAttribute<TbExDurgsSupplies, String> uStatus;

}