package project.ett;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import project.ett.TbConsulationSurgery;
import project.ett.TbEmployee;
import project.ett.TbMedicalRecord;
import project.ett.TbScheduledSurgery;
import project.ett.TbSurgeryRecord;
import project.ett.TbSurgeryService;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2017-02-06T16:29:05")
@StaticMetamodel(TbSuggestSurgery.class)
public class TbSuggestSurgery_ { 

    public static volatile CollectionAttribute<TbSuggestSurgery, TbConsulationSurgery> tbConsulationSurgeryCollection;
    public static volatile SingularAttribute<TbSuggestSurgery, TbEmployee> userID;
    public static volatile CollectionAttribute<TbSuggestSurgery, TbSurgeryRecord> tbSurgeryRecordCollection;
    public static volatile SingularAttribute<TbSuggestSurgery, TbMedicalRecord> medicalRecordID;
    public static volatile SingularAttribute<TbSuggestSurgery, TbEmployee> doctorID;
    public static volatile CollectionAttribute<TbSuggestSurgery, TbScheduledSurgery> tbScheduledSurgeryCollection;
    public static volatile SingularAttribute<TbSuggestSurgery, String> suggestSurgeryID;
    public static volatile SingularAttribute<TbSuggestSurgery, String> suggestDescription;
    public static volatile SingularAttribute<TbSuggestSurgery, Date> createDate;
    public static volatile SingularAttribute<TbSuggestSurgery, TbSurgeryService> surgeryServiceID;
    public static volatile SingularAttribute<TbSuggestSurgery, Boolean> statusSuggest;

}