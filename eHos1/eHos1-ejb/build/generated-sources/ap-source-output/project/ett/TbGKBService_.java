package project.ett;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import project.ett.TbGKBDetail;
import project.ett.TbGKBServiceDetail;
import project.ett.TbKQKB;
import project.ett.TbService;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2017-02-06T16:29:05")
@StaticMetamodel(TbGKBService.class)
public class TbGKBService_ { 

    public static volatile SingularAttribute<TbGKBService, TbGKBDetail> gKBDetailID;
    public static volatile SingularAttribute<TbGKBService, TbService> serviceID;
    public static volatile SingularAttribute<TbGKBService, Integer> gKBServiceID;
    public static volatile CollectionAttribute<TbGKBService, TbKQKB> tbKQKBCollection;
    public static volatile CollectionAttribute<TbGKBService, TbGKBServiceDetail> tbGKBServiceDetailCollection;
    public static volatile SingularAttribute<TbGKBService, String> tenDV;
    public static volatile SingularAttribute<TbGKBService, String> loaiDV;

}