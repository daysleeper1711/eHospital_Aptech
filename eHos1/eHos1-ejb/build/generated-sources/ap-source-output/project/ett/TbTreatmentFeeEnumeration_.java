package project.ett;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import project.ett.TbEmployee;
import project.ett.TbMedicalRecord;
import project.ett.TbTreatmentFeeEnumerationDetail;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2017-02-06T16:29:05")
@StaticMetamodel(TbTreatmentFeeEnumeration.class)
public class TbTreatmentFeeEnumeration_ { 

    public static volatile SingularAttribute<TbTreatmentFeeEnumeration, Double> amountReturn;
    public static volatile SingularAttribute<TbTreatmentFeeEnumeration, Integer> tfeId;
    public static volatile SingularAttribute<TbTreatmentFeeEnumeration, Double> amountPaymore;
    public static volatile SingularAttribute<TbTreatmentFeeEnumeration, String> status;
    public static volatile SingularAttribute<TbTreatmentFeeEnumeration, Boolean> isReturn;
    public static volatile SingularAttribute<TbTreatmentFeeEnumeration, TbEmployee> receivedByWhom;
    public static volatile SingularAttribute<TbTreatmentFeeEnumeration, TbMedicalRecord> medRecordID;
    public static volatile SingularAttribute<TbTreatmentFeeEnumeration, Double> totalDeposit;
    public static volatile SingularAttribute<TbTreatmentFeeEnumeration, Double> finalTotal;
    public static volatile CollectionAttribute<TbTreatmentFeeEnumeration, TbTreatmentFeeEnumerationDetail> tbTreatmentFeeEnumerationDetailCollection;
    public static volatile SingularAttribute<TbTreatmentFeeEnumeration, Date> receivedDate;

}