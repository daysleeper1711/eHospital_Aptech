package project.ett;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import project.ett.TbBed;
import project.ett.TbEmployee;
import project.ett.TbGKBService;
import project.ett.TbServiceGroup;
import project.ett.TbServicePrice;
import project.ett.TbStandardValue;
import project.ett.TbTestReqDetail;
import project.ett.TbTreatmentFeeEnumerationDetail;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2017-02-06T16:29:05")
@StaticMetamodel(TbService.class)
public class TbService_ { 

    public static volatile SingularAttribute<TbService, Boolean> status;
    public static volatile CollectionAttribute<TbService, TbTestReqDetail> tbTestReqDetailCollection;
    public static volatile CollectionAttribute<TbService, TbStandardValue> tbStandardValueCollection;
    public static volatile CollectionAttribute<TbService, TbTreatmentFeeEnumerationDetail> tbTreatmentFeeEnumerationDetailCollection;
    public static volatile SingularAttribute<TbService, Date> updateDate;
    public static volatile SingularAttribute<TbService, TbEmployee> creator;
    public static volatile SingularAttribute<TbService, String> serviceName;
    public static volatile SingularAttribute<TbService, String> serviceContent;
    public static volatile CollectionAttribute<TbService, TbBed> tbBedCollection;
    public static volatile SingularAttribute<TbService, TbServiceGroup> groupID;
    public static volatile SingularAttribute<TbService, Integer> serviceID;
    public static volatile SingularAttribute<TbService, String> unitOfMea;
    public static volatile CollectionAttribute<TbService, TbGKBService> tbGKBServiceCollection;
    public static volatile SingularAttribute<TbService, Date> createDate;
    public static volatile SingularAttribute<TbService, TbEmployee> updateEmp;
    public static volatile CollectionAttribute<TbService, TbServicePrice> tbServicePriceCollection;

}