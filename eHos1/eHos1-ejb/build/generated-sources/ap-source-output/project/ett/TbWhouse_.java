package project.ett;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2017-02-06T16:29:05")
@StaticMetamodel(TbWhouse.class)
public class TbWhouse_ { 

    public static volatile SingularAttribute<TbWhouse, Integer> qtyOrder;
    public static volatile SingularAttribute<TbWhouse, String> package1;
    public static volatile SingularAttribute<TbWhouse, Date> warDate;
    public static volatile SingularAttribute<TbWhouse, String> whouseID;
    public static volatile SingularAttribute<TbWhouse, Integer> qtyAvailable;
    public static volatile SingularAttribute<TbWhouse, Integer> onHand;
    public static volatile SingularAttribute<TbWhouse, String> itemCode;

}