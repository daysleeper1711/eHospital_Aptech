package project.ett;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import project.ett.TbEmployee;
import project.ett.TbItemCodeManagement;
import project.ett.TbSurgeryRecord;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2017-02-06T16:29:05")
@StaticMetamodel(TbDrugsDetail.class)
public class TbDrugsDetail_ { 

    public static volatile SingularAttribute<TbDrugsDetail, String> drugsDetailID;
    public static volatile SingularAttribute<TbDrugsDetail, String> unit;
    public static volatile SingularAttribute<TbDrugsDetail, TbEmployee> userID;
    public static volatile SingularAttribute<TbDrugsDetail, TbSurgeryRecord> surgeryRecordID;
    public static volatile SingularAttribute<TbDrugsDetail, Boolean> statusDrugs;
    public static volatile SingularAttribute<TbDrugsDetail, Integer> quantity;
    public static volatile SingularAttribute<TbDrugsDetail, Date> createDate;
    public static volatile SingularAttribute<TbDrugsDetail, TbItemCodeManagement> drugID;

}