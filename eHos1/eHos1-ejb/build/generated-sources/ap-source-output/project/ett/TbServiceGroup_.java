package project.ett;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import project.ett.TbEmployee;
import project.ett.TbService;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2017-02-06T16:29:05")
@StaticMetamodel(TbServiceGroup.class)
public class TbServiceGroup_ { 

    public static volatile SingularAttribute<TbServiceGroup, String> groupType;
    public static volatile SingularAttribute<TbServiceGroup, String> groupName;
    public static volatile CollectionAttribute<TbServiceGroup, TbService> tbServiceCollection;
    public static volatile SingularAttribute<TbServiceGroup, Integer> groupID;
    public static volatile SingularAttribute<TbServiceGroup, Boolean> status;
    public static volatile SingularAttribute<TbServiceGroup, String> description;
    public static volatile SingularAttribute<TbServiceGroup, Date> createDate;
    public static volatile SingularAttribute<TbServiceGroup, TbEmployee> updateEmp;
    public static volatile SingularAttribute<TbServiceGroup, Date> updateDate;
    public static volatile SingularAttribute<TbServiceGroup, TbEmployee> creator;

}