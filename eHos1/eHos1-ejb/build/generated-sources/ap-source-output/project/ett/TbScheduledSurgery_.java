package project.ett;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import project.ett.TbEmployee;
import project.ett.TbSuggestSurgery;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2017-02-06T16:29:05")
@StaticMetamodel(TbScheduledSurgery.class)
public class TbScheduledSurgery_ { 

    public static volatile SingularAttribute<TbScheduledSurgery, Date> timeOfStart;
    public static volatile SingularAttribute<TbScheduledSurgery, TbEmployee> userID;
    public static volatile SingularAttribute<TbScheduledSurgery, Boolean> statusScheduled;
    public static volatile SingularAttribute<TbScheduledSurgery, Date> createDate;
    public static volatile SingularAttribute<TbScheduledSurgery, TbSuggestSurgery> suggestSurgeryID;
    public static volatile SingularAttribute<TbScheduledSurgery, String> scheduledID;
    public static volatile SingularAttribute<TbScheduledSurgery, Date> timeOfEnd;

}