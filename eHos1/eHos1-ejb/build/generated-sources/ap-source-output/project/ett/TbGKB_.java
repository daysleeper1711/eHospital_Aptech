package project.ett;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import project.ett.TbGKBDetail;
import project.ett.TbOutPatient;
import project.ett.TbRegisterDTO;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2017-02-06T16:29:05")
@StaticMetamodel(TbGKB.class)
public class TbGKB_ { 

    public static volatile SingularAttribute<TbGKB, Date> dateOfCreate;
    public static volatile SingularAttribute<TbGKB, String> category;
    public static volatile SingularAttribute<TbGKB, Double> price;
    public static volatile CollectionAttribute<TbGKB, TbOutPatient> tbOutPatientCollection;
    public static volatile CollectionAttribute<TbGKB, TbRegisterDTO> tbRegisterDTOCollection;
    public static volatile SingularAttribute<TbGKB, String> whoUpdated;
    public static volatile SingularAttribute<TbGKB, String> gKBName;
    public static volatile SingularAttribute<TbGKB, Date> updatedate;
    public static volatile SingularAttribute<TbGKB, String> gkbid;
    public static volatile SingularAttribute<TbGKB, String> gKBDescription;
    public static volatile SingularAttribute<TbGKB, String> creator;
    public static volatile SingularAttribute<TbGKB, String> gKBStatus;
    public static volatile CollectionAttribute<TbGKB, TbGKBDetail> tbGKBDetailCollection;

}