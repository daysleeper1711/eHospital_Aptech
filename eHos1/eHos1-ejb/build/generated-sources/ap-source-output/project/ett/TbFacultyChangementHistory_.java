package project.ett;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import project.ett.TbEmployee;
import project.ett.TbMedicalRecord;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2017-02-06T16:29:05")
@StaticMetamodel(TbFacultyChangementHistory.class)
public class TbFacultyChangementHistory_ { 

    public static volatile SingularAttribute<TbFacultyChangementHistory, Date> dateIn;
    public static volatile SingularAttribute<TbFacultyChangementHistory, Integer> fchId;
    public static volatile SingularAttribute<TbFacultyChangementHistory, TbEmployee> acceptInBywhom;
    public static volatile SingularAttribute<TbFacultyChangementHistory, String> status;
    public static volatile SingularAttribute<TbFacultyChangementHistory, TbEmployee> acceptOutByWhom;
    public static volatile SingularAttribute<TbFacultyChangementHistory, Date> dateOut;
    public static volatile SingularAttribute<TbFacultyChangementHistory, String> fromFaculty;
    public static volatile SingularAttribute<TbFacultyChangementHistory, TbEmployee> updateByWhom;
    public static volatile SingularAttribute<TbFacultyChangementHistory, TbMedicalRecord> medRecordID;
    public static volatile SingularAttribute<TbFacultyChangementHistory, String> reasonToChange;
    public static volatile SingularAttribute<TbFacultyChangementHistory, String> toFaculty;
    public static volatile SingularAttribute<TbFacultyChangementHistory, Date> updateDate;
    public static volatile SingularAttribute<TbFacultyChangementHistory, String> bedName;

}