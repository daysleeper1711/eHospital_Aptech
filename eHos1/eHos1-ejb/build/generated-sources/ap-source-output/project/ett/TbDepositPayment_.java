package project.ett;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import project.ett.TbEmployee;
import project.ett.TbMedicalRecord;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2017-02-06T16:29:05")
@StaticMetamodel(TbDepositPayment.class)
public class TbDepositPayment_ { 

    public static volatile SingularAttribute<TbDepositPayment, Double> amount;
    public static volatile SingularAttribute<TbDepositPayment, Double> remaining;
    public static volatile SingularAttribute<TbDepositPayment, String> status;
    public static volatile SingularAttribute<TbDepositPayment, TbEmployee> receivedByWhom;
    public static volatile SingularAttribute<TbDepositPayment, TbMedicalRecord> medRecordID;
    public static volatile SingularAttribute<TbDepositPayment, Integer> depositID;
    public static volatile SingularAttribute<TbDepositPayment, Date> receivedDate;

}