package project.ett;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import project.ett.TbEmployee;
import project.ett.TbInsurane;
import project.ett.TbKQKB;
import project.ett.TbMedicalRecord;
import project.ett.TbOutPatient;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2017-02-06T16:29:05")
@StaticMetamodel(TbPatient.class)
public class TbPatient_ { 

    public static volatile SingularAttribute<TbPatient, TbEmployee> regByWhom;
    public static volatile SingularAttribute<TbPatient, String> phone;
    public static volatile SingularAttribute<TbPatient, String> iDCard;
    public static volatile SingularAttribute<TbPatient, Date> regDate;
    public static volatile CollectionAttribute<TbPatient, TbInsurane> tbInsuraneCollection;
    public static volatile CollectionAttribute<TbPatient, TbMedicalRecord> tbMedicalRecordCollection;
    public static volatile CollectionAttribute<TbPatient, TbKQKB> tbKQKBCollection;
    public static volatile SingularAttribute<TbPatient, String> password;
    public static volatile SingularAttribute<TbPatient, Date> updateDate;
    public static volatile SingularAttribute<TbPatient, String> address;
    public static volatile CollectionAttribute<TbPatient, TbOutPatient> tbOutPatientCollection;
    public static volatile SingularAttribute<TbPatient, Date> dob;
    public static volatile SingularAttribute<TbPatient, TbEmployee> updateByWhom;
    public static volatile SingularAttribute<TbPatient, Boolean> gender;
    public static volatile SingularAttribute<TbPatient, String> patName;
    public static volatile SingularAttribute<TbPatient, String> patID;

}