package project.ett;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import project.ett.TbBed;
import project.ett.TbEmployee;
import project.ett.TbMedicineSupplies;
import project.ett.TbPrescription;
import project.ett.TbSurgeryService;
import project.ett.TbTestRequest;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2017-02-06T16:29:05")
@StaticMetamodel(TbFaculty.class)
public class TbFaculty_ { 

    public static volatile SingularAttribute<TbFaculty, String> facultyName;
    public static volatile CollectionAttribute<TbFaculty, TbBed> tbBedCollection;
    public static volatile CollectionAttribute<TbFaculty, TbEmployee> tbEmployeeCollection;
    public static volatile CollectionAttribute<TbFaculty, TbTestRequest> tbTestRequestCollection;
    public static volatile CollectionAttribute<TbFaculty, TbMedicineSupplies> tbMedicineSuppliesCollection;
    public static volatile SingularAttribute<TbFaculty, String> faID;
    public static volatile SingularAttribute<TbFaculty, String> status;
    public static volatile CollectionAttribute<TbFaculty, TbPrescription> tbPrescriptionCollection;
    public static volatile CollectionAttribute<TbFaculty, TbSurgeryService> tbSurgeryServiceCollection;
    public static volatile CollectionAttribute<TbFaculty, TbTestRequest> tbTestRequestCollection1;

}