package project.ett;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import project.ett.TbDepositPayment;
import project.ett.TbEmployee;
import project.ett.TbFacultyChangementHistory;
import project.ett.TbMedicineSupplies;
import project.ett.TbPatient;
import project.ett.TbPrescription;
import project.ett.TbSuggestSurgery;
import project.ett.TbTestRequest;
import project.ett.TbTreatmentFeeEnumeration;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2017-02-06T16:29:05")
@StaticMetamodel(TbMedicalRecord.class)
public class TbMedicalRecord_ { 

    public static volatile CollectionAttribute<TbMedicalRecord, TbSuggestSurgery> tbSuggestSurgeryCollection;
    public static volatile CollectionAttribute<TbMedicalRecord, TbTestRequest> tbTestRequestCollection;
    public static volatile SingularAttribute<TbMedicalRecord, String> status;
    public static volatile SingularAttribute<TbMedicalRecord, String> suggestTreatment;
    public static volatile SingularAttribute<TbMedicalRecord, TbEmployee> diagnosisDoc;
    public static volatile SingularAttribute<TbMedicalRecord, TbEmployee> createByWhom;
    public static volatile SingularAttribute<TbMedicalRecord, String> dischargeStatus;
    public static volatile SingularAttribute<TbMedicalRecord, String> diagnosis;
    public static volatile CollectionAttribute<TbMedicalRecord, TbPrescription> tbPrescriptionCollection;
    public static volatile SingularAttribute<TbMedicalRecord, String> medRecordID;
    public static volatile SingularAttribute<TbMedicalRecord, String> sideDisease;
    public static volatile SingularAttribute<TbMedicalRecord, Boolean> isDeposit;
    public static volatile SingularAttribute<TbMedicalRecord, String> mainDisease;
    public static volatile SingularAttribute<TbMedicalRecord, String> symptoms;
    public static volatile CollectionAttribute<TbMedicalRecord, TbMedicineSupplies> tbMedicineSuppliesCollection;
    public static volatile CollectionAttribute<TbMedicalRecord, TbFacultyChangementHistory> tbFacultyChangementHistoryCollection;
    public static volatile CollectionAttribute<TbMedicalRecord, TbTreatmentFeeEnumeration> tbTreatmentFeeEnumerationCollection;
    public static volatile SingularAttribute<TbMedicalRecord, Date> conclusionDate;
    public static volatile SingularAttribute<TbMedicalRecord, String> healthCondition;
    public static volatile SingularAttribute<TbMedicalRecord, String> treatmentResult;
    public static volatile SingularAttribute<TbMedicalRecord, TbEmployee> conclusionByWhom;
    public static volatile SingularAttribute<TbMedicalRecord, Date> diagnosisDate;
    public static volatile CollectionAttribute<TbMedicalRecord, TbDepositPayment> tbDepositPaymentCollection;
    public static volatile SingularAttribute<TbMedicalRecord, TbPatient> patID;
    public static volatile SingularAttribute<TbMedicalRecord, Date> createDate;

}