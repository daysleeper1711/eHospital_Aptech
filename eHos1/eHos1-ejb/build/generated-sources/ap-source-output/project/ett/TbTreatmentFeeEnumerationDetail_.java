package project.ett;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import project.ett.TbService;
import project.ett.TbTreatmentFeeEnumeration;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2017-02-06T16:29:05")
@StaticMetamodel(TbTreatmentFeeEnumerationDetail.class)
public class TbTreatmentFeeEnumerationDetail_ { 

    public static volatile SingularAttribute<TbTreatmentFeeEnumerationDetail, Double> total;
    public static volatile SingularAttribute<TbTreatmentFeeEnumerationDetail, TbTreatmentFeeEnumeration> tfeId;
    public static volatile SingularAttribute<TbTreatmentFeeEnumerationDetail, Integer> tFEDetailID;
    public static volatile SingularAttribute<TbTreatmentFeeEnumerationDetail, String> status;
    public static volatile SingularAttribute<TbTreatmentFeeEnumerationDetail, TbService> serviceID;
    public static volatile SingularAttribute<TbTreatmentFeeEnumerationDetail, Integer> quantity;

}