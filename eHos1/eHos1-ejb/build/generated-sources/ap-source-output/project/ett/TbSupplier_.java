package project.ett;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import project.ett.TbImDurgsSupplies;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2017-02-06T16:29:05")
@StaticMetamodel(TbSupplier.class)
public class TbSupplier_ { 

    public static volatile CollectionAttribute<TbSupplier, TbImDurgsSupplies> tbImDurgsSuppliesCollection;
    public static volatile SingularAttribute<TbSupplier, String> supplierID;
    public static volatile SingularAttribute<TbSupplier, String> supplierName;
    public static volatile SingularAttribute<TbSupplier, String> uStatus;

}