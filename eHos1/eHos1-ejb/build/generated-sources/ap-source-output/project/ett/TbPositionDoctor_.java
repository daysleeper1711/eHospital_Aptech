package project.ett;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import project.ett.TbEmployee;
import project.ett.TbOperatorDoctorDetail;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2017-02-06T16:29:05")
@StaticMetamodel(TbPositionDoctor.class)
public class TbPositionDoctor_ { 

    public static volatile SingularAttribute<TbPositionDoctor, Boolean> statusPosition;
    public static volatile SingularAttribute<TbPositionDoctor, TbEmployee> userID;
    public static volatile SingularAttribute<TbPositionDoctor, Integer> positionID;
    public static volatile CollectionAttribute<TbPositionDoctor, TbOperatorDoctorDetail> tbOperatorDoctorDetailCollection;
    public static volatile SingularAttribute<TbPositionDoctor, Date> createDate;
    public static volatile SingularAttribute<TbPositionDoctor, String> positionName;

}