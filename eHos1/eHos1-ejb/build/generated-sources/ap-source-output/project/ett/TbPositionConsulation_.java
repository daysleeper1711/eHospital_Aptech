package project.ett;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import project.ett.TbConsulationDoctorDetail;
import project.ett.TbEmployee;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2017-02-06T16:29:05")
@StaticMetamodel(TbPositionConsulation.class)
public class TbPositionConsulation_ { 

    public static volatile SingularAttribute<TbPositionConsulation, Boolean> statusPosition;
    public static volatile SingularAttribute<TbPositionConsulation, TbEmployee> userID;
    public static volatile SingularAttribute<TbPositionConsulation, Integer> positionID;
    public static volatile SingularAttribute<TbPositionConsulation, Date> createDate;
    public static volatile SingularAttribute<TbPositionConsulation, String> positionName;
    public static volatile CollectionAttribute<TbPositionConsulation, TbConsulationDoctorDetail> tbConsulationDoctorDetailCollection;

}