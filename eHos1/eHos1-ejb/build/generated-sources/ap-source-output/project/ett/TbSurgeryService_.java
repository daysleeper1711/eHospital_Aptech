package project.ett;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import project.ett.TbCategory;
import project.ett.TbEmployee;
import project.ett.TbFaculty;
import project.ett.TbLevelSurgery;
import project.ett.TbSuggestSurgery;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2017-02-06T16:29:05")
@StaticMetamodel(TbSurgeryService.class)
public class TbSurgeryService_ { 

    public static volatile SingularAttribute<TbSurgeryService, TbEmployee> userID;
    public static volatile CollectionAttribute<TbSurgeryService, TbSuggestSurgery> tbSuggestSurgeryCollection;
    public static volatile SingularAttribute<TbSurgeryService, Boolean> statusService;
    public static volatile SingularAttribute<TbSurgeryService, TbCategory> categoryID;
    public static volatile SingularAttribute<TbSurgeryService, String> surgeryServiceName;
    public static volatile SingularAttribute<TbSurgeryService, Double> initialCost;
    public static volatile SingularAttribute<TbSurgeryService, TbLevelSurgery> levelSurgery;
    public static volatile SingularAttribute<TbSurgeryService, Date> createDate;
    public static volatile SingularAttribute<TbSurgeryService, String> surgeryServiceID;
    public static volatile SingularAttribute<TbSurgeryService, TbFaculty> faultyID;
    public static volatile SingularAttribute<TbSurgeryService, String> serviceDescription;

}