package project.ett;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import project.ett.TbImDurgsSuppliesDetails;
import project.ett.TbSupplier;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2017-02-06T16:29:05")
@StaticMetamodel(TbImDurgsSupplies.class)
public class TbImDurgsSupplies_ { 

    public static volatile SingularAttribute<TbImDurgsSupplies, Date> upDate;
    public static volatile SingularAttribute<TbImDurgsSupplies, String> package1;
    public static volatile SingularAttribute<TbImDurgsSupplies, TbSupplier> supplierID;
    public static volatile SingularAttribute<TbImDurgsSupplies, String> updateby;
    public static volatile SingularAttribute<TbImDurgsSupplies, String> numBill;
    public static volatile SingularAttribute<TbImDurgsSupplies, String> createby;
    public static volatile SingularAttribute<TbImDurgsSupplies, String> symBill;
    public static volatile CollectionAttribute<TbImDurgsSupplies, TbImDurgsSuppliesDetails> tbImDurgsSuppliesDetailsCollection;
    public static volatile SingularAttribute<TbImDurgsSupplies, String> uStatus;
    public static volatile SingularAttribute<TbImDurgsSupplies, Double> totatPrice;
    public static volatile SingularAttribute<TbImDurgsSupplies, String> note;
    public static volatile SingularAttribute<TbImDurgsSupplies, Date> crDate;

}