package project.ett;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import project.ett.TbEmployee;
import project.ett.TbItemCodeManagement;
import project.ett.TbPrescription;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2017-02-06T16:29:05")
@StaticMetamodel(TbPrescriptionDetail.class)
public class TbPrescriptionDetail_ { 

    public static volatile SingularAttribute<TbPrescriptionDetail, Double> price;
    public static volatile SingularAttribute<TbPrescriptionDetail, TbItemCodeManagement> medicineID;
    public static volatile SingularAttribute<TbPrescriptionDetail, TbEmployee> upEmployeeID;
    public static volatile SingularAttribute<TbPrescriptionDetail, Integer> afternoon;
    public static volatile SingularAttribute<TbPrescriptionDetail, TbPrescription> prescriptionID;
    public static volatile SingularAttribute<TbPrescriptionDetail, Integer> night;
    public static volatile SingularAttribute<TbPrescriptionDetail, Integer> quantity;
    public static volatile SingularAttribute<TbPrescriptionDetail, String> prescriptionDetailID;
    public static volatile SingularAttribute<TbPrescriptionDetail, Integer> morning;
    public static volatile SingularAttribute<TbPrescriptionDetail, Date> dateCreate;
    public static volatile SingularAttribute<TbPrescriptionDetail, Date> upDateTime;

}