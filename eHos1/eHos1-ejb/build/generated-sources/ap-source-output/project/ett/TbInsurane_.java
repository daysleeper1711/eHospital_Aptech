package project.ett;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import project.ett.InsuranceType;
import project.ett.TbPatient;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2017-02-06T16:29:05")
@StaticMetamodel(TbInsurane.class)
public class TbInsurane_ { 

    public static volatile SingularAttribute<TbInsurane, String> insuranceID;
    public static volatile SingularAttribute<TbInsurane, String> name;
    public static volatile SingularAttribute<TbInsurane, InsuranceType> insuranceTypeID;
    public static volatile SingularAttribute<TbInsurane, Date> dateExpire;
    public static volatile SingularAttribute<TbInsurane, Boolean> statusIns;
    public static volatile SingularAttribute<TbInsurane, Date> dateStart;
    public static volatile SingularAttribute<TbInsurane, TbPatient> patientID;
    public static volatile SingularAttribute<TbInsurane, String> regPlace;

}