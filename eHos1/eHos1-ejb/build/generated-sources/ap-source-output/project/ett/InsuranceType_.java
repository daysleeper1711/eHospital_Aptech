package project.ett;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import project.ett.TbInsurane;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2017-02-06T16:29:05")
@StaticMetamodel(InsuranceType.class)
public class InsuranceType_ { 

    public static volatile SingularAttribute<InsuranceType, String> description;
    public static volatile SingularAttribute<InsuranceType, Boolean> statusType;
    public static volatile SingularAttribute<InsuranceType, String> insuranceTypeID;
    public static volatile SingularAttribute<InsuranceType, Integer> precentage;
    public static volatile CollectionAttribute<InsuranceType, TbInsurane> tbInsuraneCollection;

}