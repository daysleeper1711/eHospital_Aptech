package project.ett;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import project.ett.TbConsulationSurgery;
import project.ett.TbEmployee;
import project.ett.TbPositionConsulation;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2017-02-06T16:29:05")
@StaticMetamodel(TbConsulationDoctorDetail.class)
public class TbConsulationDoctorDetail_ { 

    public static volatile SingularAttribute<TbConsulationDoctorDetail, TbEmployee> userID;
    public static volatile SingularAttribute<TbConsulationDoctorDetail, String> consulationDoctorDetailID;
    public static volatile SingularAttribute<TbConsulationDoctorDetail, TbPositionConsulation> positionID;
    public static volatile SingularAttribute<TbConsulationDoctorDetail, Boolean> statusDetail;
    public static volatile SingularAttribute<TbConsulationDoctorDetail, TbConsulationSurgery> consulationID;
    public static volatile SingularAttribute<TbConsulationDoctorDetail, TbEmployee> doctorID;
    public static volatile SingularAttribute<TbConsulationDoctorDetail, Date> createDate;

}