package project.ett;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import project.ett.TbEmployee;
import project.ett.TbPositionDoctor;
import project.ett.TbSurgeryRecord;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2017-02-06T16:29:05")
@StaticMetamodel(TbOperatorDoctorDetail.class)
public class TbOperatorDoctorDetail_ { 

    public static volatile SingularAttribute<TbOperatorDoctorDetail, Boolean> statusPosition;
    public static volatile SingularAttribute<TbOperatorDoctorDetail, TbEmployee> userID;
    public static volatile SingularAttribute<TbOperatorDoctorDetail, TbSurgeryRecord> surgeryRecordID;
    public static volatile SingularAttribute<TbOperatorDoctorDetail, TbPositionDoctor> positionID;
    public static volatile SingularAttribute<TbOperatorDoctorDetail, TbEmployee> doctorID;
    public static volatile SingularAttribute<TbOperatorDoctorDetail, Date> createDate;
    public static volatile SingularAttribute<TbOperatorDoctorDetail, String> operatorDoctorDetailID;

}