package project.ett;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import project.ett.TbGKB;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2017-02-06T16:29:05")
@StaticMetamodel(TbRegisterDTO.class)
public class TbRegisterDTO_ { 

    public static volatile SingularAttribute<TbRegisterDTO, String> userAddress;
    public static volatile SingularAttribute<TbRegisterDTO, String> sex;
    public static volatile SingularAttribute<TbRegisterDTO, String> phone;
    public static volatile SingularAttribute<TbRegisterDTO, String> idCard;
    public static volatile SingularAttribute<TbRegisterDTO, String> registerID;
    public static volatile SingularAttribute<TbRegisterDTO, String> name;
    public static volatile SingularAttribute<TbRegisterDTO, Date> dob;
    public static volatile SingularAttribute<TbRegisterDTO, String> gKBName;
    public static volatile SingularAttribute<TbRegisterDTO, Date> dateCreate;
    public static volatile SingularAttribute<TbRegisterDTO, TbGKB> gkbid;

}