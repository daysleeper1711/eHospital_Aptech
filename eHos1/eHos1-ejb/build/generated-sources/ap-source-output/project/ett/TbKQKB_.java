package project.ett;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import project.ett.TbEmployee;
import project.ett.TbGKBService;
import project.ett.TbKQKBDetail;
import project.ett.TbKTQ;
import project.ett.TbOutPatient;
import project.ett.TbPatient;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2017-02-06T16:29:05")
@StaticMetamodel(TbKQKB.class)
public class TbKQKB_ { 

    public static volatile CollectionAttribute<TbKQKB, TbKQKBDetail> tbKQKBDetailCollection;
    public static volatile SingularAttribute<TbKQKB, TbEmployee> employeeID;
    public static volatile SingularAttribute<TbKQKB, Date> inputDate;
    public static volatile SingularAttribute<TbKQKB, String> conclue;
    public static volatile CollectionAttribute<TbKQKB, TbKTQ> tbKTQCollection;
    public static volatile SingularAttribute<TbKQKB, Integer> examinationResultsID;
    public static volatile SingularAttribute<TbKQKB, TbPatient> patID;
    public static volatile SingularAttribute<TbKQKB, TbOutPatient> outPatientID;
    public static volatile SingularAttribute<TbKQKB, String> kQStatus;
    public static volatile SingularAttribute<TbKQKB, TbGKBService> gKBServiceID;
    public static volatile SingularAttribute<TbKQKB, String> benh;
    public static volatile SingularAttribute<TbKQKB, String> deNghi;
    public static volatile SingularAttribute<TbKQKB, String> creator;

}