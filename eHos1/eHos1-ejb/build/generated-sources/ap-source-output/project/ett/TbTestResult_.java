package project.ett;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import project.ett.TbEmployee;
import project.ett.TbTestRequest;
import project.ett.TbTestResultDetail;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2017-02-06T16:29:05")
@StaticMetamodel(TbTestResult.class)
public class TbTestResult_ { 

    public static volatile SingularAttribute<TbTestResult, TbEmployee> concludeDoc;
    public static volatile CollectionAttribute<TbTestResult, TbTestResultDetail> tbTestResultDetailCollection;
    public static volatile SingularAttribute<TbTestResult, TbTestRequest> testReqID;
    public static volatile SingularAttribute<TbTestResult, Integer> testResultID;
    public static volatile SingularAttribute<TbTestResult, Boolean> status;
    public static volatile SingularAttribute<TbTestResult, String> conclusion;
    public static volatile SingularAttribute<TbTestResult, TbEmployee> testDoc;
    public static volatile SingularAttribute<TbTestResult, Date> createDate;
    public static volatile SingularAttribute<TbTestResult, String> note;
    public static volatile SingularAttribute<TbTestResult, Date> updateDate;

}