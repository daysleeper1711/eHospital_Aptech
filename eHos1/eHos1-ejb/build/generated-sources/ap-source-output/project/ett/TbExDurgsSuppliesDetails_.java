package project.ett;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import project.ett.TbExDurgsSupplies;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2017-02-06T16:29:05")
@StaticMetamodel(TbExDurgsSuppliesDetails.class)
public class TbExDurgsSuppliesDetails_ { 

    public static volatile SingularAttribute<TbExDurgsSuppliesDetails, String> package1;
    public static volatile SingularAttribute<TbExDurgsSuppliesDetails, Date> warDate;
    public static volatile SingularAttribute<TbExDurgsSuppliesDetails, TbExDurgsSupplies> exID;
    public static volatile SingularAttribute<TbExDurgsSuppliesDetails, Integer> quantity;
    public static volatile SingularAttribute<TbExDurgsSuppliesDetails, Double> xPrice;
    public static volatile SingularAttribute<TbExDurgsSuppliesDetails, String> exDetails;
    public static volatile SingularAttribute<TbExDurgsSuppliesDetails, String> itemCode;

}