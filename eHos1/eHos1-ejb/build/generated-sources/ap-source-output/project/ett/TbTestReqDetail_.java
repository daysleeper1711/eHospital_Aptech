package project.ett;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import project.ett.TbEmployee;
import project.ett.TbService;
import project.ett.TbTestRequest;
import project.ett.TbTestResultDetail;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2017-02-06T16:29:05")
@StaticMetamodel(TbTestReqDetail.class)
public class TbTestReqDetail_ { 

    public static volatile SingularAttribute<TbTestReqDetail, Double> patPrice;
    public static volatile SingularAttribute<TbTestReqDetail, TbTestRequest> testReqID;
    public static volatile CollectionAttribute<TbTestReqDetail, TbTestResultDetail> tbTestResultDetailCollection;
    public static volatile SingularAttribute<TbTestReqDetail, TbService> serviceID;
    public static volatile SingularAttribute<TbTestReqDetail, Date> createDate;
    public static volatile SingularAttribute<TbTestReqDetail, TbEmployee> updateEmp;
    public static volatile SingularAttribute<TbTestReqDetail, Integer> testReqDetailID;
    public static volatile SingularAttribute<TbTestReqDetail, Double> realPrice;
    public static volatile SingularAttribute<TbTestReqDetail, Date> updateDate;
    public static volatile SingularAttribute<TbTestReqDetail, TbEmployee> creator;

}