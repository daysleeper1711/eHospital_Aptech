package project.ett;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import project.ett.TbEmployee;
import project.ett.TbFaculty;
import project.ett.TbMedicalRecord;
import project.ett.TbOutPatient;
import project.ett.TbTestReqDetail;
import project.ett.TbTestResult;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2017-02-06T16:29:05")
@StaticMetamodel(TbTestRequest.class)
public class TbTestRequest_ { 

    public static volatile SingularAttribute<TbTestRequest, String> testReqID;
    public static volatile SingularAttribute<TbTestRequest, TbEmployee> appointedDoc;
    public static volatile SingularAttribute<TbTestRequest, String> status;
    public static volatile SingularAttribute<TbTestRequest, String> diagnosis;
    public static volatile CollectionAttribute<TbTestRequest, TbTestResult> tbTestResultCollection;
    public static volatile CollectionAttribute<TbTestRequest, TbTestReqDetail> tbTestReqDetailCollection;
    public static volatile SingularAttribute<TbTestRequest, Date> updateDate;
    public static volatile SingularAttribute<TbTestRequest, TbOutPatient> outPatID;
    public static volatile SingularAttribute<TbTestRequest, TbFaculty> toFac;
    public static volatile SingularAttribute<TbTestRequest, TbFaculty> fromFac;
    public static volatile SingularAttribute<TbTestRequest, Double> totalPayment;
    public static volatile SingularAttribute<TbTestRequest, TbMedicalRecord> mrId;
    public static volatile SingularAttribute<TbTestRequest, Date> createDate;
    public static volatile SingularAttribute<TbTestRequest, TbEmployee> updateEmp;

}