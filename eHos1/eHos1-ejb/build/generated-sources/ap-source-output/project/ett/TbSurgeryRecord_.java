package project.ett;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import project.ett.TbDrugsDetail;
import project.ett.TbEmployee;
import project.ett.TbOperatorDoctorDetail;
import project.ett.TbSuggestSurgery;
import project.ett.TbSuppliesDetail;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2017-02-06T16:29:05")
@StaticMetamodel(TbSurgeryRecord.class)
public class TbSurgeryRecord_ { 

    public static volatile SingularAttribute<TbSurgeryRecord, Date> timeOfStart;
    public static volatile SingularAttribute<TbSurgeryRecord, Boolean> statusSurgery;
    public static volatile SingularAttribute<TbSurgeryRecord, String> surgeryRecordID;
    public static volatile SingularAttribute<TbSurgeryRecord, String> surgeryResult;
    public static volatile SingularAttribute<TbSurgeryRecord, Double> beginCost;
    public static volatile SingularAttribute<TbSurgeryRecord, TbSuggestSurgery> suggestSurgeryID;
    public static volatile SingularAttribute<TbSurgeryRecord, TbEmployee> userID;
    public static volatile SingularAttribute<TbSurgeryRecord, String> processOfSurgery;
    public static volatile CollectionAttribute<TbSurgeryRecord, TbOperatorDoctorDetail> tbOperatorDoctorDetailCollection;
    public static volatile SingularAttribute<TbSurgeryRecord, Double> payCost;
    public static volatile SingularAttribute<TbSurgeryRecord, Date> createDate;
    public static volatile SingularAttribute<TbSurgeryRecord, Date> timeOfEnd;
    public static volatile CollectionAttribute<TbSurgeryRecord, TbSuppliesDetail> tbSuppliesDetailCollection;
    public static volatile CollectionAttribute<TbSurgeryRecord, TbDrugsDetail> tbDrugsDetailCollection;

}