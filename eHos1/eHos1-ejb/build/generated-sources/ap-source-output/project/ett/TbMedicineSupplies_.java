package project.ett;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import project.ett.TbEmployee;
import project.ett.TbFaculty;
import project.ett.TbMedicalRecord;
import project.ett.TbMedicineSuppliesDetail;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2017-02-06T16:29:05")
@StaticMetamodel(TbMedicineSupplies.class)
public class TbMedicineSupplies_ { 

    public static volatile SingularAttribute<TbMedicineSupplies, TbEmployee> employeeID;
    public static volatile SingularAttribute<TbMedicineSupplies, Date> createForDate;
    public static volatile CollectionAttribute<TbMedicineSupplies, TbMedicineSuppliesDetail> tbMedicineSuppliesDetailCollection;
    public static volatile SingularAttribute<TbMedicineSupplies, Date> upStatusGived;
    public static volatile SingularAttribute<TbMedicineSupplies, Date> dateCreate;
    public static volatile SingularAttribute<TbMedicineSupplies, String> statusMedSupp;
    public static volatile SingularAttribute<TbMedicineSupplies, TbEmployee> upEmployeeID;
    public static volatile SingularAttribute<TbMedicineSupplies, String> statusReport;
    public static volatile SingularAttribute<TbMedicineSupplies, TbMedicalRecord> medicalRecordID;
    public static volatile SingularAttribute<TbMedicineSupplies, Double> priceAfterHealthInsurance;
    public static volatile SingularAttribute<TbMedicineSupplies, String> medicineSuppliesID;
    public static volatile SingularAttribute<TbMedicineSupplies, TbFaculty> facultyFrom;
    public static volatile SingularAttribute<TbMedicineSupplies, Date> upDateTime;
    public static volatile SingularAttribute<TbMedicineSupplies, Double> totalPrice;

}