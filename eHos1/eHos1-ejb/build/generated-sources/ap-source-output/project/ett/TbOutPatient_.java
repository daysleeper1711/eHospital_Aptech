package project.ett;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import project.ett.TbEmployee;
import project.ett.TbGKB;
import project.ett.TbKQKB;
import project.ett.TbPatient;
import project.ett.TbTestRequest;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2017-02-06T16:29:05")
@StaticMetamodel(TbOutPatient.class)
public class TbOutPatient_ { 

    public static volatile CollectionAttribute<TbOutPatient, TbTestRequest> tbTestRequestCollection;
    public static volatile SingularAttribute<TbOutPatient, String> status;
    public static volatile SingularAttribute<TbOutPatient, TbPatient> patID;
    public static volatile SingularAttribute<TbOutPatient, Date> createDate;
    public static volatile CollectionAttribute<TbOutPatient, TbKQKB> tbKQKBCollection;
    public static volatile SingularAttribute<TbOutPatient, TbGKB> gkbid;
    public static volatile SingularAttribute<TbOutPatient, String> outPatID;
    public static volatile SingularAttribute<TbOutPatient, TbEmployee> creator;

}