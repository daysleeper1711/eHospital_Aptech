package project.ett;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import project.ett.TbKQKB;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2017-02-06T16:29:05")
@StaticMetamodel(TbKTQ.class)
public class TbKTQ_ { 

    public static volatile SingularAttribute<TbKTQ, String> height;
    public static volatile SingularAttribute<TbKTQ, TbKQKB> examinationResultsID;
    public static volatile SingularAttribute<TbKTQ, String> bloodPressure;
    public static volatile SingularAttribute<TbKTQ, Integer> ktoid;
    public static volatile SingularAttribute<TbKTQ, String> pulse;
    public static volatile SingularAttribute<TbKTQ, String> bMIIndex;
    public static volatile SingularAttribute<TbKTQ, String> classificationFitness;
    public static volatile SingularAttribute<TbKTQ, String> kQWeight;

}