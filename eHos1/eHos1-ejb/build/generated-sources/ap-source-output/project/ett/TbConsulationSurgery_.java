package project.ett;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import project.ett.TbConsulationDoctorDetail;
import project.ett.TbEmployee;
import project.ett.TbSuggestSurgery;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2017-02-06T16:29:05")
@StaticMetamodel(TbConsulationSurgery.class)
public class TbConsulationSurgery_ { 

    public static volatile SingularAttribute<TbConsulationSurgery, TbEmployee> userID;
    public static volatile SingularAttribute<TbConsulationSurgery, Boolean> statusConsulation;
    public static volatile SingularAttribute<TbConsulationSurgery, String> consulationID;
    public static volatile SingularAttribute<TbConsulationSurgery, Date> consulationDate;
    public static volatile SingularAttribute<TbConsulationSurgery, Date> createDate;
    public static volatile SingularAttribute<TbConsulationSurgery, TbSuggestSurgery> suggestSurgeryID;
    public static volatile SingularAttribute<TbConsulationSurgery, String> consulation;
    public static volatile CollectionAttribute<TbConsulationSurgery, TbConsulationDoctorDetail> tbConsulationDoctorDetailCollection;

}