package project.ett;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import project.ett.TbEmployee;
import project.ett.TbItemCodeManagement;
import project.ett.TbSurgeryRecord;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2017-02-06T16:29:05")
@StaticMetamodel(TbSuppliesDetail.class)
public class TbSuppliesDetail_ { 

    public static volatile SingularAttribute<TbSuppliesDetail, String> unit;
    public static volatile SingularAttribute<TbSuppliesDetail, TbEmployee> userID;
    public static volatile SingularAttribute<TbSuppliesDetail, TbSurgeryRecord> surgeryRecordID;
    public static volatile SingularAttribute<TbSuppliesDetail, Boolean> statusSupplies;
    public static volatile SingularAttribute<TbSuppliesDetail, TbItemCodeManagement> suppliesID;
    public static volatile SingularAttribute<TbSuppliesDetail, Integer> quantity;
    public static volatile SingularAttribute<TbSuppliesDetail, String> suppliesDetailID;
    public static volatile SingularAttribute<TbSuppliesDetail, Date> createDate;

}