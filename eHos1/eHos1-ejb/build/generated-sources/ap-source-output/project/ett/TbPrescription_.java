package project.ett;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import project.ett.TbEmployee;
import project.ett.TbFaculty;
import project.ett.TbMedicalRecord;
import project.ett.TbPrescriptionDetail;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2017-02-06T16:29:05")
@StaticMetamodel(TbPrescription.class)
public class TbPrescription_ { 

    public static volatile SingularAttribute<TbPrescription, TbEmployee> employeeID;
    public static volatile SingularAttribute<TbPrescription, String> prescriptionID;
    public static volatile SingularAttribute<TbPrescription, Date> createForDate;
    public static volatile CollectionAttribute<TbPrescription, TbPrescriptionDetail> tbPrescriptionDetailCollection;
    public static volatile SingularAttribute<TbPrescription, Date> upStatusGived;
    public static volatile SingularAttribute<TbPrescription, Date> dateCreate;
    public static volatile SingularAttribute<TbPrescription, String> dignose;
    public static volatile SingularAttribute<TbPrescription, TbEmployee> upEmployeeID;
    public static volatile SingularAttribute<TbPrescription, String> statusReport;
    public static volatile SingularAttribute<TbPrescription, TbMedicalRecord> medicalRecordID;
    public static volatile SingularAttribute<TbPrescription, String> statusPres;
    public static volatile SingularAttribute<TbPrescription, Double> priceAfterHealthInsurance;
    public static volatile SingularAttribute<TbPrescription, TbFaculty> facultyFrom;
    public static volatile SingularAttribute<TbPrescription, Date> upDateTime;
    public static volatile SingularAttribute<TbPrescription, Double> totalPrice;

}