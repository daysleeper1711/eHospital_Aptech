package project.ett;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import project.ett.TbEmployee;
import project.ett.TbSurgeryService;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2017-02-06T16:29:05")
@StaticMetamodel(TbCategory.class)
public class TbCategory_ { 

    public static volatile SingularAttribute<TbCategory, String> categoryName;
    public static volatile SingularAttribute<TbCategory, TbEmployee> userID;
    public static volatile SingularAttribute<TbCategory, Integer> categoryID;
    public static volatile SingularAttribute<TbCategory, String> categoryDescription;
    public static volatile SingularAttribute<TbCategory, Boolean> statusCategory;
    public static volatile SingularAttribute<TbCategory, Date> createDate;
    public static volatile CollectionAttribute<TbCategory, TbSurgeryService> tbSurgeryServiceCollection;

}