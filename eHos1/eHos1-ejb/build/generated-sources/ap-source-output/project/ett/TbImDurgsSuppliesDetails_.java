package project.ett;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import project.ett.TbImDurgsSupplies;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2017-02-06T16:29:05")
@StaticMetamodel(TbImDurgsSuppliesDetails.class)
public class TbImDurgsSuppliesDetails_ { 

    public static volatile SingularAttribute<TbImDurgsSuppliesDetails, TbImDurgsSupplies> package1;
    public static volatile SingularAttribute<TbImDurgsSuppliesDetails, String> imDetails;
    public static volatile SingularAttribute<TbImDurgsSuppliesDetails, Date> warDate;
    public static volatile SingularAttribute<TbImDurgsSuppliesDetails, Integer> quantity;
    public static volatile SingularAttribute<TbImDurgsSuppliesDetails, Double> nPrice;
    public static volatile SingularAttribute<TbImDurgsSuppliesDetails, String> itemCode;

}