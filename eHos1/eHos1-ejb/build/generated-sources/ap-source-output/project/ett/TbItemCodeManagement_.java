package project.ett;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import project.ett.TbDrugsDetail;
import project.ett.TbMedicineSuppliesDetail;
import project.ett.TbPrescriptionDetail;
import project.ett.TbSuppliesDetail;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2017-02-06T16:29:05")
@StaticMetamodel(TbItemCodeManagement.class)
public class TbItemCodeManagement_ { 

    public static volatile SingularAttribute<TbItemCodeManagement, Date> upDate;
    public static volatile SingularAttribute<TbItemCodeManagement, String> itemName;
    public static volatile SingularAttribute<TbItemCodeManagement, String> createby;
    public static volatile CollectionAttribute<TbItemCodeManagement, TbPrescriptionDetail> tbPrescriptionDetailCollection;
    public static volatile CollectionAttribute<TbItemCodeManagement, TbMedicineSuppliesDetail> tbMedicineSuppliesDetailCollection;
    public static volatile SingularAttribute<TbItemCodeManagement, Double> xPrice;
    public static volatile SingularAttribute<TbItemCodeManagement, String> nation;
    public static volatile SingularAttribute<TbItemCodeManagement, Date> crDate;
    public static volatile SingularAttribute<TbItemCodeManagement, String> genericDrug;
    public static volatile SingularAttribute<TbItemCodeManagement, String> content;
    public static volatile SingularAttribute<TbItemCodeManagement, String> unit;
    public static volatile SingularAttribute<TbItemCodeManagement, String> howToUse;
    public static volatile SingularAttribute<TbItemCodeManagement, String> updateby;
    public static volatile SingularAttribute<TbItemCodeManagement, String> typeITcode;
    public static volatile SingularAttribute<TbItemCodeManagement, String> uStatus;
    public static volatile CollectionAttribute<TbItemCodeManagement, TbSuppliesDetail> tbSuppliesDetailCollection;
    public static volatile SingularAttribute<TbItemCodeManagement, String> itemCode;
    public static volatile CollectionAttribute<TbItemCodeManagement, TbDrugsDetail> tbDrugsDetailCollection;

}