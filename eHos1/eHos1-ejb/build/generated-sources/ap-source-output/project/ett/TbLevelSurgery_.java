package project.ett;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import project.ett.TbEmployee;
import project.ett.TbSurgeryService;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2017-02-06T16:29:05")
@StaticMetamodel(TbLevelSurgery.class)
public class TbLevelSurgery_ { 

    public static volatile SingularAttribute<TbLevelSurgery, String> levelDecription;
    public static volatile SingularAttribute<TbLevelSurgery, TbEmployee> userID;
    public static volatile SingularAttribute<TbLevelSurgery, Boolean> statusLevel;
    public static volatile SingularAttribute<TbLevelSurgery, Integer> levelID;
    public static volatile SingularAttribute<TbLevelSurgery, String> levelName;
    public static volatile SingularAttribute<TbLevelSurgery, Date> createDate;
    public static volatile CollectionAttribute<TbLevelSurgery, TbSurgeryService> tbSurgeryServiceCollection;

}