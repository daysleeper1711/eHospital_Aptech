package project.ett;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import project.ett.TbEmployee;
import project.ett.TbTestReqDetail;
import project.ett.TbTestResult;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2017-02-06T16:29:05")
@StaticMetamodel(TbTestResultDetail.class)
public class TbTestResultDetail_ { 

    public static volatile SingularAttribute<TbTestResultDetail, String> results;
    public static volatile SingularAttribute<TbTestResultDetail, TbTestResult> testResultID;
    public static volatile SingularAttribute<TbTestResultDetail, Integer> testResultDetailID;
    public static volatile SingularAttribute<TbTestResultDetail, TbTestReqDetail> reqDetailID;
    public static volatile SingularAttribute<TbTestResultDetail, Date> createDate;
    public static volatile SingularAttribute<TbTestResultDetail, TbEmployee> updateEmp;
    public static volatile SingularAttribute<TbTestResultDetail, Date> updateDate;
    public static volatile SingularAttribute<TbTestResultDetail, TbEmployee> creator;

}