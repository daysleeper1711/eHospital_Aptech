/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dan.dto;

import java.io.Serializable;

/**
 *
 * @author DaySLeePer
 */
public class EmpInfo implements Serializable{
    private String employeeID, empName, faculty, position, facultyID;

    public EmpInfo() {
    }

    public EmpInfo(String employeeID, String faculty, String position) {
        this.employeeID = employeeID;
        this.faculty = faculty;
        this.position = position;
    }

    public String getEmployeeID() {
        return employeeID;
    }

    public void setEmployeeID(String employeeID) {
        this.employeeID = employeeID;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public String getFacultyID() {
        return facultyID;
    }

    public void setFacultyID(String facultyID) {
        this.facultyID = facultyID;
    }
    
}
