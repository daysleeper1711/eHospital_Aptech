package dan.dto;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import project.ett.TbEmployee;

public class MedicalRecordInfo implements Serializable {

    private final SimpleDateFormat formatDateTime = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
    //--- this is for the Medical Record indentity
    private String medRecordID, status;
    //--- this is for the registration from receptionist
    private String symptoms, createDate, createByWhom;
    //--- this is for diagnosis
    //--- status: "waiting for dianogsis"
    private String diagnosis, suggestTreatment, diagnosisDate, diagnosisDoc;
    //--- this is for in treatment
    //--- status: "in treatment"
    private String currentBed, currentFaculty, dateInHospital, dateOutHospital;
    private BedInfo bedInfo; //current bed info
    //--- this is for discharge
    private String dischargeStatus, mainDisease, sideDisease, treatmentResult, healthCondition;
    private String conclusionDate, conclusionByWhom;
    private String totalTreatmentDays;
    //--- this is information of patient
    private PatientInfo patInfo;
    //--- this is the list of faculty changement history
    private Fch fch_start;
    private List<Fch> listFCH_change;

    public String getMedRecordID() {
        return medRecordID;
    }

    public void setMedRecordID(String medRecordID) {
        this.medRecordID = medRecordID;
    }

    public String getStatus() {
        if (status == null) {
            return "N/A";
        }
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSymptoms() {
        if (symptoms == null) {
            return "N/A";
        }
        return symptoms;
    }

    public void setSymptoms(String symptoms) {
        this.symptoms = symptoms;
    }

    public String getCreateDate() {
        if (createDate == null) {
            return "N/A";
        }
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        if (createDate != null) {
            this.createDate = formatDateTime.format(createDate);
        }
    }

    public String getCreateByWhom() {
        if (createByWhom == null) {
            return "N/A";
        }
        return createByWhom;
    }

    public void setCreateByWhom(TbEmployee createByWhom) {
        if (createByWhom != null) {
            this.createByWhom = createByWhom.getFullname();
        }

    }

    public String getDiagnosis() {
        if (diagnosis == null) {
            return "N/A";
        }
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getSuggestTreatment() {
        if (suggestTreatment == null) {
            return "N/A";
        }
        return suggestTreatment;
    }

    public void setSuggestTreatment(String suggestTreatment) {
        this.suggestTreatment = suggestTreatment;
    }

    public String getDiagnosisDate() {
        if (diagnosisDate == null) {
            return "N/A";
        }
        return diagnosisDate;
    }

    public void setDiagnosisDate(Date diagnosisDate) {
        if (diagnosisDate != null) {
            this.diagnosisDate = formatDateTime.format(diagnosisDate);
        }

    }

    public String getDiagnosisDoc() {
        if (diagnosisDoc == null) {
            return "N/A";
        }
        return diagnosisDoc;
    }

    public void setDiagnosingDoc(TbEmployee diagnosisDoc) {
        if (diagnosisDoc != null) {
            this.diagnosisDoc = diagnosisDoc.getFullname();
        }
    }

    public PatientInfo getPatInfo() {
        return patInfo;
    }

    public void setPatInfo(PatientInfo patInfo) {
        this.patInfo = patInfo;
    }

    public String getCurrentBed() {
        if (currentBed == null) {
            return "N/A";
        }
        return currentBed;
    }

    public void setCurrentBed(String currentBed) {
        this.currentBed = currentBed;
    }

    public String getCurrentFaculty() {
        if (currentFaculty == null) {
            return "N/A";
        }
        return currentFaculty;
    }

    public void setCurrentFaculty(String currentFaculty) {
        this.currentFaculty = currentFaculty;
    }

    public String getDateInHospital() {
        if (dateInHospital == null) {
            return "N/A";
        }
        return dateInHospital;
    }

    public void setDateInHospital(Date dateInHospital) {
        if (dateInHospital != null) {
            this.dateInHospital = formatDateTime.format(dateInHospital);
        }
    }

    public String getDateOutHospital() {
        if (dateOutHospital == null) {
            return "N/A";
        }
        return dateOutHospital;
    }

    public void setDateOutHospital(Date dateOutHospital) {
        if (dateOutHospital != null) {
            this.dateOutHospital = formatDateTime.format(dateOutHospital);
        }
    }

    public BedInfo getBedInfo() {
        if (bedInfo != null) {
            return bedInfo;

        }
        return new BedInfo(); //avoid null
    }

    public void setBedInfo(BedInfo bedInfo) {
        if (bedInfo != null) {
            this.bedInfo = bedInfo;
        }
    }

    public String getDischargeStatus() {
        if (dischargeStatus == null) {
            return "N/A";
        }
        return dischargeStatus;
    }

    public void setDischargeStatus(String dischargeStatus) {
        this.dischargeStatus = dischargeStatus;
    }

    public String getMainDisease() {
        if (mainDisease == null) {
            return "N/A";
        }
        return mainDisease;
    }

    public void setMainDisease(String mainDisease) {
        this.mainDisease = mainDisease;
    }

    public String getSideDisease() {
        if (mainDisease == null) {
            return "N/A";
        }
        return sideDisease;
    }

    public void setSideDisease(String sideDisease) {
        this.sideDisease = sideDisease;
    }

    public String getTreatmentResult() {
        if (treatmentResult == null) {
            return "N/A";
        }
        return treatmentResult;
    }

    public void setTreatmentResult(String treatmentResult) {
        this.treatmentResult = treatmentResult;
    }

    public String getHealthCondition() {
        if (healthCondition == null) {
            return "N/A";
        }
        return healthCondition;
    }

    public void setHealthCondition(String healthCondition) {
        this.healthCondition = healthCondition;
    }

    public String getConclusionDate() {
        if (conclusionDate == null) {
            return "N/A";
        }
        return conclusionDate;
    }

    public void setConclusionDate(Date conclusionDate) {
        if (conclusionDate != null) {
            this.conclusionDate = formatDateTime.format(conclusionDate);
        }
    }

    public String getConclusionByWhom() {
        if (conclusionByWhom == null) {
            return "N/A";
        }
        return conclusionByWhom;
    }

    public void setConclusionByWhom(TbEmployee conclusionByWhom) {
        if (conclusionByWhom != null) {
            this.conclusionByWhom = conclusionByWhom.getFullname();
        }
    }

    public List<Fch> getListFCH_change() {
        //--- avoid null pointer
        if (listFCH_change == null) {
            return new ArrayList<>();
        }
        return listFCH_change;
    }

    public void setListFCH_change(List<Fch> listFCH_change) {
        if (listFCH_change != null) {
            this.listFCH_change = listFCH_change;
        }
    }

    public Fch getFch_start() {
        //--- avoid null pointer
        if (fch_start == null) {
            return new Fch();
        }
        return fch_start;
    }

    public void setFch_start(Fch fch_start) {
        if (fch_start != null) {
            this.fch_start = fch_start;
        }
    }

    public String getTotalTreatmentDays() {
        if (totalTreatmentDays == null) {
            return "N/A";
        }
        return totalTreatmentDays;
    }

    public void setTotalTreatmentDays(int totalTreatmentDays) {
        if (totalTreatmentDays < 0) {
            this.totalTreatmentDays = "N/A";
        }
        this.totalTreatmentDays = totalTreatmentDays + "";
    }

}//end Medical Record Info
