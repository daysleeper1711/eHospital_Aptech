/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dan.dto;

import dan.service.BedDTO;
import dan.service.DailyDrugDTO;
import dan.service.SurgeryDTO;
import dan.service.TestDTO;
import java.io.Serializable;

/**
 *
 * @author DaySLeePer
 */
public class TreatmentFee implements Serializable{
    //1 bed fee
    private BedDTO bedFee;
    private TestDTO testFee;
    private DailyDrugDTO dailyDrugFee;
    private SurgeryDTO surgeryFee;
    private double totalFee, totalDeposit, totalReturn, totalPaymore;
    
    public TreatmentFee() {
    }

    public BedDTO getBedFee() {
        return bedFee;
    }

    public void setBedFee(BedDTO bedFee) {
        this.bedFee = bedFee;
    }

    public double getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(double totalFee) {
        this.totalFee = totalFee;
    }

    public double getTotalDeposit() {
        return totalDeposit;
    }

    public void setTotalDeposit(double totalDeposit) {
        this.totalDeposit = totalDeposit;
    }

    public double getTotalReturn() {
        return totalReturn;
    }

    public void setTotalReturn(double totalReturn) {
        this.totalReturn = totalReturn;
    }

    public double getTotalPaymore() {
        return totalPaymore;
    }

    public void setTotalPaymore(double totalPaymore) {
        this.totalPaymore = totalPaymore;
    }

    public TestDTO getTestFee() {
        return testFee;
    }

    public void setTestFee(TestDTO testFee) {
        this.testFee = testFee;
    }

    public DailyDrugDTO getDailyDrugFee() {
        return dailyDrugFee;
    }

    public void setDailyDrugFee(DailyDrugDTO dailyDrugFee) {
        this.dailyDrugFee = dailyDrugFee;
    }

    public SurgeryDTO getSurgeryFee() {
        return surgeryFee;
    }

    public void setSurgeryFee(SurgeryDTO surgeryFee) {
        this.surgeryFee = surgeryFee;
    }
    
    @Override
    public String toString() {
        return String.format("Total Payement: %.2f - Total Deposit: %.2f - Total Return: %.2f - Total Paymore: %.2f", 
                this.totalFee, this.totalDeposit, this.totalReturn, this.totalPaymore);
    }
    
}
