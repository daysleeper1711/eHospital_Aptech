/*
 - This class is used to show the info of the patient
 */
package dan.dto;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import project.ett.TbEmployee;

/**
 *
 * @author DaySLeePer
 */
public class PatientInfo implements Serializable {

    private String patID, patName, dob, gender, address, phone, regDate, regByWhom, updateDate, updateByWhom, iDCard;
    private final SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");
    private final SimpleDateFormat formatDateTime = new SimpleDateFormat("dd/MM/yyyy hh:mm a");

    public PatientInfo() {
    }

    public String getPatID() {
        if (patID == null) {
            return "N/A";
        }
        return patID;
    }

    public void setPatID(String patID) {
        this.patID = patID;
    }

    public String getPatName() {
        if (patName == null) {
            return "N/A";
        }
        return patName;
    }

    public void setPatName(String patName) {
        this.patName = patName;
    }

    public String getDob() {
        if (dob == null) {
            return "N/A";
        }
        return dob;
    }

    //-format date
    public void setDob(Date dob) {
        if (dob != null) {
            this.dob = formatDate.format(dob);
        }
    }

    public String getGender() {
        if (gender == null) {
            return "N/A";
        }
        return gender;
    }

    //0 - false: Male, 1 - true: Female
    public void setGender(boolean gender) {
        if (!gender) {
            this.gender = "Male";
        } else {
            this.gender = "Female";
        }
    }

    public String getAddress() {
        if (address == null) {
            return "N/A";
        }
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        if (phone == null) {
            return "N/A";
        }
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRegDate() {
        if (regDate == null) {
            return "N/A";
        }
        return regDate;
    }

    //- Format datetime
    public void setRegDate(Date regDate) {
        if (regDate != null) {
            this.regDate = formatDateTime.format(regDate);
        }

    }

    public String getRegByWhom() {
        if (regByWhom == null) {
            return "N/A";
        }
        return regByWhom;
    }

    public void setRegByWhom(TbEmployee regByWhom) {
        if (regByWhom != null) {
            this.regByWhom = regByWhom.getFullname();
        }
    }

    public String getUpdateDate() {
        if (updateDate == null) {
            return "N/A";
        }
        return updateDate;
    }

    //- Format datetime
    public void setUpdateDate(Date updateDate) {
        if (updateDate != null) {
            this.updateDate = formatDateTime.format(updateDate);
        }

    }

    public String getUpdateByWhom() {
        if (updateByWhom == null) {
            return "N/A";
        }
        return updateByWhom;
    }

    public void setUpdateByWhom(TbEmployee updateByWhom) {
        if (updateByWhom != null) {
            this.updateByWhom = updateByWhom.getFullname();
        }
    }

    public String getiDCard() {
        if (iDCard == null) {
            return "N/A";
        }
        return iDCard;
    }

    public void setiDCard(String iDCard) {
        this.iDCard = iDCard;
    }

}
