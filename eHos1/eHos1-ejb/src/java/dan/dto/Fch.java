/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dan.dto;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import project.ett.TbEmployee;

/**
 *
 * @author DaySLeePer
 */
public class Fch implements Serializable {

    private String fromFaculty, toFaculty, dateIn, dateOut, acceptIn, acceptOut, status, reasonToChange;
    private BedInfo bed;
    private final SimpleDateFormat formatDateTime = new SimpleDateFormat("dd/MM/yyyy hh:mm a");

    public String getFromFaculty() {
        if (fromFaculty == null) {
            return "N/A";
        }
        return fromFaculty;
    }

    public void setFromFaculty(String fromFaculty) {
        this.fromFaculty = fromFaculty;
    }

    public String getToFaculty() {
        if (toFaculty == null) {
            return "N/A";
        }
        return toFaculty;
    }

    public void setToFaculty(String toFaculty) {
        this.toFaculty = toFaculty;
    }

    public String getDateIn() {
        if (dateIn == null) {
            return "N/A";
        }
        return dateIn;
    }

    public void setDateIn(Date dateIn) {
        if (dateIn != null) {
            this.dateIn = formatDateTime.format(dateIn);
        }
    }

    public String getDateOut() {
        if (dateOut == null) {
            return "N/A";
        }
        return dateOut;
    }

    public void setDateOut(Date dateOut) {
        if (dateOut != null) {
            this.dateOut = formatDateTime.format(dateOut);
        }
    }

    public String getAcceptIn() {
        if (acceptIn == null) {
            return "N/A";
        }
        return acceptIn;
    }

    public void setAcceptIn(TbEmployee acceptIn) {
        if (acceptIn != null) {
            this.acceptIn = acceptIn.getFullname();
        }
    }

    public String getAcceptOut() {
        if (acceptOut == null) {
            return "N/A";
        }
        return acceptOut;
    }

    public void setAcceptOut(TbEmployee acceptOut) {
        if (acceptOut != null) {
            this.acceptOut = acceptOut.getFullname();
        }

    }

    public String getStatus() {
        if (status == null) {
            return "N/A";
        }
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReasonToChange() {
        if (reasonToChange == null) {
            return "N/A";
        }
        return reasonToChange;
    }

    public void setReasonToChange(String reasonToChange) {
        this.reasonToChange = reasonToChange;
    }

    public BedInfo getBed() {
        //avoid null pointer
        if (bed == null) {
            return new BedInfo();
        }
        return bed;
    }

    public void setBed(BedInfo bed) {
        if (bed != null) {
            this.bed = bed;
        }
    }

}
