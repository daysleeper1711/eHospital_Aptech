/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dan.service;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author DaySLeePer
 */
public class DailyDrugDTO implements Serializable{
    private double totalPayment, totalPaymentForDrugs, totalPaymentForDrugsSupplies;
    private List<DailyDrugService> lstDrugService;
    private List<DailyDrugSuppliesService> lstDrugSuppliesService;
    public DailyDrugDTO() {
    }

    public double getTotalPayment() {
        return totalPayment;
    }

    public void setTotalPayment() {
        this.totalPayment = this.totalPaymentForDrugs + this.totalPaymentForDrugsSupplies;
    }

    public List<DailyDrugService> getLstDrugService() {
        return lstDrugService;
    }

    public void setLstDrugService(List<DailyDrugService> lstDrugService) {
        this.lstDrugService = lstDrugService;
    }

    public List<DailyDrugSuppliesService> getLstDrugSuppliesService() {
        return lstDrugSuppliesService;
    }

    public void setLstDrugSuppliesService(List<DailyDrugSuppliesService> lstDrugSuppliesService) {
        this.lstDrugSuppliesService = lstDrugSuppliesService;
    }

    public double getTotalPaymentForDrugs() {
        return totalPaymentForDrugs;
    }

    public void setTotalPaymentForDrugs() {
        for (DailyDrugService dds : lstDrugService) {
            this.totalPaymentForDrugs += dds.getTotal();
        }
    }

    public double getTotalPaymentForDrugsSupplies() {
        return totalPaymentForDrugsSupplies;
    }

    public void setTotalPaymentForDrugsSupplies() {
        for (DailyDrugSuppliesService ddss : lstDrugSuppliesService) {
            this.totalPaymentForDrugsSupplies += ddss.getTotal();
        }
    }
    
}
