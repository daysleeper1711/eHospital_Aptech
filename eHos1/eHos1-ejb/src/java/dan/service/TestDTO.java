/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dan.service;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author DaySLeePer
 */
public class TestDTO implements Serializable{
    private double totalPayment;
    private List<TestService> lstTestService;

    public TestDTO() {
    }

    public double getTotalPayment() {
        return totalPayment;
    }

    public void setTotalPayment() {
        for (TestService ts : lstTestService) {
            this.totalPayment += ts.getTotal();
        }
    }

    public List<TestService> getLstTestService() {
        return lstTestService;
    }

    public void setLstTestService(List<TestService> lstTestService) {
        this.lstTestService = lstTestService;
    }

    @Override
    public String toString() {
        return String.format("Total payment: %.2f", this.totalPayment);
    }
    
}
