/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dan.service;

import java.io.Serializable;

/**
 *
 * @author DaySLeePer
 */
public class BedService implements Serializable{
    private String name;
    private int quantity;
    private double price, total;

    public BedService() {
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal() {
        this.total = this.price * this.quantity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return String.format("Total days: %d days - Price per day: %.2f - Total payment: %.2f", this.quantity, this.price, this.total);
    }
}
