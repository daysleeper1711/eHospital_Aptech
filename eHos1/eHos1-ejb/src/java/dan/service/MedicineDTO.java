/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dan.service;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author DaySLeePer
 */
public class MedicineDTO implements Serializable{
    private String medRecordID;
    private List<Drug> listDrug;
    private List<DrugSupplies> listDrugSupplies;

    public MedicineDTO() {
    }

    public String getMedRecordID() {
        return medRecordID;
    }

    public void setMedRecordID(String medRecordID) {
        this.medRecordID = medRecordID;
    }

    public List<Drug> getListDrug() {
        return listDrug;
    }

    public void setListDrug(List<Drug> listDrug) {
        this.listDrug = listDrug;
    }

    public List<DrugSupplies> getListDrugSupplies() {
        return listDrugSupplies;
    }

    public void setListDrugSupplies(List<DrugSupplies> listDrugSupplies) {
        this.listDrugSupplies = listDrugSupplies;
    }
    
}
