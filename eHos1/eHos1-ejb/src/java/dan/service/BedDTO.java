/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dan.service;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author DaySLeePer
 */
public class BedDTO implements Serializable{
    private double totalPayment;
    private List<BedService> lstBedService;

    public BedDTO() {
    }

    public double getTotalPayment() {
        return totalPayment;
    }

    public void setTotalPayment() {
        for (BedService bs : lstBedService) {
            this.totalPayment += bs.getTotal();
        }
    }

    public List<BedService> getLstBedService() {
        return lstBedService;
    }

    public void setLstBedService(List<BedService> lstBedService) {
        this.lstBedService = lstBedService;
    }
    
}
