/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dan.service;

import java.io.Serializable;

/**
 *
 * @author DaySLeePer
 */
public class SurgerySupplies implements Serializable{
    private String name;
    private int quantity;
    private float total;

    public SurgerySupplies() {
    }

    public SurgerySupplies(String name, int quantity, float total) {
        this.name = name;
        this.quantity = quantity;
        this.total = total;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }
    
}
