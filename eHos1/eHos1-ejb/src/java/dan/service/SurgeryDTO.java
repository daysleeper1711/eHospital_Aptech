/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dan.service;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author DaySLeePer
 */
public class SurgeryDTO implements Serializable{
    private double totalPayment;
    private List<SurgeryService> lstSurgeryService;

    public double getTotalPayment() {
        return totalPayment;
    }

    public void setTotalPayment() {
        for (SurgeryService ss : lstSurgeryService) {
            this.totalPayment += ss.getTotal();
        }
    }

    public List<SurgeryService> getLstSurgeryService() {
        return lstSurgeryService;
    }

    public void setLstSurgeryService(List<SurgeryService> lstSurgeryService) {
        this.lstSurgeryService = lstSurgeryService;
    }
    
}
