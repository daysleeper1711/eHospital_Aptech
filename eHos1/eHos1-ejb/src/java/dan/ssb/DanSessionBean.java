/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dan.ssb;

import dan.dto.BedInfo;
import dan.dto.EmpInfo;
import dan.dto.Fch;
import dan.dto.PatientInfo;
import dan.dto.MedicalRecordInfo;
import dan.dto.TreatmentFee;
import dan.service.BedDTO;
import dan.service.BedService;
import dan.service.DailyDrugDTO;
import dan.service.DailyDrugService;
import dan.service.DailyDrugSuppliesService;
import dan.service.SurgeryDTO;
import dan.service.SurgeryService;
import dan.service.TestDTO;
import dan.service.TestService;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.print.attribute.standard.MediaSize;
import project.ett.TbBed;
import project.ett.TbDepositPayment;
import project.ett.TbDrugsDetail;
import project.ett.TbEmployee;
import project.ett.TbFaculty;
import project.ett.TbFacultyChangementHistory;
import project.ett.TbMedicalRecord;
import project.ett.TbMedicineSupplies;
import project.ett.TbMedicineSuppliesDetail;
import project.ett.TbPatient;
import project.ett.TbPrescription;
import project.ett.TbPrescriptionDetail;
import project.ett.TbService;
import project.ett.TbServicePrice;
import project.ett.TbSuggestSurgery;
import project.ett.TbSurgeryRecord;
import project.ett.TbSurgeryService;
import project.ett.TbTestReqDetail;
import project.ett.TbTestRequest;
import project.ett.TbTreatmentFeeEnumeration;

@Stateless(name = "DanSSB")
@LocalBean
public class DanSessionBean {

    //--------Final variable-------------------
    //--- Patient entity
    private final String TB_PATIENT = "TbPatient t";
    private final String COL_PATIENT_ID = "t.patID";
    private final String COL_PATIENT_NAME = "t.patName";
    private final String COL_ID_CARD = "t.iDCard";
    private final String COL_CREATEDATE_PAT = "t.regDate";
    //--- Medical Record entity
    private final String TB_MEDICALRECORD = "TbMedicalRecord t";
    private final String COL_MEDRECORD_ID = "t.medRecordID";
    private final String COL_CREATEDATE_MED = "t.createDate";
    //--- FCH entity
    private final String TB_FCH = "TbFacultyChangementHistory t";
    private final String COL_FROM_FA = "t.fromFaculty";
    private final String COL_TO_FA = "t.toFaculty";
    private final String COL_FCH_STATUS = "t.status";
    private final String COL_FCH_DATEIN = "t.dateIn";
    private final String COL_FCH_DATEOUT = "t.dateOut";
    //--- Medical Record status
    private final String DIAGNOSIS_MR = "waiting for diagnosis"; //after create medical record
    private final String FINISHED_DIAGNOSIS_MR = "finished diagnosis"; //get the diagnosis
    private final String IN_TREATMENT_MR = "in treatment"; //get bed assignment
    private final String DISCHARGE_MR = "discharge"; //has permission to discharge from doctor
    private final String FULLY_DISCHARGE_MR = "fully discharge"; //after discharge's payments
    //--- Deposit status
    private final String DEPOSIT_CURRENT = "active";
    private final String DEPOSIT_PAST = "inactive";
    //--- FCH status
    private final String FCH_BEGIN = "in";
    private final String FCH_CHANGE = "change";
    private final String FCH_OUT = "out";
    //--- Bed status
    private final String BED_FREE = "free";
    private final String BED_OCCUPIED = "occupied";
    private final String BED_MAIN = "Main bed";
    private final String BED_SUB = "Sub bed";
    //--------------END------------------------

    //------- Entity manager --------------------
    @PersistenceContext(unitName = "eHos1-ejbPU")
    private EntityManager em;
    //--------------END------------------------

    //------Search---------------------
     /*
     - Enter patID and return TbPatient
     */
    public PatientInfo searchByPatID(String patID) {
        try {
            TbPatient result = em.find(TbPatient.class, patID);
            return transToPatientView(result);
        } catch (Exception e) {
        }
        return null; //if fails
    }//end searchByPatID
    /*
     - Enter ID_Card number and return TbPatient
     */

    public PatientInfo searchByIDCard(String iDCard) {
        try {
            String ejbQuery = "SELECT t FROM " + TB_PATIENT + " "
                    + "WHERE " + COL_ID_CARD + " = :iDCard";
            TbPatient result = (TbPatient) em.createQuery(ejbQuery)
                    .setParameter("iDCard", iDCard)
                    .getSingleResult();
            return transToPatientView(result);
        } catch (Exception e) {
        }
        return null;
    }//end searchByIDCard

    /*
     - Enter Patient Name and get the result list
     */
    public List<PatientInfo> searchByName(String patName) {
        try {
            List<PatientInfo> result = new ArrayList<>();
            //--- incase the pass the empty string to show all the info
            if (patName.equals("")) {
                return result;
            }
            //query search like name
            String ejbQuery = "SELECT t FROM " + TB_PATIENT + " "
                    + "WHERE " + COL_PATIENT_NAME + " LIKE :patName";
            List<TbPatient> resultList = (List<TbPatient>) em.createQuery(ejbQuery)
                    .setParameter("patName", "%" + patName + "%")
                    .getResultList();
            for (TbPatient tbPatient : resultList) {
                result.add(transToPatientView(tbPatient));
            }
            return result;
        } catch (Exception e) {
        }
        return null;
    }//end searchByName
    /*
     - Find Medical Record by ID that need to deposit
     */

    public MedicalRecordInfo searchDepositMR(String medRecordID) {
        try {
            TbMedicalRecord mr = em.find(TbMedicalRecord.class, medRecordID);
            if (!mr.getIsDeposit()) {
                return transToMRView(medRecordID);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }//end class searchMRByID
    /*
     - Get patient info from the Medical Record 
     */

    public PatientInfo patient_MR(String medRecordID) {
        try {
            TbMedicalRecord mr = em.find(TbMedicalRecord.class, medRecordID);
            return transToPatientView(mr.getPatID());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }//end patient_MR
    /*
     - Get bed info from the Medical Record 
     */

    public BedInfo bedInfo_MR(String medRecordID) {
        try {
            //--- get the current bed name
            if (!currentBed(medRecordID).equals("N/A")) {
                TbBed bed = em.createNamedQuery("TbBed.findByBedName", TbBed.class)
                        .setParameter("bedName", currentBed(medRecordID))
                        .getSingleResult();
                return transToBedView(bed);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }//end patient_MR
    /*
     - Get bed info from bed name
     */

    public BedInfo bedInfo_name(String bedName) {
        BedInfo bedInfo = new BedInfo();
        try {
            if (bedName != null) {
                TbBed bed = em.createNamedQuery("TbBed.findByBedName", TbBed.class)
                        .setParameter("bedName", bedName)
                        .getSingleResult();
                if (bed != null) {
                    bedInfo = transToBedView(bed);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bedInfo;
    }//end bedInfo_name
    /*
     - Get fch info from the Medical Record
     */

    public List<Fch> fchInfo_MR(String medRecordID) {
        List<Fch> listResult = new ArrayList<>();
        try {
            TbMedicalRecord mr = em.find(TbMedicalRecord.class, medRecordID);
            List<TbFacultyChangementHistory> listFCH
                    = (List<TbFacultyChangementHistory>) mr.getTbFacultyChangementHistoryCollection();
            for (TbFacultyChangementHistory fch : listFCH) {
                if (!fch.getStatus().equals(FCH_OUT)) {
                    Fch result = new Fch();
                    result.setFromFaculty(fch.getFromFaculty());
                    result.setToFaculty(fch.getToFaculty());
                    result.setBed(bedInfo_name(fch.getBedName()));
                    result.setDateIn(fch.getDateIn());
                    result.setAcceptIn(fch.getAcceptInBywhom());
                    result.setDateOut(fch.getDateOut());
                    result.setAcceptOut(fch.getAcceptOutByWhom());
                    result.setStatus(fch.getStatus());
                    result.setReasonToChange(fch.getReasonToChange());
                    listResult.add(result);
                }
            }
            return listResult;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }//end fchInfo_MR
    /*
     - Get all Medical Record which status is "waiting for diagnosis"
     - Faculty is match with the employee faculty
     */

    public List<MedicalRecordInfo> diagnosisMR(String facultyName) {
        try {
            List<TbMedicalRecord> listEntity = (List<TbMedicalRecord>) em.createNamedQuery("TbMedicalRecord.findByStatus")
                    .setParameter("status", DIAGNOSIS_MR)
                    .getResultList();
            List<MedicalRecordInfo> resultList = new ArrayList<>();
            for (TbMedicalRecord mr : listEntity) {
                String currentFaculty = currentFaculty(mr.getMedRecordID());
                System.out.println("Current faculty diagnosis: " + currentFaculty); //test...
                if (facultyName.equals(currentFaculty)) {
                    resultList.add(transToMRView(mr.getMedRecordID()));
                }
            }
            return resultList;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }//end diagnosisMR
    /*
<<<<<<< HEAD
     - Get all Medical Record which status is "discharge"
=======
     - Get all Medical Record which status is "in treatment"
>>>>>>> c58fa75acaf54f8595581802e720eba751610dc9
     - Faculty is match with the employee's faculty
     */

    public List<MedicalRecordInfo> inTreamentMR(String facultyName) {
        try {
            List<TbMedicalRecord> listEntity = (List<TbMedicalRecord>) em.createNamedQuery("TbMedicalRecord.findByStatus")
                    .setParameter("status", IN_TREATMENT_MR)
                    .getResultList();
            List<MedicalRecordInfo> resultList = new ArrayList<>();
            for (TbMedicalRecord mr : listEntity) {
                String currentFaculty = currentFaculty(mr.getMedRecordID());
                if (facultyName.equals(currentFaculty)) {
                    resultList.add(transToMRView(mr.getMedRecordID()));
                }
            }
            return resultList;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }//end diagnosisMR
    /*
     - Get all Medical Record which status is "discharge"
     - Faculty is match with the employee's faculty
     */

    public List<MedicalRecordInfo> dischargeMR(String facultyName) {
        try {
            List<TbMedicalRecord> listEntity = (List<TbMedicalRecord>) em.createNamedQuery("TbMedicalRecord.findByStatus")
                    .setParameter("status", DISCHARGE_MR)
                    .getResultList();
            List<MedicalRecordInfo> resultList = new ArrayList<>();
            for (TbMedicalRecord mr : listEntity) {
                String currentFaculty = currentFaculty(mr.getMedRecordID());
                if (facultyName.equals(currentFaculty)) {
                    resultList.add(transToMRView(mr.getMedRecordID()));
                }
            }
            return resultList;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }//end dischargeMR
    /*
     - Get all Medical Record which status is "finished diagnosis" and isDeposit - true
     - For assign bed
     */

    public List<MedicalRecordInfo> waitingForBedMR(String facultyName) {
        try {
            //--- search all the medical record with status "finished diagnosis"
            List<TbMedicalRecord> listEntity = (List<TbMedicalRecord>) em.createNamedQuery("TbMedicalRecord.findByStatus")
                    .setParameter("status", FINISHED_DIAGNOSIS_MR)
                    .getResultList();
            List<MedicalRecordInfo> resultList = new ArrayList<>();
            for (TbMedicalRecord mr : listEntity) {
                //--- check the current faculty of medical record is match the faculty of the employee
                String currentFaculty = currentFaculty(mr.getMedRecordID());
                //--- check condition  if currentFaculty and faculty employee are the same
                if (facultyName.equals(currentFaculty)) {
                    //--- check condition if the change event has not happened
                    if (!checkChangeFacultyEvent(mr.getMedRecordID())) {
                        //check deposit is paid or not
                        if (mr.getIsDeposit()) {
                            resultList.add(transToMRView(mr.getMedRecordID()));
                        }
                    } else {
                        resultList.add(transToMRView(mr.getMedRecordID()));
                    }
                }
            }
            return resultList;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }//end class inTreatmentMR
    //--------------END------------------------

    //-------------Insert action---------------
    /*
     - Insert patient profile
     */
    public boolean insertPatient(TbPatient insPat, String employeeID) {
        try {
            insPat.setRegDate(new Date());
            TbEmployee emp = em.find(TbEmployee.class, employeeID);
            insPat.setRegByWhom(emp);
            emp.getTbPatientCollection().add(insPat);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }//end insertPatient
    /*
     - insert new Medical Record with symptoms field
     */

    public boolean insertMR(String patID, String medRecordID, String symptoms, String employeeID) {
        try {
            TbMedicalRecord mr = new TbMedicalRecord(medRecordID);
            mr.setSymptoms(symptoms);
            //---set status into "waiting for dianogsis"
            mr.setStatus(DIAGNOSIS_MR);
            //--- set current date is the create date
            mr.setCreateDate(new Date());
            //--- set isDeposit - false
            mr.setIsDeposit(false);
            //--- set the create employee
            TbEmployee employee = em.find(TbEmployee.class, employeeID);
            mr.setCreateByWhom(employee);
            employee.getTbMedicalRecordCollection().add(mr);
            //--- set patient
            TbPatient patient = em.find(TbPatient.class, patID);
            mr.setPatID(patient);
            patient.getTbMedicalRecordCollection().add(mr);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }//end insertMR
    /*
     - Start when the patient registration complete to know where the patient will go to treatment
     - Insert new record into TbFacultyChangementHistory
     */

    public boolean inHouse(String facultyID, String medRecordID) {
        try {
            TbFacultyChangementHistory fch = new TbFacultyChangementHistory();
            fch.setFromFaculty("N/A");
            fch.setToFaculty(facultyID);
            //--- set 1sh status "start"
            fch.setStatus(FCH_BEGIN);
            TbMedicalRecord mr = em.find(TbMedicalRecord.class, medRecordID);
            fch.setMedRecordID(mr);
            mr.getTbFacultyChangementHistoryCollection().add(fch);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }//end inHouse
    /*
     - record the deposit payment
     - recognize the 1st payment by check the deposit record has been created or not
     */

    public boolean depositConfirm(String medRecordID, double amount, String employeeID) {
        try {
            TbMedicalRecord mr = em.find(TbMedicalRecord.class, medRecordID);
            TbEmployee emp = em.find(TbEmployee.class, employeeID);
            List<TbDepositPayment> listDeposit
                    = (List<TbDepositPayment>) mr.getTbDepositPaymentCollection();
            //--- this is first time payment
            if (listDeposit == null) {
                TbDepositPayment deposit = new TbDepositPayment();
                //---set status
                deposit.setStatus(DEPOSIT_CURRENT);
                //set amount 
                deposit.setAmount(amount);
                //set remaining
                deposit.setRemaining(amount);
                //---set received date
                deposit.setReceivedDate(new Date());
                //set medical record
                deposit.setMedRecordID(mr);
                mr.getTbDepositPaymentCollection().add(deposit);
                //set employee
                deposit.setReceivedByWhom(emp);
                emp.getTbDepositPaymentCollection().add(deposit);
            } else { //--- this is not the first time to pay the deposit
                double lastRemaining = 0;
                for (TbDepositPayment deposit : listDeposit) {
                    if (deposit.getStatus().equals(DEPOSIT_CURRENT)) {
                        //--- set the status of active to inactive
                        deposit.setStatus(DEPOSIT_PAST);
                        //--- set the lastRemaining
                        lastRemaining += deposit.getRemaining();
                        //--- save the deposit
                        em.persist(deposit);
                    }
                }//--- handle the last deposit
                //--- create new deposit
                TbDepositPayment newDeposit = new TbDepositPayment();
                //--- set the status for new deposit
                newDeposit.setStatus(DEPOSIT_CURRENT);
                //--- set the new amount
                newDeposit.setAmount(amount);
                //--- set the new remaining
                newDeposit.setRemaining(amount + lastRemaining);
                //---set received date
                newDeposit.setReceivedDate(new Date());
                //--- set employee
                newDeposit.setReceivedByWhom(emp);
                emp.getTbDepositPaymentCollection().add(newDeposit);
                //--- set medical record ID
                newDeposit.setMedRecordID(mr);
                mr.getTbDepositPaymentCollection().add(newDeposit);
            }
            //update isDeposit to true
            mr.setIsDeposit(true);
            em.persist(mr);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }//end depositConfirm
    /*
     - Insert treatmentFeeEnumeration + treatmentFeeEnumerationDetail
     */

    public boolean insTreatmentFee(String medRecordID) {
        try {
            TbMedicalRecord mr = em.find(TbMedicalRecord.class, medRecordID);
            if (mr.getStatus().equals(DISCHARGE_MR)) {
                TbTreatmentFeeEnumeration tfe = new TbTreatmentFeeEnumeration();
                tfe.setMedRecordID(mr);
                mr.getTbTreatmentFeeEnumerationCollection().add(tfe);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }//end insTreatmentFee

    /*
     - Search faculty entity by name
     */
    public String searchFacultyIDByName(String facultyName) {
        try {
            TbFaculty faculty = em.createNamedQuery("TbFaculty.findByFacultyName", TbFaculty.class)
                    .setParameter("facultyName", facultyName)
                    .getSingleResult();
            return faculty.getFaID();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }//end searchFacultyIDByName
    //--------------END------------------------

    //-------------Update action---------------
    /*
     - Input diagnosis medical record
     */
    public boolean diagnosisInput(String employeeID, String medRecordID, String diagnosis, String suggestTreatment) {
        try {
            TbMedicalRecord mr = em.find(TbMedicalRecord.class, medRecordID);
            mr.setDiagnosis(diagnosis);
            mr.setSuggestTreatment(suggestTreatment);
            mr.setDiagnosisDate(new Date());
            //--- set status "finished diagnosis"
            mr.setStatus(FINISHED_DIAGNOSIS_MR);
            //--- set diagnosis doc
            TbEmployee emp = em.find(TbEmployee.class, employeeID);
            mr.setDiagnosisDoc(emp);
            emp.getTbMedicalRecordCollection2().add(mr);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }//end diagnosisInput
    /*
     - Update bed status "occupied"
     - Update medical record status "in treatment"
     - Update fch bedID, dateIn, acceptIn (employee)
     */

    public boolean bedAllocation(String bedID, String medRecordID, String employeeID) {
        try {
            //--- update bed status
            TbBed bed = em.find(TbBed.class, Integer.parseInt(bedID));
            bed.setStatus(BED_OCCUPIED);
            em.persist(bed);
            //--- update medical record status
            TbMedicalRecord mr = em.find(TbMedicalRecord.class, medRecordID);
            mr.setStatus(IN_TREATMENT_MR);
            em.persist(mr);
            //--- get employee entity
            TbEmployee emp = em.find(TbEmployee.class, employeeID);
            //--- get list of fch
            List<TbFacultyChangementHistory> listFCH
                    = (List<TbFacultyChangementHistory>) mr.getTbFacultyChangementHistoryCollection();
            for (TbFacultyChangementHistory fch : listFCH) {
                //--- if any dateIn is null, set dateIn
                if (fch.getDateIn() == null) {
                    fch.setDateIn(new Date());
                    fch.setBedName(bed.getBedName());
                    fch.setAcceptInBywhom(emp);
                    emp.getTbFacultyChangementHistoryCollection2().add(fch);
                    return true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }//end bedAllocation
    /*
     - change faculty confirmation
     - update old record reason to change, date out, accept out
     - status is "change"
     - set bed status is "free"
     */

    public boolean changeFaculty(String medRecordID, String employeeID, String facultyChange, String reasonToChange) {
        //--- find medical record entity
        TbMedicalRecord mr = em.find(TbMedicalRecord.class, medRecordID);
        //--- find the current employee
        TbEmployee emp = em.find(TbEmployee.class, employeeID);
        //--- current faculty
        String currentFaculty = currentFaculty(medRecordID);
        //--- lastFCH
        TbFacultyChangementHistory lastFCH = new TbFacultyChangementHistory();
        try {
            //--- check patient does not discharge yet
            if (!mr.getStatus().equals(DISCHARGE_MR)) {
                //--- get list of fch
                List<TbFacultyChangementHistory> listFCH
                        = (List<TbFacultyChangementHistory>) mr.getTbFacultyChangementHistoryCollection();
                List<TbFacultyChangementHistory> listFCH_change = new ArrayList<>();
                //---examine the listFCH
                for (TbFacultyChangementHistory fch : listFCH) {
                    //--- if any fch got status "change" add to listFCH_change
                    if (fch.getStatus().equals(FCH_CHANGE)) {
                        listFCH_change.add(fch);
                    }
                }
                //--- condition listFCH_change is empty
                //--- which means the change faculty is not occur
                //--- the listFCH just have 1 record which status is "in"
                if (listFCH_change.isEmpty()) {
                    lastFCH = listFCH.get(0);
                } else { //--- when the list change is not empty
                    Collections.sort(listFCH_change, new Comparator<TbFacultyChangementHistory>() {
                        @Override
                        public int compare(TbFacultyChangementHistory o1, TbFacultyChangementHistory o2) {
                            return o1.getDateIn().compareTo(o2.getDateIn());
                        }
                    });
                    //--- the last fch is the last elements in listFCH_change
                    int size = listFCH_change.size();
                    lastFCH = listFCH_change.get(size - 1);
                }
                if (lastFCH != null) {
                    //---------------------------
                    //--- UPDATE last record
                    //---------------------------
//                    //--- set reason to change
//                    lastFCH.setReasonToChange(reasonToChange);
                    //--- set date out faculty
                    lastFCH.setDateOut(new Date());
                    //--- set employee accept out
                    lastFCH.setAcceptOutByWhom(emp);
                    //--- persist to database
                    emp.getTbFacultyChangementHistoryCollection().add(lastFCH);
                    //--- mr persist to database
                    mr.getTbFacultyChangementHistoryCollection().add(lastFCH);
                    //--- get current bedID
                    int currentBedID = Integer.parseInt(bedInfo_MR(medRecordID).getBedID());
                    //--- get Bed entity 
                    TbBed bed = em.find(TbBed.class, currentBedID);
                    //--- set bed status into "free" for main bed
                    if (bedInfo_MR(medRecordID).getBedType().equals(BED_MAIN)) {
                        bed.setStatus(BED_FREE);
                        //--- in case the bed is sub bed
                    } else if (bedInfo_MR(medRecordID).getBedType().equals(BED_SUB)) {
                        bed.setIsEnable(false);
                        bed.setStatus(BED_FREE);
                    }
                    em.persist(bed);
                    //--- mr set status
                    mr.setStatus(FINISHED_DIAGNOSIS_MR);
                    em.persist(mr);
                    //---------------------------
                    //--- INSERT new record
                    //---------------------------
                    //--- set fch_change
                    TbFacultyChangementHistory fch_change = new TbFacultyChangementHistory();
                    //--- set from faculty
                    fch_change.setFromFaculty(currentFaculty);
                    //--- set to Faculty
                    fch_change.setToFaculty(facultyChange);
                    //--- set reason to change
                    fch_change.setReasonToChange(reasonToChange);
                    //--- set status to "change"
                    fch_change.setStatus(FCH_CHANGE);
                    //--- set medical record ID
                    fch_change.setMedRecordID(mr);
                    mr.getTbFacultyChangementHistoryCollection().add(fch_change);
                    //--- return true for success
                    return true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }//end changeFaculty
    /*
     - Update treatment conclusion into medical record
     - Update mainDisease, sideDisease, treatmentResult, healthCondition
     - auto set dischargeStatus = "permission to discharge"
     - auto set medical record status = "discharged"
     - set dateOut, acceptOut from FCH
     - insert new record into FCH which status is "out", fromFaculty is current faculty, others are null
     */

    public boolean conclusion(String medRecordID, String employeeID, String mainDisease, String sideDisease, String treatmentResult, String healthCondition) {
        try {
            //--- get current medical record ID
            TbMedicalRecord mr = em.find(TbMedicalRecord.class, medRecordID);
            //--- get current employee
            TbEmployee emp = em.find(TbEmployee.class, employeeID);
            //--- current faculty
            String currentFaculty = currentFaculty(medRecordID);
            //--- lastFCH
            TbFacultyChangementHistory lastFCH = new TbFacultyChangementHistory();
            //--- check patient does not discharge yet
            if (!mr.getStatus().equals(DISCHARGE_MR)) {
                //--- get list of fch
                List<TbFacultyChangementHistory> listFCH
                        = (List<TbFacultyChangementHistory>) mr.getTbFacultyChangementHistoryCollection();
                List<TbFacultyChangementHistory> listFCH_change = new ArrayList<>();
                //---examine the listFCH
                for (TbFacultyChangementHistory fch : listFCH) {
                    //--- if any fch got status "change" add to listFCH_change
                    if (fch.getStatus().equals(FCH_CHANGE)) {
                        listFCH_change.add(fch);
                    }
                }
                //--- condition listFCH_change is empty
                //--- which means the change faculty is not occur
                //--- the listFCH just have 1 record which status is "in"
                if (listFCH_change.isEmpty()) {
                    lastFCH = listFCH.get(0);

                } else { //--- when the list change is not empty
                    Collections.sort(listFCH_change, new Comparator<TbFacultyChangementHistory>() {
                        @Override
                        public int compare(TbFacultyChangementHistory o1, TbFacultyChangementHistory o2) {
                            return o1.getDateIn().compareTo(o2.getDateIn());
                        }
                    });
                    //--- the last fch is the last elements in listFCH_change
                    int size = listFCH_change.size();
                    lastFCH = listFCH_change.get(size - 1);
                }
                //---UPDATE last fch
                lastFCH.setDateOut(new Date());
                lastFCH.setAcceptOutByWhom(emp);
                emp.getTbFacultyChangementHistoryCollection().add(lastFCH);
                //--- change status of the bed
                BedInfo bedInfo = bedInfo_MR(medRecordID);
                if (bedInfo.getBedType().equals(BED_MAIN)) { //--- for the main bed
                    TbBed bed = em.find(TbBed.class, Integer.parseInt(bedInfo.getBedID()));
                    bed.setStatus(BED_FREE);
                    em.persist(bed);
                } else { //--- for the sub bed
                    //--- set the isEnable to false for sub bed
                    TbBed bed = em.find(TbBed.class, Integer.parseInt(bedInfo.getBedID()));
                    bed.setStatus(BED_FREE);
                    bed.setIsEnable(false);
                    em.persist(bed);
                }
                //--- INSERT new fch
                TbFacultyChangementHistory fch_out = new TbFacultyChangementHistory();
                fch_out.setFromFaculty(currentFaculty);
                fch_out.setStatus(FCH_OUT);
                fch_out.setMedRecordID(mr);
                //--- disable all the deposit
                List<TbDepositPayment> listDeposit
                        = (List<TbDepositPayment>) mr.getTbDepositPaymentCollection();
                for (TbDepositPayment deposit : listDeposit) {
                    if (deposit.getStatus().equals(DEPOSIT_CURRENT)) {
                        deposit.setStatus(DEPOSIT_PAST);
                        em.persist(deposit);
                    }
                }
                //--- update medicalRecord
                mr.setMainDisease(mainDisease);
                mr.setSideDisease(sideDisease);
                mr.setTreatmentResult(treatmentResult);
                mr.setHealthCondition(healthCondition);
                mr.setDischargeStatus("permission to discharge");
                mr.setStatus(DISCHARGE_MR);
                mr.setConclusionDate(new Date());
                mr.setConclusionByWhom(emp);
                emp.getTbMedicalRecordCollection().add(mr);
                mr.getTbFacultyChangementHistoryCollection().add(fch_out);
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
        //--------------END------------------------

    //-------------------Utilities------------------------
        /*
     - Transform patient to view information
     */
    private PatientInfo transToPatientView(TbPatient patient) {
        PatientInfo patientView = new PatientInfo();
        try {
            if (patient != null) {
                //set item into view
                patientView.setPatID(patient.getPatID());
                patientView.setPatName(patient.getPatName());
                patientView.setDob(patient.getDob());
                patientView.setGender(patient.getGender());
                patientView.setAddress(patient.getAddress());
                patientView.setPhone(patient.getPhone());
                patientView.setiDCard(patient.getIDCard());
                patientView.setRegDate(patient.getRegDate());
                patientView.setUpdateDate(patient.getUpdateDate());
                patientView.setRegByWhom(patient.getRegByWhom());
                patientView.setUpdateByWhom(patient.getUpdateByWhom());
                return patientView;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }//end class transform
    /*
     - Transform medical record entity to view information
     */

    public MedicalRecordInfo transToMRView(String medRecordID) {
        MedicalRecordInfo mrView = new MedicalRecordInfo();
        TbMedicalRecord mr = em.find(TbMedicalRecord.class, medRecordID);
        try {
            if (mr != null) {
                //--- set item into view
                mrView.setMedRecordID(mr.getMedRecordID());
                mrView.setSymptoms(mr.getSymptoms());
                mrView.setCreateDate(mr.getCreateDate());
                mrView.setCreateByWhom(mr.getCreateByWhom());
                mrView.setDiagnosis(mr.getDiagnosis());
                mrView.setDiagnosisDate(mr.getDiagnosisDate());
                mrView.setDiagnosingDoc(mr.getDiagnosisDoc());
                mrView.setSuggestTreatment(mr.getSuggestTreatment());
                mrView.setStatus(mr.getStatus());
                //--- set patient DTO depends on medical record ID
                mrView.setPatInfo(patient_MR(mr.getMedRecordID()));
                //--- set current faculty
                mrView.setCurrentFaculty(currentFaculty(medRecordID));
                //--- set current bed
                mrView.setCurrentBed(currentBed(medRecordID));
                //--- set date in
                mrView.setDateInHospital(dateInHospital(medRecordID));
                //--- set date out hospital
                mrView.setDateOutHospital(mr.getConclusionDate());
                //--- set the current bed
                mrView.setBedInfo(bedInfo_MR(medRecordID));
                //--- set conclusion
                mrView.setMainDisease(mr.getMainDisease());
                mrView.setSideDisease(mr.getSideDisease());
                mrView.setHealthCondition(mr.getHealthCondition());
                mrView.setTreatmentResult(mr.getTreatmentResult());
                mrView.setDischargeStatus(mr.getDischargeStatus());
                mrView.setConclusionDate(mr.getConclusionDate());
                mrView.setConclusionByWhom(mr.getConclusionByWhom());
                //--- set total treatment days
                mrView.setTotalTreatmentDays(totalTreatmentDays(medRecordID));
                //--- set the fch
                List<Fch> listFCH = fchInfo_MR(medRecordID);
                List<Fch> listFCH_change = new ArrayList<>();
                for (Fch fch : listFCH) {
                    if (fch.getStatus().equals(FCH_BEGIN)) {
                        mrView.setFch_start(fch); //set fch start
                    } else if (fch.getStatus().equals(FCH_CHANGE)) {
                        listFCH_change.add(fch);
                    }
                }
                //--- set fch change
                mrView.setListFCH_change(listFCH_change);
                return mrView;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }//end class transform
    /*
     - Transform bed entity to bed info
     - just the available bed is transform. Main bed + enabled sub-bed
     */

    public BedInfo transToBedView(TbBed bed) {
        BedInfo bedView = new BedInfo();
        try {
            if (bed != null) {
                //--- set bed ID
                bedView.setBedID(bed.getBedID().toString());
                //--- set bed name
                bedView.setBedName(bed.getBedName());
                //---set bed type
                if (!bed.getIsSubbed()) {
                    bedView.setBedType(BED_MAIN);
                } else {
                    bedView.setBedType(BED_SUB);
                }
                //--- set bed status
                bedView.setStatus(bed.getStatus());
                return bedView;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }//end class transform
    /*
     - Generate patID
     - Format: PAT-01012017-1
     */

    public String generatePatID() {
        try {
            String constPrefix = "PAT-";
            StringBuilder result = new StringBuilder();
            //get the current date
            SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
            String currentDate = sdf.format(new Date());
            //query select by max date
            String getMaxCreateDateQL = "SELECT MAX(" + COL_CREATEDATE_PAT + ") FROM " + TB_PATIENT;
            Date maxDate = em.createQuery(getMaxCreateDateQL, Date.class)
                    .getSingleResult();
            //get the lastest patient ID
            if (maxDate == null) {
                result.append(constPrefix).append(currentDate).append("-").append("1");
            } else {
                //get the lastest Patient id
                String ejbQL = "SELECT " + COL_PATIENT_ID + " FROM " + TB_PATIENT + " "
                        + "WHERE " + COL_CREATEDATE_PAT + " = :maxDate";
                String patID = (String) em.createQuery(ejbQL)
                        .setParameter("maxDate", maxDate)
                        .getSingleResult();
                String number = patID.substring(13);
                int digit = Integer.parseInt(number) + 1;
                result.append(constPrefix).append(currentDate).append("-").append(digit);
            }
            return result.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }//end generatePatID
    /*
     - Generate medRecordID
     - Format: MR-01012017-1
     */

    public String generateMedRecordID() {
        try {
            String constPrefix = "MR-";
            StringBuilder result = new StringBuilder();
            //get the current date
            SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
            String currentDate = sdf.format(new Date());
            //query select by max date
            String getMaxCreateDateQL = "SELECT MAX(" + COL_CREATEDATE_MED + ") FROM " + TB_MEDICALRECORD;
            Date maxDate = em.createQuery(getMaxCreateDateQL, Date.class)
                    .getSingleResult();
            //get the lastest patient ID
            if (maxDate == null) {
                result.append(constPrefix).append(currentDate).append("-").append("1");
            } else {
                //get the lastest Patient id
                String ejbQL = "SELECT " + COL_MEDRECORD_ID + " FROM " + TB_MEDICALRECORD + " "
                        + "WHERE " + COL_CREATEDATE_MED + " = :maxDate";
                String patID = (String) em.createQuery(ejbQL)
                        .setParameter("maxDate", maxDate)
                        .getSingleResult();
                String number = patID.substring(12);
                int digit = Integer.parseInt(number) + 1;
                result.append(constPrefix).append(currentDate).append("-").append(digit);
            }
            return result.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }//end generatePatID
    /*
     - Get list faculty name
     */

    public List<String> listFacultyName() {
        List<String> resultList = new ArrayList<>();
        try {
            List<TbFaculty> findAll = (List<TbFaculty>) em.createNamedQuery("TbFaculty.findAll").getResultList();
            for (TbFaculty tbFaculty : findAll) {
                //--- extermine the faculty which is not the main function hospital faculty...
                if (tbFaculty.getFaID().equals("KING") || tbFaculty.getFaID().equals("IN")) {
                    continue;
                }
                resultList.add(tbFaculty.getFacultyName());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultList;
    }//end listFacultyName
    /*
     - Get list free bed
     - Main bed and enable sub-bed
     */

    public List<BedInfo> listAvailableFreeBed(String facultyID) {
        List<BedInfo> result = new ArrayList<>();
        try {
            //--- get tbFaculty from facultyName
            TbFaculty faculty = em.find(TbFaculty.class, facultyID);
            List<TbBed> listAllBed = (List<TbBed>) faculty.getTbBedCollection();
            for (TbBed tbBed : listAllBed) {
                if (tbBed.getStatus().equals(BED_FREE) && tbBed.getIsEnable()) {
                    BedInfo bedInfo = transToBedView(tbBed);
                    if (bedInfo != null) {
                        result.add(transToBedView(tbBed));
                    }
                }
            }
//            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    } //end class listFreeBed
    /*
     - Get list occupied bed
     - Main bed and enable sub-bed
     */

    public List<BedInfo> listAvailableOccupiedBed(String facultyID) {
        List<BedInfo> result = new ArrayList<>();
        try {
            //--- get tbFaculty from facultyName
            TbFaculty faculty = em.find(TbFaculty.class, facultyID);
            List<TbBed> listAllBed = (List<TbBed>) faculty.getTbBedCollection();
            for (TbBed tbBed : listAllBed) {
                if (tbBed.getStatus().equals(BED_OCCUPIED) && tbBed.getIsEnable()) {
                    BedInfo bedInfo = transToBedView(tbBed);
                    if (bedInfo != null) {
                        result.add(transToBedView(tbBed));
                    }
                }
            }
//            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    } //end class listFreeBed
    /*
     - Check free bed
     - Change status of faculty
     */

    public boolean checkFreeBedInFaculty(String facultyID) {
        //--- get ID from facultyName
        List<BedInfo> listAllEnableBedInFaculty = new ArrayList<>();
        List<BedInfo> listAvailableOccupiedBed = listAvailableOccupiedBed(facultyID);
        TbFaculty faculty = em.find(TbFaculty.class, facultyID);
        try {
            //--- find all bed in faculty
            List<TbBed> allBedInFaculty = (List<TbBed>) faculty.getTbBedCollection();
            for (TbBed tbBed : allBedInFaculty) {
                if (tbBed.getIsEnable()) {
                    listAllEnableBedInFaculty.add(transToBedView(tbBed));
                }
            }
            //--- condition list size occupied equals list size free
            //--- return false -> faculty is full
            if (listAvailableOccupiedBed.size() == listAllEnableBedInFaculty.size()) {
                faculty.setStatus("full");
                em.persist(faculty);
                return false;
            }
            //--- In case occupied beds < free beds
            //--- if faculty's status is full, turn to free
            if (faculty.getStatus().equals("full")) {
                faculty.setStatus("free");
                em.persist(faculty);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }
    /*
     - Method to set what info of employee will be displayed when you log in success
     - All info will go to EmpInfo object
     */

    public EmpInfo empInfo(String employeeID, String password) {
        EmpInfo info = null;
        try {
            String ejbQL = "SELECT t FROM TbEmployee t "
                    + "WHERE t.employeeID = :employeeID "
                    + "AND t.password = :password";
            TbEmployee emp = (TbEmployee) em.createQuery(ejbQL)
                    .setParameter("employeeID", employeeID)
                    .setParameter("password", password)
                    .getSingleResult();
            if (emp.getStatusEmp()) {
                //---show the faculty name that employee is belong to
                TbFaculty fa = emp.getFacultyID();
                info = new EmpInfo();
                info.setFacultyID(fa.getFaID());
                info.setFaculty(fa.getFacultyName());
                info.setEmpName(emp.getFullname());
                info.setEmployeeID(employeeID);
                info.setPosition(emp.getPosition());
            }
        } catch (Exception e) {
        }
        return info;
    }//end empInfo
    /*
     - check change faculty event
     */

    public boolean checkChangeFacultyEvent(String medRecordID) {
        try {
            TbMedicalRecord mr = em.find(TbMedicalRecord.class, medRecordID);
            //--- if patient did not discharge
            if (mr.getStatus().equals(DISCHARGE_MR)) {
                //--- get list of fch
                List<TbFacultyChangementHistory> listFCH
                        = (List<TbFacultyChangementHistory>) mr.getTbFacultyChangementHistoryCollection();
                //---examine the listFCH
                for (TbFacultyChangementHistory fch : listFCH) {
                    //--- if any fch got status "change" add to listFCH_change
                    if (fch.getStatus().equals(FCH_CHANGE)) {
                        return true;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }//end checkChangeFacultyEvent
    /*
     - Check patient is current in treatment
     - if patient is registered - true. Otherwise, false;
     */

    public boolean checkPatientIsRegistered(String patID) {
        try {
            TbPatient patient = em.find(TbPatient.class, patID);
            List<TbMedicalRecord> listMR
                    = (List<TbMedicalRecord>) patient.getTbMedicalRecordCollection();
            for (TbMedicalRecord mr : listMR) {
                if (!mr.getStatus().equals(DISCHARGE_MR)) {
                    return true;
                }
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }//end checkPatientIsNotRegistered
    /*
     - Get current date patient in
     - setting the right faculty where patient is in treatment
     */

    public Date dateInHospital(String medRecordID) {
        try {
            TbMedicalRecord mr = em.find(TbMedicalRecord.class, medRecordID);
            List<TbFacultyChangementHistory> listFCH
                    = (List<TbFacultyChangementHistory>) mr.getTbFacultyChangementHistoryCollection();
            //--- examine list of fch
            //--- get the fch which status is start
            for (TbFacultyChangementHistory fch : listFCH) {
                if (fch.getStatus().equals(FCH_BEGIN) && fch.getDateIn() != null) {
                    return fch.getDateIn();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }//end class dateInHospital
      /*
     - Get current faculty patient in
     - setting the right faculty where patient is in treatment
     */

    public String currentFaculty(String medRecordID) {
        try {
            TbMedicalRecord mr = em.find(TbMedicalRecord.class, medRecordID);
            //--- if patient did not discharge
            //--- get list of fch
            List<TbFacultyChangementHistory> listFCH
                    = (List<TbFacultyChangementHistory>) mr.getTbFacultyChangementHistoryCollection();
            List<TbFacultyChangementHistory> listFCH_change = new ArrayList<>();
            //---examine the listFCH
            for (TbFacultyChangementHistory fch : listFCH) {
                //--- if any fch got status "change" add to listFCH_change
                if (fch.getStatus().equals(FCH_CHANGE)) {
                    listFCH_change.add(fch);
                }
            }
            //--- condition listFCH_change is empty
            //--- which means the change faculty is not occur
            //--- the listFCH just have 1 record which status is "in"
            if (listFCH_change.isEmpty()) {
                TbFacultyChangementHistory fch_in = listFCH.get(0);
                return fch_in.getToFaculty();
            } else { //--- when the list change is not empty
                for (TbFacultyChangementHistory fch : listFCH_change) {
                    if (fch.getDateIn() == null) {
                        return fch.getToFaculty();
                    }
                }
                Collections.sort(listFCH_change, new Comparator<TbFacultyChangementHistory>() {
                    @Override
                    public int compare(TbFacultyChangementHistory o1, TbFacultyChangementHistory o2) {
                        return o1.getDateIn().compareTo(o2.getDateIn());
                    }
                });
                //---The max date is the last elements in the sort list
                int size = listFCH_change.size();
                return listFCH_change.get(size - 1).getToFaculty();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }//end class currentBed
    /*
     - Get current bed of patient
     */

    public String currentBed(String medRecordID) {
        try {
            //--- get medical record entity
            TbMedicalRecord mr = em.find(TbMedicalRecord.class, medRecordID);
            List<TbFacultyChangementHistory> listFCH
                    = (List<TbFacultyChangementHistory>) mr.getTbFacultyChangementHistoryCollection();
            List<TbFacultyChangementHistory> listFCH_change = new ArrayList<>();
            //---examine the listFCH
            for (TbFacultyChangementHistory fch : listFCH) {
                //--- if any fch got status "change" add to listFCH_change
                if (fch.getStatus().equals(FCH_CHANGE)) {
                    listFCH_change.add(fch);
                }
            }
            //--- condition listFCH_change is empty
            //--- which means the change faculty is not occur
            //--- the listFCH just have 1 record which status is "in"
            if (listFCH_change.isEmpty()) {
                TbFacultyChangementHistory fch_in = listFCH.get(0);
                //--- if date in is null
                //--- means the patient has not been accepted
                //--- return "N/A"
                //--- otherwise, return to faculty in record
                if (fch_in.getBedName() != null) {
                    return fch_in.getBedName();
                } else {
                    return "N/A";
                }
            } else { //--- when the list change is not empty
                for (TbFacultyChangementHistory fch : listFCH_change) {
                    if (fch.getDateIn() == null) {
                        return "N/A";
                    }
                }
                Collections.sort(listFCH_change, new Comparator<TbFacultyChangementHistory>() {
                    @Override
                    public int compare(TbFacultyChangementHistory o1, TbFacultyChangementHistory o2) {
                        return o1.getDateIn().compareTo(o2.getDateIn());
                    }
                });
                //---The max date is the last elements in the sort list
                int size = listFCH_change.size();
                return listFCH_change.get(size - 1).getBedName();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }//end currentBed
    /*
     - Check the remaining is still afford to apply service
     - Return false if totalServiceCheck > remaining
     */

    public boolean checkDepositPayment(double totalServiceCheck, String medRecordID) {
        TbMedicalRecord mr = em.find(TbMedicalRecord.class, medRecordID);
        try {
            List<TbDepositPayment> listDeposit = (List<TbDepositPayment>) mr.getTbDepositPaymentCollection();
            double totalRemaining = 0;
            for (TbDepositPayment deposit : listDeposit) {
                if (deposit.getStatus().equals(DEPOSIT_CURRENT)) {
                    totalRemaining = deposit.getRemaining();
                    System.out.println("To re : "+totalRemaining);
                    //--- if total remaining >= totalServiceCheck
                    //--- set new remaining equals to totalRemaining - totalServiceCheck
                    if (totalRemaining >= totalServiceCheck) {
                        double newRemaining = totalRemaining - totalServiceCheck;
                        deposit.setRemaining(newRemaining);
                        em.persist(deposit);
                        return true;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //--- medical record isDeposit = false
        //--- set true
        if (mr.getIsDeposit() == true) {
            mr.setIsDeposit(false);
            em.persist(mr);
        }
        return false;
    }//end isStillRemaining
    /*
<<<<<<< HEAD
<<<<<<< HEAD
=======
=======
>>>>>>> c58fa75acaf54f8595581802e720eba751610dc9
     - refund when change in service
     */

    public void refundDeposit(double oldTotal, String medRecordID) {
        try {
            //--- get medical record entity
            TbMedicalRecord mr = em.find(TbMedicalRecord.class, medRecordID);
            List<TbDepositPayment> lstDeposit
                    = (List<TbDepositPayment>) mr.getTbDepositPaymentCollection();
            //--- examine the list
            for (TbDepositPayment deposit : lstDeposit) {
                //--- find the active deposit
                if (deposit.getStatus().equals(DEPOSIT_CURRENT)) {
                    deposit.setRemaining(deposit.getRemaining() + oldTotal);
                    em.persist(deposit);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//end refundDeposit
    /*
<<<<<<< HEAD
>>>>>>> c58fa75acaf54f8595581802e720eba751610dc9
=======
>>>>>>> c58fa75acaf54f8595581802e720eba751610dc9
     - Get total treatment days
     - Calculate the differ between dateOutHospital and dateInHospital
     */

    public int totalTreatmentDays(String medRecordID) {
        try {
            TbMedicalRecord mr = em.find(TbMedicalRecord.class, medRecordID);
            if (mr.getStatus().equals(DISCHARGE_MR)) {
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                Date dateInHospital = dateInHospital(medRecordID);
                Date dateOutHospital = mr.getConclusionDate();
                //--- the total treatment days count when patient is in hospital
                return (int) (((dateOutHospital.getTime() - dateInHospital.getTime()) / (1000 * 60 * 60 * 24)) + 1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //--- if anything wrong
        return -1;
    }//end totalTreatmentDays
    /*
     - Get bed fee detail through the BedService
     */

    public BedService transToBedService(TbFacultyChangementHistory fch) {
        try {
            if (fch.getBedName() != null) {
                BedService bs = new BedService();
                //--- get bed entity from bed name
                TbBed bed = em.createNamedQuery("TbBed.findByBedName", TbBed.class)
                        .setParameter("bedName", fch.getBedName())
                        .getSingleResult();
                //--- get the service price
                List<TbServicePrice> lstBedServicePrice
                        = (List<TbServicePrice>) bed.getServiceID().getTbServicePriceCollection();
                for (TbServicePrice bsp : lstBedServicePrice) {
                    //--- get the match price and service name
                    if (bsp.getServiceID().equals(bed.getServiceID())) {
                        bs.setName(bsp.getServiceID().getServiceName());
                        bs.setPrice(bsp.getPrice());
                    }
                }
                //--- set the total days 
                Date dateInBed = fch.getDateIn();
                Date dateOutBed = fch.getDateOut();
                int totalDaysInBed
                        = (int) ((dateOutBed.getTime() - dateInBed.getTime()) / (1000 * 60 * 60 * 24));
                bs.setQuantity(totalDaysInBed + 1);
                //--- set total
                bs.setTotal();
                return bs;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //--- if anything is wrong
        return null;
    }//end bedFee
    /*
<<<<<<< HEAD
<<<<<<< HEAD
=======
=======
>>>>>>> c58fa75acaf54f8595581802e720eba751610dc9
     - Get list bed service fee
     */

    public List<BedService> lstBedService(String medRecordID) {
        List<BedService> lstBedService = new ArrayList<>();
        try {
            TbMedicalRecord mr = em.find(TbMedicalRecord.class, medRecordID);
            List<TbFacultyChangementHistory> lstFCH
                    = (List<TbFacultyChangementHistory>) mr.getTbFacultyChangementHistoryCollection();
            //--- examine the list of fch
            for (TbFacultyChangementHistory fch : lstFCH) {
                //--- if bedName not null find the TbBed
                if (fch.getBedName() != null) {
                    //--- flag for duplicate
                    boolean dup = false;
                    //--- get bed entity from bed name
                    TbBed bed = em.createNamedQuery("TbBed.findByBedName", TbBed.class)
                            .setParameter("bedName", fch.getBedName())
                            .getSingleResult();
                    //--- examine the list bed service 
                    //--- if any bed service duplicate the service, not add
                    if (lstBedService.isEmpty()) {
                        //--- set the first value into lstBedService
                        lstBedService.add(transToBedService(fch));
                    } else {
                        //--- list is not empty
                        for (BedService bs : lstBedService) {
                            //--- search the duplicate
                            //--- and total days in new bed > 0
                            if (bs.getName().equals(bed.getServiceID().getServiceName())) {
                                //--- set the total days 
                                Date dateInBed = fch.getDateIn();
                                Date dateOutBed = fch.getDateOut();
                                int totalDaysInBed
                                        = (int) ((dateOutBed.getTime() - dateInBed.getTime()) / (1000 * 60 * 60 * 24));
                                //--- condition total days in bed is 0 or not
                                //--- if 0, continue
                                //--- else set quantity again
                                if (totalDaysInBed != 0) {
                                    //--- set quantity again
                                    bs.setQuantity(bs.getQuantity() + totalDaysInBed + 1);
                                    //--- set total again
                                    bs.setTotal();
                                }
                                dup = true;
                            }
                        }
                        //--- if dup not add
                        if (!dup) {
                            lstBedService.add(transToBedService(fch));
                        }
                    }

                }
            }
            //--- in case any bed service has quantity = 0 because they go in and out same day
            //--- set the quantity +1 and total
//            for (BedService bs : lstBedService) {
//                if (bs.getQuantity() == 0) {
//                    bs.setQuantity(bs.getQuantity() + 1);
//                    bs.setTotal();
//                }
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lstBedService;
    }
    /*
     - Transform from TbTesReqDetail into TestService class
     */

    public TestService transToTestService(TbTestReqDetail testReqDetail) {
        TestService ts = new TestService();
        try {
            //--- get test service name
            ts.setName(testReqDetail.getServiceID().getServiceName());
            //--- get service price
            TbService service = testReqDetail.getServiceID();
            List<TbServicePrice> lstServicePrice
                    = (List<TbServicePrice>) service.getTbServicePriceCollection();
            for (TbServicePrice sp : lstServicePrice) {
                //--- if status is true
                if (sp.getStatus()) {
                    ts.setPrice(sp.getPrice());
                }
            }
            //--- each service has 1 time
            ts.setQuantity();
            ts.setTotal();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ts;
    }//end transToTestService
    /*
     - Get test fee
     */

    public List<TestService> lstTestFeeDetail(String medRecordID) {
        List<TestService> result = new ArrayList<>();
        try {

            //--- get medical record
            TbMedicalRecord mr = em.find(TbMedicalRecord.class, medRecordID);
            //--- get test request ID
            List<TbTestRequest> lstTestReq
                    = (List<TbTestRequest>) mr.getTbTestRequestCollection();
            //--- get the test request details
            for (TbTestRequest testReq : lstTestReq) {
                List<TbTestReqDetail> lstTestReqDetail
                        = (List<TbTestReqDetail>) testReq.getTbTestReqDetailCollection();
                //--- get each test request details
                for (TbTestReqDetail testReqDetail : lstTestReqDetail) {
                    //--- condition result is not empty
                    boolean dup = false;
                    if (!result.isEmpty()) {
                        //--- examine the dupplicate items
                        for (TestService ts : result) {
                            //--- if duplicate happens, quantity + 1
                            if (ts.getName().equals(testReqDetail.getServiceID().getServiceName())) {
                                ts.setQuantity();
                                ts.setTotal();
                                dup = true;
                                break;
                            }
                        }
                        if (!dup) {
                            result.add(transToTestService(testReqDetail));
                        }
                    } else { //--- if the result is empty, add first items
                        result.add(transToTestService(testReqDetail));
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }//end lstTestFee
    /*
     - Get daily drugs fee details
     */

    public DailyDrugService transToDailyDrugService(TbPrescriptionDetail pd) {
        DailyDrugService dds = new DailyDrugService();
        try {
            //--- set medicine name
            dds.setName(pd.getMedicineID().getItemName());
            //--- set price
            dds.setPrice(pd.getPrice());
            //--- set quantity
            dds.setQuantity(pd.getQuantity());
            //--- set total
            dds.setTotal();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dds;
    }//end transToDailyDrugService
    /*
     - Get list drug service
     */

    public List<DailyDrugService> lstDrugService(String medRecordID) {
        List<DailyDrugService> result = new ArrayList<>();
        try {
            //--- get medical record entity
            TbMedicalRecord mr = em.find(TbMedicalRecord.class, medRecordID);
            //--- get list prescription
            List<TbPrescription> lstPrescription
                    = (List<TbPrescription>) mr.getTbPrescriptionCollection();
            for (TbPrescription p : lstPrescription) {
                //--- get list prescription detail
                List<TbPrescriptionDetail> lstPrescriptionDetail
                        = (List<TbPrescriptionDetail>) p.getTbPrescriptionDetailCollection();
                for (TbPrescriptionDetail pd : lstPrescriptionDetail) {
                    //--- condition result is not empty
                    boolean dup = false;
                    if (!result.isEmpty()) {
                        //--- examine the dupplicate items
                        for (DailyDrugService dds : result) {
                            //--- if duplicate happens, quantity + 1
                            if (dds.getName().equals(pd.getMedicineID().getItemName())) {
                                //--- set new quantity
                                dds.setQuantity(dds.getQuantity() + pd.getQuantity());
                                //--- set new total
                                dds.setTotal();
                                dup = true;
                            }
                        }
                        //--- if not dup, add new pd
                        if (!dup) {
                            result.add(transToDailyDrugService(pd));
                        }
                    } else { //--- if the result is empty, add first items
                        result.add(transToDailyDrugService(pd));
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }//end lstDrugService
    /*
     - Get daily drugs fee details
     */

    public DailyDrugSuppliesService transToDailyDrugSuppliesService(TbMedicineSuppliesDetail msd) {
        DailyDrugSuppliesService ddss = new DailyDrugSuppliesService();
        try {
            //--- set medicine name
            ddss.setName(msd.getMedicineID().getItemName());
            //--- set price
            ddss.setPrice(msd.getPrice());
            //--- set quantity
            ddss.setQuantity(msd.getQuantity());
            //--- set total
            ddss.setTotal();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ddss;
    }//end transToDailyDrugService
    /*
     - Get list drug suplies service
     */

    public List<DailyDrugSuppliesService> lstDrugSuppliesService(String medRecordID) {
        List<DailyDrugSuppliesService> result = new ArrayList<>();
        try {
            //--- get medical record entity
            TbMedicalRecord mr = em.find(TbMedicalRecord.class, medRecordID);
            //--- get list prescription
            List<TbMedicineSupplies> lstMedicineSupples
                    = (List<TbMedicineSupplies>) mr.getTbMedicineSuppliesCollection();
            for (TbMedicineSupplies ms : lstMedicineSupples) {
                //--- get list prescription detail
                List<TbMedicineSuppliesDetail> lstMedicineSuppliesDetail
                        = (List<TbMedicineSuppliesDetail>) ms.getTbMedicineSuppliesDetailCollection();
                for (TbMedicineSuppliesDetail msd : lstMedicineSuppliesDetail) {
                    //--- condition result is not empty
                    boolean dup = false;
                    if (!result.isEmpty()) {
                        //--- examine the dupplicate items
                        for (DailyDrugSuppliesService ddss : result) {
                            //--- if duplicate happens, quantity + 1
                            if (ddss.getName().equals(msd.getMedicineID().getItemName())) {
                                //--- set new quantity
                                ddss.setQuantity(ddss.getQuantity() + msd.getQuantity());
                                //--- set new total
                                ddss.setTotal();
                                dup = true;
                            }
                        }
                        //--- if not dup, add new pd
                        if (!dup) {
                            result.add(transToDailyDrugSuppliesService(msd));
                        }
                    } else { //--- if the result is empty, add first items
                        result.add(transToDailyDrugSuppliesService(msd));
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }//end lstDrugSuppliesService
    /*
     - Get surgery service
     */

    public SurgeryService transToSurgeryService(TbSuggestSurgery ss) {
        SurgeryService service = new SurgeryService();
        try {
            //--- set service name
            service.setName(ss.getSurgeryServiceID().getSurgeryServiceName());
            //--- set service cost
            service.setPrice(ss.getSurgeryServiceID().getInitialCost());
            //--- set quantity
            service.setQuantity(1);
            //--- set total
            service.setTotal();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return service;
    }//end transToSurgeryService
    /*
     - Get lstSurgeryService
     */

    public List<SurgeryService> lstSurgeryService(String medRecordID) {
        List<SurgeryService> result = new ArrayList<>();
        try {
            //--- get medical record 
            TbMedicalRecord mr = em.find(TbMedicalRecord.class, medRecordID);
            //--- get list of suggest surgery
            List<TbSuggestSurgery> lstSuggestSurgery
                    = (List<TbSuggestSurgery>) mr.getTbSuggestSurgeryCollection();
            //--- examine the list suggest surgery
            for (TbSuggestSurgery ss : lstSuggestSurgery) {
                //--- condition result is not empty
                boolean dup = false;
                if (!result.isEmpty()) {
                    //--- examine the dupplicate items
                    for (SurgeryService service : result) {
                        //--- if duplicate happens, quantity + 1
                        if (service.getName().equals(ss.getSurgeryServiceID().getSurgeryServiceName())) {
                            //--- set new quantity
                            service.setQuantity(service.getQuantity() + 1);
                            //--- set new total
                            service.setTotal();
                            dup = true;
                        }
                    }
                    //--- if not dup, add new pd
                    if (!dup) {
                        result.add(transToSurgeryService(ss));
                    }
                } else { //--- if the result is empty, add first items
                    result.add(transToSurgeryService(ss));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }//end lstSurgeryService
    /*
<<<<<<< HEAD
>>>>>>> c58fa75acaf54f8595581802e720eba751610dc9
=======
>>>>>>> c58fa75acaf54f8595581802e720eba751610dc9
     - Get total deposit has been paid
     */

    public double totalDeposit(String medRecordID) {
        try {
            double totalDeposit = 0;
            //--- get medical record
            TbMedicalRecord mr = em.find(TbMedicalRecord.class, medRecordID);
            List<TbDepositPayment> listDeposit
                    = (List<TbDepositPayment>) mr.getTbDepositPaymentCollection();
            //--- examine the list
            for (TbDepositPayment deposit : listDeposit) {
                //--- get the deposit
                totalDeposit += deposit.getAmount();
            }
            return totalDeposit;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }//end totalDeposit

    /*
     - Treatment fee to view
     */
    public TreatmentFee feeDetails(String medRecordID) {
        try {
            TreatmentFee feeDetail = new TreatmentFee();
            //NED FEE
            BedDTO bedFee = new BedDTO();
            //--- set bed detail
            bedFee.setLstBedService(lstBedService(medRecordID));
            //--- set bed total
            bedFee.setTotalPayment();
            //--- set into treatment fee
            feeDetail.setBedFee(bedFee);
            //TEST FEE
            TestDTO testFee = new TestDTO();
            //--- set test fee detial
            testFee.setLstTestService(lstTestFeeDetail(medRecordID));
            //--- set test total
            testFee.setTotalPayment();
            //--- set into treatment fee
            feeDetail.setTestFee(testFee);
            //DAILY DRUG FEE
            DailyDrugDTO dailyDrugFee = new DailyDrugDTO();
            //--- set drug fee detail and set drug supplies fee detail
            dailyDrugFee.setLstDrugService(lstDrugService(medRecordID));
            dailyDrugFee.setLstDrugSuppliesService(lstDrugSuppliesService(medRecordID));
            //--- set drug and drug supplies fee total
            dailyDrugFee.setTotalPaymentForDrugs();
            dailyDrugFee.setTotalPaymentForDrugsSupplies();
            //--- set total payment of daily drugs use
            dailyDrugFee.setTotalPayment();
            //--- set into treatment fee
            feeDetail.setDailyDrugFee(dailyDrugFee);
            //SURGERY FEE
            SurgeryDTO surgeryFee = new SurgeryDTO();
            //--- set fee detail
            surgeryFee.setLstSurgeryService(lstSurgeryService(medRecordID));
            //--- set total papyment of surgery fee
            surgeryFee.setTotalPayment();
            //--- set into treatment fee
            feeDetail.setSurgeryFee(surgeryFee);
            //--- set total fee
            double totalFee = testFee.getTotalPayment()
                    + bedFee.getTotalPayment()
                    + dailyDrugFee.getTotalPayment()
                    + surgeryFee.getTotalPayment();
            feeDetail.setTotalFee(totalFee);
            //--- set total deposit
            double totalDeposit = totalDeposit(medRecordID);
            feeDetail.setTotalDeposit(totalDeposit);
            //--- set total return, total paymore
            if (totalFee > totalDeposit) {
                feeDetail.setTotalReturn(0);
                feeDetail.setTotalPaymore(totalFee - totalDeposit);
            } else if (totalFee < totalDeposit) {
                feeDetail.setTotalReturn(totalDeposit - totalFee);
                feeDetail.setTotalPaymore(0);
            } else {
                feeDetail.setTotalReturn(0);
                feeDetail.setTotalPaymore(0);
            }
            return feeDetail;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }//end feeDetails
    //--------------END------------------------
}//---END DanSessionBean
