/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package phuong.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Nguyet Phuong
 */
public class ListMedicinePresDTO implements Serializable {
    private String dignose;
    private Date dat;
    private List<MedicinePresDTO> lstMedicinePrescription ;

    public ListMedicinePresDTO() {
        lstMedicinePrescription = new ArrayList<>();
    }

    public List<MedicinePresDTO> getLtMedicinePrescription() {
        return lstMedicinePrescription;
    }

    public void setLtMedicinePrescription(List<MedicinePresDTO> lstMedicinePrescription) {
        this.lstMedicinePrescription = lstMedicinePrescription;
    }

    public String getDignose() {
        return dignose;
    }

    public void setDignose(String dignose) {
        this.dignose = dignose;
    }

    public Date getDat() {
        return dat;
    }

    public void setDat(Date dat) {
        this.dat = dat;
    }
    
     

   
    //------Add Medicine of prescriptionto session---------------------
     /*
     - 
     */

    public boolean addMedicinePres(MedicinePresDTO dto) {
        boolean check = false;
        try {
            lstMedicinePrescription.add(dto);
            check = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return check;
    }//end add Medicine
    //------Remove Medicine of prescriptionto session---------------------
     /*
     - 
     */

    public boolean removeMedicinePres(String id) {
        boolean check = false;
        try {
            for (MedicinePresDTO medPresDTO : lstMedicinePrescription) {
                if (medPresDTO.getId().equals(id)) {
                    lstMedicinePrescription.remove(medPresDTO);
                    return true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return check;
    } // END Remove Medicine of prescriptionto session

    //------Update Medicine of prescriptionto session---------------------
     /*
     - 
     */
    public boolean updateMedicinePres(String id, int quan, int morning, int afternoon, int night) {
        boolean check = false;
        try {
            for (MedicinePresDTO medPresDTO : lstMedicinePrescription) {
                if (medPresDTO.getId().equals(id)) {
                    medPresDTO.setQuantity(quan);
                    medPresDTO.setMorning(morning);
                    medPresDTO.setAfternoon(afternoon);
                    medPresDTO.setNight(night);
                    return true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return check;

    } //END Update Medicine of prescriptionto session
}
