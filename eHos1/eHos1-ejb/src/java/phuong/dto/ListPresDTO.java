/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package phuong.dto;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Nguyet Phuong
 */
public class ListPresDTO implements Serializable {
    private final SimpleDateFormat formatDateTime = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
    private String presID,date, cfdate;
    private String employName, status, patientName, bed, statusRPort;

    public ListPresDTO() {
    }

    public String getPresID() {
        return presID;
    }

    public void setPresID(String presID) {
        this.presID = presID;
    }

    public String getDate() {
        return date;
    }

    public void setDate(Date date) {
        if (date != null) {
            this.date = formatDateTime.format(date);
        }
    }

    public String getEmployName() {
        return employName;
    }

    public void setEmployName(String employName) {
        this.employName = employName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }    

    public String getCfdate() {
        return cfdate;
    }

    public void setCfdate(Date cfdate) {
        if (date != null) {
            this.cfdate = formatDateTime.format(cfdate);
        }
    }    

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getBed() {
        return bed;
    }

    public void setBed(String bed) {
        this.bed = bed;
    }

    public String getStatusRPort() {
        return statusRPort;
    }

    public void setStatusRPort(String statusRPort) {
        this.statusRPort = statusRPort;
    }
    
}
