/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package phuong.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import project.ett.TbPrescription;
import project.ett.TbPrescriptionDetail;

/**
 *
 * @author Nguyet Phuong
 */
public class EditPresDTO implements Serializable {
    private TbPrescription pres;
    private List<TbPrescriptionDetail> lstDetail;

    public EditPresDTO() {
        pres = new TbPrescription();
        lstDetail = new ArrayList<TbPrescriptionDetail>();
    }

    public TbPrescription getPres() {
        return pres;
    }

    public void setPres(TbPrescription pres) {
        this.pres = pres;
    }

    public List<TbPrescriptionDetail> getLstDetail() {
        return lstDetail;
    }

    public void setLstDetail(List<TbPrescriptionDetail> lstDetail) {
        this.lstDetail = lstDetail;
    }
    
}
