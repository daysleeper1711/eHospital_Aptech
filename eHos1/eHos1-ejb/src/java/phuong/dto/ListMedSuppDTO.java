/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package phuong.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Nguyet Phuong
 */
public class ListMedSuppDTO implements Serializable{
    private List<MedicineSuppDTO> lstMedSuppDTO;

    public ListMedSuppDTO() {
        lstMedSuppDTO = new ArrayList<>();
    }

    public List<MedicineSuppDTO> getLstMedSuppDTO() {
        return lstMedSuppDTO;
    }

    public void setLstMedSuppDTO(List<MedicineSuppDTO> lstMedSuppDTO) {
        this.lstMedSuppDTO = lstMedSuppDTO;
    }
    //------Add Medicine of prescriptionto session---------------------
     /*
     - 
     */

    public boolean addMedicineSupp(MedicineSuppDTO dto) {
        boolean check = false;
        try {
            lstMedSuppDTO.add(dto);
            check = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return check;
    }//end add Medicine
    //------Remove Medicine of prescriptionto session---------------------
     /*
     - 
     */

    public boolean removeMedicineSupp(String id) {
        boolean check = false;
        try {
            for (MedicineSuppDTO medsuppDTO : lstMedSuppDTO) {
                if (medsuppDTO.getId().equals(id)) {
                    lstMedSuppDTO.remove(medsuppDTO);
                    return true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return check;
    } // END Remove Medicine of prescriptionto session

    //------Update Medicine of prescriptionto session---------------------
     /*
     - 
     */
    public boolean updateMedicineSupp(String id, int quan) {
        boolean check = false;
        try {
            for (MedicineSuppDTO medSuppDTO : lstMedSuppDTO) {
                if (medSuppDTO.getId().equals(id)) {
                    medSuppDTO.setQuantity(quan);
                    return true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return check;

    } //END Update Medicine of prescriptionto session
    
}
