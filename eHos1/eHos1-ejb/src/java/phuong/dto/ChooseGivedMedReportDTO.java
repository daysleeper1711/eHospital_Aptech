/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package phuong.dto;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Nguyet Phuong
 */
public class ChooseGivedMedReportDTO implements Serializable {
    private String presID,statusGRP, suppID;
    private Date cdate, cfdate;

    public ChooseGivedMedReportDTO() {
    }

    public String getPresID() {
        return presID;
    }

    public void setPresID(String presID) {
        this.presID = presID;
    }

    public String getStatusGRP() {
        return statusGRP;
    }

    public void setStatusGRP(String statusGRP) {
        this.statusGRP = statusGRP;
    }

    public Date getCdate() {
        return cdate;
    }

    public void setCdate(Date cdate) {
        this.cdate = cdate;
    }

    public Date getCfdate() {
        return cfdate;
    }

    public void setCfdate(Date cfdate) {
        this.cfdate = cfdate;
    }

    public String getSuppID() {
        return suppID;
    }

    public void setSuppID(String suppID) {
        this.suppID = suppID;
    }
    
    
}
