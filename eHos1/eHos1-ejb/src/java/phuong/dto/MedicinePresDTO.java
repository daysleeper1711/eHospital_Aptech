/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package phuong.dto;

import java.io.Serializable;

/**
 *
 * @author Nguyet Phuong
 */
public class MedicinePresDTO implements Serializable {
    private String id, name;
    private int morning, afternoon, night, quantity;
    private double price;

    public MedicinePresDTO() {
    }

    public MedicinePresDTO(String id, String name, int morning, int afternoon, int night, int quantity, double price) {
        this.id = id;
        this.name = name;
        this.morning = morning;
        this.afternoon = afternoon;
        this.night = night;
        this.quantity = quantity;
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMorning() {
        return morning;
    }

    public void setMorning(int morning) {
        this.morning = morning;
    }

    public int getAfternoon() {
        return afternoon;
    }

    public void setAfternoon(int afternoon) {
        this.afternoon = afternoon;
    }

    public int getNight() {
        return night;
    }

    public void setNight(int night) {
        this.night = night;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    

}
