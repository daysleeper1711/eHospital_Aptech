/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package phuong.dto;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Nguyet Phuong
 */
public class ListSuppDTO implements Serializable {
    private final SimpleDateFormat formatDateTime = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
    private String suppID,date, cfDate;
    private String employName, status, statusReport, patientName, bed;

    public ListSuppDTO() {
    }

    public String getStatusReport() {
        return statusReport;
    }

    public void setStatusReport(String statusReport) {
        this.statusReport = statusReport;
    }

    public String getSuppID() {
        return suppID;
    }

    public void setSuppID(String suppID) {
        this.suppID = suppID;
    }

    public String getDate() {
        return date;
    }

    public void setDate(Date date) {
        if (date != null) {
            this.date = formatDateTime.format(date);
        }
    }

    public String getCfDate() {
        return cfDate;
    }

    public void setCfDate(Date cfDate) {
        if (date != null) {
            this.cfDate = formatDateTime.format(cfDate);
        }
    }
    

    public String getEmployName() {
        return employName;
    }

    public void setEmployName(String employName) {
        this.employName = employName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getBed() {
        return bed;
    }

    public void setBed(String bed) {
        this.bed = bed;
    }
    
    
}
