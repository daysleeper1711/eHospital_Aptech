/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package phuong.dto;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Nguyet Phuong
 */
public class AdminListSupp implements Serializable {
    private final SimpleDateFormat formatDateTime = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
    
    private String employeeName, suppID, dateCreate, createFDate, status;

    public AdminListSupp() {
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getSuppID() {
        return suppID;
    }

    public void setSuppID(String suppID) {
        this.suppID = suppID;
    }

    public String getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        if (dateCreate != null) {
            this.dateCreate = formatDateTime.format(dateCreate);
        }
    }

    public String getCreateFDate() {
        return createFDate;
    }

    public void setCreateFDate(Date createFDate) {
        if (createFDate != null) {
            this.createFDate = formatDateTime.format(createFDate);
        }
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    
}
