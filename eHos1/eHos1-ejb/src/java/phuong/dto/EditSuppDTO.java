/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package phuong.dto;

import java.io.Serializable;
import java.util.List;
import project.ett.TbMedicineSupplies;
import project.ett.TbMedicineSuppliesDetail;

/**
 *
 * @author Nguyet Phuong
 */
public class EditSuppDTO implements Serializable {
    private TbMedicineSupplies supp;
    private List<TbMedicineSuppliesDetail> lstSuppDeta;

    public EditSuppDTO() {
    }

    public TbMedicineSupplies getSupp() {
        return supp;
    }

    public void setSupp(TbMedicineSupplies supp) {
        this.supp = supp;
    }

    public List<TbMedicineSuppliesDetail> getLstSuppDeta() {
        return lstSuppDeta;
    }

    public void setLstSuppDeta(List<TbMedicineSuppliesDetail> lstSuppDeta) {
        this.lstSuppDeta = lstSuppDeta;
    }
    
}
