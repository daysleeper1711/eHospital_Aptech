/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phuong.ssb;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import phuong.dto.AdminListPres;
import phuong.dto.AdminListSupp;
import phuong.dto.ChooseGivedMedReportDTO;
import phuong.dto.EditPresDTO;
import phuong.dto.EditSuppDTO;
import phuong.dto.ListPresDTO;
import phuong.dto.ListSuppDTO;
import phuong.dto.lstMedicineDTO;
import project.ett.TbEmployee;
import project.ett.TbFaculty;
import project.ett.TbItemCodeManagement;
import project.ett.TbMedicalRecord;
import project.ett.TbMedicineSupplies;
import project.ett.TbMedicineSuppliesDetail;
import project.ett.TbPrescription;
import project.ett.TbPrescriptionDetail;

/**
 *
 * @author Nguyet Phuong
 */
@Stateless
@LocalBean
public class PhuongSessionBean {

    private final SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");
    //------- Entity manager --------------------
    @PersistenceContext(unitName = "eHos1-ejbPU")
    private EntityManager em;

    public void persist(Object object) {
        em.persist(object);
    }
    //--------------END------------------------

    //------Search---------------------
     /*
     - Enter Medical Record ID and return All prescription, medicine supplies,
     and create for date, detail button click -> Search all pres or med of the create for date
     */
    public List<String> searchCFDateByMedID(String medID) {
        try {
            List<String> lstdate = new ArrayList<>();
            List<String> lstSupp = searchMedSuppByMedID(medID);
            for (String tbMedicineSupplies : lstSupp) {
                boolean fla = true;
                for (String dat : lstdate) {
                    if (tbMedicineSupplies.equals(dat)) {
                        fla = false;
                        break;
                    }
                }
                if (fla) {
                    lstdate.add(tbMedicineSupplies);
                }
            }
            List<String> lstPres = searchPresByMedID(medID);
            for (String tbPrescription : lstPres) {
                boolean fla = true;
                for (String dat : lstdate) {
                    if (tbPrescription.equals(dat)) {
                        fla = false;
                        break;
                    }
                }
                if (fla) {
                    lstdate.add(tbPrescription);
                }
            }
            return lstdate;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null; //if fails
    }//end searchByMedID

    //------------ get list prescription by med recordID
    public List<String> searchMedSuppByMedID(String medID) {
        try {
            List<Date> lst = em.createQuery("SELECT t.createForDate FROM TbMedicineSupplies t WHERE t.medicalRecordID.medRecordID = ?1").setParameter(1, medID).getResultList();
            List<String> lstSupp = new ArrayList<>();
            for (Date string : lst) {
                lstSupp.add(formatDate.format(string));
            }
            return lstSupp;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    } //------------- END get list prescription by med recordID

    //------------ get list medi supplies by med recordID
    public List<String> searchPresByMedID(String medID) {
        try {
            List<Date> lstPress = em.createQuery("SELECT t.createForDate FROM TbPrescription t WHERE t.medicalRecordID.medRecordID = ?1").setParameter(1, medID).getResultList();
            List<String> lstdate = new ArrayList<>();
            for (Date string : lstPress) {
                lstdate.add(formatDate.format(string));
            }
            return lstdate;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    } //------------- END get list prescription by med recordID

    //------Get List Medicine---------------------
     /*
     - Show all medicine name have type is D(Drugs)
     */
    public List<lstMedicineDTO> showAllMedicinePres() {
        try {
            List<lstMedicineDTO> lst = new ArrayList<>();
            List<TbItemCodeManagement> lstmed = em.createQuery("SELECT t FROM TbItemCodeManagement t WHERE t.typeITcode = :typeITcode").setParameter("typeITcode", "D").getResultList();
            for (TbItemCodeManagement tbItemCodeManagement : lstmed) {
                lstMedicineDTO dto = new lstMedicineDTO();
                dto.setId(tbItemCodeManagement.getItemCode());
                dto.setName(tbItemCodeManagement.getItemName() + ", " + tbItemCodeManagement.getContent() + ", " + tbItemCodeManagement.getNation());
                lst.add(dto);
            }
            return lst;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null; //if fails
    }//end showAllMedicineName

    //------Get List Medicine---------------------
     /*
     - Show all medicine name have type is S (supplies)
     */
    public List<lstMedicineDTO> showAllMedicineSupp() {
        try {
            List<lstMedicineDTO> lst = new ArrayList<>();
            List<TbItemCodeManagement> lstmed = em.createQuery("SELECT t FROM TbItemCodeManagement t WHERE t.typeITcode = :typeITcode").setParameter("typeITcode", "S").getResultList();
            for (TbItemCodeManagement tbItemCodeManagement : lstmed) {
                lstMedicineDTO dto = new lstMedicineDTO();
                dto.setId(tbItemCodeManagement.getItemCode());
                dto.setName(tbItemCodeManagement.getItemName() + ", " + tbItemCodeManagement.getNation());
                lst.add(dto);
            }
            return lst;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null; //if fails
    }//end showAllMedicineName

    //------Search Med Name by ID---------------------
     /*
     - 
     */
    public String searchMedicineNameByID(String id) {
        try {
            TbItemCodeManagement thuoc = em.find(TbItemCodeManagement.class, id);
            String medName = "";
            if (thuoc.getTypeITcode().equals("S")) {
                medName = thuoc.getItemName() + ",  " + thuoc.getNation();
            } else {
                medName = thuoc.getItemName() + ", " + thuoc.getContent() + ", " + thuoc.getNation();
            }
            return medName;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null; //if fails
    }//end search med name

    // ------------- find max record or tb prescription 
    public String findMaxRCPres() {
        try {
            String presID = "";
            String ejbKT = "SELECT t FROM TbPrescription t";
            List<TbPrescription> ktOr = em.createQuery(ejbKT).getResultList();
            if (ktOr.size() > 0) {
                String ejbQL = "SELECT t.prescriptionID FROM TbPrescription t WHERE t.dateCreate = (Select MAX(t.dateCreate) from TbPrescription t)";
                String l_presID = (String) em.createQuery(ejbQL).getSingleResult();
                String[] array_presID = l_presID.split("_");
                int num_presID = Integer.parseInt(array_presID[1]) + 1;
                presID = "Pres_" + num_presID;
            } else {
                presID = "Pres_1";
            }
            return presID;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }//END find max record or tb prescription

    // ------------- find max record or tb prescription detail
    public String findMaxRCPresDeta() {
        try {
            String detaID = "";
            String ejbKT = "SELECT t FROM TbPrescriptionDetail t";
            List<TbPrescriptionDetail> ktDe = em.createQuery(ejbKT).getResultList();
            if (ktDe.size() > 0) {
                String ejbQL = "SELECT t.prescriptionDetailID FROM TbPrescriptionDetail t WHERE t.dateCreate = (Select MAX(t.dateCreate) from TbPrescriptionDetail t)";
                String l_detailID = (String) em.createQuery(ejbQL).getSingleResult();
                String[] array_presDetID = l_detailID.split("_");
                int num_presID = Integer.parseInt(array_presDetID[1]) + 1;
                detaID = "PresDet_" + num_presID;
            } else {
                detaID = "PresDet_1";
            }
            return detaID;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }//END find max record or tb prescription
    // ------------- find max record or tb medicine supplies

    public String findMaxRCSupp() {
        try {
            String suppID = "";
            String ejbKT = "SELECT t FROM TbMedicineSupplies t";
            List<TbMedicineSupplies> ktOr = em.createQuery(ejbKT).getResultList();
            if (ktOr.size() > 0) {
                String ejbQL = "SELECT t.medicineSuppliesID FROM TbMedicineSupplies t WHERE t.dateCreate = (Select MAX(t.dateCreate) from TbMedicineSupplies t)";
                String l_suppID = (String) em.createQuery(ejbQL).getSingleResult();
                String[] array_suppID = l_suppID.split("_");
                int num_suppID = Integer.parseInt(array_suppID[1]) + 1;
                suppID = "Supp_" + num_suppID;
            } else {
                suppID = "Supp_1";
            }
            return suppID;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }//END find max record or tb medicine supplies
    // ------------- find max record or tb medicine supplies detail

    public String findMaxRCSuppDeta() {
        try {
            String detaID = "";
            String ejbKT = "SELECT t FROM TbMedicineSuppliesDetail t";
            List<TbMedicineSuppliesDetail> ktDe = em.createQuery(ejbKT).getResultList();
            if (ktDe.size() > 0) {
                String ejbQL = "SELECT t.medicineSuppliesDetailID FROM TbMedicineSuppliesDetail t WHERE t.dateCreate = (Select MAX(t.dateCreate) from TbMedicineSuppliesDetail t)";
                String l_detailID = (String) em.createQuery(ejbQL).getSingleResult();
                String[] array_suppDetID = l_detailID.split("_");
                int num_suppID = Integer.parseInt(array_suppDetID[1]) + 1;
                detaID = "SuppDet_" + num_suppID;
            } else {
                detaID = "SuppDet_1";
            }
            return detaID;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }//END find max record or tb medicine supplies
    //-----------insert table prescription--------

    public String insertPres(TbPrescription prescription, String medRID, String empID, String faID) {
        try {
            String ejb = "SELECT t FROM TbPrescription t WHERE t.createForDate = :createForDate";
            List<TbPrescription> lstTest = em.createQuery(ejb).setParameter("createForDate", prescription.getCreateForDate()).getResultList();
            int statusRP;
            if (lstTest.size() > 0) {
                String ejbQL = "SELECT t.statusReport FROM TbPrescription t WHERE t.dateCreate = (Select MAX(t.dateCreate) from TbPrescription t where t.createForDate = :createForDate)";
                String statusReport = (String) em.createQuery(ejbQL).setParameter("createForDate", prescription.getCreateForDate()).getSingleResult();
                statusRP = Integer.parseInt(statusReport) + 1;
            } else {
                statusRP = 1;
            }
            TbMedicalRecord tbMed = em.find(TbMedicalRecord.class, medRID);
            prescription.setMedicalRecordID(tbMed);
            tbMed.getTbPrescriptionCollection().add(prescription);
            TbEmployee tbEmp = em.find(TbEmployee.class, empID);
            prescription.setEmployeeID(tbEmp);
            prescription.setStatusReport(String.valueOf(statusRP));
            tbEmp.getTbPrescriptionCollection().add(prescription);
            // faculty se them khi anh An dua DTO Medical record
            TbFaculty tbfa = em.find(TbFaculty.class, faID);
            prescription.setFacultyFrom(tbfa);
            tbfa.getTbPrescriptionCollection().add(prescription);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }//-------- END insert table prescription --------------
    // insert table prescription detail

    public Object insertDetaPres(TbPrescriptionDetail prescDetail, String presID, String medID) {
        try {
//             câu lệnh ktra xem còn đủ số lượng k, nếu còn thì -> insert, k thì báo lại cái id of medicine 
//            String ejbQL = "SELECT t.content FROM TbItemCodeManagement t WHERE t.itemCode = :itemCode";
//            double q = (double) em.createQuery(ejbQL).setParameter("itemCode", medID).getSingleResult();
//            if (q < prescDetail.getQuantity()) {
//                return medID;
//            }
//             END
            TbPrescription tbPres = em.find(TbPrescription.class, presID);
            prescDetail.setPrescriptionID(tbPres);
            tbPres.getTbPrescriptionDetailCollection().add(prescDetail);
            TbItemCodeManagement tbItem = em.find(TbItemCodeManagement.class, medID);
            prescDetail.setMedicineID(tbItem);
            tbItem.getTbPrescriptionDetailCollection().add(prescDetail);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }//END insert table prescription detail
    //-----------insert table medicine supplies--------

    public String insertSupp(TbMedicineSupplies supp, String medRID, String empID, String faID) {
        try {
            String ejb = "SELECT t FROM TbMedicineSupplies t WHERE t.createForDate = :createForDate";
            List<TbMedicineSupplies> lstTest = em.createQuery(ejb).setParameter("createForDate", supp.getCreateForDate()).getResultList();
            int statusRP;
            if (lstTest.size() > 0) {
                String ejbQL = "SELECT t.statusReport FROM TbMedicineSupplies t WHERE t.dateCreate = (Select MAX(t.dateCreate) from TbMedicineSupplies t where t.createForDate = :createForDate)";
                String statusReport = (String) em.createQuery(ejbQL).setParameter("createForDate", supp.getCreateForDate()).getSingleResult();
                statusRP = Integer.parseInt(statusReport) + 1;
            } else {
                statusRP = 1;
            }
            TbMedicalRecord tbMed = em.find(TbMedicalRecord.class, medRID);
            supp.setMedicalRecordID(tbMed);
            tbMed.getTbMedicineSuppliesCollection().add(supp);
            TbEmployee tbEmp = em.find(TbEmployee.class, empID);
            supp.setEmployeeID(tbEmp);
            supp.setStatusReport(String.valueOf(statusRP));
            tbEmp.getTbMedicineSuppliesCollection().add(supp);
            // faculty se them khi anh An dua DTO Medical record
            TbFaculty tbfa = em.find(TbFaculty.class, faID);
            supp.setFacultyFrom(tbfa);
            tbfa.getTbMedicineSuppliesCollection().add(supp);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }//-------- END insert table prescription --------------

    // insert table prescription detail
    public Object insertDetaSupp(TbMedicineSuppliesDetail suppDetail, String suppID, String medID) {
        try {
            // câu lệnh ktra xem còn đủ số lượng k, nếu còn thì -> insert, k thì báo lại cái id of medicine 
//            String ejbQL = "SELECT t.content FROM TbItemCodeManagement t WHERE t.itemCode = :itemCode";
//            double q = (double) em.createQuery(ejbQL).setParameter("itemCode", medID).getSingleResult();
//            if (q < suppDetail.getQuantity()) {
//                return medID;
//            }
            // END
            TbMedicineSupplies tbSupp = em.find(TbMedicineSupplies.class, suppID);
            suppDetail.setMedicineSuppliesID(tbSupp);
            tbSupp.getTbMedicineSuppliesDetailCollection().add(suppDetail);
            TbItemCodeManagement tbItem = em.find(TbItemCodeManagement.class, medID);
            suppDetail.setMedicineID(tbItem);
            tbItem.getTbMedicineSuppliesDetailCollection().add(suppDetail);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }//END insert table prescription detail

    //get price of medicine when have medicine price
    public double findPriceByMedicineID(String mediID) {
        try {
            String ejbQL = "SELECT t.xPrice FROM TbItemCodeManagement t WHERE t.itemCode = :itemCode";
            double price = (double) em.createQuery(ejbQL).setParameter("itemCode", mediID).getSingleResult();
            return price;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }//END get price of medicine when have medicine price
    //get list prescription by create for date, medical record

    public List<ListPresDTO> findPresByCreFDate(Date cFdate, String medRCID) {
        try {
            String ejbQL = "SELECT t FROM TbPrescription t WHERE t.createForDate = :createForDate AND t.medicalRecordID.medRecordID = :medRC";
            List<TbPrescription> lst = em.createQuery(ejbQL).setParameter("createForDate", cFdate).setParameter("medRC", medRCID).getResultList();
            List<ListPresDTO> lstPres = new ArrayList<>();
            for (TbPrescription tbPresDTO : lst) {
                ListPresDTO dto = new ListPresDTO();
                dto.setPresID(tbPresDTO.getPrescriptionID());
                dto.setEmployName(tbPresDTO.getEmployeeID().getFullname());
                dto.setDate(tbPresDTO.getDateCreate());
                dto.setCfdate(tbPresDTO.getCreateForDate());
                dto.setStatus(tbPresDTO.getStatusPres());
                dto.setStatusRPort(tbPresDTO.getStatusReport());
                lstPres.add(dto);
            }
            return lstPres;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }// END get list prescription by create for date
    //get list supp by create for date

    public List<ListSuppDTO> findSuppByCreFDate(Date cFdate, String medRC) {
        try {
            String ejbQL = "SELECT t FROM TbMedicineSupplies t WHERE t.createForDate = :createForDate AND t.medicalRecordID.medRecordID = :medRC";
            List<TbMedicineSupplies> lst = em.createQuery(ejbQL).setParameter("createForDate", cFdate).setParameter("medRC", medRC).getResultList();
            List<ListSuppDTO> lstSupp = new ArrayList<>();
            for (TbMedicineSupplies tbSuppDTO : lst) {
                ListSuppDTO dto = new ListSuppDTO();
                dto.setSuppID(tbSuppDTO.getMedicineSuppliesID());
                dto.setEmployName(tbSuppDTO.getEmployeeID().getFullname());
                dto.setDate(tbSuppDTO.getDateCreate());
                dto.setStatus(tbSuppDTO.getStatusMedSupp());
                dto.setStatusReport(tbSuppDTO.getStatusReport());
                dto.setCfDate(tbSuppDTO.getCreateForDate());
                lstSupp.add(dto);
            }
            return lstSupp;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }// END get list supp by create for date

    //get edit prescription
    public EditPresDTO editPres(String presID) {
        try {
            EditPresDTO ed = new EditPresDTO();
            String ejbQL = "SELECT t FROM TbPrescription t WHERE t.prescriptionID = :prescriptionID";
            TbPrescription pres = (TbPrescription) em.createQuery(ejbQL).setParameter("prescriptionID", presID).getSingleResult();
            ed.setPres(pres);
            List<TbPrescriptionDetail> lstDeta = (List<TbPrescriptionDetail>) pres.getTbPrescriptionDetailCollection();
            ed.setLstDetail(lstDeta);
            return ed;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }// END get edit prescription
    //get edit supp

    public EditSuppDTO editSupp(String suppID) {
        try {
            EditSuppDTO ed = new EditSuppDTO();
            String ejbQL = "SELECT t FROM TbMedicineSupplies t WHERE t.medicineSuppliesID = :medicineSuppliesID";
            TbMedicineSupplies supp = (TbMedicineSupplies) em.createQuery(ejbQL).setParameter("medicineSuppliesID", suppID).getSingleResult();
            System.out.println("AAAA"+supp.getMedicineSuppliesID());
            ed.setSupp(supp);
            List<TbMedicineSuppliesDetail> lstDeta = (List<TbMedicineSuppliesDetail>) supp.getTbMedicineSuppliesDetailCollection();
            ed.setLstSuppDeta(lstDeta);
            return ed;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }// END get edit supp
    //------Search Med Unit by ID---------------------

    public String searchMedicineUnitByID(String id) {
        try {
            String medUnit = (String) em.createQuery("SELECT t.unit FROM TbItemCodeManagement t where t.itemCode = :itemCode").setParameter("itemCode", id).getSingleResult();
            return medUnit;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null; //if fails
    }//end search med unit
    //------Search Med How to use by ID

    public String searchMedicineHTUseByID(String id) {
        try {
            String medHowToUse = (String) em.createQuery("SELECT t.howToUse FROM TbItemCodeManagement t where t.itemCode = :itemCode").setParameter("itemCode", id).getSingleResult();
            return medHowToUse;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null; //if fails
    }//end search med unit
    //find FacultyID when have faculty name

    public String findFacIDByFacName(String facName) {
        try {
            String ejbQL = "SELECT t.faID FROM TbFaculty t WHERE t.facultyName = :facultyName";
            String fac = (String) em.createQuery(ejbQL).setParameter("facultyName", facName).getSingleResult();
            return fac;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    } //END find facName by facID

    //find List prescription have status "Book Medicine"
    public List<ListPresDTO> searchPresBookMedicine() {
        try {
            String ejbQL = "SELECT t FROM TbPrescription t WHERE t.statusPres = :statusPres";
            List<TbPrescription> lstPres = em.createQuery(ejbQL).setParameter("statusPres", "Book Medicine").getResultList();
            List<ListPresDTO> lstPresRT = new ArrayList<ListPresDTO>();
            for (TbPrescription listPresDTO : lstPres) {
                ListPresDTO pre = new ListPresDTO();
                pre.setPresID(listPresDTO.getPrescriptionID());
                pre.setEmployName(listPresDTO.getEmployeeID().getFullname());
                pre.setStatus(listPresDTO.getStatusPres());
                pre.setDate(listPresDTO.getDateCreate());
                pre.setCfdate(listPresDTO.getCreateForDate());
                pre.setPatientName(listPresDTO.getMedicalRecordID().getPatID().getPatName());
                lstPresRT.add(pre);
            }
            return lstPresRT;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }// END find List prescription have status "Book Medicine"
    //find List prescription have status "New prescription"

    public List<ListPresDTO> searchPresNewPres() {
        try {
            String ejbQL = "SELECT t FROM TbPrescription t WHERE t.statusPres = :statusPres";
            List<TbPrescription> lstPres = em.createQuery(ejbQL).setParameter("statusPres", "New prescription").getResultList();
            List<ListPresDTO> lstPresRT = new ArrayList<ListPresDTO>();
            for (TbPrescription listPresDTO : lstPres) {
                ListPresDTO pre = new ListPresDTO();
                pre.setPresID(listPresDTO.getPrescriptionID());
                pre.setEmployName(listPresDTO.getEmployeeID().getFullname());
                pre.setStatus(listPresDTO.getStatusPres());
                pre.setDate(listPresDTO.getDateCreate());
                pre.setCfdate(listPresDTO.getCreateForDate());
                pre.setPatientName(listPresDTO.getMedicalRecordID().getPatID().getPatName());
                lstPresRT.add(pre);
            }
            return lstPresRT;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }// END find List prescription have status "New prescription"
    //find List prescription have status "Gived Medicine"

    public List<ListPresDTO> searchPresGivedMedicine() {
        try {
            String ejbQL = "SELECT t FROM TbPrescription t WHERE t.statusPres = :statusPres";
            List<TbPrescription> lstPres = em.createQuery(ejbQL).setParameter("statusPres", "Gived Medicine").getResultList();
            List<ListPresDTO> lstPresRT = new ArrayList<ListPresDTO>();
            for (TbPrescription listPresDTO : lstPres) {
                ListPresDTO pre = new ListPresDTO();
                pre.setPresID(listPresDTO.getPrescriptionID());
                pre.setEmployName(listPresDTO.getEmployeeID().getFullname());
                pre.setStatus(listPresDTO.getStatusPres());
                pre.setDate(listPresDTO.getDateCreate());
                pre.setCfdate(listPresDTO.getCreateForDate());
                pre.setPatientName(listPresDTO.getMedicalRecordID().getPatID().getPatName());
                lstPresRT.add(pre);
            }
            return lstPresRT;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }// END find List prescription have status "Gived Medicine"

    //find List med supp have status "Book Medicine"
    public List<ListSuppDTO> searchSuppBookMedicine() {
        try {
            String ejbQL = "SELECT t FROM TbMedicineSupplies t WHERE t.statusMedSupp = :statusMedSupp";
            List<TbMedicineSupplies> lstSupp = em.createQuery(ejbQL).setParameter("statusMedSupp", "Book Medicine").getResultList();
            List<ListSuppDTO> lstPresRT = new ArrayList<ListSuppDTO>();
            for (TbMedicineSupplies listSuppDTO : lstSupp) {
                ListSuppDTO supp = new ListSuppDTO();
                supp.setSuppID(listSuppDTO.getMedicineSuppliesID());
                supp.setEmployName(listSuppDTO.getEmployeeID().getFullname());
                supp.setStatus(listSuppDTO.getStatusMedSupp());
                supp.setDate(listSuppDTO.getDateCreate());
                supp.setCfDate(listSuppDTO.getCreateForDate());
                supp.setPatientName(listSuppDTO.getMedicalRecordID().getPatID().getPatName());
                lstPresRT.add(supp);
            }
            return lstPresRT;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }// END find List Supp have status "Book Medicine"

    //find List Supp have status "New Supplies"
    public List<ListSuppDTO> searchSuppNewPres() {
        try {
            String ejbQL = "SELECT t FROM TbMedicineSupplies t WHERE t.statusMedSupp = :statusMedSupp";
            List<TbMedicineSupplies> lstSupp = em.createQuery(ejbQL).setParameter("statusMedSupp", "New medicine supplies").getResultList();
            List<ListSuppDTO> lstPresRT = new ArrayList<ListSuppDTO>();
            for (TbMedicineSupplies listSuppDTO : lstSupp) {
                ListSuppDTO supp = new ListSuppDTO();
                supp.setSuppID(listSuppDTO.getMedicineSuppliesID());
                supp.setEmployName(listSuppDTO.getEmployeeID().getFullname());
                supp.setStatus(listSuppDTO.getStatusMedSupp());
                supp.setDate(listSuppDTO.getDateCreate());
                supp.setCfDate(listSuppDTO.getCreateForDate());
                supp.setPatientName(listSuppDTO.getMedicalRecordID().getPatID().getPatName());
                lstPresRT.add(supp);
            }
            return lstPresRT;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }// END find List Supp have status "New Supplies"
    //find List Supp have status "Gived Medicine"

    public List<ListSuppDTO> searchSuppGivedMedicine() {
        try {
            String ejbQL = "SELECT t FROM TbMedicineSupplies t WHERE t.statusMedSupp = :statusMedSupp";
            List<TbMedicineSupplies> lstSupp = em.createQuery(ejbQL).setParameter("statusMedSupp", "Gived Medicine").getResultList();
            List<ListSuppDTO> lstPresRT = new ArrayList<ListSuppDTO>();
            for (TbMedicineSupplies listSuppDTO : lstSupp) {
                ListSuppDTO supp = new ListSuppDTO();
                supp.setSuppID(listSuppDTO.getMedicineSuppliesID());
                supp.setEmployName(listSuppDTO.getEmployeeID().getFullname());
                supp.setStatus(listSuppDTO.getStatusMedSupp());
                supp.setDate(listSuppDTO.getDateCreate());
                supp.setCfDate(listSuppDTO.getCreateForDate());
                supp.setPatientName(listSuppDTO.getMedicalRecordID().getPatID().getPatName());
                lstPresRT.add(supp);
            }
            return lstPresRT;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }// END find List Supp have status "Gived Medicine"
    //update status 'Book Medicine' to prescription

    public boolean updatePresStatusBookMed(String presID) {
        try {
            TbPrescription pres = em.find(TbPrescription.class, presID);
            pres.setStatusPres("Book Medicine");
            persist(pres);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }// END update status 'Book Medicine' to prescription

    //update status 'Gived Medicine' to prescription
    public boolean updatePresStatusGivedMed(String presID) {
        try {
            TbPrescription pres = em.find(TbPrescription.class, presID);
            pres.setStatusPres("Gived Medicine");
            persist(pres);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }// END update status 'Gived Medicine' to prescription

    //update status 'Gived Medicine' to med sup
    public boolean updateMedSuppStatusGivedMed(String suppID) {
        try {
            TbMedicineSupplies supp = em.find(TbMedicineSupplies.class, suppID);
            supp.setStatusMedSupp("Gived Medicine");
            persist(supp);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }// END update status 'Gived Medicine' to prescription

    //update status 'Book Medicine' to Supp
    public boolean updateSuppStatusBookMed(String suppID) {
        try {
            TbMedicineSupplies supp = em.find(TbMedicineSupplies.class, suppID);
            supp.setStatusMedSupp("Book Medicine");
            persist(supp);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }// END update status 'Book Medicine' to prescription

    //update status 'Gived Medicine' to Supp
    public boolean updateSuppStatusGivedMed(String suppID) {
        try {
            TbMedicineSupplies supp = em.find(TbMedicineSupplies.class, suppID);
            supp.setStatusMedSupp("Gived Medicine");
            persist(supp);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }// END update status 'Gived Medicine' to prescription

    //update morning, afternoon, night of quantity
    public Object updatePresDetaQuantity(String presID, String presDetaID, int morning, int afternoon, int night, int quantity, String empID) {
        try {
            TbPrescriptionDetail presDeta = em.find(TbPrescriptionDetail.class, presDetaID);
            //             câu lệnh ktra xem còn đủ số lượng k, nếu còn thì -> insert, k thì báo lại cái id of medicine 
//            String ejbQL = "SELECT t.content FROM TbItemCodeManagement t WHERE t.itemCode = :itemCode";
//            double q = (double) em.createQuery(ejbQL).setParameter("itemCode", presDeta.getMedicineID().getItemCode()).getSingleResult();
//            if (q < (quantity - presDeta.getQuantity())) {
//                return presDetaID;
//            }
//             END
            TbEmployee emp = em.find(TbEmployee.class, empID);
            presDeta.setMorning(morning);
            presDeta.setAfternoon(afternoon);
            presDeta.setNight(night);
            presDeta.setQuantity(quantity);
            presDeta.setUpDateTime(Calendar.getInstance().getTime());
            presDeta.setUpEmployeeID(emp);
            emp.getTbPrescriptionDetailCollection().add(presDeta);
            TbPrescription pres = em.find(TbPrescription.class, presID);
            pres.setUpEmployeeID(emp);
            pres.setUpDateTime(Calendar.getInstance().getTime());
            pres.setTotalPrice(getNewTotalPriceInPrescription(presID));
            emp.getTbPrescriptionCollection1().add(pres);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    } //END update prescription detail

    //update quantity of medicine supplies
    public Object updateMedSuppDetaQuantity(String suppID, String suppDetaID, int quantity, String empID) {
        try {
            TbMedicineSuppliesDetail suppDeta = em.find(TbMedicineSuppliesDetail.class, suppDetaID);
            //             câu lệnh ktra xem còn đủ số lượng k, nếu còn thì -> insert, k thì báo lại cái id of medicine 
//            String ejbQL = "SELECT t.content FROM TbItemCodeManagement t WHERE t.itemCode = :itemCode";
//            double q = (double) em.createQuery(ejbQL).setParameter("itemCode", presDeta.getMedicineID().getItemCode()).getSingleResult();
//            if (q < (quantity - presDeta.getQuantity())) {
//                return presDetaID;
//            }
//             END
            TbEmployee emp = em.find(TbEmployee.class, empID);
            suppDeta.setQuantity(quantity);
            suppDeta.setUpDateTime(Calendar.getInstance().getTime());
            suppDeta.setUpEmployeeID(emp);
            emp.getTbMedicineSuppliesDetailCollection().add(suppDeta);
            TbMedicineSupplies supp = em.find(TbMedicineSupplies.class, suppID);
            supp.setUpEmployeeID(emp);
            supp.setUpDateTime(Calendar.getInstance().getTime());
            supp.setTotalPrice(getNewTotalPriceInMedSupp(suppID));
            emp.getTbMedicineSuppliesCollection1().add(supp);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    } //END update prescription detail

    //check medicineID have exit? in prescription
    public boolean checkMedIdInPrescription(String presID, String medID) {
        try {
            TbPrescription pres = em.find(TbPrescription.class, presID);
            List<TbPrescriptionDetail> lstPresDeta = (List<TbPrescriptionDetail>) pres.getTbPrescriptionDetailCollection();
            for (TbPrescriptionDetail tbPrescriptionDetail : lstPresDeta) {
                if (tbPrescriptionDetail.getMedicineID().getItemCode().equals(medID)) {
                    return true;
                }
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }// END check medicineID have exit? in prescription

    //check medicineID have exit? in med supplies
    public boolean checkMedIdInMedSupp(String suppID, String medID) {
        try {
            TbMedicineSupplies supp = em.find(TbMedicineSupplies.class, suppID);
            System.out.println("LLLL"+suppID);
            List<TbMedicineSuppliesDetail> lstPresDeta = (List<TbMedicineSuppliesDetail>) supp.getTbMedicineSuppliesDetailCollection();
            
            for (TbMedicineSuppliesDetail tbMedicineSuppliesDetail : lstPresDeta) {
                if (tbMedicineSuppliesDetail.getMedicineID().getItemCode().equals(medID)) {
                    return true;
                }
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }// END check medicineID have exit? in med supplies

    //get New totalPrice in prescription when add new or update quantity of prescription detail
    public double getNewTotalPriceInPrescription(String presID) {
        try {
            double totalPrice = 0;
            TbPrescription pres = em.find(TbPrescription.class, presID);
            List<TbPrescriptionDetail> lstPresDeta = (List<TbPrescriptionDetail>) pres.getTbPrescriptionDetailCollection();
            for (TbPrescriptionDetail tbPrescriptionDetail : lstPresDeta) {
                totalPrice += tbPrescriptionDetail.getPrice() * tbPrescriptionDetail.getQuantity();
            }
            return totalPrice;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }//END get new total price

    //get New totalPrice in prescription when add new or update quantity of prescription detail
    public double getNewTotalPriceInMedSupp(String suppID) {
        try {
            double totalPrice = 0;
            TbMedicineSupplies supp = em.find(TbMedicineSupplies.class, suppID);
            List<TbMedicineSuppliesDetail> lstSuppDeta = (List<TbMedicineSuppliesDetail>) supp.getTbMedicineSuppliesDetailCollection();
            for (TbMedicineSuppliesDetail tbMedicineSuppliesDetail : lstSuppDeta) {
                totalPrice += tbMedicineSuppliesDetail.getPrice() * tbMedicineSuppliesDetail.getQuantity();
            }
            return totalPrice;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }//END get new total price

    //update prescription
    public void updatePrescription(String presID, String empID) {
        try {
            TbEmployee emp = em.find(TbEmployee.class, empID);
            TbPrescription pres = em.find(TbPrescription.class, presID);
            pres.setUpEmployeeID(emp);
            pres.setUpDateTime(Calendar.getInstance().getTime());
            pres.setTotalPrice(getNewTotalPriceInPrescription(presID));
            emp.getTbPrescriptionCollection1().add(pres);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//END update prescription

    //update prescription
    public void updateMedSupplies(String suppID, String empID) {
        try {
            TbEmployee emp = em.find(TbEmployee.class, empID);
            TbMedicineSupplies supp = em.find(TbMedicineSupplies.class, suppID);
            supp.setUpEmployeeID(emp);
            supp.setUpDateTime(Calendar.getInstance().getTime());
            supp.setTotalPrice(getNewTotalPriceInMedSupp(suppID));
            emp.getTbMedicineSuppliesCollection1().add(supp);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//END update prescription

    //delete Prescription detail    
    public boolean deletePresDeta(String presDetaID, String presID) {
        try {
            TbPrescriptionDetail presDeta = em.find(TbPrescriptionDetail.class, presDetaID);
            String medicineID = presDeta.getMedicineID().getItemCode();
            TbEmployee empTest = presDeta.getUpEmployeeID();
            TbPrescription pres = em.find(TbPrescription.class, presID);
            List<TbPrescriptionDetail> lstpresDetaPres = (List<TbPrescriptionDetail>) pres.getTbPrescriptionDetailCollection();
            lstpresDetaPres.remove(presDeta);
            pres.setTbPrescriptionDetailCollection(lstpresDetaPres);
            TbItemCodeManagement med = em.find(TbItemCodeManagement.class, medicineID);
            List<TbPrescriptionDetail> lstpresDetaMed = (List<TbPrescriptionDetail>) med.getTbPrescriptionDetailCollection();
            lstpresDetaMed.remove(presDeta);
            med.setTbPrescriptionDetailCollection(lstpresDetaMed);
            if (empTest != null) {
                TbEmployee emp = em.find(TbEmployee.class, empTest.getEmployeeID());
                List<TbPrescriptionDetail> lstpresDetaEmp = (List<TbPrescriptionDetail>) emp.getTbPrescriptionDetailCollection();
                lstpresDetaEmp.remove(presDeta);
                emp.setTbPrescriptionDetailCollection(lstpresDetaMed);
            }
            em.remove(presDeta);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }//END delete prescription detail

    //delete medicine supplies detail    
    public boolean deleteMedSuppDeta(String suppDetaID, String suppID) {
        try {
            TbMedicineSuppliesDetail suppDeta = em.find(TbMedicineSuppliesDetail.class, suppDetaID);
            String medicineID = suppDeta.getMedicineID().getItemCode();
            TbEmployee empTest = suppDeta.getUpEmployeeID();
            TbMedicineSupplies supp = em.find(TbMedicineSupplies.class, suppID);
            List<TbMedicineSuppliesDetail> lstsuppDetaPres = (List<TbMedicineSuppliesDetail>) supp.getTbMedicineSuppliesDetailCollection();
            lstsuppDetaPres.remove(suppDeta);
            supp.setTbMedicineSuppliesDetailCollection(lstsuppDetaPres);
            TbItemCodeManagement med = em.find(TbItemCodeManagement.class, medicineID);
            List<TbMedicineSuppliesDetail> lstsuppDetaMed = (List<TbMedicineSuppliesDetail>) med.getTbMedicineSuppliesDetailCollection();
            lstsuppDetaMed.remove(suppDeta);
            med.setTbMedicineSuppliesDetailCollection(lstsuppDetaMed);
            if (empTest != null) {
                TbEmployee emp = em.find(TbEmployee.class, empTest.getEmployeeID());
                List<TbMedicineSuppliesDetail> lstpresDetaEmp = (List<TbMedicineSuppliesDetail>) emp.getTbMedicineSuppliesDetailCollection();
                lstpresDetaEmp.remove(suppDeta);
                emp.setTbMedicineSuppliesDetailCollection(lstpresDetaEmp);
            }
            em.remove(suppDeta);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }//END delete medicine supplies detail

    //update dignose of prescription
    public boolean updatePresDignose(String presID, String empID, String dignose) {
        try {
            TbPrescription pres = em.find(TbPrescription.class, presID);
            TbEmployee emp = em.find(TbEmployee.class, empID);
            pres.setDignose(dignose);
            pres.setUpEmployeeID(emp);
            pres.setUpDateTime(Calendar.getInstance().getTime());
            emp.getTbPrescriptionCollection1().add(pres);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }//END update dignose of prescription

    //delete Prescription detail
    public boolean deletePres(String presID) {
        try {
            TbPrescription pres = em.find(TbPrescription.class, presID);
            List<TbPrescriptionDetail> presDeta = (List<TbPrescriptionDetail>) pres.getTbPrescriptionDetailCollection();
            for (TbPrescriptionDetail tbPrescriptionDetail : presDeta) {
                deletePresDeta(tbPrescriptionDetail.getPrescriptionDetailID(), presID);
            }
            String empID1 = pres.getEmployeeID().getEmployeeID();
            TbEmployee emp1 = (TbEmployee) em.find(TbEmployee.class, empID1);
            List<TbPrescription> lstEmp1 = (List<TbPrescription>) emp1.getTbPrescriptionCollection();
            lstEmp1.remove(pres);
            emp1.setTbPrescriptionCollection(lstEmp1);
            TbEmployee empTest2 = pres.getUpEmployeeID();
            if (empTest2 != null) {
                TbEmployee emp2 = (TbEmployee) em.find(TbEmployee.class, empTest2.getEmployeeID());
                List<TbPrescription> lstEmp2 = (List<TbPrescription>) emp2.getTbPrescriptionCollection1();
                lstEmp2.remove(pres);
                emp2.setTbPrescriptionCollection1(lstEmp2);
            }
            String medrc = pres.getMedicalRecordID().getMedRecordID();
            TbMedicalRecord med = em.find(TbMedicalRecord.class, medrc);
            List<TbPrescription> lstMed = (List<TbPrescription>) med.getTbPrescriptionCollection();
            lstMed.remove(pres);
            med.setTbPrescriptionCollection(lstMed);
            TbFaculty facTest = pres.getFacultyFrom();
            if (facTest != null) {
                TbFaculty fac = em.find(TbFaculty.class, facTest.getFaID());
                List<TbPrescription> lstFac = (List<TbPrescription>) fac.getTbPrescriptionCollection();
                lstFac.remove(pres);
                fac.setTbPrescriptionCollection(lstFac);
            }
            em.remove(pres);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }//END delete prescription detail

    //delete med supplies
    public boolean deleteMedSupp(String suppID) {
        try {
            TbMedicineSupplies supp = em.find(TbMedicineSupplies.class, suppID);
            List<TbMedicineSuppliesDetail> presDeta = (List<TbMedicineSuppliesDetail>) supp.getTbMedicineSuppliesDetailCollection();
            for (TbMedicineSuppliesDetail tbMedicineSuppliesDetail : presDeta) {
                deleteMedSuppDeta(tbMedicineSuppliesDetail.getMedicineSuppliesDetailID(), suppID);
            }
            String empID1 = supp.getEmployeeID().getEmployeeID();
            TbEmployee emp1 = (TbEmployee) em.find(TbEmployee.class, empID1);
            List<TbMedicineSupplies> lstEmp1 = (List<TbMedicineSupplies>) emp1.getTbMedicineSuppliesCollection();
            lstEmp1.remove(supp);
            emp1.setTbMedicineSuppliesCollection(lstEmp1);
            TbEmployee empTest2 = supp.getUpEmployeeID();
            if (empTest2 != null) {
                TbEmployee emp2 = (TbEmployee) em.find(TbEmployee.class, empTest2.getEmployeeID());
                List<TbMedicineSupplies> lstEmp2 = (List<TbMedicineSupplies>) emp2.getTbMedicineSuppliesCollection1();
                lstEmp2.remove(supp);
                emp2.setTbMedicineSuppliesCollection1(lstEmp2);
            }
            String medrc = supp.getMedicalRecordID().getMedRecordID();
            TbMedicalRecord med = em.find(TbMedicalRecord.class, medrc);
            List<TbMedicineSupplies> lstMed = (List<TbMedicineSupplies>) med.getTbMedicineSuppliesCollection();
            lstMed.remove(supp);
            med.setTbMedicineSuppliesCollection(lstMed);
            TbFaculty facTest = supp.getFacultyFrom();
            if (facTest != null) {
                TbFaculty fac = em.find(TbFaculty.class, facTest.getFaID());
                List<TbMedicineSupplies> lstFac = (List<TbMedicineSupplies>) fac.getTbMedicineSuppliesCollection();
                lstFac.remove(supp);
                fac.setTbMedicineSuppliesCollection(lstFac);
            }
            em.remove(supp);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }//END delete med supplies

    //search list pres gived medicine by CFD and medicalRecordID
    public List<ChooseGivedMedReportDTO> searchLstGRePortByCFDMEDRC(Date cfdate, String medRC) {
        try {
            SimpleDateFormat formatDateTime = new SimpleDateFormat("yyyy/MM/dd");
            Date da;
            List<ChooseGivedMedReportDTO> lstReport = new ArrayList<ChooseGivedMedReportDTO>();
            List<ListPresDTO> lsrpres = findPresByCreFDate(cfdate, medRC);
            if (!lsrpres.isEmpty()) {
                for (ListPresDTO listPresDTO : lsrpres) {
                    ChooseGivedMedReportDTO ch = new ChooseGivedMedReportDTO();
                    ch.setPresID(listPresDTO.getPresID());
                    da = formatDateTime.parse(listPresDTO.getDate());
                    ch.setCdate(da);
                    da = formatDateTime.parse(listPresDTO.getCfdate());
                    ch.setCfdate(da);
                    ch.setStatusGRP(listPresDTO.getStatusRPort());
                    lstReport.add(ch);
                }
            }
            List<ListSuppDTO> lstSupp = findSuppByCreFDate(cfdate, medRC);
            if (!lstSupp.isEmpty()) {
                for (ListSuppDTO listSuppDTO : lstSupp) {
                    int count = 0;
                    if (!lstReport.isEmpty()) {
                        for (ChooseGivedMedReportDTO cSuppDTO1 : lstReport) {
                            if (!listSuppDTO.getStatusReport().equals(cSuppDTO1.getStatusGRP())) {
                                count++;
                            } else {
                                count = -1;
                                cSuppDTO1.setSuppID(listSuppDTO.getSuppID());
                                break;
                            }
                        }
                    }
                    if (count == lstReport.size()) {
                        ChooseGivedMedReportDTO ch = new ChooseGivedMedReportDTO();
                        ch.setSuppID(listSuppDTO.getSuppID());
                        da = formatDateTime.parse(listSuppDTO.getDate());
                        ch.setCdate(da);
                        da = formatDateTime.parse(listSuppDTO.getCfDate());
                        ch.setCfdate(da);
                        ch.setStatusGRP(listSuppDTO.getStatusReport());
                        lstReport.add(ch);
                    }
                }
            }
            Collections.sort(lstReport, new Comparator<ChooseGivedMedReportDTO>() {

                @Override
                public int compare(ChooseGivedMedReportDTO o1, ChooseGivedMedReportDTO o2) {
                    return o1.getStatusGRP().compareTo(o2.getStatusGRP());
                }

            });
            return lstReport;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }//END search list pres gived medicine by CFD and medicalRecordID

    //find list prescription by MedRCID
    public List<AdminListPres> findListPresByMedRCID(String medRCID) {
        try {
            TbMedicalRecord med = em.find(TbMedicalRecord.class, medRCID);
            if (med == null) {
                return null;
            }
            List<TbPrescription> lstPres = (List<TbPrescription>) med.getTbPrescriptionCollection();
            List<AdminListPres> lstad = new ArrayList<AdminListPres>();
            for (TbPrescription tbPrescription : lstPres) {
                AdminListPres ad = new AdminListPres();
                ad.setPresID(tbPrescription.getPrescriptionID());
                ad.setCreateFDate(tbPrescription.getCreateForDate());
                ad.setDateCreate(tbPrescription.getDateCreate());
                ad.setEmployeeName(tbPrescription.getEmployeeID().getFullname());
                ad.setStatus(tbPrescription.getStatusPres());
                lstad.add(ad);
            }
            return lstad;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }// END find list prescription by MedRCID
    
    //find list supp by MedRCID
    public List<AdminListSupp> findListSuppByMedRCID(String medRCID) {
        try {
            TbMedicalRecord med = em.find(TbMedicalRecord.class, medRCID);
            if (med == null) {
                return null;
            }
            List<TbMedicineSupplies> lstPres = (List<TbMedicineSupplies>) med.getTbMedicineSuppliesCollection();
            List<AdminListSupp> lstad = new ArrayList<AdminListSupp>();
            for (TbMedicineSupplies tbPrescription : lstPres) {
                AdminListSupp ad = new AdminListSupp();
                ad.setSuppID(tbPrescription.getMedicineSuppliesID());
                ad.setCreateFDate(tbPrescription.getCreateForDate());
                ad.setDateCreate(tbPrescription.getDateCreate());
                ad.setEmployeeName(tbPrescription.getEmployeeID().getFullname());
                ad.setStatus(tbPrescription.getStatusMedSupp());
                lstad.add(ad);
            }
            return lstad;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }// END find list supp by MedRCID
    
    //get prescription detail 
    public TbPrescriptionDetail prescriptionDetailByPresDetaID(String  presDetaID) {
        try {
            TbPrescriptionDetail predeta = em.find(TbPrescriptionDetail.class, presDetaID);
            return predeta;
        } catch (Exception e) {
        }
        return null;
    } //end get prescription detail 
    
    //get med supp detail 
    public TbMedicineSuppliesDetail suppDetailBySuppDetaID(String  presDetaID) {
        try {
            TbMedicineSuppliesDetail predeta = em.find(TbMedicineSuppliesDetail.class, presDetaID);
            return predeta;
        } catch (Exception e) {
        }
        return null;
    } //end get med supp detail 
}
