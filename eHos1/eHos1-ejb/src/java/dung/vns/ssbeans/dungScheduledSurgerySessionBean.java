/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dung.vns.ssbeans;

import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import project.ett.TbEmployee;
import project.ett.TbScheduledSurgery;
import project.ett.TbSuggestSurgery;

/**
 *
 * @author Sony
 */
@Stateless
@LocalBean
public class dungScheduledSurgerySessionBean {
    @PersistenceContext(unitName = "eHos1-ejbPU")
    private EntityManager em;

    public void persist(Object object) {
        em.persist(object);
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    
    public List<TbEmployee> getAllEmployee() {
        try {
            String ejbSQL = "SELECT t FROM TbEmployee t";
            Query query = em.createQuery(ejbSQL);
            List<TbEmployee> listPro = query.getResultList();
            return listPro;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<TbScheduledSurgery> getAllScheduledSurgery(String txtsuggestID) {
        try {
            TbSuggestSurgery tbSS = em.find(TbSuggestSurgery.class, txtsuggestID);
            return (List<TbScheduledSurgery>) tbSS.getTbScheduledSurgeryCollection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    /* Find By PK Level Surgery */

    public TbScheduledSurgery findByPK(String id) {
        return em.find(TbScheduledSurgery.class, id);
    }
    /* Insert Level Surgery */

    public boolean insertScheduledSurgery(TbScheduledSurgery tbSScheduled, String suggestID, String emID) {
        try {

            TbSuggestSurgery tbSS = em.find(TbSuggestSurgery.class, suggestID);
            tbSScheduled.setSuggestSurgeryID(tbSS);
            tbSS.getTbScheduledSurgeryCollection().add(tbSScheduled);

            TbEmployee tbE = em.find(TbEmployee.class, emID);
            tbSScheduled.setUserID(tbE);
            tbE.getTbScheduledSurgeryCollection().add(tbSScheduled);

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
    /* Update OrderDetail Method - Main - Quantity, Price */

    public boolean updateScheduledSurgery(TbScheduledSurgery tbSScheduled, String suggestID, String emID) {
        try {
            /* Update Properties */
            TbScheduledSurgery oldSScheduled = em.find(TbScheduledSurgery.class, tbSScheduled.getScheduledID());

            /* Remove collection old */
            TbSuggestSurgery tbSS = em.find(TbSuggestSurgery.class, oldSScheduled.getSuggestSurgeryID().getSuggestSurgeryID());
            tbSS.getTbScheduledSurgeryCollection().remove(oldSScheduled);

            TbEmployee tbE = em.find(TbEmployee.class, oldSScheduled.getUserID().getEmployeeID());
            tbE.getTbScheduledSurgeryCollection().remove(oldSScheduled);

            /* Add collection new */
            TbSuggestSurgery newSS = em.find(TbSuggestSurgery.class, suggestID);
            oldSScheduled.setSuggestSurgeryID(newSS);

            oldSScheduled.setTimeOfStart(tbSScheduled.getTimeOfStart());
            oldSScheduled.setTimeOfEnd(tbSScheduled.getTimeOfEnd());
            oldSScheduled.setStatusScheduled(tbSScheduled.getStatusScheduled());
            oldSScheduled.setCreateDate(tbSScheduled.getCreateDate());
            
            TbEmployee newEm = em.find(TbEmployee.class, emID);
            oldSScheduled.setUserID(newEm);

            newSS.getTbScheduledSurgeryCollection().add(oldSScheduled);
            newEm.getTbScheduledSurgeryCollection().add(oldSScheduled);

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
    /*
     public List<TbConsulationSurgery> searchLikeNameSuggestSurgery(String suggestID, String txtSearch) {
     List<TbConsulationSurgery> result = new ArrayList<>();
     try {

     TbMedicalRecord tbMR = em.find(TbMedicalRecord.class, medicalID);
     result = (List<TbSuggestSurgery>) tbMR.getTbSuggestSurgeryCollection();

     } catch (Exception e) {
     e.printStackTrace();
     }
     return result;
     }
     */

    public boolean checkExistWhenInsert(String suggestID) {
        boolean result = false;
        try {
            TbSuggestSurgery tbSS = em.find(TbSuggestSurgery.class, suggestID);
            List<TbScheduledSurgery> listresult = (List<TbScheduledSurgery>)tbSS.getTbScheduledSurgeryCollection();
            if (listresult.size() > 0) {
                return true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public boolean checkExistWhenUpdate(String suggestID) {
        boolean result = false;
        try {
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
    
}
