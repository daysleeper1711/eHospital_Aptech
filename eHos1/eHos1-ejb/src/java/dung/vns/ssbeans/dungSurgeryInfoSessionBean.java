/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dung.vns.ssbeans;

import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import project.ett.TbEmployee;
import project.ett.TbSuggestSurgery;
import project.ett.TbSurgeryRecord;

/**
 *
 * @author Sony
 */
@Stateless
@LocalBean
public class dungSurgeryInfoSessionBean {

    @PersistenceContext(unitName = "eHos1-ejbPU")
    private EntityManager em;

    public void persist(Object object) {
        em.persist(object);
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    
     public TbEmployee findEmp(String id)
    {
        return em.find(TbEmployee.class, id);
    }
    
    public List<TbEmployee> getAllEmployee() {
        try {
            String ejbSQL = "SELECT t FROM TbEmployee t";
            Query query = em.createQuery(ejbSQL);
            List<TbEmployee> listPro = query.getResultList();
            return listPro;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<TbSurgeryRecord> getAllSurgeryRecord(String txtsuggestID) {
        try {
            TbSuggestSurgery tbSS = em.find(TbSuggestSurgery.class, txtsuggestID);
            return (List<TbSurgeryRecord>) tbSS.getTbSurgeryRecordCollection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    /* Find By PK Level Surgery */

    public TbSurgeryRecord findByPK(String id) {
        return em.find(TbSurgeryRecord.class, id);
    }
    /* Insert Level Surgery */

    public boolean insertSurgeryRecord(TbSurgeryRecord tbSR, String suggestID, String emID) {
        try {

            TbSuggestSurgery tbSS = em.find(TbSuggestSurgery.class, suggestID);
            tbSR.setSuggestSurgeryID(tbSS);
            tbSS.getTbSurgeryRecordCollection().add(tbSR);

            TbEmployee tbE = em.find(TbEmployee.class, emID);
            tbSR.setUserID(tbE);
            tbE.getTbSurgeryRecordCollection().add(tbSR);

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
    /* Update OrderDetail Method - Main - Quantity, Price */

    public boolean updateSurgeryRecord(TbSurgeryRecord tbSR, String suggestID, String emID) {
        try {
            /* Update Properties */
            TbSurgeryRecord oldRS = em.find(TbSurgeryRecord.class, tbSR.getSurgeryRecordID());

            /* Remove collection old */
            TbSuggestSurgery tbSS = em.find(TbSuggestSurgery.class, oldRS.getSuggestSurgeryID().getSuggestSurgeryID());
            tbSS.getTbSurgeryRecordCollection().remove(oldRS);

            TbEmployee tbE = em.find(TbEmployee.class, oldRS.getUserID().getEmployeeID());
            tbE.getTbSurgeryRecordCollection().remove(oldRS);

            /* Add collection new */
            TbSuggestSurgery newSS = em.find(TbSuggestSurgery.class, suggestID);
            oldRS.setSuggestSurgeryID(newSS);
            
            oldRS.setProcessOfSurgery(tbSR.getProcessOfSurgery());
            oldRS.setTimeOfStart(tbSR.getTimeOfStart());
            oldRS.setTimeOfEnd(tbSR.getTimeOfEnd());
            oldRS.setSurgeryResult(tbSR.getSurgeryResult());
            oldRS.setBeginCost(tbSR.getBeginCost());
            oldRS.setPayCost(tbSR.getPayCost());
            oldRS.setStatusSurgery(tbSR.getStatusSurgery());
            oldRS.setCreateDate(tbSR.getCreateDate());
            
            TbEmployee newEm = em.find(TbEmployee.class, emID);
            oldRS.setUserID(newEm);

            newSS.getTbSurgeryRecordCollection().add(oldRS);
            newEm.getTbSurgeryRecordCollection().add(oldRS);

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
    
    public boolean checkExistWhenInsert(String suggestID) {
        boolean result = false;
        try {
            TbSuggestSurgery tbSS = em.find(TbSuggestSurgery.class, suggestID);
            List<TbSurgeryRecord> listresult = (List<TbSurgeryRecord>)tbSS.getTbSurgeryRecordCollection();
            if (listresult.size() > 0) {
                return true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public boolean checkExistWhenUpdate(String suggestID) {
        boolean result = false;
        try {
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
