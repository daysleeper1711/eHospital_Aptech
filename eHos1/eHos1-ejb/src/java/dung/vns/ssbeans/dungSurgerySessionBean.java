/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dung.vns.ssbeans;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import project.ett.TbEmployee;
import project.ett.TbLevelSurgery;

/**
 *
 * @author Sony
 */
@Stateless
@LocalBean
public class dungSurgerySessionBean {

    @PersistenceContext(unitName = "eHos1-ejbPU")
    private EntityManager em;

    public void persist(Object object) {
        em.persist(object);
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    public List<TbLevelSurgery> getAllLevelSurgery() {
        try {
            String ejbSQL = "SELECT t FROM TbLevelSurgery t";
            Query query = em.createQuery(ejbSQL);
            List<TbLevelSurgery> listPro = query.getResultList();
            return listPro;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /* Find By PK Level Surgery */
    public TbLevelSurgery findByPK(int id) {
        return em.find(TbLevelSurgery.class, id);
    }
    /* Insert Level Surgery */

    public boolean insertLevelSurgery(TbLevelSurgery tbLV, String emID) {
        try {

            TbEmployee tbE = em.find(TbEmployee.class, emID);
            tbLV.setUserID(tbE);
            tbE.getTbLevelSurgeryCollection().add(tbLV);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
    /* Update OrderDetail Method - Main - Quantity, Price */

    public boolean updateLevelSurgery(TbLevelSurgery editLV, String emID) {
        try {

            /* Update Properties */
            TbLevelSurgery eLevel = em.find(TbLevelSurgery.class, editLV.getLevelID());
            TbEmployee oldEm = em.find(TbEmployee.class, eLevel.getUserID().getEmployeeID());
            oldEm.getTbLevelSurgeryCollection().remove(eLevel);

            TbEmployee newEm = em.find(TbEmployee.class, emID);
            eLevel.setLevelName(editLV.getLevelName());
            eLevel.setLevelDecription(editLV.getLevelDecription());
            eLevel.setStatusLevel(editLV.getStatusLevel());
            eLevel.setUserID(newEm);
            eLevel.setCreateDate(editLV.getCreateDate());
            newEm.getTbLevelSurgeryCollection().add(eLevel);

            //persist(eLevel);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public List<TbLevelSurgery> searchLikeNameLevelSurgery(String txtSearch) {
        List<TbLevelSurgery> result = new ArrayList<>();
        try {
            String ejbSQL = "SELECT t FROM TbLevelSurgery t WHERE t.levelName like :levelName";
            Query query = em.createQuery(ejbSQL);
            query.setParameter("levelName", "%" + txtSearch + "%");
            List<TbLevelSurgery> listPro = query.getResultList();
            return listPro;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public boolean checkExistWhenInsert(String lvName)
    {
        boolean result = false;
        try {
            String ejbSQL = "SELECT t FROM TbLevelSurgery t WHERE t.levelName = :levelName";
            Query query = em.createQuery(ejbSQL);
            query.setParameter("levelName", lvName);
            List<TbLevelSurgery> listPro = query.getResultList();
            if(listPro.size() > 0)
            {
                result = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
    public boolean checkExistWhenUpdate(int lvID, String lvName)
    {
        boolean result = false;
        try {
            String ejbSQL = "SELECT t FROM TbLevelSurgery t WHERE t.levelID != :levelID and t.levelName = :levelName";
            Query query = em.createQuery(ejbSQL);
            query.setParameter("levelID", lvID);
            query.setParameter("levelName", lvName);            
            List<TbLevelSurgery> listPro = query.getResultList();
            if(listPro.size() > 0)
            {
                result = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
    
}
