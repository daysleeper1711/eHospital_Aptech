/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dung.vns.ssbeans;

import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import project.ett.TbDrugsDetail;
import project.ett.TbEmployee;
import project.ett.TbItemCodeManagement;
import project.ett.TbSurgeryRecord;

/**
 *
 * @author Sony
 */
@Stateless
@LocalBean
public class dungDrugsDetailSessionBean {

    @PersistenceContext(unitName = "eHos1-ejbPU")
    private EntityManager em;

    public void persist(Object object) {
        em.persist(object);
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    
    public TbItemCodeManagement findICM(String txtDrugsID)
    {
        return em.find(TbItemCodeManagement.class, txtDrugsID);
    }
    
    public TbEmployee findEmp(String id)
    {
        return em.find(TbEmployee.class, id);
    }
    
    public List<TbItemCodeManagement> getAllItemCodeManagement() {
        String ejbSQL = "SELECT t FROM TbItemCodeManagement t WHERE t.typeITcode = :typeITcode";
        Query query = em.createQuery(ejbSQL);
        query.setParameter("typeITcode", "D");
        List<TbItemCodeManagement> listPro = query.getResultList();
        return listPro;
    }

    public List<TbDrugsDetail> getAllDrugsDetail() {
        try {
            String ejbSQL = "SELECT t FROM TbDrugsDetail t";
            Query query = em.createQuery(ejbSQL);
            List<TbDrugsDetail> listPro = query.getResultList();
            return listPro;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<TbDrugsDetail> getAllDrugsDetail(String txtSurgeryRecordID) {
        try {
            TbSurgeryRecord tbSR = em.find(TbSurgeryRecord.class, txtSurgeryRecordID);
            return (List<TbDrugsDetail>) tbSR.getTbDrugsDetailCollection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    /* Find By PK  */

    public TbDrugsDetail findByPK(String id) {
        return em.find(TbDrugsDetail.class, id);
    }
    /* Insert  */

    public boolean insertDrugsDetail(TbDrugsDetail tbDD, String txtSurgeryRecordID, String txtDrugsID, String emID) {
        try {

            TbSurgeryRecord tbSR = em.find(TbSurgeryRecord.class, txtSurgeryRecordID);
            tbDD.setSurgeryRecordID(tbSR);
            tbSR.getTbDrugsDetailCollection().add(tbDD);

            TbItemCodeManagement tbICM = em.find(TbItemCodeManagement.class, txtDrugsID);
            tbDD.setDrugID(tbICM);
            tbICM.getTbDrugsDetailCollection().add(tbDD);

            TbEmployee tbE = em.find(TbEmployee.class, emID);
            tbDD.setUserID(tbE);
            tbE.getTbDrugsDetailCollection().add(tbDD);

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean updateDrugsDetail(TbDrugsDetail tbDD, String txtSurgeryRecordID, String txtDrugsID, String emID) {
        try {
            /* Update Properties */

            TbDrugsDetail oldDD = em.find(TbDrugsDetail.class, tbDD.getDrugsDetailID());

            TbSurgeryRecord oldSR = em.find(TbSurgeryRecord.class, oldDD.getSurgeryRecordID().getSurgeryRecordID());
            oldSR.getTbDrugsDetailCollection().remove(oldDD);

            TbItemCodeManagement oldICM = em.find(TbItemCodeManagement.class, oldDD.getDrugID().getItemCode());
            oldICM.getTbDrugsDetailCollection().remove(oldDD);

            oldDD.setUnit(tbDD.getUnit());
            oldDD.setQuantity(tbDD.getQuantity());
            oldDD.setStatusDrugs(tbDD.getStatusDrugs());

            TbEmployee oldEmp = em.find(TbEmployee.class, oldDD.getUserID().getEmployeeID());
            oldEmp.getTbDrugsDetailCollection().remove(oldDD);

            oldDD.setCreateDate(tbDD.getCreateDate());

            /* update info */
            TbSurgeryRecord newSR = em.find(TbSurgeryRecord.class, txtSurgeryRecordID);
            oldDD.setSurgeryRecordID(newSR);

            TbItemCodeManagement newICM = em.find(TbItemCodeManagement.class, txtDrugsID);
            oldDD.setDrugID(newICM);

            TbEmployee newEmp = em.find(TbEmployee.class, emID);
            oldDD.setUserID(newEmp);

            newSR.getTbDrugsDetailCollection().add(oldDD);
            newICM.getTbDrugsDetailCollection().add(oldDD);
            newEmp.getTbDrugsDetailCollection().add(oldDD);

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /*
     public List<TbPositionConsulation> searchLikeNamePositionConsultation(String txtSearch) {
     List<TbPositionConsulation> result = new ArrayList<>();
     try {
     String ejbSQL = "SELECT t FROM TbPositionConsulation t WHERE t.positionName like :positionName";
     Query query = em.createQuery(ejbSQL);
     query.setParameter("positionName", "%" + txtSearch + "%");
     List<TbPositionConsulation> listPro = query.getResultList();
     return listPro;
     } catch (Exception e) {
     e.printStackTrace();
     }
     return result;
     }
     */
    public boolean checkExistWhenInsert(String txtSurgeryRecordID, String txtDrugsID) {
        boolean result = false;
        try {

            TbSurgeryRecord tbSR = em.find(TbSurgeryRecord.class, txtSurgeryRecordID);
            List<TbDrugsDetail> listtbCDD = (List<TbDrugsDetail>) tbSR.getTbDrugsDetailCollection();
            if (listtbCDD.size() > 0) {
                for (TbDrugsDetail tbDDetail : listtbCDD) {
                    if (tbDDetail.getDrugID().getItemCode().equals(txtDrugsID)) {
                        return true;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public boolean checkExistWhenUpdate(String txtSurgeryRecordID, String txtDrugsDetailID, String txtDrugsID) {
        boolean result = false;
        try {

            TbSurgeryRecord tbSR = em.find(TbSurgeryRecord.class, txtSurgeryRecordID);
            List<TbDrugsDetail> listtbCDD = (List<TbDrugsDetail>) tbSR.getTbDrugsDetailCollection();
            if (listtbCDD.size() > 0) {
                for (TbDrugsDetail tbDDetail : listtbCDD) {
                    if (!tbDDetail.getDrugsDetailID().equals(txtDrugsDetailID)) {
                        if (tbDDetail.getDrugID().getItemCode().equals(txtDrugsID)) {
                            return true;
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
