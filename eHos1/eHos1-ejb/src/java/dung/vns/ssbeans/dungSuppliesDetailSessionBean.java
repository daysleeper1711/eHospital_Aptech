/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dung.vns.ssbeans;

import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import project.ett.TbEmployee;
import project.ett.TbItemCodeManagement;
import project.ett.TbSuppliesDetail;
import project.ett.TbSurgeryRecord;

/**
 *
 * @author Sony
 */
@Stateless
@LocalBean
public class dungSuppliesDetailSessionBean {
    @PersistenceContext(unitName = "eHos1-ejbPU")
    private EntityManager em;

    public void persist(Object object) {
        em.persist(object);
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    
    public TbItemCodeManagement findICM(String txtDrugsID)
    {
        return em.find(TbItemCodeManagement.class, txtDrugsID);
    }
    
    public TbEmployee findEmp(String id)
    {
        return em.find(TbEmployee.class, id);
    }
    
    public List<TbItemCodeManagement> getAllItemCodeManagement() {
        String ejbSQL = "SELECT t FROM TbItemCodeManagement t WHERE t.typeITcode = :typeITcode";
        Query query = em.createQuery(ejbSQL);
        query.setParameter("typeITcode", "S");
        List<TbItemCodeManagement> listPro = query.getResultList();
        return listPro;
    }
    
    public List<TbSuppliesDetail> getAllSuppliesDetail() {
        try {

            String ejbSQL = "SELECT t FROM TbSuppliesDetail t";
            Query query = em.createQuery(ejbSQL);
            List<TbSuppliesDetail> listPro = query.getResultList();
            return listPro;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<TbSuppliesDetail> getAllSuppliesDetail(String txtSurgeryRecordID) {
        try {
            TbSurgeryRecord tbSR = em.find(TbSurgeryRecord.class, txtSurgeryRecordID);
            return (List<TbSuppliesDetail>) tbSR.getTbSuppliesDetailCollection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    /* Find By PK  */

    public TbSuppliesDetail findByPK(String id) {
        return em.find(TbSuppliesDetail.class, id);
    }
    /* Insert  */

    public boolean insertSuppliesDetail(TbSuppliesDetail tbSD, String txtSurgeryRecordID, String txtSuppliesID, String emID) {
        try {

            TbSurgeryRecord tbSR = em.find(TbSurgeryRecord.class, txtSurgeryRecordID);
            tbSD.setSurgeryRecordID(tbSR);
            tbSR.getTbSuppliesDetailCollection().add(tbSD);


            TbItemCodeManagement tbICM = em.find(TbItemCodeManagement.class, txtSuppliesID);
            tbSD.setSuppliesID(tbICM);
            tbICM.getTbSuppliesDetailCollection().add(tbSD);

            TbEmployee tbE = em.find(TbEmployee.class, emID);
            tbSD.setUserID(tbE);
            tbE.getTbSuppliesDetailCollection().add(tbSD);

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean updateSuppliesDetail(TbSuppliesDetail tbSD, String txtSurgeryRecordID, String txtSuppliesID, String emID) {
        try {
            /* Update Properties */

            TbSuppliesDetail oldSD = em.find(TbSuppliesDetail.class, tbSD.getSuppliesDetailID());

            TbSurgeryRecord oldSR = em.find(TbSurgeryRecord.class, oldSD.getSurgeryRecordID().getSurgeryRecordID());
            oldSR.getTbSuppliesDetailCollection().remove(oldSD);

            TbItemCodeManagement oldICM = em.find(TbItemCodeManagement.class, oldSD.getSuppliesID().getItemCode());
            oldICM.getTbSuppliesDetailCollection().remove(oldSD);
            
            oldSD.setUnit(tbSD.getUnit());
            oldSD.setQuantity(tbSD.getQuantity());
            oldSD.setStatusSupplies(tbSD.getStatusSupplies());
            
            TbEmployee oldEmp = em.find(TbEmployee.class, oldSD.getUserID().getEmployeeID());
            oldEmp.getTbSuppliesDetailCollection().remove(oldSD);
            
            oldSD.setCreateDate(tbSD.getCreateDate());

            /* update info */
            TbSurgeryRecord newSR = em.find(TbSurgeryRecord.class, txtSurgeryRecordID);
            oldSD.setSurgeryRecordID(newSR);

            TbItemCodeManagement newICM = em.find(TbItemCodeManagement.class, txtSuppliesID);
            oldSD.setSuppliesID(newICM);

            TbEmployee newEmp = em.find(TbEmployee.class, emID);
            oldSD.setUserID(newEmp);

            newSR.getTbSuppliesDetailCollection().add(oldSD);
            newICM.getTbSuppliesDetailCollection().add(oldSD);
            newEmp.getTbSuppliesDetailCollection().add(oldSD);

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /*
     public List<TbPositionConsulation> searchLikeNamePositionConsultation(String txtSearch) {
     List<TbPositionConsulation> result = new ArrayList<>();
     try {
     String ejbSQL = "SELECT t FROM TbPositionConsulation t WHERE t.positionName like :positionName";
     Query query = em.createQuery(ejbSQL);
     query.setParameter("positionName", "%" + txtSearch + "%");
     List<TbPositionConsulation> listPro = query.getResultList();
     return listPro;
     } catch (Exception e) {
     e.printStackTrace();
     }
     return result;
     }
     */
    public boolean checkExistWhenInsert(String txtSurgeryRecordID, String txtSuppliesID) {
        boolean result = false;
        try {

            TbSurgeryRecord tbSR = em.find(TbSurgeryRecord.class, txtSurgeryRecordID);
            List<TbSuppliesDetail> listtbCDD = (List<TbSuppliesDetail>) tbSR.getTbSuppliesDetailCollection();
            if (listtbCDD.size() > 0) {
                for (TbSuppliesDetail tbDDetail : listtbCDD) {
                    if (tbDDetail.getSuppliesID().getItemCode().equals(txtSuppliesID)) {
                        return true;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public boolean checkExistWhenUpdate(String txtSurgeryRecordID, String txtSuppliesDetailID, String txtSuppliesID) {
        boolean result = false;
        try {

            TbSurgeryRecord tbSR = em.find(TbSurgeryRecord.class, txtSurgeryRecordID);
            List<TbSuppliesDetail> listtbCDD = (List<TbSuppliesDetail>) tbSR.getTbSuppliesDetailCollection();
            if (listtbCDD.size() > 0) {
                for (TbSuppliesDetail tbDDetail : listtbCDD) {
                    if (!tbDDetail.getSuppliesDetailID().equals(txtSuppliesDetailID)) {
                        if (tbDDetail.getSuppliesID().getItemCode().equals(txtSuppliesID)) {
                            return true;
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
    
}
