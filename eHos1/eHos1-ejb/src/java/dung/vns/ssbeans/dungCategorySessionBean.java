/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dung.vns.ssbeans;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import project.ett.TbCategory;
import project.ett.TbEmployee;

/**
 *
 * @author Sony
 */
@Stateless
@LocalBean
public class dungCategorySessionBean {

    @PersistenceContext(unitName = "eHos1-ejbPU")
    private EntityManager em;

    public void persist(Object object) {
        em.persist(object);
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    public List<TbCategory> getAllCategorySurgery() {
        try {
            String ejbSQL = "SELECT t FROM TbCategory t";
            Query query = em.createQuery(ejbSQL);
            List<TbCategory> listPro = query.getResultList();
            return listPro;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /* Find By PK Category Surgery */
    public TbCategory findByPK(int id) {
        return em.find(TbCategory.class, id);
    }
    /* Insert Category Surgery */

    public boolean insertCatetorySurgery(TbCategory tbLV, String emID) {
        try {

            TbEmployee tbE = em.find(TbEmployee.class, emID);
            tbLV.setUserID(tbE);
            tbE.getTbCategoryCollection().add(tbLV);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean updateCategorySurgery(TbCategory editLV, String emID) {
        try {
            
            /* Update Properties */
            TbCategory eLevel = em.find(TbCategory.class, editLV.getCategoryID());
            TbEmployee oldEm = em.find(TbEmployee.class, eLevel.getUserID().getEmployeeID());
            oldEm.getTbCategoryCollection().remove(eLevel);

            TbEmployee newEm = em.find(TbEmployee.class, emID);
            
            eLevel.setCategoryName(editLV.getCategoryName());
            eLevel.setCategoryDescription(editLV.getCategoryDescription());
            eLevel.setStatusCategory(editLV.getStatusCategory());
            eLevel.setUserID(newEm);
            eLevel.setCreateDate(editLV.getCreateDate());
            newEm.getTbCategoryCollection().add(eLevel);
            
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public List<TbCategory> searchLikeNameCategorySurgery(String txtSearch) {
        List<TbCategory> result = new ArrayList<>();
        try {
            String ejbSQL = "SELECT t FROM TbCategory t WHERE t.categoryName like :categoryName";
            Query query = em.createQuery(ejbSQL);
            query.setParameter("categoryName", "%" + txtSearch + "%");
            List<TbCategory> listPro = query.getResultList();
            return listPro;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public boolean checkExistWhenInsert(String lvName) {
        boolean result = false;
        try {
            String ejbSQL = "SELECT t FROM TbCategory t WHERE t.categoryName = :categoryName";
            Query query = em.createQuery(ejbSQL);
            query.setParameter("categoryName", lvName);
            List<TbCategory> listPro = query.getResultList();
            if (listPro.size() > 0) {
                result = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public boolean checkExistWhenUpdate(int lvID, String lvName) {
        boolean result = false;
        try {
            String ejbSQL = "SELECT t FROM TbCategory t WHERE t.categoryID != :categoryID and t.categoryName = :categoryName";
            Query query = em.createQuery(ejbSQL);
            query.setParameter("categoryID", lvID);
            query.setParameter("categoryName", lvName);
            List<TbCategory> listPro = query.getResultList();
            if (listPro.size() > 0) {
                result = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

}
