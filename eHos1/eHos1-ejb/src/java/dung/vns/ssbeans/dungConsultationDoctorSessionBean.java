/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dung.vns.ssbeans;

import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import project.ett.TbConsulationDoctorDetail;
import project.ett.TbConsulationSurgery;
import project.ett.TbEmployee;
import project.ett.TbPositionConsulation;

/**
 *
 * @author Sony
 */
@Stateless
@LocalBean
public class dungConsultationDoctorSessionBean {

    @PersistenceContext(unitName = "eHos1-ejbPU")
    private EntityManager em;

    public void persist(Object object) {
        em.persist(object);
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    
    public List<TbEmployee> getAllDoctor() {
        try {
            String ejbSQL = "SELECT t FROM TbEmployee t WHERE t.position = :position";
            Query query = em.createQuery(ejbSQL);
            query.setParameter("position", "doctor");
            List<TbEmployee> listPro = query.getResultList();
            return listPro;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    
    public List<TbConsulationDoctorDetail> getAllConsultationDoctorDetail() {
        try {

            String ejbSQL = "SELECT t FROM TbConsulationDoctorDetail t";
            Query query = em.createQuery(ejbSQL);
            List<TbConsulationDoctorDetail> listPro = query.getResultList();
            return listPro;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<TbConsulationDoctorDetail> getAllConsultationDoctorDetail(String txtConsultationID) {
        try {
            TbConsulationSurgery tbCS = em.find(TbConsulationSurgery.class, txtConsultationID);
            return (List<TbConsulationDoctorDetail>) tbCS.getTbConsulationDoctorDetailCollection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    /* Find By PK  */

    public TbConsulationDoctorDetail findByPK(String id) {
        return em.find(TbConsulationDoctorDetail.class, id);
    }
    /* Insert  */

    public boolean insertConsultationDoctorDetail(TbConsulationDoctorDetail tbCDD, String txtConsultationID, String txtDoctorID, int txtPositionID, String emID) {
        try {

            TbConsulationSurgery tbCS = em.find(TbConsulationSurgery.class, txtConsultationID);
            tbCDD.setConsulationID(tbCS);
            tbCS.getTbConsulationDoctorDetailCollection().add(tbCDD);

            TbEmployee tbDoctor = em.find(TbEmployee.class, txtDoctorID);
            tbCDD.setDoctorID(tbDoctor);
            tbDoctor.getTbConsulationDoctorDetailCollection().add(tbCDD);

            TbPositionConsulation tbPS = em.find(TbPositionConsulation.class, txtPositionID);
            tbCDD.setPositionID(tbPS);
            tbPS.getTbConsulationDoctorDetailCollection().add(tbCDD);

            TbEmployee tbE = em.find(TbEmployee.class, emID);
            tbCDD.setUserID(tbE);
            tbE.getTbConsulationDoctorDetailCollection().add(tbCDD);

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean updateConsultationDoctorDetail(TbConsulationDoctorDetail tbCDD, String txtConsultationID, String txtDoctorID, int txtPositionID, String emID) {
        try {
            /* Update Properties */

            TbConsulationDoctorDetail oldCDD = em.find(TbConsulationDoctorDetail.class, tbCDD.getConsulationDoctorDetailID());

            TbConsulationSurgery oldSS = em.find(TbConsulationSurgery.class, oldCDD.getConsulationID().getConsulationID());
            oldSS.getTbConsulationDoctorDetailCollection().remove(oldCDD);

            TbEmployee oldDoctor = em.find(TbEmployee.class, oldCDD.getDoctorID().getEmployeeID());
            oldDoctor.getTbConsulationDoctorDetailCollection().remove(oldCDD);

            TbPositionConsulation oldPC = em.find(TbPositionConsulation.class, oldCDD.getPositionID().getPositionID());
            oldPC.getTbConsulationDoctorDetailCollection().remove(oldCDD);

            oldCDD.setStatusDetail(tbCDD.getStatusDetail());

            TbEmployee oldEmp = em.find(TbEmployee.class, oldCDD.getUserID().getEmployeeID());
            oldEmp.getTbConsulationDoctorDetailCollection().remove(oldCDD);

            /* update info */
            TbConsulationSurgery newSS = em.find(TbConsulationSurgery.class, txtConsultationID);
            oldCDD.setConsulationID(newSS);

            TbEmployee newDoctor = em.find(TbEmployee.class, txtDoctorID);
            oldCDD.setDoctorID(newDoctor);

            TbPositionConsulation newPC = em.find(TbPositionConsulation.class, txtPositionID);
            oldCDD.setPositionID(newPC);

            TbEmployee newEmp = em.find(TbEmployee.class, emID);
            oldCDD.setUserID(newEmp);

            newSS.getTbConsulationDoctorDetailCollection().add(oldCDD);
            newDoctor.getTbConsulationDoctorDetailCollection().add(oldCDD);
            newPC.getTbConsulationDoctorDetailCollection().add(oldCDD);
            newEmp.getTbConsulationDoctorDetailCollection().add(oldCDD);

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /*
     public List<TbPositionConsulation> searchLikeNamePositionConsultation(String txtSearch) {
     List<TbPositionConsulation> result = new ArrayList<>();
     try {
     String ejbSQL = "SELECT t FROM TbPositionConsulation t WHERE t.positionName like :positionName";
     Query query = em.createQuery(ejbSQL);
     query.setParameter("positionName", "%" + txtSearch + "%");
     List<TbPositionConsulation> listPro = query.getResultList();
     return listPro;
     } catch (Exception e) {
     e.printStackTrace();
     }
     return result;
     }
     */
    public boolean checkExistWhenInsert(String txtConsultationID, String txtDoctorID) {
        boolean result = false;
        try {

            TbConsulationSurgery tbCS = em.find(TbConsulationSurgery.class, txtConsultationID);
            List<TbConsulationDoctorDetail> listtbCDD = (List<TbConsulationDoctorDetail>) tbCS.getTbConsulationDoctorDetailCollection();
            if (listtbCDD.size() > 0) {
                for (TbConsulationDoctorDetail tbConsulationDoctorDetail : listtbCDD) {
                    if (tbConsulationDoctorDetail.getDoctorID().getEmployeeID().equals(txtDoctorID)) {
                        return true;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public boolean checkExistWhenUpdate(String txtConsultationID, String txtConsultationDoctorDetailID, String txtDoctorID) {
        boolean result = false;
        try {

            TbConsulationSurgery tbCS = em.find(TbConsulationSurgery.class, txtConsultationID);
            List<TbConsulationDoctorDetail> listtbCDD = (List<TbConsulationDoctorDetail>) tbCS.getTbConsulationDoctorDetailCollection();
            if (listtbCDD.size() > 0) {
                for (TbConsulationDoctorDetail tbConsulationDoctorDetail : listtbCDD) {
                    if (!tbConsulationDoctorDetail.getConsulationDoctorDetailID().equals(txtConsultationDoctorDetailID)) {
                        if (tbConsulationDoctorDetail.getDoctorID().getEmployeeID().equals(txtDoctorID)) {
                            return true;
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

}
