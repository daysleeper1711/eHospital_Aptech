/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dung.vns.ssbeans;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import project.ett.TbCategory;
import project.ett.TbEmployee;
import project.ett.TbFaculty;
import project.ett.TbLevelSurgery;
import project.ett.TbSurgeryService;

/**
 *
 * @author Sony
 */
@Stateless
@LocalBean
public class dungSurgeryServiceSessionBean {
    @PersistenceContext(unitName = "eHos1-ejbPU")
    private EntityManager em;

    public void persist(Object object) {
        em.persist(object);
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    public List<TbSurgeryService> getAllSurgeryService() {
        try {
            String ejbSQL = "SELECT t FROM TbSurgeryService t";
            Query query = em.createQuery(ejbSQL);
            List<TbSurgeryService> listPro = query.getResultList();
            return listPro;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    
    public List<TbFaculty> getAllFaculty() {
        try {
            String ejbSQL = "SELECT t FROM TbFaculty t";
            Query query = em.createQuery(ejbSQL);
            List<TbFaculty> listPro = query.getResultList();
            return listPro;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /* Find By PK Category Surgery */
    public TbSurgeryService findByPK(String id) {
        return em.find(TbSurgeryService.class, id);
    }
    /* Insert Category Surgery */

    public boolean insertSurgeryService(TbSurgeryService tbLV, String emID, String vfac, int vcate, int vlevel) {
        try {

            TbEmployee tbE = em.find(TbEmployee.class, emID);
            TbFaculty tbF = em.find(TbFaculty.class, vfac);
            TbCategory tbC = em.find(TbCategory.class, vcate);
            TbLevelSurgery tbL = em.find(TbLevelSurgery.class, vlevel);
            
            tbLV.setUserID(tbE);
            tbE.getTbSurgeryServiceCollection().add(tbLV);
            tbLV.setFaultyID(tbF);
            tbF.getTbSurgeryServiceCollection().add(tbLV);
            tbLV.setCategoryID(tbC);
            tbC.getTbSurgeryServiceCollection().add(tbLV);
            tbLV.setLevelSurgery(tbL);
            tbL.getTbSurgeryServiceCollection().add(tbLV);
            
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean updateSurgeryService(TbSurgeryService editLV, String emID, String efac, int ecate, int elevel) {
        try {
            
            /* Update Properties */
            TbSurgeryService eLevel = em.find(TbSurgeryService.class, editLV.getSurgeryServiceID());
            
            TbEmployee oldEm = em.find(TbEmployee.class, eLevel.getUserID().getEmployeeID());
            TbFaculty oldFac = em.find(TbFaculty.class, eLevel.getFaultyID().getFaID());
            TbCategory oldCate = em.find(TbCategory.class, eLevel.getCategoryID().getCategoryID());
            TbLevelSurgery oldLevelSurgery = em.find(TbLevelSurgery.class, eLevel.getLevelSurgery().getLevelID());
            
            oldEm.getTbSurgeryServiceCollection().remove(eLevel);
            oldFac.getTbSurgeryServiceCollection().remove(eLevel);
            oldCate.getTbSurgeryServiceCollection().remove(eLevel);
            oldLevelSurgery.getTbSurgeryServiceCollection().remove(eLevel);
            

            TbEmployee newEm = em.find(TbEmployee.class, emID);
            TbFaculty newFac = em.find(TbFaculty.class, efac);
            TbCategory newCate = em.find(TbCategory.class, ecate);
            TbLevelSurgery newLevel = em.find(TbLevelSurgery.class, elevel);
           
            eLevel.setSurgeryServiceName(editLV.getSurgeryServiceName());
            eLevel.setServiceDescription(editLV.getServiceDescription());
            eLevel.setInitialCost(editLV.getInitialCost());
            
            //eLevel.setFaultyID(editLV.getFaultyID());
            eLevel.setFaultyID(newFac);
            //eLevel.setCategoryID(editLV.getCategoryID());
            eLevel.setCategoryID(newCate);
            //eLevel.setLevelSurgery(editLV.getLevelSurgery());
            eLevel.setLevelSurgery(newLevel);
            
            eLevel.setStatusService(editLV.getStatusService());
            eLevel.setUserID(newEm);
            eLevel.setCreateDate(editLV.getCreateDate());
            
            newEm.getTbSurgeryServiceCollection().add(eLevel);
            newFac.getTbSurgeryServiceCollection().add(eLevel);
            newCate.getTbSurgeryServiceCollection().add(eLevel);
            newLevel.getTbSurgeryServiceCollection().add(eLevel);
            
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public List<TbSurgeryService> searchLikeNameSurgeryService(String txtSearch) {
        List<TbSurgeryService> result = new ArrayList<>();
        try {
            String ejbSQL = "SELECT t FROM TbSurgeryService t WHERE t.surgeryServiceName like :surgeryServiceName";
            Query query = em.createQuery(ejbSQL);
            query.setParameter("surgeryServiceName", "%" + txtSearch + "%");
            List<TbSurgeryService> listPro = query.getResultList();
            return listPro;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public boolean checkExistWhenInsert(String lvName) {
        boolean result = false;
        try {
            String ejbSQL = "SELECT t FROM TbSurgeryService t WHERE t.surgeryServiceName = :surgeryServiceName";
            Query query = em.createQuery(ejbSQL);
            query.setParameter("surgeryServiceName", lvName);
            List<TbSurgeryService> listPro = query.getResultList();
            if (listPro.size() > 0) {
                result = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public boolean checkExistWhenUpdate(String lvID, String lvName) {
        boolean result = false;
        try {
            String ejbSQL = "SELECT t FROM TbSurgeryService t WHERE t.surgeryServiceID != :surgeryServiceID and t.surgeryServiceName = :surgeryServiceName";
            Query query = em.createQuery(ejbSQL);
            query.setParameter("surgeryServiceID", lvID);
            query.setParameter("surgeryServiceName", lvName);
            List<TbSurgeryService> listPro = query.getResultList();
            if (listPro.size() > 0) {
                result = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
    
    
}
