/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dung.vns.ssbeans;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import project.ett.TbEmployee;
import project.ett.TbPositionDoctor;

/**
 *
 * @author Sony
 */
@Stateless
@LocalBean
public class dungPositionDoctorSessionBean {
    @PersistenceContext(unitName = "eHos1-ejbPU")
    private EntityManager em;

    public void persist(Object object) {
        em.persist(object);
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    public List<TbPositionDoctor> getAllPositionDoctor() {
        try {
            String ejbSQL = "SELECT t FROM TbPositionDoctor t";
            Query query = em.createQuery(ejbSQL);
            List<TbPositionDoctor> listPro = query.getResultList();
            return listPro;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /* Find By PK  */
    public TbPositionDoctor findByPK(int id) {
        return em.find(TbPositionDoctor.class, id);
    }
    /* Insert  */

    public boolean insertPositionDoctor(TbPositionDoctor tbLV, String emID) {
        try {
            TbEmployee tbE = em.find(TbEmployee.class, emID);
            tbLV.setUserID(tbE);
            tbE.getTbPositionDoctorCollection().add(tbLV);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean updatePositionDoctor(TbPositionDoctor editLV, String emID) {
        try {
            /* Update Properties */
            TbPositionDoctor eLevel = em.find(TbPositionDoctor.class, editLV.getPositionID());
            TbEmployee oldEm = em.find(TbEmployee.class, eLevel.getUserID().getEmployeeID());
            oldEm.getTbPositionDoctorCollection().remove(eLevel);

            TbEmployee newEm = em.find(TbEmployee.class, emID);
            
            eLevel.setPositionName(editLV.getPositionName());
            eLevel.setStatusPosition(editLV.getStatusPosition());
            eLevel.setUserID(newEm);
            eLevel.setCreateDate(editLV.getCreateDate());
            
            newEm.getTbPositionDoctorCollection().add(eLevel);
            
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public List<TbPositionDoctor> searchLikeNamePositionDoctor(String txtSearch) {
        List<TbPositionDoctor> result = new ArrayList<>();
        try {
            String ejbSQL = "SELECT t FROM TbPositionDoctor t WHERE t.positionName like :positionName";
            Query query = em.createQuery(ejbSQL);
            query.setParameter("positionName", "%" + txtSearch + "%");
            List<TbPositionDoctor> listPro = query.getResultList();
            return listPro;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public boolean checkExistWhenInsert(String lvName) {
        boolean result = false;
        try {
            String ejbSQL = "SELECT t FROM TbPositionDoctor t WHERE t.positionName = :positionName";
            Query query = em.createQuery(ejbSQL);
            query.setParameter("positionName", lvName);
            List<TbPositionDoctor> listPro = query.getResultList();
            if (listPro.size() > 0) {
                result = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public boolean checkExistWhenUpdate(int lvID, String lvName) {
        boolean result = false;
        try {
            String ejbSQL = "SELECT t FROM TbPositionDoctor t WHERE t.positionID != :positionID and t.positionName = :positionName";
            Query query = em.createQuery(ejbSQL);
            query.setParameter("positionID", lvID);
            query.setParameter("positionName", lvName);
            List<TbPositionDoctor> listPro = query.getResultList();
            if (listPro.size() > 0) {
                result = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
