/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dung.vns.ssbeans;

import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import project.ett.TbConsulationSurgery;
import project.ett.TbEmployee;
import project.ett.TbSuggestSurgery;

/**
 *
 * @author Sony
 */
@Stateless
@LocalBean
public class dungConsultationSurgerySessionBean {

    @PersistenceContext(unitName = "eHos1-ejbPU")
    private EntityManager em;

    public void persist(Object object) {
        em.persist(object);
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    public List<TbEmployee> getAllEmployee() {
        try {
            String ejbSQL = "SELECT t FROM TbEmployee t";
            Query query = em.createQuery(ejbSQL);
            List<TbEmployee> listPro = query.getResultList();
            return listPro;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<TbConsulationSurgery> getAllConsultationSurgery(String txtsuggestID) {
        try {
            TbSuggestSurgery tbSS = em.find(TbSuggestSurgery.class, txtsuggestID);
            return (List<TbConsulationSurgery>) tbSS.getTbConsulationSurgeryCollection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    /* Find By PK Level Surgery */

    public TbConsulationSurgery findByPK(String id) {
        return em.find(TbConsulationSurgery.class, id);
    }
    /* Insert Level Surgery */

    //public boolean insertSuggestSurgery(TbSuggestSurgery tbLV, String medicalID, String serviceID, String docID, String emID) {
    public boolean insertConsultationSurgery(TbConsulationSurgery tbCS, String suggestID, String emID) {
        try {

            TbSuggestSurgery tbSS = em.find(TbSuggestSurgery.class, suggestID);
            tbCS.setSuggestSurgeryID(tbSS);
            tbSS.getTbConsulationSurgeryCollection().add(tbCS);

            TbEmployee tbE = em.find(TbEmployee.class, emID);
            tbCS.setUserID(tbE);
            tbE.getTbConsulationSurgeryCollection().add(tbCS);

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
    /* Update OrderDetail Method - Main - Quantity, Price */

    public boolean updateConsultationSurgery(TbConsulationSurgery tbCS, String suggestID, String emID) {
        try {
            /* Update Properties */
            TbConsulationSurgery oldCS = em.find(TbConsulationSurgery.class, tbCS.getConsulationID());

            /* Remove collection old */
            TbSuggestSurgery tbSS = em.find(TbSuggestSurgery.class, oldCS.getSuggestSurgeryID().getSuggestSurgeryID());
            tbSS.getTbConsulationSurgeryCollection().remove(oldCS);

            TbEmployee tbE = em.find(TbEmployee.class, oldCS.getUserID().getEmployeeID());
            tbE.getTbConsulationSurgeryCollection().remove(oldCS);

            /* Add collection new */
            TbSuggestSurgery newSS = em.find(TbSuggestSurgery.class, suggestID);
            oldCS.setSuggestSurgeryID(newSS);

            oldCS.setConsulationDate(tbCS.getConsulationDate());
            oldCS.setConsulation(tbCS.getConsulation());
            oldCS.setStatusConsulation(tbCS.getStatusConsulation());

            TbEmployee newEm = em.find(TbEmployee.class, emID);
            oldCS.setUserID(newEm);

            newSS.getTbConsulationSurgeryCollection().add(oldCS);
            newEm.getTbConsulationSurgeryCollection().add(oldCS);

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
    /*
     public List<TbConsulationSurgery> searchLikeNameSuggestSurgery(String suggestID, String txtSearch) {
     List<TbConsulationSurgery> result = new ArrayList<>();
     try {

     TbMedicalRecord tbMR = em.find(TbMedicalRecord.class, medicalID);
     result = (List<TbSuggestSurgery>) tbMR.getTbSuggestSurgeryCollection();

     } catch (Exception e) {
     e.printStackTrace();
     }
     return result;
     }
     */

    public boolean checkExistWhenInsert(String suggestID) {
        boolean result = false;
        try {

            TbSuggestSurgery tbSS = em.find(TbSuggestSurgery.class, suggestID);
            List<TbConsulationSurgery> listresult = (List<TbConsulationSurgery>) tbSS.getTbConsulationSurgeryCollection();
            if (listresult.size() > 0) {
                return true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public boolean checkExistWhenUpdate(String suggestID) {
        boolean result = false;
        try {
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

}
