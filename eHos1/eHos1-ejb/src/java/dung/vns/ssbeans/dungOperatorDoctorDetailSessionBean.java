/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dung.vns.ssbeans;

import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import project.ett.TbEmployee;
import project.ett.TbOperatorDoctorDetail;
import project.ett.TbPositionDoctor;
import project.ett.TbSurgeryRecord;

/**
 *
 * @author Sony
 */
@Stateless
@LocalBean
public class dungOperatorDoctorDetailSessionBean {
    @PersistenceContext(unitName = "eHos1-ejbPU")
    private EntityManager em;

    public void persist(Object object) {
        em.persist(object);
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    
    public TbEmployee findEmp(String id)
    {
        return em.find(TbEmployee.class, id);
    }
    
    public List<TbOperatorDoctorDetail> getAllOperatorDoctorDetail() {
        try {

            String ejbSQL = "SELECT t FROM TbOperatorDoctorDetail t";
            Query query = em.createQuery(ejbSQL);
            List<TbOperatorDoctorDetail> listPro = query.getResultList();
            return listPro;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<TbOperatorDoctorDetail> getAllOperatorDoctorDetail(String txtSurgeryRecordID) {
        try {
            TbSurgeryRecord tbSR = em.find(TbSurgeryRecord.class, txtSurgeryRecordID);
            return (List<TbOperatorDoctorDetail>) tbSR.getTbOperatorDoctorDetailCollection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    /* Find By PK  */

    public TbOperatorDoctorDetail findByPK(String id) {
        return em.find(TbOperatorDoctorDetail.class, id);
    }
    /* Insert  */

    public boolean insertOperatorDoctorDetail(TbOperatorDoctorDetail tbODD, String txtSurgeryRecordID, String txtDoctorID, int txtPositionID, String emID) {
        try {

            TbSurgeryRecord tbSR = em.find(TbSurgeryRecord.class, txtSurgeryRecordID);
            tbODD.setSurgeryRecordID(tbSR);
            tbSR.getTbOperatorDoctorDetailCollection().add(tbODD);

            TbEmployee tbDoctor = em.find(TbEmployee.class, txtDoctorID);
            tbODD.setDoctorID(tbDoctor);
            tbDoctor.getTbOperatorDoctorDetailCollection().add(tbODD);

            TbPositionDoctor tbPD = em.find(TbPositionDoctor.class, txtPositionID);
            tbODD.setPositionID(tbPD);
            tbPD.getTbOperatorDoctorDetailCollection().add(tbODD);
            
            TbEmployee tbE = em.find(TbEmployee.class, emID);
            tbODD.setUserID(tbE);
            tbE.getTbOperatorDoctorDetailCollection().add(tbODD);

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean updateDrugsDetail(TbOperatorDoctorDetail tbODD, String txtSurgeryRecordID, String txtDoctorID, String txtPositionID, String emID) {
        try {
            /* Update Properties */

            TbOperatorDoctorDetail oldODD = em.find(TbOperatorDoctorDetail.class, tbODD.getOperatorDoctorDetailID());

            TbSurgeryRecord oldSR = em.find(TbSurgeryRecord.class, oldODD.getSurgeryRecordID().getSurgeryRecordID());
            oldSR.getTbOperatorDoctorDetailCollection().remove(oldODD);

            TbEmployee oldDoctor = em.find(TbEmployee.class, oldODD.getDoctorID().getEmployeeID());
            oldDoctor.getTbOperatorDoctorDetailCollection().remove(oldODD);
            
            TbPositionDoctor oldPD = em.find(TbPositionDoctor.class, oldODD.getPositionID().getPositionID());
            oldPD.getTbOperatorDoctorDetailCollection().remove(oldODD);
            
            oldODD.setStatusPosition(tbODD.getStatusPosition());
            
            TbEmployee oldEmp = em.find(TbEmployee.class, oldODD.getUserID().getEmployeeID());
            oldEmp.getTbOperatorDoctorDetailCollection().remove(oldODD);
            
            oldODD.setCreateDate(tbODD.getCreateDate());

            /* update info */
            TbSurgeryRecord newSR = em.find(TbSurgeryRecord.class, txtSurgeryRecordID);
            oldODD.setSurgeryRecordID(newSR);
            
            TbEmployee newDoctor = em.find(TbEmployee.class, txtDoctorID);
            oldODD.setDoctorID(newDoctor);
            
            TbPositionDoctor newPD = em.find(TbPositionDoctor.class, txtPositionID);
            oldODD.setPositionID(newPD);
            
            TbEmployee newEmp = em.find(TbEmployee.class, emID);
            oldODD.setUserID(newEmp);

            
            newSR.getTbOperatorDoctorDetailCollection().add(tbODD);
            newDoctor.getTbOperatorDoctorDetailCollection().add(tbODD);
            newPD.getTbOperatorDoctorDetailCollection().add(tbODD);
            newEmp.getTbOperatorDoctorDetailCollection().add(tbODD);
            
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /*
     public List<TbPositionConsulation> searchLikeNamePositionConsultation(String txtSearch) {
     List<TbPositionConsulation> result = new ArrayList<>();
     try {
     String ejbSQL = "SELECT t FROM TbPositionConsulation t WHERE t.positionName like :positionName";
     Query query = em.createQuery(ejbSQL);
     query.setParameter("positionName", "%" + txtSearch + "%");
     List<TbPositionConsulation> listPro = query.getResultList();
     return listPro;
     } catch (Exception e) {
     e.printStackTrace();
     }
     return result;
     }
     */
    public boolean checkExistWhenInsert(String txtSurgeryRecordID, String txtDoctorID) {
        boolean result = false;
        try {
            TbSurgeryRecord tbSR = em.find(TbSurgeryRecord.class, txtSurgeryRecordID);
            List<TbOperatorDoctorDetail> listtbCDD = (List<TbOperatorDoctorDetail>) tbSR.getTbOperatorDoctorDetailCollection();
            if (listtbCDD.size() > 0) {
                for (TbOperatorDoctorDetail tbDDetail : listtbCDD) {
                    if (tbDDetail.getDoctorID().getEmployeeID().equals(txtDoctorID)) {
                        return true;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public boolean checkExistWhenUpdate(String txtSurgeryRecordID, String txtOperatorDoctorDetailID, String txtDoctorID) {
        boolean result = false;
        try {

            TbSurgeryRecord tbSR = em.find(TbSurgeryRecord.class, txtSurgeryRecordID);
            List<TbOperatorDoctorDetail> listtbCDD = (List<TbOperatorDoctorDetail>) tbSR.getTbOperatorDoctorDetailCollection();
            if (listtbCDD.size() > 0) {
                for (TbOperatorDoctorDetail tbDDetail : listtbCDD) {
                    if (!tbDDetail.getOperatorDoctorDetailID().equals(txtOperatorDoctorDetailID)) {
                        if (tbDDetail.getDoctorID().getEmployeeID().equals(txtDoctorID)) {
                            return true;
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
