/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dung.vns.ssbeans;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import project.ett.TbEmployee;
import project.ett.TbMedicalRecord;
import project.ett.TbSuggestSurgery;
import project.ett.TbSurgeryService;

/**
 *
 * @author Sony
 */
@Stateless
@LocalBean
public class dungSuggestSurgerySessionBean {

    @PersistenceContext(unitName = "eHos1-ejbPU")
    private EntityManager em;

    public void persist(Object object) {
        em.persist(object);
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    public List<TbEmployee> getAllEmployee() {
        try {
            String ejbSQL = "SELECT t FROM TbEmployee t";
            Query query = em.createQuery(ejbSQL);
            List<TbEmployee> listPro = query.getResultList();
            return listPro;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    public List<TbEmployee> getAllDoctor() {
        try {
            String ejbSQL = "SELECT t FROM TbEmployee t WHERE t.position = :position";
            Query query = em.createQuery(ejbSQL);
            query.setParameter("position", "doctor");
            List<TbEmployee> listPro = query.getResultList();
            return listPro;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    public List<TbEmployee> getAllDoctorNurse() {
        try {
            String ejbSQL = "SELECT t FROM TbEmployee t WHERE t.position = :position or t.position = :position1";
            Query query = em.createQuery(ejbSQL);
            query.setParameter("position", "doctor");
            query.setParameter("position1", "nurse");
            List<TbEmployee> listPro = query.getResultList();
            return listPro;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    public List<TbSuggestSurgery> getAllSuggestSurgery(String medicalID) {
        try {
            TbMedicalRecord tbMR = em.find(TbMedicalRecord.class, medicalID);
            return (List<TbSuggestSurgery>) tbMR.getTbSuggestSurgeryCollection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    /* Find By PK Level Surgery */
    public TbSuggestSurgery findByPK(String id) {
        return em.find(TbSuggestSurgery.class, id);
    }
    /* Insert Level Surgery */
    public boolean insertSuggestSurgery(TbSuggestSurgery tbLV, String medicalID, String serviceID, String docID, String emID) {
        try {

            TbMedicalRecord tbMR = em.find(TbMedicalRecord.class, medicalID);
            tbLV.setMedicalRecordID(tbMR);
            tbMR.getTbSuggestSurgeryCollection().add(tbLV);

            TbSurgeryService tbSS = em.find(TbSurgeryService.class, serviceID);
            tbLV.setSurgeryServiceID(tbSS);
            tbSS.getTbSuggestSurgeryCollection().add(tbLV);

            TbEmployee tbED = em.find(TbEmployee.class, docID);
            tbLV.setDoctorID(tbED);
            tbED.getTbSuggestSurgeryCollection().add(tbLV);

            TbEmployee tbE = em.find(TbEmployee.class, emID);
            tbLV.setUserID(tbE);
            tbE.getTbSuggestSurgeryCollection().add(tbLV);

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
    /* Update OrderDetail Method - Main - Quantity, Price */
    public boolean updateSuggestSurgery(TbSuggestSurgery editLV, String medicalID, String serviceID, String docID, String emID) {
        try {
            /* Update Properties */
            TbSuggestSurgery eLevel = em.find(TbSuggestSurgery.class, editLV.getSuggestSurgeryID());

            /* Remove collection old */
            TbMedicalRecord oldMR = em.find(TbMedicalRecord.class, eLevel.getMedicalRecordID().getMedRecordID());
            oldMR.getTbSuggestSurgeryCollection().remove(eLevel);

            TbSurgeryService oldSS = em.find(TbSurgeryService.class, eLevel.getSurgeryServiceID().getSurgeryServiceID());
            oldSS.getTbSuggestSurgeryCollection().remove(eLevel);

            TbEmployee oldDoc = em.find(TbEmployee.class, eLevel.getUserID().getEmployeeID());
            oldDoc.getTbSuggestSurgeryCollection().remove(eLevel);

            TbEmployee oldEm = em.find(TbEmployee.class, eLevel.getUserID().getEmployeeID());
            oldEm.getTbSuggestSurgeryCollection().remove(eLevel);

            /* Add collection new */
            TbMedicalRecord newMR = em.find(TbMedicalRecord.class, medicalID);
            eLevel.setMedicalRecordID(newMR);

            TbSurgeryService newSS = em.find(TbSurgeryService.class, serviceID);
            eLevel.setSurgeryServiceID(newSS);

            eLevel.setSuggestDescription(editLV.getSuggestDescription());

            TbEmployee newDoc = em.find(TbEmployee.class, docID);
            eLevel.setDoctorID(newDoc);

            eLevel.setStatusSuggest(editLV.getStatusSuggest());

            TbEmployee newEm = em.find(TbEmployee.class, emID);
            eLevel.setUserID(newEm);

            newMR.getTbSuggestSurgeryCollection().add(eLevel);
            newSS.getTbSuggestSurgeryCollection().add(eLevel);
            newDoc.getTbSuggestSurgeryCollection().add(eLevel);
            newEm.getTbSuggestSurgeryCollection().add(eLevel);

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public List<TbSuggestSurgery> searchLikeNameSuggestSurgery(String medicalID, String txtSearch) {
        List<TbSuggestSurgery> result = new ArrayList<>();
        try {

            TbMedicalRecord tbMR = em.find(TbMedicalRecord.class, medicalID);
            result = (List<TbSuggestSurgery>) tbMR.getTbSuggestSurgeryCollection();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public boolean checkExistWhenInsert(String medicalID, String serviceID) {
        boolean result = false;
        try {
            TbMedicalRecord tbMR = em.find(TbMedicalRecord.class, medicalID);
            List<TbSuggestSurgery> listresult = (List<TbSuggestSurgery>) tbMR.getTbSuggestSurgeryCollection();
            if (listresult.size() > 0) {
                for (TbSuggestSurgery tbSuggestSurgery : listresult) {
                    if (tbSuggestSurgery.getSurgeryServiceID().getSurgeryServiceID().equals(serviceID)) {
                        return true;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public boolean checkExistWhenUpdate(String suggestID, String medicalID, String serviceID) {
        boolean result = false;
        try {
            TbMedicalRecord tbMR = em.find(TbMedicalRecord.class, medicalID);
            List<TbSuggestSurgery> listresult = (List<TbSuggestSurgery>) tbMR.getTbSuggestSurgeryCollection();
            if (listresult.size() > 0) {
                for (TbSuggestSurgery tbSuggestSurgery : listresult) {
                    if (!tbSuggestSurgery.getSuggestSurgeryID().equals(suggestID)) {
                        if (tbSuggestSurgery.getSurgeryServiceID().getSurgeryServiceID().equals(serviceID)) {
                            return true;
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

}
