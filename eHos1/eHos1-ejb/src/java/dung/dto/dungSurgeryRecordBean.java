/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dung.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import project.ett.TbDrugsDetail;
import project.ett.TbOperatorDoctorDetail;
import project.ett.TbSuppliesDetail;
import project.ett.TbSurgeryRecord;

/**
 *
 * @author Sony
 */
public class dungSurgeryRecordBean implements Serializable {

    private TbSurgeryRecord tbSRecord;
    private List<TbDrugsDetail> listtbDrugs;
    private List<TbSuppliesDetail> listtbSupplies;
    private List<TbOperatorDoctorDetail> listtbOperator;

    public dungSurgeryRecordBean() {
        tbSRecord = new TbSurgeryRecord();
        listtbDrugs = new ArrayList<>();
        listtbSupplies = new ArrayList<>();
        listtbOperator = new ArrayList<>();
    }

    public TbSurgeryRecord getTbSRecord() {
        return tbSRecord;
    }

    public void setTbSRecord(TbSurgeryRecord tbSRecord) {
        this.tbSRecord = tbSRecord;
    }

    public List<TbDrugsDetail> getListtbDrugs() {
        return listtbDrugs;
    }

    public void setListtbDrugs(List<TbDrugsDetail> listtbDrugs) {
        this.listtbDrugs = listtbDrugs;
    }

    public List<TbSuppliesDetail> getListtbSupplies() {
        return listtbSupplies;
    }

    public void setListtbSupplies(List<TbSuppliesDetail> listtbSupplies) {
        this.listtbSupplies = listtbSupplies;
    }

    public List<TbOperatorDoctorDetail> getListtbOperator() {
        return listtbOperator;
    }

    public void setListtbOperator(List<TbOperatorDoctorDetail> listtbOperator) {
        this.listtbOperator = listtbOperator;
    }

    /* Create Methods */
    public boolean addDrugsDetail(TbDrugsDetail dto) {
        boolean result = false;
        try {
            listtbDrugs.add(dto);
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public boolean removeDrugsDetail(String id) {
        boolean result = false;
        try {
            for (Iterator<TbDrugsDetail> iterator = listtbDrugs.iterator(); iterator.hasNext();) {
                TbDrugsDetail value = iterator.next();
                if (value.getDrugsDetailID().equals(id)) {
                    iterator.remove();
                    result = true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public boolean updateQuantityDrugsDetail(String id, int quan) {
        boolean result = false;
        try {

            for (TbDrugsDetail item : listtbDrugs) {
                if (item.getDrugsDetailID().equals(id)) {
                    item.setQuantity(quan);
                    result = true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public boolean addSuppliesDetail(TbSuppliesDetail dto) {
        boolean result = false;
        try {
            listtbSupplies.add(dto);
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public boolean removeSuppliesDetail(String id) {
        boolean result = false;
        try {
            for (Iterator<TbSuppliesDetail> iterator = listtbSupplies.iterator(); iterator.hasNext();) {
                TbSuppliesDetail value = iterator.next();
                if (value.getSuppliesDetailID().equals(id)) {
                    iterator.remove();
                    result = true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
    
    public boolean updateQuantitySuppliesDetail(String id, int quan) {
        boolean result = false;
        try {

            for (TbSuppliesDetail item : listtbSupplies) {
                if (item.getSuppliesDetailID().equals(id)) {
                    item.setQuantity(quan);
                    result = true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public boolean addOperatorDoctorDetail(TbOperatorDoctorDetail dto) {
        boolean result = false;
        try {
            listtbOperator.add(dto);
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public boolean removeOperatorDoctoDetail(String id) {
        boolean result = false;
        try {
            for (Iterator<TbOperatorDoctorDetail> iterator = listtbOperator.iterator(); iterator.hasNext();) {
                TbOperatorDoctorDetail value = iterator.next();
                if (value.getOperatorDoctorDetailID().equals(id)) {
                    iterator.remove();
                    result = true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    
}
