/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dung.dto;

import java.io.Serializable;

/**
 *
 * @author Sony
 */
public class dungSurgeryRequireInfo implements Serializable{
    private String medicalrecordID;
    private String patientname;
    private String sexpatient;
    private String facultyname;
    private String bedname;

    public dungSurgeryRequireInfo() {
    }

    public String getMedicalrecordID() {
        return medicalrecordID;
    }

    public void setMedicalrecordID(String medicalrecordID) {
        this.medicalrecordID = medicalrecordID;
    }

    public String getPatientname() {
        return patientname;
    }

    public void setPatientname(String patientname) {
        this.patientname = patientname;
    }

    public String getSexpatient() {
        return sexpatient;
    }

    public void setSexpatient(String sexpatient) {
        this.sexpatient = sexpatient;
    }

    public String getFacultyname() {
        return facultyname;
    }

    public void setFacultyname(String facultyname) {
        this.facultyname = facultyname;
    }

    public String getBedname() {
        return bedname;
    }

    public void setBedname(String bedname) {
        this.bedname = bedname;
    }
    
    
    
}
