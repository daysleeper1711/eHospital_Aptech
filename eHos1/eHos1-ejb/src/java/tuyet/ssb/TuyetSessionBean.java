/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tuyet.ssb;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Set;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import project.ett.TbDepositPayment;
import project.ett.TbEmployee;
import project.ett.TbFaculty;
import project.ett.TbGKB;
import project.ett.TbMedicalRecord;
import project.ett.TbOutPatient;
import project.ett.TbPatient;
import project.ett.TbService;
import project.ett.TbServiceGroup;
import project.ett.TbServicePrice;
import project.ett.TbStandardValue;
import project.ett.TbTestReqDetail;
import project.ett.TbTestRequest;
import project.ett.TbTestResult;
import project.ett.TbTestResultDetail;
import tuyet.dto.TuyetCartServiceDTO;
import tuyet.dto.TuyetMedicalRecordDTO;
import tuyet.dto.TuyetRequestDTO;
import tuyet.dto.TuyetServiceDTO;

/**
 *
 * @author Hoang Tuyet
 */
@Stateless
@LocalBean
public class TuyetSessionBean {
    
    @PersistenceContext(unitName = "eHos1-ejbPU")
    private EntityManager em;
    
    public void persist(Object object) {
        em.persist(object);
    }

    //Method search test by Medical record (test request management page)
    public TuyetMedicalRecordDTO findbyMRID(String MR_ID) {
        try {
            String ejbQL = "SELECT t FROM TbMedicalRecord t WHERE t.medRecordID = :mrID";
            Query query = em.createQuery(ejbQL);
            query.setParameter("mrID", MR_ID);
            TbMedicalRecord mr = (TbMedicalRecord) query.getSingleResult();
            if (mr != null) {
                //create new TuyetMedicalRecordDTO object
                TuyetMedicalRecordDTO MR_DTO = new TuyetMedicalRecordDTO();
                //fill data for TuyetMedicalRecordDTO object
                MR_DTO.setTbMR(mr);
                MR_DTO.setTbPat(mr.getPatID());
                List<TbTestRequest> listRequest = (List<TbTestRequest>) mr.getTbTestRequestCollection();
                //Create list Request of MR
                List<TuyetRequestDTO> listReqDTO = new ArrayList<TuyetRequestDTO>();
                for (TbTestRequest request : listRequest) {
                    TuyetRequestDTO reqDTO = new TuyetRequestDTO();
                    reqDTO.setRequest(request);
                    List<TbTestReqDetail> listReqDetail = (List<TbTestReqDetail>) request.getTbTestReqDetailCollection();
                    reqDTO.setReqDetails(listReqDetail);
                    List<TbTestResult> listResult = (List<TbTestResult>) request.getTbTestResultCollection();
                    reqDTO.setResult(listResult);
                    listReqDTO.add(reqDTO);
                }
                MR_DTO.setListRequestDTO(listReqDTO);
                return MR_DTO;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

//    public TbPatient findbyPatID(String id) {
//        TbPatient patient = new TbPatient();
//        try {
//            String ejbQL = "SELECT t FROM TbPatient t WHERE t.patID = :patID";
//            Query query = em.createQuery(ejbQL);
//            query.setParameter("patID", id);
//            patient = (TbPatient) query.getSingleResult();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return patient;
//    }
    //check deposit
    public boolean checkDeposit(double deposit, double payment) {
        try {
            if (payment <= deposit) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    //get deposit
    public double getDeposit(String mrID) {
        try {
            TbMedicalRecord mr = em.createNamedQuery("TbMedicalRecord.findByMedRecordID", TbMedicalRecord.class)
                    .setParameter("medRecordID", mrID)
                    .getSingleResult();
            List<TbDepositPayment> listDepo = (List<TbDepositPayment>) mr.getTbDepositPaymentCollection();
            for (TbDepositPayment dp : listDepo) {
                if (dp.getStatus().equals("active")) {
                    return dp.getRemaining();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    //identity TestResquestID 
    public String identityTestID() {
        String newID = "";
        try {
            Date now = new Date();
            String fnow = new SimpleDateFormat("ddMMyyyy").format(now);
            String ql = "SELECT MAX(t.createDate) FROM TbTestRequest t";
            Query qr = em.createQuery(ql);
            Date ngaythang = (Date) qr.getSingleResult();
            if (ngaythang != null) {
                String ejbQL = "SELECT t FROM TbTestRequest t WHERE t.createDate = :createDate";
                Query query = em.createQuery(ejbQL);
                query.setParameter("createDate", ngaythang);
                Object obj = query.getSingleResult();
                
                if (obj != null) {
                    if ((ngaythang.getDate() == now.getDate()) && (ngaythang.getMonth() == now.getMonth()) && (ngaythang.getYear() == now.getYear())) {
                        TbTestRequest test = (TbTestRequest) obj;
                        String testID = test.getTestReqID();
                        int a = Integer.parseInt(testID.substring(14, testID.length()));
                        int b = a + 1;
                        
                        newID = "Test-" + fnow + "-" + b;
                    } else {
                        newID = "Test-" + fnow + "-" + 1;
                    }
                }
            } else {
                newID = "Test-" + fnow + "-" + 1;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return newID;
    }

    //Insert tbTest Request
    public boolean insertTestRequest(TbTestRequest test, String mr_ID, String outPatID, String frFacID, String toFacID, String createEmpID) {
        boolean check = false;
        try {
            if (!mr_ID.equals("")) {
                //set value fo test
                TbMedicalRecord tbMR = em.find(TbMedicalRecord.class, mr_ID);
                test.setMrId(tbMR);
                TbFaculty frfac = em.find(TbFaculty.class, frFacID);
                test.setFromFac(frfac);
                TbFaculty tofac = em.find(TbFaculty.class, toFacID);
                test.setToFac(tofac);
                TbEmployee appDoc = em.find(TbEmployee.class, createEmpID);
                test.setAppointedDoc(appDoc);
                //add test on tbMedicalrecord and persist to database
                tbMR.getTbTestRequestCollection().add(test);
                frfac.getTbTestRequestCollection1().add(test);
                tofac.getTbTestRequestCollection().add(test);
                appDoc.getTbTestRequestCollection1().add(test);
                check = true;
            } else {
                TbOutPatient op = em.find(TbOutPatient.class, outPatID);
                test.setOutPatID(op);
                TbFaculty frfac = em.find(TbFaculty.class, frFacID);
                test.setFromFac(frfac);
                TbFaculty tofac = em.find(TbFaculty.class, toFacID);
                test.setToFac(tofac);
                TbEmployee appDoc = em.find(TbEmployee.class, createEmpID);
                test.setAppointedDoc(appDoc);
                op.getTbTestRequestCollection().add(test);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return check;
    }

    //Search information of Test request by Test request ID
    public TuyetRequestDTO findByTestID(String id) {
        try {
            TuyetRequestDTO dto = new TuyetRequestDTO();
            TbTestRequest test = em.createNamedQuery("TbTestRequest.findByTestReqID", TbTestRequest.class)
                    .setParameter("testReqID", id)
                    .getSingleResult();
            List<TbTestReqDetail> listDetail = (List<TbTestReqDetail>) test.getTbTestReqDetailCollection();
            List<TbTestResult> listResult = (List<TbTestResult>) test.getTbTestResultCollection();
            
            dto.setRequest(test);
            dto.setReqDetails(listDetail);
            dto.setResult(listResult);
            return dto;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    // delete test request
    public boolean deleteTestRequest(String id) {
        try {
            TbTestRequest test = em.createNamedQuery("TbTestRequest.findByTestReqID", TbTestRequest.class)
                    .setParameter("testReqID", id)
                    .getSingleResult();

            //--delete test in tbOutPatient
//            TbOutPatient outPat = test.getOutPatID();
//            outPat.getTbTestRequestCollection().remove(test);
            //--delete test in tbMR
            TbMedicalRecord mr = test.getMrId();
            mr.getTbTestRequestCollection().remove(test);
            //--delete test in tbFaculty (ToFac)
            TbFaculty toFac = test.getToFac();
            toFac.getTbTestRequestCollection1().remove(test);
            //--delete test in tbFaculty (FromFac)
            TbFaculty frFac = test.getFromFac();
            frFac.getTbTestRequestCollection1().remove(test);
            //--delete test in tbEmployee (UpdateEmp)
            TbEmployee updateEmp = test.getUpdateEmp();
            if (updateEmp != null) {
                updateEmp.getTbTestRequestCollection().remove(test);
            }
            //--delete test in tbEmployee (appointed doctor)
            TbEmployee appDoc = test.getAppointedDoc();
            appDoc.getTbTestRequestCollection1().remove(test);
            
            em.remove(test);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    //update status test request
    public void updateTestStatus(String testID, String status) {
        try {
            TbTestRequest req = em.find(TbTestRequest.class, testID);
            req.setStatus(status);
            persist(req);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //update diagnosis Test Request
    public boolean updateTestRequest(String testID, String diagnosis, String empUp) {
        try {
            TbTestRequest req = em.find(TbTestRequest.class, testID);
            req.setDiagnosis(diagnosis);
            Date now = new Date();
            req.setUpdateDate(now);
            TbEmployee emp = em.find(TbEmployee.class, empUp);
            req.setUpdateEmp(emp);
            emp.getTbTestRequestCollection().add(req);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    //insert tbTest request Detail
    public void insertReqDetail(String reqID, int serviceID, double price, String creator) {
        try {
            TbTestRequest test = em.find(TbTestRequest.class, reqID);
            TbService service = em.find(TbService.class, serviceID);
            TbEmployee createEmp = em.find(TbEmployee.class, creator);
            Date createDate = new Date();
            
            TbTestReqDetail detail = new TbTestReqDetail();
            detail.setTestReqID(test);
            detail.setServiceID(service);
            detail.setRealPrice(price);
            detail.setCreateDate(createDate);
            detail.setCreator(createEmp);
            
            test.getTbTestReqDetailCollection().add(detail);
            service.getTbTestReqDetailCollection().add(detail);
            createEmp.getTbTestReqDetailCollection1().add(detail);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //delete test request detail
    public boolean deleteTestDetail(int id) {
        try {
            TbTestReqDetail reqDetail = em.find(TbTestReqDetail.class, id);
            //--remove from tbTestRequest
            TbTestRequest req = reqDetail.getTestReqID();
            req.getTbTestReqDetailCollection().remove(reqDetail);
            //--remove from TbService
            TbService service = reqDetail.getServiceID();
            service.getTbTestReqDetailCollection().remove(reqDetail);
            //--remove from tbEmployee (creator)
            TbEmployee creator = reqDetail.getCreator();
            creator.getTbTestReqDetailCollection1().remove(reqDetail);
            //--remove from tbEmployee (updateEmp)

//            TbEmployee updateEmp = reqDetail.getUpdateEmp();
//            if(updateEmp != null) {
//                updateEmp.getTbTestReqDetailCollection().remove(reqDetail);
//            }
            em.remove(reqDetail);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    //Search faculty by Name
    public String findbyFacName(String name) {
        String facID = "";
        try {
            TbFaculty fa = em.createNamedQuery("TbFaculty.findByFacultyName", TbFaculty.class)
                    .setParameter("facultyName", name)
                    .getSingleResult();
            facID = fa.getFaID();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return facID;
    }

    //check old cart and new cart to update test request detail
    //--compare new to old cart
    public boolean compareNewtoOld(int id, List<TbTestReqDetail> list) {
        try {
            for (TbTestReqDetail item : list) {
                if (item.getServiceID().getServiceID() == id) {
                    return true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    //--compare old to newcart
    public boolean compareOldToNew(int id, List<TuyetCartServiceDTO> list) {
        try {
            for (TuyetCartServiceDTO item : list) {
                if (item.getServiceID() == id) {
                    return true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    //methods handle Group Service
    //--get all group
    public List<TbServiceGroup> getAllGroup() {
        try {
            List<TbServiceGroup> list = em.createNamedQuery("TbServiceGroup.findAll", TbServiceGroup.class).getResultList();
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //--insert group
    public boolean insertGroup(String creatorID, TbServiceGroup newGroup) {
        try {
            TbEmployee creator = em.find(TbEmployee.class, creatorID);
            newGroup.setCreator(creator);
            creator.getTbServiceGroupCollection1().add(newGroup);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    //--update group
    public boolean updateGroup(String updateEmpID, TbServiceGroup group) {
        try {
            TbServiceGroup updateGroup = em.find(TbServiceGroup.class, group.getGroupID());
            updateGroup.setGroupName(group.getGroupName());
            updateGroup.setDescription(group.getDescription());
            updateGroup.setUpdateDate(group.getUpdateDate());
            updateGroup.setGroupType(group.getGroupType());
            
            TbEmployee upEmp = em.find(TbEmployee.class, updateEmpID);
            updateGroup.setUpdateEmp(upEmp);
            upEmp.getTbServiceGroupCollection().add(updateGroup);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    //update group status
    public boolean updateGroupStatus(int groupID, String status, String updateEmpID) {
        try {
            TbServiceGroup group = em.find(TbServiceGroup.class, groupID);
            group.setUpdateDate(new Date());
            TbEmployee emp = em.find(TbEmployee.class, updateEmpID);
            group.setUpdateEmp(emp);
            if (status.equals("1") || status.equals("Enable")) {
                group.setStatus(Boolean.TRUE);
            } else {
                group.setStatus(Boolean.FALSE);
                //disable all service of group
                List<TbService> listService = (List<TbService>) group.getTbServiceCollection();
                for (TbService sv : listService) {
                    updateServiceStatus(sv.getServiceID(), updateEmpID, false);
                }
            }
            emp.getTbServiceGroupCollection().add(group);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    //--find group by ID
    public TbServiceGroup findGroupbyID(int id) {
        try {
            TbServiceGroup group = em.createNamedQuery("TbServiceGroup.findByGroupID", TbServiceGroup.class)
                    .setParameter("groupID", id).getSingleResult();
            return group;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //--search group
    public List<TbServiceGroup> searchGroup(String name, String status, String type) {
        try {
            String ejbQL = "SELECT t FROM TbServiceGroup t";
            Query query = em.createQuery(ejbQL);
            List<TbServiceGroup> listgroup = query.getResultList();
            int a = 0;
            int b = 0;
            int c = 0;
            if (listgroup != null) {
                //search by name
                if (!name.isEmpty()) {
                    List<TbServiceGroup> listTemp = new ArrayList<TbServiceGroup>();
                    for (TbServiceGroup item : listgroup) {
                        if (item.getGroupName().toLowerCase().contains(name)) {
                            listTemp.add(item);
                        }
                    }
                    if (!listTemp.isEmpty()) {
                        listgroup = listTemp;
                    } else {
                        a = 1;
                    }
                }
                //search by type
                if (!type.isEmpty()) {
                    List<TbServiceGroup> listTemp = new ArrayList<TbServiceGroup>();
                    for (TbServiceGroup item : listgroup) {
                        if (item.getGroupType().trim().equals(type)) {
                            listTemp.add(item);
                        }
                    }
                    if (!listTemp.isEmpty()) {
                        listgroup = listTemp;
                    } else {
                        b = 1;
                    }
                }
                //search by status
                if (!status.isEmpty()) {
                    List<TbServiceGroup> listTemp = new ArrayList<TbServiceGroup>();
                    for (TbServiceGroup item : listgroup) {
                        String stt = "0";
                        if (item.getStatus()) {
                            stt = "1";
                        }
                        if (stt.equals(status)) {
                            listTemp.add(item);
                        }
                    }
                    if (!listTemp.isEmpty()) {
                        listgroup = listTemp;
                    } else {
                        c = 1;
                    }
                }
                //information search invalid
                if ((a != 0) || (b != 0) || (c != 0)) {
                    return null;
                }
                return listgroup;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //methods handel service
    //--method getAll service
    public List<TbService> getAll() {
        try {
            String ejbQL = "SELECT t FROM TbService t";
            Query query = em.createQuery(ejbQL);
            List<TbService> listService = query.getResultList();
            return listService;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //--Method search service (search service page & add service to test page)
    public List<TuyetServiceDTO> searchService(String serviceName, String groupName, String status) {
        try {
            String ejbQL = "SELECT t FROM TbService t";
            Query query = em.createQuery(ejbQL);
            List<TbService> listService = query.getResultList();
            int a = 0;
            int b = 0;
            int c = 0;
            if (listService != null) {
                //search by name
                if (!serviceName.isEmpty()) {
                    List<TbService> listTemp = new ArrayList<TbService>();
                    for (TbService item : listService) {
                        if (item.getServiceName().toLowerCase().contains(serviceName)) {
                            listTemp.add(item);
                        }
                    }
                    if (!listTemp.isEmpty()) {
                        listService = listTemp;
                    } else {
                        a = 1;
                    }
                }
                //search by group
                if (!groupName.isEmpty()) {
                    List<TbService> listTemp = new ArrayList<TbService>();
                    for (TbService item : listService) {
                        if (item.getGroupID().getGroupName().equals(groupName)) {
                            listTemp.add(item);
                        }
                    }
                    if (!listTemp.isEmpty()) {
                        listService = listTemp;
                    } else {
                        b = 1;
                    }
                }

                //search by status
                if (!status.isEmpty()) {
                    List<TbService> listTemp = new ArrayList<TbService>();
                    for (TbService item : listService) {
                        String stt = "0";
                        if (item.getStatus()) {
                            stt = "1";
                        }
                        if (stt.equals(status)) {
                            listTemp.add(item);
                        }
                    }
                    if (!listTemp.isEmpty()) {
                        listService = listTemp;
                    } else {
                        c = 1;
                    }
                }
                
                List<TuyetServiceDTO> listServiceDTO = new ArrayList<TuyetServiceDTO>();
                for (TbService item : listService) {
                    TuyetServiceDTO dto = new TuyetServiceDTO();
                    dto.setService(item);
                    List<TbServicePrice> listPrice = (List<TbServicePrice>) item.getTbServicePriceCollection();
                    dto.setListPrice(listPrice);
                    List<TbStandardValue> listStandar = (List<TbStandardValue>) item.getTbStandardValueCollection();
                    dto.setListStandarValue(listStandar);
                    
                    listServiceDTO.add(dto);
                }
                if ((a != 0) || (b != 0) || (c != 0)) {
                    return null;
                }
                return listServiceDTO;
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //--find service by ID
    public TuyetServiceDTO findServiceByID(int id) {
        try {
            String ejbQL = "SELECT t FROM TbService t WHERE t.serviceID = :serviceID";
            Query query = em.createQuery(ejbQL);
            query.setParameter("serviceID", id);
            TbService rs = (TbService) query.getSingleResult();
            if (rs != null) {
                TuyetServiceDTO dto = new TuyetServiceDTO();
                dto.setService(rs);
                List<TbServicePrice> listPrice = (List<TbServicePrice>) rs.getTbServicePriceCollection();
                dto.setListPrice(listPrice);
                List<TbStandardValue> listSV = (List<TbStandardValue>) rs.getTbStandardValueCollection();
                dto.setListStandarValue(listSV);
                return dto;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //--insert Service
    public boolean insertService(int groupID, String creatorID, TbService service) {
        boolean check = false;
        try {
            TbServiceGroup group = em.find(TbServiceGroup.class, groupID);
            service.setGroupID(group);
            TbEmployee empID = em.find(TbEmployee.class, creatorID);
            service.setCreator(empID);
            group.getTbServiceCollection().add(service);
            empID.getTbServiceCollection1().add(service);
            //code catch error of sql
//            ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
//            Validator validator = factory.getValidator();
//
//            Set<ConstraintViolation<TbService>> constraintViolations = validator.validate(service);

//            if (constraintViolations.size() > 0) {
//                System.out.println("Constraint Violations occurred..");
//                for (ConstraintViolation<TbService> contraints : constraintViolations) {
//                    System.out.println(contraints.getRootBeanClass().getSimpleName()
//                            + "." + contraints.getPropertyPath() + " " + contraints.getMessage());
//                }
//            }
            check = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return check;
    }

    //--update service
    public boolean updateService(int groupID, String updateEmpID, TbService service) {
        try {
            TbService updateService = em.find(TbService.class, service.getServiceID());
            
            updateService.setServiceName(service.getServiceName());
            updateService.setServiceContent(service.getServiceContent());
            updateService.setUnitOfMea(service.getUnitOfMea());
            updateService.setUpdateDate(service.getUpdateDate());
            
            TbServiceGroup group = em.find(TbServiceGroup.class, groupID);
            updateService.setGroupID(group);
            TbEmployee empID = em.find(TbEmployee.class, updateEmpID);
            updateService.setUpdateEmp(empID);
            
            group.getTbServiceCollection().add(updateService);
            empID.getTbServiceCollection().add(updateService);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    //--update status of service
    public boolean updateServiceStatus(int serviceID, String updateEmpID, boolean status) {
        try {
            TbEmployee emp = em.find(TbEmployee.class, updateEmpID);
            //update status for service
            TbService service = em.find(TbService.class, serviceID);
            service.setStatus(status);
            service.setUpdateDate(new Date());
            service.setUpdateEmp(emp);
            emp.getTbServiceCollection().add(service);
            //update status for price
            List<TbServicePrice> listPrice = (List<TbServicePrice>) service.getTbServicePriceCollection();
//            Collections.sort(listPrice, new Comparator<TbServicePrice>() {
//                        @Override
//                        public int compare(TbServicePrice o1, TbServicePrice o2) {
//                            return o1.getCreateDate().compareTo(o2.getCreateDate());
//                        }
//                    });
            //take current price to update status
            TbServicePrice curPrice = listPrice.get(listPrice.size() - 1);
            curPrice.setStatus(status);
            em.persist(curPrice);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    //Methods handle Price
    //--insert price
    public boolean insertPrice(int serviceID, String userID, double newPrice) {
        try {
            System.out.println("insert price");
            TbService sv = em.find(TbService.class, serviceID);
            TbEmployee emp = em.find(TbEmployee.class, userID);
            //update status for old price
            List<TbServicePrice> list = (List<TbServicePrice>) sv.getTbServicePriceCollection();
            
            if (list != null) {
                for (TbServicePrice p : list) {
                    if (p.getStatus()) {
                        p.setStatus(Boolean.FALSE);
                        p.setUpdateDate(new Date());
                        p.setUpdateEmp(emp);
                        emp.getTbServicePriceCollection().add(p);
                    }
                }
            }
            //set value and insert new price
            TbServicePrice price = new TbServicePrice();
            
            price.setPrice(newPrice);
            price.setCreateDate(new Date());
            price.setStatus(Boolean.TRUE);

            //set creator
            price.setCreator(emp);
            emp.getTbServicePriceCollection1().add(price);

            //set service ID
            price.setServiceID(sv);
            sv.getTbServicePriceCollection().add(price);
            return true;
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    //--Get All Price
    public List<TbServicePrice> getAllPrice() {
        try {
            List<TbServicePrice> list = em.createNamedQuery("TbServicePrice.findAll", TbServicePrice.class).getResultList();
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //--Search price by service name, status
    public List<TbServicePrice> searchPrice(String serviceName, String status) {
        try {
            String ejbQL = "SELECT t FROM TbServicePrice t";
            Query query = em.createQuery(ejbQL);
            List<TbServicePrice> listPrice = query.getResultList();
            int a = 0;
            int c = 0;
            if (listPrice != null) {
                //search by name
                if (!serviceName.isEmpty()) {
                    List<TbServicePrice> listTemp = new ArrayList<TbServicePrice>();
                    for (TbServicePrice item : listPrice) {
                        if (item.getServiceID().getServiceName().toLowerCase().contains(serviceName)) {
                            listTemp.add(item);
                        }
                    }
                    if (!listTemp.isEmpty()) {
                        listPrice = listTemp;
                    } else {
                        a = 1;
                    }
                }

                //search by status
                if (!status.isEmpty()) {
                    List<TbServicePrice> listTemp = new ArrayList<TbServicePrice>();
                    for (TbServicePrice item : listPrice) {
                        String stt = "0";
                        if (item.getStatus()) {
                            stt = "1";
                        }
                        if (stt.equals(status)) {
                            listTemp.add(item);
                        }
                    }
                    if (!listTemp.isEmpty()) {
                        listPrice = listTemp;
                    } else {
                        c = 1;
                    }
                }
                
                if ((a != 0) || (c != 0)) {
                    return null;
                }
                return listPrice;
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //Method handle standard value
    public boolean insertSV(int serviceID, String createID, TbStandardValue sv) {
        try {
            TbService service = em.find(TbService.class, serviceID);
            sv.setServiceID(service);
            TbEmployee creator = em.find(TbEmployee.class, createID);
            sv.setCreator(creator);
            
            service.getTbStandardValueCollection().add(sv);
            creator.getTbStandardValueCollection1().add(sv);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    //--update standard value
    public boolean updateStandardValue(String upEmpID, TbStandardValue sv) {
        try {
            TbStandardValue upSV = em.find(TbStandardValue.class, sv.getSvId());
            upSV.setSVfemale(sv.getSVfemale());
            upSV.setSVmale(sv.getSVmale());
            upSV.setUpdateDate(sv.getUpdateDate());
            
            TbEmployee upEmp = em.find(TbEmployee.class, upEmpID);
            upSV.setUpdateEmp(upEmp);
            
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    //methods handle result && result detail
    //--insert result
    public boolean insertResult(String reqID, String creatorID, TbTestResult rs) {
        try {
            TbTestRequest test = em.find(TbTestRequest.class, reqID);
            rs.setTestReqID(test);
            TbEmployee creator = em.find(TbEmployee.class, creatorID);
            rs.setTestDoc(creator);
            
            test.getTbTestResultCollection().add(rs);
            creator.getTbTestResultCollection1().add(rs);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    //--insert result detail
    public boolean insertResultDetail(int rsID, String creatorID, int reqDetailID, TbTestResultDetail rsd) {
        try {
            TbTestResult rs = em.find(TbTestResult.class, rsID);
            rsd.setTestResultID(rs);
            TbEmployee creator = em.find(TbEmployee.class, creatorID);
            rsd.setCreator(creator);
            TbTestReqDetail reqDe = em.find(TbTestReqDetail.class, reqDetailID);
            rsd.setReqDetailID(reqDe);
            
            rs.getTbTestResultDetailCollection().add(rsd);
            creator.getTbTestResultDetailCollection1().add(rsd);
            reqDe.getTbTestResultDetailCollection().add(rsd);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    //--update result
    public boolean updateResult(int rsID, String empID, String conclusion, String note) {
        try {
            System.out.println("****************");
            TbTestResult rs = em.find(TbTestResult.class, rsID);
            rs.setConclusion(conclusion);
            rs.setNote(note);
            rs.setStatus(Boolean.FALSE);//status 'done'
            rs.setUpdateDate(new Date());
            
            TbEmployee updateEmp = em.find(TbEmployee.class, empID);
            rs.setConcludeDoc(updateEmp);
            updateEmp.getTbTestResultCollection().add(rs);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}//end class
