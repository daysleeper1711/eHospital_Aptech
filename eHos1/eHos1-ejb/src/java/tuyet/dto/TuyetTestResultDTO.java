/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tuyet.dto;

import java.io.Serializable;
import java.util.List;
import project.ett.TbTestResult;
import project.ett.TbTestResultDetail;

/**
 *
 * @author Hoang Tuyet
 */
public class TuyetTestResultDTO implements Serializable {
  private TbTestResult testResult;
  private List<TbTestResultDetail> listResultDetail;

    public TuyetTestResultDTO() {
    }

    public TbTestResult getTestResult() {
        return testResult;
    }

    public void setTestResult(TbTestResult testResult) {
        this.testResult = testResult;
    }

    public List<TbTestResultDetail> getListResultDetail() {
        return listResultDetail;
    }

    public void setListResultDetail(List<TbTestResultDetail> listResultDetail) {
        this.listResultDetail = listResultDetail;
    }
  
}//end class
