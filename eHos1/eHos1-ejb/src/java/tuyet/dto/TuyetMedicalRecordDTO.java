/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tuyet.dto;

import java.io.Serializable;
import java.util.List;
import project.ett.TbMedicalRecord;
import project.ett.TbPatient;
import project.ett.TbTestRequest;


/**
 *
 * @author Hoang Tuyet
 */
public class TuyetMedicalRecordDTO implements Serializable {
   private TbMedicalRecord tbMR;
   private TbPatient tbPat;
   //private List<TbTestRequest> listTbRequest;
   private List<TuyetRequestDTO> listRequestDTO;

   

    public TuyetMedicalRecordDTO() {
    }

    public TbPatient getTbPat() {
        return tbPat;
    }

    public void setTbPat(TbPatient tbPat) {
        this.tbPat = tbPat;
    }

    public TbMedicalRecord getTbMR() {
        return tbMR;
    }

    public void setTbMR(TbMedicalRecord tbMR) {
        this.tbMR = tbMR;
    }
     public List<TuyetRequestDTO> getListRequestDTO() {
        return listRequestDTO;
    }

    public void setListRequestDTO(List<TuyetRequestDTO> listRequestDTO) {
        this.listRequestDTO = listRequestDTO;
    }

//    public List<TbTestRequest> getListTbRequest() {
//        return listTbRequest;
//    }
//
//    public void setListTbRequest(List<TbTestRequest> listTbRequest) {
//        this.listTbRequest = listTbRequest;
//    }
   
}//end class
