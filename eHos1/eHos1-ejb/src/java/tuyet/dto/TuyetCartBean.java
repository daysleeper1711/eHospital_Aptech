/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tuyet.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Hoang Tuyet
 */
public class TuyetCartBean implements Serializable {
    private List<TuyetCartServiceDTO> serviceCart;

    public TuyetCartBean() {
        serviceCart = new ArrayList<TuyetCartServiceDTO>();
    }

    public List<TuyetCartServiceDTO> getServiceCart() {
        return serviceCart;
    }

    public void setServiceCart(List<TuyetCartServiceDTO> serviceCart) {
        this.serviceCart = serviceCart;
    }
    //methods
    public boolean addToCart(TuyetCartServiceDTO dto) {
        boolean check = false;
        try {
            for (TuyetCartServiceDTO item : serviceCart) {
                if (item.getServiceID() == dto.getServiceID()) {
                    return true;
                }
            }
            
            serviceCart.add(dto);
            check = true;
            
        } catch (Exception e) {
            e.printStackTrace();
        }
       return check;
    }
    
    public boolean removeFromCart(int id) {
        boolean check = false;
        try {
            for (TuyetCartServiceDTO item : serviceCart) {
                if (item.getServiceID() == id) {
                    serviceCart.remove(item);
                    return true;
                }
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        return check;
    }
    
    public double totalPayment() {
        double total = 0;
        try {
            for (TuyetCartServiceDTO item : serviceCart) {
                total = total + item.getUnitPrice();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return total;
    }
}//end class
