/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tuyet.dto;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Hoang Tuyet
 */
public class ResultDTO implements Serializable {
    private int reqDetailID;
    private String serviceName, svMale, svfMale;
    private String result, unitOfMea, groupName, cpStandard;

    public ResultDTO() {
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getSvMale() {
        if(svMale != null) {
            return svMale.replace("-", "(-)").replace(":", "-");
        } else {
            return svMale;
        }
    }

    public void setSvMale(String svMale) {
        this.svMale = svMale;
    }

    public String getSvfMale() {
        if(svfMale != null) {
        return svfMale.replace("-", "(-)").replace(":", "-");
        } else {
            return svfMale;
        }
    }

    public void setSvfMale(String svfMale) {
        this.svfMale = svfMale;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getUnitOfMea() {
        return unitOfMea;
    }

    public void setUnitOfMea(String unitOfMea) {
        this.unitOfMea = unitOfMea;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getCpStandard() {
        return cpStandard;
    }

    public void setCpStandard(String cpStandard) {
        this.cpStandard = cpStandard;
    }

    public int getReqDetailID() {
        return reqDetailID;
    }

    public void setReqDetailID(int reqDetailID) {
        this.reqDetailID = reqDetailID;
    }
    
}//end class
