/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tuyet.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import project.ett.TbService;
import project.ett.TbServicePrice;
import project.ett.TbStandardValue;

/**
 *
 * @author Hoang Tuyet
 */
public class TuyetServiceDTO implements Serializable {
    private TbService service;
    private List<TbStandardValue> listStandarValue;
    private List<TbServicePrice> listPrice;
    private double curPrice;
    private Date createDate;
    private String status;
    private String svMale;
    private String svfMale;
    private String serviceName;

    public TuyetServiceDTO() {
    }

    public TbService getService() {
        return service;
    }

    public void setService(TbService service) {
        this.service = service;
    }

    public List<TbStandardValue> getListStandarValue() {
        return listStandarValue;
    }

    public void setListStandarValue(List<TbStandardValue> listStandarValue) {
        this.listStandarValue = listStandarValue;
    }

    public List<TbServicePrice> getListPrice() {
        return listPrice;
    }

    public void setListPrice(List<TbServicePrice> listPrice) {
        this.listPrice = listPrice;
    }

    public double getCurPrice() {
        double p = 0;
        for(TbServicePrice price : listPrice) {
            if(price.getStatus()) {
                p = price.getPrice();
            }
        }
        return p;
    }

    public void setCurPrice(double curPrice) {
        this.curPrice = curPrice;
    }

    public Date getCreateDate() {
        return service.getCreateDate();
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getStatus() {
        String stt = "Disable";
        if(service.getStatus()) {
            stt = "Enable";
        }
        return stt;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSvMale() {
        if(listStandarValue == null || listStandarValue.isEmpty()) {
            return null;
        } else {
            TbStandardValue sv = listStandarValue.get(listStandarValue.size() - 1);
            String male = sv.getSVmale();
            return male;
        }
    }

    public void setSvMale(String svMale) {
        this.svMale = svMale;
    }

    public String getSvfMale() {
        if(listStandarValue == null || listStandarValue.isEmpty()) {
            return null;
        } else {
            TbStandardValue sv = listStandarValue.get(listStandarValue.size() - 1);
            String fmale = sv.getSVfemale();
            return fmale;
        }
    }

    public void setSvfMale(String svfMale) {
        this.svfMale = svfMale;
    }

    public String getServiceName() {
        return this.service.getServiceName();
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }
    
}//end class
