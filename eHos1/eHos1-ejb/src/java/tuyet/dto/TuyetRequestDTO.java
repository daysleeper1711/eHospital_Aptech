/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tuyet.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import project.ett.TbTestReqDetail;
import project.ett.TbTestRequest;
import project.ett.TbTestResult;

/**
 *
 * @author Hoang Tuyet
 */
public class TuyetRequestDTO implements Serializable{
    private TbTestRequest request;
    private List<TbTestReqDetail> reqDetails;
    private List<TbTestResult> result;
    private Date createDate; //create date of request

    public TuyetRequestDTO() {
    }

    public TbTestRequest getRequest() {
        return request;
    }

    public void setRequest(TbTestRequest request) {
        this.request = request;
    }

    public List<TbTestReqDetail> getReqDetails() {
        return reqDetails;
    }

    public void setReqDetails(List<TbTestReqDetail> reqDetails) {
        this.reqDetails = reqDetails;
    }

    public List<TbTestResult> getResult() {
        return result;
    }

    public void setResult(List<TbTestResult> result) {
        this.result = result;
    }

    public Date getCreateDate() {
        return request.getCreateDate();
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
 
}//end class
