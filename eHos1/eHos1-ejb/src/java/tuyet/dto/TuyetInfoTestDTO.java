/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tuyet.dto;

import java.io.Serializable;

/**
 *
 * @author Hoang Tuyet
 */
public class TuyetInfoTestDTO implements Serializable {
    String patType,admissionID, patName, patAddr, patGender, patAge, frFac, toFac;
    String testID, diagnosis, appDoc;
    double oldPayment;
   
    public TuyetInfoTestDTO() {
    }

    public String getPatType() {
        return patType;
    }

    public void setPatType(String patType) {
        this.patType = patType;
    }

    public String getAdmissionID() {
        return admissionID;
    }

    public void setAdmissionID(String admissionID) {
        this.admissionID = admissionID;
    }

    public String getPatName() {
        return patName;
    }

    public void setPatName(String patName) {
        this.patName = patName;
    }

    public String getPatAddr() {
        return patAddr;
    }

    public void setPatAddr(String patAddr) {
        this.patAddr = patAddr;
    }

    public String getPatGender() {
        return patGender;
    }

    public void setPatGender(String patGender) {
        this.patGender = patGender;
    }

    public String getPatAge() {
        return patAge;
    }

    public void setPatAge(String patAge) {
        this.patAge = patAge;
    }

    public String getFrFac() {
        return frFac;
    }

    public void setFrFac(String frFac) {
        this.frFac = frFac;
    }

    public String getToFac() {
        return toFac;
    }

    public void setToFac(String toFac) {
        this.toFac = toFac;
    }

    public String getTestID() {
        return testID;
    }

    public void setTestID(String testID) {
        this.testID = testID;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getAppDoc() {
        return appDoc;
    }

    public void setAppDoc(String appDoc) {
        this.appDoc = appDoc;
    }

    public double getOldPayment() {
        return oldPayment;
    }

    public void setOldPayment(double oldPayment) {
        this.oldPayment = oldPayment;
    }
    
}//end class
