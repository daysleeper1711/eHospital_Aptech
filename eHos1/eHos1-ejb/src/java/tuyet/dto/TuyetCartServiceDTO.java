/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tuyet.dto;

import java.io.Serializable;

/**
 *
 * @author Hoang Tuyet
 */
public class TuyetCartServiceDTO implements Serializable {
    private String testRequestID, admissionID, serviceName, serviceGroup ;
    int serviceID, testReqDetailID;
    double unitPrice;

    public TuyetCartServiceDTO() {
    }

    public String getTestRequestID() {
        return testRequestID;
    }

    public void setTestRequestID(String testRequestID) {
        this.testRequestID = testRequestID;
    }

    public String getAdmissionID() {
        return admissionID;
    }

    public void setAdmissionID(String admissionID) {
        this.admissionID = admissionID;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceGroup() {
        return serviceGroup;
    }

    public void setServiceGroup(String serviceGroup) {
        this.serviceGroup = serviceGroup;
    }

    public int getServiceID() {
        return serviceID;
    }

    public void setServiceID(int serviceID) {
        this.serviceID = serviceID;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public int getTestReqDetailID() {
        return testReqDetailID;
    }

    public void setTestReqDetailID(int testReqDetailID) {
        this.testReqDetailID = testReqDetailID;
    }

 
}//end class
