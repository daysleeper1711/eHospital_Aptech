/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project.ett;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DaySLeePer
 */
@Entity
@Table(name = "tbSurgeryService")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbSurgeryService.findAll", query = "SELECT t FROM TbSurgeryService t"),
    @NamedQuery(name = "TbSurgeryService.findBySurgeryServiceID", query = "SELECT t FROM TbSurgeryService t WHERE t.surgeryServiceID = :surgeryServiceID"),
    @NamedQuery(name = "TbSurgeryService.findBySurgeryServiceName", query = "SELECT t FROM TbSurgeryService t WHERE t.surgeryServiceName = :surgeryServiceName"),
    @NamedQuery(name = "TbSurgeryService.findByServiceDescription", query = "SELECT t FROM TbSurgeryService t WHERE t.serviceDescription = :serviceDescription"),
    @NamedQuery(name = "TbSurgeryService.findByInitialCost", query = "SELECT t FROM TbSurgeryService t WHERE t.initialCost = :initialCost"),
    @NamedQuery(name = "TbSurgeryService.findByStatusService", query = "SELECT t FROM TbSurgeryService t WHERE t.statusService = :statusService"),
    @NamedQuery(name = "TbSurgeryService.findByCreateDate", query = "SELECT t FROM TbSurgeryService t WHERE t.createDate = :createDate")})
public class TbSurgeryService implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "SurgeryServiceID")
    private String surgeryServiceID;
    @Size(max = 100)
    @Column(name = "SurgeryServiceName")
    private String surgeryServiceName;
    @Size(max = 250)
    @Column(name = "ServiceDescription")
    private String serviceDescription;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "InitialCost")
    private Double initialCost;
    @Column(name = "StatusService")
    private Boolean statusService;
    @Column(name = "CreateDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @JoinColumn(name = "LevelSurgery", referencedColumnName = "LevelID")
    @ManyToOne
    private TbLevelSurgery levelSurgery;
    @JoinColumn(name = "FaultyID", referencedColumnName = "Fa_ID")
    @ManyToOne
    private TbFaculty faultyID;
    @JoinColumn(name = "UserID", referencedColumnName = "EmployeeID")
    @ManyToOne
    private TbEmployee userID;
    @JoinColumn(name = "CategoryID", referencedColumnName = "CategoryID")
    @ManyToOne
    private TbCategory categoryID;
    @OneToMany(mappedBy = "surgeryServiceID")
    private Collection<TbSuggestSurgery> tbSuggestSurgeryCollection;

    public TbSurgeryService() {
    }

    public TbSurgeryService(String surgeryServiceID) {
        this.surgeryServiceID = surgeryServiceID;
    }

    public String getSurgeryServiceID() {
        return surgeryServiceID;
    }

    public void setSurgeryServiceID(String surgeryServiceID) {
        this.surgeryServiceID = surgeryServiceID;
    }

    public String getSurgeryServiceName() {
        return surgeryServiceName;
    }

    public void setSurgeryServiceName(String surgeryServiceName) {
        this.surgeryServiceName = surgeryServiceName;
    }

    public String getServiceDescription() {
        return serviceDescription;
    }

    public void setServiceDescription(String serviceDescription) {
        this.serviceDescription = serviceDescription;
    }

    public Double getInitialCost() {
        return initialCost;
    }

    public void setInitialCost(Double initialCost) {
        this.initialCost = initialCost;
    }

    public Boolean getStatusService() {
        return statusService;
    }

    public void setStatusService(Boolean statusService) {
        this.statusService = statusService;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public TbLevelSurgery getLevelSurgery() {
        return levelSurgery;
    }

    public void setLevelSurgery(TbLevelSurgery levelSurgery) {
        this.levelSurgery = levelSurgery;
    }

    public TbFaculty getFaultyID() {
        return faultyID;
    }

    public void setFaultyID(TbFaculty faultyID) {
        this.faultyID = faultyID;
    }

    public TbEmployee getUserID() {
        return userID;
    }

    public void setUserID(TbEmployee userID) {
        this.userID = userID;
    }

    public TbCategory getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(TbCategory categoryID) {
        this.categoryID = categoryID;
    }

    @XmlTransient
    public Collection<TbSuggestSurgery> getTbSuggestSurgeryCollection() {
        return tbSuggestSurgeryCollection;
    }

    public void setTbSuggestSurgeryCollection(Collection<TbSuggestSurgery> tbSuggestSurgeryCollection) {
        this.tbSuggestSurgeryCollection = tbSuggestSurgeryCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (surgeryServiceID != null ? surgeryServiceID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbSurgeryService)) {
            return false;
        }
        TbSurgeryService other = (TbSurgeryService) object;
        if ((this.surgeryServiceID == null && other.surgeryServiceID != null) || (this.surgeryServiceID != null && !this.surgeryServiceID.equals(other.surgeryServiceID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "project.ett.TbSurgeryService[ surgeryServiceID=" + surgeryServiceID + " ]";
    }
    
}
