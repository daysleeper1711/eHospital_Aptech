/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project.ett;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DaySLeePer
 */
@Entity
@Table(name = "tbTestResult")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbTestResult.findAll", query = "SELECT t FROM TbTestResult t"),
    @NamedQuery(name = "TbTestResult.findByTestResultID", query = "SELECT t FROM TbTestResult t WHERE t.testResultID = :testResultID"),
    @NamedQuery(name = "TbTestResult.findByConclusion", query = "SELECT t FROM TbTestResult t WHERE t.conclusion = :conclusion"),
    @NamedQuery(name = "TbTestResult.findByNote", query = "SELECT t FROM TbTestResult t WHERE t.note = :note"),
    @NamedQuery(name = "TbTestResult.findByCreateDate", query = "SELECT t FROM TbTestResult t WHERE t.createDate = :createDate"),
    @NamedQuery(name = "TbTestResult.findByUpdateDate", query = "SELECT t FROM TbTestResult t WHERE t.updateDate = :updateDate"),
    @NamedQuery(name = "TbTestResult.findByStatus", query = "SELECT t FROM TbTestResult t WHERE t.status = :status")})
public class TbTestResult implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "TestResultID")
    private Integer testResultID;
    @Size(max = 100)
    @Column(name = "Conclusion")
    private String conclusion;
    @Size(max = 100)
    @Column(name = "Note")
    private String note;
    @Column(name = "CreateDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "UpdateDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @Column(name = "Status")
    private Boolean status;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "testResultID")
    private Collection<TbTestResultDetail> tbTestResultDetailCollection;
    @JoinColumn(name = "TestReqID", referencedColumnName = "TestReqID")
    @ManyToOne
    private TbTestRequest testReqID;
    @JoinColumn(name = "ConcludeDoc", referencedColumnName = "EmployeeID")
    @ManyToOne
    private TbEmployee concludeDoc;
    @JoinColumn(name = "TestDoc", referencedColumnName = "EmployeeID")
    @ManyToOne
    private TbEmployee testDoc;

    public TbTestResult() {
    }

    public TbTestResult(Integer testResultID) {
        this.testResultID = testResultID;
    }

    public Integer getTestResultID() {
        return testResultID;
    }

    public void setTestResultID(Integer testResultID) {
        this.testResultID = testResultID;
    }

    public String getConclusion() {
        return conclusion;
    }

    public void setConclusion(String conclusion) {
        this.conclusion = conclusion;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    @XmlTransient
    public Collection<TbTestResultDetail> getTbTestResultDetailCollection() {
        return tbTestResultDetailCollection;
    }

    public void setTbTestResultDetailCollection(Collection<TbTestResultDetail> tbTestResultDetailCollection) {
        this.tbTestResultDetailCollection = tbTestResultDetailCollection;
    }

    public TbTestRequest getTestReqID() {
        return testReqID;
    }

    public void setTestReqID(TbTestRequest testReqID) {
        this.testReqID = testReqID;
    }

    public TbEmployee getConcludeDoc() {
        return concludeDoc;
    }

    public void setConcludeDoc(TbEmployee concludeDoc) {
        this.concludeDoc = concludeDoc;
    }

    public TbEmployee getTestDoc() {
        return testDoc;
    }

    public void setTestDoc(TbEmployee testDoc) {
        this.testDoc = testDoc;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (testResultID != null ? testResultID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbTestResult)) {
            return false;
        }
        TbTestResult other = (TbTestResult) object;
        if ((this.testResultID == null && other.testResultID != null) || (this.testResultID != null && !this.testResultID.equals(other.testResultID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "project.ett.TbTestResult[ testResultID=" + testResultID + " ]";
    }
    
}
