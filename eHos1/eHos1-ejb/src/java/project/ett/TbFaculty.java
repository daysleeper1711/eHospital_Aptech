/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project.ett;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DaySLeePer
 */
@Entity
@Table(name = "tbFaculty")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbFaculty.findAll", query = "SELECT t FROM TbFaculty t"),
    @NamedQuery(name = "TbFaculty.findByFaID", query = "SELECT t FROM TbFaculty t WHERE t.faID = :faID"),
    @NamedQuery(name = "TbFaculty.findByFacultyName", query = "SELECT t FROM TbFaculty t WHERE t.facultyName = :facultyName"),
    @NamedQuery(name = "TbFaculty.findByStatus", query = "SELECT t FROM TbFaculty t WHERE t.status = :status")})
public class TbFaculty implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "Fa_ID")
    private String faID;
    @Size(max = 50)
    @Column(name = "FacultyName")
    private String facultyName;
    @Size(max = 30)
    @Column(name = "Status")
    private String status;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "faID")
    private Collection<TbBed> tbBedCollection;
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "facultyID")
    private Collection<TbEmployee> tbEmployeeCollection;
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "facultyFrom")
    private Collection<TbMedicineSupplies> tbMedicineSuppliesCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "faultyID")
    private Collection<TbSurgeryService> tbSurgeryServiceCollection;
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "toFac")
    private Collection<TbTestRequest> tbTestRequestCollection;
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "fromFac")
    private Collection<TbTestRequest> tbTestRequestCollection1;
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "facultyFrom")
    private Collection<TbPrescription> tbPrescriptionCollection;

    public TbFaculty() {
    }

    public TbFaculty(String faID) {
        this.faID = faID;
    }

    public String getFaID() {
        return faID;
    }

    public void setFaID(String faID) {
        this.faID = faID;
    }

    public String getFacultyName() {
        return facultyName;
    }

    public void setFacultyName(String facultyName) {
        this.facultyName = facultyName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @XmlTransient
    public Collection<TbBed> getTbBedCollection() {
        return tbBedCollection;
    }

    public void setTbBedCollection(Collection<TbBed> tbBedCollection) {
        this.tbBedCollection = tbBedCollection;
    }

    @XmlTransient
    public Collection<TbEmployee> getTbEmployeeCollection() {
        return tbEmployeeCollection;
    }

    public void setTbEmployeeCollection(Collection<TbEmployee> tbEmployeeCollection) {
        this.tbEmployeeCollection = tbEmployeeCollection;
    }

    @XmlTransient
    public Collection<TbMedicineSupplies> getTbMedicineSuppliesCollection() {
        return tbMedicineSuppliesCollection;
    }

    public void setTbMedicineSuppliesCollection(Collection<TbMedicineSupplies> tbMedicineSuppliesCollection) {
        this.tbMedicineSuppliesCollection = tbMedicineSuppliesCollection;
    }

    @XmlTransient
    public Collection<TbSurgeryService> getTbSurgeryServiceCollection() {
        return tbSurgeryServiceCollection;
    }

    public void setTbSurgeryServiceCollection(Collection<TbSurgeryService> tbSurgeryServiceCollection) {
        this.tbSurgeryServiceCollection = tbSurgeryServiceCollection;
    }

    @XmlTransient
    public Collection<TbTestRequest> getTbTestRequestCollection() {
        return tbTestRequestCollection;
    }

    public void setTbTestRequestCollection(Collection<TbTestRequest> tbTestRequestCollection) {
        this.tbTestRequestCollection = tbTestRequestCollection;
    }

    @XmlTransient
    public Collection<TbTestRequest> getTbTestRequestCollection1() {
        return tbTestRequestCollection1;
    }

    public void setTbTestRequestCollection1(Collection<TbTestRequest> tbTestRequestCollection1) {
        this.tbTestRequestCollection1 = tbTestRequestCollection1;
    }

    @XmlTransient
    public Collection<TbPrescription> getTbPrescriptionCollection() {
        return tbPrescriptionCollection;
    }

    public void setTbPrescriptionCollection(Collection<TbPrescription> tbPrescriptionCollection) {
        this.tbPrescriptionCollection = tbPrescriptionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (faID != null ? faID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbFaculty)) {
            return false;
        }
        TbFaculty other = (TbFaculty) object;
        if ((this.faID == null && other.faID != null) || (this.faID != null && !this.faID.equals(other.faID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "project.ett.TbFaculty[ faID=" + faID + " ]";
    }
    
}
