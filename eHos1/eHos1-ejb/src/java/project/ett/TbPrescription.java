/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project.ett;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DaySLeePer
 */
@Entity
@Table(name = "tbPrescription")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbPrescription.findAll", query = "SELECT t FROM TbPrescription t"),
    @NamedQuery(name = "TbPrescription.findByPrescriptionID", query = "SELECT t FROM TbPrescription t WHERE t.prescriptionID = :prescriptionID"),
    @NamedQuery(name = "TbPrescription.findByCreateForDate", query = "SELECT t FROM TbPrescription t WHERE t.createForDate = :createForDate"),
    @NamedQuery(name = "TbPrescription.findByDateCreate", query = "SELECT t FROM TbPrescription t WHERE t.dateCreate = :dateCreate"),
    @NamedQuery(name = "TbPrescription.findByTotalPrice", query = "SELECT t FROM TbPrescription t WHERE t.totalPrice = :totalPrice"),
    @NamedQuery(name = "TbPrescription.findByStatusPres", query = "SELECT t FROM TbPrescription t WHERE t.statusPres = :statusPres"),
    @NamedQuery(name = "TbPrescription.findByStatusReport", query = "SELECT t FROM TbPrescription t WHERE t.statusReport = :statusReport"),
    @NamedQuery(name = "TbPrescription.findByUpStatusGived", query = "SELECT t FROM TbPrescription t WHERE t.upStatusGived = :upStatusGived"),
    @NamedQuery(name = "TbPrescription.findByDignose", query = "SELECT t FROM TbPrescription t WHERE t.dignose = :dignose"),
    @NamedQuery(name = "TbPrescription.findByPriceAfterHealthInsurance", query = "SELECT t FROM TbPrescription t WHERE t.priceAfterHealthInsurance = :priceAfterHealthInsurance"),
    @NamedQuery(name = "TbPrescription.findByUpDateTime", query = "SELECT t FROM TbPrescription t WHERE t.upDateTime = :upDateTime")})
public class TbPrescription implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "PrescriptionID")
    private String prescriptionID;
    @Column(name = "CreateForDate")
    @Temporal(TemporalType.DATE)
    private Date createForDate;
    @Column(name = "DateCreate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreate;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "TotalPrice")
    private Double totalPrice;
    @Size(max = 50)
    @Column(name = "StatusPres")
    private String statusPres;
    @Size(max = 50)
    @Column(name = "StatusReport")
    private String statusReport;
    @Column(name = "upStatusGived")
    @Temporal(TemporalType.TIMESTAMP)
    private Date upStatusGived;
    @Size(max = 200)
    @Column(name = "Dignose")
    private String dignose;
    @Column(name = "PriceAfterHealthInsurance")
    private Double priceAfterHealthInsurance;
    @Column(name = "UpDateTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date upDateTime;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "prescriptionID")
    private Collection<TbPrescriptionDetail> tbPrescriptionDetailCollection;
    @JoinColumn(name = "MedicalRecordID", referencedColumnName = "MedRecordID")
    @ManyToOne
    private TbMedicalRecord medicalRecordID;
    @JoinColumn(name = "FacultyFrom", referencedColumnName = "Fa_ID")
    @ManyToOne
    private TbFaculty facultyFrom;
    @JoinColumn(name = "UpEmployeeID", referencedColumnName = "EmployeeID")
    @ManyToOne
    private TbEmployee upEmployeeID;
    @JoinColumn(name = "EmployeeID", referencedColumnName = "EmployeeID")
    @ManyToOne
    private TbEmployee employeeID;

    public TbPrescription() {
    }

    public TbPrescription(String prescriptionID) {
        this.prescriptionID = prescriptionID;
    }

    public String getPrescriptionID() {
        return prescriptionID;
    }

    public void setPrescriptionID(String prescriptionID) {
        this.prescriptionID = prescriptionID;
    }

    public Date getCreateForDate() {
        return createForDate;
    }

    public void setCreateForDate(Date createForDate) {
        this.createForDate = createForDate;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getStatusPres() {
        return statusPres;
    }

    public void setStatusPres(String statusPres) {
        this.statusPres = statusPres;
    }

    public String getStatusReport() {
        return statusReport;
    }

    public void setStatusReport(String statusReport) {
        this.statusReport = statusReport;
    }

    public Date getUpStatusGived() {
        return upStatusGived;
    }

    public void setUpStatusGived(Date upStatusGived) {
        this.upStatusGived = upStatusGived;
    }

    public String getDignose() {
        return dignose;
    }

    public void setDignose(String dignose) {
        this.dignose = dignose;
    }

    public Double getPriceAfterHealthInsurance() {
        return priceAfterHealthInsurance;
    }

    public void setPriceAfterHealthInsurance(Double priceAfterHealthInsurance) {
        this.priceAfterHealthInsurance = priceAfterHealthInsurance;
    }

    public Date getUpDateTime() {
        return upDateTime;
    }

    public void setUpDateTime(Date upDateTime) {
        this.upDateTime = upDateTime;
    }

    @XmlTransient
    public Collection<TbPrescriptionDetail> getTbPrescriptionDetailCollection() {
        return tbPrescriptionDetailCollection;
    }

    public void setTbPrescriptionDetailCollection(Collection<TbPrescriptionDetail> tbPrescriptionDetailCollection) {
        this.tbPrescriptionDetailCollection = tbPrescriptionDetailCollection;
    }

    public TbMedicalRecord getMedicalRecordID() {
        return medicalRecordID;
    }

    public void setMedicalRecordID(TbMedicalRecord medicalRecordID) {
        this.medicalRecordID = medicalRecordID;
    }

    public TbFaculty getFacultyFrom() {
        return facultyFrom;
    }

    public void setFacultyFrom(TbFaculty facultyFrom) {
        this.facultyFrom = facultyFrom;
    }

    public TbEmployee getUpEmployeeID() {
        return upEmployeeID;
    }

    public void setUpEmployeeID(TbEmployee upEmployeeID) {
        this.upEmployeeID = upEmployeeID;
    }

    public TbEmployee getEmployeeID() {
        return employeeID;
    }

    public void setEmployeeID(TbEmployee employeeID) {
        this.employeeID = employeeID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (prescriptionID != null ? prescriptionID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbPrescription)) {
            return false;
        }
        TbPrescription other = (TbPrescription) object;
        if ((this.prescriptionID == null && other.prescriptionID != null) || (this.prescriptionID != null && !this.prescriptionID.equals(other.prescriptionID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "project.ett.TbPrescription[ prescriptionID=" + prescriptionID + " ]";
    }
    
}
