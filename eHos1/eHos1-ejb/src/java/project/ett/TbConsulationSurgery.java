/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project.ett;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DaySLeePer
 */
@Entity
@Table(name = "tbConsulationSurgery")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbConsulationSurgery.findAll", query = "SELECT t FROM TbConsulationSurgery t"),
    @NamedQuery(name = "TbConsulationSurgery.findByConsulationID", query = "SELECT t FROM TbConsulationSurgery t WHERE t.consulationID = :consulationID"),
    @NamedQuery(name = "TbConsulationSurgery.findByConsulationDate", query = "SELECT t FROM TbConsulationSurgery t WHERE t.consulationDate = :consulationDate"),
    @NamedQuery(name = "TbConsulationSurgery.findByConsulation", query = "SELECT t FROM TbConsulationSurgery t WHERE t.consulation = :consulation"),
    @NamedQuery(name = "TbConsulationSurgery.findByStatusConsulation", query = "SELECT t FROM TbConsulationSurgery t WHERE t.statusConsulation = :statusConsulation"),
    @NamedQuery(name = "TbConsulationSurgery.findByCreateDate", query = "SELECT t FROM TbConsulationSurgery t WHERE t.createDate = :createDate")})
public class TbConsulationSurgery implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "ConsulationID")
    private String consulationID;
    @Column(name = "ConsulationDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date consulationDate;
    @Size(max = 250)
    @Column(name = "Consulation")
    private String consulation;
    @Column(name = "StatusConsulation")
    private Boolean statusConsulation;
    @Column(name = "CreateDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @OneToMany(mappedBy = "consulationID")
    private Collection<TbConsulationDoctorDetail> tbConsulationDoctorDetailCollection;
    @JoinColumn(name = "SuggestSurgeryID", referencedColumnName = "SuggestSurgeryID")
    @ManyToOne
    private TbSuggestSurgery suggestSurgeryID;
    @JoinColumn(name = "UserID", referencedColumnName = "EmployeeID")
    @ManyToOne
    private TbEmployee userID;

    public TbConsulationSurgery() {
    }

    public TbConsulationSurgery(String consulationID) {
        this.consulationID = consulationID;
    }

    public String getConsulationID() {
        return consulationID;
    }

    public void setConsulationID(String consulationID) {
        this.consulationID = consulationID;
    }

    public Date getConsulationDate() {
        return consulationDate;
    }

    public void setConsulationDate(Date consulationDate) {
        this.consulationDate = consulationDate;
    }

    public String getConsulation() {
        return consulation;
    }

    public void setConsulation(String consulation) {
        this.consulation = consulation;
    }

    public Boolean getStatusConsulation() {
        return statusConsulation;
    }

    public void setStatusConsulation(Boolean statusConsulation) {
        this.statusConsulation = statusConsulation;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @XmlTransient
    public Collection<TbConsulationDoctorDetail> getTbConsulationDoctorDetailCollection() {
        return tbConsulationDoctorDetailCollection;
    }

    public void setTbConsulationDoctorDetailCollection(Collection<TbConsulationDoctorDetail> tbConsulationDoctorDetailCollection) {
        this.tbConsulationDoctorDetailCollection = tbConsulationDoctorDetailCollection;
    }

    public TbSuggestSurgery getSuggestSurgeryID() {
        return suggestSurgeryID;
    }

    public void setSuggestSurgeryID(TbSuggestSurgery suggestSurgeryID) {
        this.suggestSurgeryID = suggestSurgeryID;
    }

    public TbEmployee getUserID() {
        return userID;
    }

    public void setUserID(TbEmployee userID) {
        this.userID = userID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (consulationID != null ? consulationID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbConsulationSurgery)) {
            return false;
        }
        TbConsulationSurgery other = (TbConsulationSurgery) object;
        if ((this.consulationID == null && other.consulationID != null) || (this.consulationID != null && !this.consulationID.equals(other.consulationID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "project.ett.TbConsulationSurgery[ consulationID=" + consulationID + " ]";
    }
    
}
