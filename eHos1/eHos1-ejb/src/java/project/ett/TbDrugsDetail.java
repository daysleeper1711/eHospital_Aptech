/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project.ett;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DaySLeePer
 */
@Entity
@Table(name = "tbDrugsDetail")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbDrugsDetail.findAll", query = "SELECT t FROM TbDrugsDetail t"),
    @NamedQuery(name = "TbDrugsDetail.findByDrugsDetailID", query = "SELECT t FROM TbDrugsDetail t WHERE t.drugsDetailID = :drugsDetailID"),
    @NamedQuery(name = "TbDrugsDetail.findByUnit", query = "SELECT t FROM TbDrugsDetail t WHERE t.unit = :unit"),
    @NamedQuery(name = "TbDrugsDetail.findByQuantity", query = "SELECT t FROM TbDrugsDetail t WHERE t.quantity = :quantity"),
    @NamedQuery(name = "TbDrugsDetail.findByStatusDrugs", query = "SELECT t FROM TbDrugsDetail t WHERE t.statusDrugs = :statusDrugs"),
    @NamedQuery(name = "TbDrugsDetail.findByCreateDate", query = "SELECT t FROM TbDrugsDetail t WHERE t.createDate = :createDate")})
public class TbDrugsDetail implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "DrugsDetailID")
    private String drugsDetailID;
    @Size(max = 50)
    @Column(name = "Unit")
    private String unit;
    @Column(name = "Quantity")
    private Integer quantity;
    @Column(name = "StatusDrugs")
    private Boolean statusDrugs;
    @Column(name = "CreateDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @JoinColumn(name = "SurgeryRecordID", referencedColumnName = "SurgeryRecordID")
    @ManyToOne
    private TbSurgeryRecord surgeryRecordID;
    @JoinColumn(name = "DrugID", referencedColumnName = "ItemCode")
    @ManyToOne
    private TbItemCodeManagement drugID;
    @JoinColumn(name = "UserID", referencedColumnName = "EmployeeID")
    @ManyToOne
    private TbEmployee userID;

    public TbDrugsDetail() {
    }

    public TbDrugsDetail(String drugsDetailID) {
        this.drugsDetailID = drugsDetailID;
    }

    public String getDrugsDetailID() {
        return drugsDetailID;
    }

    public void setDrugsDetailID(String drugsDetailID) {
        this.drugsDetailID = drugsDetailID;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Boolean getStatusDrugs() {
        return statusDrugs;
    }

    public void setStatusDrugs(Boolean statusDrugs) {
        this.statusDrugs = statusDrugs;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public TbSurgeryRecord getSurgeryRecordID() {
        return surgeryRecordID;
    }

    public void setSurgeryRecordID(TbSurgeryRecord surgeryRecordID) {
        this.surgeryRecordID = surgeryRecordID;
    }

    public TbItemCodeManagement getDrugID() {
        return drugID;
    }

    public void setDrugID(TbItemCodeManagement drugID) {
        this.drugID = drugID;
    }

    public TbEmployee getUserID() {
        return userID;
    }

    public void setUserID(TbEmployee userID) {
        this.userID = userID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (drugsDetailID != null ? drugsDetailID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbDrugsDetail)) {
            return false;
        }
        TbDrugsDetail other = (TbDrugsDetail) object;
        if ((this.drugsDetailID == null && other.drugsDetailID != null) || (this.drugsDetailID != null && !this.drugsDetailID.equals(other.drugsDetailID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "project.ett.TbDrugsDetail[ drugsDetailID=" + drugsDetailID + " ]";
    }
    
}
