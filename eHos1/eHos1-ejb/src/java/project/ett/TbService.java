/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project.ett;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DaySLeePer
 */
@Entity
@Table(name = "tbService")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbService.findAll", query = "SELECT t FROM TbService t"),
    @NamedQuery(name = "TbService.findByServiceID", query = "SELECT t FROM TbService t WHERE t.serviceID = :serviceID"),
    @NamedQuery(name = "TbService.findByServiceName", query = "SELECT t FROM TbService t WHERE t.serviceName = :serviceName"),
    @NamedQuery(name = "TbService.findByServiceContent", query = "SELECT t FROM TbService t WHERE t.serviceContent = :serviceContent"),
    @NamedQuery(name = "TbService.findByUnitOfMea", query = "SELECT t FROM TbService t WHERE t.unitOfMea = :unitOfMea"),
    @NamedQuery(name = "TbService.findByStatus", query = "SELECT t FROM TbService t WHERE t.status = :status"),
    @NamedQuery(name = "TbService.findByCreateDate", query = "SELECT t FROM TbService t WHERE t.createDate = :createDate"),
    @NamedQuery(name = "TbService.findByUpdateDate", query = "SELECT t FROM TbService t WHERE t.updateDate = :updateDate")})
public class TbService implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ServiceID")
    private Integer serviceID;
    @Size(max = 200)
    @Column(name = "ServiceName")
    private String serviceName;
    @Size(max = 100)
    @Column(name = "ServiceContent")
    private String serviceContent;
    @Size(max = 20)
    @Column(name = "UnitOfMea")
    private String unitOfMea;
    @Column(name = "Status")
    private Boolean status;
    @Column(name = "CreateDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "UpdateDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "serviceID")
    private Collection<TbBed> tbBedCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "serviceID")
    private Collection<TbGKBService> tbGKBServiceCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "serviceID")
    private Collection<TbTestReqDetail> tbTestReqDetailCollection;
    @JoinColumn(name = "GroupID", referencedColumnName = "GroupID")
    @ManyToOne
    private TbServiceGroup groupID;
    @JoinColumn(name = "UpdateEmp", referencedColumnName = "EmployeeID")
    @ManyToOne
    private TbEmployee updateEmp;
    @JoinColumn(name = "Creator", referencedColumnName = "EmployeeID")
    @ManyToOne
    private TbEmployee creator;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "serviceID")
    private Collection<TbServicePrice> tbServicePriceCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "serviceID")
    private Collection<TbStandardValue> tbStandardValueCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "serviceID")
    private Collection<TbTreatmentFeeEnumerationDetail> tbTreatmentFeeEnumerationDetailCollection;

    public TbService() {
    }

    public TbService(Integer serviceID) {
        this.serviceID = serviceID;
    }

    public Integer getServiceID() {
        return serviceID;
    }

    public void setServiceID(Integer serviceID) {
        this.serviceID = serviceID;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceContent() {
        return serviceContent;
    }

    public void setServiceContent(String serviceContent) {
        this.serviceContent = serviceContent;
    }

    public String getUnitOfMea() {
        return unitOfMea;
    }

    public void setUnitOfMea(String unitOfMea) {
        this.unitOfMea = unitOfMea;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    @XmlTransient
    public Collection<TbBed> getTbBedCollection() {
        return tbBedCollection;
    }

    public void setTbBedCollection(Collection<TbBed> tbBedCollection) {
        this.tbBedCollection = tbBedCollection;
    }

    @XmlTransient
    public Collection<TbGKBService> getTbGKBServiceCollection() {
        return tbGKBServiceCollection;
    }

    public void setTbGKBServiceCollection(Collection<TbGKBService> tbGKBServiceCollection) {
        this.tbGKBServiceCollection = tbGKBServiceCollection;
    }

    @XmlTransient
    public Collection<TbTestReqDetail> getTbTestReqDetailCollection() {
        return tbTestReqDetailCollection;
    }

    public void setTbTestReqDetailCollection(Collection<TbTestReqDetail> tbTestReqDetailCollection) {
        this.tbTestReqDetailCollection = tbTestReqDetailCollection;
    }

    public TbServiceGroup getGroupID() {
        return groupID;
    }

    public void setGroupID(TbServiceGroup groupID) {
        this.groupID = groupID;
    }

    public TbEmployee getUpdateEmp() {
        return updateEmp;
    }

    public void setUpdateEmp(TbEmployee updateEmp) {
        this.updateEmp = updateEmp;
    }

    public TbEmployee getCreator() {
        return creator;
    }

    public void setCreator(TbEmployee creator) {
        this.creator = creator;
    }

    @XmlTransient
    public Collection<TbServicePrice> getTbServicePriceCollection() {
        return tbServicePriceCollection;
    }

    public void setTbServicePriceCollection(Collection<TbServicePrice> tbServicePriceCollection) {
        this.tbServicePriceCollection = tbServicePriceCollection;
    }

    @XmlTransient
    public Collection<TbStandardValue> getTbStandardValueCollection() {
        return tbStandardValueCollection;
    }

    public void setTbStandardValueCollection(Collection<TbStandardValue> tbStandardValueCollection) {
        this.tbStandardValueCollection = tbStandardValueCollection;
    }

    @XmlTransient
    public Collection<TbTreatmentFeeEnumerationDetail> getTbTreatmentFeeEnumerationDetailCollection() {
        return tbTreatmentFeeEnumerationDetailCollection;
    }

    public void setTbTreatmentFeeEnumerationDetailCollection(Collection<TbTreatmentFeeEnumerationDetail> tbTreatmentFeeEnumerationDetailCollection) {
        this.tbTreatmentFeeEnumerationDetailCollection = tbTreatmentFeeEnumerationDetailCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (serviceID != null ? serviceID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbService)) {
            return false;
        }
        TbService other = (TbService) object;
        if ((this.serviceID == null && other.serviceID != null) || (this.serviceID != null && !this.serviceID.equals(other.serviceID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "project.ett.TbService[ serviceID=" + serviceID + " ]";
    }
    
}
