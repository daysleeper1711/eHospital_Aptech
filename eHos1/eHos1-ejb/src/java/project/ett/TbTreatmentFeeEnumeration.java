/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project.ett;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DaySLeePer
 */
@Entity
@Table(name = "tbTreatmentFeeEnumeration")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbTreatmentFeeEnumeration.findAll", query = "SELECT t FROM TbTreatmentFeeEnumeration t"),
    @NamedQuery(name = "TbTreatmentFeeEnumeration.findByTfeId", query = "SELECT t FROM TbTreatmentFeeEnumeration t WHERE t.tfeId = :tfeId"),
    @NamedQuery(name = "TbTreatmentFeeEnumeration.findByReceivedDate", query = "SELECT t FROM TbTreatmentFeeEnumeration t WHERE t.receivedDate = :receivedDate"),
    @NamedQuery(name = "TbTreatmentFeeEnumeration.findByFinalTotal", query = "SELECT t FROM TbTreatmentFeeEnumeration t WHERE t.finalTotal = :finalTotal"),
    @NamedQuery(name = "TbTreatmentFeeEnumeration.findByTotalDeposit", query = "SELECT t FROM TbTreatmentFeeEnumeration t WHERE t.totalDeposit = :totalDeposit"),
    @NamedQuery(name = "TbTreatmentFeeEnumeration.findByIsReturn", query = "SELECT t FROM TbTreatmentFeeEnumeration t WHERE t.isReturn = :isReturn"),
    @NamedQuery(name = "TbTreatmentFeeEnumeration.findByAmountReturn", query = "SELECT t FROM TbTreatmentFeeEnumeration t WHERE t.amountReturn = :amountReturn"),
    @NamedQuery(name = "TbTreatmentFeeEnumeration.findByAmountPaymore", query = "SELECT t FROM TbTreatmentFeeEnumeration t WHERE t.amountPaymore = :amountPaymore"),
    @NamedQuery(name = "TbTreatmentFeeEnumeration.findByStatus", query = "SELECT t FROM TbTreatmentFeeEnumeration t WHERE t.status = :status")})
public class TbTreatmentFeeEnumeration implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
//    @Basic(optional = false)
//    @NotNull
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "TFE_ID")
    private Integer tfeId;
    @Column(name = "ReceivedDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date receivedDate;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "FinalTotal")
    private Double finalTotal;
    @Column(name = "TotalDeposit")
    private Double totalDeposit;
    @Column(name = "IsReturn")
    private Boolean isReturn;
    @Column(name = "AmountReturn")
    private Double amountReturn;
    @Column(name = "AmountPaymore")
    private Double amountPaymore;
    @Size(max = 30)
    @Column(name = "Status")
    private String status;
    @JoinColumn(name = "MedRecordID", referencedColumnName = "MedRecordID")
    @ManyToOne
    private TbMedicalRecord medRecordID;
    @JoinColumn(name = "ReceivedByWhom", referencedColumnName = "EmployeeID")
    @ManyToOne
    private TbEmployee receivedByWhom;
    @OneToMany(mappedBy = "tfeId")
    private Collection<TbTreatmentFeeEnumerationDetail> tbTreatmentFeeEnumerationDetailCollection;

    public TbTreatmentFeeEnumeration() {
    }

    public TbTreatmentFeeEnumeration(Integer tfeId) {
        this.tfeId = tfeId;
    }

    public Integer getTfeId() {
        return tfeId;
    }

    public void setTfeId(Integer tfeId) {
        this.tfeId = tfeId;
    }

    public Date getReceivedDate() {
        return receivedDate;
    }

    public void setReceivedDate(Date receivedDate) {
        this.receivedDate = receivedDate;
    }

    public Double getFinalTotal() {
        return finalTotal;
    }

    public void setFinalTotal(Double finalTotal) {
        this.finalTotal = finalTotal;
    }

    public Double getTotalDeposit() {
        return totalDeposit;
    }

    public void setTotalDeposit(Double totalDeposit) {
        this.totalDeposit = totalDeposit;
    }

    public Boolean getIsReturn() {
        return isReturn;
    }

    public void setIsReturn(Boolean isReturn) {
        this.isReturn = isReturn;
    }

    public Double getAmountReturn() {
        return amountReturn;
    }

    public void setAmountReturn(Double amountReturn) {
        this.amountReturn = amountReturn;
    }

    public Double getAmountPaymore() {
        return amountPaymore;
    }

    public void setAmountPaymore(Double amountPaymore) {
        this.amountPaymore = amountPaymore;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public TbMedicalRecord getMedRecordID() {
        return medRecordID;
    }

    public void setMedRecordID(TbMedicalRecord medRecordID) {
        this.medRecordID = medRecordID;
    }

    public TbEmployee getReceivedByWhom() {
        return receivedByWhom;
    }

    public void setReceivedByWhom(TbEmployee receivedByWhom) {
        this.receivedByWhom = receivedByWhom;
    }

    @XmlTransient
    public Collection<TbTreatmentFeeEnumerationDetail> getTbTreatmentFeeEnumerationDetailCollection() {
        return tbTreatmentFeeEnumerationDetailCollection;
    }

    public void setTbTreatmentFeeEnumerationDetailCollection(Collection<TbTreatmentFeeEnumerationDetail> tbTreatmentFeeEnumerationDetailCollection) {
        this.tbTreatmentFeeEnumerationDetailCollection = tbTreatmentFeeEnumerationDetailCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tfeId != null ? tfeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbTreatmentFeeEnumeration)) {
            return false;
        }
        TbTreatmentFeeEnumeration other = (TbTreatmentFeeEnumeration) object;
        if ((this.tfeId == null && other.tfeId != null) || (this.tfeId != null && !this.tfeId.equals(other.tfeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "project.ett.TbTreatmentFeeEnumeration[ tfeId=" + tfeId + " ]";
    }
    
}
