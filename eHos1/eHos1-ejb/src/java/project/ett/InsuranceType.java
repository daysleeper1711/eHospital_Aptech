/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project.ett;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DaySLeePer
 */
@Entity
@Table(name = "InsuranceType")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "InsuranceType.findAll", query = "SELECT i FROM InsuranceType i"),
    @NamedQuery(name = "InsuranceType.findByInsuranceTypeID", query = "SELECT i FROM InsuranceType i WHERE i.insuranceTypeID = :insuranceTypeID"),
    @NamedQuery(name = "InsuranceType.findByDescription", query = "SELECT i FROM InsuranceType i WHERE i.description = :description"),
    @NamedQuery(name = "InsuranceType.findByPrecentage", query = "SELECT i FROM InsuranceType i WHERE i.precentage = :precentage"),
    @NamedQuery(name = "InsuranceType.findByStatusType", query = "SELECT i FROM InsuranceType i WHERE i.statusType = :statusType")})
public class InsuranceType implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "InsuranceTypeID")
    private String insuranceTypeID;
    @Size(max = 50)
    @Column(name = "Description")
    private String description;
    @Column(name = "Precentage")
    private Integer precentage;
    @Column(name = "StatusType")
    private Boolean statusType;
    @OneToMany(mappedBy = "insuranceTypeID")
    private Collection<TbInsurane> tbInsuraneCollection;

    public InsuranceType() {
    }

    public InsuranceType(String insuranceTypeID) {
        this.insuranceTypeID = insuranceTypeID;
    }

    public String getInsuranceTypeID() {
        return insuranceTypeID;
    }

    public void setInsuranceTypeID(String insuranceTypeID) {
        this.insuranceTypeID = insuranceTypeID;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getPrecentage() {
        return precentage;
    }

    public void setPrecentage(Integer precentage) {
        this.precentage = precentage;
    }

    public Boolean getStatusType() {
        return statusType;
    }

    public void setStatusType(Boolean statusType) {
        this.statusType = statusType;
    }

    @XmlTransient
    public Collection<TbInsurane> getTbInsuraneCollection() {
        return tbInsuraneCollection;
    }

    public void setTbInsuraneCollection(Collection<TbInsurane> tbInsuraneCollection) {
        this.tbInsuraneCollection = tbInsuraneCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (insuranceTypeID != null ? insuranceTypeID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InsuranceType)) {
            return false;
        }
        InsuranceType other = (InsuranceType) object;
        if ((this.insuranceTypeID == null && other.insuranceTypeID != null) || (this.insuranceTypeID != null && !this.insuranceTypeID.equals(other.insuranceTypeID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "project.ett.InsuranceType[ insuranceTypeID=" + insuranceTypeID + " ]";
    }
    
}
