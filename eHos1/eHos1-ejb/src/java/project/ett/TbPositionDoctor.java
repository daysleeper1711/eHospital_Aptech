/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project.ett;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DaySLeePer
 */
@Entity
@Table(name = "tbPositionDoctor")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbPositionDoctor.findAll", query = "SELECT t FROM TbPositionDoctor t"),
    @NamedQuery(name = "TbPositionDoctor.findByPositionID", query = "SELECT t FROM TbPositionDoctor t WHERE t.positionID = :positionID"),
    @NamedQuery(name = "TbPositionDoctor.findByPositionName", query = "SELECT t FROM TbPositionDoctor t WHERE t.positionName = :positionName"),
    @NamedQuery(name = "TbPositionDoctor.findByStatusPosition", query = "SELECT t FROM TbPositionDoctor t WHERE t.statusPosition = :statusPosition"),
    @NamedQuery(name = "TbPositionDoctor.findByCreateDate", query = "SELECT t FROM TbPositionDoctor t WHERE t.createDate = :createDate")})
public class TbPositionDoctor implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PositionID")
    private Integer positionID;
    @Size(max = 50)
    @Column(name = "PositionName")
    private String positionName;
    @Column(name = "StatusPosition")
    private Boolean statusPosition;
    @Column(name = "CreateDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @OneToMany(mappedBy = "positionID")
    private Collection<TbOperatorDoctorDetail> tbOperatorDoctorDetailCollection;
    @JoinColumn(name = "UserID", referencedColumnName = "EmployeeID")
    @ManyToOne
    private TbEmployee userID;

    public TbPositionDoctor() {
    }

    public TbPositionDoctor(Integer positionID) {
        this.positionID = positionID;
    }

    public Integer getPositionID() {
        return positionID;
    }

    public void setPositionID(Integer positionID) {
        this.positionID = positionID;
    }

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public Boolean getStatusPosition() {
        return statusPosition;
    }

    public void setStatusPosition(Boolean statusPosition) {
        this.statusPosition = statusPosition;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @XmlTransient
    public Collection<TbOperatorDoctorDetail> getTbOperatorDoctorDetailCollection() {
        return tbOperatorDoctorDetailCollection;
    }

    public void setTbOperatorDoctorDetailCollection(Collection<TbOperatorDoctorDetail> tbOperatorDoctorDetailCollection) {
        this.tbOperatorDoctorDetailCollection = tbOperatorDoctorDetailCollection;
    }

    public TbEmployee getUserID() {
        return userID;
    }

    public void setUserID(TbEmployee userID) {
        this.userID = userID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (positionID != null ? positionID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbPositionDoctor)) {
            return false;
        }
        TbPositionDoctor other = (TbPositionDoctor) object;
        if ((this.positionID == null && other.positionID != null) || (this.positionID != null && !this.positionID.equals(other.positionID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "project.ett.TbPositionDoctor[ positionID=" + positionID + " ]";
    }
    
}
