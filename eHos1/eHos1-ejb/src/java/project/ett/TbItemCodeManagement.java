/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project.ett;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DaySLeePer
 */
@Entity
@Table(name = "tbItemCode_Management")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbItemCodeManagement.findAll", query = "SELECT t FROM TbItemCodeManagement t"),
    @NamedQuery(name = "TbItemCodeManagement.findByItemCode", query = "SELECT t FROM TbItemCodeManagement t WHERE t.itemCode = :itemCode"),
    @NamedQuery(name = "TbItemCodeManagement.findByItemName", query = "SELECT t FROM TbItemCodeManagement t WHERE t.itemName = :itemName"),
    @NamedQuery(name = "TbItemCodeManagement.findByContent", query = "SELECT t FROM TbItemCodeManagement t WHERE t.content = :content"),
    @NamedQuery(name = "TbItemCodeManagement.findByNation", query = "SELECT t FROM TbItemCodeManagement t WHERE t.nation = :nation"),
    @NamedQuery(name = "TbItemCodeManagement.findByUnit", query = "SELECT t FROM TbItemCodeManagement t WHERE t.unit = :unit"),
    @NamedQuery(name = "TbItemCodeManagement.findByGenericDrug", query = "SELECT t FROM TbItemCodeManagement t WHERE t.genericDrug = :genericDrug"),
    @NamedQuery(name = "TbItemCodeManagement.findByHowToUse", query = "SELECT t FROM TbItemCodeManagement t WHERE t.howToUse = :howToUse"),
    @NamedQuery(name = "TbItemCodeManagement.findByCrDate", query = "SELECT t FROM TbItemCodeManagement t WHERE t.crDate = :crDate"),
    @NamedQuery(name = "TbItemCodeManagement.findByCreateby", query = "SELECT t FROM TbItemCodeManagement t WHERE t.createby = :createby"),
    @NamedQuery(name = "TbItemCodeManagement.findByUpdateby", query = "SELECT t FROM TbItemCodeManagement t WHERE t.updateby = :updateby"),
    @NamedQuery(name = "TbItemCodeManagement.findByUpDate", query = "SELECT t FROM TbItemCodeManagement t WHERE t.upDate = :upDate"),
    @NamedQuery(name = "TbItemCodeManagement.findByXPrice", query = "SELECT t FROM TbItemCodeManagement t WHERE t.xPrice = :xPrice"),
    @NamedQuery(name = "TbItemCodeManagement.findByTypeITcode", query = "SELECT t FROM TbItemCodeManagement t WHERE t.typeITcode = :typeITcode"),
    @NamedQuery(name = "TbItemCodeManagement.findByUStatus", query = "SELECT t FROM TbItemCodeManagement t WHERE t.uStatus = :uStatus")})
public class TbItemCodeManagement implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "ItemCode")
    private String itemCode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "ItemName")
    private String itemName;
    @Size(max = 100)
    @Column(name = "Content")
    private String content;
    @Size(max = 20)
    @Column(name = "Nation")
    private String nation;
    @Size(max = 50)
    @Column(name = "Unit")
    private String unit;
    @Size(max = 100)
    @Column(name = "GenericDrug")
    private String genericDrug;
    @Size(max = 100)
    @Column(name = "HowToUse")
    private String howToUse;
    @Column(name = "Cr_Date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date crDate;
    @Size(max = 20)
    @Column(name = "Createby")
    private String createby;
    @Size(max = 20)
    @Column(name = "Updateby")
    private String updateby;
    @Column(name = "Up_Date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date upDate;
    @Column(name = "X_Price")
    private Double xPrice;
    @Size(max = 2)
    @Column(name = "Type_ITcode")
    private String typeITcode;
    @Size(max = 2)
    @Column(name = "U_Status")
    private String uStatus;
    @OneToMany(mappedBy = "medicineID")
    private Collection<TbMedicineSuppliesDetail> tbMedicineSuppliesDetailCollection;
    @OneToMany(mappedBy = "medicineID")
    private Collection<TbPrescriptionDetail> tbPrescriptionDetailCollection;
    @OneToMany(mappedBy = "drugID")
    private Collection<TbDrugsDetail> tbDrugsDetailCollection;
    @OneToMany(mappedBy = "suppliesID")
    private Collection<TbSuppliesDetail> tbSuppliesDetailCollection;

    public TbItemCodeManagement() {
    }

    public TbItemCodeManagement(String itemCode) {
        this.itemCode = itemCode;
    }

    public TbItemCodeManagement(String itemCode, String itemName) {
        this.itemCode = itemCode;
        this.itemName = itemName;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getGenericDrug() {
        return genericDrug;
    }

    public void setGenericDrug(String genericDrug) {
        this.genericDrug = genericDrug;
    }

    public String getHowToUse() {
        return howToUse;
    }

    public void setHowToUse(String howToUse) {
        this.howToUse = howToUse;
    }

    public Date getCrDate() {
        return crDate;
    }

    public void setCrDate(Date crDate) {
        this.crDate = crDate;
    }

    public String getCreateby() {
        return createby;
    }

    public void setCreateby(String createby) {
        this.createby = createby;
    }

    public String getUpdateby() {
        return updateby;
    }

    public void setUpdateby(String updateby) {
        this.updateby = updateby;
    }

    public Date getUpDate() {
        return upDate;
    }

    public void setUpDate(Date upDate) {
        this.upDate = upDate;
    }

    public Double getXPrice() {
        return xPrice;
    }

    public void setXPrice(Double xPrice) {
        this.xPrice = xPrice;
    }

    public String getTypeITcode() {
        return typeITcode;
    }

    public void setTypeITcode(String typeITcode) {
        this.typeITcode = typeITcode;
    }

    public String getUStatus() {
        return uStatus;
    }

    public void setUStatus(String uStatus) {
        this.uStatus = uStatus;
    }

    @XmlTransient
    public Collection<TbMedicineSuppliesDetail> getTbMedicineSuppliesDetailCollection() {
        return tbMedicineSuppliesDetailCollection;
    }

    public void setTbMedicineSuppliesDetailCollection(Collection<TbMedicineSuppliesDetail> tbMedicineSuppliesDetailCollection) {
        this.tbMedicineSuppliesDetailCollection = tbMedicineSuppliesDetailCollection;
    }

    @XmlTransient
    public Collection<TbPrescriptionDetail> getTbPrescriptionDetailCollection() {
        return tbPrescriptionDetailCollection;
    }

    public void setTbPrescriptionDetailCollection(Collection<TbPrescriptionDetail> tbPrescriptionDetailCollection) {
        this.tbPrescriptionDetailCollection = tbPrescriptionDetailCollection;
    }

    @XmlTransient
    public Collection<TbDrugsDetail> getTbDrugsDetailCollection() {
        return tbDrugsDetailCollection;
    }

    public void setTbDrugsDetailCollection(Collection<TbDrugsDetail> tbDrugsDetailCollection) {
        this.tbDrugsDetailCollection = tbDrugsDetailCollection;
    }

    @XmlTransient
    public Collection<TbSuppliesDetail> getTbSuppliesDetailCollection() {
        return tbSuppliesDetailCollection;
    }

    public void setTbSuppliesDetailCollection(Collection<TbSuppliesDetail> tbSuppliesDetailCollection) {
        this.tbSuppliesDetailCollection = tbSuppliesDetailCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (itemCode != null ? itemCode.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbItemCodeManagement)) {
            return false;
        }
        TbItemCodeManagement other = (TbItemCodeManagement) object;
        if ((this.itemCode == null && other.itemCode != null) || (this.itemCode != null && !this.itemCode.equals(other.itemCode))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "project.ett.TbItemCodeManagement[ itemCode=" + itemCode + " ]";
    }
    
}
