/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project.ett;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DaySLeePer
 */
@Entity
@Table(name = "tbKQ_KB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbKQKB.findAll", query = "SELECT t FROM TbKQKB t"),
    @NamedQuery(name = "TbKQKB.findByExaminationResultsID", query = "SELECT t FROM TbKQKB t WHERE t.examinationResultsID = :examinationResultsID"),
    @NamedQuery(name = "TbKQKB.findByInputDate", query = "SELECT t FROM TbKQKB t WHERE t.inputDate = :inputDate"),
    @NamedQuery(name = "TbKQKB.findByCreator", query = "SELECT t FROM TbKQKB t WHERE t.creator = :creator"),
    @NamedQuery(name = "TbKQKB.findByKQStatus", query = "SELECT t FROM TbKQKB t WHERE t.kQStatus = :kQStatus"),
    @NamedQuery(name = "TbKQKB.findByConclue", query = "SELECT t FROM TbKQKB t WHERE t.conclue = :conclue"),
    @NamedQuery(name = "TbKQKB.findByBenh", query = "SELECT t FROM TbKQKB t WHERE t.benh = :benh"),
    @NamedQuery(name = "TbKQKB.findByDeNghi", query = "SELECT t FROM TbKQKB t WHERE t.deNghi = :deNghi")})
public class TbKQKB implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ExaminationResultsID")
    private Integer examinationResultsID;
    @Column(name = "InputDate")
    @Temporal(TemporalType.DATE)
    private Date inputDate;
    @Size(max = 20)
    @Column(name = "Creator")
    private String creator;
    @Size(max = 10)
    @Column(name = "KQStatus")
    private String kQStatus;
    @Size(max = 100)
    @Column(name = "Conclue")
    private String conclue;
    @Size(max = 150)
    @Column(name = "Benh")
    private String benh;
    @Size(max = 150)
    @Column(name = "DeNghi")
    private String deNghi;
    @JoinColumn(name = "PatID", referencedColumnName = "PatID")
    @ManyToOne
    private TbPatient patID;
    @JoinColumn(name = "OutPatientID", referencedColumnName = "OutPatID")
    @ManyToOne
    private TbOutPatient outPatientID;
    @JoinColumn(name = "GKB_ServiceID", referencedColumnName = "GKB_ServiceID")
    @ManyToOne
    private TbGKBService gKBServiceID;
    @JoinColumn(name = "EmployeeID", referencedColumnName = "EmployeeID")
    @ManyToOne
    private TbEmployee employeeID;
    @OneToMany(mappedBy = "examinationResultsID")
    private Collection<TbKQKBDetail> tbKQKBDetailCollection;
    @OneToMany(mappedBy = "examinationResultsID")
    private Collection<TbKTQ> tbKTQCollection;

    public TbKQKB() {
    }

    public TbKQKB(Integer examinationResultsID) {
        this.examinationResultsID = examinationResultsID;
    }

    public Integer getExaminationResultsID() {
        return examinationResultsID;
    }

    public void setExaminationResultsID(Integer examinationResultsID) {
        this.examinationResultsID = examinationResultsID;
    }

    public Date getInputDate() {
        return inputDate;
    }

    public void setInputDate(Date inputDate) {
        this.inputDate = inputDate;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getKQStatus() {
        return kQStatus;
    }

    public void setKQStatus(String kQStatus) {
        this.kQStatus = kQStatus;
    }

    public String getConclue() {
        return conclue;
    }

    public void setConclue(String conclue) {
        this.conclue = conclue;
    }

    public String getBenh() {
        return benh;
    }

    public void setBenh(String benh) {
        this.benh = benh;
    }

    public String getDeNghi() {
        return deNghi;
    }

    public void setDeNghi(String deNghi) {
        this.deNghi = deNghi;
    }

    public TbPatient getPatID() {
        return patID;
    }

    public void setPatID(TbPatient patID) {
        this.patID = patID;
    }

    public TbOutPatient getOutPatientID() {
        return outPatientID;
    }

    public void setOutPatientID(TbOutPatient outPatientID) {
        this.outPatientID = outPatientID;
    }

    public TbGKBService getGKBServiceID() {
        return gKBServiceID;
    }

    public void setGKBServiceID(TbGKBService gKBServiceID) {
        this.gKBServiceID = gKBServiceID;
    }

    public TbEmployee getEmployeeID() {
        return employeeID;
    }

    public void setEmployeeID(TbEmployee employeeID) {
        this.employeeID = employeeID;
    }

    @XmlTransient
    public Collection<TbKQKBDetail> getTbKQKBDetailCollection() {
        return tbKQKBDetailCollection;
    }

    public void setTbKQKBDetailCollection(Collection<TbKQKBDetail> tbKQKBDetailCollection) {
        this.tbKQKBDetailCollection = tbKQKBDetailCollection;
    }

    @XmlTransient
    public Collection<TbKTQ> getTbKTQCollection() {
        return tbKTQCollection;
    }

    public void setTbKTQCollection(Collection<TbKTQ> tbKTQCollection) {
        this.tbKTQCollection = tbKTQCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (examinationResultsID != null ? examinationResultsID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbKQKB)) {
            return false;
        }
        TbKQKB other = (TbKQKB) object;
        if ((this.examinationResultsID == null && other.examinationResultsID != null) || (this.examinationResultsID != null && !this.examinationResultsID.equals(other.examinationResultsID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "project.ett.TbKQKB[ examinationResultsID=" + examinationResultsID + " ]";
    }
    
}
