/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project.ett;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DaySLeePer
 */
@Entity
@Table(name = "tbBed")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbBed.findAll", query = "SELECT t FROM TbBed t"),
    @NamedQuery(name = "TbBed.findByBedID", query = "SELECT t FROM TbBed t WHERE t.bedID = :bedID"),
    @NamedQuery(name = "TbBed.findByBedName", query = "SELECT t FROM TbBed t WHERE t.bedName = :bedName"),
    @NamedQuery(name = "TbBed.findByIsSubbed", query = "SELECT t FROM TbBed t WHERE t.isSubbed = :isSubbed"),
    @NamedQuery(name = "TbBed.findByIsEnable", query = "SELECT t FROM TbBed t WHERE t.isEnable = :isEnable"),
    @NamedQuery(name = "TbBed.findByCreateDate", query = "SELECT t FROM TbBed t WHERE t.createDate = :createDate"),
    @NamedQuery(name = "TbBed.findByUpdateDate", query = "SELECT t FROM TbBed t WHERE t.updateDate = :updateDate"),
    @NamedQuery(name = "TbBed.findByStatus", query = "SELECT t FROM TbBed t WHERE t.status = :status")})
public class TbBed implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
//    @Basic(optional = false)
//    @NotNull
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "BedID")
    private Integer bedID;
    @Size(max = 30)
    @Column(name = "BedName")
    private String bedName;
    @Column(name = "IsSubbed")
    private Boolean isSubbed;
    @Column(name = "IsEnable")
    private Boolean isEnable;
    @Column(name = "CreateDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "UpdateDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @Size(max = 20)
    @Column(name = "Status")
    private String status;
    @JoinColumn(name = "ServiceID", referencedColumnName = "ServiceID")
    @ManyToOne
    private TbService serviceID;
    @JoinColumn(name = "Fa_ID", referencedColumnName = "Fa_ID")
    @ManyToOne
    private TbFaculty faID;
    @JoinColumn(name = "UpdateByWhom", referencedColumnName = "EmployeeID")
    @ManyToOne
    private TbEmployee updateByWhom;
    @JoinColumn(name = "CreateByWhom", referencedColumnName = "EmployeeID")
    @ManyToOne
    private TbEmployee createByWhom;

    public TbBed() {
    }

    public TbBed(Integer bedID) {
        this.bedID = bedID;
    }

    public Integer getBedID() {
        return bedID;
    }

    public void setBedID(Integer bedID) {
        this.bedID = bedID;
    }

    public String getBedName() {
        return bedName;
    }

    public void setBedName(String bedName) {
        this.bedName = bedName;
    }

    public Boolean getIsSubbed() {
        return isSubbed;
    }

    public void setIsSubbed(Boolean isSubbed) {
        this.isSubbed = isSubbed;
    }

    public Boolean getIsEnable() {
        return isEnable;
    }

    public void setIsEnable(Boolean isEnable) {
        this.isEnable = isEnable;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public TbService getServiceID() {
        return serviceID;
    }

    public void setServiceID(TbService serviceID) {
        this.serviceID = serviceID;
    }

    public TbFaculty getFaID() {
        return faID;
    }

    public void setFaID(TbFaculty faID) {
        this.faID = faID;
    }

    public TbEmployee getUpdateByWhom() {
        return updateByWhom;
    }

    public void setUpdateByWhom(TbEmployee updateByWhom) {
        this.updateByWhom = updateByWhom;
    }

    public TbEmployee getCreateByWhom() {
        return createByWhom;
    }

    public void setCreateByWhom(TbEmployee createByWhom) {
        this.createByWhom = createByWhom;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bedID != null ? bedID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbBed)) {
            return false;
        }
        TbBed other = (TbBed) object;
        if ((this.bedID == null && other.bedID != null) || (this.bedID != null && !this.bedID.equals(other.bedID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "project.ett.TbBed[ bedID=" + bedID + " ]";
    }
    
}
