/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project.ett;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DaySLeePer
 */
@Entity
@Table(name = "tbLevelSurgery")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbLevelSurgery.findAll", query = "SELECT t FROM TbLevelSurgery t"),
    @NamedQuery(name = "TbLevelSurgery.findByLevelID", query = "SELECT t FROM TbLevelSurgery t WHERE t.levelID = :levelID"),
    @NamedQuery(name = "TbLevelSurgery.findByLevelName", query = "SELECT t FROM TbLevelSurgery t WHERE t.levelName = :levelName"),
    @NamedQuery(name = "TbLevelSurgery.findByLevelDecription", query = "SELECT t FROM TbLevelSurgery t WHERE t.levelDecription = :levelDecription"),
    @NamedQuery(name = "TbLevelSurgery.findByStatusLevel", query = "SELECT t FROM TbLevelSurgery t WHERE t.statusLevel = :statusLevel"),
    @NamedQuery(name = "TbLevelSurgery.findByCreateDate", query = "SELECT t FROM TbLevelSurgery t WHERE t.createDate = :createDate")})
public class TbLevelSurgery implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "LevelID")
    private Integer levelID;
    @Size(max = 50)
    @Column(name = "LevelName")
    private String levelName;
    @Size(max = 250)
    @Column(name = "LevelDecription")
    private String levelDecription;
    @Column(name = "StatusLevel")
    private Boolean statusLevel;
    @Column(name = "CreateDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @JoinColumn(name = "UserID", referencedColumnName = "EmployeeID")
    @ManyToOne
    private TbEmployee userID;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "levelSurgery")
    private Collection<TbSurgeryService> tbSurgeryServiceCollection;

    public TbLevelSurgery() {
    }

    public TbLevelSurgery(Integer levelID, String levelName, String levelDecription) {
        this.levelID = levelID;
        this.levelName = levelName;
        this.levelDecription = levelDecription;
    }

    public TbLevelSurgery(String levelName, String levelDecription) {
        this.levelName = levelName;
        this.levelDecription = levelDecription;
    }
    
    public TbLevelSurgery(Integer levelID) {
        this.levelID = levelID;
    }

    public Integer getLevelID() {
        return levelID;
    }

    public void setLevelID(Integer levelID) {
        this.levelID = levelID;
    }

    public String getLevelName() {
        return levelName;
    }

    public void setLevelName(String levelName) {
        this.levelName = levelName;
    }

    public String getLevelDecription() {
        return levelDecription;
    }

    public void setLevelDecription(String levelDecription) {
        this.levelDecription = levelDecription;
    }

    public Boolean getStatusLevel() {
        return statusLevel;
    }

    public void setStatusLevel(Boolean statusLevel) {
        this.statusLevel = statusLevel;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public TbEmployee getUserID() {
        return userID;
    }

    public void setUserID(TbEmployee userID) {
        this.userID = userID;
    }

    @XmlTransient
    public Collection<TbSurgeryService> getTbSurgeryServiceCollection() {
        return tbSurgeryServiceCollection;
    }

    public void setTbSurgeryServiceCollection(Collection<TbSurgeryService> tbSurgeryServiceCollection) {
        this.tbSurgeryServiceCollection = tbSurgeryServiceCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (levelID != null ? levelID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbLevelSurgery)) {
            return false;
        }
        TbLevelSurgery other = (TbLevelSurgery) object;
        if ((this.levelID == null && other.levelID != null) || (this.levelID != null && !this.levelID.equals(other.levelID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "project.ett.TbLevelSurgery[ levelID=" + levelID + " ]";
    }
    
}
