/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project.ett;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DaySLeePer
 */
@Entity
@Table(name = "tbPrescriptionDetail")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbPrescriptionDetail.findAll", query = "SELECT t FROM TbPrescriptionDetail t"),
    @NamedQuery(name = "TbPrescriptionDetail.findByPrescriptionDetailID", query = "SELECT t FROM TbPrescriptionDetail t WHERE t.prescriptionDetailID = :prescriptionDetailID"),
    @NamedQuery(name = "TbPrescriptionDetail.findByMorning", query = "SELECT t FROM TbPrescriptionDetail t WHERE t.morning = :morning"),
    @NamedQuery(name = "TbPrescriptionDetail.findByAfternoon", query = "SELECT t FROM TbPrescriptionDetail t WHERE t.afternoon = :afternoon"),
    @NamedQuery(name = "TbPrescriptionDetail.findByNight", query = "SELECT t FROM TbPrescriptionDetail t WHERE t.night = :night"),
    @NamedQuery(name = "TbPrescriptionDetail.findByQuantity", query = "SELECT t FROM TbPrescriptionDetail t WHERE t.quantity = :quantity"),
    @NamedQuery(name = "TbPrescriptionDetail.findByPrice", query = "SELECT t FROM TbPrescriptionDetail t WHERE t.price = :price"),
    @NamedQuery(name = "TbPrescriptionDetail.findByDateCreate", query = "SELECT t FROM TbPrescriptionDetail t WHERE t.dateCreate = :dateCreate"),
    @NamedQuery(name = "TbPrescriptionDetail.findByUpDateTime", query = "SELECT t FROM TbPrescriptionDetail t WHERE t.upDateTime = :upDateTime")})
public class TbPrescriptionDetail implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "PrescriptionDetailID")
    private String prescriptionDetailID;
    @Column(name = "Morning")
    private Integer morning;
    @Column(name = "Afternoon")
    private Integer afternoon;
    @Column(name = "Night")
    private Integer night;
    @Column(name = "Quantity")
    private Integer quantity;
    @Column(name = "Price")
    private Double price;
    @Column(name = "DateCreate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreate;
    @Column(name = "UpDateTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date upDateTime;
    @JoinColumn(name = "PrescriptionID", referencedColumnName = "PrescriptionID")
    @ManyToOne
    private TbPrescription prescriptionID;
    @JoinColumn(name = "MedicineID", referencedColumnName = "ItemCode")
    @ManyToOne
    private TbItemCodeManagement medicineID;
    @JoinColumn(name = "UpEmployeeID", referencedColumnName = "EmployeeID")
    @ManyToOne
    private TbEmployee upEmployeeID;

    public TbPrescriptionDetail() {
    }

    public TbPrescriptionDetail(String prescriptionDetailID) {
        this.prescriptionDetailID = prescriptionDetailID;
    }

    public String getPrescriptionDetailID() {
        return prescriptionDetailID;
    }

    public void setPrescriptionDetailID(String prescriptionDetailID) {
        this.prescriptionDetailID = prescriptionDetailID;
    }

    public Integer getMorning() {
        return morning;
    }

    public void setMorning(Integer morning) {
        this.morning = morning;
    }

    public Integer getAfternoon() {
        return afternoon;
    }

    public void setAfternoon(Integer afternoon) {
        this.afternoon = afternoon;
    }

    public Integer getNight() {
        return night;
    }

    public void setNight(Integer night) {
        this.night = night;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public Date getUpDateTime() {
        return upDateTime;
    }

    public void setUpDateTime(Date upDateTime) {
        this.upDateTime = upDateTime;
    }

    public TbPrescription getPrescriptionID() {
        return prescriptionID;
    }

    public void setPrescriptionID(TbPrescription prescriptionID) {
        this.prescriptionID = prescriptionID;
    }

    public TbItemCodeManagement getMedicineID() {
        return medicineID;
    }

    public void setMedicineID(TbItemCodeManagement medicineID) {
        this.medicineID = medicineID;
    }

    public TbEmployee getUpEmployeeID() {
        return upEmployeeID;
    }

    public void setUpEmployeeID(TbEmployee upEmployeeID) {
        this.upEmployeeID = upEmployeeID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (prescriptionDetailID != null ? prescriptionDetailID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbPrescriptionDetail)) {
            return false;
        }
        TbPrescriptionDetail other = (TbPrescriptionDetail) object;
        if ((this.prescriptionDetailID == null && other.prescriptionDetailID != null) || (this.prescriptionDetailID != null && !this.prescriptionDetailID.equals(other.prescriptionDetailID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "project.ett.TbPrescriptionDetail[ prescriptionDetailID=" + prescriptionDetailID + " ]";
    }
    
}
