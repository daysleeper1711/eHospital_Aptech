/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project.ett;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DaySLeePer
 */
@Entity
@Table(name = "tbFacultyChangementHistory")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbFacultyChangementHistory.findAll", query = "SELECT t FROM TbFacultyChangementHistory t"),
    @NamedQuery(name = "TbFacultyChangementHistory.findByFchId", query = "SELECT t FROM TbFacultyChangementHistory t WHERE t.fchId = :fchId"),
    @NamedQuery(name = "TbFacultyChangementHistory.findByBedName", query = "SELECT t FROM TbFacultyChangementHistory t WHERE t.bedName = :bedName"),
    @NamedQuery(name = "TbFacultyChangementHistory.findByFromFaculty", query = "SELECT t FROM TbFacultyChangementHistory t WHERE t.fromFaculty = :fromFaculty"),
    @NamedQuery(name = "TbFacultyChangementHistory.findByToFaculty", query = "SELECT t FROM TbFacultyChangementHistory t WHERE t.toFaculty = :toFaculty"),
    @NamedQuery(name = "TbFacultyChangementHistory.findByDateIn", query = "SELECT t FROM TbFacultyChangementHistory t WHERE t.dateIn = :dateIn"),
    @NamedQuery(name = "TbFacultyChangementHistory.findByDateOut", query = "SELECT t FROM TbFacultyChangementHistory t WHERE t.dateOut = :dateOut"),
    @NamedQuery(name = "TbFacultyChangementHistory.findByUpdateDate", query = "SELECT t FROM TbFacultyChangementHistory t WHERE t.updateDate = :updateDate"),
    @NamedQuery(name = "TbFacultyChangementHistory.findByReasonToChange", query = "SELECT t FROM TbFacultyChangementHistory t WHERE t.reasonToChange = :reasonToChange"),
    @NamedQuery(name = "TbFacultyChangementHistory.findByStatus", query = "SELECT t FROM TbFacultyChangementHistory t WHERE t.status = :status")})
public class TbFacultyChangementHistory implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "FCH_ID")
    private Integer fchId;
    @Size(max = 30)
    @Column(name = "BedName")
    private String bedName;
    @Size(max = 20)
    @Column(name = "FromFaculty")
    private String fromFaculty;
    @Size(max = 20)
    @Column(name = "ToFaculty")
    private String toFaculty;
    @Column(name = "DateIn")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateIn;
    @Column(name = "DateOut")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateOut;
    @Column(name = "UpdateDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @Size(max = 100)
    @Column(name = "ReasonToChange")
    private String reasonToChange;
    @Size(max = 30)
    @Column(name = "Status")
    private String status;
    @JoinColumn(name = "MedRecordID", referencedColumnName = "MedRecordID")
    @ManyToOne
    private TbMedicalRecord medRecordID;
    @JoinColumn(name = "UpdateByWhom", referencedColumnName = "EmployeeID")
    @ManyToOne
    private TbEmployee updateByWhom;
    @JoinColumn(name = "AcceptOutByWhom", referencedColumnName = "EmployeeID")
    @ManyToOne
    private TbEmployee acceptOutByWhom;
    @JoinColumn(name = "AcceptInBywhom", referencedColumnName = "EmployeeID")
    @ManyToOne
    private TbEmployee acceptInBywhom;

    public TbFacultyChangementHistory() {
    }

    public TbFacultyChangementHistory(Integer fchId) {
        this.fchId = fchId;
    }

    public Integer getFchId() {
        return fchId;
    }

    public void setFchId(Integer fchId) {
        this.fchId = fchId;
    }

    public String getBedName() {
        return bedName;
    }

    public void setBedName(String bedName) {
        this.bedName = bedName;
    }

    public String getFromFaculty() {
        return fromFaculty;
    }

    public void setFromFaculty(String fromFaculty) {
        this.fromFaculty = fromFaculty;
    }

    public String getToFaculty() {
        return toFaculty;
    }

    public void setToFaculty(String toFaculty) {
        this.toFaculty = toFaculty;
    }

    public Date getDateIn() {
        return dateIn;
    }

    public void setDateIn(Date dateIn) {
        this.dateIn = dateIn;
    }

    public Date getDateOut() {
        return dateOut;
    }

    public void setDateOut(Date dateOut) {
        this.dateOut = dateOut;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getReasonToChange() {
        return reasonToChange;
    }

    public void setReasonToChange(String reasonToChange) {
        this.reasonToChange = reasonToChange;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public TbMedicalRecord getMedRecordID() {
        return medRecordID;
    }

    public void setMedRecordID(TbMedicalRecord medRecordID) {
        this.medRecordID = medRecordID;
    }

    public TbEmployee getUpdateByWhom() {
        return updateByWhom;
    }

    public void setUpdateByWhom(TbEmployee updateByWhom) {
        this.updateByWhom = updateByWhom;
    }

    public TbEmployee getAcceptOutByWhom() {
        return acceptOutByWhom;
    }

    public void setAcceptOutByWhom(TbEmployee acceptOutByWhom) {
        this.acceptOutByWhom = acceptOutByWhom;
    }

    public TbEmployee getAcceptInBywhom() {
        return acceptInBywhom;
    }

    public void setAcceptInBywhom(TbEmployee acceptInBywhom) {
        this.acceptInBywhom = acceptInBywhom;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (fchId != null ? fchId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbFacultyChangementHistory)) {
            return false;
        }
        TbFacultyChangementHistory other = (TbFacultyChangementHistory) object;
        if ((this.fchId == null && other.fchId != null) || (this.fchId != null && !this.fchId.equals(other.fchId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "project.ett.TbFacultyChangementHistory[ fchId=" + fchId + " ]";
    }
    
}
