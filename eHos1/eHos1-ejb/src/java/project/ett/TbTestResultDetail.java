/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project.ett;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DaySLeePer
 */
@Entity
@Table(name = "tbTestResultDetail")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbTestResultDetail.findAll", query = "SELECT t FROM TbTestResultDetail t"),
    @NamedQuery(name = "TbTestResultDetail.findByTestResultDetailID", query = "SELECT t FROM TbTestResultDetail t WHERE t.testResultDetailID = :testResultDetailID"),
    @NamedQuery(name = "TbTestResultDetail.findByResults", query = "SELECT t FROM TbTestResultDetail t WHERE t.results = :results"),
    @NamedQuery(name = "TbTestResultDetail.findByCreateDate", query = "SELECT t FROM TbTestResultDetail t WHERE t.createDate = :createDate"),
    @NamedQuery(name = "TbTestResultDetail.findByUpdateDate", query = "SELECT t FROM TbTestResultDetail t WHERE t.updateDate = :updateDate")})
public class TbTestResultDetail implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "TestResultDetailID")
    private Integer testResultDetailID;
    @Size(max = 20)
    @Column(name = "Results")
    private String results;
    @Column(name = "CreateDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "UpdateDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @JoinColumn(name = "TestResultID", referencedColumnName = "TestResultID")
    @ManyToOne
    private TbTestResult testResultID;
    @JoinColumn(name = "UpdateEmp", referencedColumnName = "EmployeeID")
    @ManyToOne
    private TbEmployee updateEmp;
    @JoinColumn(name = "Creator", referencedColumnName = "EmployeeID")
    @ManyToOne
    private TbEmployee creator;
    @JoinColumn(name = "ReqDetailID", referencedColumnName = "TestReqDetailID")
    @ManyToOne
    private TbTestReqDetail reqDetailID;

    public TbTestResultDetail() {
    }

    public TbTestResultDetail(Integer testResultDetailID) {
        this.testResultDetailID = testResultDetailID;
    }

    public Integer getTestResultDetailID() {
        return testResultDetailID;
    }

    public void setTestResultDetailID(Integer testResultDetailID) {
        this.testResultDetailID = testResultDetailID;
    }

    public String getResults() {
        return results;
    }

    public void setResults(String results) {
        this.results = results;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public TbTestResult getTestResultID() {
        return testResultID;
    }

    public void setTestResultID(TbTestResult testResultID) {
        this.testResultID = testResultID;
    }

    public TbEmployee getUpdateEmp() {
        return updateEmp;
    }

    public void setUpdateEmp(TbEmployee updateEmp) {
        this.updateEmp = updateEmp;
    }

    public TbEmployee getCreator() {
        return creator;
    }

    public void setCreator(TbEmployee creator) {
        this.creator = creator;
    }

    public TbTestReqDetail getReqDetailID() {
        return reqDetailID;
    }

    public void setReqDetailID(TbTestReqDetail reqDetailID) {
        this.reqDetailID = reqDetailID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (testResultDetailID != null ? testResultDetailID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbTestResultDetail)) {
            return false;
        }
        TbTestResultDetail other = (TbTestResultDetail) object;
        if ((this.testResultDetailID == null && other.testResultDetailID != null) || (this.testResultDetailID != null && !this.testResultDetailID.equals(other.testResultDetailID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "project.ett.TbTestResultDetail[ testResultDetailID=" + testResultDetailID + " ]";
    }
    
}
