/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project.ett;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DaySLeePer
 */
@Entity
@Table(name = "tbSupplier")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbSupplier.findAll", query = "SELECT t FROM TbSupplier t"),
    @NamedQuery(name = "TbSupplier.findBySupplierID", query = "SELECT t FROM TbSupplier t WHERE t.supplierID = :supplierID"),
    @NamedQuery(name = "TbSupplier.findBySupplierName", query = "SELECT t FROM TbSupplier t WHERE t.supplierName = :supplierName"),
    @NamedQuery(name = "TbSupplier.findByUStatus", query = "SELECT t FROM TbSupplier t WHERE t.uStatus = :uStatus")})
public class TbSupplier implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "SupplierID")
    private String supplierID;
    @Size(max = 100)
    @Column(name = "SupplierName")
    private String supplierName;
    @Size(max = 2)
    @Column(name = "U_Status")
    private String uStatus;
    @OneToMany(mappedBy = "supplierID",cascade = CascadeType.ALL)
    private Collection<TbImDurgsSupplies> tbImDurgsSuppliesCollection;

    public TbSupplier() {
    }

    public TbSupplier(String supplierID) {
        this.supplierID = supplierID;
    }

    public String getSupplierID() {
        return supplierID;
    }

    public void setSupplierID(String supplierID) {
        this.supplierID = supplierID;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getUStatus() {
        return uStatus;
    }

    public void setUStatus(String uStatus) {
        this.uStatus = uStatus;
    }

    @XmlTransient
    public Collection<TbImDurgsSupplies> getTbImDurgsSuppliesCollection() {
        return tbImDurgsSuppliesCollection;
    }

    public void setTbImDurgsSuppliesCollection(Collection<TbImDurgsSupplies> tbImDurgsSuppliesCollection) {
        this.tbImDurgsSuppliesCollection = tbImDurgsSuppliesCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (supplierID != null ? supplierID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbSupplier)) {
            return false;
        }
        TbSupplier other = (TbSupplier) object;
        if ((this.supplierID == null && other.supplierID != null) || (this.supplierID != null && !this.supplierID.equals(other.supplierID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "project.ett.TbSupplier[ supplierID=" + supplierID + " ]";
    }
    
}
