/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project.ett;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DaySLeePer
 */
@Entity
@Table(name = "tbInsurane")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbInsurane.findAll", query = "SELECT t FROM TbInsurane t"),
    @NamedQuery(name = "TbInsurane.findByInsuranceID", query = "SELECT t FROM TbInsurane t WHERE t.insuranceID = :insuranceID"),
    @NamedQuery(name = "TbInsurane.findByName", query = "SELECT t FROM TbInsurane t WHERE t.name = :name"),
    @NamedQuery(name = "TbInsurane.findByDateStart", query = "SELECT t FROM TbInsurane t WHERE t.dateStart = :dateStart"),
    @NamedQuery(name = "TbInsurane.findByDateExpire", query = "SELECT t FROM TbInsurane t WHERE t.dateExpire = :dateExpire"),
    @NamedQuery(name = "TbInsurane.findByRegPlace", query = "SELECT t FROM TbInsurane t WHERE t.regPlace = :regPlace"),
    @NamedQuery(name = "TbInsurane.findByStatusIns", query = "SELECT t FROM TbInsurane t WHERE t.statusIns = :statusIns")})
public class TbInsurane implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "InsuranceID")
    private String insuranceID;
    @Size(max = 50)
    @Column(name = "Name")
    private String name;
    @Column(name = "DateStart")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateStart;
    @Column(name = "DateExpire")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateExpire;
    @Size(max = 50)
    @Column(name = "RegPlace")
    private String regPlace;
    @Column(name = "StatusIns")
    private Boolean statusIns;
    @JoinColumn(name = "PatientID", referencedColumnName = "PatID")
    @ManyToOne
    private TbPatient patientID;
    @JoinColumn(name = "InsuranceTypeID", referencedColumnName = "InsuranceTypeID")
    @ManyToOne
    private InsuranceType insuranceTypeID;

    public TbInsurane() {
    }

    public TbInsurane(String insuranceID) {
        this.insuranceID = insuranceID;
    }

    public String getInsuranceID() {
        return insuranceID;
    }

    public void setInsuranceID(String insuranceID) {
        this.insuranceID = insuranceID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateExpire() {
        return dateExpire;
    }

    public void setDateExpire(Date dateExpire) {
        this.dateExpire = dateExpire;
    }

    public String getRegPlace() {
        return regPlace;
    }

    public void setRegPlace(String regPlace) {
        this.regPlace = regPlace;
    }

    public Boolean getStatusIns() {
        return statusIns;
    }

    public void setStatusIns(Boolean statusIns) {
        this.statusIns = statusIns;
    }

    public TbPatient getPatientID() {
        return patientID;
    }

    public void setPatientID(TbPatient patientID) {
        this.patientID = patientID;
    }

    public InsuranceType getInsuranceTypeID() {
        return insuranceTypeID;
    }

    public void setInsuranceTypeID(InsuranceType insuranceTypeID) {
        this.insuranceTypeID = insuranceTypeID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (insuranceID != null ? insuranceID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbInsurane)) {
            return false;
        }
        TbInsurane other = (TbInsurane) object;
        if ((this.insuranceID == null && other.insuranceID != null) || (this.insuranceID != null && !this.insuranceID.equals(other.insuranceID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "project.ett.TbInsurane[ insuranceID=" + insuranceID + " ]";
    }
    
}
