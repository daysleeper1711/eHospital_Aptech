/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project.ett;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DaySLeePer
 */
@Entity
@Table(name = "tbEmployee")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbEmployee.findAll", query = "SELECT t FROM TbEmployee t"),
    @NamedQuery(name = "TbEmployee.findByEmployeeID", query = "SELECT t FROM TbEmployee t WHERE t.employeeID = :employeeID"),
    @NamedQuery(name = "TbEmployee.findByFullname", query = "SELECT t FROM TbEmployee t WHERE t.fullname = :fullname"),
    @NamedQuery(name = "TbEmployee.findByPassword", query = "SELECT t FROM TbEmployee t WHERE t.password = :password"),
    @NamedQuery(name = "TbEmployee.findByPhone", query = "SELECT t FROM TbEmployee t WHERE t.phone = :phone"),
    @NamedQuery(name = "TbEmployee.findByAddress", query = "SELECT t FROM TbEmployee t WHERE t.address = :address"),
    @NamedQuery(name = "TbEmployee.findByPosition", query = "SELECT t FROM TbEmployee t WHERE t.position = :position"),
    @NamedQuery(name = "TbEmployee.findByDob", query = "SELECT t FROM TbEmployee t WHERE t.dob = :dob"),
    @NamedQuery(name = "TbEmployee.findByEmail", query = "SELECT t FROM TbEmployee t WHERE t.email = :email"),
    @NamedQuery(name = "TbEmployee.findByStatusEmp", query = "SELECT t FROM TbEmployee t WHERE t.statusEmp = :statusEmp")})
public class TbEmployee implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "EmployeeID")
    private String employeeID;
    @Size(max = 50)
    @Column(name = "Fullname")
    private String fullname;
    @Size(max = 50)
    @Column(name = "Password")
    private String password;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Size(max = 12)
    @Column(name = "Phone")
    private String phone;
    @Size(max = 50)
    @Column(name = "Address")
    private String address;
    @Size(max = 50)
    @Column(name = "Position")
    private String position;
    @Column(name = "DOB")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dob;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 50)
    @Column(name = "Email")
    private String email;
    @Column(name = "StatusEmp")
    private Boolean statusEmp;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "updateByWhom")
    private Collection<TbBed> tbBedCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createByWhom")
    private Collection<TbBed> tbBedCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "updateByWhom")
    private Collection<TbFacultyChangementHistory> tbFacultyChangementHistoryCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "acceptOutByWhom")
    private Collection<TbFacultyChangementHistory> tbFacultyChangementHistoryCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "acceptInBywhom")
    private Collection<TbFacultyChangementHistory> tbFacultyChangementHistoryCollection2;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "employeeID")
    private Collection<TbKQKB> tbKQKBCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "conclusionByWhom")
    private Collection<TbMedicalRecord> tbMedicalRecordCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "diagnosisDoc")
    private Collection<TbMedicalRecord> tbMedicalRecordCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createByWhom")
    private Collection<TbMedicalRecord> tbMedicalRecordCollection2;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "upEmployeeID")
    private Collection<TbMedicineSuppliesDetail> tbMedicineSuppliesDetailCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userID")
    private Collection<TbScheduledSurgery> tbScheduledSurgeryCollection;
    @JoinColumn(name = "FacultyID", referencedColumnName = "Fa_ID")
    @ManyToOne
    private TbFaculty facultyID;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "upEmployeeID")
    private Collection<TbPrescriptionDetail> tbPrescriptionDetailCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "receivedByWhom")
    private Collection<TbDepositPayment> tbDepositPaymentCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userID")
    private Collection<TbLevelSurgery> tbLevelSurgeryCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userID")
    private Collection<TbOperatorDoctorDetail> tbOperatorDoctorDetailCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "doctorID")
    private Collection<TbOperatorDoctorDetail> tbOperatorDoctorDetailCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userID")
    private Collection<TbPositionDoctor> tbPositionDoctorCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userID")
    private Collection<TbConsulationDoctorDetail> tbConsulationDoctorDetailCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "doctorID")
    private Collection<TbConsulationDoctorDetail> tbConsulationDoctorDetailCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "creator")
    private Collection<TbOutPatient> tbOutPatientCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "upEmployeeID")
    private Collection<TbMedicineSupplies> tbMedicineSuppliesCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "employeeID")
    private Collection<TbMedicineSupplies> tbMedicineSuppliesCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userID")
    private Collection<TbSurgeryRecord> tbSurgeryRecordCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "receivedByWhom")
    private Collection<TbTreatmentFeeEnumeration> tbTreatmentFeeEnumerationCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userID")
    private Collection<TbSurgeryService> tbSurgeryServiceCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "updateEmp")
    private Collection<TbTestReqDetail> tbTestReqDetailCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "creator")
    private Collection<TbTestReqDetail> tbTestReqDetailCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userID")
    private Collection<TbDrugsDetail> tbDrugsDetailCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userID")
    private Collection<TbConsulationSurgery> tbConsulationSurgeryCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userID")
    private Collection<TbPositionConsulation> tbPositionConsulationCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "updateEmp")
    private Collection<TbTestRequest> tbTestRequestCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "appointedDoc")
    private Collection<TbTestRequest> tbTestRequestCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "upEmployeeID")
    private Collection<TbPrescription> tbPrescriptionCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "employeeID")
    private Collection<TbPrescription> tbPrescriptionCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userID")
    private Collection<TbSuggestSurgery> tbSuggestSurgeryCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "doctorID")
    private Collection<TbSuggestSurgery> tbSuggestSurgeryCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "updateByWhom")
    private Collection<TbPatient> tbPatientCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "regByWhom")
    private Collection<TbPatient> tbPatientCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "updateEmp")
    private Collection<TbServiceGroup> tbServiceGroupCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "creator")
    private Collection<TbServiceGroup> tbServiceGroupCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "updateEmp")
    private Collection<TbTestResultDetail> tbTestResultDetailCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "creator")
    private Collection<TbTestResultDetail> tbTestResultDetailCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userID")
    private Collection<TbCategory> tbCategoryCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "updateEmp")
    private Collection<TbService> tbServiceCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "creator")
    private Collection<TbService> tbServiceCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userID")
    private Collection<TbSuppliesDetail> tbSuppliesDetailCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "updateEmp")
    private Collection<TbServicePrice> tbServicePriceCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "creator")
    private Collection<TbServicePrice> tbServicePriceCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "updateEmp")
    private Collection<TbStandardValue> tbStandardValueCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "creator")
    private Collection<TbStandardValue> tbStandardValueCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "concludeDoc")
    private Collection<TbTestResult> tbTestResultCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "testDoc")
    private Collection<TbTestResult> tbTestResultCollection1;

    public TbEmployee() {
    }

    public TbEmployee(String employeeID) {
        this.employeeID = employeeID;
    }

    public String getEmployeeID() {
        return employeeID;
    }

    public void setEmployeeID(String employeeID) {
        this.employeeID = employeeID;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getStatusEmp() {
        return statusEmp;
    }

    public void setStatusEmp(Boolean statusEmp) {
        this.statusEmp = statusEmp;
    }

    @XmlTransient
    public Collection<TbBed> getTbBedCollection() {
        return tbBedCollection;
    }

    public void setTbBedCollection(Collection<TbBed> tbBedCollection) {
        this.tbBedCollection = tbBedCollection;
    }

    @XmlTransient
    public Collection<TbBed> getTbBedCollection1() {
        return tbBedCollection1;
    }

    public void setTbBedCollection1(Collection<TbBed> tbBedCollection1) {
        this.tbBedCollection1 = tbBedCollection1;
    }

    @XmlTransient
    public Collection<TbFacultyChangementHistory> getTbFacultyChangementHistoryCollection() {
        return tbFacultyChangementHistoryCollection;
    }

    public void setTbFacultyChangementHistoryCollection(Collection<TbFacultyChangementHistory> tbFacultyChangementHistoryCollection) {
        this.tbFacultyChangementHistoryCollection = tbFacultyChangementHistoryCollection;
    }

    @XmlTransient
    public Collection<TbFacultyChangementHistory> getTbFacultyChangementHistoryCollection1() {
        return tbFacultyChangementHistoryCollection1;
    }

    public void setTbFacultyChangementHistoryCollection1(Collection<TbFacultyChangementHistory> tbFacultyChangementHistoryCollection1) {
        this.tbFacultyChangementHistoryCollection1 = tbFacultyChangementHistoryCollection1;
    }

    @XmlTransient
    public Collection<TbFacultyChangementHistory> getTbFacultyChangementHistoryCollection2() {
        return tbFacultyChangementHistoryCollection2;
    }

    public void setTbFacultyChangementHistoryCollection2(Collection<TbFacultyChangementHistory> tbFacultyChangementHistoryCollection2) {
        this.tbFacultyChangementHistoryCollection2 = tbFacultyChangementHistoryCollection2;
    }

    @XmlTransient
    public Collection<TbKQKB> getTbKQKBCollection() {
        return tbKQKBCollection;
    }

    public void setTbKQKBCollection(Collection<TbKQKB> tbKQKBCollection) {
        this.tbKQKBCollection = tbKQKBCollection;
    }

    @XmlTransient
    public Collection<TbMedicalRecord> getTbMedicalRecordCollection() {
        return tbMedicalRecordCollection;
    }

    public void setTbMedicalRecordCollection(Collection<TbMedicalRecord> tbMedicalRecordCollection) {
        this.tbMedicalRecordCollection = tbMedicalRecordCollection;
    }

    @XmlTransient
    public Collection<TbMedicalRecord> getTbMedicalRecordCollection1() {
        return tbMedicalRecordCollection1;
    }

    public void setTbMedicalRecordCollection1(Collection<TbMedicalRecord> tbMedicalRecordCollection1) {
        this.tbMedicalRecordCollection1 = tbMedicalRecordCollection1;
    }

    @XmlTransient
    public Collection<TbMedicalRecord> getTbMedicalRecordCollection2() {
        return tbMedicalRecordCollection2;
    }

    public void setTbMedicalRecordCollection2(Collection<TbMedicalRecord> tbMedicalRecordCollection2) {
        this.tbMedicalRecordCollection2 = tbMedicalRecordCollection2;
    }

    @XmlTransient
    public Collection<TbMedicineSuppliesDetail> getTbMedicineSuppliesDetailCollection() {
        return tbMedicineSuppliesDetailCollection;
    }

    public void setTbMedicineSuppliesDetailCollection(Collection<TbMedicineSuppliesDetail> tbMedicineSuppliesDetailCollection) {
        this.tbMedicineSuppliesDetailCollection = tbMedicineSuppliesDetailCollection;
    }

    @XmlTransient
    public Collection<TbScheduledSurgery> getTbScheduledSurgeryCollection() {
        return tbScheduledSurgeryCollection;
    }

    public void setTbScheduledSurgeryCollection(Collection<TbScheduledSurgery> tbScheduledSurgeryCollection) {
        this.tbScheduledSurgeryCollection = tbScheduledSurgeryCollection;
    }

    public TbFaculty getFacultyID() {
        return facultyID;
    }

    public void setFacultyID(TbFaculty facultyID) {
        this.facultyID = facultyID;
    }

    @XmlTransient
    public Collection<TbPrescriptionDetail> getTbPrescriptionDetailCollection() {
        return tbPrescriptionDetailCollection;
    }

    public void setTbPrescriptionDetailCollection(Collection<TbPrescriptionDetail> tbPrescriptionDetailCollection) {
        this.tbPrescriptionDetailCollection = tbPrescriptionDetailCollection;
    }

    @XmlTransient
    public Collection<TbDepositPayment> getTbDepositPaymentCollection() {
        return tbDepositPaymentCollection;
    }

    public void setTbDepositPaymentCollection(Collection<TbDepositPayment> tbDepositPaymentCollection) {
        this.tbDepositPaymentCollection = tbDepositPaymentCollection;
    }

    @XmlTransient
    public Collection<TbLevelSurgery> getTbLevelSurgeryCollection() {
        return tbLevelSurgeryCollection;
    }

    public void setTbLevelSurgeryCollection(Collection<TbLevelSurgery> tbLevelSurgeryCollection) {
        this.tbLevelSurgeryCollection = tbLevelSurgeryCollection;
    }

    @XmlTransient
    public Collection<TbOperatorDoctorDetail> getTbOperatorDoctorDetailCollection() {
        return tbOperatorDoctorDetailCollection;
    }

    public void setTbOperatorDoctorDetailCollection(Collection<TbOperatorDoctorDetail> tbOperatorDoctorDetailCollection) {
        this.tbOperatorDoctorDetailCollection = tbOperatorDoctorDetailCollection;
    }

    @XmlTransient
    public Collection<TbOperatorDoctorDetail> getTbOperatorDoctorDetailCollection1() {
        return tbOperatorDoctorDetailCollection1;
    }

    public void setTbOperatorDoctorDetailCollection1(Collection<TbOperatorDoctorDetail> tbOperatorDoctorDetailCollection1) {
        this.tbOperatorDoctorDetailCollection1 = tbOperatorDoctorDetailCollection1;
    }

    @XmlTransient
    public Collection<TbPositionDoctor> getTbPositionDoctorCollection() {
        return tbPositionDoctorCollection;
    }

    public void setTbPositionDoctorCollection(Collection<TbPositionDoctor> tbPositionDoctorCollection) {
        this.tbPositionDoctorCollection = tbPositionDoctorCollection;
    }

    @XmlTransient
    public Collection<TbConsulationDoctorDetail> getTbConsulationDoctorDetailCollection() {
        return tbConsulationDoctorDetailCollection;
    }

    public void setTbConsulationDoctorDetailCollection(Collection<TbConsulationDoctorDetail> tbConsulationDoctorDetailCollection) {
        this.tbConsulationDoctorDetailCollection = tbConsulationDoctorDetailCollection;
    }

    @XmlTransient
    public Collection<TbConsulationDoctorDetail> getTbConsulationDoctorDetailCollection1() {
        return tbConsulationDoctorDetailCollection1;
    }

    public void setTbConsulationDoctorDetailCollection1(Collection<TbConsulationDoctorDetail> tbConsulationDoctorDetailCollection1) {
        this.tbConsulationDoctorDetailCollection1 = tbConsulationDoctorDetailCollection1;
    }

    @XmlTransient
    public Collection<TbOutPatient> getTbOutPatientCollection() {
        return tbOutPatientCollection;
    }

    public void setTbOutPatientCollection(Collection<TbOutPatient> tbOutPatientCollection) {
        this.tbOutPatientCollection = tbOutPatientCollection;
    }

    @XmlTransient
    public Collection<TbMedicineSupplies> getTbMedicineSuppliesCollection() {
        return tbMedicineSuppliesCollection;
    }

    public void setTbMedicineSuppliesCollection(Collection<TbMedicineSupplies> tbMedicineSuppliesCollection) {
        this.tbMedicineSuppliesCollection = tbMedicineSuppliesCollection;
    }

    @XmlTransient
    public Collection<TbMedicineSupplies> getTbMedicineSuppliesCollection1() {
        return tbMedicineSuppliesCollection1;
    }

    public void setTbMedicineSuppliesCollection1(Collection<TbMedicineSupplies> tbMedicineSuppliesCollection1) {
        this.tbMedicineSuppliesCollection1 = tbMedicineSuppliesCollection1;
    }

    @XmlTransient
    public Collection<TbSurgeryRecord> getTbSurgeryRecordCollection() {
        return tbSurgeryRecordCollection;
    }

    public void setTbSurgeryRecordCollection(Collection<TbSurgeryRecord> tbSurgeryRecordCollection) {
        this.tbSurgeryRecordCollection = tbSurgeryRecordCollection;
    }

    @XmlTransient
    public Collection<TbTreatmentFeeEnumeration> getTbTreatmentFeeEnumerationCollection() {
        return tbTreatmentFeeEnumerationCollection;
    }

    public void setTbTreatmentFeeEnumerationCollection(Collection<TbTreatmentFeeEnumeration> tbTreatmentFeeEnumerationCollection) {
        this.tbTreatmentFeeEnumerationCollection = tbTreatmentFeeEnumerationCollection;
    }

    @XmlTransient
    public Collection<TbSurgeryService> getTbSurgeryServiceCollection() {
        return tbSurgeryServiceCollection;
    }

    public void setTbSurgeryServiceCollection(Collection<TbSurgeryService> tbSurgeryServiceCollection) {
        this.tbSurgeryServiceCollection = tbSurgeryServiceCollection;
    }

    @XmlTransient
    public Collection<TbTestReqDetail> getTbTestReqDetailCollection() {
        return tbTestReqDetailCollection;
    }

    public void setTbTestReqDetailCollection(Collection<TbTestReqDetail> tbTestReqDetailCollection) {
        this.tbTestReqDetailCollection = tbTestReqDetailCollection;
    }

    @XmlTransient
    public Collection<TbTestReqDetail> getTbTestReqDetailCollection1() {
        return tbTestReqDetailCollection1;
    }

    public void setTbTestReqDetailCollection1(Collection<TbTestReqDetail> tbTestReqDetailCollection1) {
        this.tbTestReqDetailCollection1 = tbTestReqDetailCollection1;
    }

    @XmlTransient
    public Collection<TbDrugsDetail> getTbDrugsDetailCollection() {
        return tbDrugsDetailCollection;
    }

    public void setTbDrugsDetailCollection(Collection<TbDrugsDetail> tbDrugsDetailCollection) {
        this.tbDrugsDetailCollection = tbDrugsDetailCollection;
    }

    @XmlTransient
    public Collection<TbConsulationSurgery> getTbConsulationSurgeryCollection() {
        return tbConsulationSurgeryCollection;
    }

    public void setTbConsulationSurgeryCollection(Collection<TbConsulationSurgery> tbConsulationSurgeryCollection) {
        this.tbConsulationSurgeryCollection = tbConsulationSurgeryCollection;
    }

    @XmlTransient
    public Collection<TbPositionConsulation> getTbPositionConsulationCollection() {
        return tbPositionConsulationCollection;
    }

    public void setTbPositionConsulationCollection(Collection<TbPositionConsulation> tbPositionConsulationCollection) {
        this.tbPositionConsulationCollection = tbPositionConsulationCollection;
    }

    @XmlTransient
    public Collection<TbTestRequest> getTbTestRequestCollection() {
        return tbTestRequestCollection;
    }

    public void setTbTestRequestCollection(Collection<TbTestRequest> tbTestRequestCollection) {
        this.tbTestRequestCollection = tbTestRequestCollection;
    }

    @XmlTransient
    public Collection<TbTestRequest> getTbTestRequestCollection1() {
        return tbTestRequestCollection1;
    }

    public void setTbTestRequestCollection1(Collection<TbTestRequest> tbTestRequestCollection1) {
        this.tbTestRequestCollection1 = tbTestRequestCollection1;
    }

    @XmlTransient
    public Collection<TbPrescription> getTbPrescriptionCollection() {
        return tbPrescriptionCollection;
    }

    public void setTbPrescriptionCollection(Collection<TbPrescription> tbPrescriptionCollection) {
        this.tbPrescriptionCollection = tbPrescriptionCollection;
    }

    @XmlTransient
    public Collection<TbPrescription> getTbPrescriptionCollection1() {
        return tbPrescriptionCollection1;
    }

    public void setTbPrescriptionCollection1(Collection<TbPrescription> tbPrescriptionCollection1) {
        this.tbPrescriptionCollection1 = tbPrescriptionCollection1;
    }

    @XmlTransient
    public Collection<TbSuggestSurgery> getTbSuggestSurgeryCollection() {
        return tbSuggestSurgeryCollection;
    }

    public void setTbSuggestSurgeryCollection(Collection<TbSuggestSurgery> tbSuggestSurgeryCollection) {
        this.tbSuggestSurgeryCollection = tbSuggestSurgeryCollection;
    }

    @XmlTransient
    public Collection<TbSuggestSurgery> getTbSuggestSurgeryCollection1() {
        return tbSuggestSurgeryCollection1;
    }

    public void setTbSuggestSurgeryCollection1(Collection<TbSuggestSurgery> tbSuggestSurgeryCollection1) {
        this.tbSuggestSurgeryCollection1 = tbSuggestSurgeryCollection1;
    }

    @XmlTransient
    public Collection<TbPatient> getTbPatientCollection() {
        return tbPatientCollection;
    }

    public void setTbPatientCollection(Collection<TbPatient> tbPatientCollection) {
        this.tbPatientCollection = tbPatientCollection;
    }

    @XmlTransient
    public Collection<TbPatient> getTbPatientCollection1() {
        return tbPatientCollection1;
    }

    public void setTbPatientCollection1(Collection<TbPatient> tbPatientCollection1) {
        this.tbPatientCollection1 = tbPatientCollection1;
    }

    @XmlTransient
    public Collection<TbServiceGroup> getTbServiceGroupCollection() {
        return tbServiceGroupCollection;
    }

    public void setTbServiceGroupCollection(Collection<TbServiceGroup> tbServiceGroupCollection) {
        this.tbServiceGroupCollection = tbServiceGroupCollection;
    }

    @XmlTransient
    public Collection<TbServiceGroup> getTbServiceGroupCollection1() {
        return tbServiceGroupCollection1;
    }

    public void setTbServiceGroupCollection1(Collection<TbServiceGroup> tbServiceGroupCollection1) {
        this.tbServiceGroupCollection1 = tbServiceGroupCollection1;
    }

    @XmlTransient
    public Collection<TbTestResultDetail> getTbTestResultDetailCollection() {
        return tbTestResultDetailCollection;
    }

    public void setTbTestResultDetailCollection(Collection<TbTestResultDetail> tbTestResultDetailCollection) {
        this.tbTestResultDetailCollection = tbTestResultDetailCollection;
    }

    @XmlTransient
    public Collection<TbTestResultDetail> getTbTestResultDetailCollection1() {
        return tbTestResultDetailCollection1;
    }

    public void setTbTestResultDetailCollection1(Collection<TbTestResultDetail> tbTestResultDetailCollection1) {
        this.tbTestResultDetailCollection1 = tbTestResultDetailCollection1;
    }

    @XmlTransient
    public Collection<TbCategory> getTbCategoryCollection() {
        return tbCategoryCollection;
    }

    public void setTbCategoryCollection(Collection<TbCategory> tbCategoryCollection) {
        this.tbCategoryCollection = tbCategoryCollection;
    }

    @XmlTransient
    public Collection<TbService> getTbServiceCollection() {
        return tbServiceCollection;
    }

    public void setTbServiceCollection(Collection<TbService> tbServiceCollection) {
        this.tbServiceCollection = tbServiceCollection;
    }

    @XmlTransient
    public Collection<TbService> getTbServiceCollection1() {
        return tbServiceCollection1;
    }

    public void setTbServiceCollection1(Collection<TbService> tbServiceCollection1) {
        this.tbServiceCollection1 = tbServiceCollection1;
    }

    @XmlTransient
    public Collection<TbSuppliesDetail> getTbSuppliesDetailCollection() {
        return tbSuppliesDetailCollection;
    }

    public void setTbSuppliesDetailCollection(Collection<TbSuppliesDetail> tbSuppliesDetailCollection) {
        this.tbSuppliesDetailCollection = tbSuppliesDetailCollection;
    }

    @XmlTransient
    public Collection<TbServicePrice> getTbServicePriceCollection() {
        return tbServicePriceCollection;
    }

    public void setTbServicePriceCollection(Collection<TbServicePrice> tbServicePriceCollection) {
        this.tbServicePriceCollection = tbServicePriceCollection;
    }

    @XmlTransient
    public Collection<TbServicePrice> getTbServicePriceCollection1() {
        return tbServicePriceCollection1;
    }

    public void setTbServicePriceCollection1(Collection<TbServicePrice> tbServicePriceCollection1) {
        this.tbServicePriceCollection1 = tbServicePriceCollection1;
    }

    @XmlTransient
    public Collection<TbStandardValue> getTbStandardValueCollection() {
        return tbStandardValueCollection;
    }

    public void setTbStandardValueCollection(Collection<TbStandardValue> tbStandardValueCollection) {
        this.tbStandardValueCollection = tbStandardValueCollection;
    }

    @XmlTransient
    public Collection<TbStandardValue> getTbStandardValueCollection1() {
        return tbStandardValueCollection1;
    }

    public void setTbStandardValueCollection1(Collection<TbStandardValue> tbStandardValueCollection1) {
        this.tbStandardValueCollection1 = tbStandardValueCollection1;
    }

    @XmlTransient
    public Collection<TbTestResult> getTbTestResultCollection() {
        return tbTestResultCollection;
    }

    public void setTbTestResultCollection(Collection<TbTestResult> tbTestResultCollection) {
        this.tbTestResultCollection = tbTestResultCollection;
    }

    @XmlTransient
    public Collection<TbTestResult> getTbTestResultCollection1() {
        return tbTestResultCollection1;
    }

    public void setTbTestResultCollection1(Collection<TbTestResult> tbTestResultCollection1) {
        this.tbTestResultCollection1 = tbTestResultCollection1;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (employeeID != null ? employeeID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbEmployee)) {
            return false;
        }
        TbEmployee other = (TbEmployee) object;
        if ((this.employeeID == null && other.employeeID != null) || (this.employeeID != null && !this.employeeID.equals(other.employeeID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "project.ett.TbEmployee[ employeeID=" + employeeID + " ]";
    }
    
}
