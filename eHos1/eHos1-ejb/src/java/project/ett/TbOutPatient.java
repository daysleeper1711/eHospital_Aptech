/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project.ett;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DaySLeePer
 */
@Entity
@Table(name = "tbOutPatient")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbOutPatient.findAll", query = "SELECT t FROM TbOutPatient t"),
    @NamedQuery(name = "TbOutPatient.findByOutPatID", query = "SELECT t FROM TbOutPatient t WHERE t.outPatID = :outPatID"),
    @NamedQuery(name = "TbOutPatient.findByCreateDate", query = "SELECT t FROM TbOutPatient t WHERE t.createDate = :createDate"),
    @NamedQuery(name = "TbOutPatient.findByStatus", query = "SELECT t FROM TbOutPatient t WHERE t.status = :status")})
public class TbOutPatient implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "OutPatID")
    private String outPatID;
    @Column(name = "CreateDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Size(max = 30)
    @Column(name = "Status")
    private String status;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "outPatientID")
    private Collection<TbKQKB> tbKQKBCollection;
    @JoinColumn(name = "PatID", referencedColumnName = "PatID")
    @ManyToOne
    private TbPatient patID;
    @JoinColumn(name = "GKBID", referencedColumnName = "GKBID")
    @ManyToOne
    private TbGKB gkbid;
    @JoinColumn(name = "Creator", referencedColumnName = "EmployeeID")
    @ManyToOne
    private TbEmployee creator;
    @OneToMany(mappedBy = "outPatID")
    private Collection<TbTestRequest> tbTestRequestCollection;

    public TbOutPatient() {
    }

    public TbOutPatient(String outPatID) {
        this.outPatID = outPatID;
    }

    public String getOutPatID() {
        return outPatID;
    }

    public void setOutPatID(String outPatID) {
        this.outPatID = outPatID;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @XmlTransient
    public Collection<TbKQKB> getTbKQKBCollection() {
        return tbKQKBCollection;
    }

    public void setTbKQKBCollection(Collection<TbKQKB> tbKQKBCollection) {
        this.tbKQKBCollection = tbKQKBCollection;
    }

    public TbPatient getPatID() {
        return patID;
    }

    public void setPatID(TbPatient patID) {
        this.patID = patID;
    }

    public TbGKB getGkbid() {
        return gkbid;
    }

    public void setGkbid(TbGKB gkbid) {
        this.gkbid = gkbid;
    }

    public TbEmployee getCreator() {
        return creator;
    }

    public void setCreator(TbEmployee creator) {
        this.creator = creator;
    }

    @XmlTransient
    public Collection<TbTestRequest> getTbTestRequestCollection() {
        return tbTestRequestCollection;
    }

    public void setTbTestRequestCollection(Collection<TbTestRequest> tbTestRequestCollection) {
        this.tbTestRequestCollection = tbTestRequestCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (outPatID != null ? outPatID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbOutPatient)) {
            return false;
        }
        TbOutPatient other = (TbOutPatient) object;
        if ((this.outPatID == null && other.outPatID != null) || (this.outPatID != null && !this.outPatID.equals(other.outPatID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "project.ett.TbOutPatient[ outPatID=" + outPatID + " ]";
    }
    
}
