/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project.ett;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DaySLeePer
 */
@Entity
@Table(name = "tbRegisterDTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbRegisterDTO.findAll", query = "SELECT t FROM TbRegisterDTO t"),
    @NamedQuery(name = "TbRegisterDTO.findByRegisterID", query = "SELECT t FROM TbRegisterDTO t WHERE t.registerID = :registerID"),
    @NamedQuery(name = "TbRegisterDTO.findByName", query = "SELECT t FROM TbRegisterDTO t WHERE t.name = :name"),
    @NamedQuery(name = "TbRegisterDTO.findByDob", query = "SELECT t FROM TbRegisterDTO t WHERE t.dob = :dob"),
    @NamedQuery(name = "TbRegisterDTO.findBySex", query = "SELECT t FROM TbRegisterDTO t WHERE t.sex = :sex"),
    @NamedQuery(name = "TbRegisterDTO.findByDateCreate", query = "SELECT t FROM TbRegisterDTO t WHERE t.dateCreate = :dateCreate"),
    @NamedQuery(name = "TbRegisterDTO.findByUserAddress", query = "SELECT t FROM TbRegisterDTO t WHERE t.userAddress = :userAddress"),
    @NamedQuery(name = "TbRegisterDTO.findByPhone", query = "SELECT t FROM TbRegisterDTO t WHERE t.phone = :phone"),
    @NamedQuery(name = "TbRegisterDTO.findByIdCard", query = "SELECT t FROM TbRegisterDTO t WHERE t.idCard = :idCard"),
    @NamedQuery(name = "TbRegisterDTO.findByGKBName", query = "SELECT t FROM TbRegisterDTO t WHERE t.gKBName = :gKBName")})
public class TbRegisterDTO implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "RegisterID")
    private String registerID;
    @Size(max = 30)
    @Column(name = "Name")
    private String name;
    @Column(name = "DOB")
    @Temporal(TemporalType.DATE)
    private Date dob;
    @Size(max = 5)
    @Column(name = "Sex")
    private String sex;
    @Column(name = "DateCreate")
    @Temporal(TemporalType.DATE)
    private Date dateCreate;
    @Size(max = 50)
    @Column(name = "UserAddress")
    private String userAddress;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Size(max = 12)
    @Column(name = "Phone")
    private String phone;
    @Size(max = 15)
    @Column(name = "IdCard")
    private String idCard;
    @Size(max = 60)
    @Column(name = "GKBName")
    private String gKBName;
    @JoinColumn(name = "GKBID", referencedColumnName = "GKBID")
    @ManyToOne
    private TbGKB gkbid;

    public TbRegisterDTO() {
    }

    public TbRegisterDTO(String registerID) {
        this.registerID = registerID;
    }

    public String getRegisterID() {
        return registerID;
    }

    public void setRegisterID(String registerID) {
        this.registerID = registerID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getGKBName() {
        return gKBName;
    }

    public void setGKBName(String gKBName) {
        this.gKBName = gKBName;
    }

    public TbGKB getGkbid() {
        return gkbid;
    }

    public void setGkbid(TbGKB gkbid) {
        this.gkbid = gkbid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (registerID != null ? registerID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbRegisterDTO)) {
            return false;
        }
        TbRegisterDTO other = (TbRegisterDTO) object;
        if ((this.registerID == null && other.registerID != null) || (this.registerID != null && !this.registerID.equals(other.registerID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "project.ett.TbRegisterDTO[ registerID=" + registerID + " ]";
    }
    
}
