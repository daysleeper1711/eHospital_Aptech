/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project.ett;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DaySLeePer
 */
@Entity
@Table(name = "tbIm_Durgs_Supplies_Details")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbImDurgsSuppliesDetails.findAll", query = "SELECT t FROM TbImDurgsSuppliesDetails t"),
    @NamedQuery(name = "TbImDurgsSuppliesDetails.findByImDetails", query = "SELECT t FROM TbImDurgsSuppliesDetails t WHERE t.imDetails = :imDetails"),
    @NamedQuery(name = "TbImDurgsSuppliesDetails.findByItemCode", query = "SELECT t FROM TbImDurgsSuppliesDetails t WHERE t.itemCode = :itemCode"),
    @NamedQuery(name = "TbImDurgsSuppliesDetails.findByWarDate", query = "SELECT t FROM TbImDurgsSuppliesDetails t WHERE t.warDate = :warDate"),
    @NamedQuery(name = "TbImDurgsSuppliesDetails.findByQuantity", query = "SELECT t FROM TbImDurgsSuppliesDetails t WHERE t.quantity = :quantity"),
    @NamedQuery(name = "TbImDurgsSuppliesDetails.findByNPrice", query = "SELECT t FROM TbImDurgsSuppliesDetails t WHERE t.nPrice = :nPrice")})
public class TbImDurgsSuppliesDetails implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "Im_Details")
    private String imDetails;
    @Size(max = 20)
    @Column(name = "ItemCode")
    private String itemCode;
    @Column(name = "War_Date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date warDate;
    @Column(name = "Quantity")
    private Integer quantity;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "N_Price")
    private Double nPrice;
    @JoinColumn(name = "Package", referencedColumnName = "Package")
    @ManyToOne
    private TbImDurgsSupplies package1;

    public TbImDurgsSuppliesDetails() {
    }

    public TbImDurgsSuppliesDetails(String imDetails) {
        this.imDetails = imDetails;
    }

    public String getImDetails() {
        return imDetails;
    }

    public void setImDetails(String imDetails) {
        this.imDetails = imDetails;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public Date getWarDate() {
        return warDate;
    }

    public void setWarDate(Date warDate) {
        this.warDate = warDate;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Double getNPrice() {
        return nPrice;
    }

    public void setNPrice(Double nPrice) {
        this.nPrice = nPrice;
    }

    public TbImDurgsSupplies getPackage1() {
        return package1;
    }

    public void setPackage1(TbImDurgsSupplies package1) {
        this.package1 = package1;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (imDetails != null ? imDetails.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbImDurgsSuppliesDetails)) {
            return false;
        }
        TbImDurgsSuppliesDetails other = (TbImDurgsSuppliesDetails) object;
        if ((this.imDetails == null && other.imDetails != null) || (this.imDetails != null && !this.imDetails.equals(other.imDetails))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "project.ett.TbImDurgsSuppliesDetails[ imDetails=" + imDetails + " ]";
    }
    
}
