/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project.ett;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DaySLeePer
 */
@Entity
@Table(name = "tbStandardValue")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbStandardValue.findAll", query = "SELECT t FROM TbStandardValue t"),
    @NamedQuery(name = "TbStandardValue.findBySvId", query = "SELECT t FROM TbStandardValue t WHERE t.svId = :svId"),
    @NamedQuery(name = "TbStandardValue.findBySVmale", query = "SELECT t FROM TbStandardValue t WHERE t.sVmale = :sVmale"),
    @NamedQuery(name = "TbStandardValue.findBySVfemale", query = "SELECT t FROM TbStandardValue t WHERE t.sVfemale = :sVfemale"),
    @NamedQuery(name = "TbStandardValue.findByCreateDate", query = "SELECT t FROM TbStandardValue t WHERE t.createDate = :createDate"),
    @NamedQuery(name = "TbStandardValue.findByUpdateDate", query = "SELECT t FROM TbStandardValue t WHERE t.updateDate = :updateDate")})
public class TbStandardValue implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "SV_ID")
    private Integer svId;
    @Size(max = 30)
    @Column(name = "SV_male")
    private String sVmale;
    @Size(max = 30)
    @Column(name = "SV_female")
    private String sVfemale;
    @Column(name = "CreateDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "UpdateDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @JoinColumn(name = "ServiceID", referencedColumnName = "ServiceID")
    @ManyToOne
    private TbService serviceID;
    @JoinColumn(name = "UpdateEmp", referencedColumnName = "EmployeeID")
    @ManyToOne
    private TbEmployee updateEmp;
    @JoinColumn(name = "Creator", referencedColumnName = "EmployeeID")
    @ManyToOne
    private TbEmployee creator;

    public TbStandardValue() {
    }

    public TbStandardValue(Integer svId) {
        this.svId = svId;
    }

    public Integer getSvId() {
        return svId;
    }

    public void setSvId(Integer svId) {
        this.svId = svId;
    }

    public String getSVmale() {
        return sVmale;
    }

    public void setSVmale(String sVmale) {
        this.sVmale = sVmale;
    }

    public String getSVfemale() {
        return sVfemale;
    }

    public void setSVfemale(String sVfemale) {
        this.sVfemale = sVfemale;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public TbService getServiceID() {
        return serviceID;
    }

    public void setServiceID(TbService serviceID) {
        this.serviceID = serviceID;
    }

    public TbEmployee getUpdateEmp() {
        return updateEmp;
    }

    public void setUpdateEmp(TbEmployee updateEmp) {
        this.updateEmp = updateEmp;
    }

    public TbEmployee getCreator() {
        return creator;
    }

    public void setCreator(TbEmployee creator) {
        this.creator = creator;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (svId != null ? svId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbStandardValue)) {
            return false;
        }
        TbStandardValue other = (TbStandardValue) object;
        if ((this.svId == null && other.svId != null) || (this.svId != null && !this.svId.equals(other.svId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "project.ett.TbStandardValue[ svId=" + svId + " ]";
    }

}
