/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project.ett;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DaySLeePer
 */
@Entity
@Table(name = "tbKQ_KBDetail")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbKQKBDetail.findAll", query = "SELECT t FROM TbKQKBDetail t"),
    @NamedQuery(name = "TbKQKBDetail.findByKQKBDetailID", query = "SELECT t FROM TbKQKBDetail t WHERE t.kQKBDetailID = :kQKBDetailID"),
    @NamedQuery(name = "TbKQKBDetail.findByKq", query = "SELECT t FROM TbKQKBDetail t WHERE t.kq = :kq")})
public class TbKQKBDetail implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "KQ_KBDetailID")
    private Integer kQKBDetailID;
    @Size(max = 20)
    @Column(name = "KQ")
    private String kq;
    @JoinColumn(name = "ExaminationResultsID", referencedColumnName = "ExaminationResultsID")
    @ManyToOne
    private TbKQKB examinationResultsID;

    public TbKQKBDetail() {
    }

    public TbKQKBDetail(Integer kQKBDetailID) {
        this.kQKBDetailID = kQKBDetailID;
    }

    public Integer getKQKBDetailID() {
        return kQKBDetailID;
    }

    public void setKQKBDetailID(Integer kQKBDetailID) {
        this.kQKBDetailID = kQKBDetailID;
    }

    public String getKq() {
        return kq;
    }

    public void setKq(String kq) {
        this.kq = kq;
    }

    public TbKQKB getExaminationResultsID() {
        return examinationResultsID;
    }

    public void setExaminationResultsID(TbKQKB examinationResultsID) {
        this.examinationResultsID = examinationResultsID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (kQKBDetailID != null ? kQKBDetailID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbKQKBDetail)) {
            return false;
        }
        TbKQKBDetail other = (TbKQKBDetail) object;
        if ((this.kQKBDetailID == null && other.kQKBDetailID != null) || (this.kQKBDetailID != null && !this.kQKBDetailID.equals(other.kQKBDetailID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "project.ett.TbKQKBDetail[ kQKBDetailID=" + kQKBDetailID + " ]";
    }
    
}
