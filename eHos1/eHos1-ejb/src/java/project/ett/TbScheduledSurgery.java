/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project.ett;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DaySLeePer
 */
@Entity
@Table(name = "tbScheduledSurgery")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbScheduledSurgery.findAll", query = "SELECT t FROM TbScheduledSurgery t"),
    @NamedQuery(name = "TbScheduledSurgery.findByScheduledID", query = "SELECT t FROM TbScheduledSurgery t WHERE t.scheduledID = :scheduledID"),
    @NamedQuery(name = "TbScheduledSurgery.findByTimeOfStart", query = "SELECT t FROM TbScheduledSurgery t WHERE t.timeOfStart = :timeOfStart"),
    @NamedQuery(name = "TbScheduledSurgery.findByTimeOfEnd", query = "SELECT t FROM TbScheduledSurgery t WHERE t.timeOfEnd = :timeOfEnd"),
    @NamedQuery(name = "TbScheduledSurgery.findByStatusScheduled", query = "SELECT t FROM TbScheduledSurgery t WHERE t.statusScheduled = :statusScheduled"),
    @NamedQuery(name = "TbScheduledSurgery.findByCreateDate", query = "SELECT t FROM TbScheduledSurgery t WHERE t.createDate = :createDate")})
public class TbScheduledSurgery implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "ScheduledID")
    private String scheduledID;
    @Column(name = "TimeOfStart")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timeOfStart;
    @Column(name = "TimeOfEnd")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timeOfEnd;
    @Column(name = "StatusScheduled")
    private Boolean statusScheduled;
    @Column(name = "CreateDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @JoinColumn(name = "SuggestSurgeryID", referencedColumnName = "SuggestSurgeryID")
    @ManyToOne
    private TbSuggestSurgery suggestSurgeryID;
    @JoinColumn(name = "UserID", referencedColumnName = "EmployeeID")
    @ManyToOne
    private TbEmployee userID;

    public TbScheduledSurgery() {
    }

    public TbScheduledSurgery(String scheduledID) {
        this.scheduledID = scheduledID;
    }

    public String getScheduledID() {
        return scheduledID;
    }

    public void setScheduledID(String scheduledID) {
        this.scheduledID = scheduledID;
    }

    public Date getTimeOfStart() {
        return timeOfStart;
    }

    public void setTimeOfStart(Date timeOfStart) {
        this.timeOfStart = timeOfStart;
    }

    public Date getTimeOfEnd() {
        return timeOfEnd;
    }

    public void setTimeOfEnd(Date timeOfEnd) {
        this.timeOfEnd = timeOfEnd;
    }

    public Boolean getStatusScheduled() {
        return statusScheduled;
    }

    public void setStatusScheduled(Boolean statusScheduled) {
        this.statusScheduled = statusScheduled;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public TbSuggestSurgery getSuggestSurgeryID() {
        return suggestSurgeryID;
    }

    public void setSuggestSurgeryID(TbSuggestSurgery suggestSurgeryID) {
        this.suggestSurgeryID = suggestSurgeryID;
    }

    public TbEmployee getUserID() {
        return userID;
    }

    public void setUserID(TbEmployee userID) {
        this.userID = userID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (scheduledID != null ? scheduledID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbScheduledSurgery)) {
            return false;
        }
        TbScheduledSurgery other = (TbScheduledSurgery) object;
        if ((this.scheduledID == null && other.scheduledID != null) || (this.scheduledID != null && !this.scheduledID.equals(other.scheduledID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "project.ett.TbScheduledSurgery[ scheduledID=" + scheduledID + " ]";
    }
    
}
