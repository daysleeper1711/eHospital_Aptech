/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project.ett;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DaySLeePer
 */
@Entity
@Table(name = "tbIm_Durgs_Supplies")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbImDurgsSupplies.findAll", query = "SELECT t FROM TbImDurgsSupplies t"),
    @NamedQuery(name = "TbImDurgsSupplies.findByPackage1", query = "SELECT t FROM TbImDurgsSupplies t WHERE t.package1 = :package1"),
    @NamedQuery(name = "TbImDurgsSupplies.findByNote", query = "SELECT t FROM TbImDurgsSupplies t WHERE t.note = :note"),
    @NamedQuery(name = "TbImDurgsSupplies.findByCrDate", query = "SELECT t FROM TbImDurgsSupplies t WHERE t.crDate = :crDate"),
    @NamedQuery(name = "TbImDurgsSupplies.findByCreateby", query = "SELECT t FROM TbImDurgsSupplies t WHERE t.createby = :createby"),
    @NamedQuery(name = "TbImDurgsSupplies.findByUpdateby", query = "SELECT t FROM TbImDurgsSupplies t WHERE t.updateby = :updateby"),
    @NamedQuery(name = "TbImDurgsSupplies.findByUpDate", query = "SELECT t FROM TbImDurgsSupplies t WHERE t.upDate = :upDate"),
    @NamedQuery(name = "TbImDurgsSupplies.findByNumBill", query = "SELECT t FROM TbImDurgsSupplies t WHERE t.numBill = :numBill"),
    @NamedQuery(name = "TbImDurgsSupplies.findBySymBill", query = "SELECT t FROM TbImDurgsSupplies t WHERE t.symBill = :symBill"),
    @NamedQuery(name = "TbImDurgsSupplies.findByTotatPrice", query = "SELECT t FROM TbImDurgsSupplies t WHERE t.totatPrice = :totatPrice"),
    @NamedQuery(name = "TbImDurgsSupplies.findByUStatus", query = "SELECT t FROM TbImDurgsSupplies t WHERE t.uStatus = :uStatus")})
public class TbImDurgsSupplies implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 32)
    @Column(name = "Package")
    private String package1;
    @Size(max = 200)
    @Column(name = "Note")
    private String note;
    @Column(name = "Cr_Date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date crDate;
    @Size(max = 20)
    @Column(name = "Createby")
    private String createby;
    @Size(max = 20)
    @Column(name = "Updateby")
    private String updateby;
    @Column(name = "Up_Date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date upDate;
    @Size(max = 20)
    @Column(name = "NumBill")
    private String numBill;
    @Size(max = 20)
    @Column(name = "SymBill")
    private String symBill;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "Totat_Price")
    private Double totatPrice;
    @Size(max = 2)
    @Column(name = "U_Status")
    private String uStatus;
    @JoinColumn(name = "SupplierID", referencedColumnName = "SupplierID")
    @ManyToOne
    private TbSupplier supplierID;
    @OneToMany(mappedBy = "package1",cascade = CascadeType.ALL)
    private Collection<TbImDurgsSuppliesDetails> tbImDurgsSuppliesDetailsCollection;

    public TbImDurgsSupplies() {
    }

    public TbImDurgsSupplies(String package1) {
        this.package1 = package1;
    }

    public String getPackage1() {
        return package1;
    }

    public void setPackage1(String package1) {
        this.package1 = package1;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Date getCrDate() {
        return crDate;
    }

    public void setCrDate(Date crDate) {
        this.crDate = crDate;
    }

    public String getCreateby() {
        return createby;
    }

    public void setCreateby(String createby) {
        this.createby = createby;
    }

    public String getUpdateby() {
        return updateby;
    }

    public void setUpdateby(String updateby) {
        this.updateby = updateby;
    }

    public Date getUpDate() {
        return upDate;
    }

    public void setUpDate(Date upDate) {
        this.upDate = upDate;
    }

    public String getNumBill() {
        return numBill;
    }

    public void setNumBill(String numBill) {
        this.numBill = numBill;
    }

    public String getSymBill() {
        return symBill;
    }

    public void setSymBill(String symBill) {
        this.symBill = symBill;
    }

    public Double getTotatPrice() {
        return totatPrice;
    }

    public void setTotatPrice(Double totatPrice) {
        this.totatPrice = totatPrice;
    }

    public String getUStatus() {
        return uStatus;
    }

    public void setUStatus(String uStatus) {
        this.uStatus = uStatus;
    }

    public TbSupplier getSupplierID() {
        return supplierID;
    }

    public void setSupplierID(TbSupplier supplierID) {
        this.supplierID = supplierID;
    }

    @XmlTransient
    public Collection<TbImDurgsSuppliesDetails> getTbImDurgsSuppliesDetailsCollection() {
        return tbImDurgsSuppliesDetailsCollection;
    }

    public void setTbImDurgsSuppliesDetailsCollection(Collection<TbImDurgsSuppliesDetails> tbImDurgsSuppliesDetailsCollection) {
        this.tbImDurgsSuppliesDetailsCollection = tbImDurgsSuppliesDetailsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (package1 != null ? package1.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbImDurgsSupplies)) {
            return false;
        }
        TbImDurgsSupplies other = (TbImDurgsSupplies) object;
        if ((this.package1 == null && other.package1 != null) || (this.package1 != null && !this.package1.equals(other.package1))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "project.ett.TbImDurgsSupplies[ package1=" + package1 + " ]";
    }
    
}
