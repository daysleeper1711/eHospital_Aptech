/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project.ett;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DaySLeePer
 */
@Entity
@Table(name = "tbServiceGroup")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbServiceGroup.findAll", query = "SELECT t FROM TbServiceGroup t"),
    @NamedQuery(name = "TbServiceGroup.findByGroupID", query = "SELECT t FROM TbServiceGroup t WHERE t.groupID = :groupID"),
    @NamedQuery(name = "TbServiceGroup.findByGroupName", query = "SELECT t FROM TbServiceGroup t WHERE t.groupName = :groupName"),
    @NamedQuery(name = "TbServiceGroup.findByDescription", query = "SELECT t FROM TbServiceGroup t WHERE t.description = :description"),
    @NamedQuery(name = "TbServiceGroup.findByStatus", query = "SELECT t FROM TbServiceGroup t WHERE t.status = :status"),
    @NamedQuery(name = "TbServiceGroup.findByCreateDate", query = "SELECT t FROM TbServiceGroup t WHERE t.createDate = :createDate"),
    @NamedQuery(name = "TbServiceGroup.findByUpdateDate", query = "SELECT t FROM TbServiceGroup t WHERE t.updateDate = :updateDate")})
public class TbServiceGroup implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "GroupID")
    private Integer groupID;
    @Size(max = 30)
    @Column(name = "GroupName")
    private String groupName;
    @Size(max = 20)
    @Column(name = "GroupType")
    private String groupType;
    @Size(max = 100)
    @Column(name = "Description")
    private String description;
    @Column(name = "Status")
    private Boolean status;
    @Column(name = "CreateDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "UpdateDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @JoinColumn(name = "UpdateEmp", referencedColumnName = "EmployeeID")
    @ManyToOne
    private TbEmployee updateEmp;
    @JoinColumn(name = "Creator", referencedColumnName = "EmployeeID")
    @ManyToOne
    private TbEmployee creator;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "groupID")
    private Collection<TbService> tbServiceCollection;

    public TbServiceGroup() {
    }

    public TbServiceGroup(Integer groupID) {
        this.groupID = groupID;
    }

    public Integer getGroupID() {
        return groupID;
    }

    public void setGroupID(Integer groupID) {
        this.groupID = groupID;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGroupType() {
        return groupType;
    }

    public void setGroupType(String groupType) {
        this.groupType = groupType;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public TbEmployee getUpdateEmp() {
        return updateEmp;
    }

    public void setUpdateEmp(TbEmployee updateEmp) {
        this.updateEmp = updateEmp;
    }

    public TbEmployee getCreator() {
        return creator;
    }

    public void setCreator(TbEmployee creator) {
        this.creator = creator;
    }

    @XmlTransient
    public Collection<TbService> getTbServiceCollection() {
        return tbServiceCollection;
    }

    public void setTbServiceCollection(Collection<TbService> tbServiceCollection) {
        this.tbServiceCollection = tbServiceCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (groupID != null ? groupID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbServiceGroup)) {
            return false;
        }
        TbServiceGroup other = (TbServiceGroup) object;
        if ((this.groupID == null && other.groupID != null) || (this.groupID != null && !this.groupID.equals(other.groupID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "project.ett.TbServiceGroup[ groupID=" + groupID + " ]";
    }
    
}
