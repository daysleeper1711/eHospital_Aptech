/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project.ett;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DaySLeePer
 */
@Entity
@Table(name = "tbMedicineSupplies")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbMedicineSupplies.findAll", query = "SELECT t FROM TbMedicineSupplies t"),
    @NamedQuery(name = "TbMedicineSupplies.findByMedicineSuppliesID", query = "SELECT t FROM TbMedicineSupplies t WHERE t.medicineSuppliesID = :medicineSuppliesID"),
    @NamedQuery(name = "TbMedicineSupplies.findByCreateForDate", query = "SELECT t FROM TbMedicineSupplies t WHERE t.createForDate = :createForDate"),
    @NamedQuery(name = "TbMedicineSupplies.findByDateCreate", query = "SELECT t FROM TbMedicineSupplies t WHERE t.dateCreate = :dateCreate"),
    @NamedQuery(name = "TbMedicineSupplies.findByTotalPrice", query = "SELECT t FROM TbMedicineSupplies t WHERE t.totalPrice = :totalPrice"),
    @NamedQuery(name = "TbMedicineSupplies.findByStatusMedSupp", query = "SELECT t FROM TbMedicineSupplies t WHERE t.statusMedSupp = :statusMedSupp"),
    @NamedQuery(name = "TbMedicineSupplies.findByStatusReport", query = "SELECT t FROM TbMedicineSupplies t WHERE t.statusReport = :statusReport"),
    @NamedQuery(name = "TbMedicineSupplies.findByUpStatusGived", query = "SELECT t FROM TbMedicineSupplies t WHERE t.upStatusGived = :upStatusGived"),
    @NamedQuery(name = "TbMedicineSupplies.findByPriceAfterHealthInsurance", query = "SELECT t FROM TbMedicineSupplies t WHERE t.priceAfterHealthInsurance = :priceAfterHealthInsurance"),
    @NamedQuery(name = "TbMedicineSupplies.findByUpDateTime", query = "SELECT t FROM TbMedicineSupplies t WHERE t.upDateTime = :upDateTime")})
public class TbMedicineSupplies implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "MedicineSuppliesID")
    private String medicineSuppliesID;
    @Column(name = "CreateForDate")
    @Temporal(TemporalType.DATE)
    private Date createForDate;
    @Column(name = "DateCreate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreate;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "TotalPrice")
    private Double totalPrice;
    @Size(max = 50)
    @Column(name = "StatusMedSupp")
    private String statusMedSupp;
    @Size(max = 50)
    @Column(name = "StatusReport")
    private String statusReport;
    @Column(name = "upStatusGived")
    @Temporal(TemporalType.TIMESTAMP)
    private Date upStatusGived;
    @Column(name = "PriceAfterHealthInsurance")
    private Double priceAfterHealthInsurance;
    @Column(name = "UpDateTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date upDateTime;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "medicineSuppliesID")
    private Collection<TbMedicineSuppliesDetail> tbMedicineSuppliesDetailCollection;
    @JoinColumn(name = "MedicalRecordID", referencedColumnName = "MedRecordID")
    @ManyToOne
    private TbMedicalRecord medicalRecordID;
    @JoinColumn(name = "FacultyFrom", referencedColumnName = "Fa_ID")
    @ManyToOne
    private TbFaculty facultyFrom;
    @JoinColumn(name = "UpEmployeeID", referencedColumnName = "EmployeeID")
    @ManyToOne
    private TbEmployee upEmployeeID;
    @JoinColumn(name = "EmployeeID", referencedColumnName = "EmployeeID")
    @ManyToOne
    private TbEmployee employeeID;

    public TbMedicineSupplies() {
    }

    public TbMedicineSupplies(String medicineSuppliesID) {
        this.medicineSuppliesID = medicineSuppliesID;
    }

    public String getMedicineSuppliesID() {
        return medicineSuppliesID;
    }

    public void setMedicineSuppliesID(String medicineSuppliesID) {
        this.medicineSuppliesID = medicineSuppliesID;
    }

    public Date getCreateForDate() {
        return createForDate;
    }

    public void setCreateForDate(Date createForDate) {
        this.createForDate = createForDate;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getStatusMedSupp() {
        return statusMedSupp;
    }

    public void setStatusMedSupp(String statusMedSupp) {
        this.statusMedSupp = statusMedSupp;
    }

    public String getStatusReport() {
        return statusReport;
    }

    public void setStatusReport(String statusReport) {
        this.statusReport = statusReport;
    }

    public Date getUpStatusGived() {
        return upStatusGived;
    }

    public void setUpStatusGived(Date upStatusGived) {
        this.upStatusGived = upStatusGived;
    }

    public Double getPriceAfterHealthInsurance() {
        return priceAfterHealthInsurance;
    }

    public void setPriceAfterHealthInsurance(Double priceAfterHealthInsurance) {
        this.priceAfterHealthInsurance = priceAfterHealthInsurance;
    }

    public Date getUpDateTime() {
        return upDateTime;
    }

    public void setUpDateTime(Date upDateTime) {
        this.upDateTime = upDateTime;
    }

    @XmlTransient
    public Collection<TbMedicineSuppliesDetail> getTbMedicineSuppliesDetailCollection() {
        return tbMedicineSuppliesDetailCollection;
    }

    public void setTbMedicineSuppliesDetailCollection(Collection<TbMedicineSuppliesDetail> tbMedicineSuppliesDetailCollection) {
        this.tbMedicineSuppliesDetailCollection = tbMedicineSuppliesDetailCollection;
    }

    public TbMedicalRecord getMedicalRecordID() {
        return medicalRecordID;
    }

    public void setMedicalRecordID(TbMedicalRecord medicalRecordID) {
        this.medicalRecordID = medicalRecordID;
    }

    public TbFaculty getFacultyFrom() {
        return facultyFrom;
    }

    public void setFacultyFrom(TbFaculty facultyFrom) {
        this.facultyFrom = facultyFrom;
    }

    public TbEmployee getUpEmployeeID() {
        return upEmployeeID;
    }

    public void setUpEmployeeID(TbEmployee upEmployeeID) {
        this.upEmployeeID = upEmployeeID;
    }

    public TbEmployee getEmployeeID() {
        return employeeID;
    }

    public void setEmployeeID(TbEmployee employeeID) {
        this.employeeID = employeeID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (medicineSuppliesID != null ? medicineSuppliesID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbMedicineSupplies)) {
            return false;
        }
        TbMedicineSupplies other = (TbMedicineSupplies) object;
        if ((this.medicineSuppliesID == null && other.medicineSuppliesID != null) || (this.medicineSuppliesID != null && !this.medicineSuppliesID.equals(other.medicineSuppliesID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "project.ett.TbMedicineSupplies[ medicineSuppliesID=" + medicineSuppliesID + " ]";
    }
    
}
