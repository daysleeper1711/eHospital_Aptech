/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project.ett;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DaySLeePer
 */
@Entity
@Table(name = "tbWhouse")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbWhouse.findAll", query = "SELECT t FROM TbWhouse t"),
    @NamedQuery(name = "TbWhouse.findByWhouseID", query = "SELECT t FROM TbWhouse t WHERE t.whouseID = :whouseID"),
    @NamedQuery(name = "TbWhouse.findByItemCode", query = "SELECT t FROM TbWhouse t WHERE t.itemCode = :itemCode"),
    @NamedQuery(name = "TbWhouse.findByPackage1", query = "SELECT t FROM TbWhouse t WHERE t.package1 = :package1"),
    @NamedQuery(name = "TbWhouse.findByWarDate", query = "SELECT t FROM TbWhouse t WHERE t.warDate = :warDate"),
    @NamedQuery(name = "TbWhouse.findByOnHand", query = "SELECT t FROM TbWhouse t WHERE t.onHand = :onHand"),
    @NamedQuery(name = "TbWhouse.findByQtyAvailable", query = "SELECT t FROM TbWhouse t WHERE t.qtyAvailable = :qtyAvailable"),
    @NamedQuery(name = "TbWhouse.findByQtyOrder", query = "SELECT t FROM TbWhouse t WHERE t.qtyOrder = :qtyOrder")})
public class TbWhouse implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "WhouseID")
    private String whouseID;
    @Size(max = 20)
    @Column(name = "ItemCode")
    private String itemCode;
    @Size(max = 32)
    @Column(name = "Package")
    private String package1;
    @Column(name = "War_Date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date warDate;
    @Column(name = "OnHand")
    private Integer onHand;
    @Column(name = "Qty_Available")
    private Integer qtyAvailable;
    @Column(name = "Qty_Order")
    private Integer qtyOrder;

    public TbWhouse() {
    }

    public TbWhouse(String whouseID) {
        this.whouseID = whouseID;
    }

    public String getWhouseID() {
        return whouseID;
    }

    public void setWhouseID(String whouseID) {
        this.whouseID = whouseID;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getPackage1() {
        return package1;
    }

    public void setPackage1(String package1) {
        this.package1 = package1;
    }

    public Date getWarDate() {
        return warDate;
    }

    public void setWarDate(Date warDate) {
        this.warDate = warDate;
    }

    public Integer getOnHand() {
        return onHand;
    }

    public void setOnHand(Integer onHand) {
        this.onHand = onHand;
    }

    public Integer getQtyAvailable() {
        return qtyAvailable;
    }

    public void setQtyAvailable(Integer qtyAvailable) {
        this.qtyAvailable = qtyAvailable;
    }

    public Integer getQtyOrder() {
        return qtyOrder;
    }

    public void setQtyOrder(Integer qtyOrder) {
        this.qtyOrder = qtyOrder;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (whouseID != null ? whouseID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbWhouse)) {
            return false;
        }
        TbWhouse other = (TbWhouse) object;
        if ((this.whouseID == null && other.whouseID != null) || (this.whouseID != null && !this.whouseID.equals(other.whouseID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "project.ett.TbWhouse[ whouseID=" + whouseID + " ]";
    }
    
}
