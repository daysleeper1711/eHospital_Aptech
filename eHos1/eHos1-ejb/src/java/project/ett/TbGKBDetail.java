/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project.ett;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DaySLeePer
 */
@Entity
@Table(name = "tbGKB_Detail")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbGKBDetail.findAll", query = "SELECT t FROM TbGKBDetail t"),
    @NamedQuery(name = "TbGKBDetail.findByGKBDetailID", query = "SELECT t FROM TbGKBDetail t WHERE t.gKBDetailID = :gKBDetailID"),
    @NamedQuery(name = "TbGKBDetail.findByTenGK", query = "SELECT t FROM TbGKBDetail t WHERE t.tenGK = :tenGK")})
public class TbGKBDetail implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "GKB_DetailID")
    private Integer gKBDetailID;
    @Size(max = 50)
    @Column(name = "TenGK")
    private String tenGK;
    @OneToMany(mappedBy = "gKBDetailID")
    private Collection<TbGKBService> tbGKBServiceCollection;
    @JoinColumn(name = "GKBID", referencedColumnName = "GKBID")
    @ManyToOne
    private TbGKB gkbid;

    public TbGKBDetail() {
    }

    public TbGKBDetail(Integer gKBDetailID) {
        this.gKBDetailID = gKBDetailID;
    }

    public Integer getGKBDetailID() {
        return gKBDetailID;
    }

    public void setGKBDetailID(Integer gKBDetailID) {
        this.gKBDetailID = gKBDetailID;
    }

    public String getTenGK() {
        return tenGK;
    }

    public void setTenGK(String tenGK) {
        this.tenGK = tenGK;
    }

    @XmlTransient
    public Collection<TbGKBService> getTbGKBServiceCollection() {
        return tbGKBServiceCollection;
    }

    public void setTbGKBServiceCollection(Collection<TbGKBService> tbGKBServiceCollection) {
        this.tbGKBServiceCollection = tbGKBServiceCollection;
    }

    public TbGKB getGkbid() {
        return gkbid;
    }

    public void setGkbid(TbGKB gkbid) {
        this.gkbid = gkbid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gKBDetailID != null ? gKBDetailID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbGKBDetail)) {
            return false;
        }
        TbGKBDetail other = (TbGKBDetail) object;
        if ((this.gKBDetailID == null && other.gKBDetailID != null) || (this.gKBDetailID != null && !this.gKBDetailID.equals(other.gKBDetailID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "project.ett.TbGKBDetail[ gKBDetailID=" + gKBDetailID + " ]";
    }
    
}
