/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project.ett;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DaySLeePer
 */
@Entity
@Table(name = "tbServicePrice")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbServicePrice.findAll", query = "SELECT t FROM TbServicePrice t"),
    @NamedQuery(name = "TbServicePrice.findByPriceID", query = "SELECT t FROM TbServicePrice t WHERE t.priceID = :priceID"),
    @NamedQuery(name = "TbServicePrice.findByPrice", query = "SELECT t FROM TbServicePrice t WHERE t.price = :price"),
    @NamedQuery(name = "TbServicePrice.findByCreateDate", query = "SELECT t FROM TbServicePrice t WHERE t.createDate = :createDate"),
    @NamedQuery(name = "TbServicePrice.findByUpdateDate", query = "SELECT t FROM TbServicePrice t WHERE t.updateDate = :updateDate"),
    @NamedQuery(name = "TbServicePrice.findByStatus", query = "SELECT t FROM TbServicePrice t WHERE t.status = :status")})
public class TbServicePrice implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PriceID")
    private Integer priceID;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "Price")
    private Double price;
    @Column(name = "CreateDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "UpdateDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @Column(name = "Status")
    private Boolean status;
    @JoinColumn(name = "ServiceID", referencedColumnName = "ServiceID")
    @ManyToOne
    private TbService serviceID;
    @JoinColumn(name = "UpdateEmp", referencedColumnName = "EmployeeID")
    @ManyToOne
    private TbEmployee updateEmp;
    @JoinColumn(name = "Creator", referencedColumnName = "EmployeeID")
    @ManyToOne
    private TbEmployee creator;

    public TbServicePrice() {
    }

    public TbServicePrice(Integer priceID) {
        this.priceID = priceID;
    }

    public Integer getPriceID() {
        return priceID;
    }

    public void setPriceID(Integer priceID) {
        this.priceID = priceID;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public TbService getServiceID() {
        return serviceID;
    }

    public void setServiceID(TbService serviceID) {
        this.serviceID = serviceID;
    }

    public TbEmployee getUpdateEmp() {
        return updateEmp;
    }

    public void setUpdateEmp(TbEmployee updateEmp) {
        this.updateEmp = updateEmp;
    }

    public TbEmployee getCreator() {
        return creator;
    }

    public void setCreator(TbEmployee creator) {
        this.creator = creator;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (priceID != null ? priceID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbServicePrice)) {
            return false;
        }
        TbServicePrice other = (TbServicePrice) object;
        if ((this.priceID == null && other.priceID != null) || (this.priceID != null && !this.priceID.equals(other.priceID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "project.ett.TbServicePrice[ priceID=" + priceID + " ]";
    }
    
}
