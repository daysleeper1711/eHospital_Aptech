/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project.ett;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DaySLeePer
 */
@Entity
@Table(name = "tbKTQ")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbKTQ.findAll", query = "SELECT t FROM TbKTQ t"),
    @NamedQuery(name = "TbKTQ.findByKtoid", query = "SELECT t FROM TbKTQ t WHERE t.ktoid = :ktoid"),
    @NamedQuery(name = "TbKTQ.findByHeight", query = "SELECT t FROM TbKTQ t WHERE t.height = :height"),
    @NamedQuery(name = "TbKTQ.findByKQWeight", query = "SELECT t FROM TbKTQ t WHERE t.kQWeight = :kQWeight"),
    @NamedQuery(name = "TbKTQ.findByPulse", query = "SELECT t FROM TbKTQ t WHERE t.pulse = :pulse"),
    @NamedQuery(name = "TbKTQ.findByBloodPressure", query = "SELECT t FROM TbKTQ t WHERE t.bloodPressure = :bloodPressure"),
    @NamedQuery(name = "TbKTQ.findByBMIIndex", query = "SELECT t FROM TbKTQ t WHERE t.bMIIndex = :bMIIndex"),
    @NamedQuery(name = "TbKTQ.findByClassificationFitness", query = "SELECT t FROM TbKTQ t WHERE t.classificationFitness = :classificationFitness")})
public class TbKTQ implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "KTOID")
    private Integer ktoid;
    @Size(max = 5)
    @Column(name = "height")
    private String height;
    @Size(max = 5)
    @Column(name = "KQWeight")
    private String kQWeight;
    @Size(max = 5)
    @Column(name = "pulse")
    private String pulse;
    @Size(max = 5)
    @Column(name = "BloodPressure")
    private String bloodPressure;
    @Size(max = 5)
    @Column(name = "BMI_Index")
    private String bMIIndex;
    @Size(max = 5)
    @Column(name = "Classification_Fitness")
    private String classificationFitness;
    @JoinColumn(name = "ExaminationResultsID", referencedColumnName = "ExaminationResultsID")
    @ManyToOne
    private TbKQKB examinationResultsID;

    public TbKTQ() {
    }

    public TbKTQ(Integer ktoid) {
        this.ktoid = ktoid;
    }

    public Integer getKtoid() {
        return ktoid;
    }

    public void setKtoid(Integer ktoid) {
        this.ktoid = ktoid;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getKQWeight() {
        return kQWeight;
    }

    public void setKQWeight(String kQWeight) {
        this.kQWeight = kQWeight;
    }

    public String getPulse() {
        return pulse;
    }

    public void setPulse(String pulse) {
        this.pulse = pulse;
    }

    public String getBloodPressure() {
        return bloodPressure;
    }

    public void setBloodPressure(String bloodPressure) {
        this.bloodPressure = bloodPressure;
    }

    public String getBMIIndex() {
        return bMIIndex;
    }

    public void setBMIIndex(String bMIIndex) {
        this.bMIIndex = bMIIndex;
    }

    public String getClassificationFitness() {
        return classificationFitness;
    }

    public void setClassificationFitness(String classificationFitness) {
        this.classificationFitness = classificationFitness;
    }

    public TbKQKB getExaminationResultsID() {
        return examinationResultsID;
    }

    public void setExaminationResultsID(TbKQKB examinationResultsID) {
        this.examinationResultsID = examinationResultsID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ktoid != null ? ktoid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbKTQ)) {
            return false;
        }
        TbKTQ other = (TbKTQ) object;
        if ((this.ktoid == null && other.ktoid != null) || (this.ktoid != null && !this.ktoid.equals(other.ktoid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "project.ett.TbKTQ[ ktoid=" + ktoid + " ]";
    }
    
}
