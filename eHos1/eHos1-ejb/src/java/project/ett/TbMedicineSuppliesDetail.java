/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project.ett;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DaySLeePer
 */
@Entity
@Table(name = "tbMedicineSuppliesDetail")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbMedicineSuppliesDetail.findAll", query = "SELECT t FROM TbMedicineSuppliesDetail t"),
    @NamedQuery(name = "TbMedicineSuppliesDetail.findByMedicineSuppliesDetailID", query = "SELECT t FROM TbMedicineSuppliesDetail t WHERE t.medicineSuppliesDetailID = :medicineSuppliesDetailID"),
    @NamedQuery(name = "TbMedicineSuppliesDetail.findByQuantity", query = "SELECT t FROM TbMedicineSuppliesDetail t WHERE t.quantity = :quantity"),
    @NamedQuery(name = "TbMedicineSuppliesDetail.findByPrice", query = "SELECT t FROM TbMedicineSuppliesDetail t WHERE t.price = :price"),
    @NamedQuery(name = "TbMedicineSuppliesDetail.findByDateCreate", query = "SELECT t FROM TbMedicineSuppliesDetail t WHERE t.dateCreate = :dateCreate"),
    @NamedQuery(name = "TbMedicineSuppliesDetail.findByUpDateTime", query = "SELECT t FROM TbMedicineSuppliesDetail t WHERE t.upDateTime = :upDateTime")})
public class TbMedicineSuppliesDetail implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "MedicineSuppliesDetailID")
    private String medicineSuppliesDetailID;
    @Column(name = "Quantity")
    private Integer quantity;
    @Column(name = "Price")
    private Double price;
    @Column(name = "DateCreate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreate;
    @Column(name = "UpDateTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date upDateTime;
    @JoinColumn(name = "MedicineSuppliesID", referencedColumnName = "MedicineSuppliesID")
    @ManyToOne
    private TbMedicineSupplies medicineSuppliesID;
    @JoinColumn(name = "MedicineID", referencedColumnName = "ItemCode")
    @ManyToOne
    private TbItemCodeManagement medicineID;
    @JoinColumn(name = "UpEmployeeID", referencedColumnName = "EmployeeID")
    @ManyToOne
    private TbEmployee upEmployeeID;

    public TbMedicineSuppliesDetail() {
    }

    public TbMedicineSuppliesDetail(String medicineSuppliesDetailID) {
        this.medicineSuppliesDetailID = medicineSuppliesDetailID;
    }

    public String getMedicineSuppliesDetailID() {
        return medicineSuppliesDetailID;
    }

    public void setMedicineSuppliesDetailID(String medicineSuppliesDetailID) {
        this.medicineSuppliesDetailID = medicineSuppliesDetailID;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public Date getUpDateTime() {
        return upDateTime;
    }

    public void setUpDateTime(Date upDateTime) {
        this.upDateTime = upDateTime;
    }

    public TbMedicineSupplies getMedicineSuppliesID() {
        return medicineSuppliesID;
    }

    public void setMedicineSuppliesID(TbMedicineSupplies medicineSuppliesID) {
        this.medicineSuppliesID = medicineSuppliesID;
    }

    public TbItemCodeManagement getMedicineID() {
        return medicineID;
    }

    public void setMedicineID(TbItemCodeManagement medicineID) {
        this.medicineID = medicineID;
    }

    public TbEmployee getUpEmployeeID() {
        return upEmployeeID;
    }

    public void setUpEmployeeID(TbEmployee upEmployeeID) {
        this.upEmployeeID = upEmployeeID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (medicineSuppliesDetailID != null ? medicineSuppliesDetailID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbMedicineSuppliesDetail)) {
            return false;
        }
        TbMedicineSuppliesDetail other = (TbMedicineSuppliesDetail) object;
        if ((this.medicineSuppliesDetailID == null && other.medicineSuppliesDetailID != null) || (this.medicineSuppliesDetailID != null && !this.medicineSuppliesDetailID.equals(other.medicineSuppliesDetailID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "project.ett.TbMedicineSuppliesDetail[ medicineSuppliesDetailID=" + medicineSuppliesDetailID + " ]";
    }
    
}
