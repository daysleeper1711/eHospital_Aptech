/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project.ett;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DaySLeePer
 */
@Entity
@Table(name = "tbCategory")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbCategory.findAll", query = "SELECT t FROM TbCategory t"),
    @NamedQuery(name = "TbCategory.findByCategoryID", query = "SELECT t FROM TbCategory t WHERE t.categoryID = :categoryID"),
    @NamedQuery(name = "TbCategory.findByCategoryName", query = "SELECT t FROM TbCategory t WHERE t.categoryName = :categoryName"),
    @NamedQuery(name = "TbCategory.findByCategoryDescription", query = "SELECT t FROM TbCategory t WHERE t.categoryDescription = :categoryDescription"),
    @NamedQuery(name = "TbCategory.findByStatusCategory", query = "SELECT t FROM TbCategory t WHERE t.statusCategory = :statusCategory"),
    @NamedQuery(name = "TbCategory.findByCreateDate", query = "SELECT t FROM TbCategory t WHERE t.createDate = :createDate")})
public class TbCategory implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CategoryID")
    private Integer categoryID;
    @Size(max = 50)
    @Column(name = "CategoryName")
    private String categoryName;
    @Size(max = 250)
    @Column(name = "CategoryDescription")
    private String categoryDescription;
    @Column(name = "StatusCategory")
    private Boolean statusCategory;
    @Column(name = "CreateDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "categoryID")
    private Collection<TbSurgeryService> tbSurgeryServiceCollection;
    @JoinColumn(name = "UserID", referencedColumnName = "EmployeeID")
    @ManyToOne
    private TbEmployee userID;

    public TbCategory() {
    }

    public TbCategory(Integer categoryID) {
        this.categoryID = categoryID;
    }

    public Integer getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(Integer categoryID) {
        this.categoryID = categoryID;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryDescription() {
        return categoryDescription;
    }

    public void setCategoryDescription(String categoryDescription) {
        this.categoryDescription = categoryDescription;
    }

    public Boolean getStatusCategory() {
        return statusCategory;
    }

    public void setStatusCategory(Boolean statusCategory) {
        this.statusCategory = statusCategory;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @XmlTransient
    public Collection<TbSurgeryService> getTbSurgeryServiceCollection() {
        return tbSurgeryServiceCollection;
    }

    public void setTbSurgeryServiceCollection(Collection<TbSurgeryService> tbSurgeryServiceCollection) {
        this.tbSurgeryServiceCollection = tbSurgeryServiceCollection;
    }

    public TbEmployee getUserID() {
        return userID;
    }

    public void setUserID(TbEmployee userID) {
        this.userID = userID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (categoryID != null ? categoryID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbCategory)) {
            return false;
        }
        TbCategory other = (TbCategory) object;
        if ((this.categoryID == null && other.categoryID != null) || (this.categoryID != null && !this.categoryID.equals(other.categoryID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "project.ett.TbCategory[ categoryID=" + categoryID + " ]";
    }
    
}
