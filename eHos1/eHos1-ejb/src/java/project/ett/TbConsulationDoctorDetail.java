/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project.ett;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DaySLeePer
 */
@Entity
@Table(name = "tbConsulationDoctorDetail")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbConsulationDoctorDetail.findAll", query = "SELECT t FROM TbConsulationDoctorDetail t"),
    @NamedQuery(name = "TbConsulationDoctorDetail.findByConsulationDoctorDetailID", query = "SELECT t FROM TbConsulationDoctorDetail t WHERE t.consulationDoctorDetailID = :consulationDoctorDetailID"),
    @NamedQuery(name = "TbConsulationDoctorDetail.findByStatusDetail", query = "SELECT t FROM TbConsulationDoctorDetail t WHERE t.statusDetail = :statusDetail"),
    @NamedQuery(name = "TbConsulationDoctorDetail.findByCreateDate", query = "SELECT t FROM TbConsulationDoctorDetail t WHERE t.createDate = :createDate")})
public class TbConsulationDoctorDetail implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "ConsulationDoctorDetailID")
    private String consulationDoctorDetailID;
    @Column(name = "StatusDetail")
    private Boolean statusDetail;
    @Column(name = "CreateDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @JoinColumn(name = "PositionID", referencedColumnName = "PositionID")
    @ManyToOne
    private TbPositionConsulation positionID;
    @JoinColumn(name = "UserID", referencedColumnName = "EmployeeID")
    @ManyToOne
    private TbEmployee userID;
    @JoinColumn(name = "DoctorID", referencedColumnName = "EmployeeID")
    @ManyToOne
    private TbEmployee doctorID;
    @JoinColumn(name = "ConsulationID", referencedColumnName = "ConsulationID")
    @ManyToOne
    private TbConsulationSurgery consulationID;

    public TbConsulationDoctorDetail() {
    }

    public TbConsulationDoctorDetail(String consulationDoctorDetailID) {
        this.consulationDoctorDetailID = consulationDoctorDetailID;
    }

    public String getConsulationDoctorDetailID() {
        return consulationDoctorDetailID;
    }

    public void setConsulationDoctorDetailID(String consulationDoctorDetailID) {
        this.consulationDoctorDetailID = consulationDoctorDetailID;
    }

    public Boolean getStatusDetail() {
        return statusDetail;
    }

    public void setStatusDetail(Boolean statusDetail) {
        this.statusDetail = statusDetail;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public TbPositionConsulation getPositionID() {
        return positionID;
    }

    public void setPositionID(TbPositionConsulation positionID) {
        this.positionID = positionID;
    }

    public TbEmployee getUserID() {
        return userID;
    }

    public void setUserID(TbEmployee userID) {
        this.userID = userID;
    }

    public TbEmployee getDoctorID() {
        return doctorID;
    }

    public void setDoctorID(TbEmployee doctorID) {
        this.doctorID = doctorID;
    }

    public TbConsulationSurgery getConsulationID() {
        return consulationID;
    }

    public void setConsulationID(TbConsulationSurgery consulationID) {
        this.consulationID = consulationID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (consulationDoctorDetailID != null ? consulationDoctorDetailID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbConsulationDoctorDetail)) {
            return false;
        }
        TbConsulationDoctorDetail other = (TbConsulationDoctorDetail) object;
        if ((this.consulationDoctorDetailID == null && other.consulationDoctorDetailID != null) || (this.consulationDoctorDetailID != null && !this.consulationDoctorDetailID.equals(other.consulationDoctorDetailID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "project.ett.TbConsulationDoctorDetail[ consulationDoctorDetailID=" + consulationDoctorDetailID + " ]";
    }
    
}
