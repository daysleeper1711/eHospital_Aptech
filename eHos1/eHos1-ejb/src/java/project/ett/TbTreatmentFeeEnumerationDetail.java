/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project.ett;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DaySLeePer
 */
@Entity
@Table(name = "tbTreatmentFeeEnumerationDetail")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbTreatmentFeeEnumerationDetail.findAll", query = "SELECT t FROM TbTreatmentFeeEnumerationDetail t"),
    @NamedQuery(name = "TbTreatmentFeeEnumerationDetail.findByTFEDetailID", query = "SELECT t FROM TbTreatmentFeeEnumerationDetail t WHERE t.tFEDetailID = :tFEDetailID"),
    @NamedQuery(name = "TbTreatmentFeeEnumerationDetail.findByQuantity", query = "SELECT t FROM TbTreatmentFeeEnumerationDetail t WHERE t.quantity = :quantity"),
    @NamedQuery(name = "TbTreatmentFeeEnumerationDetail.findByTotal", query = "SELECT t FROM TbTreatmentFeeEnumerationDetail t WHERE t.total = :total"),
    @NamedQuery(name = "TbTreatmentFeeEnumerationDetail.findByStatus", query = "SELECT t FROM TbTreatmentFeeEnumerationDetail t WHERE t.status = :status")})
public class TbTreatmentFeeEnumerationDetail implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
//    @Basic(optional = false)
//    @NotNull
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "TFE_DetailID")
    private Integer tFEDetailID;
    @Column(name = "Quantity")
    private Integer quantity;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "Total")
    private Double total;
    @Size(max = 30)
    @Column(name = "Status")
    private String status;
    @JoinColumn(name = "TFE_ID", referencedColumnName = "TFE_ID")
    @ManyToOne
    private TbTreatmentFeeEnumeration tfeId;
    @JoinColumn(name = "ServiceID", referencedColumnName = "ServiceID")
    @ManyToOne
    private TbService serviceID;

    public TbTreatmentFeeEnumerationDetail() {
    }

    public TbTreatmentFeeEnumerationDetail(Integer tFEDetailID) {
        this.tFEDetailID = tFEDetailID;
    }

    public Integer getTFEDetailID() {
        return tFEDetailID;
    }

    public void setTFEDetailID(Integer tFEDetailID) {
        this.tFEDetailID = tFEDetailID;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public TbTreatmentFeeEnumeration getTfeId() {
        return tfeId;
    }

    public void setTfeId(TbTreatmentFeeEnumeration tfeId) {
        this.tfeId = tfeId;
    }

    public TbService getServiceID() {
        return serviceID;
    }

    public void setServiceID(TbService serviceID) {
        this.serviceID = serviceID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tFEDetailID != null ? tFEDetailID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbTreatmentFeeEnumerationDetail)) {
            return false;
        }
        TbTreatmentFeeEnumerationDetail other = (TbTreatmentFeeEnumerationDetail) object;
        if ((this.tFEDetailID == null && other.tFEDetailID != null) || (this.tFEDetailID != null && !this.tFEDetailID.equals(other.tFEDetailID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "project.ett.TbTreatmentFeeEnumerationDetail[ tFEDetailID=" + tFEDetailID + " ]";
    }
    
}
