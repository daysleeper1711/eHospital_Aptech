/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project.ett;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DaySLeePer
 */
@Entity
@Table(name = "tbSurgeryRecord")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbSurgeryRecord.findAll", query = "SELECT t FROM TbSurgeryRecord t"),
    @NamedQuery(name = "TbSurgeryRecord.findBySurgeryRecordID", query = "SELECT t FROM TbSurgeryRecord t WHERE t.surgeryRecordID = :surgeryRecordID"),
    @NamedQuery(name = "TbSurgeryRecord.findByProcessOfSurgery", query = "SELECT t FROM TbSurgeryRecord t WHERE t.processOfSurgery = :processOfSurgery"),
    @NamedQuery(name = "TbSurgeryRecord.findByTimeOfStart", query = "SELECT t FROM TbSurgeryRecord t WHERE t.timeOfStart = :timeOfStart"),
    @NamedQuery(name = "TbSurgeryRecord.findByTimeOfEnd", query = "SELECT t FROM TbSurgeryRecord t WHERE t.timeOfEnd = :timeOfEnd"),
    @NamedQuery(name = "TbSurgeryRecord.findBySurgeryResult", query = "SELECT t FROM TbSurgeryRecord t WHERE t.surgeryResult = :surgeryResult"),
    @NamedQuery(name = "TbSurgeryRecord.findByBeginCost", query = "SELECT t FROM TbSurgeryRecord t WHERE t.beginCost = :beginCost"),
    @NamedQuery(name = "TbSurgeryRecord.findByPayCost", query = "SELECT t FROM TbSurgeryRecord t WHERE t.payCost = :payCost"),
    @NamedQuery(name = "TbSurgeryRecord.findByStatusSurgery", query = "SELECT t FROM TbSurgeryRecord t WHERE t.statusSurgery = :statusSurgery"),
    @NamedQuery(name = "TbSurgeryRecord.findByCreateDate", query = "SELECT t FROM TbSurgeryRecord t WHERE t.createDate = :createDate")})
public class TbSurgeryRecord implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "SurgeryRecordID")
    private String surgeryRecordID;
    @Size(max = 250)
    @Column(name = "ProcessOfSurgery")
    private String processOfSurgery;
    @Column(name = "TimeOfStart")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timeOfStart;
    @Column(name = "TimeOfEnd")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timeOfEnd;
    @Size(max = 250)
    @Column(name = "SurgeryResult")
    private String surgeryResult;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "BeginCost")
    private Double beginCost;
    @Column(name = "PayCost")
    private Double payCost;
    @Column(name = "StatusSurgery")
    private Boolean statusSurgery;
    @Column(name = "CreateDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @OneToMany(mappedBy = "surgeryRecordID")
    private Collection<TbOperatorDoctorDetail> tbOperatorDoctorDetailCollection;
    @JoinColumn(name = "SuggestSurgeryID", referencedColumnName = "SuggestSurgeryID")
    @ManyToOne
    private TbSuggestSurgery suggestSurgeryID;
    @JoinColumn(name = "UserID", referencedColumnName = "EmployeeID")
    @ManyToOne
    private TbEmployee userID;
    @OneToMany(mappedBy = "surgeryRecordID")
    private Collection<TbDrugsDetail> tbDrugsDetailCollection;
    @OneToMany(mappedBy = "surgeryRecordID")
    private Collection<TbSuppliesDetail> tbSuppliesDetailCollection;

    public TbSurgeryRecord() {
    }

    public TbSurgeryRecord(String surgeryRecordID) {
        this.surgeryRecordID = surgeryRecordID;
    }

    public String getSurgeryRecordID() {
        return surgeryRecordID;
    }

    public void setSurgeryRecordID(String surgeryRecordID) {
        this.surgeryRecordID = surgeryRecordID;
    }

    public String getProcessOfSurgery() {
        return processOfSurgery;
    }

    public void setProcessOfSurgery(String processOfSurgery) {
        this.processOfSurgery = processOfSurgery;
    }

    public Date getTimeOfStart() {
        return timeOfStart;
    }

    public void setTimeOfStart(Date timeOfStart) {
        this.timeOfStart = timeOfStart;
    }

    public Date getTimeOfEnd() {
        return timeOfEnd;
    }

    public void setTimeOfEnd(Date timeOfEnd) {
        this.timeOfEnd = timeOfEnd;
    }

    public String getSurgeryResult() {
        return surgeryResult;
    }

    public void setSurgeryResult(String surgeryResult) {
        this.surgeryResult = surgeryResult;
    }

    public Double getBeginCost() {
        return beginCost;
    }

    public void setBeginCost(Double beginCost) {
        this.beginCost = beginCost;
    }

    public Double getPayCost() {
        return payCost;
    }

    public void setPayCost(Double payCost) {
        this.payCost = payCost;
    }

    public Boolean getStatusSurgery() {
        return statusSurgery;
    }

    public void setStatusSurgery(Boolean statusSurgery) {
        this.statusSurgery = statusSurgery;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @XmlTransient
    public Collection<TbOperatorDoctorDetail> getTbOperatorDoctorDetailCollection() {
        return tbOperatorDoctorDetailCollection;
    }

    public void setTbOperatorDoctorDetailCollection(Collection<TbOperatorDoctorDetail> tbOperatorDoctorDetailCollection) {
        this.tbOperatorDoctorDetailCollection = tbOperatorDoctorDetailCollection;
    }

    public TbSuggestSurgery getSuggestSurgeryID() {
        return suggestSurgeryID;
    }

    public void setSuggestSurgeryID(TbSuggestSurgery suggestSurgeryID) {
        this.suggestSurgeryID = suggestSurgeryID;
    }

    public TbEmployee getUserID() {
        return userID;
    }

    public void setUserID(TbEmployee userID) {
        this.userID = userID;
    }

    @XmlTransient
    public Collection<TbDrugsDetail> getTbDrugsDetailCollection() {
        return tbDrugsDetailCollection;
    }

    public void setTbDrugsDetailCollection(Collection<TbDrugsDetail> tbDrugsDetailCollection) {
        this.tbDrugsDetailCollection = tbDrugsDetailCollection;
    }

    @XmlTransient
    public Collection<TbSuppliesDetail> getTbSuppliesDetailCollection() {
        return tbSuppliesDetailCollection;
    }

    public void setTbSuppliesDetailCollection(Collection<TbSuppliesDetail> tbSuppliesDetailCollection) {
        this.tbSuppliesDetailCollection = tbSuppliesDetailCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (surgeryRecordID != null ? surgeryRecordID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbSurgeryRecord)) {
            return false;
        }
        TbSurgeryRecord other = (TbSurgeryRecord) object;
        if ((this.surgeryRecordID == null && other.surgeryRecordID != null) || (this.surgeryRecordID != null && !this.surgeryRecordID.equals(other.surgeryRecordID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "project.ett.TbSurgeryRecord[ surgeryRecordID=" + surgeryRecordID + " ]";
    }
    
}
