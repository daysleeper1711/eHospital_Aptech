/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project.ett;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DaySLeePer
 */
@Entity
@Table(name = "tbDepositPayment")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbDepositPayment.findAll", query = "SELECT t FROM TbDepositPayment t"),
    @NamedQuery(name = "TbDepositPayment.findByDepositID", query = "SELECT t FROM TbDepositPayment t WHERE t.depositID = :depositID"),
    @NamedQuery(name = "TbDepositPayment.findByAmount", query = "SELECT t FROM TbDepositPayment t WHERE t.amount = :amount"),
    @NamedQuery(name = "TbDepositPayment.findByRemaining", query = "SELECT t FROM TbDepositPayment t WHERE t.remaining = :remaining"),
    @NamedQuery(name = "TbDepositPayment.findByReceivedDate", query = "SELECT t FROM TbDepositPayment t WHERE t.receivedDate = :receivedDate"),
    @NamedQuery(name = "TbDepositPayment.findByStatus", query = "SELECT t FROM TbDepositPayment t WHERE t.status = :status")})
public class TbDepositPayment implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "DepositID")
    private Integer depositID;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "Amount")
    private Double amount;
    @Column(name = "Remaining")
    private Double remaining;
    @Column(name = "ReceivedDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date receivedDate;
    @Size(max = 30)
    @Column(name = "Status")
    private String status;
    @JoinColumn(name = "MedRecordID", referencedColumnName = "MedRecordID")
    @ManyToOne
    private TbMedicalRecord medRecordID;
    @JoinColumn(name = "ReceivedByWhom", referencedColumnName = "EmployeeID")
    @ManyToOne
    private TbEmployee receivedByWhom;

    public TbDepositPayment() {
    }

    public TbDepositPayment(Integer depositID) {
        this.depositID = depositID;
    }

    public Integer getDepositID() {
        return depositID;
    }

    public void setDepositID(Integer depositID) {
        this.depositID = depositID;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getRemaining() {
        return remaining;
    }

    public void setRemaining(Double remaining) {
        this.remaining = remaining;
    }

    public Date getReceivedDate() {
        return receivedDate;
    }

    public void setReceivedDate(Date receivedDate) {
        this.receivedDate = receivedDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public TbMedicalRecord getMedRecordID() {
        return medRecordID;
    }

    public void setMedRecordID(TbMedicalRecord medRecordID) {
        this.medRecordID = medRecordID;
    }

    public TbEmployee getReceivedByWhom() {
        return receivedByWhom;
    }

    public void setReceivedByWhom(TbEmployee receivedByWhom) {
        this.receivedByWhom = receivedByWhom;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (depositID != null ? depositID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbDepositPayment)) {
            return false;
        }
        TbDepositPayment other = (TbDepositPayment) object;
        if ((this.depositID == null && other.depositID != null) || (this.depositID != null && !this.depositID.equals(other.depositID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "project.ett.TbDepositPayment[ depositID=" + depositID + " ]";
    }
    
}
