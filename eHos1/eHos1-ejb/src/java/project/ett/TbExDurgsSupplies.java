/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project.ett;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DaySLeePer
 */
@Entity
@Table(name = "tbEx_Durgs_Supplies")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbExDurgsSupplies.findAll", query = "SELECT t FROM TbExDurgsSupplies t"),
    @NamedQuery(name = "TbExDurgsSupplies.findByExID", query = "SELECT t FROM TbExDurgsSupplies t WHERE t.exID = :exID"),
    @NamedQuery(name = "TbExDurgsSupplies.findByNote", query = "SELECT t FROM TbExDurgsSupplies t WHERE t.note = :note"),
    @NamedQuery(name = "TbExDurgsSupplies.findByCrDate", query = "SELECT t FROM TbExDurgsSupplies t WHERE t.crDate = :crDate"),
    @NamedQuery(name = "TbExDurgsSupplies.findByCreateby", query = "SELECT t FROM TbExDurgsSupplies t WHERE t.createby = :createby"),
    @NamedQuery(name = "TbExDurgsSupplies.findByUpdateby", query = "SELECT t FROM TbExDurgsSupplies t WHERE t.updateby = :updateby"),
    @NamedQuery(name = "TbExDurgsSupplies.findByUpDate", query = "SELECT t FROM TbExDurgsSupplies t WHERE t.upDate = :upDate"),
    @NamedQuery(name = "TbExDurgsSupplies.findByDocentry", query = "SELECT t FROM TbExDurgsSupplies t WHERE t.docentry = :docentry"),
    @NamedQuery(name = "TbExDurgsSupplies.findByTypeEx", query = "SELECT t FROM TbExDurgsSupplies t WHERE t.typeEx = :typeEx"),
    @NamedQuery(name = "TbExDurgsSupplies.findByUStatus", query = "SELECT t FROM TbExDurgsSupplies t WHERE t.uStatus = :uStatus"),
    @NamedQuery(name = "TbExDurgsSupplies.findByRetrun", query = "SELECT t FROM TbExDurgsSupplies t WHERE t.retrun = :retrun")})
public class TbExDurgsSupplies implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "Ex_ID")
    private String exID;
    @Size(max = 200)
    @Column(name = "Note")
    private String note;
    @Column(name = "Cr_Date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date crDate;
    @Size(max = 20)
    @Column(name = "Createby")
    private String createby;
    @Size(max = 20)
    @Column(name = "Updateby")
    private String updateby;
    @Column(name = "Up_Date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date upDate;
    @Size(max = 20)
    @Column(name = "Docentry")
    private String docentry;
    @Size(max = 2)
    @Column(name = "Type_Ex")
    private String typeEx;
    @Size(max = 2)
    @Column(name = "U_Status")
    private String uStatus;
    @Column(name = "Retrun")
    private Boolean retrun;
    @OneToMany(mappedBy = "exID",cascade = CascadeType.ALL)
    private Collection<TbExDurgsSuppliesDetails> tbExDurgsSuppliesDetailsCollection;

    public TbExDurgsSupplies() {
    }

    public TbExDurgsSupplies(String exID) {
        this.exID = exID;
    }

    public String getExID() {
        return exID;
    }

    public void setExID(String exID) {
        this.exID = exID;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Date getCrDate() {
        return crDate;
    }

    public void setCrDate(Date crDate) {
        this.crDate = crDate;
    }

    public String getCreateby() {
        return createby;
    }

    public void setCreateby(String createby) {
        this.createby = createby;
    }

    public String getUpdateby() {
        return updateby;
    }

    public void setUpdateby(String updateby) {
        this.updateby = updateby;
    }

    public Date getUpDate() {
        return upDate;
    }

    public void setUpDate(Date upDate) {
        this.upDate = upDate;
    }

    public String getDocentry() {
        return docentry;
    }

    public void setDocentry(String docentry) {
        this.docentry = docentry;
    }

    public String getTypeEx() {
        return typeEx;
    }

    public void setTypeEx(String typeEx) {
        this.typeEx = typeEx;
    }

    public String getUStatus() {
        return uStatus;
    }

    public void setUStatus(String uStatus) {
        this.uStatus = uStatus;
    }

    public Boolean getRetrun() {
        return retrun;
    }

    public void setRetrun(Boolean retrun) {
        this.retrun = retrun;
    }

    @XmlTransient
    public Collection<TbExDurgsSuppliesDetails> getTbExDurgsSuppliesDetailsCollection() {
        return tbExDurgsSuppliesDetailsCollection;
    }

    public void setTbExDurgsSuppliesDetailsCollection(Collection<TbExDurgsSuppliesDetails> tbExDurgsSuppliesDetailsCollection) {
        this.tbExDurgsSuppliesDetailsCollection = tbExDurgsSuppliesDetailsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (exID != null ? exID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbExDurgsSupplies)) {
            return false;
        }
        TbExDurgsSupplies other = (TbExDurgsSupplies) object;
        if ((this.exID == null && other.exID != null) || (this.exID != null && !this.exID.equals(other.exID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "project.ett.TbExDurgsSupplies[ exID=" + exID + " ]";
    }
    
}
