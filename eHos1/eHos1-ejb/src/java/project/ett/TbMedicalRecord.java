/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project.ett;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DaySLeePer
 */
@Entity
@Table(name = "tbMedicalRecord")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbMedicalRecord.findAll", query = "SELECT t FROM TbMedicalRecord t"),
    @NamedQuery(name = "TbMedicalRecord.findByMedRecordID", query = "SELECT t FROM TbMedicalRecord t WHERE t.medRecordID = :medRecordID"),
    @NamedQuery(name = "TbMedicalRecord.findBySymptoms", query = "SELECT t FROM TbMedicalRecord t WHERE t.symptoms = :symptoms"),
    @NamedQuery(name = "TbMedicalRecord.findByCreateDate", query = "SELECT t FROM TbMedicalRecord t WHERE t.createDate = :createDate"),
    @NamedQuery(name = "TbMedicalRecord.findByDiagnosis", query = "SELECT t FROM TbMedicalRecord t WHERE t.diagnosis = :diagnosis"),
    @NamedQuery(name = "TbMedicalRecord.findBySuggestTreatment", query = "SELECT t FROM TbMedicalRecord t WHERE t.suggestTreatment = :suggestTreatment"),
    @NamedQuery(name = "TbMedicalRecord.findByDiagnosisDate", query = "SELECT t FROM TbMedicalRecord t WHERE t.diagnosisDate = :diagnosisDate"),
    @NamedQuery(name = "TbMedicalRecord.findByDischargeStatus", query = "SELECT t FROM TbMedicalRecord t WHERE t.dischargeStatus = :dischargeStatus"),
    @NamedQuery(name = "TbMedicalRecord.findByMainDisease", query = "SELECT t FROM TbMedicalRecord t WHERE t.mainDisease = :mainDisease"),
    @NamedQuery(name = "TbMedicalRecord.findBySideDisease", query = "SELECT t FROM TbMedicalRecord t WHERE t.sideDisease = :sideDisease"),
    @NamedQuery(name = "TbMedicalRecord.findByTreatmentResult", query = "SELECT t FROM TbMedicalRecord t WHERE t.treatmentResult = :treatmentResult"),
    @NamedQuery(name = "TbMedicalRecord.findByHealthCondition", query = "SELECT t FROM TbMedicalRecord t WHERE t.healthCondition = :healthCondition"),
    @NamedQuery(name = "TbMedicalRecord.findByConclusionDate", query = "SELECT t FROM TbMedicalRecord t WHERE t.conclusionDate = :conclusionDate"),
    @NamedQuery(name = "TbMedicalRecord.findByIsDeposit", query = "SELECT t FROM TbMedicalRecord t WHERE t.isDeposit = :isDeposit"),
    @NamedQuery(name = "TbMedicalRecord.findByStatus", query = "SELECT t FROM TbMedicalRecord t WHERE t.status = :status")})
public class TbMedicalRecord implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "MedRecordID")
    private String medRecordID;
    @Size(max = 200)
    @Column(name = "Symptoms")
    private String symptoms;
    @Column(name = "CreateDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Size(max = 200)
    @Column(name = "Diagnosis")
    private String diagnosis;
    @Size(max = 200)
    @Column(name = "SuggestTreatment")
    private String suggestTreatment;
    @Column(name = "DiagnosisDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date diagnosisDate;
    @Size(max = 50)
    @Column(name = "DischargeStatus")
    private String dischargeStatus;
    @Size(max = 50)
    @Column(name = "MainDisease")
    private String mainDisease;
    @Size(max = 50)
    @Column(name = "SideDisease")
    private String sideDisease;
    @Size(max = 50)
    @Column(name = "TreatmentResult")
    private String treatmentResult;
    @Size(max = 50)
    @Column(name = "HealthCondition")
    private String healthCondition;
    @Column(name = "ConclusionDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date conclusionDate;
    @Column(name = "IsDeposit")
    private Boolean isDeposit;
    @Size(max = 30)
    @Column(name = "Status")
    private String status;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "medRecordID")
    private Collection<TbFacultyChangementHistory> tbFacultyChangementHistoryCollection;
    @JoinColumn(name = "PatID", referencedColumnName = "PatID")
    @ManyToOne
    private TbPatient patID;
    @JoinColumn(name = "ConclusionByWhom", referencedColumnName = "EmployeeID")
    @ManyToOne
    private TbEmployee conclusionByWhom;
    @JoinColumn(name = "DiagnosisDoc", referencedColumnName = "EmployeeID")
    @ManyToOne
    private TbEmployee diagnosisDoc;
    @JoinColumn(name = "CreateByWhom", referencedColumnName = "EmployeeID")
    @ManyToOne
    private TbEmployee createByWhom;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "medRecordID")
    private Collection<TbDepositPayment> tbDepositPaymentCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "medicalRecordID")
    private Collection<TbMedicineSupplies> tbMedicineSuppliesCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "medRecordID")
    private Collection<TbTreatmentFeeEnumeration> tbTreatmentFeeEnumerationCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "mrId")
    private Collection<TbTestRequest> tbTestRequestCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "medicalRecordID")
    private Collection<TbPrescription> tbPrescriptionCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "medicalRecordID")
    private Collection<TbSuggestSurgery> tbSuggestSurgeryCollection;

    public TbMedicalRecord() {
    }

    public TbMedicalRecord(String medRecordID) {
        this.medRecordID = medRecordID;
    }

    public String getMedRecordID() {
        return medRecordID;
    }

    public void setMedRecordID(String medRecordID) {
        this.medRecordID = medRecordID;
    }

    public String getSymptoms() {
        return symptoms;
    }

    public void setSymptoms(String symptoms) {
        this.symptoms = symptoms;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getSuggestTreatment() {
        return suggestTreatment;
    }

    public void setSuggestTreatment(String suggestTreatment) {
        this.suggestTreatment = suggestTreatment;
    }

    public Date getDiagnosisDate() {
        return diagnosisDate;
    }

    public void setDiagnosisDate(Date diagnosisDate) {
        this.diagnosisDate = diagnosisDate;
    }

    public String getDischargeStatus() {
        return dischargeStatus;
    }

    public void setDischargeStatus(String dischargeStatus) {
        this.dischargeStatus = dischargeStatus;
    }

    public String getMainDisease() {
        return mainDisease;
    }

    public void setMainDisease(String mainDisease) {
        this.mainDisease = mainDisease;
    }

    public String getSideDisease() {
        return sideDisease;
    }

    public void setSideDisease(String sideDisease) {
        this.sideDisease = sideDisease;
    }

    public String getTreatmentResult() {
        return treatmentResult;
    }

    public void setTreatmentResult(String treatmentResult) {
        this.treatmentResult = treatmentResult;
    }

    public String getHealthCondition() {
        return healthCondition;
    }

    public void setHealthCondition(String healthCondition) {
        this.healthCondition = healthCondition;
    }

    public Date getConclusionDate() {
        return conclusionDate;
    }

    public void setConclusionDate(Date conclusionDate) {
        this.conclusionDate = conclusionDate;
    }

    public Boolean getIsDeposit() {
        return isDeposit;
    }

    public void setIsDeposit(Boolean isDeposit) {
        this.isDeposit = isDeposit;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @XmlTransient
    public Collection<TbFacultyChangementHistory> getTbFacultyChangementHistoryCollection() {
        return tbFacultyChangementHistoryCollection;
    }

    public void setTbFacultyChangementHistoryCollection(Collection<TbFacultyChangementHistory> tbFacultyChangementHistoryCollection) {
        this.tbFacultyChangementHistoryCollection = tbFacultyChangementHistoryCollection;
    }

    public TbPatient getPatID() {
        return patID;
    }

    public void setPatID(TbPatient patID) {
        this.patID = patID;
    }

    public TbEmployee getConclusionByWhom() {
        return conclusionByWhom;
    }

    public void setConclusionByWhom(TbEmployee conclusionByWhom) {
        this.conclusionByWhom = conclusionByWhom;
    }

    public TbEmployee getDiagnosisDoc() {
        return diagnosisDoc;
    }

    public void setDiagnosisDoc(TbEmployee diagnosisDoc) {
        this.diagnosisDoc = diagnosisDoc;
    }

    public TbEmployee getCreateByWhom() {
        return createByWhom;
    }

    public void setCreateByWhom(TbEmployee createByWhom) {
        this.createByWhom = createByWhom;
    }

    @XmlTransient
    public Collection<TbDepositPayment> getTbDepositPaymentCollection() {
        return tbDepositPaymentCollection;
    }

    public void setTbDepositPaymentCollection(Collection<TbDepositPayment> tbDepositPaymentCollection) {
        this.tbDepositPaymentCollection = tbDepositPaymentCollection;
    }

    @XmlTransient
    public Collection<TbMedicineSupplies> getTbMedicineSuppliesCollection() {
        return tbMedicineSuppliesCollection;
    }

    public void setTbMedicineSuppliesCollection(Collection<TbMedicineSupplies> tbMedicineSuppliesCollection) {
        this.tbMedicineSuppliesCollection = tbMedicineSuppliesCollection;
    }

    @XmlTransient
    public Collection<TbTreatmentFeeEnumeration> getTbTreatmentFeeEnumerationCollection() {
        return tbTreatmentFeeEnumerationCollection;
    }

    public void setTbTreatmentFeeEnumerationCollection(Collection<TbTreatmentFeeEnumeration> tbTreatmentFeeEnumerationCollection) {
        this.tbTreatmentFeeEnumerationCollection = tbTreatmentFeeEnumerationCollection;
    }

    @XmlTransient
    public Collection<TbTestRequest> getTbTestRequestCollection() {
        return tbTestRequestCollection;
    }

    public void setTbTestRequestCollection(Collection<TbTestRequest> tbTestRequestCollection) {
        this.tbTestRequestCollection = tbTestRequestCollection;
    }

    @XmlTransient
    public Collection<TbPrescription> getTbPrescriptionCollection() {
        return tbPrescriptionCollection;
    }

    public void setTbPrescriptionCollection(Collection<TbPrescription> tbPrescriptionCollection) {
        this.tbPrescriptionCollection = tbPrescriptionCollection;
    }

    @XmlTransient
    public Collection<TbSuggestSurgery> getTbSuggestSurgeryCollection() {
        return tbSuggestSurgeryCollection;
    }

    public void setTbSuggestSurgeryCollection(Collection<TbSuggestSurgery> tbSuggestSurgeryCollection) {
        this.tbSuggestSurgeryCollection = tbSuggestSurgeryCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (medRecordID != null ? medRecordID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbMedicalRecord)) {
            return false;
        }
        TbMedicalRecord other = (TbMedicalRecord) object;
        if ((this.medRecordID == null && other.medRecordID != null) || (this.medRecordID != null && !this.medRecordID.equals(other.medRecordID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "project.ett.TbMedicalRecord[ medRecordID=" + medRecordID + " ]";
    }
    
}
