/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project.ett;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DaySLeePer
 */
@Entity
@Table(name = "tbPatient")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbPatient.findAll", query = "SELECT t FROM TbPatient t"),
    @NamedQuery(name = "TbPatient.findByPatID", query = "SELECT t FROM TbPatient t WHERE t.patID = :patID"),
    @NamedQuery(name = "TbPatient.findByPatName", query = "SELECT t FROM TbPatient t WHERE t.patName = :patName"),
    @NamedQuery(name = "TbPatient.findByPassword", query = "SELECT t FROM TbPatient t WHERE t.password = :password"),
    @NamedQuery(name = "TbPatient.findByDob", query = "SELECT t FROM TbPatient t WHERE t.dob = :dob"),
    @NamedQuery(name = "TbPatient.findByGender", query = "SELECT t FROM TbPatient t WHERE t.gender = :gender"),
    @NamedQuery(name = "TbPatient.findByAddress", query = "SELECT t FROM TbPatient t WHERE t.address = :address"),
    @NamedQuery(name = "TbPatient.findByIDCard", query = "SELECT t FROM TbPatient t WHERE t.iDCard = :iDCard"),
    @NamedQuery(name = "TbPatient.findByPhone", query = "SELECT t FROM TbPatient t WHERE t.phone = :phone"),
    @NamedQuery(name = "TbPatient.findByRegDate", query = "SELECT t FROM TbPatient t WHERE t.regDate = :regDate"),
    @NamedQuery(name = "TbPatient.findByUpdateDate", query = "SELECT t FROM TbPatient t WHERE t.updateDate = :updateDate")})
public class TbPatient implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "PatID")
    private String patID;
    @Size(max = 100)
    @Column(name = "PatName")
    private String patName;
    @Size(max = 100)
    @Column(name = "Password")
    private String password;
    @Column(name = "Dob")
    @Temporal(TemporalType.DATE)
    private Date dob;
    @Column(name = "Gender")
    private Boolean gender;
    @Size(max = 150)
    @Column(name = "Address")
    private String address;
    @Size(max = 20)
    @Column(name = "ID_Card")
    private String iDCard;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Size(max = 20)
    @Column(name = "Phone")
    private String phone;
    @Column(name = "RegDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date regDate;
    @Column(name = "UpdateDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "patID")
    private Collection<TbKQKB> tbKQKBCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "patID")
    private Collection<TbMedicalRecord> tbMedicalRecordCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "patID")
    private Collection<TbOutPatient> tbOutPatientCollection;
    @JoinColumn(name = "UpdateByWhom", referencedColumnName = "EmployeeID")
    @ManyToOne
    private TbEmployee updateByWhom;
    @JoinColumn(name = "RegByWhom", referencedColumnName = "EmployeeID")
    @ManyToOne
    private TbEmployee regByWhom;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "patientID")
    private Collection<TbInsurane> tbInsuraneCollection;

    public TbPatient() {
    }

    public TbPatient(String patID) {
        this.patID = patID;
    }

    public String getPatID() {
        return patID;
    }

    public void setPatID(String patID) {
        this.patID = patID;
    }

    public String getPatName() {
        return patName;
    }

    public void setPatName(String patName) {
        this.patName = patName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public Boolean getGender() {
        return gender;
    }

    public void setGender(Boolean gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getIDCard() {
        return iDCard;
    }

    public void setIDCard(String iDCard) {
        this.iDCard = iDCard;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getRegDate() {
        return regDate;
    }

    public void setRegDate(Date regDate) {
        this.regDate = regDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    @XmlTransient
    public Collection<TbKQKB> getTbKQKBCollection() {
        return tbKQKBCollection;
    }

    public void setTbKQKBCollection(Collection<TbKQKB> tbKQKBCollection) {
        this.tbKQKBCollection = tbKQKBCollection;
    }

    @XmlTransient
    public Collection<TbMedicalRecord> getTbMedicalRecordCollection() {
        return tbMedicalRecordCollection;
    }

    public void setTbMedicalRecordCollection(Collection<TbMedicalRecord> tbMedicalRecordCollection) {
        this.tbMedicalRecordCollection = tbMedicalRecordCollection;
    }

    @XmlTransient
    public Collection<TbOutPatient> getTbOutPatientCollection() {
        return tbOutPatientCollection;
    }

    public void setTbOutPatientCollection(Collection<TbOutPatient> tbOutPatientCollection) {
        this.tbOutPatientCollection = tbOutPatientCollection;
    }

    public TbEmployee getUpdateByWhom() {
        return updateByWhom;
    }

    public void setUpdateByWhom(TbEmployee updateByWhom) {
        this.updateByWhom = updateByWhom;
    }

    public TbEmployee getRegByWhom() {
        return regByWhom;
    }

    public void setRegByWhom(TbEmployee regByWhom) {
        this.regByWhom = regByWhom;
    }

    @XmlTransient
    public Collection<TbInsurane> getTbInsuraneCollection() {
        return tbInsuraneCollection;
    }

    public void setTbInsuraneCollection(Collection<TbInsurane> tbInsuraneCollection) {
        this.tbInsuraneCollection = tbInsuraneCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (patID != null ? patID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbPatient)) {
            return false;
        }
        TbPatient other = (TbPatient) object;
        if ((this.patID == null && other.patID != null) || (this.patID != null && !this.patID.equals(other.patID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "project.ett.TbPatient[ patID=" + patID + " ]";
    }
    
}
