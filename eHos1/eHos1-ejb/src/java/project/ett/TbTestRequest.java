/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project.ett;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DaySLeePer
 */
@Entity
@Table(name = "tbTestRequest")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbTestRequest.findAll", query = "SELECT t FROM TbTestRequest t"),
    @NamedQuery(name = "TbTestRequest.findByTestReqID", query = "SELECT t FROM TbTestRequest t WHERE t.testReqID = :testReqID"),
    @NamedQuery(name = "TbTestRequest.findByDiagnosis", query = "SELECT t FROM TbTestRequest t WHERE t.diagnosis = :diagnosis"),
    @NamedQuery(name = "TbTestRequest.findByTotalPayment", query = "SELECT t FROM TbTestRequest t WHERE t.totalPayment = :totalPayment"),
    @NamedQuery(name = "TbTestRequest.findByCreateDate", query = "SELECT t FROM TbTestRequest t WHERE t.createDate = :createDate"),
    @NamedQuery(name = "TbTestRequest.findByUpdateDate", query = "SELECT t FROM TbTestRequest t WHERE t.updateDate = :updateDate"),
    @NamedQuery(name = "TbTestRequest.findByStatus", query = "SELECT t FROM TbTestRequest t WHERE t.status = :status")})
public class TbTestRequest implements Serializable, Comparable<TbTestRequest> {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "TestReqID")
    private String testReqID;
    @Size(max = 100)
    @Column(name = "Diagnosis")
    private String diagnosis;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "TotalPayment")
    private Double totalPayment;
    @Column(name = "CreateDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "UpdateDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @Size(max = 20)
    @Column(name = "Status")
    private String status;
    @OneToMany(cascade = CascadeType.ALL ,mappedBy = "testReqID")
    private Collection<TbTestReqDetail> tbTestReqDetailCollection;
    @JoinColumn(name = "OutPatID", referencedColumnName = "OutPatID")
    @ManyToOne
    private TbOutPatient outPatID;
    @JoinColumn(name = "MR_ID", referencedColumnName = "MedRecordID")
    @ManyToOne
    private TbMedicalRecord mrId;
    @JoinColumn(name = "ToFac", referencedColumnName = "Fa_ID")
    @ManyToOne
    private TbFaculty toFac;
    @JoinColumn(name = "FromFac", referencedColumnName = "Fa_ID")
    @ManyToOne
    private TbFaculty fromFac;
    @JoinColumn(name = "UpdateEmp", referencedColumnName = "EmployeeID")
    @ManyToOne
    private TbEmployee updateEmp;
    @JoinColumn(name = "AppointedDoc", referencedColumnName = "EmployeeID")
    @ManyToOne
    private TbEmployee appointedDoc;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "testReqID")
    private Collection<TbTestResult> tbTestResultCollection;

    public TbTestRequest() {
    }

    public TbTestRequest(String testReqID) {
        this.testReqID = testReqID;
    }

    public String getTestReqID() {
        return testReqID;
    }

    public void setTestReqID(String testReqID) {
        this.testReqID = testReqID;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public Double getTotalPayment() {
        return totalPayment;
    }

    public void setTotalPayment(Double totalPayment) {
        this.totalPayment = totalPayment;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @XmlTransient
    public Collection<TbTestReqDetail> getTbTestReqDetailCollection() {
        return tbTestReqDetailCollection;
    }

    public void setTbTestReqDetailCollection(Collection<TbTestReqDetail> tbTestReqDetailCollection) {
        this.tbTestReqDetailCollection = tbTestReqDetailCollection;
    }

    public TbOutPatient getOutPatID() {
        return outPatID;
    }

    public void setOutPatID(TbOutPatient outPatID) {
        this.outPatID = outPatID;
    }

    public TbMedicalRecord getMrId() {
        return mrId;
    }

    public void setMrId(TbMedicalRecord mrId) {
        this.mrId = mrId;
    }

    public TbFaculty getToFac() {
        return toFac;
    }

    public void setToFac(TbFaculty toFac) {
        this.toFac = toFac;
    }

    public TbFaculty getFromFac() {
        return fromFac;
    }

    public void setFromFac(TbFaculty fromFac) {
        this.fromFac = fromFac;
    }

    public TbEmployee getUpdateEmp() {
        return updateEmp;
    }

    public void setUpdateEmp(TbEmployee updateEmp) {
        this.updateEmp = updateEmp;
    }

    public TbEmployee getAppointedDoc() {
        return appointedDoc;
    }

    public void setAppointedDoc(TbEmployee appointedDoc) {
        this.appointedDoc = appointedDoc;
    }

    @XmlTransient
    public Collection<TbTestResult> getTbTestResultCollection() {
        return tbTestResultCollection;
    }

    public void setTbTestResultCollection(Collection<TbTestResult> tbTestResultCollection) {
        this.tbTestResultCollection = tbTestResultCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (testReqID != null ? testReqID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbTestRequest)) {
            return false;
        }
        TbTestRequest other = (TbTestRequest) object;
        if ((this.testReqID == null && other.testReqID != null) || (this.testReqID != null && !this.testReqID.equals(other.testReqID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "project.ett.TbTestRequest[ testReqID=" + testReqID + " ]";
    }

    @Override
    public int compareTo(TbTestRequest o) {
        return (int) (o.createDate.getTime() - this.createDate.getTime());
    }
   
    
}
