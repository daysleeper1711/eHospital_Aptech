/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project.ett;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DaySLeePer
 */
@Entity
@Table(name = "tbSuggestSurgery")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbSuggestSurgery.findAll", query = "SELECT t FROM TbSuggestSurgery t"),
    @NamedQuery(name = "TbSuggestSurgery.findBySuggestSurgeryID", query = "SELECT t FROM TbSuggestSurgery t WHERE t.suggestSurgeryID = :suggestSurgeryID"),
    @NamedQuery(name = "TbSuggestSurgery.findBySuggestDescription", query = "SELECT t FROM TbSuggestSurgery t WHERE t.suggestDescription = :suggestDescription"),
    @NamedQuery(name = "TbSuggestSurgery.findByStatusSuggest", query = "SELECT t FROM TbSuggestSurgery t WHERE t.statusSuggest = :statusSuggest"),
    @NamedQuery(name = "TbSuggestSurgery.findByCreateDate", query = "SELECT t FROM TbSuggestSurgery t WHERE t.createDate = :createDate")})
public class TbSuggestSurgery implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "SuggestSurgeryID")
    private String suggestSurgeryID;
    @Size(max = 250)
    @Column(name = "SuggestDescription")
    private String suggestDescription;
    @Column(name = "StatusSuggest")
    private Boolean statusSuggest;
    @Column(name = "CreateDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @OneToMany(mappedBy = "suggestSurgeryID")
    private Collection<TbScheduledSurgery> tbScheduledSurgeryCollection;
    @OneToMany(mappedBy = "suggestSurgeryID")
    private Collection<TbSurgeryRecord> tbSurgeryRecordCollection;
    @OneToMany(mappedBy = "suggestSurgeryID")
    private Collection<TbConsulationSurgery> tbConsulationSurgeryCollection;
    @JoinColumn(name = "SurgeryServiceID", referencedColumnName = "SurgeryServiceID")
    @ManyToOne
    private TbSurgeryService surgeryServiceID;
    @JoinColumn(name = "MedicalRecordID", referencedColumnName = "MedRecordID")
    @ManyToOne
    private TbMedicalRecord medicalRecordID;
    @JoinColumn(name = "UserID", referencedColumnName = "EmployeeID")
    @ManyToOne
    private TbEmployee userID;
    @JoinColumn(name = "DoctorID", referencedColumnName = "EmployeeID")
    @ManyToOne
    private TbEmployee doctorID;

    public TbSuggestSurgery() {
    }

    public TbSuggestSurgery(String suggestSurgeryID) {
        this.suggestSurgeryID = suggestSurgeryID;
    }

    public String getSuggestSurgeryID() {
        return suggestSurgeryID;
    }

    public void setSuggestSurgeryID(String suggestSurgeryID) {
        this.suggestSurgeryID = suggestSurgeryID;
    }

    public String getSuggestDescription() {
        return suggestDescription;
    }

    public void setSuggestDescription(String suggestDescription) {
        this.suggestDescription = suggestDescription;
    }

    public Boolean getStatusSuggest() {
        return statusSuggest;
    }

    public void setStatusSuggest(Boolean statusSuggest) {
        this.statusSuggest = statusSuggest;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @XmlTransient
    public Collection<TbScheduledSurgery> getTbScheduledSurgeryCollection() {
        return tbScheduledSurgeryCollection;
    }

    public void setTbScheduledSurgeryCollection(Collection<TbScheduledSurgery> tbScheduledSurgeryCollection) {
        this.tbScheduledSurgeryCollection = tbScheduledSurgeryCollection;
    }

    @XmlTransient
    public Collection<TbSurgeryRecord> getTbSurgeryRecordCollection() {
        return tbSurgeryRecordCollection;
    }

    public void setTbSurgeryRecordCollection(Collection<TbSurgeryRecord> tbSurgeryRecordCollection) {
        this.tbSurgeryRecordCollection = tbSurgeryRecordCollection;
    }

    @XmlTransient
    public Collection<TbConsulationSurgery> getTbConsulationSurgeryCollection() {
        return tbConsulationSurgeryCollection;
    }

    public void setTbConsulationSurgeryCollection(Collection<TbConsulationSurgery> tbConsulationSurgeryCollection) {
        this.tbConsulationSurgeryCollection = tbConsulationSurgeryCollection;
    }

    public TbSurgeryService getSurgeryServiceID() {
        return surgeryServiceID;
    }

    public void setSurgeryServiceID(TbSurgeryService surgeryServiceID) {
        this.surgeryServiceID = surgeryServiceID;
    }

    public TbMedicalRecord getMedicalRecordID() {
        return medicalRecordID;
    }

    public void setMedicalRecordID(TbMedicalRecord medicalRecordID) {
        this.medicalRecordID = medicalRecordID;
    }

    public TbEmployee getUserID() {
        return userID;
    }

    public void setUserID(TbEmployee userID) {
        this.userID = userID;
    }

    public TbEmployee getDoctorID() {
        return doctorID;
    }

    public void setDoctorID(TbEmployee doctorID) {
        this.doctorID = doctorID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (suggestSurgeryID != null ? suggestSurgeryID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbSuggestSurgery)) {
            return false;
        }
        TbSuggestSurgery other = (TbSuggestSurgery) object;
        if ((this.suggestSurgeryID == null && other.suggestSurgeryID != null) || (this.suggestSurgeryID != null && !this.suggestSurgeryID.equals(other.suggestSurgeryID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "project.ett.TbSuggestSurgery[ suggestSurgeryID=" + suggestSurgeryID + " ]";
    }
    
}
