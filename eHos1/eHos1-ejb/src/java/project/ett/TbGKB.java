/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project.ett;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DaySLeePer
 */
@Entity
@Table(name = "tbGKB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbGKB.findAll", query = "SELECT t FROM TbGKB t"),
    @NamedQuery(name = "TbGKB.findByGkbid", query = "SELECT t FROM TbGKB t WHERE t.gkbid = :gkbid"),
    @NamedQuery(name = "TbGKB.findByGKBName", query = "SELECT t FROM TbGKB t WHERE t.gKBName = :gKBName"),
    @NamedQuery(name = "TbGKB.findByCategory", query = "SELECT t FROM TbGKB t WHERE t.category = :category"),
    @NamedQuery(name = "TbGKB.findByGKBDescription", query = "SELECT t FROM TbGKB t WHERE t.gKBDescription = :gKBDescription"),
    @NamedQuery(name = "TbGKB.findByDateOfCreate", query = "SELECT t FROM TbGKB t WHERE t.dateOfCreate = :dateOfCreate"),
    @NamedQuery(name = "TbGKB.findByGKBStatus", query = "SELECT t FROM TbGKB t WHERE t.gKBStatus = :gKBStatus"),
    @NamedQuery(name = "TbGKB.findByCreator", query = "SELECT t FROM TbGKB t WHERE t.creator = :creator"),
    @NamedQuery(name = "TbGKB.findByUpdatedate", query = "SELECT t FROM TbGKB t WHERE t.updatedate = :updatedate"),
    @NamedQuery(name = "TbGKB.findByPrice", query = "SELECT t FROM TbGKB t WHERE t.price = :price"),
    @NamedQuery(name = "TbGKB.findByWhoUpdated", query = "SELECT t FROM TbGKB t WHERE t.whoUpdated = :whoUpdated")})
public class TbGKB implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "GKBID")
    private String gkbid;
    @Size(max = 50)
    @Column(name = "GKBName")
    private String gKBName;
    @Size(max = 80)
    @Column(name = "Category")
    private String category;
    @Size(max = 150)
    @Column(name = "GKBDescription")
    private String gKBDescription;
    @Column(name = "DateOfCreate")
    @Temporal(TemporalType.DATE)
    private Date dateOfCreate;
    @Size(max = 8)
    @Column(name = "GKBStatus")
    private String gKBStatus;
    @Size(max = 20)
    @Column(name = "Creator")
    private String creator;
    @Column(name = "Updatedate")
    @Temporal(TemporalType.DATE)
    private Date updatedate;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "Price")
    private Double price;
    @Size(max = 20)
    @Column(name = "WhoUpdated")
    private String whoUpdated;
    @OneToMany(mappedBy = "gkbid")
    private Collection<TbOutPatient> tbOutPatientCollection;
    @OneToMany(mappedBy = "gkbid")
    private Collection<TbRegisterDTO> tbRegisterDTOCollection;
    @OneToMany(mappedBy = "gkbid")
    private Collection<TbGKBDetail> tbGKBDetailCollection;

    public TbGKB() {
    }

    public TbGKB(String gkbid) {
        this.gkbid = gkbid;
    }

    public String getGkbid() {
        return gkbid;
    }

    public void setGkbid(String gkbid) {
        this.gkbid = gkbid;
    }

    public String getGKBName() {
        return gKBName;
    }

    public void setGKBName(String gKBName) {
        this.gKBName = gKBName;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getGKBDescription() {
        return gKBDescription;
    }

    public void setGKBDescription(String gKBDescription) {
        this.gKBDescription = gKBDescription;
    }

    public Date getDateOfCreate() {
        return dateOfCreate;
    }

    public void setDateOfCreate(Date dateOfCreate) {
        this.dateOfCreate = dateOfCreate;
    }

    public String getGKBStatus() {
        return gKBStatus;
    }

    public void setGKBStatus(String gKBStatus) {
        this.gKBStatus = gKBStatus;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public Date getUpdatedate() {
        return updatedate;
    }

    public void setUpdatedate(Date updatedate) {
        this.updatedate = updatedate;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getWhoUpdated() {
        return whoUpdated;
    }

    public void setWhoUpdated(String whoUpdated) {
        this.whoUpdated = whoUpdated;
    }

    @XmlTransient
    public Collection<TbOutPatient> getTbOutPatientCollection() {
        return tbOutPatientCollection;
    }

    public void setTbOutPatientCollection(Collection<TbOutPatient> tbOutPatientCollection) {
        this.tbOutPatientCollection = tbOutPatientCollection;
    }

    @XmlTransient
    public Collection<TbRegisterDTO> getTbRegisterDTOCollection() {
        return tbRegisterDTOCollection;
    }

    public void setTbRegisterDTOCollection(Collection<TbRegisterDTO> tbRegisterDTOCollection) {
        this.tbRegisterDTOCollection = tbRegisterDTOCollection;
    }

    @XmlTransient
    public Collection<TbGKBDetail> getTbGKBDetailCollection() {
        return tbGKBDetailCollection;
    }

    public void setTbGKBDetailCollection(Collection<TbGKBDetail> tbGKBDetailCollection) {
        this.tbGKBDetailCollection = tbGKBDetailCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gkbid != null ? gkbid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbGKB)) {
            return false;
        }
        TbGKB other = (TbGKB) object;
        if ((this.gkbid == null && other.gkbid != null) || (this.gkbid != null && !this.gkbid.equals(other.gkbid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "project.ett.TbGKB[ gkbid=" + gkbid + " ]";
    }
    
}
