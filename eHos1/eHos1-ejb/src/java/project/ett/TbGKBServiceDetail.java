/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project.ett;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DaySLeePer
 */
@Entity
@Table(name = "tbGKB_ServiceDetail")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbGKBServiceDetail.findAll", query = "SELECT t FROM TbGKBServiceDetail t"),
    @NamedQuery(name = "TbGKBServiceDetail.findByGKBServiceDetailID", query = "SELECT t FROM TbGKBServiceDetail t WHERE t.gKBServiceDetailID = :gKBServiceDetailID"),
    @NamedQuery(name = "TbGKBServiceDetail.findByTenDV", query = "SELECT t FROM TbGKBServiceDetail t WHERE t.tenDV = :tenDV")})
public class TbGKBServiceDetail implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "GKB_ServiceDetailID")
    private Integer gKBServiceDetailID;
    @Size(max = 50)
    @Column(name = "TenDV")
    private String tenDV;
    @JoinColumn(name = "GKB_ServiceID", referencedColumnName = "GKB_ServiceID")
    @ManyToOne
    private TbGKBService gKBServiceID;

    public TbGKBServiceDetail() {
    }

    public TbGKBServiceDetail(Integer gKBServiceDetailID) {
        this.gKBServiceDetailID = gKBServiceDetailID;
    }

    public Integer getGKBServiceDetailID() {
        return gKBServiceDetailID;
    }

    public void setGKBServiceDetailID(Integer gKBServiceDetailID) {
        this.gKBServiceDetailID = gKBServiceDetailID;
    }

    public String getTenDV() {
        return tenDV;
    }

    public void setTenDV(String tenDV) {
        this.tenDV = tenDV;
    }

    public TbGKBService getGKBServiceID() {
        return gKBServiceID;
    }

    public void setGKBServiceID(TbGKBService gKBServiceID) {
        this.gKBServiceID = gKBServiceID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gKBServiceDetailID != null ? gKBServiceDetailID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbGKBServiceDetail)) {
            return false;
        }
        TbGKBServiceDetail other = (TbGKBServiceDetail) object;
        if ((this.gKBServiceDetailID == null && other.gKBServiceDetailID != null) || (this.gKBServiceDetailID != null && !this.gKBServiceDetailID.equals(other.gKBServiceDetailID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "project.ett.TbGKBServiceDetail[ gKBServiceDetailID=" + gKBServiceDetailID + " ]";
    }
    
}
