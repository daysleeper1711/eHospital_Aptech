/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project.ett;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DaySLeePer
 */
@Entity
@Table(name = "tbGKB_Service")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbGKBService.findAll", query = "SELECT t FROM TbGKBService t"),
    @NamedQuery(name = "TbGKBService.findByGKBServiceID", query = "SELECT t FROM TbGKBService t WHERE t.gKBServiceID = :gKBServiceID"),
    @NamedQuery(name = "TbGKBService.findByTenDV", query = "SELECT t FROM TbGKBService t WHERE t.tenDV = :tenDV"),
    @NamedQuery(name = "TbGKBService.findByLoaiDV", query = "SELECT t FROM TbGKBService t WHERE t.loaiDV = :loaiDV")})
public class TbGKBService implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "GKB_ServiceID")
    private Integer gKBServiceID;
    @Size(max = 50)
    @Column(name = "TenDV")
    private String tenDV;
    @Size(max = 50)
    @Column(name = "LoaiDV")
    private String loaiDV;
    @OneToMany(mappedBy = "gKBServiceID")
    private Collection<TbKQKB> tbKQKBCollection;
    @OneToMany(mappedBy = "gKBServiceID")
    private Collection<TbGKBServiceDetail> tbGKBServiceDetailCollection;
    @JoinColumn(name = "ServiceID", referencedColumnName = "ServiceID")
    @ManyToOne
    private TbService serviceID;
    @JoinColumn(name = "GKB_DetailID", referencedColumnName = "GKB_DetailID")
    @ManyToOne
    private TbGKBDetail gKBDetailID;

    public TbGKBService() {
    }

    public TbGKBService(Integer gKBServiceID) {
        this.gKBServiceID = gKBServiceID;
    }

    public Integer getGKBServiceID() {
        return gKBServiceID;
    }

    public void setGKBServiceID(Integer gKBServiceID) {
        this.gKBServiceID = gKBServiceID;
    }

    public String getTenDV() {
        return tenDV;
    }

    public void setTenDV(String tenDV) {
        this.tenDV = tenDV;
    }

    public String getLoaiDV() {
        return loaiDV;
    }

    public void setLoaiDV(String loaiDV) {
        this.loaiDV = loaiDV;
    }

    @XmlTransient
    public Collection<TbKQKB> getTbKQKBCollection() {
        return tbKQKBCollection;
    }

    public void setTbKQKBCollection(Collection<TbKQKB> tbKQKBCollection) {
        this.tbKQKBCollection = tbKQKBCollection;
    }

    @XmlTransient
    public Collection<TbGKBServiceDetail> getTbGKBServiceDetailCollection() {
        return tbGKBServiceDetailCollection;
    }

    public void setTbGKBServiceDetailCollection(Collection<TbGKBServiceDetail> tbGKBServiceDetailCollection) {
        this.tbGKBServiceDetailCollection = tbGKBServiceDetailCollection;
    }

    public TbService getServiceID() {
        return serviceID;
    }

    public void setServiceID(TbService serviceID) {
        this.serviceID = serviceID;
    }

    public TbGKBDetail getGKBDetailID() {
        return gKBDetailID;
    }

    public void setGKBDetailID(TbGKBDetail gKBDetailID) {
        this.gKBDetailID = gKBDetailID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gKBServiceID != null ? gKBServiceID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbGKBService)) {
            return false;
        }
        TbGKBService other = (TbGKBService) object;
        if ((this.gKBServiceID == null && other.gKBServiceID != null) || (this.gKBServiceID != null && !this.gKBServiceID.equals(other.gKBServiceID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "project.ett.TbGKBService[ gKBServiceID=" + gKBServiceID + " ]";
    }
    
}
