/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project.ett;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DaySLeePer
 */
@Entity
@Table(name = "tbEx_Durgs_Supplies_Details")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbExDurgsSuppliesDetails.findAll", query = "SELECT t FROM TbExDurgsSuppliesDetails t"),
    @NamedQuery(name = "TbExDurgsSuppliesDetails.findByExDetails", query = "SELECT t FROM TbExDurgsSuppliesDetails t WHERE t.exDetails = :exDetails"),
    @NamedQuery(name = "TbExDurgsSuppliesDetails.findByItemCode", query = "SELECT t FROM TbExDurgsSuppliesDetails t WHERE t.itemCode = :itemCode"),
    @NamedQuery(name = "TbExDurgsSuppliesDetails.findByPackage1", query = "SELECT t FROM TbExDurgsSuppliesDetails t WHERE t.package1 = :package1"),
    @NamedQuery(name = "TbExDurgsSuppliesDetails.findByWarDate", query = "SELECT t FROM TbExDurgsSuppliesDetails t WHERE t.warDate = :warDate"),
    @NamedQuery(name = "TbExDurgsSuppliesDetails.findByQuantity", query = "SELECT t FROM TbExDurgsSuppliesDetails t WHERE t.quantity = :quantity"),
    @NamedQuery(name = "TbExDurgsSuppliesDetails.findByXPrice", query = "SELECT t FROM TbExDurgsSuppliesDetails t WHERE t.xPrice = :xPrice")})
public class TbExDurgsSuppliesDetails implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "Ex_Details")
    private String exDetails;
    @Size(max = 20)
    @Column(name = "ItemCode")
    private String itemCode;
    @Size(max = 32)
    @Column(name = "Package")
    private String package1;
    @Column(name = "War_Date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date warDate;
    @Column(name = "Quantity")
    private Integer quantity;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "X_Price")
    private Double xPrice;
    @JoinColumn(name = "Ex_ID", referencedColumnName = "Ex_ID")
    @ManyToOne
    private TbExDurgsSupplies exID;

    public TbExDurgsSuppliesDetails() {
    }

    public TbExDurgsSuppliesDetails(String exDetails) {
        this.exDetails = exDetails;
    }

    public String getExDetails() {
        return exDetails;
    }

    public void setExDetails(String exDetails) {
        this.exDetails = exDetails;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getPackage1() {
        return package1;
    }

    public void setPackage1(String package1) {
        this.package1 = package1;
    }

    public Date getWarDate() {
        return warDate;
    }

    public void setWarDate(Date warDate) {
        this.warDate = warDate;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Double getXPrice() {
        return xPrice;
    }

    public void setXPrice(Double xPrice) {
        this.xPrice = xPrice;
    }

    public TbExDurgsSupplies getExID() {
        return exID;
    }

    public void setExID(TbExDurgsSupplies exID) {
        this.exID = exID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (exDetails != null ? exDetails.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbExDurgsSuppliesDetails)) {
            return false;
        }
        TbExDurgsSuppliesDetails other = (TbExDurgsSuppliesDetails) object;
        if ((this.exDetails == null && other.exDetails != null) || (this.exDetails != null && !this.exDetails.equals(other.exDetails))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "project.ett.TbExDurgsSuppliesDetails[ exDetails=" + exDetails + " ]";
    }
    
}
