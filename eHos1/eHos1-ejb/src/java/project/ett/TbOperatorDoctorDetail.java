/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project.ett;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DaySLeePer
 */
@Entity
@Table(name = "tbOperatorDoctorDetail")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbOperatorDoctorDetail.findAll", query = "SELECT t FROM TbOperatorDoctorDetail t"),
    @NamedQuery(name = "TbOperatorDoctorDetail.findByOperatorDoctorDetailID", query = "SELECT t FROM TbOperatorDoctorDetail t WHERE t.operatorDoctorDetailID = :operatorDoctorDetailID"),
    @NamedQuery(name = "TbOperatorDoctorDetail.findByStatusPosition", query = "SELECT t FROM TbOperatorDoctorDetail t WHERE t.statusPosition = :statusPosition"),
    @NamedQuery(name = "TbOperatorDoctorDetail.findByCreateDate", query = "SELECT t FROM TbOperatorDoctorDetail t WHERE t.createDate = :createDate")})
public class TbOperatorDoctorDetail implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "OperatorDoctorDetailID")
    private String operatorDoctorDetailID;
    @Column(name = "StatusPosition")
    private Boolean statusPosition;
    @Column(name = "CreateDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @JoinColumn(name = "SurgeryRecordID", referencedColumnName = "SurgeryRecordID")
    @ManyToOne
    private TbSurgeryRecord surgeryRecordID;
    @JoinColumn(name = "PositionID", referencedColumnName = "PositionID")
    @ManyToOne
    private TbPositionDoctor positionID;
    @JoinColumn(name = "UserID", referencedColumnName = "EmployeeID")
    @ManyToOne
    private TbEmployee userID;
    @JoinColumn(name = "DoctorID", referencedColumnName = "EmployeeID")
    @ManyToOne
    private TbEmployee doctorID;

    public TbOperatorDoctorDetail() {
    }

    public TbOperatorDoctorDetail(String operatorDoctorDetailID) {
        this.operatorDoctorDetailID = operatorDoctorDetailID;
    }

    public String getOperatorDoctorDetailID() {
        return operatorDoctorDetailID;
    }

    public void setOperatorDoctorDetailID(String operatorDoctorDetailID) {
        this.operatorDoctorDetailID = operatorDoctorDetailID;
    }

    public Boolean getStatusPosition() {
        return statusPosition;
    }

    public void setStatusPosition(Boolean statusPosition) {
        this.statusPosition = statusPosition;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public TbSurgeryRecord getSurgeryRecordID() {
        return surgeryRecordID;
    }

    public void setSurgeryRecordID(TbSurgeryRecord surgeryRecordID) {
        this.surgeryRecordID = surgeryRecordID;
    }

    public TbPositionDoctor getPositionID() {
        return positionID;
    }

    public void setPositionID(TbPositionDoctor positionID) {
        this.positionID = positionID;
    }

    public TbEmployee getUserID() {
        return userID;
    }

    public void setUserID(TbEmployee userID) {
        this.userID = userID;
    }

    public TbEmployee getDoctorID() {
        return doctorID;
    }

    public void setDoctorID(TbEmployee doctorID) {
        this.doctorID = doctorID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (operatorDoctorDetailID != null ? operatorDoctorDetailID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbOperatorDoctorDetail)) {
            return false;
        }
        TbOperatorDoctorDetail other = (TbOperatorDoctorDetail) object;
        if ((this.operatorDoctorDetailID == null && other.operatorDoctorDetailID != null) || (this.operatorDoctorDetailID != null && !this.operatorDoctorDetailID.equals(other.operatorDoctorDetailID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "project.ett.TbOperatorDoctorDetail[ operatorDoctorDetailID=" + operatorDoctorDetailID + " ]";
    }
    
}
