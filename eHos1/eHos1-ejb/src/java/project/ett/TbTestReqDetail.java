/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project.ett;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DaySLeePer
 */
@Entity
@Table(name = "tbTestReqDetail")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbTestReqDetail.findAll", query = "SELECT t FROM TbTestReqDetail t"),
    @NamedQuery(name = "TbTestReqDetail.findByTestReqDetailID", query = "SELECT t FROM TbTestReqDetail t WHERE t.testReqDetailID = :testReqDetailID"),
    @NamedQuery(name = "TbTestReqDetail.findByRealPrice", query = "SELECT t FROM TbTestReqDetail t WHERE t.realPrice = :realPrice"),
    @NamedQuery(name = "TbTestReqDetail.findByPatPrice", query = "SELECT t FROM TbTestReqDetail t WHERE t.patPrice = :patPrice"),
    @NamedQuery(name = "TbTestReqDetail.findByCreateDate", query = "SELECT t FROM TbTestReqDetail t WHERE t.createDate = :createDate"),
    @NamedQuery(name = "TbTestReqDetail.findByUpdateDate", query = "SELECT t FROM TbTestReqDetail t WHERE t.updateDate = :updateDate")})
public class TbTestReqDetail implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "TestReqDetailID")
    private Integer testReqDetailID;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "RealPrice")
    private Double realPrice;
    @Column(name = "PatPrice")
    private Double patPrice;
    @Column(name = "CreateDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "UpdateDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @JoinColumn(name = "TestReqID", referencedColumnName = "TestReqID")
    @ManyToOne
    private TbTestRequest testReqID;
    @JoinColumn(name = "ServiceID", referencedColumnName = "ServiceID")
    @ManyToOne
    private TbService serviceID;
    @JoinColumn(name = "UpdateEmp", referencedColumnName = "EmployeeID")
    @ManyToOne
    private TbEmployee updateEmp;
    @JoinColumn(name = "Creator", referencedColumnName = "EmployeeID")
    @ManyToOne
    private TbEmployee creator;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "reqDetailID")
    private Collection<TbTestResultDetail> tbTestResultDetailCollection;

    public TbTestReqDetail() {
    }

    public TbTestReqDetail(Integer testReqDetailID) {
        this.testReqDetailID = testReqDetailID;
    }

    public Integer getTestReqDetailID() {
        return testReqDetailID;
    }

    public void setTestReqDetailID(Integer testReqDetailID) {
        this.testReqDetailID = testReqDetailID;
    }

    public Double getRealPrice() {
        return realPrice;
    }

    public void setRealPrice(Double realPrice) {
        this.realPrice = realPrice;
    }

    public Double getPatPrice() {
        return patPrice;
    }

    public void setPatPrice(Double patPrice) {
        this.patPrice = patPrice;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public TbTestRequest getTestReqID() {
        return testReqID;
    }

    public void setTestReqID(TbTestRequest testReqID) {
        this.testReqID = testReqID;
    }

    public TbService getServiceID() {
        return serviceID;
    }

    public void setServiceID(TbService serviceID) {
        this.serviceID = serviceID;
    }

    public TbEmployee getUpdateEmp() {
        return updateEmp;
    }

    public void setUpdateEmp(TbEmployee updateEmp) {
        this.updateEmp = updateEmp;
    }

    public TbEmployee getCreator() {
        return creator;
    }

    public void setCreator(TbEmployee creator) {
        this.creator = creator;
    }
    
    @XmlTransient
    public Collection<TbTestResultDetail> getTbTestResultDetailCollection() {
        return tbTestResultDetailCollection;
    }

    public void setTbTestResultDetailCollection(Collection<TbTestResultDetail> tbTestResultDetailCollection) {
        this.tbTestResultDetailCollection = tbTestResultDetailCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (testReqDetailID != null ? testReqDetailID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbTestReqDetail)) {
            return false;
        }
        TbTestReqDetail other = (TbTestReqDetail) object;
        if ((this.testReqDetailID == null && other.testReqDetailID != null) || (this.testReqDetailID != null && !this.testReqDetailID.equals(other.testReqDetailID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "project.ett.TbTestReqDetail[ testReqDetailID=" + testReqDetailID + " ]";
    }
    
}
