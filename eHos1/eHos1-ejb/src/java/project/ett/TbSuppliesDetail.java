/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project.ett;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DaySLeePer
 */
@Entity
@Table(name = "tbSuppliesDetail")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbSuppliesDetail.findAll", query = "SELECT t FROM TbSuppliesDetail t"),
    @NamedQuery(name = "TbSuppliesDetail.findBySuppliesDetailID", query = "SELECT t FROM TbSuppliesDetail t WHERE t.suppliesDetailID = :suppliesDetailID"),
    @NamedQuery(name = "TbSuppliesDetail.findByUnit", query = "SELECT t FROM TbSuppliesDetail t WHERE t.unit = :unit"),
    @NamedQuery(name = "TbSuppliesDetail.findByQuantity", query = "SELECT t FROM TbSuppliesDetail t WHERE t.quantity = :quantity"),
    @NamedQuery(name = "TbSuppliesDetail.findByStatusSupplies", query = "SELECT t FROM TbSuppliesDetail t WHERE t.statusSupplies = :statusSupplies"),
    @NamedQuery(name = "TbSuppliesDetail.findByCreateDate", query = "SELECT t FROM TbSuppliesDetail t WHERE t.createDate = :createDate")})
public class TbSuppliesDetail implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "SuppliesDetailID")
    private String suppliesDetailID;
    @Size(max = 50)
    @Column(name = "Unit")
    private String unit;
    @Column(name = "Quantity")
    private Integer quantity;
    @Column(name = "StatusSupplies")
    private Boolean statusSupplies;
    @Column(name = "CreateDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @JoinColumn(name = "SurgeryRecordID", referencedColumnName = "SurgeryRecordID")
    @ManyToOne
    private TbSurgeryRecord surgeryRecordID;
    @JoinColumn(name = "SuppliesID", referencedColumnName = "ItemCode")
    @ManyToOne
    private TbItemCodeManagement suppliesID;
    @JoinColumn(name = "UserID", referencedColumnName = "EmployeeID")
    @ManyToOne
    private TbEmployee userID;

    public TbSuppliesDetail() {
    }

    public TbSuppliesDetail(String suppliesDetailID) {
        this.suppliesDetailID = suppliesDetailID;
    }

    public String getSuppliesDetailID() {
        return suppliesDetailID;
    }

    public void setSuppliesDetailID(String suppliesDetailID) {
        this.suppliesDetailID = suppliesDetailID;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Boolean getStatusSupplies() {
        return statusSupplies;
    }

    public void setStatusSupplies(Boolean statusSupplies) {
        this.statusSupplies = statusSupplies;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public TbSurgeryRecord getSurgeryRecordID() {
        return surgeryRecordID;
    }

    public void setSurgeryRecordID(TbSurgeryRecord surgeryRecordID) {
        this.surgeryRecordID = surgeryRecordID;
    }

    public TbItemCodeManagement getSuppliesID() {
        return suppliesID;
    }

    public void setSuppliesID(TbItemCodeManagement suppliesID) {
        this.suppliesID = suppliesID;
    }

    public TbEmployee getUserID() {
        return userID;
    }

    public void setUserID(TbEmployee userID) {
        this.userID = userID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (suppliesDetailID != null ? suppliesDetailID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbSuppliesDetail)) {
            return false;
        }
        TbSuppliesDetail other = (TbSuppliesDetail) object;
        if ((this.suppliesDetailID == null && other.suppliesDetailID != null) || (this.suppliesDetailID != null && !this.suppliesDetailID.equals(other.suppliesDetailID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "project.ett.TbSuppliesDetail[ suppliesDetailID=" + suppliesDetailID + " ]";
    }
    
}
