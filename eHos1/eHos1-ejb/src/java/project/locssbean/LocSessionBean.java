/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project.locssbean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import project.ett.TbExDurgsSupplies;
import project.ett.TbExDurgsSuppliesDetails;
import project.ett.TbImDurgsSupplies;
import project.ett.TbImDurgsSuppliesDetails;
import project.ett.TbItemCodeManagement;
import project.ett.TbSupplier;
import project.ett.TbWhouse;
import project.locdto.ExDurgsSuppliesHeader_Details_DTO;
import project.locdto.ImDurgsSuppliesHeader_Details_DTO;


/**
 *
 * @author locth-laptop
 */
@Stateless
@LocalBean
public class LocSessionBean {

    @PersistenceContext(unitName = "eHos1-ejbPU")
    private EntityManager em;

    public void persist(Object object) {
        em.persist(object);
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
//***************************** Supplier Management *****************************************************
//=========================== Insert Supplier ===========================================================
    public void insertSupplier(TbSupplier dto) {
        try {
            dto.setSupplierID(identity("SLIE"));
            em.persist(dto);
            em.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//=========================== Update Supplier ===========================================================
    public void updateSupplier(TbSupplier dto) {
        try {
            TbSupplier up_dto = em.find(TbSupplier.class, dto.getSupplierID());
            up_dto.setSupplierName(dto.getSupplierName());
            up_dto.setUStatus(dto.getUStatus());
            em.persist(up_dto);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//=========================== Find By SupplierID ===========================================================    
    public TbSupplier findbySupplierID(String key) {
        TbSupplier dto = new TbSupplier();
        try {
            dto = em.find(TbSupplier.class, key);
            return dto;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

//=========================== Find By Like Supplier Name ===========================================================    
    public List<TbSupplier> findbyLikeSupplierName(String key) {

        try {
            List<TbSupplier> list = new ArrayList<TbSupplier>();
            String sql = "SELECT t FROM TbSupplier t WHERE t.supplierName like :supplierName";
            Query query = em.createQuery(sql).setParameter("supplierName", "%" + key + "%");
            list = query.getResultList();
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

//=========================== Find By Supplier Status ===========================================================        
    public List<TbSupplier> findbySupplierStatus(String key) {
        try {
            List<TbSupplier> list = new ArrayList<TbSupplier>();
            String sql = "SELECT t FROM TbSupplier t WHERE t.uStatus = :uStatus";
            Query query = em.createQuery(sql).setParameter("uStatus", key);
            list = query.getResultList();
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

//=========================== Find By All Supplier ===========================================================        
    public List<TbSupplier> findbyAllSupplier() {
        try {
            List<TbSupplier> list = new ArrayList<TbSupplier>();
            String sql = "SELECT t FROM TbSupplier t";
            Query query = em.createQuery(sql);
            list = query.getResultList();
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

//***************************** ItemCode Management *****************************************************
//=========================== Find By ItemCode ===========================================================        
    public TbItemCodeManagement findbyItemCode(String key) {
        try {
            TbItemCodeManagement dto = em.find(TbItemCodeManagement.class, key);
            return dto;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

//=========================== Find By Item Name ===========================================================        
    public List<TbItemCodeManagement> findbyLikeItemName(String key) {
        try {
            List<TbItemCodeManagement> list = new ArrayList<TbItemCodeManagement>();
            String sql = "SELECT t FROM TbItemCodeManagement t WHERE t.itemName like :itemName";
            Query query = em.createQuery(sql).setParameter("itemName", "%" + key + "%");
            list = query.getResultList();
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

//=========================== Find By All ItemCode ===========================================================        
    public List<TbItemCodeManagement> findbyAllItemCode() {
        try {
            List<TbItemCodeManagement> list = new ArrayList<TbItemCodeManagement>();
            String sql = "SELECT t FROM TbItemCodeManagement t";
            Query query = em.createQuery(sql);
            list = query.getResultList();
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

//=========================== Find By GenericDrug ===========================================================        
    public List<TbItemCodeManagement> findbyLikeGenericDrug(String key) {
        try {
            List<TbItemCodeManagement> list = new ArrayList<TbItemCodeManagement>();
            String sql = "SELECT t FROM TbItemCodeManagement t WHERE t.genericDrug like :genericDrug";
            Query query = em.createQuery(sql).setParameter("genericDrug", "%" + key + "%");
            list = query.getResultList();
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

//=========================== Find By Type ===========================================================        
    public List<TbItemCodeManagement> findbyType(String key) {
        try {
            List<TbItemCodeManagement> list = new ArrayList<TbItemCodeManagement>();
            String sql = "SELECT t FROM TbItemCodeManagement t WHERE t.typeITcode = :typeITcode";
            Query query = em.createQuery(sql).setParameter("typeITcode", key);
            list = query.getResultList();
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

//=========================== Update ItemCode ===========================================================
    public void updateItemCode(TbItemCodeManagement dto, String user) {
        try {
            TbItemCodeManagement up_dto = new TbItemCodeManagement();
            up_dto = em.find(TbItemCodeManagement.class, dto.getItemCode());
            if (up_dto != null) {
                up_dto.setItemName(dto.getItemName());
                up_dto.setUStatus(dto.getUStatus());
                up_dto.setContent(dto.getContent());
                up_dto.setGenericDrug(dto.getGenericDrug());
                up_dto.setHowToUse(dto.getHowToUse());
                up_dto.setNation(dto.getNation());
                up_dto.setTypeITcode(dto.getTypeITcode());
                up_dto.setUnit(dto.getUnit());
                up_dto.setXPrice(dto.getXPrice());
                // ngay update, user update
                Date d = new Date();
                up_dto.setUpDate(d);
                up_dto.setUpdateby(user);
            }
            em.persist(up_dto);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//=========================== Insert ItemCode ===========================================================
    public void insertItemCode(TbItemCodeManagement dto, String user) {
        try {
            TbItemCodeManagement insert_dto = new TbItemCodeManagement();
            insert_dto = dto;

            Date d = new Date();

            insert_dto.setItemCode(identity("TICO"));
            insert_dto.setUStatus("A");
            insert_dto.setCrDate(d);
            insert_dto.setCreateby(user);
            insert_dto.setUpDate(d);
            insert_dto.setUpdateby(user);

            em.persist(insert_dto);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

//***************************** Warehouse *****************************************************
//=========================== Find by ItemCode ===========================================================
    public List<TbWhouse> findbyWarehouseItemCode(String key) {
        try {
            String sql = "SELECT t FROM TbWhouse t WHERE t.itemCode = :itemCode AND t.onHand > :onHand";
            Query query = em.createQuery(sql).setParameter("itemCode", key).setParameter("onHand", 0);
            List<TbWhouse> list = query.getResultList();
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

//=========================== Find by Package ===========================================================
    public List<TbWhouse> findbyWarehousePackage(String key) {

        try {
            List<TbWhouse> list = new ArrayList<TbWhouse>();
            String sql = "SELECT t FROM TbWhouse t WHERE t.package1 = :package1";
            Query query = em.createQuery(sql).setParameter("package1", key);
            list = query.getResultList();
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

//=========================== Insert Whouse ===========================================================
    public void insertWarehouse(TbWhouse dto) {
        try {
            //Lay PK
            String pk = identity("WHOE");
            TbWhouse insert_dto = new TbWhouse();
            insert_dto = dto;
            insert_dto.setWhouseID(pk);
            em.persist(dto);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//=========================== Lay nhung Itemcode con ton Whouse ===========================================================
    public List<TbWhouse> checkItemCodeWarehouseCanBook(String itemcode) {
        try {

            String sql = "SELECT t FROM TbWhouse t WHERE t.itemCode = :itemCode AND t.qtyAvailable > :qtyAvailable";
            Query query = em.createQuery(sql).setParameter("qtyAvailable", 0).setParameter("itemCode", itemcode);
            List<TbWhouse> list = query.getResultList();
            return list;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

//=========================== Book Whouse ===========================================================
    public List<TbWhouse> bookWhouse(List<TbWhouse> list, int quantity, String pkHeaderExport) {
        try {

            int conlai = quantity;

            while (conlai > 0) {
                int fifo = 0;
                for (TbWhouse item : list) {
                    //Lay ra lo hang nhap trc xuat trc
                    int a = Integer.parseInt(item.getPackage1().substring(4, item.getPackage1().length()));
                    if (fifo == 0 || fifo > a) {
                        fifo = a;
                    }
                }
                //Lay ra lo hang nhap trc
                String pacK = "IMPO" + fifo;
                TbWhouse dto_whouse = new TbWhouse();
                for (TbWhouse item : list) {
                    if (pacK.equals(item.getPackage1())) {
                        dto_whouse = item;
                    }
                }

                //Cap nhap kho
                if (conlai >= dto_whouse.getQtyAvailable()) {
                    //Book Whouse
                    conlai = conlai - dto_whouse.getQtyAvailable();
                    int setQuantity = dto_whouse.getQtyAvailable();

                    dto_whouse.setQtyAvailable(setQuantity - setQuantity);
                    dto_whouse.setQtyOrder(dto_whouse.getQtyOrder() + setQuantity);
                    em.persist(dto_whouse);

                    //Tao details cho Export
                    TbExDurgsSuppliesDetails dto_exportdetails = new TbExDurgsSuppliesDetails();
                    dto_exportdetails.setItemCode(dto_whouse.getItemCode());
                    dto_exportdetails.setPackage1(dto_whouse.getPackage1());
                    dto_exportdetails.setQuantity(setQuantity);
                    dto_exportdetails.setWarDate(dto_whouse.getWarDate());
                    //Lay gia ban
                    TbItemCodeManagement dto_itemcode = em.find(TbItemCodeManagement.class, dto_whouse.getItemCode());
                    dto_exportdetails.setXPrice(dto_itemcode.getXPrice());
                    //insert 
                    insertExportDrugsDetals(dto_exportdetails, pkHeaderExport);

                } else if (conlai < dto_whouse.getQtyAvailable()) {
                    //Book Whouse
                    int setQuantity = dto_whouse.getQtyAvailable() - conlai;

                    dto_whouse.setQtyAvailable(setQuantity);
                    dto_whouse.setQtyOrder(dto_whouse.getQtyOrder() + conlai);

                    em.persist(dto_whouse);

                    //Tao details cho Export
                    TbExDurgsSuppliesDetails dto_exportdetails = new TbExDurgsSuppliesDetails();
                    dto_exportdetails.setItemCode(dto_whouse.getItemCode());
                    dto_exportdetails.setPackage1(dto_whouse.getPackage1());
                    dto_exportdetails.setQuantity(conlai);
                    dto_exportdetails.setWarDate(dto_whouse.getWarDate());
                    //Lay gia ban
                    TbItemCodeManagement dto_itemcode = em.find(TbItemCodeManagement.class, dto_whouse.getItemCode());
                    dto_exportdetails.setXPrice(dto_itemcode.getXPrice());
                    //insert 
                    insertExportDrugsDetals(dto_exportdetails, pkHeaderExport);

                    // Cap nhat lai sl con lai
                    conlai = 0;
                }

                //Remove dto da dung ra khoi List
                list.remove(dto_whouse);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

//=========================== Nha Book Whouse ===========================================================
    public void nhaBook(TbExDurgsSupplies dto_exportheader, List<TbExDurgsSuppliesDetails> list_exportdetails, List<TbWhouse> list_whouse, int quatity) {
        try {
            //Nha book
            for (TbExDurgsSuppliesDetails item : list_exportdetails) {
                //Lay so luong da book
                int sldabook = item.getQuantity();
                for (TbWhouse item2 : list_whouse) {
                    if (item2.getPackage1().equals(item.getPackage1())) {
                        //Update lai sl trong Whouse
                        int slhientaican = item2.getQtyAvailable();
                        item2.setQtyAvailable(slhientaican + sldabook);
                        int sldabookwhouse = item2.getQtyOrder();
                        item2.setQtyOrder(sldabookwhouse - sldabook);
                        em.persist(item2);

                        dto_exportheader.getTbExDurgsSuppliesDetailsCollection().remove(item);
                        em.remove(item);
                    }
                }
            }

            //Check neu khong can book san pham nua thi xoa header
            List<TbExDurgsSuppliesDetails> listemp = (List<TbExDurgsSuppliesDetails>) dto_exportheader.getTbExDurgsSuppliesDetailsCollection();
            if (listemp.size() == 0 && quatity == 0) {
                em.remove(dto_exportheader);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

//=========================== Lay ra dong da book Whouse ===========================================================
    public TbWhouse getLineBookedWhouse(String itemcode, String package1) {
        try {

            String sql = "SELECT t FROM TbWhouse t WHERE t.itemCode = :itemCode AND t.package1 = :package1";
            Query query = em.createQuery(sql).setParameter("itemCode", itemcode).setParameter("package1", package1);
            TbWhouse dto = (TbWhouse) query.getSingleResult();
            return dto;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

//=========================== Xu Ly Export Whouse ===========================================================
    public boolean xuLyExportWarehouse(String docentry, String type, String itemcode, int quantity, String note, String user) {
        try {

            //Check co chung tu xuat hang chua ==> Lay qua Phieu xuat
            String pk = "";
            TbExDurgsSupplies dto = new TbExDurgsSupplies();
            String sql = "SELECT t FROM TbExDurgsSupplies t WHERE t.docentry = :docentry AND t.typeEx = :typeEx";
            Query query = em.createQuery(sql).setParameter("docentry", docentry).setParameter("typeEx", type);
            Object obj = null;
            boolean checknulldto = false;
            try {
                obj = query.getSingleResult();
                checknulldto = true;
            } catch (Exception e) {
                checknulldto = false;
            }

            if (checknulldto) {
                dto = (TbExDurgsSupplies) obj;
            } else {
                dto = null;
            }

            //Lay danh sach itemcode ton
            List<TbWhouse> listwhouse = checkItemCodeWarehouseCanBook(itemcode);

            if (dto != null) {
                //Da ton tai
                pk = dto.getExID();
                //Check itemcode đa tồn chưa

                List<TbExDurgsSuppliesDetails> list_exportdetails = (List<TbExDurgsSuppliesDetails>) dto.getTbExDurgsSuppliesDetailsCollection();
                int sldabook = 0;
                List<TbWhouse> list_whouse = new ArrayList<TbWhouse>();
                List<TbExDurgsSuppliesDetails> list_book = new ArrayList<TbExDurgsSuppliesDetails>();
                for (TbExDurgsSuppliesDetails item : list_exportdetails) {
                    if (itemcode.equals(item.getItemCode())) {
                        sldabook += item.getQuantity();
                        //Lay ra dto exportdetails da book
                        list_book.add(item);
                        //Lay ra dong Kho da book
                        TbWhouse dto_a = getLineBookedWhouse(item.getItemCode(), item.getPackage1());
                        list_whouse.add(dto_a);
                    }
                }

                //Itemcode co ton/da book
                if (sldabook > 0) {
                    //Lay sl con trong cua itemcode
                    int slcanbook = 0;
                    List<TbWhouse> listwhousecanbook = checkItemCodeWarehouseCanBook(itemcode);
                    for (TbWhouse item : listwhousecanbook) {
                        slcanbook += item.getQtyAvailable();
                    }

                    int sltotat = slcanbook + sldabook;

                    if (sltotat >= quantity) {
                        // Nha book
                        nhaBook(dto, list_book, list_whouse, quantity);
                        listwhouse = checkItemCodeWarehouseCanBook(itemcode);
                        bookWhouse(listwhouse, quantity, pk);
                        return true;
                    } else {
                        return false;
                    }
                }

            } else {

                //Check itemcode co du de book
                int sumQuantity = 0;
                for (TbWhouse item : listwhouse) {
                    sumQuantity += item.getQtyAvailable();
                }
                if (sumQuantity < quantity) {
                    return false;
                }

                //Chua co thi tao moi
                dto = new TbExDurgsSupplies();
                if (note == null) {
                    note = "";
                }
                dto.setNote(note);
                Date d = new Date();
                dto.setCrDate(d);
                dto.setUpDate(d);
                dto.setCreateby(user);
                dto.setUpdateby(user);
                dto.setTypeEx(type);
                dto.setDocentry(docentry);
                dto.setRetrun(Boolean.TRUE);
                dto.setUStatus("O");
                pk = insertExportDrugsHeader(dto);

                //Book Whouse
                bookWhouse(listwhouse, quantity, pk);
                return true;
            }

            //Check itemcode co du de book
            int sumQuantity = 0;
            for (TbWhouse item : listwhouse) {
                sumQuantity += item.getQtyAvailable();
            }
            if (sumQuantity < quantity) {
                return false;
            }

            //Book Whouse
            bookWhouse(listwhouse, quantity, pk);
            return true;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

//=========================== Finish Export-Whouse ===========================================================
    public TbExDurgsSupplies finishExportWhouse(String pkExportHeader) {
        try {
            //Check phieu da hoan tat chua
            boolean check = false;
            //Cap nhat trang thai phieu xuat ExportHeader
            if (updatefinishStatus_ExportDrugsHeader(pkExportHeader)) {
                check = true;
            }

            //Cap nhat kho
            if (check) {
                updateFinishExportWhouse(pkExportHeader);
            }

            TbExDurgsSupplies dto = em.find(TbExDurgsSupplies.class, pkExportHeader);
            return dto;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

//***************************** Import Drugs *****************************************************
//=========================== Insert Import Drugs Header =========================================================== 
    public String insertImportDrugsHeader(TbImDurgsSupplies dto, String pkSupplierID) {
        try {
            String pk = identity("IMPO");
            TbImDurgsSupplies insert_dto = new TbImDurgsSupplies();
            insert_dto = dto;
            insert_dto.setPackage1(pk);
            TbSupplier sup_dto = em.find(TbSupplier.class, pkSupplierID);
            insert_dto.setSupplierID(sup_dto);
            sup_dto.getTbImDurgsSuppliesCollection().add(insert_dto);
            return pk;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

//=========================== Insert Import Drugs Header =========================================================== 
    public List<TbImDurgsSupplies> findbyNumBill_ImportDrugsHeader(String key) {
        try {

            String sql = "SELECT t FROM TbImDurgsSupplies t WHERE t.numBill = :numBill";
            Query query = em.createQuery(sql).setParameter("numBill", key);
            List<TbImDurgsSupplies> list = query.getResultList();
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

//=========================== Insert Import Drugs Header =========================================================== 
    public TbSupplier findbySupplier_ImportDrugsHeader(String key) {
        try {

            TbImDurgsSupplies dto_im = em.find(TbImDurgsSupplies.class, key);
            TbSupplier dto_sup = dto_im.getSupplierID();
            return dto_sup;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

//=========================== Find by Package Import Drugs Header =========================================================== 
    public TbImDurgsSupplies findbyPackage_ImportDrugsHeader(String key) {
        try {
            TbImDurgsSupplies dto = new TbImDurgsSupplies();
            String sql = "SELECT t FROM TbImDurgsSupplies t WHERE t.package1 = :package1";
            Query query = em.createQuery(sql).setParameter("package1", key);
            dto = (TbImDurgsSupplies) query.getSingleResult();
            return dto;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

//=========================== Find by Time Import Drugs Header =========================================================== 
    public List<TbImDurgsSupplies> findbyTime_ImportDrugsHeader(Date fromdate, Date todate) {
        try {
            List<TbImDurgsSupplies> list = new ArrayList<TbImDurgsSupplies>();
            String sql = "SELECT t FROM TbImDurgsSupplies t WHERE t.crDate > :fromDate AND t.crDate < :toDate";
            Query query = em.createQuery(sql).setParameter("fromDate", fromdate).setParameter("toDate", todate);
            list = query.getResultList();
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

//=========================== Insert Import Drugs Details =========================================================== 
    public void insertImportDrugsDetails(TbImDurgsSuppliesDetails dto_details, String pk_header) {
        try {
            TbImDurgsSupplies dto_header = em.find(TbImDurgsSupplies.class, pk_header);
            dto_details.setPackage1(dto_header);
            String pk_details = identity("IMDE");
            dto_details.setImDetails(pk_details);
            dto_header.getTbImDurgsSuppliesDetailsCollection().add(dto_details);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//=========================== Find by Import Drugs Package  =========================================================== 
    public ImDurgsSuppliesHeader_Details_DTO findbyPackage_ImportHeader_Details(String key) {
        try {
            TbImDurgsSupplies dto_header = em.find(TbImDurgsSupplies.class, key);

            ImDurgsSuppliesHeader_Details_DTO head_details = new ImDurgsSuppliesHeader_Details_DTO();
            //set header in list
            head_details.setHeader(dto_header);

            //get details
            List<TbImDurgsSuppliesDetails> list_details = (List<TbImDurgsSuppliesDetails>) dto_header.getTbImDurgsSuppliesDetailsCollection();

            //set details in list
            head_details.setDetails(list_details);
            return head_details;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

//***************************** Export Drugs *****************************************************
//=========================== Insert Export Drugs Header ===========================================================
    public String insertExportDrugsHeader(TbExDurgsSupplies dto) {
        try {
            //Lay PK
            String pk = identity("EXPO");
            dto.setExID(pk);
            em.persist(dto);
            return pk;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

//=========================== Insert Export Drugs Detals ===========================================================
    public void insertExportDrugsDetals(TbExDurgsSuppliesDetails dto_details, String pk_header) {
        try {
            //Lay PK
            String pk_details = identity("EXDE");
            TbExDurgsSupplies dto_header = em.find(TbExDurgsSupplies.class, pk_header);
            dto_details.setExDetails(pk_details);
            dto_details.setExID(dto_header);
            dto_header.getTbExDurgsSuppliesDetailsCollection().add(dto_details);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//=========================== Find by Docentry anh Type Export Drugs Header =========================================================== 
    public List<TbExDurgsSupplies> findbyDocentry_Type_ExportDrugsHeader(String docentry, String type) {
        try {
            List<TbExDurgsSupplies> list = new ArrayList<TbExDurgsSupplies>();
            String sql = "SELECT t FROM TbExDurgsSupplies t WHERE t.docentry = :docentry AND t.typeEx = :typeEx";
            Query query = em.createQuery(sql).setParameter("docentry", docentry).setParameter("typeEx", type);
            list = query.getResultList();
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

//=========================== Find by Status Export Drugs Header =========================================================== 
    public List<TbExDurgsSupplies> findbyStatus_ExportDrugsHeader(String status) {
        try {
            List<TbExDurgsSupplies> list = new ArrayList<TbExDurgsSupplies>();
            String sql = "SELECT t FROM TbExDurgsSupplies t WHERE t.uStatus = :uStatus";
            Query query = em.createQuery(sql).setParameter("uStatus", status);
            list = query.getResultList();
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

//=========================== Update Finish Status Export Drugs Header =========================================================== 
    public boolean updatefinishStatus_ExportDrugsHeader(String pk) {
        try {
            TbExDurgsSupplies dto_ExHeader = em.find(TbExDurgsSupplies.class, pk);
            if (dto_ExHeader.getUStatus().equals("F")) {
                return false;
            }
            dto_ExHeader.setUStatus("F");
            em.persist(dto_ExHeader);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

//=========================== Update Finish Export Whouse =========================================================== 
    public void updateFinishExportWhouse(String pk) {
        try {
            TbExDurgsSupplies dto_ExHeader = em.find(TbExDurgsSupplies.class, pk);

            List<TbExDurgsSuppliesDetails> listdetails = (List<TbExDurgsSuppliesDetails>) dto_ExHeader.getTbExDurgsSuppliesDetailsCollection();

            for (TbExDurgsSuppliesDetails item : listdetails) {
                //Lay ra dong Kho da book
                TbWhouse dto_a = getLineBookedWhouse(item.getItemCode(), item.getPackage1());

                //Lay so luong can tru kho
                int quatityWhouse = item.getQuantity();

                //Tru kho
                dto_a.setOnHand(dto_a.getOnHand() - quatityWhouse);
                dto_a.setQtyOrder(dto_a.getQtyOrder() - quatityWhouse);
                em.persist(dto_a);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//=========================== Find by Time Export Drugs Header =========================================================== 
    public List<TbExDurgsSupplies> findbyTime_ExportDrugsHeader(Date fromdate, Date todate) {
        try {
            List<TbExDurgsSupplies> list = new ArrayList<TbExDurgsSupplies>();
            String sql = "SELECT t FROM TbExDurgsSupplies t WHERE t.crDate > :fromDate AND t.crDate < :toDate";
            Query query = em.createQuery(sql).setParameter("fromDate", fromdate).setParameter("toDate", todate);
            list = query.getResultList();
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

//=========================== Find by PK Header Export Header_Details =========================================================== 
    public ExDurgsSuppliesHeader_Details_DTO findbyPK_ExportDrugsHeader(String pk) {
        try {
            ExDurgsSuppliesHeader_Details_DTO dto = new ExDurgsSuppliesHeader_Details_DTO();
            TbExDurgsSupplies header = em.find(TbExDurgsSupplies.class, pk);

            List<TbExDurgsSuppliesDetails> details = (List<TbExDurgsSuppliesDetails>) header.getTbExDurgsSuppliesDetailsCollection();

            dto.setHeader(header);
            dto.setDetails(details);

            return dto;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

//***************************** Identity *****************************************************
//=========================== Identity All ===========================================================
    private String identity(String type) {
        String newID = "";
        Object obj = new Object();
        try {
            //itemcode
            if (type == "TICO") {
                String ejbQL = "SELECT t.itemCode FROM TbItemCodeManagement t";
                Query query = em.createQuery(ejbQL);
                obj = query.getResultList();
            } //export
            else if (type == "EXPO") {
                String ejbQL = "SELECT t.exID FROM TbExDurgsSupplies t";
                Query query = em.createQuery(ejbQL);
                obj = query.getResultList();
            } //export detail
            else if (type == "EXDE") {
                String ejbQL = "SELECT t.exDetails FROM TbExDurgsSuppliesDetails t";
                Query query = em.createQuery(ejbQL);
                obj = query.getResultList();
            } //import
            else if (type == "IMPO") {

                String ejbQL = "SELECT t.package1 FROM TbImDurgsSupplies t";
                Query query = em.createQuery(ejbQL);
                obj = query.getResultList();
            } //import detail
            else if (type == "IMDE") {
                String ejbQL = "SELECT t.imDetails FROM TbImDurgsSuppliesDetails t";
                Query query = em.createQuery(ejbQL);
                obj = query.getResultList();
            } //whouse
            else if (type == "WHOE") {
                String ejbQL = "SELECT t.whouseID FROM TbWhouse t";
                Query query = em.createQuery(ejbQL);
                obj = query.getResultList();
            } //Supplier
            else if (type == "SLIE") {
                String ejbQL = "SELECT t.supplierID FROM TbSupplier t";
                Query query = em.createQuery(ejbQL);
                obj = query.getResultList();
            }

            if (obj != null) {
                List<String> liststring = (List<String>) obj;
                int temp = 0;
                for (String item : liststring) {
                    int a = Integer.parseInt(item.substring(4, item.length()));
                    if (a > temp) {
                        temp = a;
                    }
                }
                int b = temp + 1;
                newID = type + b;
            } else {
                newID = type + 1;
            }

        } catch (Exception e) {

            e.printStackTrace();
        }
        return newID;
    }

}//end class
