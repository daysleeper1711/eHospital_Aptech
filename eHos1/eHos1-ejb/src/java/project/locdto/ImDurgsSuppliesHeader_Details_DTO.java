/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project.locdto;

import java.io.Serializable;
import java.util.List;
import project.ett.TbImDurgsSupplies;
import project.ett.TbImDurgsSuppliesDetails;

/**
 *
 * @author locth-laptop
 */
public class ImDurgsSuppliesHeader_Details_DTO implements Serializable{
    private TbImDurgsSupplies header;
    private List<TbImDurgsSuppliesDetails> details;

    public TbImDurgsSupplies getHeader() {
        return header;
    }

    public void setHeader(TbImDurgsSupplies header) {
        this.header = header;
    }

    public List<TbImDurgsSuppliesDetails> getDetails() {
        return details;
    }

    public void setDetails(List<TbImDurgsSuppliesDetails> details) {
        this.details = details;
    }
}
