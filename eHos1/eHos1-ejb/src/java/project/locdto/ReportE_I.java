/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project.locdto;

import java.io.Serializable;

/**
 *
 * @author locth-laptop
 */
public class ReportE_I implements Serializable{
    private String itemCode,itemName;
    private int quantity;

    public String getItemCode() {
        return itemCode;
    }

    public ReportE_I(String itemCode, String itemName, int quantity) {
        this.itemCode = itemCode;
        this.itemName = itemName;
        this.quantity = quantity;
    }

    public ReportE_I() {
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
