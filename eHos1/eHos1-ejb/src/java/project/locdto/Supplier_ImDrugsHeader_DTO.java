/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project.locdto;

import java.io.Serializable;
import java.util.List;
import project.ett.TbImDurgsSupplies;
import project.ett.TbSupplier;

/**
 *
 * @author locth-laptop
 */
public class Supplier_ImDrugsHeader_DTO implements Serializable{
    private TbSupplier s;
    private TbImDurgsSupplies i;

    public TbSupplier getS() {
        return s;
    }

    public Supplier_ImDrugsHeader_DTO() {
    }

    public Supplier_ImDrugsHeader_DTO(TbSupplier s, TbImDurgsSupplies i) {
        this.s = s;
        this.i = i;
    }

    public void setS(TbSupplier s) {
        this.s = s;
    }

    public TbImDurgsSupplies getI() {
        return i;
    }

    public void setI(TbImDurgsSupplies i) {
        this.i = i;
    }

    
}
