/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project.locdto;

import java.io.Serializable;
import java.util.List;
import project.ett.TbExDurgsSupplies;
import project.ett.TbExDurgsSuppliesDetails;

/**
 *
 * @author locth-laptop
 */
public class ExDurgsSuppliesHeader_Details_DTO implements Serializable{
    private TbExDurgsSupplies header;

    private List<TbExDurgsSuppliesDetails> details;

    public TbExDurgsSupplies getHeader() {
        return header;
    }

    public ExDurgsSuppliesHeader_Details_DTO(TbExDurgsSupplies header, List<TbExDurgsSuppliesDetails> details) {
        this.header = header;
        this.details = details;
    }

    public ExDurgsSuppliesHeader_Details_DTO() {
    }

    public void setHeader(TbExDurgsSupplies header) {
        this.header = header;
    }

    public List<TbExDurgsSuppliesDetails> getDetails() {
        return details;
    }

    public void setDetails(List<TbExDurgsSuppliesDetails> details) {
        this.details = details;
    }
    
    
}
