/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project.locdto;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author locth-laptop
 */
public class WhouseFullDTO implements Serializable{
    private String itemcode,itemname,package1,warDate;
    private int onHand,qtyAvailable,qtyOrder;
    

    public WhouseFullDTO(String itemcode, String itemname, String package1, int onHand, int qtyAvailable, int qtyOrder, String warDate) {
        this.itemcode = itemcode;
        this.itemname = itemname;
        this.package1 = package1;
        this.onHand = onHand;
        this.qtyAvailable = qtyAvailable;
        this.qtyOrder = qtyOrder;
        this.warDate = warDate;
    }

    public WhouseFullDTO() {
    }

    public String getItemcode() {
        return itemcode;
    }

    public void setItemcode(String itemcode) {
        this.itemcode = itemcode;
    }

    public String getItemname() {
        return itemname;
    }

    public void setItemname(String itemname) {
        this.itemname = itemname;
    }

    public String getPackage1() {
        return package1;
    }

    public void setPackage1(String package1) {
        this.package1 = package1;
    }

    public int getOnHand() {
        return onHand;
    }

    public void setOnHand(int onHand) {
        this.onHand = onHand;
    }

    public int getQtyAvailable() {
        return qtyAvailable;
    }

    public void setQtyAvailable(int qtyAvailable) {
        this.qtyAvailable = qtyAvailable;
    }

    public int getQtyOrder() {
        return qtyOrder;
    }

    public void setQtyOrder(int qtyOrder) {
        this.qtyOrder = qtyOrder;
    }

    public String getWarDate() {
        return warDate;
    }

    public void setWarDate(String warDate) {
        this.warDate = warDate;
    }
}
